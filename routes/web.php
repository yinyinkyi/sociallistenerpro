<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('mailPreview',['as' => 'mailPreview','uses' => 'MailController@mailPreview']);
Route::get('interactionCount',['as' => 'interactionCount','uses' => 'Inbound\inboundController@interactionCount']);

Route::get('testingcron',['as' => 'testingcron','uses' => 'CronjobController@testingcron']);
Route::get('dailyMail',['as' => 'dailyMail','uses' => 'CronjobController@dailyMail']);
Route::get('weeklyMail',['as' => 'weeklyMail','uses' => 'CronjobController@weeklyMail']);

Route::get('sentMail',['as' => 'sentMail','uses' => 'MailController@MailForm']);
Route::get('testMail',['as' => 'testMail','uses' => 'MailController@testMailForm']);
Route::get('SendMailReport',['as' => 'SendMailReport','uses' => 'MailController@SendMailReport']);

Route::get('pdfview',array('as'=>'pdfview','uses'=>'MailController@SendMailReport'));

Route::get('benchmarking',['as' => 'benchmarking','uses' => 'BenchMarkController@benchmarking']);

Route::get('getAllBench',['as' => 'getAllBench','uses' => 'BenchMarkController@getAllBench']);

Route::get('scheduleChange',['as' => 'scheduleChange','uses' => 'MailController@scheduleChange']);

Route::get('getMail',['as' => 'getMail','uses' => 'MailController@getMail']);
Route::get('MailPageSummary',['as' => 'MailPageSummary','uses' => 'MailController@MailPageSummary']);
Route::get('getMailMention',['as' => 'getMailMention','uses' => 'MailController@getMailMention']);
Route::get('getSentimentDetail',['as' => 'getSentimentDetail','uses' => 'MailController@getSentimentDetail']);
Route::get('getMailPostTag',['as' => 'getMailPostTag','uses' => 'MailController@getMailPostTag']);
Route::get('getBankType',['as' => 'getBankType','uses' => 'HomeController@getBankType']);

Route::get('DateTest',['as' => 'DateTest','uses' => 'MongoCRUDController@DateTest']);
Route::get('handle',['as' => 'handle','uses' => 'MongoCRUDController@handle']);
Route::get('autoTagging',['as' => 'autoTagging','uses' => 'HomeController@autoTagging']);
Route::get('updateByAutoTag',['as' => 'updateByAutoTag','uses' => 'HomeController@updateByAutoTag']);

Route::get('chargesAndRates',['as' => 'chargesAndRates','uses' => 'HomeController@chargesAndRates']);
Route::get('getExchangeRates',['as' => 'getExchangeRates','uses' => 'BankController@getExchangeRates']);
Route::get('getCallDeposit',['as' => 'getCallDeposit','uses' => 'BankController@getCallDeposit']);
Route::get('getfixedDeposit',['as' => 'getfixedDeposit','uses' => 'BankController@getfixedDeposit']);
Route::get('getIndustryPost',['as' => 'getIndustryPost','uses' => 'PageManageController@getIndustryPost']);
Route::get('getIndustryComment',['as' => 'getIndustryComment','uses' => 'PageManageController@getIndustryComment']);
Route::get('getAllCreditCard',['as' => 'getAllCreditCard','uses' => 'BankController@getAllCreditCard']);



Route::get('getGroupPost',['as' => 'getGroupPost','uses' => 'GroupController@getGroupPost']);
Route::get('getGroupComment',['as' => 'getGroupComment','uses' => 'GroupController@getGroupComment']);

Route::get('getRelatedGroupPosts',['as' => 'getRelatedGroupPosts','uses' => 'GroupController@getRelatedGroupPosts']);
Route::get('group_post',['as' => 'group_post','uses' => 'GroupController@index']);

Route::get('exchange_store',['as' => 'exchange_store','uses' => 'BankController@exchange_store']);

// just for testing Highlighted voice Cron
Route::get('insertHighlightedComment',['as' => 'insertHighlightedComment','uses' => 'MongoCRUDController@insertHighlightedComment']);

Route::get('get_highlighted_page_name',['as' => 'get_highlighted_page_name','uses' => 'CronjobController@get_highlighted_page_name']);

Route::get('bind_exchangeData',['as' => 'bind_exchangeData','uses' => 'BankController@bind_exchangeData']);
Route::get('bind_fixDepoData',['as' => 'bind_fixDepoData','uses' => 'BankController@bind_fixDepoData']);

Route::get('exchangeEntry',['as' => 'exchangeEntry','uses' => 'BankController@exchangeEntry']);
Route::get('fixedDepositEntry',['as' => 'fixedDepositEntry','uses' => 'BankController@fixedDepositEntry']);


Route::post('setMentionPredict',['as' => 'setMentionPredict','uses' => 'mentionDataController@setMentionPostPredict']);
Route::post('setArticlePredict',['as' => 'setArticlePredict','uses' => 'mentionDataController@setArticlePredict']);
Route::get('auth/{provider}', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

// Route::get('/', ['as' => '/','uses' => 'HomeController@index']);
Route::get('/', ['as' => 'brand','uses' => 'HomeController@index']);

/*Auth::routes();*/
Route::get('login', ['as' => 'login','uses' => 'Auth\LoginController@showLoginForm']);
Route::get('landing', ['as' => 'landing','uses' => 'Auth\LoginController@LandingPage']);
Route::post('login',['as' => '','uses' => 'Auth\LoginController@login']);

Route::get('comment', ['as' => 'updateComment','uses' => 'MongoCRUDController@commentUpdate']);

Route::get('tag',['as' => 'MakeTag','uses' => 'MongoCRUDController@doTag']);

Route::get('upload_asr',['as' => 'upload_asr','uses' => 'tagsController@upload_asr']);

// Password Reset Routes...
Route::post('password/email', [
  'as' => 'password.email',
  'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
  'as' => 'password.request',
  'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
  'as' => '',
  'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
  'as' => 'password.reset',
  'uses' => 'Auth\ResetPasswordController@showResetForm'
]);
Route::get('register', [
  'as' => 'register',
  'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register'
]);

Route::post('register_out', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register_out'
]);

Route::get('register_out', [
  'as' => 'register_out',
  'uses' => 'Auth\RegisterController@showOutRegistrationForm'
]);


Route::group(['middleware' => 'auth'], function () {
 
  Route::post('/notification/tag/notification','tagsController@notification');
  

Route::get('gethiddendiv', [
  'as'   => 'gethiddendiv',
  'uses' => 'HomeController@gethiddendiv'
]);

Route::get('getCronStatus', [
  'as'   => 'getCronStatus',
  'uses' => 'HomeController@getCronStatus'
]);


Route::get('pointoutmention', [
  'as'   => 'pointoutmention',
  'uses' => 'HomeController@pointoutmention'
]);

Route::get('pointoutcomment', [
  'as'   => 'pointoutcomment',
  'uses' => 'HomeController@pointoutcomment'
]);
Route::get('pointoutpost', [
  'as'   => 'pointoutpost',
  'uses' => 'HomeController@pointoutpost'
]);

Route::get('brand_store', [
  'as'   => 'brand_store',
  'uses' => 'ProjectController@brandStore'
]);
Route::post('campaign_store', [
  'as'   => 'campaign_store',
  'uses' => 'CampaignController@Store'
]);

Route::get('brand_update', [
  'as'   => 'brand_update',
  'uses' => 'ProjectController@brandUpdate'
]);
Route::post('campaign_update/', [
  'as'   => 'campaign_update',
  'uses' => 'CampaignController@Update'
]);

// Route::get('wordcloud',['as'=>'wordcloud','uses' => 'WordCloudController@index']);
Route::resource('wordcloud','WordCloudController');


Route::get('brandList/edit/{id}', [
  'as'   => 'brandList/edit',
  'uses' => 'ProjectController@brandEdit'
]);

Route::get('campaignlist', [
  'as'   => 'campaignlist',
  'uses' => 'CampaignController@campaignlist'
]);
Route::get('get_campaignList', [
  'as'   => 'get_campaignList',
  'uses' => 'CampaignController@get_campaignList'
]);

Route::get('campaign/create', [
  'as'   => 'campaign/create',
  'uses' => 'CampaignController@Create'
]);
Route::get('audience', [
  'as'   => 'audience',
  'uses' => 'CampaignController@audience'
]);
Route::post('audience_store', [
  'as'   => 'audience_store',
  'uses' => 'CampaignController@audience_store'
]);
Route::get('campaign/edit/{id}', [
  'as'   => 'campaign/edit',
  'uses' => 'CampaignController@Edit'
]);
Route::get('campaign/delete/{id}', [
  'as'   => 'campaign/delete',
  'uses' => 'CampaignController@delete'
]);

Route::get('brandList/delete/{id}', [
  'as'   => 'brandList/delete',
  'uses' => 'ProjectController@brand_delete'
]);

Route::get('brandList/create', [
  'as'   => 'brandList/create',
  'uses' => 'ProjectController@brandCreate'
]);
Route::get('keyword-group/edit/{id}', [
  'as'   => 'keyword-group/edit',
  'uses' => 'ProjectKeywordController@edit'
]);
Route::get('otherkeyword-group/edit/{id}', [
  'as'   => 'otherkeyword-group/edit',
  'uses' => 'OtherKeywordController@edit'
]);

Route::get('keyword-group/delete/{group_name}', [
  'as'   => 'keyword-group/delete',
  'uses' => 'ProjectKeywordController@keywordGroup_delete'
]);

Route::get('otherkeyword-group/delete/{group_name}', [
  'as'   => 'otherkeyword-group/delete',
  'uses' => 'OtherKeywordController@keywordGroup_delete'
]);

Route::get('keyword-group/create', [
  'as'   => 'keyword-group/create',
  'uses' => 'ProjectKeywordController@create'
]);
Route::get('otherkeyword-group/create', [
  'as'   => 'otherkeyword-group/create',
  'uses' => 'OtherKeywordController@create'
]);
Route::post('keyword-group-store', [
  'as'   => 'keyword-group-store',
  'uses' => 'ProjectKeywordController@store'
]);
Route::get('otherkeyword-group-store', [
  'as'   => 'otherkeyword-group-store',
  'uses' => 'OtherKeywordController@store'
]);
Route::post('keyword_group_update', [
  'as'   => 'keyword_group_update',
  'uses' => 'ProjectKeywordController@update'
]);
Route::get('otherkeyword_group_update', [
  'as'   => 'otherkeyword_group_update',
  'uses' => 'OtherKeywordController@update'
]);
Route::get('getallKeywords', [
  'as'   => 'getallKeywords',
  'uses' => 'ProjectKeywordController@getallKeywords'
]);
Route::get('getkeywordgroup', [
  'as'   => 'getkeywordgroup',
  'uses' => 'ProjectKeywordController@getkeywordgroup'
]);

Route::get('getotherkeywordgroup', [
  'as'   => 'getotherkeywordgroup',
  'uses' => 'OtherKeywordController@getotherkeywordgroup'
]);



Route::get('mention', [
  'as'   => 'mention',
  'uses' => 'HomeController@mention'
]);


Route::get('analysis', [
  'as'   => 'analysis',
  'uses' => 'HomeController@analysis'
]);

Route::get('comparison', [
  'as'   => 'comparison',
  'uses' => 'HomeController@comparison'
]);
Route::get('keygroupcomparison', [
  'as'   => 'keygroupcomparison',
  'uses' => 'HomeController@comparisonkeygroup'
]);


Route::get('competitor', [
  'as'   => 'competitor',
  'uses' => 'HomeController@competitor'
]);
Route::get('industry', [
  'as'   => 'industry',
  'uses' => 'HomeController@industry'
]);

Route::get('otherSources', [
  'as'   => 'otherSources',
  'uses' => 'HomeController@otherSources'
]);

Route::get('campaign', [
  'as'   => 'campaign',
  'uses' => 'HomeController@campaign'
]);


Route::get('insight', [
  'as'   => 'insight',
  'uses' => 'HomeController@insight'
]);
Route::get('page_manage', [
  'as'   => 'page_manage',
  'uses' => 'HomeController@page_manage'
]);
Route::get('insight_facebook', [
  'as'   => 'insight_facebook',
  'uses' => 'HomeController@insight_facebook'
]);

Route::get('monitor', [
  'as'   => 'monitor',
  'uses' => 'HomeController@monitor'
]);

Route::get('brandList', [
  'as'   => 'brandList',
  'uses' => 'HomeController@brandList'
]);
Route::get('keyword-group', [
  'as'   => 'keyword-group',
  'uses' => 'HomeController@keywordGroup'
]);
Route::get('other-keyword-group', [
  'as'   => 'other-keyword-group',
  'uses' => 'HomeController@OtherkeywordGroup'
]);
Route::get('getallpages', [
  'as'   => 'getallpages',
  'uses' => 'HomeController@getallpages'
]);
Route::get('getOtherPage', [
  'as'   => 'getOtherPage',
  'uses' => 'HomeController@getOtherPage'
]);
Route::get('getGroupName', [
  'as'   => 'getGroupName',
  'uses' => 'HomeController@getGroupName'
]);
Route::get('getBankPage', [
  'as'   => 'getBankPage',
  'uses' => 'HomeController@getBankPage'
]);
Route::get('getMfsPage', [
  'as'   => 'getMfsPage',
  'uses' => 'HomeController@getMfsPage'
]);
Route::get('getCardPage', [
  'as'   => 'getCardPage',
  'uses' => 'HomeController@getCardPage'
]);


Route::get('/dashboard', ['as' => 'dashboard','uses' => 'HomeController@dashboard']);
Route::get('post', ['as' => 'post','uses' => 'HomeController@post']);
Route::get('comment', ['as' => 'comment','uses' => 'HomeController@comment']);

Route::get('/charts', 
  ['as' => 'charts', function () {
    return view('charts.chartjs');
}]);

Route::resource('users','UserController');
Route::get('deleteuser/{id}', [ 'as' => 'deleteuser', 'uses' => 'UserController@destroy']);

Route::get('getuserlist', [ 'as' => 'getuserlist', 'uses' => 'UserController@getUserData']);

Route::get('getbrandlist', [ 'as' => 'getbrandlist', 'uses' => 'ProjectController@getProjectData']);
Route::get('getkeywordgrouplist',['as'=>'getkeywordgrouplist','uses'=>'ProjectKeywordController@getkeywordgrouplist']);
Route::get('getOtherkeywordgrouplist',['as'=>'getOtherkeywordgrouplist','uses'=>'OtherKeywordController@getkeywordgrouplist']);

/*Auth::routes();*/

Route::get('register', [
  'as' => 'register',
  'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register'
]);
Route::get('getmentionchartdatabymongo', [ 'as' => 'getmentionchartdatabymongo', 'uses' => 'MongodbDataController@getmentionchartdatabymongo']);
Route::get('getinteractiondatabymongo', [ 'as' => 'getinteractiondatabymongo', 'uses' => 'MongodbDataController@getinteractiondatabymongo']);
Route::get('getpopularmention', [ 'as' => 'getpopularmention', 'uses' => 'MongodbDataController@getpopularmention']);
Route::get('getlatestmention', [ 'as' => 'getlatestmention', 'uses' => 'MongodbDataController@getlatestmention']);
Route::get('getmentionchartdata',['as'=>'getmentionchartdata','uses'=>'RedshiftDataController@getmentionchartdata']);


Route::get('getRelatedStatus',['as'=>'getRelatedStatus','uses'=>'PageManageController@getRelatedStatus']);

//mention Data Controller
Route::get('getmentiondetail',['as'=>'getmentiondetail','uses'=>'mentionDataController@getmentiondetail']);
Route::get('getcampaignmentiondetail',['as'=>'getcampaignmentiondetail','uses'=>'CampaignController@getcampaignmentiondetail']);
Route::get('getsentimentdetail',['as'=>'getsentimentdetail','uses'=>'mentionDataController@getsentimentdetail']);

Route::get('getemotiondetail',['as'=>'getemotiondetail','uses'=>'mentionDataController@getemotiondetail']);
Route::get('getpopularnegative',['as'=>'getpopularnegative','uses'=>'mentionDataController@getpopularnegative']);
Route::get('getinteractiondetail',['as'=>'getinteractiondetail','uses'=>'mentionDataController@getinteractiondetail']);
Route::get('getinterestdata',['as'=>'getinterestdata','uses'=>'mentionDataController@getinterestdata']);
Route::get('getallmention',['as'=>'getallmention','uses'=>'mentionDataController@getallmention']);
Route::get('getallmentioncomment',['as'=>'getallmentioncomment','uses'=>'mentionDataController@getallmentioncomment']);
Route::get('getsentimentbycategory',['as'=>'getsentimentbycategory','uses'=>'mentionDataController@getsentimentbycategory']);
Route::get('getPagewithmentioncount',['as'=>'getPagewithmentioncount','uses'=>'mentionDataController@getPagewithmentioncount']);
Route::get('getPagewithWebsitementioncount',['as'=>'getPagewithWebsitementioncount','uses'=>'mentionDataController@getPagewithWebsitementioncount']);
Route::get('getnetpromotorscore',['as'=>'getnetpromotorscore','uses'=>'mentionDataController@getnetpromotorscore']);
Route::get('getInfluencerProfile',['as'=>'getInfluencerProfile','uses'=>'mentionDataController@getInfluencerProfile']);
Route::get('getInfluencerPage',['as'=>'getInfluencerPage','uses'=>'mentionDataController@getInfluencerPage']);
Route::get('getMentionStatus',['as'=>'getMentionStatus','uses'=>'mentionDataController@getMentionStatus']);
Route::get('getCampaignMentionStatus',['as'=>'getCampaignMentionStatus','uses'=>'CampaignController@getCampaignMentionStatus']);

Route::get('getMentionPost',['as'=>'getMentionPost','uses'=>'mentionDataController@getMentionPost']);
Route::get('getAllArticle',['as'=>'getAllArticle','uses'=>'mentionDataController@getAllArticle']);
Route::get('getAllArticle_forCampaign',['as'=>'getAllArticle_forCampaign','uses'=>'CampaignController@getAllArticle_forCampaign']);

Route::get('getRelatedMention',['as'=>'getRelatedMention','uses'=>'mentionDataController@getRelatedMention']);
Route::get('getRelatedArticle',['as'=>'getRelatedArticle','uses'=>'mentionDataController@getRelatedArticle']);
Route::get('getMentionComment',['as'=>'getMentionComment','uses'=>'mentionDataController@getMentionComment']);
Route::get('getAllKeywords',['as'=>'getAllKeywords','uses'=>'mentionDataController@getAllKeywords']);
Route::get('getKeywordsByGroup',['as'=>'getKeywordsByGroup','uses'=>'mentionDataController@getKeywordsByGroup']);
Route::get('getMentionSentiDetail',['as'=>'getMentionSentiDetail','uses'=>'mentionDataController@getMentionSentiDetail']);
Route::get('getCampaignSentiDetail',['as'=>'getCampaignSentiDetail','uses'=>'CampaignController@getCampaignSentiDetail']);
Route::get('getallmentioninsert',['as'=>'getallmentioninsert','uses'=>'ProjectController@insert_Posts_Data']);

Route::get('getReplyComments',['as'=>'getReplyComments','uses'=>'PageManageController@getReplyComments']);
Route::get('getDashboardSentimentGraph',['as'=>'getDashboardSentimentGraph','uses'=>'MongodbDataController@getDashboardSentimentGraph']);
Route::get('getDashboardMentionGraph',['as'=>'getDashboardMentionGraph','uses'=>'MongodbDataController@getDashboardMentionGraph']);
Route::get('gethighlighted',['as'=>'gethighlighted','uses'=>'MongodbDataController@gethighlighted']);


Route::get('getcomments', [
  'as'=>'getcomments',
  'uses' => 'mentionDataController@getcomments'
]);

Route::post('/ajax/edit/postbookmark', [
  'as'=>'postbookmark',
  'uses' => 'MongoCRUDController@UpdateBookMarkPost'
]);
Route::post('/ajax/edit/in_postbookmark', [
  'as'=>'in_postbookmark',
  'uses' => 'MongoCRUDController@UpdateBookMarkInboundPost'
]);

Route::post('/ajax/edit/post_comment_bookmark', [
  'as'=>'post_comment_bookmark',
  'uses' => 'MongoCRUDController@UpdateBookMarkComment'
]);
Route::post('/ajax/edit/inbound_comment_bookmark', [
  'as'=>'inbound_comment_bookmark',
  'uses' => 'MongoCRUDController@UpdateBookMarkInboundComment'
]);


Route::post('logout',['as' => 'logout','uses' => 'Auth\LoginController@logout']);


/*point out controller*/
Route::get('getSentimentMention',['as'=>'getSentimentMention','uses'=>'pointOutController@getSentimentMention']);
Route::get('getsentimentcomment',['as'=>'getsentimentcomment','uses'=>'pointOutController@getsentimentcomment']);
Route::get('getCategoryMention',['as'=>'getCategoryMention','uses'=>'pointOutController@getCategoryMention']);
Route::get('remove_keyword', [
  'as'   => 'remove_keyword',
  'uses' => 'pointOutController@remove_keyword'
]);

//insight
Route::get('getemotionRate',['as'=>'getemotionRate','uses'=>'InsightController@getemotionRate']);

Route::get('getcommentEmotionbyPostType',['as'=>'getcommentEmotionbyPostType','uses'=>'InsightController@getcommentEmotionbyPostType']);

Route::get('getInteractionByPostType',['as'=>'getInteractionByPostType','uses'=>'InsightController@getInteractionByPostType']);
//Campaign
Route::get('getCampaignPost',['as'=>'getCampaignPost','uses'=>'CampaignController@getCampaignPost']);
Route::get('getcampaigncomment',['as'=>'getcampaigncomment','uses'=>'CampaignController@getcampaigncomment']);
Route::get('getcampaignMention',['as'=>'getcampaignMention','uses'=>'CampaignController@getcampaignMention']);
Route::get('getcampaignMentionCmt',['as'=>'getcampaignMentionCmt','uses'=>'CampaignController@getcampaignMentionCmt']);
Route::get('getCampaignGroup',['as'=>'getCampaignGroup','uses'=>'CampaignController@getCampaignGroup']);
Route::get('getOtherCampaign',['as'=>'getOtherCampaign','uses'=>'CampaignController@getOtherCampaign']);
 //page management
Route::get('getRelatedcomment',['as'=>'getRelatedcomment','uses'=>'PageManageController@getRelatedcomment']);
Route::get('getInboundPost',['as'=>'getInboundPost','uses'=>'PageManageController@getInboundPost']);
Route::get('getTopAndLatestPost',['as'=>'getTopAndLatestPost','uses'=>'PageManageController@getTopAndLatestPost']);
Route::get('getTopComment',['as'=>'getTopComment','uses'=>'PageManageController@getTopComment']);
Route::get('getRelatedPosts',['as'=>'getRelatedPosts','uses'=>'PageManageController@getRelatedPosts']);
Route::get('getsentimentbypage',['as'=>'getsentimentbypage','uses'=>'PageManageController@getsentimentbypage']);
Route::get('relatedcomment', [
  'as'   => 'relatedcomment',
  'uses' => 'HomeController@relatedcomment'
]);
Route::get('relatedgroupcomment', [
  'as'   => 'relatedgroupcomment',
  'uses' => 'HomeController@relatedgroupcomment'
]);

Route::get('mentionPageList', [
  'as'   => 'mentionPageList',
  'uses' => 'HomeController@mentionPageList'
]);
Route::get('getMentionPageList', [
  'as'   => 'getMentionPageList',
  'uses' => 'mentionDataController@getMentionPageList'
]);
Route::get('setMentionPageTier_Mongo', [
  'as'   => 'setMentionPageTier_Mongo',
  'uses' => 'mentionDataController@setMentionPageTier_Mongo'
]);
Route::get('getRelatedCampaign_comment',['as'=>'getRelatedCampaign_comment','uses'=>'CampaignController@getRelatedCampaign_comment']);
Route::get('related_campaigncomment', [
  'as'   => 'related_campaigncomment',
  'uses' => 'HomeController@related_campaigncomment'
]);
Route::get('highlightedcomment_old', [
  'as'   => 'highlightedcomment_old_',
  'uses' => 'HomeController@highlightedcomment'
]);
Route::get('HighlightedComment', [
  'as'   => 'HighlightedComment',
  'uses' => 'CronjobController@HighlightedComment'
]);
Route::get('relatedcompetitorcomment', [
  'as'   => 'relatedcompetitorcomment',
  'uses' => 'HomeController@relatedcompetitorcomment'
]);

Route::get('relatedpost', [
  'as'   => 'relatedpost',
  'uses' => 'HomeController@relatedpost'
]);


Route::get('relatedmention', [
  'as'   => 'relatedmention',
  'uses' => 'HomeController@relatedmention'
]);
Route::get('relatedcampaignmention', [
  'as'   => 'relatedcampaignmention',
  'uses' => 'HomeController@relatedcampaignmention'
]);
Route::get('relatedmentioncmt', [
  'as'   => 'relatedmentioncmt',
  'uses' => 'HomeController@relatedmentioncmt'
]);
Route::get('relatedarticle', [
  'as'   => 'relatedarticle',
  'uses' => 'HomeController@relatedarticle'
]);



Route::post('setUpdatedPredict', [
  'as'=>'setUpdatedPredict',
  'uses' => 'PageManageController@Updatepredict'
]);

Route::post('SetHideMention', [
  'as'=>'SetHideMention',
  'uses' => 'mentionDataController@UpdateHideMention'
]);

Route::post('SetCampaign', [
  'as'=>'SetCampaign',
  'uses' => 'PageManageController@SetCampaign'
]);

Route::post('SetGroupPostCampaign', [
  'as'=>'SetGroupPostCampaign',
  'uses' => 'PageManageController@SetGroupPostCampaign'
]);

Route::post('SetCampaign_mention', [
  'as'=>'SetCampaign_mention',
  'uses' => 'PageManageController@SetCampaign_mention'
]);

Route::post('SetCampaign_article', [
  'as'=>'SetCampaign_article',
  'uses' => 'PageManageController@SetCampaign_article'
]);

Route::post('session-cmtID',[
  'as'=>'session-cmtID',
  'uses' => 'HomeController@setSessionCmtID'
]);


Route::post('setUpdatedPostPredict', [
  'as'=>'setUpdatedPostPredict',
  'uses' => 'PageManageController@UpdatedPostPredict'
]);

Route::post('setActionUpdate', [
  'as'=>'setActionUpdate',
  'uses' => 'PageManageController@ActionUpdate'
]);
//tags Group
Route::resource('tagsGroup', 'tagsGroupController');
Route::get('tagsGroup/edit/{id}', [
  'as'   => 'tagsGroup/edit',
  'uses' => 'tagsGroupController@edit'
]);
Route::post('tagstore', [
  'as'   => 'tagstore',
  'uses' => 'tagsGroupController@store'
]);
Route::get('tagsGroup/delete/{id}', [
  'as'   => 'tagsGroup/delete',
  'uses' => 'tagsGroupController@destroy'
]);
Route::get('tagGrouplist', [ 'as' => 'tagGrouplist', 'uses' => 'tagsGroupController@gettagGrouplist']);
Route::get('gettagBytagGroup',['as' =>'gettagBytagGroup','uses' =>'tagsGroupController@gettagBytagGroup']);
Route::get('getRemit',['as' =>'getRemit','uses' =>'BankController@getRemit']);
Route::get('get_acc2acc_transfer',['as' =>'get_acc2acc_transfer','uses' =>'BankController@get_acc2acc_transfer']);
Route::get('get_acc2agent_transfer',['as' =>'get_acc2agent_transfer','uses' =>'BankController@get_acc2agent_transfer']);

Route::get('get_agent2agent_transfer',['as' =>'get_agent2agent_transfer','uses' =>'BankController@get_agent2agent_transfer']);
Route::get('get_withdraw_reg',['as' =>'get_withdraw_reg','uses' =>'BankController@get_withdraw_reg']);
Route::get('get_withdraw_nonreg',['as' =>'get_withdraw_nonreg','uses' =>'BankController@get_withdraw_nonreg']);


//tags
Route::resource('tags', 'tagsController');
Route::resource('companyInfo', 'CompanyInfoController');
Route::get('getCompanyInfo', [ 'as' => 'getCompanyInfo', 'uses' => 'CompanyInfoController@getCompanyInfo']);
Route::get('gettaglist', [ 'as' => 'gettaglist', 'uses' => 'tagsController@gettaglist']);
Route::get('getwordlist', [ 'as' => 'getwordlist', 'uses' => 'WordCloudController@getwordlist']);
Route::get('tags/delete/{id}', [ 'as' => 'tags/delete', 'uses' => 'tagsController@destroy']);
Route::get('wordcloud/delete/{id}', [ 'as' => 'wordcloud/delete', 'uses' => 'WordCloudController@destroy']);

Route::get('/notify', 'PusherController@sendNotification');
Route::post('quick_tag', [
  'as'=>'quick_tag',
  'uses' => 'tagsController@quick_tag'
]);


Route::get('exchange_compare',['as'=>'exchange_compare','uses'=>'BankController@exchange_compare']);

Route::get('getDailyExchangeRates',['as'=>'getDailyExchangeRates','uses'=>'BankController@getDailyExchangeRates']);

//Inbound
Route::get('getInboundSentiDetail',['as'=>'getInboundSentiDetail','uses'=>'Inbound\inboundController@getInboundSentiDetail']);
Route::get('getInHighlighted',['as'=>'getInHighlighted','uses'=>'Inbound\inboundController@getHighlighted_New']);
Route::get('getHighlighted_frequent',['as'=>'getHighlighted_frequent','uses'=>'Inbound\inboundController@getHighlighted_frequent']);
Route::get('getKeyword',['as'=>'getKeyword','uses'=>'Inbound\inboundController@getKeyword']);
// Route::get('getHighlighted_New',['as'=>'getHighlighted_New','uses'=>'Inbound\inboundController@getHighlighted_New']);
Route::get('getinboundReaction',['as'=>'getinboundReaction','uses'=>'Inbound\inboundController@getinboundReaction']);
Route::get('getinboundPostReaction',['as'=>'getinboundpostReaction','uses'=>'Inbound\inboundController@getinboundPostReaction']);
Route::get('getinboundsentiment',['as'=>'getinboundsentiment','uses'=>'Inbound\inboundController@getInboundsentiment']);
Route::get('getInboundPointOut',['as'=>'getInboundPointOut','uses'=>'pointOutController@getInboundPointOut']);
Route::get('getInboundinterest',['as'=>'getInboundinterest','uses'=>'Inbound\inboundController@getInboundinterest']);
Route::get('getInboundlatestPOST', [ 'as' => 'getInboundlatestPOST', 'uses' => 'Inbound\inboundController@getInboundlatestPOST']);
Route::get('getInboundPostPoint',['as'=>'getInboundPostPoint','uses'=>'pointOutController@getInboundPostPoint']);
Route::get('getInboundcomments', [
  'as'=>'getInboundcomments',
  'uses' => 'PageManageController@getInboundcomments'
]);
Route::get('getTagSentiment',['as'=>'getTagSentiment','uses'=>'Inbound\inboundController@getTagSentiment']);
Route::get('getCampaignTagSentiment',['as'=>'getCampaignTagSentiment','uses'=>'Inbound\inboundController@getCampaignTagSentiment']);

Route::get('getMentionTierDetail',['as'=>'getMentionTierDetail','uses'=>'mentionDataController@getMentionTierDetail']);
Route::get('getMentionTier',['as'=>'getMentionTier','uses'=>'mentionDataController@getMentionTier']);
Route::get('getTagQty',['as'=>'getTagQty','uses'=>'Inbound\inboundController@getTagQty']);
Route::get('getPostTagQty',['as'=>'getPostTagQty','uses'=>'Inbound\inboundController@getPostTagQty']);
Route::get('getTagCount',['as'=>'getTagCount','uses'=>'Inbound\inboundController@getTagCount']);
Route::get('getPostTagCount',['as'=>'getPostTagCount','uses'=>'Inbound\inboundController@getPostTagCount']);
Route::get('getTopicCount',['as'=>'getTopicCount','uses'=>'Inbound\inboundController@getTopicCount']);

Route::get('getcmtTagCount',['as'=>'getcmtTagCount','uses'=>'Inbound\inboundController@getcmtTagCount']);

Route::get('getCampaignKwCount',['as'=>'getCampaignKwCount','uses'=>'CampaignController@getCampaignKwCount']);
Route::get('getCampaignTagCount',['as'=>'getCampaignTagCount','uses'=>'Inbound\inboundController@getCampaignTagCount']);
//insightcampaign_name
Route::get('getInteractionByInboundPostType',['as'=>'getInteractionByInboundPostType','uses'=>'Inbound\insightInboundController@getInteractionByInboundPostType']);
Route::get('getEmotionByInboundPostType',['as'=>'getEmotionByInboundPostType','uses'=>'Inbound\insightInboundController@getEmotionByInboundPostType']);
Route::get('getserviceAnalysis',['as'=>'getserviceAnalysis','uses'=>'Inbound\insightInboundController@getserviceAnalysis']);
Route::get('getAccessToken',['as'=>'getAccessToken','uses'=>'Inbound\inboundController@getAccssToken']);
Route::get('getPostingStatus',['as'=>'getPostingStatus','uses'=>'Inbound\inboundController@getPostingStatus']);
Route::get('getEngagementStatus',['as'=>'getEngagementStatus','uses'=>'Inbound\inboundController@getEngagementStatus']);
Route::get('getPageSummary',['as'=>'getPageSummary','uses'=>'Inbound\inboundController@getPageSummary']);

//testing
Route::get('sync_mongodata',['as'=>'sync_mongodata','uses'=>'TestController@Sync_MongoData']);

//facebook Insight
Route::get('getFbReach',['as'=>'getFbReach','uses'=>'Inbound\FBInsightController@getFbReach']);
Route::get('getFbReachCompare',['as'=>'getFbReachCompare','uses'=>'Inbound\FBInsightController@getFbReachCompare']);
Route::get('getcityReach',['as'=>'getcityReach','uses'=>'Inbound\FBInsightController@getCityReach']);
Route::get('get-agegender-Reach',['as'=>'get-agegender-Reach','uses'=>'Inbound\FBInsightController@GetageGenderReach']);
Route::get('getPageFan',['as'=>'getPageFan','uses'=>'Inbound\FBInsightController@GetPageFan']);
Route::get('getPageLike',['as'=>'getPageLike','uses'=>'Inbound\FBInsightController@GetPageLike']);
Route::get('getPageFanDif',['as'=>'getPageFanDif','uses'=>'Inbound\FBInsightController@GetPageFanDif']);
Route::get('getcityFan',['as'=>'getcityFan','uses'=>'Inbound\FBInsightController@getCityFan']);
Route::get('get-agegender-Fan',['as'=>'get-agegender-Fan','uses'=>'Inbound\FBInsightController@GetageGenderFan']);


//Report
Route::get('rpt-sentipredict',['as'=>'rpt-sentipredict','uses'=>'HomeController@RptSentiPredict']);
Route::get('rpt-humanpredict',['as'=>'rpt-humanpredict','uses'=>'HomeController@RptHumanPredict']);
Route::get('rpt-postDetail-export',['as'=>'rpt-postDetail-export','uses'=>'HomeController@RptPostExport']);
Route::get('comparisonpdf', ['as'   => 'comparisonpdf','uses' => 'HomeController@comparisonpdf']);
Route::get('pagespdf', ['as'   => 'pagespdf','uses' => 'HomeController@pagespdf']);

Route::get('rpt-analysis',['as'=>'rpt-analysis','uses'=>'HomeController@RptAnalysis']);
Route::get('rpt-pagesAnalysis',['as'=>'rpt-pagesAnalysis','uses'=>'HomeController@RptPagesAnalysis']);
Route::get('getPostExcelData',['as'=>'getPostExcelData','uses'=>'ReportController@getPostExcelData']);
Route::get('senti-predict-condition',['as'=>'senti-predict-condition','uses'=>'ReportController@SentiPredictCondition']);
Route::get('human-predict-list',['as'=>'human-predict-list','uses'=>'ReportController@HumanPredictRecord']);
Route::get('comparison_pdf',['as'=>'comparison_pdf','uses'=>'ReportController@comparison_pdf']);
});



//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});
 
Route::get('page_delete',['as'=>'pagedelete','uses'=>'ProjectController@page_delete']);

Route::get('edit-picture', [ 'as' => 'edit-picture', 'uses' => 'HomeController@showPicture']);
Route::post('edit-picture', 'HomeController@storePicture');
<?php

return [

    // These CSS rules will be applied after the regular template CSS

    
        'css' => [
            // '.button-content .button { background: red }',
   '.btn-rounded {
    border-radius: 60px;
    padding: 7px 18px;
    background: #fff;
}'
        ],
    

    'colors' => [

        'highlight' => '#004ca3',
        'button'    => '#004cad',

    ],

    'view' => [
        'senderName'  => null,
        'reminder'    => null,
        'unsubscribe' => null,
        'address'     => null,

        'logo'        => [
            'path'   => '%PUBLIC%/assets/images/kbz.jpg',
            'width'  => '',
            'height' => '',
        ],

        'twitter'  => null,
        'facebook' => null,
        'flickr'   => null,
    ],

];

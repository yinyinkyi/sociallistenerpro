<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold' => $rootDir . '/lib/fonts/ZapfDingbats',
    'italic' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold_italic' => $rootDir . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '/lib/fonts/Symbol',
    'bold' => $rootDir . '/lib/fonts/Symbol',
    'italic' => $rootDir . '/lib/fonts/Symbol',
    'bold_italic' => $rootDir . '/lib/fonts/Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => $rootDir . '/lib/fonts/DejaVuSerif',
  ),
  'font awesome 5 brands' => array(
    'normal' => $fontDir . '/9203478b65caa19aa7537322ecad38ce',
  ),
  'font awesome 5 free' => array(
    'normal' => $fontDir . '/c649a7e7909ac92c3ec87309660f9f20',
  ),
  'simple-line-icons' => array(
    'normal' => $fontDir . '/287b7786c2e90a7bf96d1057bafb4d19',
  ),
  'weathericons' => array(
    'normal' => $fontDir . '/7f9e7e791ae882fb974bf3a9a868a562',
  ),
  'linea-arrows-10' => array(
    'normal' => $fontDir . '/81fa4cd2614cbe26875ff207f6b633a7',
  ),
  'linea-basic-10' => array(
    'normal' => $fontDir . '/7f98fe8269dc52752fe5954141bfd708',
  ),
  'linea-basic-elaboration-10' => array(
    'normal' => $fontDir . '/5b3138b3387fc310bda6619a9f814892',
  ),
  'linea-ecommerce-10' => array(
    'normal' => $fontDir . '/ecb59d0714b198f888423eeb4541c7d7',
  ),
  'linea-music-10' => array(
    'normal' => $fontDir . '/4f06f70d3c0ea5c6434ec414a8e1e2da',
  ),
  'linea-software-10' => array(
    'normal' => $fontDir . '/198fb4609ac1a7f217e19a225772f33a',
  ),
  'linea-weather-10' => array(
    'normal' => $fontDir . '/e256b0f2684eab406048117596c5dcf4',
  ),
  'themify' => array(
    'normal' => $fontDir . '/cb1c133c564caedf48a4468c41c89298',
  ),
  'material design icons' => array(
    'normal' => $fontDir . '/9b00144b41e55fb2187502b0dc19c1f9',
  ),
  'mm3' => array(
    'normal' => $fontDir . '/6945c77486599aec80fc091c1ac8f4b1',
  ),
) ?>
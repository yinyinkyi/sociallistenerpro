<?php


namespace BIMiner;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class MongoPages extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'pages';

}
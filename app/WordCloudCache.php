<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class WordCloudCache extends Model
{
    //
      protected $table = 'wordCloudCache';
  protected $dates = ['from_date','to_date','created_at','updated_at'];
    protected $fillable = [
        'from_date','to_date','json_data','brand_id','page_name'
    ];
}

<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;


class Project extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'projects';
	protected $dates = ['created_at'];
    protected $fillable = [
        'id','name','new_mentions','created_at','user_id','monitor_pages','own_pages','default_page','pages_name'
    ];

    public function keywords()
    {
        return $this->hasMany('BIMiner\ProjectKeyword');
    }

}

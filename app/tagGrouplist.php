<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class tagGrouplist extends Model
{
  protected $table = 'tagGrouplists';
  protected $dates = ['created_at','updated_at'];
  protected $fillable = ['id','tagGroup_id','tag_id'];
}

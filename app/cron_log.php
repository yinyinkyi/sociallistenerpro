<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class cron_log extends Model
{
     //
      protected $table = 'cron_log';
  protected $dates = ['created_at','updated_at'];
    protected $fillable = [
        'id','brand_id','finish_status','cron_type','keyword_group','going_on'
    ];
}

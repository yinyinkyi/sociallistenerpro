<?php

namespace BIMiner\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $inputs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
         // $this->inputs = $inputs;
         $this->today_date = $inputs[0];
         $this->page_growth = $inputs[1];
         $this->post_count = $inputs[2];
         $this->comment_count = $inputs[3];

         $this->cmtTotal = $inputs[4];
         $this->postTotal = $inputs[5];
         $this->articleTotal = $inputs[6];
         $this->mentionTotal = $inputs[7];

         $this->pTagName1 = $inputs[8];
         $this->pTagCount1 = $inputs[9];
         $this->pTagName2 =$inputs[10];
         $this->pTagCount2 =$inputs[11];
         $this->pTagName3 = $inputs[12];
         $this->pTagCount3 = $inputs[13];
         $this->pTagName4 = $inputs[14];
         $this->pTagCount4 = $inputs[15];
         $this->pTagName5 = $inputs[16];
         $this->pTagCount5 = $inputs[17];

         $this->cTagName1 = $inputs[18];
         $this->cTagCount1 = $inputs[19];
         $this->cTagName2 =$inputs[20];
         $this->cTagCount2 =$inputs[21];
         $this->cTagName3 = $inputs[22];
         $this->cTagCount3 = $inputs[23];
         $this->cTagName4 = $inputs[24];
         $this->cTagCount4 = $inputs[25];
         $this->cTagName5 = $inputs[26];
         $this->cTagCount5 = $inputs[27];

         $this->post_pos_pcent = $inputs[28];
         $this->post_neg_pcent = $inputs[29];
         $this->post_neutral_pcent = $inputs[30];

         $this->cmt_pos_pcent = $inputs[31];
         $this->cmt_neg_pcent = $inputs[32];
         $this->cmt_neutral_pcent = $inputs[33];
         $this->reportType = $inputs[34];
         $this->to_date = $inputs[35];
         $this->from_date = $inputs[36];
         $this->imgurl = $inputs[37];




     
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('intelligence@baganit.com')
                ->subject('Bagan Intelligence Miner - Your weekly report is ready!')
                ->bcc(['steven@baganit.com','winmoe.eric@baganit.com','pyaephyo34@gmail.com'])
                ->view('mail')
                ->with([
                  'today_date' =>  $this->today_date,
                  'page_growth'=>  $this->page_growth,
                  'post_count' =>  $this->post_count,
                  'comment_count' => $this->comment_count,

                  'cmtTotal' => $this->cmtTotal,
                  'postTotal'=> $this->postTotal,
                  'articleTotal'=>$this->articleTotal,
                  'mentionTotal' => $this->mentionTotal,

                  'pTagName1' =>  $this->pTagName1,
                  'pTagCount1' =>  $this->pTagCount1,
                  'pTagName2' =>  $this->pTagName2,
                  'pTagCount2' =>  $this->pTagCount2,
                  'pTagName3' =>  $this->pTagName3,
                  'pTagCount3' =>  $this->pTagCount3,
                  'pTagName4' =>  $this->pTagName4,
                  'pTagCount4' =>  $this->pTagCount4,
                  'pTagName5' =>  $this->pTagName5,
                  'pTagCount5' =>  $this->pTagCount5,

                  'cTagName1' =>  $this->cTagName1,
                  'cTagCount1' =>  $this->cTagCount1,
                  'cTagName2' =>  $this->cTagName2,
                  'cTagCount2' =>  $this->cTagCount2,
                  'cTagName3' =>  $this->cTagName3,
                  'cTagCount3' =>  $this->cTagCount3,
                  'cTagName4' =>  $this->cTagName4,
                  'cTagCount4' =>  $this->cTagCount4,
                  'cTagName5' =>  $this->cTagName5,
                  'cTagCount5' =>  $this->cTagCount5,
                  
                  'post_pos_pcent' =>  $this->post_pos_pcent,
                  'post_neg_pcent' =>  $this->post_neg_pcent,
                  'post_neutral_pcent' =>  $this->post_neutral_pcent,

                  'cmt_pos_pcent' =>  $this->cmt_pos_pcent,
                  'cmt_neg_pcent' =>  $this->cmt_neg_pcent,
                  'cmt_neutral_pcent' =>  $this->cmt_neutral_pcent,
                  'reportType' => $this->reportType,
                  'to_date' => $this->to_date,
                  'from_date' => $this->from_date,
                  'imgurl' => $this->imgurl,





                
                ]);
    }
}

<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class ManageDB extends Model
{
    protected $table = 'manage_dbs';
	protected $dates = ['created_at'];
    protected $fillable = [
        'id','user_id','db_name','created_at','brand_id'
    ];
}

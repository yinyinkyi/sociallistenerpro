<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
     //
      protected $table = 'company_info';
  protected $dates = ['created_at','updated_at'];
    protected $fillable = [
        'id','name','phone','monitor_pages','address','logo_path','brand_id','created_at','updated_at'
    ];
}

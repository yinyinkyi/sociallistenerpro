<?php

namespace BIMiner\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use BIMiner\Http\Controllers\stdClass;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use BIMiner\MongoProfile;
use DB;
use MongoDB;
use BIMiner\Comment;
use BIMiner\Project;
use BIMiner\tags;
use BIMiner\User;
use BIMiner\cron_log;
use BIMiner\MongodbData;
use BIMiner\MongoPages;

class sync_mention extends Command
{
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync_mention_1year:sync_mention_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Mention Data for one year for the fist time ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $projects = DB::table('cron_log')->select('*')->where('finish_status',0)->where('cron_type','sync_mention')->get();
        
      foreach($projects as $project)
      {
        $id = $project->brand_id;
      
        $result =$this->Insert_Mention_Data($id);
        if($result)
          {
             DB::table('cron_log')
            ->where('brand_id', $id)
            ->update(['finish_status' => 1]);
          }
       
      }
      $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
       $date_time = $date->format('dmYHis');
       $this->info('Sync Mention for one year Successfully.'. $date_time);
    }
    public function Insert_Mention_Data($project_id)
    {
      //for edit
     $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];

     
     
     $keyword_data = $this->getprojectkeyword($project_id);
     $filter['$or'] = $this->getkeywordfilter($keyword_data);
// $this->info($filter);
     $pagename = $this->getpagename($project_id);//this filter will use later to filter (to avoid including data from monitor pages)
     $pagename= explode(',', $pagename[0]["monitor_pages"]);
     $pagename= $this->Format_Page_Name($pagename);
    
    // $page['$or']= $this->getpagefilter($other_pages);
 
    /* dd($page);*/
     $today = date('d-m-Y h:i:s');//22-09-2018//  date('d-m-Y')

    $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
    $date_Begin=date('Y-m-d', strtotime('-1 year', strtotime($today_plus)));
    // dd($date_Begin);
    //start with today (00:00:00) hour
    $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
   
    $date_End=date('Y-m-d h:i:s',strtotime($today));
    $date_End=date('Y-m-d', strtotime('+1 day', strtotime($date_End)));
    //start with tomorrow (00:00:00) hour
    $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);

 $mention_post_id_array = $this->get_table_id($project_id,"posts","id");
 
 $mention_post_result=MongodbData::raw(function ($collection) use($filter,$mention_post_id_array,$dateBegin,$dateEnd) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             [ 'id'=>[ '$nin'=> $mention_post_id_array ] ],
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
              $filter
                      ]
        ]  
                   
       ],
       ['$sort' =>['created_time'=>-1]],
       // ['$limit' =>2000]
     
        
      ]);
})->toArray();

// dd($mention_post_result);

// return $mention_post_result;


$data=[];
 $page_array=[];
 $data_message=[];

 foreach ($mention_post_result as  $key => $row) {
                $id ='';$datetime=NULL;
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';$original_message='';$unicode = '';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                $isDeleted=0;
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                    if(isset($row['original_message'])) $original_message = $row['original_message'];

                    if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['created_time']))
                    {
                    $utcdatetime = $row["created_time"];
                    $datetime = $utcdatetime->toDateTime();
                    $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                    $datetime = $datetime->format('Y-m-d H:i:s');
                 }
             
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['mark'])) 
                {
                  if($row['mark'] == 'deleted')
                {
                   $isDeleted =1;
                }
                 
                }

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'original_message' => $original_message,
                    'unicode' => $unicode,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>'',
                    'checked_sentiment' =>'',
                    'decided_keyword' =>'',
                    'emotion' =>'',
                    'checked_emotion' =>'',
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'isDeleted'=>$isDeleted,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

  }
  $path = storage_path('app/data_output/mention_posts'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data);

 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,original_message,unicode,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,created_time,change_predict,checked_predict,isBookMark,isDeleted,created_at,updated_at)";


 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);


 $post_id = $this->getMentionPostId($project_id);/*dd( $filter);*/
// dd($post_id);
 
$mention_cmt_id_array = $this->get_table_id($project_id,"comments","id");
  /*dd($inbound_cmt_id_array);*/
 $mention_comment_result=Comment::raw(function ($collection) use($filter,$mention_cmt_id_array,$post_id,$dateBegin,$dateEnd) {//print_r($filter);
 
    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             // ['post_id'=>['$in'=>$post_id]],
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             ['id'=>[ '$nin'=> $mention_cmt_id_array ] ],
             $filter
           
            
                      ]
        ]  
                   
       ],
       ['$sort' =>['created_time'=>-1]],
       
        
    ]);
})->toArray();

 $profile_id=[];
  $data_comment=[];
 foreach ($mention_comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';$profile='';
                $comment_count =0;$original_message='';$unicode='';
                $sentiment='';$emotion='';$parent='';
                $tags='';
                if(isset($row['profile']))
                {
                $profile = $row['profile'];
                $profile_id[]=new MongoDB\BSON\ObjectId((string)$profile);
                } 

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['original_message'])) $original_message = $row['original_message'];
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['parent'])) $parent =  $row['parent'];
           

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'wb_message' =>'',
                    'original_message' =>$original_message,
                    'unicode' =>$unicode,
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    'sentiment' =>'',
                    'checked_sentiment' =>'',
                    'decided_keyword' =>'',
                    'emotion' =>'',
                    'checked_emotion' =>'',
                    'interest' =>'',
                    'profile'=>$profile,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'parent'=>$parent,
                    'isBookMark' =>0,
                    'manual_date'=>'NULL',
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }

$path = storage_path('app/data_output/mention_comments'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_comment);
 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,interest,profile,change_predict,checked_predict,parent,isBookMark,manual_date,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);
 return true;



 

}
//local function
public function getprojectkeyword($brand_id)
    {
   $keyword_data = ProjectKeyword::select('main_keyword','require_keyword','exclude_keyword')->where('project_id','=',$brand_id)->get()->toArray();
   return $keyword_data;
    }

 public function getkeywordfilter($keyword_data)
{
     $conditional=[];
         foreach ($keyword_data as  $key => $row) {
         
            
                       //  $request['main_keyword']       =    '.*' . $row['main_keyword'];
                         //$request['require_keyword']       =    $row['require_keyword'];
     $main_keyword_filter =[ 'message' => new MongoDB\BSON\Regex(".*" . $row['main_keyword'],'i' )];
      
       
                         $require_keyword = explode(",", $row['require_keyword']);
                          $conditional_require_or=[];
                         $require_keyword_filter=[];
                            foreach($require_keyword as $i =>$element)
                            {
                               if(!empty($element))
                                {
                                 $require_keyword_filter[] = [ 'message' => new MongoDB\BSON\Regex(".*". $element,'i'  )];
                            $conditional_require_or['$or']=$require_keyword_filter;
                              }
                           
                            }
                         
                            
                          $exclude_keyword = explode(",", $row['exclude_keyword']);
                          $conditional_exclude_or=[];
                          $exclude_keyword_filter=[];
                        
                            foreach($exclude_keyword as $i =>$element)
                            {
                                if(!empty($element))
                                {
                                               

                            $exclude_keyword_filter[] = [ 'message' => ['$not'=>new MongoDB\BSON\Regex(".*". $element,'i'  )]];
                            $conditional_exclude_or['$or']=$exclude_keyword_filter;
                                }
                            }
                      

                             $conditional_and['$and'] =[$main_keyword_filter];

                            if(!empty($conditional_require_or))
                            {
                                $conditional_and['$and']=[$conditional_and,$conditional_require_or];
                            }
                           
                                               
                            if(!empty($conditional_exclude_or))
                            {
                               
                               $conditional_and['$and']=[$conditional_and,$conditional_exclude_or];
                            }
                           
                          //   $conditional_and['$and'] =[$main_keyword_filter,$conditional_require_or,$conditional_exclude_or];

                        
                        $conditional[] =$conditional_and;
                       
         }

return  $conditional;

}
public function getpagename($brand_id)
    {
     $page_list = Project::select('monitor_pages')->where('id','=',$brand_id)->get()->toArray();
      return $page_list;
    }

    public function Format_Page_Name($array)
    {
      $new_array=[];
       foreach ($array as  $key => $row) {
        $arr_page_name=explode("-",$row);
        if(count($arr_page_name)>0)
        {
          $last_index=$arr_page_name[count($arr_page_name)-1];
          if(is_numeric($last_index))
          $new_array[] = $last_index;
          else
          $new_array[] = $row;
        }
        else
        {
          $new_array[]=$row;
        }
       
          
      
       }

      return  $new_array;
    }

      public function getMentionPostId($brand_id)
    {
        $id_query="select id from temp_".$brand_id."_posts";
        $id_result = DB::select($id_query);
         $id_array = array_column($id_result, 'id');
          return  $id_array;
    }

  public function get_table_id($project_id,$table,$require_var,$related_pages=[])
    {
     
      $filter_pages=$this->getPageWhere($related_pages);
      
      $query = "SELECT ".$require_var." from temp_".$project_id."_".$table ."  Where 1=1 " . $filter_pages;
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }
    public function getPageWhereGen($related_pages)
{
    $pages='';
     $filter_pages = '';
     $related_pages = $this->Format_Page_Name($related_pages);
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  posts.page_name in (".$pages.")";
      }
    return  $filter_pages;
}

public function getPageWhere($related_pages)
{
    $pages='';
     $filter_pages = '';
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  AND page_name in (".$pages.")";
      }
    return  $filter_pages;
}
   function doCSV($path, $array)
{
   $fp = fopen($path, 'w');
$i = 0;
foreach ($array as $fields) {
    if($i == 0){
        fputcsv($fp, array_keys($fields));
    }
    fputcsv($fp, array_values($fields));
    $i++;
}

fclose($fp);
}

    function convertKtoThousand($s)
{
    if (strpos(strtoupper($s), "K") != false) {
    $s = rtrim($s, "kK");
    return floatval($s) * 1000;
  } else if (strpos(strtoupper($s), "M") != false) {
    $s = rtrim($s, "mM");
    return floatval($s) * 1000000;
  } else {
    return floatval($s);
  }
}
  
}

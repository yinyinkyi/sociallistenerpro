<?php

namespace BIMiner\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       // 'BIMiner\Console\Commands\cronWordbreak_post',
       // 'BIMiner\Console\Commands\cronWordbreak_comment',
       // 'BIMiner\Console\Commands\cronPosts',
       // 'BIMiner\Console\Commands\cronComments',
       // 'BIMiner\Console\Commands\cronTag_comment',

        // 'BIMiner\Console\Commands\cronWordbreak_inboundPost',
        // 'BIMiner\Console\Commands\cronWordbreak_inboundComment',
        // 'BIMiner\Console\Commands\cronInboundPosts',
        // 'BIMiner\Console\Commands\cronInboundComments'
      
        // 'BIMiner\Console\Commands\cronTag_inboundComment'
        // 'BIMiner\Console\Commands\cronGettingTag'
        
    ];
		// protected $commands = [
		//         Commands\MakeView::class
		//     ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $schedule->call('BIMiner\Http\Controllers\CronjobController@HighlightedComment')->cron('0 12 * * *')->runInBackground();

        // if (Schema::hasTable('tasks')) {
        //         // Get all tasks from the database
        //         $tasks = Task::all();
        //         // Go through each task to dynamically set them up.
        //     foreach ($tasks as $task) {
        //          // Use the scheduler to add the task at its desired frequency
        //         $schedule->call(function() use ($task) {
        //             // Place your logic here
        //         })->cron($task->frequency);
        //         }
        //     }

        // $schedule->call('BIMiner\Http\Controllers\CronjobController@dailyMail')->cron('0 9 * * *')->runInBackground();
        $schedule->call('BIMiner\Http\Controllers\CronjobController@weeklyMail')->cron('0 9 * * 2')->runInBackground(); 
        // 0 for sunday ,1 for monday , 2 for tuesday ...
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

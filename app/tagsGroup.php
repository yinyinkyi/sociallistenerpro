<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class tagsGroup extends Model
{
  protected $table = 'tagsGroup';
  protected $dates = ['created_at'];
  protected $fillable = ['id','name','brand_id'];
}

<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class PageRename extends Model
{
    //
    protected $table = 'page_rename';

    protected $fillable = [
        'id','origin_name','custom_name','brand_id','created_at','updated_at'
    ];
}

<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class WordCloudDict extends Model
{
    //
    protected $table = 'word_dict';
	protected $dates = ['created_at','updated_at'];
    protected $fillable = ['id','brand_id','word','created_at','updated_at'];
}

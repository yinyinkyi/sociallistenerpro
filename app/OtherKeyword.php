<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class OtherKeyword extends Model
{
    //

        protected $fillable = [
        'id','project_id','main_keyword','require_keyword','exclude_keyword','group_name','company_id'
    ];
    protected $table='other_keywords';
}

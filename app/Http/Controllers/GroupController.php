<?php

namespace BIMiner\Http\Controllers;

use BIMiner\MongodbData;
use BIMiner\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use Yajra\Datatables\Datatables;
use BIMiner\Project;
use BIMiner\InboundPages;

class GroupController extends Controller
{
	use GlobalController;
    public function index(){
  		if($user=Auth::user())
        {
            $login_user = auth()->user()->id;
            $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
            foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

            $permission_data = $this->getPermission();
            $source ='in';
            $title="Brand";
            $project_data = $this->getProject();

            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
    
	        if(count($project_data)>0)
	      	{
	        
		      $project_name = $project_data[0]['name']; 
		      $brand_id= $project_data[0]['id'];
		      $ownpage=$project_data[0]['monitor_pages'];
		      $ownpage=explode(',', $ownpage);
		      $count = $this->getProjectCount($project_data);
          $companyData=$this->getCompanyData($brand_id);
	      	  return view('groupPostView',compact('pid','source','accountPermission','project_data','count','title','permission_data','tags','page_arr','companyData','brand_id'));

	        }
	        else
	        {
	          return \Redirect::route('brandList',['source'=>$source,'action_permission' => $action_permission]);
	        }
        }
        else
        {
        	$title="";
            return view('auth.login',compact('title'));
        }

    }
    public function getGroupPost(){

        if(null !== Input::get('fday'))
        {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
      	else
      	{
         	$dateBegin=date('Y-m-d');
      	}

        if(null !==Input::get('sday'))
        {
            $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
      
        else
        {
            $dateEnd=date('Y-m-d');

        }
        $date_filter="  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";

   		$add_global_filter=' 1=1 ';
      	if(null !== Input::get('tsearch_senti'))
      	{
        	$add_global_filter .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
      	}
       	if(null !== Input::get('tsearch_tag'))
      	{
        	$add_global_filter .=" AND " . $this->getTagWhereOrGen(Input::get('tsearch_tag'),'posts');
      	}
	   	if(null !== Input::get('group_name'))
      	{
        	$add_global_filter .=" AND posts.group_name In('".Input::get('group_name')."')";
      	}
      
        $brand_id = Input::get('brand_id');
        $brand_id = 40;

        $tags =$this->gettagsBybrandid($brand_id);

        $query = "SELECT posts.group_name group_name,posts.message message,IF(posts.checked_tags_id is null,posts.tags_id,posts.checked_tags_id) ". 
                 " tags,posts.tags_id tags_id,posts.checked_tags_id checked_tags_id,posts.sentiment senti,posts.checked_sentiment csenti,posts.temp_id,link,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
                  " IF(posts.checked_sentiment is null,posts.sentiment,posts.checked_sentiment) sentiment,posts.emotion,posts.campaign FROM temp_group_posts posts WHERE " .$add_global_filter. " AND " .$date_filter. " ORDER by timestamp(posts.created_time) DESC";
        $data= DB::select($query);
        $data_mention = [];
        $hp_both = '';
        $hp_tags = '';
        $hp = '';
        foreach($data as $key => $value) {
           // dd($value->tags_id,$value->checked_tags_id);
        	// dd($value->temp_id);
        	if($value->tags_id !== $value->checked_tags_id && $value->senti !== $value->csenti){
        		$hp_both = 'human predict!';
        	}
            elseif($value->tags_id == $value->checked_tags_id){
            	$hp_tags = '';
            }
            elseif($value->tags_id !== $value->checked_tags_id){
            	$hp_tags = 'human predict!';
            }
            elseif($value->senti == $value->csenti) {
            	$hp = '';
            }
            elseif($value->senti !== $value->csenti) {
            	$hp = 'human predict!';
            }
        	$data_mention[] = array(
        		'hp_tags' => $hp_tags,
        		'hp' => $hp,
        		'hp_both' => $hp_both,
        		'group_name' => $value->group_name,
        		'message' => $value->message,
        		'tags' => $value->tags,
        		'temp_id' => $value->temp_id,
        		'link' => $value->link,
        		'created_time' =>$value->created_time,
        		'sentiment' => $value->sentiment,
        		'campaign' =>$value->campaign,
        		'emotion' =>$value->emotion
        		
        	);

        }
     
		// dd($data_mention);
        return Datatables::of($data_mention)
               
                 ->addColumn('post', function ($data_mention) use($tags){
                 
                  // $fb_p_id=explode('_', $data_mention['id']);
                  // $fb_p_id=$fb_p_id[1];
                  // $page_id='';
                  // $arr_page_id=explode("-",$data_mention['page_name']);
                  // if(count($arr_page_id)>0)
                  // {
                  //   $last_index=$arr_page_id[count($arr_page_id)-1];
                  //   if(is_numeric($last_index))
                  //   $page_id = $last_index;
                  //   else
                  //   $page_id = $data_mention['page_name'];
                  // }
                  // else
                  // {
                  //   $page_id=$data_mention['page_name'];
                  // }
                 	// dd($data_mention);

            	// $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
                 	$post_Link = $data_mention['link'];
                // $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
              //   if($data_mention['page_mongo_id'] <> '')
              //   {  $page_photo='https://graph.facebook.com/'.$data_mention['page_mongo_id'].'/picture?type=large';}
              // else
              //   { 
                	$page_photo='assets/images/unknown_photo.jpg';
                // }

              //     if($data_mention['imgurl'] <> '')
              //   {  $page_photo=$data_mention['imgurl'];}
              // else
              //   { $page_photo='assets/images/unknown_photo.jpg'; }
            
              
              

                        // $total = (int) $data_mention['positive'] +(int) $data_mention['negative']+(int) $data_mention['cmtneutral'];
                        // $pos_pcent='';$neg_pcent='';$neutral_pcent='';
                       
                         $html= '<div class="profiletimeline"><div class="sl-item">
                                              <div class="sl-left"> <img src="'.$page_photo.'" alt="user" class="img-circle  img-bordered-sm"> </div> ';
                         $html .= ' <div class="sl-right"> <div><div><a href="'.$post_Link.'" class="link"  target="_blank">'.$data_mention['group_name'].' </a>';
                        // $html.='  <label class="text-info"   for="campaign" style=""><i class="mdi mdi-flag-outline"></i></label> 
                        //            ';
                        if((int)$data_mention['campaign'] == 0)
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['temp_id'].'" aria-pressed="false">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                        else
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['temp_id'].'" aria-pressed="true">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                    //                      if($data->hp <> '' && $data->hp_tags <>'')
                    // $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    // else if($data->hp <> '')
                    // $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                     if($data_mention['hp_both'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';

                     if($data_mention['hp_tags'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                 	if($data_mention['hp'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                        // if((int)$data_mention['isDeleted']==1)
                        // {
                        //  $html.='  <span  class="text-red">   This post is no longer available on FB. </span>';
                        // }
                        $html.='<div class="form-material sl-right">
                        <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data_mention['created_time'].'</span> </div>';
                      //    $html.='<div style="width:55%;display: inline-block">';
                      //    if((int) $data_mention['negative'] > 0)
                      //  { 
                      //   $neg_pcent=round((int) $data_mention['negative']/$total*100,2);
                      //   $html.='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'.$neg_pcent.'%';
                      // }
                      // if((int) $data_mention['positive'] > 0)
                      //   {
                      //     $pos_pcent=round((int) $data_mention['positive']/$total*100,2);
                      //     $html.='<span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'.$pos_pcent.'%';
                      //   }
                      // if((int) $data_mention['cmtneutral'] > 0)
                      //  {
                      //   $neutral_pcent=round((int) $data_mention['cmtneutral']/$total*100,2);
                      //  $html.='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'.$neutral_pcent.'%';
                      //  }
                                 
                      //   $html.='</div>';
                        // $html.='</div>';
                         $html.='<p>Sentiment : <select id="sentiment_'.$data_mention['temp_id'].'"  class="form-material form-control in-comment-select edit_senti ';
                     if($data_mention['sentiment']=="")$html.='"><option value="" selected="selected" ></option>';
                     if($data_mention['sentiment']=="pos")$html.='text-success" >';
                     if($data_mention['sentiment']=="neg")$html.='text-red" >';
                     if($data_mention['sentiment']=="neutral" || $data_mention['sentiment'] == "NA")$html.='text-warning" >';
                  
                      $html.='<option value="pos" class="text-success"';
                      if($data_mention['sentiment'] == "pos")
                      $html.= 'selected="selected"';
                      $html.= '>Positive</option><option value="neg" class="text-red"';
                      if($data_mention['sentiment'] == "neg")
                      $html.= 'selected="selected"';
                      $html.= '>Negative</option><option value="neutral" class="text-warning"';
                      if($data_mention['sentiment'] == "neutral" || $data_mention['sentiment'] == "NA")
                      $html.= 'selected="selected"';      
                      $html.= '>Neutral</option></select>';
                        // if($data_mention['visitor'] <> 1){
                         $html.=' |  Tag :<select id="tags_'.$data_mention['temp_id'].'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                        
                       $tag_array=explode(',', $data_mention['tags']);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';
                    // }
                        $html.='<p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['temp_id'].'">View More</a></p>';
                                  
                        $html.= '</div></div>';
                                   
                       $html.='<div class="sl-right" align="right"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green see_comment"  id="seeComment_'.$data_mention['temp_id'].'" >See Comment</a> <a href="'.$post_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See Post on FB</a></div>';
                        $html .='</div>';

                          return $html;
                         

                      })
            // ->addColumn('reaction', function ($data_mention) {
            //               return  $data_mention['total'];
            //           }) 

            // ->addColumn('comment', function ($data_mention) {
            //                return "<span style='font-weight:bold'>" . $data_mention['comment_count']."</span>";
            //           }) 
            // ->addColumn('share', function ($data_mention) {
            //               return  "<span style='font-weight:bold'>" . $data_mention['shared']."</span>";
            //           }) 
            // ->addColumn('overall', function ($data_mention) {
            //                if((int)$data_mention['positive'] >  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-success" style="font-weight:500">Positive</span>';
            //             }
            //             else   if((int)$data_mention['positive'] <  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-red" style="font-weight:500">Negative</span>';
            //             }
            //                else   if((int)$data_mention['positive'] ==  0 && (int)$data_mention['negative'] == 0 && (int)$data_mention['cmtneutral']  == 0 )
            //             {
            //               $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==0 &&  (int)$data_mention['negative'] ==0 &&  (int)$data_mention['cmtneutral'] <> 0)
            //             {
            //               $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'] &&  (int)$data_mention['cmtneutral'] <> 0)
            //             {
            //               $html='<span class="text-success" style="font-weight:500">Positive</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-red" style="font-weight:500">Negative</span>';
            //             }
                     
            //             return $html;
            //           }) 

                  
                // ->rawColumns(['post','overall','reaction','comment','share'])
                ->rawColumns(['post'])
             
                ->make(true);
    	
    }

        public function getGroupComment(){

        if(null !== Input::get('fday'))
        {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
        else
        {
          $dateBegin=date('Y-m-d');
        }

        if(null !==Input::get('sday'))
        {
            $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
      
        else
        {
            $dateEnd=date('Y-m-d');

        }
        $post_con = '';
        $date_filter = '';
        if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
        {
          $date_filter = ' 1=1 ';
        }
        else
        {
          $date_filter="  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        }
        if(null !== Input::get('post_id')){
          $post_con = " and post_id ='".Input::get('post_id')."'";
        }
      $add_global_filter=' 1=1 ';
        if(null !== Input::get('tsearch_senti'))
        {
          $add_global_filter .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
        }
        if(null !== Input::get('tsearch_tag'))
        {
          $add_global_filter .=" AND " . $this->getTagWhereOrGen(Input::get('tsearch_tag'),'cmts');
        }
      if(null !== Input::get('group_name'))
        {
          $add_global_filter .=" AND posts.group_name In('".Input::get('group_name')."')";
        }
      
        $brand_id = Input::get('brand_id');
        $brand_id = 40;

        $tags =$this->gettagsBybrandid($brand_id);

        $query = "SELECT posts.group_name group_name,cmts.message message,IF(cmts.checked_tags_id is null,cmts.tags_id,cmts.checked_tags_id) ". 
                 " tags,cmts.tags_id tags_id,cmts.checked_tags_id checked_tags_id,cmts.sentiment senti,cmts.checked_sentiment csenti,cmts.temp_id,cmts.link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
                  " IF(cmts.checked_sentiment is null,cmts.sentiment,cmts.checked_sentiment) sentiment,cmts.emotion,cmts.campaign FROM temp_group_comments cmts LEFT JOIN temp_group_posts posts ON posts.temp_id=cmts.post_id WHERE " .$add_global_filter. $post_con . " AND ". $date_filter ." ORDER by timestamp(posts.created_time) DESC";
        $data= DB::select($query);
        $data_mention = [];
        $hp_both = '';
        $hp_tags = '';
        $hp = '';
        foreach($data as $key => $value) {
           // dd($value->tags_id,$value->checked_tags_id);
          // dd($value->temp_id);
          if($value->tags_id !== $value->checked_tags_id && $value->senti !== $value->csenti){
            $hp_both = 'human predict!';
          }
            elseif($value->tags_id == $value->checked_tags_id){
              $hp_tags = '';
            }
            elseif($value->tags_id !== $value->checked_tags_id){
              $hp_tags = 'human predict!';
            }
            elseif($value->senti == $value->csenti) {
              $hp = '';
            }
            elseif($value->senti !== $value->csenti) {
              $hp = 'human predict!';
            }
          $data_mention[] = array(
            'hp_tags' => $hp_tags,
            'hp' => $hp,
            'hp_both' => $hp_both,
            'group_name' => $value->group_name,
            'message' => $value->message,
            'tags' => $value->tags,
            'temp_id' => $value->temp_id,
            'link' => $value->link,
            'created_time' =>$value->created_time,
            'sentiment' => $value->sentiment,
            'campaign' =>$value->campaign,
            'emotion' =>$value->emotion
            
          );

        }
     
    // dd($data_mention);
        return Datatables::of($data_mention)
               
                 ->addColumn('post', function ($data_mention) use($tags){
                 
                  // $fb_p_id=explode('_', $data_mention['id']);
                  // $fb_p_id=$fb_p_id[1];
                  // $page_id='';
                  // $arr_page_id=explode("-",$data_mention['page_name']);
                  // if(count($arr_page_id)>0)
                  // {
                  //   $last_index=$arr_page_id[count($arr_page_id)-1];
                  //   if(is_numeric($last_index))
                  //   $page_id = $last_index;
                  //   else
                  //   $page_id = $data_mention['page_name'];
                  // }
                  // else
                  // {
                  //   $page_id=$data_mention['page_name'];
                  // }
                  // dd($data_mention);

              // $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
                  $post_Link = $data_mention['link'];
                // $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
              //   if($data_mention['page_mongo_id'] <> '')
              //   {  $page_photo='https://graph.facebook.com/'.$data_mention['page_mongo_id'].'/picture?type=large';}
              // else
              //   { 
                  $page_photo='assets/images/unknown_photo.jpg';
                // }

              //     if($data_mention['imgurl'] <> '')
              //   {  $page_photo=$data_mention['imgurl'];}
              // else
              //   { $page_photo='assets/images/unknown_photo.jpg'; }
            
              
              

                        // $total = (int) $data_mention['positive'] +(int) $data_mention['negative']+(int) $data_mention['cmtneutral'];
                        // $pos_pcent='';$neg_pcent='';$neutral_pcent='';
                       
                         $html= '<div class="profiletimeline"><div class="sl-item">
                                              <div class="sl-left"> <img src="'.$page_photo.'" alt="user" class="img-circle  img-bordered-sm"> </div> ';
                         $html .= ' <div class="sl-right"> <div><div><a href="'.$post_Link.'" class="link"  target="_blank">'.$data_mention['group_name'].' </a>';
                        // $html.='  <label class="text-info"   for="campaign" style=""><i class="mdi mdi-flag-outline"></i></label> 
                        //            ';
                        if((int)$data_mention['campaign'] == 0)
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['temp_id'].'" aria-pressed="false">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                        else
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['temp_id'].'" aria-pressed="true">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                    //                      if($data->hp <> '' && $data->hp_tags <>'')
                    // $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    // else if($data->hp <> '')
                    // $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                     if($data_mention['hp_both'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';

                     if($data_mention['hp_tags'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                  if($data_mention['hp'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                        // if((int)$data_mention['isDeleted']==1)
                        // {
                        //  $html.='  <span  class="text-red">   This post is no longer available on FB. </span>';
                        // }
                        $html.='<div class="form-material sl-right">
                        <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data_mention['created_time'].'</span> </div>';
                      //    $html.='<div style="width:55%;display: inline-block">';
                      //    if((int) $data_mention['negative'] > 0)
                      //  { 
                      //   $neg_pcent=round((int) $data_mention['negative']/$total*100,2);
                      //   $html.='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'.$neg_pcent.'%';
                      // }
                      // if((int) $data_mention['positive'] > 0)
                      //   {
                      //     $pos_pcent=round((int) $data_mention['positive']/$total*100,2);
                      //     $html.='<span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'.$pos_pcent.'%';
                      //   }
                      // if((int) $data_mention['cmtneutral'] > 0)
                      //  {
                      //   $neutral_pcent=round((int) $data_mention['cmtneutral']/$total*100,2);
                      //  $html.='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'.$neutral_pcent.'%';
                      //  }
                                 
                      //   $html.='</div>';
                        // $html.='</div>';
                         $html.='<p>Sentiment : <select id="sentiment_'.$data_mention['temp_id'].'"  class="form-material form-control in-comment-select edit_senti ';
                     if($data_mention['sentiment']=="")$html.='"><option value="" selected="selected" ></option>';
                     if($data_mention['sentiment']=="pos")$html.='text-success" >';
                     if($data_mention['sentiment']=="neg")$html.='text-red" >';
                     if($data_mention['sentiment']=="neutral" || $data_mention['sentiment'] == "NA")$html.='text-warning" >';
                  
                      $html.='<option value="pos" class="text-success"';
                      if($data_mention['sentiment'] == "pos")
                      $html.= 'selected="selected"';
                      $html.= '>Positive</option><option value="neg" class="text-red"';
                      if($data_mention['sentiment'] == "neg")
                      $html.= 'selected="selected"';
                      $html.= '>Negative</option><option value="neutral" class="text-warning"';
                      if($data_mention['sentiment'] == "neutral" || $data_mention['sentiment'] == "NA")
                      $html.= 'selected="selected"';      
                      $html.= '>Neutral</option></select>';
                        // if($data_mention['visitor'] <> 1){
                         $html.=' |  Tag :<select id="tags_'.$data_mention['temp_id'].'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                        
                       $tag_array=explode(',', $data_mention['tags']);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';
                    // }
                        $html.='<p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'</p>';
                                  
                        $html.= '</div></div>';
                                   
                       $html.='<div class="sl-right" align="right">  <a href="'.$post_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See  on FB</a></div>';
                        $html .='</div>';

                          return $html;
                         

                      })
            // ->addColumn('reaction', function ($data_mention) {
            //               return  $data_mention['total'];
            //           }) 

            // ->addColumn('comment', function ($data_mention) {
            //                return "<span style='font-weight:bold'>" . $data_mention['comment_count']."</span>";
            //           }) 
            // ->addColumn('share', function ($data_mention) {
            //               return  "<span style='font-weight:bold'>" . $data_mention['shared']."</span>";
            //           }) 
            // ->addColumn('overall', function ($data_mention) {
            //                if((int)$data_mention['positive'] >  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-success" style="font-weight:500">Positive</span>';
            //             }
            //             else   if((int)$data_mention['positive'] <  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-red" style="font-weight:500">Negative</span>';
            //             }
            //                else   if((int)$data_mention['positive'] ==  0 && (int)$data_mention['negative'] == 0 && (int)$data_mention['cmtneutral']  == 0 )
            //             {
            //               $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==0 &&  (int)$data_mention['negative'] ==0 &&  (int)$data_mention['cmtneutral'] <> 0)
            //             {
            //               $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'] &&  (int)$data_mention['cmtneutral'] <> 0)
            //             {
            //               $html='<span class="text-success" style="font-weight:500">Positive</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-red" style="font-weight:500">Negative</span>';
            //             }
                     
            //             return $html;
            //           }) 

                  
                // ->rawColumns(['post','overall','reaction','comment','share'])
                ->rawColumns(['post'])
             
                ->make(true);
      
    }
    public function getRelatedGroupPosts(){
    	$post_id=Input::get('id');
        $brand_id=Input::get('brand_id');

        $permission_data = $this->getPermission();
        $edit_permission=$permission_data['edit'];
		// ANY_VALUE(posts.page_name) page_name,
    	$query="SELECT  posts.group_name,posts.temp_id,1 as edit_permission,ANY_VALUE(posts.message) message,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ". " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.local_picture) local_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion ". " from temp_group_posts posts  ". "  where posts.temp_id='".$post_id."'   GROUP BY id";
        // dd($query);
        $data = DB::select($query);
        echo json_encode($data);
    }
}

<?php

namespace BIMiner\Http\Controllers;

use BIMiner\MongodbData;
use BIMiner\MongoPages;
use BIMiner\MongoFollowers;
use BIMiner\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use BIMiner\Project;
use Yajra\Datatables\Datatables;
class mentionDataController extends Controller
{
   use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRelatedMention()
    {
      
      $post_id=Input::get('id');
      $brand_id=Input::get('brand_id');

       $permission_data = $this->getPermission();
       $edit_permission=$permission_data['edit'];

      $query="SELECT  posts.id,1 as edit_permission,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
      " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.full_picture) full_picture, ANY_VALUE(posts.local_picture) local_picture, ANY_VALUE(posts.type) post_type,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion, sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA,".
      " ANY_VALUE(posts.isDeleted) isDeleted from temp_posts posts  LEFT JOIN temp_comments cmts on posts.id=cmts.post_id".
      "  where posts.id='".$post_id."'  and cmts.parent='' GROUP BY id";

              $data = DB::select($query);
      //if no data above query we need to just show post data without comment data , in order to get post data remove cmts.parent''
        if (count($data) <=0 )
        {
          $query="SELECT  posts.id,1 as edit_permission,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
          " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.full_picture) full_picture, ANY_VALUE(posts.local_picture) local_picture, ANY_VALUE(posts.type) post_type,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion, sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA, ".
          " ANY_VALUE(posts.isDeleted) isDeleted from temp_posts posts  LEFT JOIN temp_comments cmts on posts.id=cmts.post_id".
          "  where posts.id='".$post_id."'  GROUP BY id";
          $data = DB::select($query);
        }
        // dd($data);
         if(null !== Input::get('keyword_group'))
        {
          $keyword_group = Input::get('keyword_group') ;
        }
        else
        {
          $keyword_group = '' ;
        }
        $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);
        $keyForHighLight=$this->getKeywordforHighLight($keyword_data);
        if(null !== Input::get('page_name'))
        {
          $page_name = Input::get('page_name') ;
        }
        else
        {
          $page_name = '' ;
        }
        // $keyword_campaign = $this->getcampaignkeyword($brand_id,$page_name);
         $keyword_campaign='';
         if(null !== Input::get('campaign_name'))
          {
            $keyword_campaign= $this->getCampaignKeywordFilter(Input::get('campaign_name'),$brand_id);
          }
          // dd(Input::get('campaign_name'));
        echo json_encode(array($data,$keyForHighLight, $keyword_campaign ));
    }
    public function getMentionPost()
    {
       $senti_con = '';
       if(null !== Input::get('sentiType'))
      {
        $senti_con = " and posts.checked_sentiment='".Input::get('sentiType')."'";
      }
       $tag_con = '';
       if(null !== Input::get('search_tag'))
      {
        $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
      }
      // $tier_con = '';
      //  if(null !== Input::get('tier'))
      // {
      //   $tier_con = " and posts.checked_tags_id Like '%".Input::get('tier')."%'";
      // }

      $brand_id=Input::get('brand_id');
 
        if(null !== Input::get('fday'))
      {
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
        $dateBegin=date('Y-m-d');
      }
    

      if(null !==Input::get('sday'))
      {

        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      
      else
      {
        $dateEnd=date('Y-m-d');
      }


      $add_inbound_con ="";
      $inboundpages=$this->getInboundPages($brand_id);

    
      if ($inboundpages !== '')
      {
        $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
      }

      $excludepages = $this->getExcludePages($brand_id);
      if($excludepages != ""){
        $add_inbound_con .=" and (posts.page_name  not in (".$excludepages.") or posts.page_name is NULL)";
      }
       $period_type = '';
      $additional_filter=" AND 1=1 ";
      
        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

            
          if($period_type == 'week')
          {
           $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

          }
          else
          {
              $additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
          }

       }

        if(null !== Input::get('tsearch_senti'))
        {
         $additional_filter .= ' AND ' . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
        }
          // $keyword_data = $this->getprojectkeyword($brand_id);
      
        // if(isset($keyword_data [0]['main_keyword']))

        if(null !== Input::get('keyword_group'))
      {
        $keyword_group = Input::get('keyword_group') ;
      }
      else
      {
        $keyword_group = '' ;
      }
         $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);

         if(isset($keyword_data [0]['main_keyword']))
         $additional_filter  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 
        else
         $additional_filter .= ' AND 1=2';
    
 
        if(null !== Input::get('tsearch_keyword'))
      {
       $additional_filter .= ' AND ' .$this->getKeywordWhereGen(Input::get('tsearch_keyword'),'posts');
      }
       if(null !== Input::get('page_name'))
      {
       $additional_filter .= ' AND posts.page_name="' .Input::get('page_name') . '" ';
      }

      $query='';
      $query_result=[];
      $tier_con = '';
      if(null !==Input::get('tier'))
      {
        $tier_filter_pages = MongoPages::where('page_tier',Input::get('tier'))->where('watch','on')->get()->toArray();
        foreach ($tier_filter_pages as $key => $value) {
          # code...
          $page[] = $value['page_name'];
        }
        $page_string = implode(',', $page);
     
        $result_string = "'" . str_replace(",", "','", $page_string) . "'";
           // dd($result_string);
        $tier_con = " AND posts.page_name IN(".$result_string.")";
      }
      // dd($tier_con);
      if(Input::get('keyword_group') == "KBZPay"){

        $query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,replace(shared, ',', '') shared,'".$brand_id."'brand_id,IF(posts.sentiment = '','~',posts.sentiment) old_senti,IF(posts.sentiment = posts.checked_sentiment,'','human predict!') hp,IF(posts.checked_sentiment <> '',posts.checked_sentiment,posts.sentiment) sentiment,posts.checked_tags_id tags,IF(posts.tags_id <> posts.checked_tags_id || posts.checked_tags_id <> '','human predict!','') hp_tags, posts.decided_keyword,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
        " created_time,posts.link,posts.full_picture,posts.type post_type,posts.emotion,posts.isDeleted,cmts.checked_sentiment ".
        " cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.id cmt_id,cmts.isHide cmt_isHide,cmts.checked_predict,posts.campaign, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
        " ELSE 'mdi-star' ".
        " END) isBookMark ".
        "FROM temp_posts_kbzpay posts LEFT JOIN temp_comments_kbzpay cmts on posts.id=cmts.post_id ".
        " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $additional_filter . $senti_con . $tier_con .
        " ORDER by timestamp(posts.created_time) DESC ";

    }else{

      $query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,replace(shared, ',', '') shared,'".$brand_id."'brand_id,IF(posts.sentiment = '','~',posts.sentiment) old_senti,IF(posts.sentiment = posts.checked_sentiment,'','human predict!') hp,IF(posts.checked_sentiment <> '',posts.checked_sentiment,posts.sentiment) sentiment,posts.checked_tags_id tags,IF(posts.tags_id <> posts.checked_tags_id || posts.checked_tags_id <> '','human predict!','') hp_tags, posts.decided_keyword,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
      " created_time,posts.link,posts.full_picture,posts.type post_type,posts.emotion,posts.isDeleted,cmts.checked_sentiment ".
      " cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.id cmt_id,cmts.isHide cmt_isHide,cmts.checked_predict,posts.campaign, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
      " ELSE 'mdi-star' ".
      " END) isBookMark ".
      "FROM temp_posts posts LEFT JOIN temp_comments cmts on posts.id=cmts.post_id ".
      " WHERE posts.isHide=0 and  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con . $additional_filter . $senti_con . $tag_con . $tier_con .
      " ORDER by timestamp(posts.created_time) DESC ";

    }

      
      //dd($query);
      
      $query_result = DB::select($query);


      $data = [];
       $data_mention=[];


       foreach ($query_result as  $key => $row) {
        //change page name from id to orginal user register name.
        
      $comment_count=0;
       
      $pos=($row->cmt_sentiment=='pos' && $row->parent=== '' )?1:0 ;
      $neg=($row->cmt_sentiment=='neg' && $row->parent=== '' )?1:0;
      $cmtneutral=($row->cmt_sentiment=='neutral' && $row->parent=== '' )?1:0;
      $cmtNA=($row->cmt_sentiment=='NA' && $row->parent=== '' )?1:0;
      $cmtneutral=(int)$cmtneutral+(int) $cmtNA;
      $anger =($row->cmt_emotion=='anger' && $row->parent==='' )?1:0 ;
      $interest=($row->cmt_emotion=='interest' && $row->parent==='' )?1:0;
      $disgust=($row->cmt_emotion=='disgust' && $row->parent ==='')?1:0;
      $fear=($row->cmt_emotion=='fear' && $row->parent=== '' )?1:0;
      $joy=($row->cmt_emotion=='joy' && $row->parent==='')?1:0;
      $like=($row->cmt_emotion=='like' && $row->parent==='' )?1:0;
      $love=($row->cmt_emotion=='love' && $row->parent==='' )?1:0;
      $neutral=($row->cmt_emotion=='neutral' && $row->parent==='' )?1:0;
      $sadness=($row->cmt_emotion=='sadness' && $row->parent==='' )?1:0;
      $surprise=($row->cmt_emotion=='surprise' && $row->parent==='' )?1:0;
      $trust=($row->cmt_emotion=='trust' && $row->parent==='')?1:0;
       
         
         $message=$this->readmore($row->message);

        $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
        $message = $this->highLightText($keyword_data,$message);

         $total = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry;
         //$message=$row->message;
                  if (isset($row ->id))
         {
            // $comment_count=($row->parent==='')?1:0;
               if (isset($row ->cmt_id) && $row ->cmt_isHide==0 )   $comment_count=1;


                       $id= $row ->id;
         
          if (!array_key_exists($id , $data)) {
          $data[$id] = array(
                      'id'     =>$row->id,
                      'message' => $message,
                      'page_name' =>$row->page_name,
                      'created_time' =>$row->created_time,
                      'link' =>$row->link,
                      'full_picture' =>$row->full_picture,
                      'sentiment' =>$row->sentiment,
                      'hp' => $row->hp,
                      'hp_tags' =>$row->hp_tags,
                      'tags' => $row->tags,
                      'old_senti' => $row->old_senti,
                      'emotion' =>$row->emotion,
                      'brand_id' => $row->brand_id,
                      'decided_keyword' => $row->decided_keyword,
                      'positive'=> $pos,'negative'=>$neg,'cmtneutral'=>$cmtneutral,
                      'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,'shared'=>$row->shared,
                      'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                      'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                      'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                      'total'=>$total,
                      'comment_count'=>$comment_count,
                      'post_type'=>$row->post_type,
                      'isDeleted'=>$row->isDeleted ,
                      'isBookMark'=>$row->isBookMark,
                      'campaign'=>$row->campaign
                  );
      }
      else
      {
       $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
       $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
       $data[$id]['cmtneutral'] = (int) $data[$id]['cmtneutral'] +$cmtneutral;
       $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
       $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
       $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
       $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
       $data[$id]['like'] = (int) $data[$id]['like'] + $like;
       $data[$id]['love'] = (int) $data[$id]['love'] + $love;
       $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
       $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
       $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
       $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;
       $data[$id]['comment_count'] = (int) $data[$id]['comment_count'] + $comment_count;

      }
      }
                  }

             if(null !== Input::get('filter_overall'))
            {
              if( Input::get('filter_overall') === 'pos')//
              {     foreach ($data as $key => $value) {

        if( (int)$value['positive'] <  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'])
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
                }
              else if( Input::get('filter_overall') === 'neg')
              {
           foreach ($data as $key => $value) {

            if((int)$value['positive'] >  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'] &&  (int)$value['cmtneutral'] <> 0)
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }
               else if( Input::get('filter_overall') === 'neutral')
              {
                          foreach ($data as $key => $value) {

                       if( ! ( ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 ) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0)))
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }

            }

            $data_mention=$data;
            $tags =$this->gettagsBybrandid($brand_id);
// dd($tags);

            return Datatables::of($data_mention)
     
             ->addColumn('post', function ($data_mention) use($tags){
              $fb_p_id=explode('_', $data_mention['id']);
              $fb_p_id=$fb_p_id[1];
              $page_id='';
              $arr_page_id=explode("-",$data_mention['page_name']);
              if(count($arr_page_id)>0)
              {
                $last_index=$arr_page_id[count($arr_page_id)-1];
                if(is_numeric($last_index))
                $page_id = $last_index;
                else
                $page_id = $data_mention['page_name'];
              }
              else
              {
                $page_id=$data_mention['page_name'];
              }

            $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
            $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
           

                    $total = (int) $data_mention['positive'] +(int) $data_mention['negative']+(int) $data_mention['cmtneutral'];
                    $pos_pcent='';$neg_pcent='';$neutral_pcent='';
                   
                     $html= '<div class="profiletimeline"><div class="sl-item">
                                          <div class="sl-left"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                          <div class="form-material sl-right"> <div> <div> <a href="'.$page_Link.'" class="link"  target="_blank">'.$data_mention['page_name'].' </a>';                    
                    if($data_mention['hp'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input Sentiment" data-toggle="tooltip"></i></a>';

                    $html.='<span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data_mention['created_time'].'</span> </div>';

                    // $html.= '<p>Sentiment : <select class="form-control sentiment-color in-comment-select" id="sentiment_'.$data_mention['id'].'"  >';
                    //         if($data_mention['sentiment'] == "")
                    //         $html.= '<option value="" selected="selected"></option>';
                    //         $html.='<option value="pos"';
                    //         if($data_mention['sentiment'] == "pos")
                    //         $html.= 'selected="selected"';
                    //         $html.= '>pos</option><option value="neg"';
                    //         if($data_mention['sentiment'] == "neg")
                    //         $html.= 'selected="selected"';
                    //         $html.= '>neg</option><option value="neutral"';
                    //         if($data_mention['sentiment'] == "neutral")
                    //         $html.= 'selected="selected"';      
                    //         $html.= '>neutral</option>';
                            
                    //         $html.='</select>';

 $html.='<p>Sentiment : <select id="sentiment_'.$data_mention['id'].'" style="width:50%;margin-right: 15px;" class="form-material form-control in-comment-select edit_senti ';
                   if($data_mention['sentiment']=="")$html.='"><option value="" selected="selected" ></option>';
                   if($data_mention['sentiment']=="pos")$html.='text-success" >';
                   if($data_mention['sentiment']=="neg")$html.='text-red" >';
                   if($data_mention['sentiment']=="neutral" || $data_mention['sentiment'] == "NA")$html.='text-warning" >';
                
                    $html.='<option value="pos" class="text-success"';
                    if($data_mention['sentiment'] == "pos")
                    $html.= 'selected="selected"';
                    $html.= '>Positive</option><option value="neg" class="text-red"';
                    if($data_mention['sentiment'] == "neg")
                    $html.= 'selected="selected"';
                    $html.= '>Negative</option><option value="neutral" class="text-warning"';
                    if($data_mention['sentiment'] == "neutral" || $data_mention['sentiment'] == "NA")
                    $html.= 'selected="selected"';      
                    $html.= '>Neutral</option></select>';

                    

                            // $html.= '<a class="edit_predict_post" id="'.$data_mention['id'].'" href="javascript:void(0)">
                            // <span class="mdi mdi-content-save" style="font-size:30px;"></span></a></p>';
                    if((int)$data_mention['campaign'] == 0)
                    $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="false">
                                        <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                        <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                    </button>';
                    else
                    $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="true">
                                        <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                        <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                    </button>';    
                    $html.='<div style="display:none"  align="center" style="vertical-align: top;" id="spin_'.$data_mention['id'].'"> <img src="assets/images/ajax-loader.gif" id="loader"></div>';
                     $html.=' <p> Tag :<select id="tags_'.$data_mention['id'].'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                       $tag_array=explode(',', $data_mention['tags']);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';
                    // if((int)$data_mention['isDeleted']==1)
                    // {
                    //  $html.='  <span  class="text-red">   This post is no longer available on FB. </span>';
                    // }
                 
                             
                      $html.='<p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['id'].'">View More</a></p>';
                              
                    $html.= '</div></div>
                               
                    <div class="sl-right" style="color:#0092ff">';
                   
                    if((int)$data_mention['liked'] > 0)
                    $html.='👍 '.$this->thousandsCurrencyFormat($data_mention['liked']);
                   if((int)$data_mention['loved'] > 0)
                    $html.=' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']);
                   if((int)$data_mention['haha'] > 0)
                    $html.=' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']);
                   if((int)$data_mention['wow'] > 0)
                    $html.=' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']);
                   if((int)$data_mention['sad'] > 0)
                    $html.=' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']);
                   if((int)$data_mention['angry'] > 0)
                    $html.=' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']);
                   if((int)$data_mention['comment_count'] > 0)
                    $html.='<a href="javascript:void(0)"  class="see_comment" id="seeComment_'.$data_mention['id'].'"  style="float:right"><span class="postPanel_actionIconContainer" id="idb5d"><span class="postPanel_icon mdi mdi-comment-outline" title="Comments"></span><span>'.$data_mention['comment_count'].'</span></span></a>';
                    $html.=' </div><div class="sl-right" align="right"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-gray post_hide"  id="hide_'.$data_mention['id'].'" >Hide Post</a> <a href="'.$post_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See Post on FB</a></div></div> </div>';
                      return $html;
                  })
        ->addColumn('reaction', function ($data_mention) {
                      return "<span style='font-weight:bold'>" . number_format($data_mention['total'])."</span>";
                  }) 
        ->addColumn('share', function ($data_mention) {
                      return  "<span style='font-weight:bold'>" . number_format($data_mention['shared'])."</span>";
                  }) 
            ->rawColumns(['post','reaction','share'])
            ->make(true);
    }
    public function getAllArticle(Request $request)
    {
       $senti_con = '';
       if(null !== Input::get('sentiType'))
      {
        $senti_con = " and posts.checked_sentiment='".Input::get('sentiType')."'";
      }
       $tag_con = '';
       if(null !== Input::get('search_tag'))
      {
        $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
      }


      $brand_id=Input::get('brand_id');
      if(null !== Input::get('fday'))
      {
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
        $dateBegin=date('Y-m-d');
      }

      if(null !==Input::get('sday'))
      {
        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      else
      {
        $dateEnd=date('Y-m-d');
      }

      $add_inbound_con ="";
      $inboundpages=$this->getInboundPages($brand_id);
      if($inboundpages !== '')
      {
        $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
      }
      $excludepages = $this->getExcludePages($brand_id);
      if($excludepages != ""){
        $add_inbound_con .=" and (posts.page_name  not in (".$excludepages.") or posts.page_name is NULL)";
      }

      $period_type = '';
      $additional_filter=" AND 1=1 ";
      if(null !==Input::get('period'))
      {
         $period=Input::get('period');
         $char_count = substr_count($period,"-");
         $format_type = '';
         if($char_count == 1)
         {
            $format_type ='%Y-%m';
         }
         else if($char_count == 2)
         {
            $format_type ='%Y-%m-%d';
         }   
          else if($char_count > 2)
          {
            $pieces = explode(" - ", $period);
            $format_type ='%Y-%m-%d';
            $period_type ='week';
          }
          if($period_type == 'week')
          {
            $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

          }
          else
          {
            $additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
          }
       }
          if(null !== Input::get('tsearch_senti'))
          {
           $additional_filter .= ' AND ' . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
          }
            // $keyword_data = $this->getprojectkeyword($brand_id);
        
          // if(isset($keyword_data [0]['main_keyword']))

          if(null !== Input::get('keyword_group'))
          {
            $keyword_group = Input::get('keyword_group') ;
          }
          else
          {
            $keyword_group = '' ;
          }
         $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);
         if(isset($keyword_data [0]['main_keyword']))
         $additional_filter  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 

        else
         $additional_filter .= ' AND 1=2';
    
 
          if(null !== Input::get('tsearch_keyword'))
          {
            $additional_filter .= ' AND ' .$this->getKeywordWhereGen(Input::get('tsearch_keyword'),'posts');
          }
         if(null !== Input::get('page_name'))
         {
            $additional_filter .= ' AND posts.page_name="' .Input::get('page_name') . '" ';
         }
        $query='';
        $query_result=[];

       $tags =$this->gettagsBybrandid($brand_id);

        // dd($add_inbound_con);
     $tier_con = '';
      if(null !==Input::get('tier'))
      {
        $tier_filter_pages = MongoPages::where('page_tier',Input::get('tier'))->where('watch','on')->get()->toArray();
        foreach ($tier_filter_pages as $key => $value) {
          # code...
          $page[] = $value['page_name'];
        }
        $page_string = implode(',', $page);
     
        $result_string = "'" . str_replace(",", "','", $page_string) . "'";
           // dd($result_string);
        $tier_con = " AND posts.page_name IN(".$result_string.")";
      }

      if( Input::get('keyword_group') == "KBZPay"){

        $query = "SELECT '".$brand_id."' brand_id,IF(posts.sentiment = '','~',posts.sentiment) old_senti,IF(posts.sentiment = posts.checked_sentiment,'','human predict!') hp,posts.campaign,IF(
                posts.checked_sentiment <> '',posts.checked_sentiment,posts.sentiment) sentiment,posts.id,posts.title, posts.link,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, 
                '%d-%m-%Y %H:%i%p') created_time,posts.checked_tags_id tags,IF(posts.tags_id = posts.checked_tags_id || posts.checked_tags_id = '','','human predict!') hp_tags 
                FROM temp_web_mentions_kbzpay posts  WHERE (posts.isHide = 0 or posts.isHide is null) AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". " ORDER by timestamp(posts.created_time) DESC ";

      }else{

        $query = "SELECT '".$brand_id."' brand_id,IF(posts.sentiment = '','~',posts.sentiment) old_senti,IF(posts.sentiment = posts.checked_sentiment,'','human predict!') hp,posts.campaign,IF(posts.checked_sentiment <> '',posts.checked_sentiment,posts.sentiment) sentiment,posts.id,posts.title,posts.link,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %H:%i %p') created_time,posts.checked_tags_id tags,IF(posts.tags_id = posts.checked_tags_id || posts.checked_tags_id = '','','human predict!') hp_tags FROM temp_web_mentions posts  WHERE (posts.isHide = 0 or posts.isHide is null) AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".  $additional_filter .  $senti_con . $tag_con . $tier_con . " ORDER by timestamp(posts.created_time) DESC ";

      }
      
        $query_result = DB::select($query);

         // dd($query_result);
        $data = [];
        $data_mention=[];


       foreach ($query_result as  $key => $row) {
        //change page name from id to orginal user register name.
        // dd($row->hp_tags);
        $message=$this->readmore($row->message,2000);
        // dd($keyword_data); 
        $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
        $message = $this->highLightText($keyword_data,$message);
        // dd($message);
        $data[] = array(
            'id'     =>$row->id,
            'title' => $row->title,
            'message' => $message,
            'page_name' =>$row->page_name,
            'hp_tags' =>$row->hp_tags,
            'link' => $row->link,
            'created_time' =>$row->created_time,
            'sentiment' =>$row->sentiment,
            'hp' => $row->hp,
            'tags' => $row->tags,
            'old_senti' => $row->old_senti,
            'brand_id' => $row->brand_id,
            'campaign'=> $row->campaign
          );
        }

             if(null !== Input::get('filter_overall'))
            {
              if( Input::get('filter_overall') === 'pos')//
              {     foreach ($data as $key => $value) {

        if( (int)$value['positive'] <  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'])
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
                }
              else if( Input::get('filter_overall') === 'neg')
              {
           foreach ($data as $key => $value) {

            if((int)$value['positive'] >  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'] &&  (int)$value['cmtneutral'] <> 0)
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }
               else if( Input::get('filter_overall') === 'neutral')
              {
                          foreach ($data as $key => $value) {

                       if( ! ( ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 ) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0)))
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }

            }

            $data_mention=$data;
           // dd($data_mention['title']);
            return Datatables::of($data_mention)
     
             ->addColumn('post', function ($data_mention) use($tags){
              $fb_p_id=explode('_', $data_mention['id']);
              $fb_p_id=$fb_p_id[1];
              $page_id='';
              $arr_page_id=explode("-",$data_mention['page_name']);
              if(count($arr_page_id)>0)
              {
                $last_index=$arr_page_id[count($arr_page_id)-1];
                if(is_numeric($last_index))
                $page_id = $last_index;
                else
                $page_id = $data_mention['page_name'];
              }
              else
              {
                $page_id=$data_mention['page_name'];
              }

            $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
            $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
            $article_Link = $data_mention['link'];
            // if(! isset($article_Link)) $article_Link = '#';      
            // <div class="profiletimeline"><div class="sl-item">
            // <div class="sl-left"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div> 

            //dd($data_mention['created_time']);

            $html= '<div class="form-material sl-right"> <a href="'.$article_Link.'" style="font-size:18px;font-weight:800px;" target="_blank">'. $data_mention['title'] .'</a>';
                    // $html.='<span style="float:right" class="sl-date">'.$data_mention['created_time'].' </span> </div>';

            if((int)$data_mention['campaign'] == 0)
            $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="false" style="margin-left: 10px;">
                                <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                            </button>';
            else
            $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="true" style="margin-left: 10px;">
                                <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                            </button>';  

             if($data_mention['hp'] <> '' && $data_mention['hp_tags'] <> '')
              $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
              else if($data_mention['hp'] <> '')
              $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
              else if($data_mention['hp_tags'] <> '')
              $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';  
            // if($data_mention['hp'] <> ''){
            //   $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input Sentiment" data-toggle="tooltip"></i></a>';
            // }
            $html.='<div><p style="float: right;width: 100%;">
            <span style="float:left" class="sl-date"><i class="mdi mdi-calendar-clock"></i>'.$data_mention['created_time'].' by <a href="'.$page_Link.'" class="link"  target="_blank" >'.$data_mention['page_name'].' </a></span>';

             $html.=' <p> Tag :<select id="tags_'.$data_mention['id'].'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                       $tag_array=explode(',', $data_mention['tags']);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';



            $html.='<select id="sentiment_'.$data_mention['id'].'" style="float: right;" class="form-material form-control in-comment-select edit_senti ';
                   if($data_mention['sentiment']=="")$html.='"><option value="" selected="selected" ></option>';
                   if($data_mention['sentiment']=="pos")$html.='text-success" >';
                   if($data_mention['sentiment']=="neg")$html.='text-red" >';
                   if($data_mention['sentiment']=="neutral" || $data_mention['sentiment'] == "NA")$html.='text-warning" >';
                
                    $html.='<option value="pos" class="text-success"';
                    if($data_mention['sentiment'] == "pos")
                    $html.= 'selected="selected"';
                    $html.= '>Positive</option><option value="neg" class="text-red"';
                    if($data_mention['sentiment'] == "neg")
                    $html.= 'selected="selected"';
                    $html.= '>Negative</option><option value="neutral" class="text-warning"';
                    if($data_mention['sentiment'] == "neutral" || $data_mention['sentiment'] == "NA")
                    $html.= 'selected="selected"';      
                    $html.= '>Neutral</option></select></p>';


                    // $html.= '<a class="edit_predict_post" id="'.$data_mention['id'].'" href="javascript:void(0)">
                    // <span class="mdi mdi-content-save" style="font-size:30px;"></span></a></p>';
                    $html.='<div style="display:none"  align="center" style="vertical-align: top;" id="spin_'.$data_mention['id'].'"> <img src="assets/images/ajax-loader.gif" id="loader"></div>';
                    
                    $html.='</div><p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['id'].'">View More</a></p>';
                              
                    $html.= '</div><div class="sl-right" align="right"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-gray post_hide"  id="hide_'.$data_mention['id'].'" >Hide Article</a></div></div></div></div>';
                    return $html;

                  })
            ->rawColumns(['post'])
         
            ->make(true);
    }
    public function getMentionPageList()
    {
      
       $data=MongoPages::where('watch', 'on')->where('id','<>','')->get()->toArray();
            return Datatables::of($data)
       ->addColumn('action', function ($data) {

         $html='<div class="message-box"><div class="form-group has-info">'.
                '<select id="'.$data['id'].'" class="form-control custom-select tier_select"> ';
              if(isset($data["page_tier"]))
              {
             

                    $html.='<option value="Tier 1"';
                    if($data["page_tier"] == "Tier 1")
                    $html.= 'selected=selected';
                     $html.= '>Tier 1</option>';

                     $html.='<option value="Tier 2"';
                    if($data["page_tier"] == "Tier 2")
                    $html.= 'selected=selected';
                     $html.= '>Tier 2</option>';


                    $html.='<option value="Tier 3"';
                    if($data["page_tier"] == "Tier 3")
                    $html.= 'selected=selected';
                     $html.= '>Tier 3</option>';

            
              }
              else{
              $html.='<option selected="selected" disabled>Choose Tier</option>';
              $html.='<option value="Tier 1">Tier 1</option>';
              $html.='<option value="Tier 2">Tier 2</option>';
              $html.='<option value="Tier 3">Tier 3</option>';
             }
              $html.='</select></div>'; 
              return $html;

            })
       ->rawColumns(['action'])
      ->make(true);

    }
    public function getRelatedArticle()
    {
      $post_id=Input::get('id');
      $brand_id=Input::get('brand_id');

       $permission_data = $this->getPermission();
       $edit_permission=$permission_data['edit'];

      $query="SELECT  posts.id,1 as edit_permission,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
      " created_time,posts.title title,posts.link link".
      " from temp_web_mentions posts ".
      "  where posts.id='".$post_id."' GROUP BY id";

              $data = DB::select($query);

         if(null !== Input::get('keyword_group'))
        {
          $keyword_group = Input::get('keyword_group') ;
        }
        else
        {
          $keyword_group = '' ;
        }
        $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);
        $keyForHighLight=$this->getKeywordforHighLight($keyword_data);
        if(null !== Input::get('page_name'))
        {
          $page_name = Input::get('page_name') ;
        }
        else
        {
          $page_name = '' ;
        }
        // $keyword_campaign = $this->getcampaignkeyword($brand_id,$page_name);
        $keyword_campaign='';
         if(null !== Input::get('campaign_name'))
          {
            $keyword_campaign= $this->getCampaignKeywordFilter(Input::get('campaign_name'),$brand_id);
          }
        echo json_encode(array($data,$keyForHighLight, $keyword_campaign ));
    }
    public function setMentionPageTier_Mongo()
    {
      $page_level =  Input::get('page_level');
      $page_id =  Input::get('page_id');
      $data=MongoPages::where('id',  $page_id)->update(['page_tier'=>$page_level]);
      return $data;

    }

     public function getMentionComment(Request $request)
    {
       $senti_con = '';
       if(null !== Input::get('sentiType'))
      {
        $senti_con = " and cmts.checked_sentiment='".Input::get('sentiType')."'";
      }


      $brand_id=Input::get('brand_id');
      
   
   
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment ='';
      
      $period_type = '';
      $additional_filter=" AND 1=1 ";
      
        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

            
              if($period_type == 'week')
              {
               $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

              }
              else
              {
                  $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
              }

        }
    

            if(null !== Input::get('sentiment'))
            $filter_sentiment=Input::get('sentiment');

          
            // global filter
             $add_global_filter='';
     
     

              if(null !== Input::get('fday'))
            {
                $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            }
            else
            {
                $dateBegin=date('Y-m-d');
            }
          

          if(null !==Input::get('sday'))
          {

               $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          
          else
          {
              $dateEnd=date('Y-m-d');

          }



            $add_con=' 1=1';

         if(null !== Input::get('tsearch_senti'))
              {
                // $global_senti=Input::get('tsearch_senti'); 
                // if($global_senti == 'neutral')
                // $add_global_filter .=" and (cmts.checked_sentiment = '".$global_senti."' or cmts.checked_sentiment = 'NA')";
                // else
                // $add_global_filter .=" and cmts.checked_sentiment = '".$global_senti."'";
                $add_con .= ' AND ' . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
              }
           // $keyword_data = $this->getprojectkeyword($brand_id);
           //  if(isset($keyword_data [0]['main_keyword']))
           //  $add_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")"; 
           //  else
           //  $add_con .= ' AND 1=2';
              $keyword_data =[];
             if(null !== Input::get('keyword_group'))
          {
             $keyword_group = Input::get('keyword_group') ;
             $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);
             if(isset($keyword_data [0]['main_keyword']))
             $additional_filter  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")"; 
            else
             $additional_filter .= ' AND 1=2';
          }
          // dd($additional_filter);
          if(null !== Input::get('page_name'))
          {
           $additional_filter .= ' AND cmts.page_name="' .Input::get('page_name') . '" ';
          }
          // else
          // {
          //   $keyword_group = '' ;
          // }
        

         if(null !== Input::get('tsearch_keyword'))
           { 
               $add_con .= ' AND ' . $this->getKeywordWhereGen(Input::get('tsearch_keyword'),'cmts');
           }
          
          
            if(null !== Input::get('tsearch_emo'))
            {
              $global_emo=Input::get('tsearch_emo');
                $add_con .=" and cmts.emotion = '".$global_emo."'";

               }
               if(null !== Input::get('tsearch_interest') && Input::get('tsearch_interest') === "true")
            {
              // echo "hihi";
                $add_con .=" and cmts.interest = 1";

               }

          if(null !== Input::get('post_id'))
           $add_con .= " and cmts.post_id='".Input::get('post_id')."'"; 

          $tags =$this->gettags($brand_id);

          $date_filter='';
          if(null !== Input::get('fday'))
          {
           
             $date_filter=" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
          }
          else
          {
            $date_filter = ' 1=1 ';
          }
          $add_inbound_con ="";
          $inboundpages=$this->getInboundPages($brand_id);
          if ($inboundpages !== '')
          {
            $add_inbound_con = " and (cmts.page_name  not in (".$inboundpages.") or cmts.page_name is NULL)";
          }
          $excludepages = $this->getExcludePages($brand_id);
          if($excludepages != ""){
            $add_inbound_con .=" and ( cmts.page_name  not in (".$excludepages.") or  cmts.page_name is NULL)";
          }
        $tier_con = '';
        if(null !==Input::get('tier'))
        {
          $tier_filter_pages = MongoPages::where('page_tier',Input::get('tier'))->where('watch','on')->get()->toArray();
          foreach ($tier_filter_pages as $key => $value) {
            # code...
            $page[] = $value['page_name'];
          }
          $page_string = implode(',', $page);
       
          $result_string = "'" . str_replace(",", "','", $page_string) . "'";
             // dd($result_string);
          $tier_con = " AND cmts.page_name IN(".$result_string.")";
        }
          if(Input::get('keyword_group') == "KBZPay"){

             $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i: %p') created_time, ".
                    " cmts.checked_sentiment sentiment,cmts.emotion,cmts.page_name, ". 
                    " IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp ".
                    " FROM temp_comments_kbzpay cmts  ".
                    " WHERE ".$date_filter. $additional_filter . $senti_con . $tier_con .

                    " ORDER by timestamp(cmts.created_time) DESC";

          }else{

           $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i: %p') created_time, ".
                  " cmts.checked_sentiment sentiment,cmts.emotion,cmts.page_name, ". 
                  " IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp ".
                  " FROM temp_comments cmts  ".
                  " WHERE cmts.isHide=0 and ".$date_filter."  AND ". $add_con .  $add_inbound_con . $additional_filter . $senti_con . $tier_con .

                  " ORDER by timestamp(cmts.created_time) DESC";

        }
           // dd($query);

            // print_r($query);
            // return;
          $data = DB::select($query);
// dd($data);

          $permission_data = $this->getPermission(); 
          // if($is_competitor == "false")

            return Datatables::of($data)
            
           ->addColumn('post_div', function ($data)   use($permission_data,$keyword_data ) {
             $post_message_tag='';
            
            $page_name=$data->page_name ;
            
            //generate comment link
            $profilePhoto='';$ProfileName='Unknown';
            $profileLink='javascript:void(0)';
            $fb_p_id=explode('_', $data->post_id);
            $fb_p_id=$fb_p_id[1];
            $fb_c_id=explode('_', $data->id);
            $fb_c_id=$fb_c_id[1];
            if($page_name <> '')
            $ProfileName=$page_name;

            

            $page_id='';
            $arr_page_id=explode("-",$page_name);
            if(count($arr_page_id)>0)
            {
              $last_index=$arr_page_id[count($arr_page_id)-1];
              if(is_numeric($last_index))
              $page_id = $last_index;
              else
              $page_id = $page_name;
            }
            else
            {
              $page_id=$page_name;
            }

            $comment_Link= 'https://www.facebook.com/'.$fb_c_id;
            $classforbtn='';$stylefora='';$bgcolor='';
                  if($data->sentiment=='neg')
                    {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                  else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                    else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}

                    $html = '<div  id="'.$data->id.'"  class="profiletimeline"><div class="sl-item">';
                    if($profilePhoto<>'')
                    $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                    else
                    /*$html .= '<div class="sl-left user-img"> <span class="round"><i class="mdi mdi-comment-account"></i></span> </div>';*/
                  $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                   
                     $html .='<div class="form-material sl-right"><div>  <div>
                  <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a> ' ;
                   if($data->hp <> '')
                  $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input" data-toggle="tooltip"></i></a>';

                  $html .=' <span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                           
                   $html.='<p>Sentiment : <select id="sentiment_'.$data->id.'"  class="form-material form-control in-comment-select edit_senti ';
                   if($data->sentiment=="")$html.='"><option value="" selected="selected" ></option>';
                   if($data->sentiment=="pos")$html.='text-success" >';
                   if($data->sentiment=="neg")$html.='text-red" >';
                   if($data->sentiment=="neutral" || $data->sentiment == "NA")$html.='text-warning" >';
                
                    $html.='<option value="pos" class="text-success"';
                    if($data->sentiment == "pos")
                    $html.= 'selected="selected"';
                    $html.= '>Positive</option><option value="neg" class="text-red"';
                    if($data->sentiment == "neg")
                    $html.= 'selected="selected"';
                    $html.= '>Negative</option><option value="neutral" class="text-warning"';
                    if($data->sentiment == "neutral" || $data->sentiment == "NA")
                    $html.= 'selected="selected"';      
                    $html.= '>Neutral</option></select>';

                    $html.='</p><p class="m-t-10">'.$this->highLightText($keyword_data,$data->message).'</p>'.$post_message_tag.'</div>';
                    if($data->cmtType =='sticker' || $data->cmtType =='photo' || $data->cmtType =='share' || $data->cmtType =='share_large_image' || $data->cmtType =='animated_image_share'  )
                       {
                          $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'.$data->id.'">'.
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                              '<img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">'.
                      '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                       else if($data->cmtType =='video_inline' || $data->cmtType =='animated_image_video' || $data->cmtType =='video_share_youtube' || $data->cmtType =='video_share_highlighted')
                       {
                         $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'.
                          '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                          '<iframe src="https://www.facebook.com/plugins/video.php?href='.$data->cmtLink.'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'.
                          '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                    $html.='<div class="sl-right">';
                            if((int)$data->cmtLiked > 0)
                            $html.='👍 '.$this->thousandsCurrencyFormat($data->cmtLiked);
                           if((int)$data->cmtLove > 0)
                            $html.=' ❤️ '.$this->thousandsCurrencyFormat($data->cmtLove);
                           if((int)$data->cmtHaha > 0)
                            $html.=' 😆 '.$this->thousandsCurrencyFormat($data->cmtHaha);
                           if((int)$data->cmtWow > 0)
                            $html.=' 😮 '.$this->thousandsCurrencyFormat($data->cmtWow);
                           if((int)$data->cmtSad > 0)
                            $html.=' 😪 '.$this->thousandsCurrencyFormat($data->cmtSad);
                           if((int)$data->cmtAngry > 0)
                            $html.=' 😡 '.$this->thousandsCurrencyFormat($data->cmtAngry);
                            $html.='</div>
                        <div class="sl-right" style="float: right;">
                        <div style="display: inline-block"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-gray comment_hide"  id="hide_'.$data->id.'" >Hide Comment</a> <a href="'.$comment_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See on FB</a></div>
                      <div style="display: inline-block" class="btn-group">';
                  
              $html.='</div></div></div></div>';

                    return $html;

                })
          ->addColumn('reaction', function ($data) {
            $total = (int)$data->cmtLiked + (int)$data->cmtLove + (int)$data->cmtWow + (int)$data->cmtHaha + (int)$data->cmtSad+ (int)$data->cmtAngry;
                      return "<span style='font-weight:bold'>" . number_format($total)."</span>";
                  }) 
      
          ->rawColumns(['post_div','reaction'])
       
          ->make(true);

    }
   
    public function getmentiondetail()
    {
                //get keywork
              //  $keyword_data = ProjectKeyword::find(1);//projectid
              $brand_id=Input::get('brand_id');
              /* $brand_id=81;*/
              $groupType=Input::get('periodType');
              $filter_keyword ='';
              $filter_sentiment='';
              if(null !== Input::get('keyword'))
                $filter_keyword=Input::get('keyword'); 
              if(null !== Input::get('sentiment'))
                $filter_sentiment=Input::get('sentiment'); 
          /*
                $brand_id=30;
                $groupType='month';*/
                

                if(null !== Input::get('fday'))
                {
                  $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
                }
                else
                {
                  $dateBegin=date('Y-m-d');
                }
                

                if(null !==Input::get('sday'))
                {

                 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
               }
               
               else
               {
                $dateEnd=date('Y-m-d');

              }
          /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
          $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

           // $groupType="month";
              $dateformat ="";
              $add_con="";
              $add_comment_con="";
              $add_article_con="";

              if($groupType=="month")
              {
                $dateformat ="%Y-%m"; 
              }

              else 
              {
                $dateformat ="%Y-%m-%d";                
                

              }
              //$format =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
              

              if($filter_keyword !== '')
              {
                $add_con = " and message Like '%".$filter_keyword."%'";
                $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
                $add_article_con = " and article.message Like '%".$filter_keyword."%'";
              }

              if($filter_sentiment !== '')
              {
                $add_con .= " and sentiment = '".$filter_sentiment."'";
                $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
                $add_article_con .= " and article.sentiment  = '".$filter_sentiment."'";
              }

              $add_inbound_con ="";

              $inboundpages=$this->getInboundPages($brand_id);
              if ($inboundpages !== '')
              {
                $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
                
              }
              $excludepages = $this->getExcludePages($brand_id);
              if($excludepages != ""){
                $add_inbound_con .=" and (page_name  not in (".$excludepages.") or page_name is NULL)";
              }

              $comment_key_con=" AND 1=1 ";
              $post_key_con=" AND 1=1 ";
              $article_key_con=" AND 1=1 ";

              if(null !== Input::get('keyword_group'))
              {
                $keyword_group = Input::get('keyword_group') ;
              }
              else
              {
                $keyword_group = '' ;
              }

              // $keyword_group=auth()->user()->default_keyword;
              $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);
              $color =  $this->getbrandcolor($keyword_group);
              if(isset($keyword_data [0]['main_keyword']))
              {
                $comment_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")";
                $post_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")";  
                $article_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'article') . ")";
              }
              
              else
              {

               $comment_key_con .= ' AND 1=2';
               $post_key_con .= ' AND 1=2';
               $article_key_con .= ' AND 1=2';
             }

             if(Input::get('keyword_group') == 'KBZPay'){
             
               $query = "select count(*) count,created_time  from (SELECT DATE_FORMAT(created_time,'".$dateformat."') created_time,posts.page_name FROM temp_posts_kbzpay posts  WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')"."
               UNION ALL
               SELECT DATE_FORMAT(cmts.created_time,'".$dateformat."') created_time,cmts.page_name FROM temp_comments_kbzpay cmts ".
               "WHERE (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')"." 
               UNION ALL
               SELECT DATE_FORMAT(article.created_time,'".$dateformat."') created_time,article.page_name FROM temp_web_mentions_kbzpay article WHERE (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')) t1 Group By created_time ORDER BY created_time ";
            }else{

             $query = "select count(*) count,created_time  from (SELECT DATE_FORMAT(created_time,'".$dateformat."') created_time,posts.page_name FROM temp_posts posts  WHERE posts.isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con .$add_inbound_con. $post_key_con  ." 
             UNION ALL
             SELECT DATE_FORMAT(cmts.created_time,'".$dateformat."') created_time,cmts.page_name FROM temp_comments cmts ".
             "WHERE cmts.isHide=0 and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_comment_con .$add_inbound_con .  $comment_key_con  . " 
             UNION ALL
             SELECT DATE_FORMAT(article.created_time,'".$dateformat."') created_time,article.page_name FROM temp_web_mentions article WHERE article.isHide = 0 and (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_article_con  . $article_key_con . ") t1 Group By created_time ORDER BY created_time ";

           }
 
             $query_result = DB::select($query);
            // dd($query_result);
             
             $data_mention = [];



             foreach ($query_result as  $key => $row) {
              
              $request['periods'] =$row ->created_time;
              $request['mention'] =$row ->count;
              $request['periodLabel'] ="";
              
              
              
              if($groupType == "week")
              {
                $weekperiod=$this ->rangeWeek ($request['periods']);
                $request['periodLabel'] =$weekperiod['start'] . " - " . $weekperiod['end'] ;
                $periodLabel = $request['periodLabel'];
                if (!array_key_exists($periodLabel , $data_mention)) {
                  $data_mention[$periodLabel] = array(
                    'periodLabel' => $periodLabel,
                    'mention' =>   $request['mention'],
                    'periods' =>  $request['periods'],
                    'color' => $color
                  );
                }
                else
                {
                 $data_mention[$periodLabel]['mention'] = (int) $data_mention[$periodLabel]['mention'] +  $request["mention"];
                 $data_mention[$periodLabel]['periods'] = $request['periods'];
                 $data_mention[$periodLabel]['color'] = $color;
               }

             }
             else
             {
               $request['periodLabel'] =$request['periods'] ;
               $periodLabel = $request['periodLabel'];
               if (!array_key_exists($periodLabel , $data_mention)) {
                $data_mention[$periodLabel] = array(
                  'periodLabel' => $periodLabel,
                  'mention' =>   $request['mention'],
                  'periods' =>  $request['periods'],
                  'color' => $color,
                );
              }
              else
              {
               $data_mention[$periodLabel]['mention'] = (int) $data_mention[$periodLabel]['mention'] +  $request["mention"];
               $data_mention[$periodLabel]['periods'] = $request['periods'];
               $data_mention[$periodLabel]['color'] = $color;

             }
             
            }
            }
            $data_mention = $this->unique_multidim_array($data_mention,'periodLabel'); 
            usort($data_mention, function($a1, $a2) {
             $value1 = strtotime($a1['periods']);
             $value2 = strtotime($a2['periods']);
             return $value1 - $value2;
            });
// dd($data_mention);
            echo json_encode(array($data_mention,$color));

    }
        public function getMentionTier()
    {

            $brand_id=Input::get('brand_id');

            if(null !== Input::get('fday'))   $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            else                              $dateBegin=date('Y-m-d');

            if(null !==Input::get('sday'))    $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            else                              $dateEnd=date('Y-m-d');

                 
            $comment_key_con=" AND 1=1 ";
            $post_key_con=" AND 1=1 ";
            $article_key_con=" AND 1=1 ";

            if(null !== Input::get('keyword_group')){
             $keyword_data = $this->getprojectkeyword($brand_id,Input::get('keyword_group'));
             if(isset($keyword_data [0]['main_keyword'])){
              $comment_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")";
              $post_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 
              $article_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'article') . ")";  
            }
            else{
             $comment_key_con .= ' AND 1=2';
             $post_key_con .= ' AND 1=2';
             $article_key_con .= ' AND 1=2';
           }
         }
          $add_inbound_con ="";
          $inboundpages=$this->getInboundPages($brand_id);
          if ($inboundpages !== ''){
            $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
          }
          $excludepages = $this->getExcludePages($brand_id);
          if($excludepages != ""){
            $add_inbound_con .=" and (page_name  not in (".$excludepages.") or page_name is NULL)";
          }

          $query = "Select page_name,count(page_name) 'count' from ( SELECT page_name  FROM temp_posts posts  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .$add_inbound_con .  $post_key_con  ." and page_name <> ''   " .
           " UNION ALL" . 
           " SELECT page_name  FROM temp_comments cmts  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inbound_con . $comment_key_con  ." and page_name <> ''  " .
           " UNION ALL" .
           " SELECT page_name  FROM temp_web_mentions article  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $article_key_con  ." and page_name <> ''  " .
           " ) t1 group by page_name ORDER BY page_name DESC " ;
 
          $query_result = DB::select($query);
          // dd($query_result);
          $page = [];
          foreach($query_result as $pg)
          {
            $page[] = $pg->page_name;
          }
          // dd($page);

//    $mention_post_result=MongoPages::select(raw("COALESCE(SUM(IF(page_tier = 'Tier 1' ,1,0)),0) As tier1"),raw("COALESCE(SUM(IF(page_tier = 'Tier 2' ,1,0)),0) As tier2"),raw("COALESCE(SUM(IF(page_tier = 'Tier 3' ,1,0)),0) As tier3"))->whereIn('page_name',$query_result)->get()->toArray();
// dd($mention_post_result);
           $mention_page_result = MongoPages::whereIn('page_name', $page)->get()->toArray();
// dd($mention_post_result);s
           // dd($mention_page_result);

            // $tier1 =[];$tier2=[];$tier3=[];
            $data=[];
            $tier1_count=0;$tier2_count=0;$tier3_count=0;
            foreach ($mention_page_result as $key => $value) {

              foreach($query_result as $pg)
              {
                if($pg->page_name == $value['page_name']){
                  if(isset($value['page_tier'])){
                  $tier = $value['page_tier'];
                  if($tier == 'Tier 1') $tier1_count += $pg->count;
                  elseif($tier == 'Tier 2') $tier2_count += $pg->count;
                  else $tier3_count += $pg->count;
                }
              }

              }
            }
            // dd($tier1_count,$tier2_count,$tier3_count);
                $data = array(array(

               'Tier1' => $tier1_count ,
               'Tier2' => $tier2_count ,
               'Tier3' => $tier3_count ,
             )

            );
                // dd($data);
                 echo json_encode($data);
           // SELECT name,COUNT(*) as count FROM tablename GROUP BY name ORDER BY count DESC;


          // DB::table("nilais")
          //     ->select("nama", "aspek", DB::raw("SUM(IF(kelompok='core',bobot,0))/SUM(IF(kelompok='core',1,0)) AS core"), DB::raw("SUM(IF(kelompok='secondary',bobot,0))/SUM(IF(kelompok='secondary',1,0)) AS secondary"))
          //     ->join(//Dont know the USING syntax)
          //     ->join(//I don't know the USING syntax)
          //     ->join("gaps", "e.selisih", "nilai-nilai_sub") // this looks strange to...
          //     ->get();

          //  $query ="SELECT COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period ." created_time  ".
          //   "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
          //   " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
          //   " AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_searchkw_con .
          //   " GROUP BY ".$group_period .
          //   " ORDER BY ".$group_period;

          // $query_result = DB::select($query);
          // dd($query_result);
//    $data = [];
//    $total=0;

//    $data_sentiment = [];
//    $data_mention = [];
//    $total=0;
//    $data_message = [];

//     foreach ($query_result as  $key => $row) {
          
//               $utcdatetime = $row->created_time;
         
               
//                $pos =$row->positive;
//                $neg =$row->negative;
//                $neutral =(int)$row->neutral + (int)$row->NA;
                       
//                 $request['periodLabel'] ="";
      
//                 if($groupType == "week")
// {
//     $weekperiod=$this ->rangeWeek ($utcdatetime);
//     $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
    
//  if (!array_key_exists($periodLabel , $data_sentiment)) {
//     $data_sentiment[$periodLabel] = array(
//                 'periodLabel' => $periodLabel,
//                 'positive' => $pos,
//                 'negative' => $neg,
//                 'neutral' => $neutral,
//                 'periods' =>$utcdatetime,
//             );
// }
// else
// {
//  $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
//  $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
//  $data_sentiment[$periodLabel]['neutral'] = $data_sentiment[$periodLabel]['neutral'] + $neutral;
//  $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
// }
 
                  


// }
//  else
// {
//  $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
//  $data_sentiment[$utcdatetime]['positive'] = $pos;
//  $data_sentiment[$utcdatetime]['negative'] = $neg;
//  $data_sentiment[$utcdatetime]['neutral'] = $neutral;
//  $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

// }

                
   }

    public function getMentionTierDetail()
    {

            $brand_id=Input::get('brand_id');

            if(null !== Input::get('fday'))   $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            else                              $dateBegin=date('Y-m-d');

            if(null !==Input::get('sday'))    $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            else                              $dateEnd=date('Y-m-d');

                 
            $comment_key_con=" AND 1=1 ";
            $post_key_con=" AND 1=1 ";
            $article_key_con=" AND 1=1 ";

            if(null !== Input::get('keyword_group')){
             $keyword_data = $this->getprojectkeyword($brand_id,Input::get('keyword_group'));
             if(isset($keyword_data [0]['main_keyword'])){
              $comment_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")";
              $post_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 
              $article_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'article') . ")";  
            }
            else{
             $comment_key_con .= ' AND 1=2';
             $post_key_con .= ' AND 1=2';
             $article_key_con .= ' AND 1=2';
           }
         }
          $add_inbound_con ="";
          $inboundpages=$this->getInboundPages($brand_id);
          if ($inboundpages !== ''){
            $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
          }
          $excludepages = $this->getExcludePages($brand_id);
          if($excludepages != ""){
            $add_inbound_con .=" and (page_name  not in (".$excludepages.") or page_name is NULL)";
          }

          if (Input::get('keyword_group') == "KBZPay") 
          {
            $query = "Select page_name,count(page_name) 'count' from ( SELECT page_name  FROM temp_posts_kbzpay posts  WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')".
                     " UNION ALL" . 
                     " SELECT page_name  FROM temp_comments_kbzpay cmts  WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')".
                     " UNION ALL" .
                     " SELECT page_name  FROM temp_web_mentions_kbzpay article  WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') and page_name <> ''  " .
                     " ) t1 group by page_name ORDER BY page_name DESC " ;
          }else{

          $query = "Select page_name,count(page_name) 'count' from ( SELECT page_name  FROM temp_posts posts  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .$add_inbound_con .  $post_key_con  ." and page_name <> ''    " .
           " UNION ALL" . 
           " SELECT page_name  FROM temp_comments cmts  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inbound_con . $comment_key_con  ." and page_name <> '' " .
           " UNION ALL" .
           " SELECT page_name  FROM temp_web_mentions article  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $article_key_con  ." and page_name <> ''  " .
           " ) t1 group by page_name ORDER BY page_name DESC " ;

         }
 
          $query_result = DB::select($query);
          //dd($query);
          // dd($query_result);
          $page = [];
          foreach($query_result as $pg)
          {
            $page[] = $pg->page_name;
          }
          // dd($page);


//    $mention_post_result=MongoPages::select(raw("COALESCE(SUM(IF(page_tier = 'Tier 1' ,1,0)),0) As tier1"),raw("COALESCE(SUM(IF(page_tier = 'Tier 2' ,1,0)),0) As tier2"),raw("COALESCE(SUM(IF(page_tier = 'Tier 3' ,1,0)),0) As tier3"))->whereIn('page_name',$query_result)->get()->toArray();
// dd($mention_post_result);
           $mention_page_result = MongoPages::whereIn('page_name', $page)->get()->toArray();
          //dd($mention_page_result);

           // dd($mention_page_result);

            // $tier1 =[];$tier2=[];$tier3=[];

            $data=[];
            $tier1_count=0;$tier2_count=0;$tier3_count=0;
            foreach ($mention_page_result as $key => $value) {
              # code...


              foreach($query_result as $pg)
              {
                if($pg->page_name == $value['page_name']){
                    if(isset($value['page_tier'])){
                    $tier = $value['page_tier'];
                    if($tier == 'Tier 1') $tier1_count += $pg->count;
                    elseif($tier == 'Tier 2') $tier2_count += $pg->count;
                    else $tier3_count += $pg->count;
                }
              }


            }
            }
            // dd($tier1_count,$tier2_count,$tier3_count);
                $data = array(

                array('periodLabel' =>'Tier 1','tier1' => $tier1_count,'tier2' => $tier2_count,'tier3' => $tier3_count),
                array('periodLabel' =>'Tier 2','tier1' => $tier1_count,'tier2' => $tier2_count,'tier3' => $tier3_count),
                array('periodLabel' =>'Tier 3','tier1' => $tier1_count,'tier2' => $tier2_count,'tier3' => $tier3_count),
            );
                // dd($data);
                 echo json_encode($data);
           // SELECT name,COUNT(*) as count FROM tablename GROUP BY name ORDER BY count DESC;


          // DB::table("nilais")
          //     ->select("nama", "aspek", DB::raw("SUM(IF(kelompok='core',bobot,0))/SUM(IF(kelompok='core',1,0)) AS core"), DB::raw("SUM(IF(kelompok='secondary',bobot,0))/SUM(IF(kelompok='secondary',1,0)) AS secondary"))
          //     ->join(//Dont know the USING syntax)
          //     ->join(//I don't know the USING syntax)
          //     ->join("gaps", "e.selisih", "nilai-nilai_sub") // this looks strange to...
          //     ->get();

          //  $query ="SELECT COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period ." created_time  ".
          //   "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
          //   " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
          //   " AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_searchkw_con .
          //   " GROUP BY ".$group_period .
          //   " ORDER BY ".$group_period;

          // $query_result = DB::select($query);
          // dd($query_result);
//    $data = [];
//    $total=0;

//    $data_sentiment = [];
//    $data_mention = [];
//    $total=0;
//    $data_message = [];

//     foreach ($query_result as  $key => $row) {
          
//               $utcdatetime = $row->created_time;
         
               
//                $pos =$row->positive;
//                $neg =$row->negative;
//                $neutral =(int)$row->neutral + (int)$row->NA;
                       
//                 $request['periodLabel'] ="";
      
//                 if($groupType == "week")
// {
//     $weekperiod=$this ->rangeWeek ($utcdatetime);
//     $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
    
//  if (!array_key_exists($periodLabel , $data_sentiment)) {
//     $data_sentiment[$periodLabel] = array(
//                 'periodLabel' => $periodLabel,
//                 'positive' => $pos,
//                 'negative' => $neg,
//                 'neutral' => $neutral,
//                 'periods' =>$utcdatetime,
//             );
// }
// else
// {
//  $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
//  $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
//  $data_sentiment[$periodLabel]['neutral'] = $data_sentiment[$periodLabel]['neutral'] + $neutral;
//  $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
// }
 
                  


// }
//  else
// {
//  $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
//  $data_sentiment[$utcdatetime]['positive'] = $pos;
//  $data_sentiment[$utcdatetime]['negative'] = $neg;
//  $data_sentiment[$utcdatetime]['neutral'] = $neutral;
//  $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

// }

                
   }

 // $data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
 //    echo json_encode($data_sentiment);
    

    public function getPagewithmentioncount()
    {
            //get keywork
            //  $keyword_data = ProjectKeyword::find(1);//projectid
            $brand_id=Input::get('brand_id');
            /* $brand_id=81;*/
            $groupType=Input::get('periodType');
            $filter_keyword ='';
            $filter_sentiment='';
            if(null !== Input::get('keyword'))
              $filter_keyword=Input::get('keyword'); 
            if(null !== Input::get('sentiment'))
              $filter_sentiment=Input::get('sentiment'); 
              /*
                    $brand_id=30;
                    $groupType='month';*/;    

                    if(null !== Input::get('fday'))
                    {
                      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
                    }
                    else
                    {
                      $dateBegin=date('Y-m-d');
                    }
                    

                    if(null !==Input::get('sday'))
                    {

                     $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
                   }
                   
                   else
                   {
                    $dateEnd=date('Y-m-d');

                  }
              // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2019 01 06')));
              // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2019 02 06')));

               // $groupType="month";
                  $dateformat ="";
                  $add_con="";
                  $add_comment_con="";
                  $add_article_con="";

                  if($groupType=="month")
                  {
                    $dateformat ="%Y-%m"; 
                  }

                  else 
                  {
                    $dateformat ="%Y-%m-%d";                
                    

                  }
              //$format =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
                  

                  if($filter_keyword !== '')
                  {
                    $add_con = " and posts.message Like '%".$filter_keyword."%'";
                    $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
                    $add_article_con = " and article.message Like '%".$filter_keyword."%'";
                  }

                  if($filter_sentiment !== '')
                  {
                    $add_con .= " and posts.checked_sentiment = '".$filter_sentiment."'";
                    $add_comment_con .= " and cmts.checked_sentiment  = '".$filter_sentiment."'";
                    $add_article_con .= " and article.checked_sentiment  = '".$filter_sentiment."'";
                  }

                  $add_inbound_con ="";
                  $inboundpages=$this->getInboundPages($brand_id);
                  if ($inboundpages !== '')
                  {
                    $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
                  }
                  $excludepages = $this->getExcludePages($brand_id);
                  if($excludepages != ""){
                    $add_inbound_con .=" and (page_name  not in (".$excludepages.") or page_name is NULL)";
                  }

                  $comment_key_con=" AND 1=1 ";
                  $post_key_con=" AND 1=1 ";
                  $article_key_con=" AND 1=1 ";

                  if(null !== Input::get('keyword_group'))
                  {
                   $keyword_data = $this->getprojectkeyword($brand_id,Input::get('keyword_group'));
                   if(isset($keyword_data [0]['main_keyword']))
                   {
                    $comment_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")";
                    $post_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 
                    $article_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'article') . ")";  
                  }
                  
                  else
                  {

                   $comment_key_con .= ' AND 1=2';
                   $post_key_con .= ' AND 1=2';
                   $article_key_con .= ' AND 1=2';
                 }
               }

               if(Input::get('keyword_group') == "KBZPay"){

                 $query = "SELECT page_name,count(page_name) as page_count FROM temp_posts_kbzpay posts WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . " GROUP BY page_name". " UNION ALL " . "SELECT page_name,count(page_name) as page_count FROM temp_comments_kbzpay cmts WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" ." GROUP BY page_name  ". 
                 " UNION ALL " . " SELECT page_name,count(page_name) as page_count FROM temp_web_mentions_kbzpay article WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . " GROUP BY page_name".  " ORDER BY page_count DESC ";

              }else{

                 $query = "SELECT page_name,count(page_name) as page_count  FROM temp_posts posts  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con .$add_inbound_con. $post_key_con  ." and page_name <> ''  GROUP BY page_name  " .
               " UNION ALL" .
               " SELECT page_name,count(page_name) as page_count  FROM temp_comments cmts  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con .$add_inbound_con. $comment_key_con  ." and page_name <> '' GROUP BY page_name " .
               " UNION ALL" .
               " SELECT page_name,count(page_name) as page_count  FROM temp_web_mentions article  WHERE isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_article_con . $article_key_con  ." and page_name <> '' GROUP BY page_name " .
               "ORDER BY page_count DESC " ;

              }

               
               //dd($query);
               $query_result = DB::select($query);
               // dd($query_result);

               $data=[];
               foreach ($query_result as $key => $value) {

                $page_name=$value->page_name;
                $page_count=$value->page_count;
                if (!array_key_exists($page_name , $data)) {
                  $data[$page_name] = array(
                    'page_name' => $page_name,
                    'count' =>   $page_count,
                    
                  );
                }
                else
                {
                  $data[$page_name]['count'] = (int) $data[$page_name]['count'] + (int) $page_count;
                }
              }
              usort($data, function($a, $b) {
              return $b['count'] <=> $a['count'];
            });

              // dd($data);
              
              echo json_encode($data);

    }
     public function getPagewithWebsitementioncount()
    {
           
            //  $keyword_data = ProjectKeyword::find(1);//projectid
            $brand_id=Input::get('brand_id');
            $groupType=Input::get('periodType');
            $filter_keyword ='';
            $filter_sentiment='';
            if(null !== Input::get('keyword'))
              $filter_keyword=Input::get('keyword'); 
            if(null !== Input::get('sentiment'))
              $filter_sentiment=Input::get('sentiment');

            if(null !== Input::get('fday'))
            {
              $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            }
            else
            {
              $dateBegin=date('Y-m-d');
            }
            if(null !==Input::get('sday'))
            {

             $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }  
                   
           else
           {
            $dateEnd=date('Y-m-d');
           }
              // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2019 01 06')));
              // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2019 02 06')));

               // $groupType="month";
          $dateformat ="";
          $add_con="";
          $add_comment_con="";

          if($groupType=="month")
          {
            $dateformat ="%Y-%m"; 
          }
          else 
          {
            $dateformat ="%Y-%m-%d"; 
          }
              //$format =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
                  

          if($filter_keyword !== '')
          {
            $add_con = " and message Like '%".$filter_keyword."%'";
          }

          if($filter_sentiment !== '')
          {
            $add_con .= " and sentiment = '".$filter_sentiment."'";
          }

          $add_inbound_con ="";
          $inboundpages=$this->getInboundPages($brand_id);
          if ($inboundpages !== '')
          {
            $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
          }

          $post_key_con=" AND 1=1 ";

          if(null !== Input::get('keyword_group'))
          {
             $keyword_data = $this->getprojectkeyword($brand_id,Input::get('keyword_group'));
             if(isset($keyword_data [0]['main_keyword']))
             {
              $post_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")";  
            }
                    
            else
            {
               $post_key_con .= ' AND 1=2';
            }
          }
           $query = "SELECT page_name,count(page_name) as page_count  FROM temp_web_mentions posts WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con . $post_key_con  ." and page_name <> '' and (isHide=0 or isHide is null)  GROUP BY page_name order by page_count DESC " ;
           
           $query_result = DB::select($query);

               $data=[];
               foreach ($query_result as $key => $value) {

                $page_name=$value->page_name;
                $page_count=$value->page_count;
                if (!array_key_exists($page_name , $data)) {
                  $data[$page_name] = array(
                    'page_name' => $page_name,
                    'count' =>   $page_count,
                    
                  );
                }
                else
                {
                  $data[$page_name]['count'] = (int) $data[$page_name]['count'] + (int) $page_count;
                }
              }

              echo json_encode($data);

    }


    public function getMentionSentiDetail()
    {
     //get keywork
     //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
     
       if(null !== Input::get('fday'))
          {
             $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
             $dateBegin=date('Y-m-d');
          }
        

        if(null !==Input::get('sday'))
        {

            $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
    
        else
        {
           $dateEnd=date('Y-m-d');

        }
        // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
        // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));

         // $groupType="month";
         $group_period ="";
         $group_period_post ="";
         $group_period_cmt ="";
         $group_period_article = "";
         $add_con="";
         $add_comment_con="";
        if($groupType=="month")
        {
        $group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
        $group_period_post =" DATE_FORMAT(posts.created_time, '%Y-%m')"; 
        $group_period_cmt =" DATE_FORMAT(cmts.created_time, '%Y-%m')"; 
        $group_period_article = " DATE_FORMAT(article.created_time, '%Y-%m')"; 

         }

           else 
         {
        $group_period =" DATE_FORMAT(created_time, '%Y-%m-%d')";
        $group_period_cmt =" DATE_FORMAT(cmts.created_time, '%Y-%m')";
        $group_period_article =" DATE_FORMAT(article.created_time, '%Y-%m')";    

         }

            $add_inbound_con ="";
        $inboundpages=$this->getInboundPages($brand_id);

        if ($inboundpages !== '')
        {
          $add_inbound_con = " AND (page_name  not in (".$inboundpages.") or page_name is NULL)";
        }
        $excludepages = $this->getExcludePages($brand_id);
        if($excludepages != ""){
          $add_inbound_con .=" AND (page_name  not in (".$excludepages.") or page_name is NULL)";
        }

        $comment_key_con=" AND 1=1 ";
        $post_key_con=" AND 1=1 ";
        $article_key_con=" AND 1=1 ";
         if(null !== Input::get('keyword_group'))
          {
            $keyword_group = Input::get('keyword_group') ;
          }
          else
          {
            $keyword_group = '' ;
          }
         $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);
         if(isset($keyword_data [0]['main_keyword']))
         {
          $comment_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")";
          $post_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")";  
          $article_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'article') . ")";
         }
         
          else
          {

           $comment_key_con .= ' AND 1=2';
           $post_key_con .= ' AND 1=2';
           $article_key_con .= ' AND 1=2';
          }
    if(Input::get('keyword_group') == 'KBZPay'){
        if($groupType == 'null')
        {
          $query =" SELECT  sum(positive) positive,sum(negative) negative From ( SELECT COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative  ".
          "FROM temp_comments_kbzpay cmts  WHERE ".
          " (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(posts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(posts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(posts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(posts.checked_sentiment = 'neg', 1, 0)),0) negative ".
          "FROM temp_posts_kbzpay posts WHERE ".
          "  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .
          "SELECT COALESCE(sum(IF(article.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(article.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(article.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(article.checked_sentiment = 'neg', 1, 0)),0) negative ".
          "FROM temp_web_mentions_kbzpay article WHERE ".
          "  (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .
           " ) T1 ";

        }else{

         $query =" SELECT  sum(positive) positive,sum(negative) negative,created_time From ( SELECT COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period_cmt ." created_time  ".
          "FROM temp_comments_kbzpay cmts  WHERE ".
          "  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .
          " GROUP BY ".$group_period_cmt .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(posts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(posts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(posts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(posts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period_post ." created_time  ".
          "FROM temp_posts_kbzpay posts WHERE ".
          "  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(article.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(article.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(article.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(article.checked_sentiment = 'neg', 1, 0)),0) negative, ".$group_period_article ." created_time  ".
          "FROM temp_web_mentions_kbzpay article WHERE ".
          "  (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .
          " GROUP BY ".$group_period . 
          " ) T1 Group By created_time";
        }
      }else{

          if($groupType == 'null')
            {
              $query =" SELECT  sum(positive) positive,sum(negative) negative From ( SELECT COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative  ".
              "FROM temp_comments cmts  WHERE cmts.isHide=0 and ".
              " (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con .$comment_key_con .
              " UNION ALL ".
              "SELECT COALESCE(sum(IF(posts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(posts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(posts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(posts.checked_sentiment = 'neg', 1, 0)),0) negative ".
              "FROM temp_posts posts WHERE posts.isHide=0 and  ".
              "  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con . $post_key_con.
              " UNION ALL ".
              "SELECT COALESCE(sum(IF(article.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(article.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(article.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(article.checked_sentiment = 'neg', 1, 0)),0) negative ".
              "FROM temp_web_mentions article WHERE article.isHide=0 and  ".
              "  (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $article_key_con.
               " ) T1 ";
            }
        else
        {
         $query =" SELECT  sum(positive) positive,sum(negative) negative,created_time From ( SELECT COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period_cmt ." created_time  ".
          "FROM temp_comments cmts  WHERE cmts.isHide=0 and ".
          "  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con .$comment_key_con .
          " GROUP BY ".$group_period_cmt .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(posts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(posts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(posts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(posts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period_post ." created_time  ".
          "FROM temp_posts posts WHERE posts.isHide=0 and  ".
          "  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con . $post_key_con.
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(article.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(article.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(article.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(article.checked_sentiment = 'neg', 1, 0)),0) negative, ".$group_period_article ." created_time  ".
          "FROM temp_web_mentions article WHERE article.isHide=0 and  ".
          "  (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $article_key_con.
          " GROUP BY ".$group_period . 
          " ) T1 Group By created_time";
        }

      }

        $query_result = DB::select($query);
        // print_r($query_result);
         //return;
           $data = [];
           $total=0;

           $data_sentiment = [];
           $data_mention = [];
           $total=0;
           $data_message = [];

            foreach ($query_result as  $key => $row) {
                  
                      $utcdatetime = $row->created_time;
                 
                       
                       $pos =$row->positive;
                       $neg =$row->negative;
                     //  $neutral =(int)$row->neutral + (int)$row->NA;
                               
                        $request['periodLabel'] ="";
              
                        if($groupType == "week")
        {
            $weekperiod=$this ->rangeWeek ($utcdatetime);
            $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
            
         if (!array_key_exists($periodLabel , $data_sentiment)) {
            $data_sentiment[$periodLabel] = array(
                        'periodLabel' => $periodLabel,
                        'positive' => $pos,
                        'negative' => $neg,
                        'periods' =>$utcdatetime,
                    );
        }
        else
        {
         $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
         $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
         $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
        }
         
                          


        }
         else
        {
         $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
         $data_sentiment[$utcdatetime]['positive'] = $pos;
         $data_sentiment[$utcdatetime]['negative'] = $neg;
         $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

        }

                        
           }

         //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
            usort($data_sentiment, function($a1, $a2) {
           $value1 = strtotime($a1['periods']);
           $value2 = strtotime($a2['periods']);
           return $value1 - $value2;
        });
            echo json_encode($data_sentiment);

    }
    public function getMentionStatus()
    {
         $data = array();
         $brand_id=Input::get('brand_id');


         if(null !== Input::get('fday'))
          {
             $date_Week_Begin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
             $date_Week_Begin=date('Y-m-d');
          }
        

          if(null !==Input::get('sday'))
          {

             $date_End=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          
          else
          {
             $date_End=date('Y-m-d');

          }

         $add_inbound_con ="";
         $inboundpages=$this->getInboundPages($brand_id);

          if ($inboundpages !== '')
          {
            $add_inbound_con = " AND (page_name  not in (".$inboundpages.") or page_name is NULL)";
          }
          $excludepages = $this->getExcludePages($brand_id);
          if($excludepages != ""){
            $add_inbound_con .=" and (page_name  not in (".$excludepages.") or page_name is NULL)";
          }
          $comment_key_con=" AND 1=1 ";
          $post_key_con=" AND 1=1 ";
          $article_key_con=" AND 1=1 ";
         if(null !== Input::get('keyword_group'))
            {
              $keyword_group = Input::get('keyword_group') ;
            }
            else
            {
              $keyword_group = '' ;
            }
               $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);
               if(isset($keyword_data [0]['main_keyword']))
               {
                // dd($keyword_data);
                $comment_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")";
                $post_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 
                $article_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'article') . ")"; 
               }
               
              else
              {

               $comment_key_con .= ' AND 1=2';
               $post_key_con .= ' AND 1=2';
              }

            if( Input::get('keyword_group') == "KBZPay"){
         
              $query_comment = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
                "FROM temp_comments_kbzpay cmts  WHERE ".
                "  (DATE(cmts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
                
              $query_comment_result = DB::select($query_comment);

              $query_post ="SELECT count(*) total,sum(IF(posts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(posts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(posts.checked_sentiment = 'neutral' OR  posts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_posts_kbzpay posts ".
              " WHERE (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') ";

              $query_post_result = DB::select($query_post);

               $query_article ="SELECT count(*) article_total,count(Distinct page_name) As author_total,sum(IF(article.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(article.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(article.checked_sentiment = 'neutral' OR  article.checked_sentiment ='NA' , 1, 0)) neutral FROM temp_web_mentions_kbzpay  article".
              " WHERE article.isHide=0 AND (DATE(created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " ;

              $query_article_result = DB::select($query_article);

          }else{


            $query_comment = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
              "FROM temp_comments cmts  WHERE cmts.isHide=0 and ".
              "  (DATE(cmts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')" . $add_inbound_con . $comment_key_con ;

            $query_comment_result = DB::select($query_comment);

            $query_post ="SELECT count(*) total,sum(IF(posts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(posts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(posts.checked_sentiment = 'neutral' OR  posts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_posts posts ".
            " WHERE posts.isHide=0 and  (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " . $add_inbound_con . $post_key_con;

            $query_post_result = DB::select($query_post);

             $query_article ="SELECT count(*) article_total,count(Distinct page_name) As author_total,sum(IF(article.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(article.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(article.checked_sentiment = 'neutral' OR  article.checked_sentiment ='NA' , 1, 0)) neutral FROM temp_web_mentions  article".
            " WHERE (article.isHide=0 or article.isHide is null) AND (DATE(created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " .  $article_key_con;

            $query_article_result = DB::select($query_article);

          }

          echo json_encode(array($query_comment_result,$query_post_result,$query_article_result));

    }
    public function setMentionPostPredict()
    {
          $post_id = Input::post('id');

          $brand_id = Input::post('brand_id');
          $sentiment = Input::post('sentiment');
        
          $username = Auth::user()->username;
          $decided_keyword = Input::post('decided_keyword');
          if($decided_keyword == Null)
          {
            $decided_keyword = "";
          }
          $manual_date = new \DateTime("now", new \DateTimeZone('Asia/Rangoon') );
          $manual_date = $manual_date->format('Y-m-d H:i:s');
    

         $result =DB::table('temp_posts')
       
        ->where('id', $post_id)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('checked_sentiment' => $sentiment,'change_predict'=>1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));  // update the record in the DB. 

           $row = DB::table('predict_logs') //check row exist
                ->where('id', $post_id) 
                ->where('type','mention_post')
                ->limit(1)
                ->get()->toArray();
              
              if(!empty($row))
              {
                  DB::table('predict_logs')->where('id',$post_id)->where('type','mention_post')
                    ->update(array('checked_sentiment' => $sentiment,'change_predict' => 1,'decided_keyword'=>$decided_keyword,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));
              }
              else
              {
                // dd("data not exist");
                  DB::table('predict_logs')
                    ->insert(array('id' => $post_id,'type' => 'mention_post','checked_sentiment' => $sentiment,'change_predict' => 1,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
              }
               

         
            return $result;
    }
    public function setArticlePredict()
    {

        $post_id = Input::post('id');
        $brand_id = Input::post('brand_id');
        $sentiment = Input::post('sentiment');
        $username = Auth::user()->username;
        $manual_date = new \DateTime("now", new \DateTimeZone('Asia/Rangoon') );
        $manual_date = $manual_date->format('Y-m-d H:i:s');

        $result =DB::table('temp_web_mentions')
        ->where('id', $post_id) 
        ->limit(1) 
        ->update(array('checked_sentiment' => $sentiment,'change_predict'=>1,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString())); 

        $row = DB::table('predict_logs') //check row exist
                ->where('id', $post_id) 
                ->where('type','web_mention')
                ->limit(1)
                ->get()->toArray();
              
              if(!empty($row))
              {
                  DB::table('predict_logs')->where('id',$post_id)->where('type','web_mention')
                    ->update(array('checked_sentiment' => $sentiment,'change_predict' => 1,'manual_date'=>$manual_date,'manual_by'=>$username,'updated_at'=>now()->toDateTimeString()));
              }
              else
              {
                // dd("data not exist");
                  DB::table('predict_logs')
                    ->insert(array('id' => $post_id,'type' => 'web_mention','checked_sentiment' => $sentiment,'change_predict' => 1,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
              }
        // dd($result);
        return $result;
     }

    public function getsentimentdetail()
    {
         //get keywork
         //  $keyword_data = ProjectKeyword::find(1);//projectid
          $brand_id=Input::get('brand_id');
          $groupType=Input::get('periodType');
          $filter_keyword ='';
          if(null !== Input::get('keyword'))
           $filter_keyword=Input::get('keyword'); 

          /*$brand_id=59;*/
          /*$groupType='month';*/
          if(null !== Input::get('fday'))
          {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
            $dateBegin=date('Y-m-d');
          }
        

          if(null !==Input::get('sday'))
          {

              $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          
          else
          {
              $dateEnd=date('Y-m-d');

          }
          /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
          $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));*/

           // $groupType="month";
           $group_period ="";
           $add_con="";
           $add_comment_con="";
          if($groupType=="month")
          {
          $group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
           }

             else 
           {
          $group_period =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
                        

           }
          if($filter_keyword !== '')
          {
            $add_con = " and message Like '%".$filter_keyword."%'";
          }
          $add_inbound_con ="";

          $inboundpages=$this->getInboundPages($brand_id);
          if ($inboundpages !== '')
          {
             $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
            
          }
          $query = "SELECT sum(positive) positive,sum(negative) negative,sum(neutral) neutral,".$group_period." created_time from " .
          " (SELECT IF(sentiment = 'pos', 1, 0)  positive, IF(sentiment = 'neg', 1, 0) negative,".
          " IF(sentiment = 'neutral', 1, 0) neutral,posts.page_name,message, DATE_FORMAT(created_time, '%Y-%m-%d') created_time ".
          " FROM temp_posts  ".
          " UNION ALL ".
          " SELECT IF(cmts.sentiment = 'pos', 1, 0)  positive, IF(cmts.sentiment = 'neg', 1, 0) negative,".
          " IF(cmts.sentiment = 'neutral', 1, 0) neutral,cmts.page_name,cmts.message,DATE_FORMAT(cmts.created_time, '%Y-%m-%d') created_time".
          " FROM temp_comments cmts ) T1 ".
          " WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con.$add_inbound_con.
          "GROUP BY ".$group_period .
          " ORDER BY ".$group_period;

          /*echo $query;
          return;*/

          $query_result = DB::select($query);

             $data = [];
             $total=0;

             $data_sentiment = [];
             $data_mention = [];
             $total=0;
             $data_message = [];

              foreach ($query_result as  $key => $row) {
                    
                        $utcdatetime = $row->created_time;
                   
                         
                         $pos =$row->positive;
                         $neg =$row->negative;
                         $nan =$row->neutral;
                    
                          $request['periodLabel'] ="";
                
                          if($groupType == "week")
          {
              $weekperiod=$this ->rangeWeek ($utcdatetime);
              $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
              
           if (!array_key_exists($periodLabel , $data_sentiment)) {
              $data_sentiment[$periodLabel] = array(
                          'periodLabel' => $periodLabel,
                          'positive' => $pos,
                          'negative' => $neg,
                          'neutral' => $nan,
                          'periods' =>$utcdatetime,
                      );
          }
          else
          {
           $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
           $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
           $data_sentiment[$periodLabel]['neutral'] = $data_sentiment[$periodLabel]['neutral'] + $nan;
           $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
          }
           
                            


          }
           else
          {
           $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
           $data_sentiment[$utcdatetime]['positive'] = $pos;
           $data_sentiment[$utcdatetime]['negative'] = $neg;
           $data_sentiment[$utcdatetime]['neutral'] =  $nan;
           $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

          }

                          
             }

           $data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
              echo json_encode($data_sentiment);

        }
    public function getKeywordsByGroup()
    {
       $brand_id=Input::get('brand_id');

       if(null !== Input::get('campaign_name'))
       {
          $campaign_name=Input::get('campaign_name');
          $keyword_list=[];
          $query="select campaign_keyword from campaignGroup cg join campaignGroupList cgl on cg.id=cgl.campaignGroup_id  where name='" .$campaign_name . "'" ;
          $keyword_data = DB::select($query);
          // dd($keyword_data);
         
        foreach ($keyword_data as $key => $row) {
              $keyword_list [] = $row->campaign_keyword;
                       
                }
       }
       else
       {
       $keyword_group=Input::get('keyword_group');
       // $brand_id = 34;
       $keyword_list=[];
       // $keyword_data = $this->getprojectkeywordCombine($brand_id);
       $query="select main_keyword,require_keyword from project_keywords where project_id=" .$brand_id . " and group_name='".str_replace("'","\'", $keyword_group)."'" ;
          $keyword_data = DB::select($query);
         
        foreach ($keyword_data as $key => $row) {
                          $main_keyword=$row->main_keyword;
                          $keyword_list [] = $main_keyword;
                          $require_keyword=$row->require_keyword;
                          $require_keyword = explode(',', $require_keyword);
                          foreach ($require_keyword as $key => $key_data) {
                            if($key_data <> '')
                            $keyword_list []= $main_keyword . "," .$key_data;
                            # code...
                          }
                       
                }
              }
                array_unique($keyword_list);
              echo json_encode($keyword_list);
    }
    public function getAllKeywords()
    {
       $brand_id=Input::get('brand_id');
       // $brand_id = 34;
       $keyword_list=[];
       $keyword_data = $this->getprojectkeywordCombine($brand_id);
            // dd($keyword_data);
        foreach ($keyword_data as $key => $row) {
                          $main_keyword=$row->main_keyword;
                          $keyword_list [] = $main_keyword;
                          $require_keyword=$row->require_keyword;
                          $require_keyword = explode(',', $require_keyword);
                          foreach ($require_keyword as $key => $key_data) {
                            if($key_data <> '')
                            $keyword_list []= $main_keyword . "," .$key_data;
                          
                          }
                       
         }
         // dd($key)

         array_unique($keyword_list);
         echo json_encode($keyword_list);
    }

    public function getemotiondetail()
        {
          //get keywork
          //  $keyword_data = ProjectKeyword::find(1);//projectid
          $brand_id=Input::get('brand_id');
          $groupType=Input::get('periodType');
          $filter_keyword ='';
          $filter_sentiment='';
          if(null !== Input::get('keyword'))
           $filter_keyword=Input::get('keyword'); 

           if(null !== Input::get('sentiment'))
          $filter_sentiment=Input::get('sentiment'); 

           /*   $brand_id=46;
             $groupType='month';*/
            if(null !== Input::get('fday'))
              {
                $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
              $dateBegin=date('Y-m-d');
              }
            

              if(null !==Input::get('sday'))
                {

             $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
                }
                
                else
                {
            $dateEnd=date('Y-m-d');
                }

            /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
            $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));*/

             // $groupType="month";
             $group_period ="";
             $add_con="";
             $add_comment_con ="";

            if($groupType=="month")
            {
            $group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
             }

               else 
             {
            $group_period =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
                          

             }
            if($filter_keyword !== '')
            {
              $add_con = "  and message Like '%".$filter_keyword."%'";
              $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
            }

            if($filter_sentiment !== '')
            {
              $add_con .= " and sentiment = '".$filter_sentiment."'";
              $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
            }

            $add_inbound_con ="";
            $inboundpages=$this->getInboundPages($brand_id);
            if ($inboundpages !== '')
            {
                $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
            }

            $query = "SELECT sum(anger) anger,sum(interest) interest, ".
            " sum(disgust) disgust,sum(fear) fear,sum(joy) joy,sum(liked) liked,sum(love) love,sum(neutral) neutral,".
            " sum(sadness) sadness,sum(surprise) surprise,sum(trust) trust, "
             .$group_period .  " created_time from (SELECT IF(emotion = 'anger', 1, 0) anger, ".
            " IF(emotion = 'interest', 1, 0) interest, IF(emotion = 'disgust', 1, 0) disgust, ".
            " IF(emotion = 'fear', 1, 0) fear, IF(emotion = 'joy', 1, 0) joy, IF(emotion = 'like', 1, 0) liked, ".
            " IF(emotion = 'love', 1, 0) love, IF(emotion = 'neutral', 1, 0) neutral, IF(emotion = 'sadness', 1, 0) sadness, ".
            " IF(emotion = 'surprise', 1, 0) surprise,IF(emotion = 'trust', 1, 0) trust, ".
            " DATE_FORMAT(created_time, '%Y-%m-%d') created_time,posts.page_name, message,sentiment FROM temp_posts  ". 
            " UNION ALL ".
            " SELECT IF(cmts.emotion = 'anger', 1, 0) anger, IF(cmts.emotion = 'interest', 1, 0) interest, ".
            " IF(cmts.emotion = 'disgust', 1, 0) disgust, IF(cmts.emotion = 'fear', 1, 0) fear, IF(cmts.emotion = 'joy', 1, 0) ".
            " joy, IF(cmts.emotion = 'like', 1, 0) liked, IF(cmts.emotion = 'love', 1, 0) love, ".
            " IF(cmts.emotion = 'neutral', 1, 0)".
            " neutral, IF(cmts.emotion = 'sadness', 1, 0) sadness, IF(cmts.emotion = 'surprise', 1, 0) surprise, ".
            " IF(cmts.emotion = 'trust', 1, 0) trust, DATE_FORMAT(cmts.created_time, '%Y-%m-%d') created_time ,posts.page_name,cmts.message message,cmts.sentiment sentiment FROM ".
            " temp_comments  cmts LEFT JOIN  temp_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NULL) T1 ".
            " WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_con . $add_inbound_con.
            " GROUP BY ".$group_period .
            " ORDER by ".$group_period;



            /*echo $query ;
            return;*/

            $query_result = DB::select($query);

               $data_emotion = [];
               $total=0;

                foreach ($query_result as  $key => $row) {
                      
                            $utcdatetime = $row ->created_time;
                            $request["periods"]=$utcdatetime;
                                
                            $request['anger']=$row ->anger;
                            $request['interest']=$row ->interest;
                            $request['disgust']=$row ->disgust;
                            $request['fear']=$row ->fear;
                            $request['joy']=$row ->joy;
                            $request['like']=$row ->liked;
                            $request['love']=$row ->love;
                            $request['neutral']=$row ->neutral;
                            $request['sadness']=$row ->sadness;
                            $request['surprise']=$row ->surprise;
                            $request['trust']=$row ->trust;
                            $periodLabel ='';

                            if($groupType == "week")
            {
                $weekperiod=$this ->rangeWeek ( $utcdatetime);
               $periodLabel=$weekperiod['start'] . " - " . $weekperiod['end'] ;
              
            }
            else
            {
             
              $periodLabel =$utcdatetime ;
            }

            if (!array_key_exists($periodLabel , $data_emotion)) {
                $data_emotion[$periodLabel] = array(
                            'periodLabel' => $periodLabel,
                            'anger' => $row ->anger,
                            'interest' =>$row ->interest,
                            'disgust' =>$row ->disgust,
                            'fear' => $row ->fear,
                            'joy' =>  $row ->joy,
                            'like' => $row ->liked,
                            'love' =>  $row ->love,
                            'neutral' =>$row ->neutral,
                            'sadness' => $row ->sadness,
                            'surprise' => $row ->surprise,
                            'trust' =>  $row ->trust,
                            'periods' =>  $utcdatetime,
                        );
            }
            else
            {
             $data_emotion[$periodLabel]['anger'] = $data_emotion[$periodLabel]['anger'] +  $row ->anger;
             $data_emotion[$periodLabel]['interest'] = $data_emotion[$periodLabel]['interest'] + $row ->interest;
             $data_emotion[$periodLabel]['disgust'] = $data_emotion[$periodLabel]['disgust'] + $row ->disgust;
             $data_emotion[$periodLabel]['fear'] = $data_emotion[$periodLabel]['fear'] +$row ->fear;
             $data_emotion[$periodLabel]['joy'] = $data_emotion[$periodLabel]['joy'] + $row ->joy;
             $data_emotion[$periodLabel]['like'] = $data_emotion[$periodLabel]['like'] + $row ->liked;
             $data_emotion[$periodLabel]['love'] = $data_emotion[$periodLabel]['love'] +  $row ->love;
             $data_emotion[$periodLabel]['neutral'] = $data_emotion[$periodLabel]['neutral'] + $row ->neutral;
             $data_emotion[$periodLabel]['sadness'] = $data_emotion[$periodLabel]['sadness'] + $row ->sadness;
             $data_emotion[$periodLabel]['surprise'] = $data_emotion[$periodLabel]['surprise'] + $row ->surprise;
             $data_emotion[$periodLabel]['trust'] = $data_emotion[$periodLabel]['trust'] +  $row ->trust;
             $data_emotion[$periodLabel]['periods'] = $utcdatetime;
            }
                      
                          }
                           
            $data_emotion = $this->unique_multidim_array($data_emotion,'periodLabel'); 



                echo json_encode($data_emotion);

        }
public function getinterestdata()
{
      $brand_id=Input::get('brand_id');
 
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      // if(null !== Input::get('sentiment'))
      // $filter_sentiment=Input::get('sentiment'); 

     //  $brand_id=59;
     // $groupType='month';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con ="";
if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}
// if($filter_sentiment !== '')
// {
//   $add_con .= " and sentiment = '".$filter_sentiment."'";
//   $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
// }
  // $table = "temp_".$brand_id."_comments";
  // dd($table);

  $query = "SELECT sum(interest) interest_sum,count(*) comment_count,  DATE_FORMAT(created_time, '".$group_period."') periodLabel  FROM temp_comments WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con .
" GROUP BY DATE_FORMAT(created_time, '".$group_period."')" ;
// echo $query;
// return ;
$query_result = DB::select($query);
// dd($query_result);
   echo json_encode($query_result);

}
        



public function getinteractiondetail()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

      /*$brand_id=49;*/
    /* $groupType='month';*/
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));
*/
 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con ="";

if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}
if($filter_sentiment !== '')
{
  $add_con .= " and sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
}
$add_inbound_con ="";
$add_inbound_cmt_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
    
}


$query = "SELECT sum(Liked) Liked,sum(Love) Love,sum(Haha) Haha,sum(Angry) Angry,sum(Sad) Sad,sum(Wow) Wow,sum( post_count) post_count,  created_time, SUM(replace(shared, ',', '')) shared ".
" FROM (SELECT sum(Liked) Liked,sum(Love) Love,sum(Haha) Haha,sum(Angry) Angry,sum(Sad) Sad,sum(Wow) Wow,count(*)  post_count, DATE_FORMAT(created_time, '".$group_period."') created_time, ".
" SUM(replace(shared, ',', '')) shared FROM temp_posts posts WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con . $add_inbound_con .
" GROUP BY DATE_FORMAT(created_time, '".$group_period."')" .
" UNION ALL ".
"SELECT  '0' Liked,'0' Love,'0' Haha,'0' Angry,'0' Sad,'0' Wow,count(*)  post_count, DATE_FORMAT(cmts.created_time, '".$group_period."') created_time,'0' shared FROM temp_comments cmts ".
" WHERE  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_comment_con . $add_inbound_con .
" GROUP BY DATE_FORMAT(cmts.created_time, '".$group_period."'))T1 " .
" GROUP BY created_time ORDER BY created_time";

/*echo $query ;
return;*/

$query_result = DB::select($query);
// dd($query_result);
   $data = [];
   $total=0;

    foreach ($query_result as  $key => $row) {
          
                           
                /*$datetime = $row ->created_time;

                $request['post_count'] =$row ->post_count;
             
                $request['Wow'] = $row ->Wow;
                $request['Love'] = $row ->Love;
                $request['Angry'] = $row ->Angry;
                $request['Sad'] = $row ->Sad;
                $request['Haha'] = $row ->Haha;
                $request['Like'] = $row ->Liked;
*/
                $datetime = $row ->created_time;
                $periodLabel ="";
                $weekcount ="";
                if($groupType == "week")
                {
                  $weekperiod=$this ->rangeWeek ($datetime);
                  $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
                 if (!array_key_exists($periodLabel , $data)) {
                  $data[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'Wow' =>  $row ->Wow,
                'Love' => $row ->Love,
                'Angry' => $row ->Angry,
                'Sad' => $row ->Sad,
                'Haha' => $row ->Haha,
                'Like' =>$row ->Liked,
                'periods' => $periodLabel,
                'shared' =>$row ->shared,
                'post_count' =>$row ->post_count,
            );
                                                                }
                else
                {
$data[$periodLabel]['Wow'] = (int) $data[$periodLabel]['Wow'] + (int) $row ->Wow;
$data[$periodLabel]['Love'] = (int) $data[$periodLabel]['Love'] + (int)$row ->Love;
$data[$periodLabel]['Angry'] = (int) $data[$periodLabel]['Angry'] +(int) $row ->Angry;
$data[$periodLabel]['Sad'] = (int) $data[$periodLabel]['Sad'] + (int) $row ->Sad;
$data[$periodLabel]['Haha'] = (int) $data[$periodLabel]['Haha'] + (int) $row ->Haha;
$data[$periodLabel]['Like'] = (int) $data[$periodLabel]['Like'] + (int) $row ->Liked;
$data[$periodLabel]['periods'] =  $datetime;
$data[$periodLabel]['shared'] =(int) $data[$periodLabel]['shared'] +(int)$row ->shared;
$data[$periodLabel]['post_count'] =(int) $data[$periodLabel]['post_count'] +(int)$row ->post_count;

                }
   
 
                }
    else 
    {
                 $data[$datetime]['Wow'] = $row ->Wow;
                 $data[$datetime]['Love'] =$row ->Love;
                 $data[$datetime]['Angry'] =$row ->Angry;
                 $data[$datetime]['Sad'] = $row ->Sad;
                 $data[$datetime]['Haha'] =$row ->Haha;
                 $data[$datetime]['Like'] =$row ->Liked;
                 $data[$datetime]['periods'] =  $datetime;
                 $data[$datetime]['periodLabel'] =  $datetime;
                 $data[$datetime]['shared'] =$row ->shared;
                 $data[$datetime]['post_count'] =$row ->post_count;
    }
                
              
                 
               
            }


$data = $this->unique_multidim_array($data,'periodLabel'); 
 

    echo json_encode($data);


}



public function getpopularnegative()

{
  //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
         
    /*  $brand_id=6;*/
 
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}
 $query = "SELECT  (sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry)) ".
 " total,posts.id,message,posts.page_name,link,full_picture,sentiment,emotion,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p')".
 " created_time,CAST(followers.fan_count AS UNSIGNED) +SUM(replace(shared, ',', '')) each_influencer  FROM  ".
 " temp_posts posts inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
 " WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inbound_con .
 " and sentiment='neg'  GROUP BY DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p'),posts.id,message,page_name,link,full_picture,sentiment,emotion,fan_count ".
 " ORDER by total,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p') DESC LIMIT 10 ";

$query_result = DB::select($query);
$data = [];
 $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];

 foreach ($query_result as  $key => $row) {

              $request["message"] = $row ->message;
              $request["page_name"] = $row ->page_name;
              $request["created_time"] =date('d-m-Y H:m:s', strtotime($row ->created_time));
              if(isset($row ->link))
              $request["link"] =$row ->link;
            else
              $request["link"] ='#';

             if(isset($row ->full_picture))
              $request["full_picture"] =$row ->full_picture;
            else
              $request["full_picture"] ='';

              $request["sentiment"] =$row ->sentiment;
              $request["emotion"] =$row ->emotion;
              $request["id"] =$row ->id;
              $request["edit_permission"]=$edit_permission;
              $data[] = $request;

            }
  echo json_encode($data);

}

public function getallmention()
    {

    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      /*$brand_id=59;*/
    /*  $groupType=Input::get('periodType');*/
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

      /*$brand_id=30;*/
 
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2017 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2017 06 30')));*/

$add_con='';
if($filter_keyword !== '')
  $add_con = " and posts.message Like '%".$filter_keyword."%'";

if($filter_sentiment !== '')
{
  $add_con .= " and posts.sentiment = '".$filter_sentiment."'";
}

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query = "SELECT  posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_posts posts LEFT JOIN temp_comments cmts on posts.id=cmts.post_id ".
" WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_con . $add_inbound_con .
" ORDER by DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') DESC";

/*echo $query;
return;*/
$query_result = DB::select($query);
     
 /*print_r($query_result);
 return;*/
 $data = [];
 $data_mention=[];
 foreach ($query_result as  $key => $row) {
   $pos =($row->cmt_sentiment=='pos')?1:0 ;$neg=($row->cmt_sentiment=='neg')?1:0;
   $anger =($row->cmt_emotion=='anger')?1:0 ; $interest=($row->cmt_emotion=='interest')?1:0;
   $disgust=($row->cmt_emotion=='disgust')?1:0;$fear=($row->cmt_emotion=='fear')?1:0;
   $joy=($row->cmt_emotion=='joy')?1:0;$like=($row->cmt_emotion=='like')?1:0;
   $love=($row->cmt_emotion=='love')?1:0;$neutral=($row->cmt_emotion=='neutral')?1:0;
   $sadness=($row->cmt_emotion=='sadness')?1:0;$surprise=($row->cmt_emotion=='surprise')?1:0;
   $trust=($row->cmt_emotion=='trust')?1:0;
   $message=$this->readmore($row->message);
   //$message=$row->message;
            if (isset($row ->id))
   {
                 $id= $row ->id;
   
    if (!array_key_exists($id , $data)) {
    $data[$id] = array(
                'id'     =>$row->id,
                'message' => $message,
                'page_name' =>$row->page_name,
                'created_time' =>$row->created_time,
                'link' =>$row->link,
                'full_picture' =>$row->full_picture,
                'sentiment' =>$row->sentiment,
                'emotion' =>$row->emotion,
                'positive'=> $pos,'negative'=>$neg,
                'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                'isBookMark'=>$row->isBookMark
            );
}
else
{
 $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
 $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
 $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
 $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
 $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
 $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
 $data[$id]['like'] = (int) $data[$id]['like'] + $like;
 $data[$id]['love'] = (int) $data[$id]['love'] + $love;
 $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
 $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
 $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
 $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;



}
//echo $data[$id]['negative'];echo $neg;return;

}
 
            }
            $data_mention=$data;

/*$id='799069593563429_1007801336023586';
echo $data[$id]['negative'];
 print_r($data_mention);
 return;
 $data_mention = $this->unique_multidim_array($data_mention,'id'); */

$permission_data = $this->getPermission();
 return Datatables::of($data_mention)
       ->addColumn('action', function ($data_mention) {
                return '<a href="javascript:void(0)" id="'.$data_mention['id'].'" name="'.$data_mention['isBookMark'].'" class="mdi '.$data_mention['isBookMark'].' text-yellow btn-bookmark" style="float:right;color:#f39c12"></a>';
            }) 

       ->addColumn('post_div', function ($data_mention) use($permission_data) {
               $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left"> <img src="'.$data_mention['full_picture'].'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                    <div class="sl-right"> <div><a href="'.$data_mention['link'].'" class="link"  target="_blank">'.$data_mention['page_name'].'</a>
              <span class="sl-date">'.$data_mention['created_time'].'  </span>

               <p>Sentiment : <select class="form-control custom-select sentiment-color customize-select" id="sentiment_'.$data_mention['id'].'"  style="max-width:13%;color:#dd4b39;height: 20px "><option value="pos"';
                if($data_mention['sentiment'] == "pos")
                $html.= 'selected="selected"';
                $html.= '>pos</option><option value="neg"';
                if($data_mention['sentiment'] == "neg")
                $html.= 'selected="selected"';
                $html.= '>neg</option><option value="neutral"';
                if($data_mention['sentiment'] == "neutral")
                $html.= 'selected="selected"';     
                $html.= '>neutral</option><option value="NA"';
                if($data_mention['sentiment'] == "NA")
                $html.= 'selected="selected"';  
                $html.= '>NA</option></select>';

                $html.= ' | Emotion : <select class="form-control custom-select emotion-color customize-select" id="emotion_'.$data_mention['id'].'" style="max-width:13%;color:#55acee;height: 20px"><option value="anger"';
                if($data_mention['emotion'] == "anger")
                $html.= 'selected="selected"';
                $html.= '>anger</option><option value="interest"';
                if($data_mention['emotion'] == "interest")
                $html.= 'selected="selected"';     
                $html.= '>interest</option><option value="disgust"';
                if($data_mention['emotion'] == "disgust")
                $html.= 'selected="selected"';  
                $html.= '>disgust</option><option value="fear"';
                if($data_mention['emotion'] == "fear")
                $html.= 'selected="selected"'; 
                $html.= '>fear</option><option value="joy"';
                if($data_mention['emotion'] == "joy")
                $html.= 'selected="selected"'; 
                $html.= '>joy</option><option value="like"';
                if($data_mention['emotion'] == "like")
                $html.= 'selected="selected"'; 
                $html.= '>like</option><option value="love"';
                if($data_mention['emotion'] == "love")
                $html.= 'selected="selected"'; 
                $html.= '>love</option><option value="neutral"';
                if($data_mention['emotion'] == "neutral")
                $html.= 'selected="selected"';
                $html.= '>neutral</option><option value="sadness"';
                if($data_mention['emotion'] == "sadness")
                $html.= 'selected="selected"';
                $html.= '>sadness</option><option value="surprise"';
                if($data_mention['emotion'] == "surprise")
                $html.= 'selected="selected"';
                $html.= '>sadness</option><option value="trust"';
                if($data_mention['emotion'] == "trust")
                $html.= 'selected="selected"';
                $html.= '>trust</option><option value="NA"';
                if($data_mention['emotion'] == "NA")
                $html.= 'selected="selected"';
                $html.= '>NA</option></select>';
                
                if($permission_data['edit'] === 1)
                $html.= ' <a class="edit_post_predict" id="'.$data_mention['id'].'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>';
                                                    
                                                     
                $html.=' </span></p>

                                    <p class="m-t-10">'.$data_mention['message'].'...<a href="'.$data_mention['link'].'" target="_blank"> Read More
                                      </a></p></div></div>
                                      </div></div>';
    if($data_mention['positive']>0 || $data_mention['negative']>0 || $data_mention['anger']>0  || $data_mention['interest']>0 || $data_mention['disgust']>0 || $data_mention['fear']>0 || $data_mention['joy']>0 || $data_mention['like']>0 || $data_mention['love']>0 || $data_mention['neutral']>0 || $data_mention['sadness']>0 || $data_mention['surprise']>0|| $data_mention['trust']>0){                                   
   $html.= '<div><div style="margin:0 0 0 70px;"> <form>
    <fieldset>
        <legend>Customer Feedback</legend>';
      }
             if($data_mention['positive']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39:padding:5px" class="popup" id="'.$data_mention['id'].'" name="Positive">positive : </a> 
                  <span class="label label-light-danger">'.$data_mention['positive'].'</span> ';
                  
         } 
             if($data_mention['negative']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39" class="popup" id="'.$data_mention['id'].'" name="Negative">negative : </a>
                <span class="label label-light-danger" style="font-size:15px">'.$data_mention['negative'].'</span> ';
                  
         } 
        
                if($data_mention['anger']>0){  
          $html.='<a href="javascript:void(0)" style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="anger">anger : </a> 
                  <span class="label label-light-danger">'.$data_mention['anger'].'</span> ';                 
               }
                if($data_mention['interest']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="interest">interest : </a> 
                  <span class="label label-light-danger">'.$data_mention['interest'].'</span> ';                 
               }
                if($data_mention['disgust']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="disgust">disgust : </a> 
                  <span class="label label-light-danger">'.$data_mention['disgust'].'</span> ';                 
               }
                if($data_mention['fear']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="fear">fear : </a> 
                  <span class="label label-light-danger">'.$data_mention['fear'].'</span> ';                 
               }
                if($data_mention['joy']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="joy">joy : </a> 
                  <span class="label label-light-danger">'.$data_mention['joy'].'</span> ';                 
               }
                if($data_mention['like']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="like">like : </a> 
                  <span class="label label-light-danger">'.$data_mention['like'].'</span> ';                 
               }
                if($data_mention['love']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="love">love : </a> 
                  <span class="label label-light-danger">'.$data_mention['love'].'</span> ';                 
               }
                if($data_mention['neutral']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="neutral">neutral : </a> 
                  <span class="label label-light-danger">'.$data_mention['neutral'].'</span> ';                 
               }
                if($data_mention['sadness']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="sadness">sadness : </a> 
                  <span class="label label-light-danger">'.$data_mention['sadness'].'</span> ';                 
               }
                if($data_mention['surprise']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="surprise">surprise : </a> 
                  <span class="label label-light-danger">'.$data_mention['surprise'].'</span> ';                 
               }
                  if($data_mention['trust']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="trust">trust : </a> 
                  <span class="label label-light-danger">'.$data_mention['trust'].'</span></fieldset>
</form></div></div>';                 
               }
                return $html;
               

            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);

          //  echo json_encode($data);
    }
    public function getcomments()
    {
       //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $post_id=Input::get('id');
      $brand_id=Input::get('brand_id');
      $cmt_type=Input::get('cmt_type');


/*      $post_id='308814943257921_308831799922902';
      $brand_id=59;
      $cmt_type='pos';*/

      if($cmt_type == 'Positive')
      {
$cmt_type ='pos';
      }
      if($cmt_type =="Negative")
      {
      	$cmt_type='neg';
      }
      $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];


        $query="SELECT id,".$edit_permission." as edit_permission,message,sentiment,emotion,DATE_FORMAT(created_time, '%d-%m-%Y %h:%i:%s %p') created_time ".
        " from temp_comments where post_id='".$post_id."' and (sentiment='".$cmt_type."' or emotion='".$cmt_type."') ORDER BY DATE_FORMAT(created_time, '%d-%m-%Y %h:%i:%s %p') DESC";

       /* echo $query;
        return;*/
        $data = DB::select($query);
       /* echo count($data);*/
        echo json_encode($data);
    }

    public function getallmentioncomment()
    {

    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
   
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment ='';

      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

     /* $brand_id=30;*/
 
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

$add_con='';
if($filter_keyword !== '')
  $add_con = " and cmts.message Like '%".$filter_keyword."%'";

if($filter_sentiment !== '')

  $add_con .= " and cmts.sentiment = '".$filter_sentiment."'";
 
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
   $add_inbound_con = " and (cmts.page_name  not in (".$inboundpages.") or cmts.page_name is NULL)";
}

$query="SELECT cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.sentiment,cmts.emotion,(CASE WHEN cmts.isBookMark = 0 THEN 'mdi-star-outline' ELSE 'mdi-star' END) isBookMark ".
" FROM temp_comments cmts ".
" Where (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_con .  $add_inbound_con . 

" ORDER by DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p')";

// echo $query;
// return;
$data = DB::select($query);
/*     
 print_r($data);
 return;*/
$permission_data = $this->getPermission(); 
 return Datatables::of($data)
       ->addColumn('action', function ($data) {
                return '<a href="javascript:void(0)" id="'.$data->id.'" name="'.$data->isBookMark.'" class="mdi '.$data->isBookMark.' text-yellow btn-bookmark-comment" style="float:right;color:#f39c12"></a>';
            }) 

       ->addColumn('post_div', function ($data) use($permission_data){
                $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left user-img"> <span class="round">A</span> </div> 
                                    <div class="sl-right"> <div><a href="javascript:void(0)" class="link">Comment</a>
              <span class="sl-date">'.$data->created_time.'</span>
<p>Sentiment : <select class="form-control custom-select sentiment-color customize-select" id="sentiment_'.$data->id.'"  >';
                 if($data->sentiment == "")
                $html.= '<option value="" selected="selected"></option>';
                $html.='<option value="pos"';
                if($data->sentiment == "pos")
                $html.= 'selected="selected"';
                $html.= '>pos</option><option value="neg"';
                if($data->sentiment == "neg")
                $html.= 'selected="selected"';     
                $html.= '>neg</option><option value="neutral"';
                if($data->sentiment == "neutral")
                $html.= 'selected="selected"';  
                $html.= '>neutral</option></select>';

                $html.= ' | Emotion : <select class="form-control custom-select emotion-color customize-select" id="emotion_'.$data->id.'" >';
                if($data->emotion == "")
                $html.= '<option value="" selected="selected"></option>';
                $html.='<option value="anger"';
                if($data->emotion == "anger")
                $html.= 'selected="selected"';
                $html.= '>anger</option><option value="interest"';
                if($data->emotion == "interest")
                $html.= 'selected="selected"';     
                $html.= '>interest</option><option value="disgust"';
                if($data->emotion == "disgust")
                $html.= 'selected="selected"';  
                $html.= '>disgust</option><option value="fear"';
                if($data->emotion == "fear")
                $html.= 'selected="selected"'; 
                $html.= '>fear</option><option value="joy"';
                if($data->emotion == "joy")
                $html.= 'selected="selected"'; 
                $html.= '>joy</option><option value="like"';
                if($data->emotion == "like")
                $html.= 'selected="selected"'; 
                $html.= '>like</option><option value="love"';
                if($data->emotion == "love")
                $html.= 'selected="selected"'; 
                $html.= '>love</option><option value="neutral"';
                if($data->emotion == "neutral")
                $html.= 'selected="selected"';
                $html.= '>neutral</option><option value="sadness"';
                if($data->emotion == "sadness")
                $html.= 'selected="selected"';
                $html.= '>sadness</option><option value="surprise"';
                if($data->emotion == "surprise")
                $html.= 'selected="selected"';
               $html.= '>surprise</option><option value="trust"';
                if($data->emotion == "trust")
                $html.= 'selected="selected"';
                $html.= '>trust</option><option value="NA"';
                if($data->emotion == "NA")
                $html.= 'selected="selected"';
                $html.= '>NA</option></select>';
                if($permission_data['edit'] === 1)
                $html.= ' <a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>';
                                                    
                                                     
                $html.='</span></p>

                                    <p class="m-t-10">'.$data->message.'</p></div></div></div></div>';
                  return $html;
           
            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);

          //  echo json_encode($data);
    }


    public function getallmentionwithcomment()
    {

         //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
         $brand_id=Input::get('brand_id');
         $filter_keyword='';
      if(null !== Input::get('keyword'))
        $filter_keyword=Input::get('keyword');
         $brand_id=16;
       $keyword_data = $this->getprojectkeyword($brand_id);
      
      $filter['$or'] = $this->getkeywordfilter($keyword_data);

        if(null !== Input::get('fday'))
            {
                   $dateBegin =new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('fday')))* 1000);

            }
      
           else
           {
                $dateBegin =new MongoDB\BSON\UTCDateTime(\Carbon\Carbon::now()->timestamp * 1000);
           }
    

    if(null !==Input::get('sday'))
    {

        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('sday')))* 1000);
    }
    
    else
    {
               $dateEnd =new MongoDB\BSON\UTCDateTime( \Carbon\Carbon::now()->timestamp * 1000);

           }

        
        $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2017 01 08'))* 1000);
        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 08 08'))* 1000);

     

  


 $query_result=MongodbData::raw(function ($collection) use($dateBegin,$dateEnd,$filter,$filter_keyword) {
    return $collection->aggregate([

           
             [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             $filter,
             ['message' => new MongoDB\BSON\Regex(".*" . $filter_keyword,'i' )],


          
]

        ]  
                   
                         ]

                         ,
               [
        '$lookup'=>[
            'from'=>'comments',
            'localField'=>'id',
            'foreignField'=>'post_id',
            'as'=>'comments'
        ]
   ],
   
  /* [
      '$match' => ['pages' => ['$ne'=> [] ]]
   ],*/
    [ '$unwind' => '$comments' ] ,/*,
                          [
            '$sort' =>['created_time'=>-1]


            ]*/
      
        
      
        

             


    ]);
})->toArray();
print_r($query_result);
return ;
$data = [];
$data_message = [];


foreach ($query_result as $row) {// print_r($row);
    //echo($row["name"]);return;
             /*   $test = $row["name"];
                echo  $test;
                return ;*/
              
                $request["message"] = $row["message"];
                if(isset($row["page_name"]))
                $request["page_name"] = $row["page_name"];
            else
                 $request["page_name"] ="Unknown";

                /* $timestamp = $row["created_time"] * 1000;*/
               //  $timestamp = 1453939200 * 1000;
                 $utcdatetime = $row["created_time"];

                  $datetime = $utcdatetime->toDateTime();

                 $request["created_time"] =$datetime->format('d-M-Y h:i:s A');
                 if(isset($row["link"]))
                $request["link"] =$row["link"];
              else
                $request["link"] ="";
               if(isset($row["full_picture"]))
                $request["full_picture"] =$row["full_picture"];
              else
                $request["full_picture"]="";

                $request["sentiment"] ="";
                $request["emotion"] ="";

                if(isset($row["isBookMark"]) && $row["isBookMark"] === true)
                $request["isBookMark"] ='mdi-star';
                else
                $request["isBookMark"]='mdi-star-outline';

                $request["id"]=$row["id"];
                $data[] = $request;

                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row["message"]);;
            }
/*dd(count($data_message));*/


    }

public function getsentimentbycategory()
    {
   //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');

      /*$brand_id=59;
*/
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));*/

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query = "select category, sum(IF(sentiment = 'pos', 1, 0)) positive,sum(IF(sentiment = 'neg', 1, 0)) negative , ".
" sum(IF(sentiment = 'pos', 1, 0)) + sum(IF(sentiment = 'neg', 1, 0)) total ".
" from temp_posts posts inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name  ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')  ". $add_inbound_con.
" GROUP BY category ".
" ORDER BY  total DESC ";

/*echo $query;
return;*/


$query_result = DB::select($query);


/*
    print_r($data);
    return;*/
      echo json_encode($query_result);

}
public function getnetpromotorscore()
    {

    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');

      /*$brand_id=2;*/

 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query = "select posts.page_name, sum(IF(sentiment = 'pos', 1, 0)) positive,sum(IF(sentiment = 'neg', 1, 0)) negative , ".
" sum(IF(sentiment = 'pos', 1, 0)) - sum(IF(sentiment = 'neg', 1, 0)) netscore ".
" from temp_posts posts inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name  ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con .
" GROUP BY posts.page_name ".
" ORDER BY  netscore DESC Limit 20";

/*echo $query;
return;*/

$query_result = DB::select($query);

      echo json_encode($query_result);

}

public function getInfluencerProfile()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
        $brand_id=Input::get('brand_id');
   
       /*$brand_id=6;*/
        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query="SELECT posts.page_name page_name,ANY_VALUE(full_picture) full_picture,fan_count,SUM(replace(shared, ',', '')) shared,".
" CAST(fan_count AS UNSIGNED)+SUM(replace(shared, ',', '')) total ".
" FROM temp_posts posts inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
" inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name".
" where profile='1'" .$add_inbound_con . " group by page_name,fan_count ".
" order by total DESC ".
" Limit 10 ";
/*echo $query;*/
$query_result = DB::select($query);
  
echo json_encode($query_result);

}

public function getInfluencerPage()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
        $brand_id=Input::get('brand_id');
   
       /*$brand_id=6;*/
        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query="SELECT posts.page_name page_name,ANY_VALUE(full_picture) full_picture,fan_count,SUM(replace(shared, ',', '')) shared,".
" CAST(fan_count AS UNSIGNED)+SUM(replace(shared, ',', '')) total ".
" FROM temp_posts posts inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
" inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name".
" where profile='0' ". $add_inbound_con . " group by page_name,fan_count ". $add_inbound_con .
" order by total DESC ".
" Limit 10 ";
/*echo $query;*/
$query_result = DB::select($query);
  
echo json_encode($query_result);

}


 public function insert_Posts_Data()
    {
     
     $keyword_data = $this->getprojectkeyword(81);
       /*  $key_data_count = count($keyword_data);*/
     $filter['$or'] = $this->getkeywordfilter($keyword_data);
    $query_result=MongodbData::raw(function ($collection) use($filter) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             $filter
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
/*dd($query_result);*/
$data=[];
 $id_array=[];

 foreach ($query_result as  $key => $row) {
                $id ='';
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';
                $page_name ='';
                $share =0;
                $reaction ='';
             

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                if(isset($row['message'])) $message = preg_replace('/(\r\n|\r|\n)+/', " ",$row['message']);
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$id_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'page_name' =>$page_name,
                    'share' =>$share,
                    'reaction' =>$reaction,
                    'created_time' =>$datetime,
                    'isBookMark' =>0,
                   ];                 

  }
  
$result = DB::table('temp_81_posts')->insert($data);

$follower_result=MongoFollowers::raw(function ($collection) use($filter,$id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $id_array ] ] 
            
      ]

        ]  
           ]
    
    ]);
})->toArray();

 $data_followers=[];
 foreach ($follower_result as  $key => $row) {
                $id ='';
                $page_name ='';
                $fan_count ='';
                $date ='';
             
   
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['fan_count'])) $fan_count = $row['fan_count'];
                if(isset($row['date'])) $date = $row['date'];
                if(isset($row['reaction'])) $reaction =$row['reaction'];
          
                 $data_followers[] =[
                    'id' => $id,
                    'page_name' =>$page_name,
                    'fan_count' => $fan_count,
                    'date' =>$date
                    ];                 

  }
$result = DB::table('temp_81_followers')->insert($data_followers);

 $pages_result=MongoPages::raw(function ($collection) use($filter,$id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $id_array ] ] 
            
      ]

        ]  
           ]
    
    ]);
})->toArray();
 $data_pages=[];
 foreach ($pages_result as  $key => $row) {
                $page_name ='';
                $about ='';
                $name ='';
                $category ='';
                $sub_category ='';
 
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['name'])) $name =$row['name'];
                if(isset($row['about'])) $about = $row['about'];
                if(isset($row['category'])) $category = $row['category'];
                if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data_pages[] =[
                    'page_name' => $page_name,
                    'name' =>$name,
                    'about' => $about,
                    'category' =>$category,
                    'sub_category' =>$sub_category,
                    ];                 

  }
$result = DB::table('temp_81_pages')->insert($data_pages);
//dd($pages_result);
 $comment_result=Comment::raw(function ($collection) use($filter) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             $filter
            
      ]

        ]  
           ]
    
    ]);
})->toArray();
 
  $data_comment=[];
 foreach ($comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';
                $comment_count =0;
   
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = preg_replace('/(\r\n|\r|\n)+/', " ",$row['message']);
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    ];                 

  }
  $result = DB::table('temp_81_comments')->insert($data_comment);
    }
    public function UpdateHideMention()
    {
          $id = Input::post('id');
          $brand_id = Input::post('brand_id');
          $table_type = Input::post('table_type');
          
          if($table_type == "post")
          {
             $result =DB::table('temp_posts')
            ->where('id', $id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isHide' => 1));  // update the record in the DB. 
            $row = DB::table('predict_logs') //check row exist
                ->where('id', $id) 
                ->where('type','mention_post')
                ->limit(1)
                ->get()->toArray();
              
              if(!empty($row))
              {
                  DB::table('predict_logs')->where('id',$id)->where('type','mention_post')
                    ->update(array('isHide' => 1));
              }
              else
              {
                // dd("data not exist");
                  DB::table('predict_logs')
                    ->insert(array('id' => $id,'type' => 'mention_post','isHide' => 1,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
              }

          }
          elseif($table_type == "web_mention")
          {
             $result =DB::table('temp_web_mentions')
            ->where('id', $id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isHide' => 1));  // update the record in the DB. 
            $row = DB::table('predict_logs') //check row exist
                ->where('id', $id) 
                ->where('type','web_mention')
                ->limit(1)
                ->get()->toArray();
              
              if(!empty($row))
              {
                  DB::table('predict_logs')->where('id',$id)->where('type','web_mention')
                    ->update(array('isHide' => 1));
              }
              else
              {
                // dd("data not exist");
                  DB::table('predict_logs')
                    ->insert(array('id' => $id,'type' => 'web_mention','isHide' => 1,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
              }

          }
          else
          {
              $result =DB::table('temp_comments')
            ->where('id', $id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isHide' => 1));  // update the record in the DB.  
            $row = DB::table('predict_logs') //check row exist
                ->where('id', $id) 
                ->where('type','mention_comment')
                ->limit(1)
                ->get()->toArray();
              
              if(!empty($row))
              {
                  DB::table('predict_logs')->where('id',$id)->where('type','mention_comment')
                    ->update(array('isHide' => 1));
              }
              else
              {
                // dd("data not exist");
                  DB::table('predict_logs')
                    ->insert(array('id' => $id,'type' => 'mention_comment','isHide' => 1,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
              }

          }

            return $result;

    }
//region for local function

    function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 
function highLightText($keyword_data,$message)
{
  $highligh_string  = $message;
  // dd($highligh_string);
  $keyForHighLight=$this->getKeywordforHighLight($keyword_data);  //return an array
  // dd($keyForHighLight);
  usort($keyForHighLight, function($a, $b) {
    return strlen($b) - strlen($a);
});

  foreach ($keyForHighLight as $key => $value) {
 
   $highligh_string = str_ireplace($value, "<span style='background:#FFEB3B'>".$value."</span>",$highligh_string,$i);
}
  //dd($highligh_string );

  return $highligh_string;
}
function check_array_value($to_find, $array_values, $strict = false) {
    foreach ($array_values as $key =>$item) {
        if (($strict ? $item === $to_find : $item == $to_find) || (is_array($item) && $this->check_array_value($to_find, $item, $strict))) {
          $key = (int) $key+1;
            return $key;
        }
    }
    return 0;
}
function rangeMonth ($datestr) {
   date_default_timezone_set (date_default_timezone_get());
   $dt = strtotime ($datestr);
   return array (
     "start" => date ('Y-m-d', strtotime ('first day of this month', $dt)),
     "end" => date ('Y-m-d', strtotime ('last day of this month', $dt))
   );
 }

 function rangeWeek ($datestr) {
   date_default_timezone_set (date_default_timezone_get());
   $dt = strtotime ($datestr);
   return array (
     "start" => date ('N', $dt) == 1 ? date ('Y-m-d', $dt) : date ('Y-m-d', strtotime ('last monday', $dt)),
     "end" => date('N', $dt) == 7 ? date ('Y-m-d', $dt) : date ('Y-m-d', strtotime ('next sunday', $dt))
   );
 }

    function replace_null($value, $replace) {
    if (is_null($value)) return $replace;
    return $value;
}

function sumofarray($arr)
{
  $result=0;
   foreach($arr as $i => $match)
    {
    
    if(strpos($match,'K') !== false)
        {
        $new = str_replace("K", "", $match);
         $new = 1000*$new;
        }
        else
        {
         $new=$match;   
        }

       
       $result = $result + (float)$new;
 
    }

    return $result;
}

   
function convertKtoThousand($value)
{
  $result=0;
 
    if(!empty($value[0]))
    {

    if(strpos($value[0],'K') !== false)
        {
        $new = str_replace("K", "", $value[0]);
         $new = 1000*$new;
        }
        else
        {
         $new=$value[0];   
        }

       
       $result = $result + (float)$new;
    }
    

    return $result;
}
//end region for local function


}



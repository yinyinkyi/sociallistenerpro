<?php

namespace BIMiner\Http\Controllers;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use BIMiner\MongodbData;
use BIMiner\InboundPages;
use BIMiner\Comment;
use BIMiner\Project;
use BIMiner\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use Yajra\Datatables\Datatables;
use BIMiner\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;


class CronjobController extends Controller
{
    use GlobalController;
    public function HighlightedComment(){
        $projects = DB::table('projects')->select('*')->whereIn('id', [34,38,40])->get();

        $today = date('d-m-Y h:i:s');
        $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
        $dateBegin=date('Y-m-d', strtotime('-6 months', strtotime($today_plus)));
        $dateEnd=date('Y-m-d',strtotime($today_plus));


        foreach($projects as $project)
        {
            $id = $project->id;
            $monitor_pages = $project->monitor_pages;
            $monitor_pages = explode(',', $monitor_pages);
            $monitor_pages = $this->Format_Page_Name($monitor_pages);
            foreach($monitor_pages as $page)
            {

                // if($page != "UnionPayInternationalMM" && $page != "VisaMM" && $page != "jcbcardmyanmar" && $page != "MyanmarPaymentUnion"){
                //   continue;
                // }

                // if($page == "UnionPayInternationalMM" || $page == "VisaMM" || $page == "jcbcardmyanmar" || $page == "MyanmarPaymentUnion"){
                //   continue;
                // }

                $filter_page_name=$this->Format_Page_name_single($page);
                // dd($filter_page_name);  
                // $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
                // foreach($customName as $name) $filter_page_name = $name->origin_name;
                $filter_pages= " cmts.page_name in ('". $filter_page_name."') ";

                  $dict_arr =array();
                  $word_dict = DB::select("SELECT DISTINCT word FROM word_dict WHERE brand_id=".$id);
                  // dd($word_dict);
                    foreach($word_dict as $word_dict_result)
                    {
                      // dd(strtolower(str_replace(' ','',$word_dict_result->word)));
                      if(strtolower(str_replace(' ','',$word_dict_result->word)) == strtolower(str_replace(' ','',$filter_page_name)))
                      {
                        continue;
                      }
                      $dictWord =  strtolower(str_replace(' ','',$word_dict_result->word));
                       if (preg_match('/[က-အ]/', $dictWord)) $dictWord = $dictWord;
                       else  $dictWord = ' '.$dictWord.' ';
                       
                      $query = "SELECT cmts.id FROM temp_inbound_comments cmts ".
                        " WHERE  cmts.post_id IS NOT NULL AND  cmts.parent='' AND  LOWER(REPLACE(cmts.message,' ','')) like '%".$dictWord."%'  AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages .
                        " ORDER by timestamp(cmts.created_time) DESC limit 1000";
                        // dd($query);
                      $word_count = DB::select($query);
                      $word_str = '';
                     for($i = 0; $i < count($word_count); $i++){
                       if($i == count($word_count) -1 ){
                        $word_str = $word_str . $word_count[$i]->id ;
                       }
                       else{
                          $word_str = $word_str . $word_count[$i]->id . ",";
                        }
                      }

                      $word_str_arr = explode(",",$word_str);

                      if(count($word_count) > 0 ){
                       
                      
                        $formData = array(
                            'topic_name' => $dictWord,
                            'weight' => count($word_count),
                            'comment_id' => $word_str_arr
                        );
                        $dict_arr[] = $formData;

                      }

                    }
                    $dict_arr = array_unique($dict_arr, SORT_REGULAR);
                    if(!empty($dict_arr)){
                          $sortArray = array(); 
                          foreach($dict_arr as $dict_arr_item){ 
                            foreach($dict_arr_item as $key=>$value){ 
                                if(!isset($sortArray[$key])){ 
                                    $sortArray[$key] = array(); 
                                } 
                                $sortArray[$key][] = $value; 
                              } 
                            } 

                              $orderby = "weight"; 
                              array_multisort($sortArray[$orderby],SORT_DESC,$dict_arr); 
                              $dict_arr= array_slice($dict_arr,0,50);
                      }
                   
                       
                foreach($dict_arr as $row)
                {
                  
                  $comment_id = implode(',',$row['comment_id']);

                  

                  DB::table('highlighted_voices')->insert(['brand_id' => $id,'word' => $row['topic_name'],'weight'=>$row['weight'],'comment_id'=>$comment_id,'page_name'=>$page,'today_date'=>now()->toDateTimeString(),'created_at'=>now()->toDateTimeString()]);  
                       
                }
              
            }
        }
            $del_date = date('Y-m-d',strtotime('-5 day', strtotime(date('Y-m-d'))));
            $del_data = DB::table('highlighted_voices')->where('today_date','<',$del_date)->delete();

        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
        $date_time = $date->format('d-m-Y H:i:s');
        echo 'insert highlighted comments Successfully.'. $date_time;
        echo "";

   }


   public function get_highlighted_page_name(){
     //dd(now()->toDateString());

      $query = "SELECT distinct page_name FROM highlighted_voices  ".
      " WHERE  today_date >= '".now()->toDateString()."'";
     // dd($query);
      $word_count = DB::select($query);

      dd($word_count);
   }


   public function dailyMail(){

      // $page_name = 'CBBankmyanmar';   CBBankmyanmar /abankmyanmar/  
      // $kw_group = 'CB Bank';       /*CB Bank/ A Bank /*/


      $sday = date('Y-m-d',strtotime("-1 days"));
      $fday = date('Y-m-d',strtotime("-1 days"));
      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',$fday)));
      $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',$sday)));
      $fandateEnd = date('Y-m-d', strtotime('+1 day', strtotime($dateEnd)));
      $to_date = date("j M Y",strtotime("-1 days"));
      $from_date = date("j M Y",strtotime("-1 days"));
      $today_date = date("j M Y",strtotime("-1 days"));

      $reportType='Daily Report';
      $mails = DB::table('mail_report')->get();
      foreach ($mails as $key => $value) {

        $page_name = $value->page_name;
        $kw_group  = $value->keyword;
        $brand_id  = $value->brand_id;
        $email     = explode(',',$value->mail_address);

        $InboundPages_result=InboundPages::raw(function ($collection) use($page_name) {

        return $collection->aggregate([
          [
            '$match' =>[
                 '$and'=> [ 
                      ['page_name'=>['$eq'=>$page_name]],
                           
                    ]
                ] 
                               
            ],

             ['$sort' =>['_id'=>1]]
                    
          ]);
        })->toArray();

        $imgurl = '';
        foreach ($InboundPages_result as  $key => $row) {
          if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
          }

        $page_name=  $this->Format_Page_name_single($page_name);

        $fanGro_date = date('Y-m-d',strtotime("-2 days"));
        $pageData = $this->MailPageSummary($fanGro_date,$dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);
    
        $post_count = $pageData[0];
        $comment_count = $pageData[1];
        $page_growth = $pageData[2];

        $mentionData = $this->getMailMention($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);
        $cmtTotal = $mentionData[0];
        $postTotal = $mentionData[1];
        $articleTotal = $mentionData[2];
        $mentionTotal = (int)$cmtTotal + (int)$postTotal + (int)$articleTotal;

      
        $postTagData = $this->getMailPostTag($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);
        $pTagName1='';$pTagName2='';$pTagName3='';$pTagName4='';$pTagName5='';
        $pTagCount1='';$pTagCount2='';$pTagCount3='';$pTagCount4='';$pTagCount5='';
        if (array_key_exists(0,$postTagData)) $pTagName1 = $postTagData[0];
        if (array_key_exists(1,$postTagData)) $pTagCount1 = $postTagData[1];
        if (array_key_exists(2,$postTagData)) $pTagName2 = $postTagData[2];
        if (array_key_exists(3,$postTagData)) $pTagCount2 = $postTagData[3];
        if (array_key_exists(4,$postTagData)) $pTagName3 = $postTagData[4];
        if (array_key_exists(5,$postTagData)) $pTagCount3 = $postTagData[5];
        if (array_key_exists(6,$postTagData)) $pTagName4 = $postTagData[6];
        if (array_key_exists(7,$postTagData)) $pTagCount4 = $postTagData[7];
        if (array_key_exists(8,$postTagData)) $pTagName5 = $postTagData[8];
        if (array_key_exists(9,$postTagData)) $pTagCount5 = $postTagData[9];

        $cmtTagData = $this->getMailCommentTag($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);

        $cTagName1='';$cTagName2='';$cTagName3='';$cTagName4='';$cTagName5='';
        $cTagCount1='';$cTagCount2='';$cTagCount3='';$cTagCount4='';$cTagCount5='';
        if (array_key_exists(0,$cmtTagData)) $cTagName1 = $cmtTagData[0];
        if (array_key_exists(1,$cmtTagData)) $cTagCount1 = $cmtTagData[1];
        if (array_key_exists(2,$cmtTagData)) $cTagName2 = $cmtTagData[2];
        if (array_key_exists(3,$cmtTagData)) $cTagCount2 = $cmtTagData[3];
        if (array_key_exists(4,$cmtTagData)) $cTagName3 = $cmtTagData[4];
        if (array_key_exists(5,$cmtTagData)) $cTagCount3 = $cmtTagData[5];
        if (array_key_exists(6,$cmtTagData)) $cTagName4 = $cmtTagData[6];
        if (array_key_exists(7,$cmtTagData)) $cTagCount4 = $cmtTagData[7];
        if (array_key_exists(8,$cmtTagData)) $cTagName5 = $cmtTagData[8];
        if (array_key_exists(9,$cmtTagData)) $cTagCount5 = $cmtTagData[9];

        $sentiData = $this->getSentimentDetail($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);

            $overall_pos =$sentiData[0]['ov_positive'] ;
            // dd($overall_pos);
            $overall_neg = $sentiData[0]['ov_negative'] ;
            $overall_neutral = $sentiData[0]['ov_neutral'] ;
       
            $all_post_total=(int)$overall_pos+(int)$overall_neg+(int)$overall_neutral;
           
           $post_pos_pcent = '-';$post_neg_pcent='-';$post_neutral_pcent='-';
           if($all_post_total <> 0){
            $post_pos_pcent=(float)(($overall_pos/$all_post_total)*100);

            $post_pos_pcent= number_format((float)$post_pos_pcent, 2, '.', '');
            $post_pos_pcent = str_replace('.00', '', $post_pos_pcent);
            // dd($post_pos_pcent);
            $post_neg_pcent=(float)(($overall_neg/$all_post_total)*100);
            $post_neg_pcent= number_format((float)$post_neg_pcent, 2, '.', '');
            $post_neg_pcent = str_replace('.00', '', $post_neg_pcent);

            $post_neutral_pcent=(float)(($overall_neutral/$all_post_total)*100);
            $post_neutral_pcent= number_format((float)$post_neutral_pcent, 2, '.', '');
            $post_neutral_pcent = str_replace('.00', '', $post_neutral_pcent);
           }

            $cmt_total = (int)$sentiData[1]['total'];
            $cmt_neu = (int)$sentiData[1]['neutral'] + (int)$sentiData[1]['NA'];
            $cmt_pos = (int)$sentiData[1]['positive'];
            $cmt_neg = (int)$sentiData[1]['negative'];
             
            $all_cmt_total =   $cmt_neu + $cmt_pos +  $cmt_neg;
            $cmt_pos_pcent = '-';$cmt_neg_pcent='-';$cmt_neutral_pcent='-';
            if($all_cmt_total <> 0){
            $cmt_pos_pcent=(float)(($cmt_pos/$all_cmt_total)*100);
            $cmt_pos_pcent= number_format((float)$cmt_pos_pcent, 2, '.', '');
            $cmt_pos_pcent = str_replace('.00', '', $cmt_pos_pcent);

            $cmt_neg_pcent=(float)(($cmt_neg/$all_cmt_total)*100);
            $cmt_neg_pcent= number_format((float)$cmt_neg_pcent, 2, '.', '');
            $cmt_neg_pcent = str_replace('.00', '', $cmt_neg_pcent);


            $cmt_neutral_pcent=(float)(($cmt_neu/$all_cmt_total)*100);
            $cmt_neutral_pcent= number_format((float)$cmt_neutral_pcent, 2, '.', '');
            $cmt_neutral_pcent = str_replace('.00', '', $cmt_neutral_pcent);
        }
        // dd('hel');
        $inputs = [$today_date,$page_growth,$post_count,$comment_count,$cmtTotal,$postTotal,$articleTotal,$mentionTotal,$pTagName1,$pTagCount1,$pTagName2,$pTagCount2,$pTagName3,$pTagCount3,$pTagName4,$pTagCount4,$pTagName5,$pTagCount5,$cTagName1,$cTagCount1,$cTagName2,$cTagCount2,$cTagName3,$cTagCount3,$cTagName4,$cTagCount4,$cTagName5,$cTagCount5,$post_pos_pcent,$post_neg_pcent,$post_neutral_pcent,$cmt_pos_pcent,$cmt_neg_pcent,$cmt_neutral_pcent,$reportType,$to_date,$from_date,$imgurl];
        foreach($email as $single_mail){
             Mail::to($single_mail,'BIT')->send(new SendMailable($inputs));
        }

      }
        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
        $date_time = $date->format('d-m-Y H:i:s');
        echo 'Dail Mail Sent Successfully.'. $date_time;
        echo "";
   }
    public function weeklyMail(){

      // $page_name = 'CBBankmyanmar';   CBBankmyanmar /abankmyanmar/  
      // $kw_group = 'CB Bank';       /*CB Bank/ A Bank /*/


      $sday = date('Y-m-d',strtotime("-1 days"));
      $fday = date('Y-m-d',strtotime("-7 days"));
      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',$fday)));
      $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',$sday)));
      $fandateEnd = date('Y-m-d', strtotime('+1 day', strtotime($dateEnd)));
      $to_date = date("j M Y",strtotime("-1 days"));
      $from_date = date("j M Y",strtotime("-7 days"));
      $fanGro_date = date('Y-m-d',strtotime("-8 days"));
      $today_date = '';

      $reportType='Weekly Report';
      $mails = DB::table('mail_report')->get();
      foreach ($mails as $key => $value) {

        $page_name = $value->page_name;
        // $page_name='abankmyanmar';
        $kw_group  = $value->keyword;
        $brand_id  = $value->brand_id;
        $email     = explode(',',$value->mail_address);

        $InboundPages_result=InboundPages::raw(function ($collection) use($page_name) {

        return $collection->aggregate([
          [
            '$match' =>[
                 '$and'=> [ 
                      ['page_name'=>['$eq'=>$page_name]],
                           
                    ]
                ] 
                               
            ],

             ['$sort' =>['_id'=>1]]
                    
          ]);
        })->toArray();

        $imgurl = '';
        foreach ($InboundPages_result as  $key => $row) {
          if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
          }

        $page_name=  $this->Format_Page_name_single($page_name);

        
        $pageData = $this->MailPageSummary($fanGro_date,$dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);
    
        $post_count = $pageData[0];
        $comment_count = $pageData[1];
        $page_growth = $pageData[2];

        $mentionData = $this->getMailMention($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);
        $cmtTotal = $mentionData[0];
        $postTotal = $mentionData[1];
        $articleTotal = $mentionData[2];
        $mentionTotal = (int)$cmtTotal + (int)$postTotal + (int)$articleTotal;

      
        $postTagData = $this->getMailPostTag($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);
        $pTagName1='';$pTagName2='';$pTagName3='';$pTagName4='';$pTagName5='';
        $pTagCount1='';$pTagCount2='';$pTagCount3='';$pTagCount4='';$pTagCount5='';
        if (array_key_exists(0,$postTagData)) $pTagName1 = $postTagData[0];
        if (array_key_exists(1,$postTagData)) $pTagCount1 = $postTagData[1];
        if (array_key_exists(2,$postTagData)) $pTagName2 = $postTagData[2];
        if (array_key_exists(3,$postTagData)) $pTagCount2 = $postTagData[3];
        if (array_key_exists(4,$postTagData)) $pTagName3 = $postTagData[4];
        if (array_key_exists(5,$postTagData)) $pTagCount3 = $postTagData[5];
        if (array_key_exists(6,$postTagData)) $pTagName4 = $postTagData[6];
        if (array_key_exists(7,$postTagData)) $pTagCount4 = $postTagData[7];
        if (array_key_exists(8,$postTagData)) $pTagName5 = $postTagData[8];
        if (array_key_exists(9,$postTagData)) $pTagCount5 = $postTagData[9];

        $cmtTagData = $this->getMailCommentTag($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);

        $cTagName1='';$cTagName2='';$cTagName3='';$cTagName4='';$cTagName5='';
        $cTagCount1='';$cTagCount2='';$cTagCount3='';$cTagCount4='';$cTagCount5='';
        if (array_key_exists(0,$cmtTagData)) $cTagName1 = $cmtTagData[0];
        if (array_key_exists(1,$cmtTagData)) $cTagCount1 = $cmtTagData[1];
        if (array_key_exists(2,$cmtTagData)) $cTagName2 = $cmtTagData[2];
        if (array_key_exists(3,$cmtTagData)) $cTagCount2 = $cmtTagData[3];
        if (array_key_exists(4,$cmtTagData)) $cTagName3 = $cmtTagData[4];
        if (array_key_exists(5,$cmtTagData)) $cTagCount3 = $cmtTagData[5];
        if (array_key_exists(6,$cmtTagData)) $cTagName4 = $cmtTagData[6];
        if (array_key_exists(7,$cmtTagData)) $cTagCount4 = $cmtTagData[7];
        if (array_key_exists(8,$cmtTagData)) $cTagName5 = $cmtTagData[8];
        if (array_key_exists(9,$cmtTagData)) $cTagCount5 = $cmtTagData[9];

        $sentiData = $this->getSentimentDetail($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id);

            $overall_pos =$sentiData[0]['ov_positive'] ;
            // dd($overall_pos);
            $overall_neg = $sentiData[0]['ov_negative'] ;
            $overall_neutral = $sentiData[0]['ov_neutral'] ;
       
            $all_post_total=(int)$overall_pos+(int)$overall_neg+(int)$overall_neutral;
           
           $post_pos_pcent = '-';$post_neg_pcent='-';$post_neutral_pcent='-';
           if($all_post_total <> 0){
            $post_pos_pcent=(float)(($overall_pos/$all_post_total)*100);

            $post_pos_pcent= number_format((float)$post_pos_pcent, 2, '.', '');
            $post_pos_pcent = str_replace('.00', '', $post_pos_pcent);
            // dd($post_pos_pcent);
            $post_neg_pcent=(float)(($overall_neg/$all_post_total)*100);
            $post_neg_pcent= number_format((float)$post_neg_pcent, 2, '.', '');
            $post_neg_pcent = str_replace('.00', '', $post_neg_pcent);

            $post_neutral_pcent=(float)(($overall_neutral/$all_post_total)*100);
            $post_neutral_pcent= number_format((float)$post_neutral_pcent, 2, '.', '');
            $post_neutral_pcent = str_replace('.00', '', $post_neutral_pcent);
           }

            $cmt_total = (int)$sentiData[1]['total'];
            $cmt_neu = (int)$sentiData[1]['neutral'] + (int)$sentiData[1]['NA'];
            $cmt_pos = (int)$sentiData[1]['positive'];
            $cmt_neg = (int)$sentiData[1]['negative'];
             
            $all_cmt_total =   $cmt_neu + $cmt_pos +  $cmt_neg;
            $cmt_pos_pcent = '-';$cmt_neg_pcent='-';$cmt_neutral_pcent='-';
            if($all_cmt_total <> 0){
            $cmt_pos_pcent=(float)(($cmt_pos/$all_cmt_total)*100);
            $cmt_pos_pcent= number_format((float)$cmt_pos_pcent, 2, '.', '');
            $cmt_pos_pcent = str_replace('.00', '', $cmt_pos_pcent);

            $cmt_neg_pcent=(float)(($cmt_neg/$all_cmt_total)*100);
            $cmt_neg_pcent= number_format((float)$cmt_neg_pcent, 2, '.', '');
            $cmt_neg_pcent = str_replace('.00', '', $cmt_neg_pcent);


            $cmt_neutral_pcent=(float)(($cmt_neu/$all_cmt_total)*100);
            $cmt_neutral_pcent= number_format((float)$cmt_neutral_pcent, 2, '.', '');
            $cmt_neutral_pcent = str_replace('.00', '', $cmt_neutral_pcent);
        }
        // dd('hel');
        $inputs = [$today_date,$page_growth,$post_count,$comment_count,$cmtTotal,$postTotal,$articleTotal,$mentionTotal,$pTagName1,$pTagCount1,$pTagName2,$pTagCount2,$pTagName3,$pTagCount3,$pTagName4,$pTagCount4,$pTagName5,$pTagCount5,$cTagName1,$cTagCount1,$cTagName2,$cTagCount2,$cTagName3,$cTagCount3,$cTagName4,$cTagCount4,$cTagName5,$cTagCount5,$post_pos_pcent,$post_neg_pcent,$post_neutral_pcent,$cmt_pos_pcent,$cmt_neg_pcent,$cmt_neutral_pcent,$reportType,$to_date,$from_date,$imgurl];
       
      // return view('mail',compact('today_date','page_growth','post_count','comment_count','cmtTotal','postTotal','articleTotal','mentionTotal','pTagName1','pTagCount1','pTagName2','pTagCount2','pTagName3','pTagCount3','pTagName4','pTagCount4','pTagName5','pTagCount5','cTagName1','cTagCount1','cTagName2','cTagCount2','cTagName3','cTagCount3','cTagName4','cTagCount4','cTagName5','cTagCount5','post_pos_pcent','post_neg_pcent','post_neutral_pcent','cmt_pos_pcent','cmt_neg_pcent','cmt_neutral_pcent','reportType','to_date','from_date','imgurl'));
        foreach($email as $single_mail){
             Mail::to($single_mail,'BIT')->send(new SendMailable($inputs));
        }

      }
        $date = new \DateTime('now', new \DateTimeZone('Asia/Rangoon'));
        $date_time = $date->format('d-m-Y H:i:s');
        echo 'Weekly Mail Sent Successfully.'. $date_time;
        echo "";
   }

           public function getSentimentDetail($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id)
      {
           $filter_pages= " posts.page_name in ('".$page_name."') ";
           
          $query_post_today ="select  COALESCE(sum(IF(positive>negative, 1, 0)),0) ov_positive, COALESCE(sum(IF(negative>positive, 1, 0)),0) ov_negative from (SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative  FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id 
      where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = ''  ".
      " AND cmts.tag_flag=1 AND  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') " . 
      " GROUP BY posts.id)T1";
      $query_today_post_result = DB::select($query_post_today);



      $query_post_week ="SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral' OR  cmts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id 
      where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = ''  ". 
      " AND cmts.tag_flag=1 AND  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') " .
      " GROUP BY posts.id";
      $query_week_post_result = DB::select($query_post_week);

     $query_comment_week = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
        "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ". 
        " AND cmts.tag_flag=1 AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";
       
      $query_week_comment_result = DB::select($query_comment_week);
     foreach ($query_week_comment_result as $key => $value) {
      $cmt_arr['total'] = $value->total;
      $cmt_arr['neutral'] = $value->neutral;
      $cmt_arr['NA'] = $value->NA;
      $cmt_arr['positive'] = $value->positive;
      $cmt_arr['negative'] = $value->negative;


     }
// dd($cmt_arr);
  $data_week_post_result=[];
  $ov_positive=0;$ov_negative=0;$ov_neutral=0;
  foreach ($query_week_post_result  as $key => $value) {
 
    $ov_positive = $ov_positive+$value->positive;
    $ov_negative = $ov_negative+$value->negative;
    $ov_neutral = $ov_neutral+$value->neutral;
    
  }

  $data_week_post_result=array(
                'ov_positive'     =>$ov_positive,
                'ov_negative' => $ov_negative,
                'ov_neutral' => $ov_neutral,
              );
// dd($data_week_post_result);
  return array($data_week_post_result,$cmt_arr);
    }
     public function getMailCommentTag($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id)
    { 
      $brand_id=Input::get('brand_id');
      $campaign_filter_con = '';
      
      
         $period_type = '';
         $page_id='';$imgurl = '';
        $additional_filter=" AND 1=1 ";$additional_filter_post=" AND 1=1 ";
      
            if(null !==Input::get('period'))
            {
            $period=Input::get('period');
            $char_count = substr_count($period,"-");
            // dd($char_count);
            $format_type = '';
            if($char_count == 1)
            {
                $format_type ='%Y-%m';
            }
            else if($char_count == 2)
            {
                 $format_type ='%Y-%m-%d';
            }   
            else if($char_count > 2)
            {
                /*this is week*/
                $pieces = explode(" - ", $period);
                $format_type ='%Y-%m-%d';
                $period_type ='week';
            }
            // dd($period_type);

          
    if($period_type == 'week')
    {
     $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') "; 
     $additional_filter_post .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";      

    }
    else
    {
        $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
        $additional_filter_post .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
        // $additional_filter_post .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";   
    }

           }
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

       $keyword_con='';
       if('1' == Input::get('keyword_flag'))
       {
         $keyword_data = $this->getprojectkeyword($brand_id);
         if(isset($keyword_data [0]['main_keyword']))
         $keyword_con  =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'cmts'). ")"; // param (data_arr,table alais)
         else
         $keyword_con = ' AND 1=2';
        // dd( $keyword_con);
       }

       
             // $brand_id=17;
              // $groupType='day';
   
           $filter_pages = ' 1=1 ';     
      // if(null !== Input::get('filter_page_name'))
      //      {
      //         $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
      //         $filter_pages= " and page_name in ('".$filter_page_name."') ";
            
            
      //      }
           
             //get all page with no filter
            
         
              $filter_page_name=$this->Format_Page_name_single($page_name);

             

              $filter_pages .= " and cmts.page_name in ('". $filter_page_name."') ";
              $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                      
                      $page_id = '';$imgurl='';
                        foreach ($InboundPages_result as  $key => $row) {
                         
                          if(isset($row['id'])) $page_id= $row['id'];
                           if(isset($row['imageurl'])) $imgurl= $row['imageurl'];


                        }
            

           
        


       // dd($filter_pages);
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));

       $add_con="";
       $add_comment_con="";
       $CmtType = "";
        if(null !== Input::get('CmtType'))
        { 
          $CmtType = " and cmts.checked_sentiment='".Input::get('CmtType')."'";
        }

       $tag_con = '';$post_tag_con='';
       if(null !== Input::get('search_tag'))
      {
        // $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
         $tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'cmts'); 
         $post_tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'posts');
      }

      if($filter_keyword !== '')
      {
        $add_con = " and cmts.message Like '%".$filter_keyword."%'";
      }

      if(null !== Input::get('post_id') )
      {
        $add_con .="   and posts.id ='" .Input::get('post_id'). "'";
        
        
      }
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = '  1=1 ';
        $date_filter_post = '  1=1 ';
      }
      else
      {
        $date_filter =" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        $date_filter_post =" (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
      }
      $tagOrder = Input::get('tagOrder');
      $fieldName = Input::get('fieldName');

      // if(null !== $tagOrder && null !== $fieldName){
      //   if($fieldName == 'name') $order_con = " ORDER by "

      // }


      
          // dd($additional_filter);
           $query = " SELECT cmts.checked_tags_id  tags " .
       "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND  cmts.checked_tags_id <>'' AND ". $filter_pages.
      " AND " . $date_filter . $add_con  .  $keyword_con . $additional_filter . $CmtType .$tag_con ;

        




      // dd($query);
      //  return;

    $query_result = DB::select($query);
// dd($query_result);
       $data = [];
       $total=0;

       $data_tag = [];
       $total=0;
      

        foreach ($query_result as  $key => $row) {
              
       
                   $tags =$row->tags;
            
                $arr_tag=explode(',', $tags);
              
                   $request['tagLabel'] ="";
      
          foreach ($arr_tag as  $key => $tag) {
            if($tag <> '')
            {
              $tagInfo=$this->gettagsByid($tag);
                if(count($tagInfo)>0)
                    {
                     

                      foreach ($tagInfo as  $key => $taginfo) {

                      if (!array_key_exists($taginfo->id, $data_tag)) { 
                        $data_tag[$taginfo->id] = array(
                                    'tagLabel' =>$taginfo->name,
                                    'tagId' =>$taginfo->id,
                                    'page_id'=>$page_id,
                                    'imgurl'=>$imgurl,
                                    'tagCount' => 1,
                                 
                                   
                                );
                        }
                        else
                      {
                          // dd($taginfo->id);
                       
                       $data_tag[$taginfo->id]['tagCount'] = (int) $data_tag[$taginfo->id]['tagCount'] + 1;
                           
                      }
                      }
                    
                      
                    }

            }
                    
                
          }
      
             

    

 


                
         }
// dd($data_tag);
       //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
// dd($fieldName);
        if(null !== $tagOrder && null !== $fieldName){

         if($fieldName == 'name' && $tagOrder == 'ASC'){
          // dd('hy');
            usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?-1:1;
          });

        }
             
          if($fieldName == 'name' && $tagOrder == 'DESC'){
               usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?1:-1;
          });
               array_reverse($data_tag);
               

           
              // dd($data_tag);
          }
           if($fieldName == 'count' && $tagOrder == 'ASC'){
             $price = array_column($data_tag, 'tagCount');
            array_multisort($price, $data_tag);

          //   usort($data_tag, function($a, $b) {
          //   if($a['tagCount']==$b['tagCount']) return 0;
          //   return $a['tagCount'] < $b['tagCount']?1:-1;
          // });

        }
                 if($fieldName == 'count' && $tagOrder == 'DESC'){
            usort($data_tag, function($a, $b) {
            if($a['tagCount']==$b['tagCount']) return 0;
            return $a['tagCount'] < $b['tagCount']?1:-1;
          });

        }

   


      }
        else{
           usort($data_tag, function($a, $b) {
          if($a['tagCount']==$b['tagCount']) return 0;
          return $a['tagCount'] < $b['tagCount']?1:-1;
      });
         }

        // if(null === Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, 5, true);
           // else if ("no" !== Input::get('limit'))
           // $data_tag = array_slice($data_tag, 0, (int) Input::get('limit'), true);
           $tagarr = [];
           foreach ($data_tag as $key => $value) {
           
              array_push($tagarr, $value['tagLabel']);
              array_push($tagarr, $value['tagCount']);


           }
// dd($tagarr);
           return $tagarr;

    }


    public function getMailPostTag($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id)
    { 
              $brand_id=Input::get('brand_id');
      $campaign_filter_con = '';
      
      
         $period_type = '';
         $page_id='';$imgurl = '';
        $additional_filter=" AND 1=1 ";$additional_filter_post=" AND 1=1 ";
      
            if(null !==Input::get('period'))
            {
            $period=Input::get('period');
            $char_count = substr_count($period,"-");
            // dd($char_count);
            $format_type = '';
            if($char_count == 1)
            {
                $format_type ='%Y-%m';
            }
            else if($char_count == 2)
            {
                 $format_type ='%Y-%m-%d';
            }   
            else if($char_count > 2)
            {
                /*this is week*/
                $pieces = explode(" - ", $period);
                $format_type ='%Y-%m-%d';
                $period_type ='week';
            }
            // dd($period_type);

          
    if($period_type == 'week')
    {
     $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') "; 
     $additional_filter_post .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";      

    }
    else
    {
        $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
        $additional_filter_post .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
        // $additional_filter_post .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";   
    }

           }
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

       $keyword_con='';
       if('1' == Input::get('keyword_flag'))
       {
         $keyword_data = $this->getprojectkeyword($brand_id);
         if(isset($keyword_data [0]['main_keyword']))
         $keyword_con  =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'cmts'). ")"; // param (data_arr,table alais)
         else
         $keyword_con = ' AND 1=2';
        // dd( $keyword_con);
       }

       
             // $brand_id=17;
              // $groupType='day';
   
           $filter_pages = ' 1=1 ';     
      // if(null !== Input::get('filter_page_name'))
      //      {
      //         $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
      //         $filter_pages= " and page_name in ('".$filter_page_name."') ";
            
            
      //      }
           
             //get all page with no filter
            
         
              $filter_page_name=$this->Format_Page_name_single($page_name);

             

              $filter_pages .= " and posts.page_name in ('". $filter_page_name."') ";
              $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                      
                      $page_id = '';$imgurl='';
                        foreach ($InboundPages_result as  $key => $row) {
                         
                          if(isset($row['id'])) $page_id= $row['id'];
                           if(isset($row['imageurl'])) $imgurl= $row['imageurl'];


                        }
            

           
        


       // dd($filter_pages);
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));

       $add_con="";
       $add_comment_con="";
       $CmtType = "";
        if(null !== Input::get('CmtType'))
        { 
          $CmtType = " and cmts.checked_sentiment='".Input::get('CmtType')."'";
        }

       $tag_con = '';$post_tag_con='';
       if(null !== Input::get('search_tag'))
      {
        // $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
         $tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'cmts'); 
         $post_tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'posts');
      }

      if($filter_keyword !== '')
      {
        $add_con = " and cmts.message Like '%".$filter_keyword."%'";
      }

      if(null !== Input::get('post_id') )
      {
        $add_con .="   and posts.id ='" .Input::get('post_id'). "'";
        
        
      }
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = '  1=1 ';
        $date_filter_post = '  1=1 ';
      }
      else
      {
        $date_filter =" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        $date_filter_post =" (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
      }
      $tagOrder = Input::get('tagOrder');
      $fieldName = Input::get('fieldName');

      // if(null !== $tagOrder && null !== $fieldName){
      //   if($fieldName == 'name') $order_con = " ORDER by "

      // }


      
          // dd($additional_filter);
             $query = " SELECT posts.checked_tags_id  tags " .
       "FROM temp_inbound_posts posts ".
        " WHERE  posts.id IS NOT NULL  AND  posts.checked_tags_id <>'' ".
      " AND " . $filter_pages  . " AND " .  $date_filter_post  .  $add_con  .  $additional_filter_post . $post_tag_con;
      // dd($query);
        




      // dd($query);
      //  return;

    $query_result = DB::select($query);
// dd($query_result);
       $data = [];
       $total=0;

       $data_tag = [];
       $total=0;
      

        foreach ($query_result as  $key => $row) {
              
       
                   $tags =$row->tags;
            
                $arr_tag=explode(',', $tags);
              
                   $request['tagLabel'] ="";
      
          foreach ($arr_tag as  $key => $tag) {
            if($tag <> '')
            {
              $tagInfo=$this->gettagsByid($tag);
                if(count($tagInfo)>0)
                    {
                     

                      foreach ($tagInfo as  $key => $taginfo) {

                      if (!array_key_exists($taginfo->id, $data_tag)) { 
                        $data_tag[$taginfo->id] = array(
                                    'tagLabel' =>$taginfo->name,
                                    'tagId' =>$taginfo->id,
                                    'page_id'=>$page_id,
                                    'imgurl'=>$imgurl,
                                    'tagCount' => 1,
                                 
                                   
                                );
                        }
                        else
                      {
                          // dd($taginfo->id);
                       
                       $data_tag[$taginfo->id]['tagCount'] = (int) $data_tag[$taginfo->id]['tagCount'] + 1;
                           
                      }
                      }
                    
                      
                    }

            }
                    
                
          }
      
             

    

 


                
         }
// dd($data_tag);
       //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
// dd($fieldName);
        if(null !== $tagOrder && null !== $fieldName){

         if($fieldName == 'name' && $tagOrder == 'ASC'){
          // dd('hy');
            usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?-1:1;
          });

        }
             
          if($fieldName == 'name' && $tagOrder == 'DESC'){
               usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?1:-1;
          });
               array_reverse($data_tag);
               

           
              // dd($data_tag);
          }
           if($fieldName == 'count' && $tagOrder == 'ASC'){
             $price = array_column($data_tag, 'tagCount');
            array_multisort($price, $data_tag);

          //   usort($data_tag, function($a, $b) {
          //   if($a['tagCount']==$b['tagCount']) return 0;
          //   return $a['tagCount'] < $b['tagCount']?1:-1;
          // });

        }
                 if($fieldName == 'count' && $tagOrder == 'DESC'){
            usort($data_tag, function($a, $b) {
            if($a['tagCount']==$b['tagCount']) return 0;
            return $a['tagCount'] < $b['tagCount']?1:-1;
          });

        }

   


      }
        else{
           usort($data_tag, function($a, $b) {
          if($a['tagCount']==$b['tagCount']) return 0;
          return $a['tagCount'] < $b['tagCount']?1:-1;
      });
         }

        // if(null === Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, 5, true);
           // else if ("no" !== Input::get('limit'))
           // $data_tag = array_slice($data_tag, 0, (int) Input::get('limit'), true);
           $tagarr = [];
           foreach ($data_tag as $key => $value) {
           
              array_push($tagarr, $value['tagLabel']);
              array_push($tagarr, $value['tagCount']);


           }

           return $tagarr;

    }
    public function MailPageSummary($fanGro_date,$dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id)
    {

      $dateBeginWeek=date('Y-m-d', strtotime(str_replace(' ','/',$fanGro_date)));
      // dd($fandateEnd,$dateBeginWeek);
      $InboundPages_result=InboundPages::raw(function ($collection) use($page_name) {

      return $collection->aggregate([
          [
          '$match' =>[
               '$and'=> [ 
               ['page_name'=>$page_name],
                         
                        ]
          ] 
                     
         ],

         ['$sort' =>['_id'=>1]]
          
      ]);
    })->toArray();
      
    $page_id='';$imgurl = '';
    foreach ($InboundPages_result as  $key => $row) {
     
      if(isset($row['id'])) $page_id = $row['id'];
      if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
    }

    $MongoFan = DB::connection('mongodb')->collection('followers')->whereBetween('createdAt', array( Carbon::createFromDate(date('Y', strtotime($dateBeginWeek)),date('m', strtotime($dateBeginWeek)), date('d', strtotime($dateBeginWeek))),
    Carbon::createFromDate(date('Y', strtotime($fandateEnd)),date('m', strtotime($fandateEnd)),date('d', strtotime($fandateEnd)))))->orderBy('createdAt', 'asc')->where('id',$page_id)->get();
  
    $filterdata=[];
    foreach ($MongoFan as  $key => $row) {
      if(isset($MongoFan[$key]['date']))
            {
        if(!is_string($MongoFan[$key]['date']))
        {
                  $utcdatetime = $MongoFan[$key]['date'];
                  $fan_date = $utcdatetime->toDateTime();
                  $fan_date=$fan_date->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                  $fan_date = $fan_date->format('Y-m-d H:i:s');
                  $date= date("Y-m-d",strtotime($fan_date));
                  $time = date("H:i",strtotime($fan_date));
            if($time == '14:30')
            {
              if(isset($MongoFan[$key]['fan_count']))
              {
                $filterdata[]=$MongoFan[$key]['fan_count'];
              }
                            
            } 
              }
        }
      }
// dd($filterdata);
      $query = "SELECT count(*) pCount FROM temp_inbound_posts posts WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND posts.page_name in ('".$page_name."')  And visitor = 0";
      $postCount = DB::select($query);
            foreach ($postCount as $key => $value) {
              $pCount = $value->pCount;
              

            }
   
      $cmtquery = "SELECT count(*) cCount FROM temp_inbound_comments cmt WHERE (DATE(cmt.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND cmt.page_name in ('".$page_name."') ";
      $cmtCount = DB::select($cmtquery);
      foreach ($cmtCount as $key => $value) {
              $cCount = $value->cCount;
            }

            end($filterdata);// move the internal pointer to the end of the array
            $key = key($filterdata);
            $pageGrowth = (((int)$filterdata[$key] - (int)$filterdata[0])/(int)$filterdata[0])*100;
            $pageGrowth= number_format((float)$pageGrowth, 3, '.', '');
            $pageGrowth = $pageGrowth.'%';

            $finalData = array($pCount,$cCount,$pageGrowth);
            // dd($finalData);      
            return $finalData;
   

    }
    public function getMailMention($dateBegin,$dateEnd,$fandateEnd,$page_name,$kw_group,$brand_id)
    {

         $data = array();
         
         $add_inbound_con ="";
         $inboundpages=$this->getInboundPages($brand_id);

          if ($inboundpages !== '')
          {
            $add_inbound_con = " AND (page_name  not in (".$inboundpages.") or page_name is NULL)";
          }
          $excludepages = $this->getExcludePages($brand_id);
          if($excludepages != ""){
            $add_inbound_con .=" and (page_name  not in (".$excludepages.") or page_name is NULL)";
          }
          // dd($add_inbound_con);
          $comment_key_con=" AND 1=1 ";
          $post_key_con=" AND 1=1 ";
          $article_key_con=" AND 1=1 ";
       
           $keyword_data = $this->getprojectkeyword($brand_id,$kw_group);
           if(isset($keyword_data [0]['main_keyword']))
           {
            $comment_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")";
            $post_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 
            $article_key_con  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'article') . ")"; 
           }
           
            else
            {

             $comment_key_con .= ' AND 1=2';
             $post_key_con .= ' AND 1=2';
             $article_key_con .= ' AND 1=2';

            }
         


            $query_comment = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
              "FROM temp_comments cmts  WHERE cmts.isHide=0 and ".
              "  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con . $comment_key_con ;


            $query_comment_result = DB::select($query_comment);

            $query_post ="SELECT count(*) total,sum(IF(posts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(posts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(posts.checked_sentiment = 'neutral' OR  posts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_posts posts ".
            " WHERE posts.isHide=0 and  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') " . $add_inbound_con . $post_key_con;
            // dd($query_post_week);
            $query_post_result = DB::select($query_post);

             $query_article ="SELECT count(*) article_total,sum(IF(article.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(article.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(article.checked_sentiment = 'neutral' OR  article.checked_sentiment ='NA' , 1, 0)) neutral FROM temp_web_mentions  article".
            " WHERE (article.isHide=0 or article.isHide is null) AND (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') "  . $add_inbound_con  .  $article_key_con;
            // dd($query_article);
            $query_article_result = DB::select($query_article);
  
            foreach($query_comment_result as $cmt) $cmtTotal = $cmt->total;
            foreach($query_post_result as $post) $postTotal = $post->total;
            foreach($query_article_result as $article) $articleTotal = $article->article_total;
// dd($articleTotal);

            return array($cmtTotal,$postTotal,$articleTotal);
    }


   public function Format_Page_Name($array){
      $new_array=[];
        foreach ($array as  $key => $row) {
          $arr_page_name=explode("-",$row);
            if(count($arr_page_name)>0){
              $last_index=$arr_page_name[count($arr_page_name)-1];
              if(is_numeric($last_index))
              $new_array[] = $last_index;
              else
              $new_array[] = $row;
            }
            else{
              $new_array[]=$row;
            }
          }

           return  $new_array;
    }

    public function Format_Page_name_single($pagename){
      $newpagename='';
      $arr_page_name=explode("-",$pagename);
      if(count($arr_page_name)>0){
        $last_index=$arr_page_name[count($arr_page_name)-1];
        if(is_numeric($last_index))
        $newpagename = $last_index;
        else
        $newpagename = $pagename;
      }
      else{
          $newpagename=$pagename;
      }
        return  $newpagename;
    }
}

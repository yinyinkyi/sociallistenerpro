<?php

namespace BIMiner\Http\Controllers\Inbound;

use BIMiner\MongodbData;
use BIMiner\MongoPages;
use BIMiner\MongoFollowers;
use BIMiner\Comment;
use BIMiner\WordCloudCache;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use Yajra\Datatables\Datatables;
use BIMiner\Http\Controllers\Controller;
use BIMiner\Http\Controllers\GlobalController;
use BIMiner\InboundPages;

class inboundController extends Controller
{
   use GlobalController;
  
   public function getHighlighted_New()
   {
         $brand_id = Input::get('brand_id');
         //$brand_id =34;
         if(null !== Input::get('fday'))
          {
           $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
          $dateBegin=date('Y-m-d');
          }
          if(null !==Input::get('sday'))
          {

            $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }

          else
          {
            $dateEnd=date('Y-m-d');
          }
          $filter_page_name = '';
          
          if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
          {
           $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
           $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
          }
          // dd($brand_id);

          // else
          // {
          //    $my_pages= $this->getOwnPage($brand_id);
          //    $my_pages=explode(',', $my_pages);
          //    $filter_pages=$this->getPageWhereGen($my_pages);

          // }
      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2019 01 01')));
      $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2019 03 29')));
          $removed_keyword=[];

          $removed_query = "SELECT keyword FROM  keywords_removed where brand_id=".$brand_id;
          $removed_result = DB::select($removed_query);
          foreach ($removed_result as  $key => $row) {
                    $removed_keyword[$row->keyword] =$row->keyword ;
                    }
                    // dd($removed_keyword);

          $check_cache_query="SELECT id,from_date,to_date,json_data from wordCloudCache  WHERE brand_id=".$brand_id." AND page_name='".$filter_page_name."'  AND  DATE(from_date) ='".$dateBegin."' AND DATE(to_date)='".$dateEnd."'";
        
          
          $check_cache = DB::select($check_cache_query);
          // dd($check_cache);

          if( count($check_cache) > 0 )
          {
            foreach($check_cache as $check_cache_result)
            {
                $json_code_data =$check_cache_result->json_data;
            }

          }
          //Prepare for api call
          else{


            $word_dict = DB::select("SELECT word FROM word_dict;");

            //$dict_arr[];
            foreach($word_dict as $word_dict_result)
            {
                $query = "SELECT cmts.id FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
                " WHERE  cmts.post_id IS NOT NULL AND cmts.message like '%".$word_dict_result->word."%'  AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages .
                " ORDER by timestamp(posts.created_time) DESC,posts.id,timestamp(cmts.created_time) DESC";

                $word_count = DB::select($query);

               $word_str = '';
               for($i = 0; $i < count($word_count); $i++){
                 if($i == count($word_count) -1 ){
                  $word_str = $word_str . $word_count[$i]->id ;
                 }
                 else{
                    $word_str = $word_str . $word_count[$i]->id . ",";
                 }

               }

                $word_str_arr = explode(",",$word_str);
                if(count($word_count) > 0 ){
                    $formData = array(
                      'topic_name' => $word_dict_result->word,
                      'weight' => count($word_count),
                      'comment_id' => $word_str_arr
                  );
                  $dict_arr[] = $formData;
                }
            }


          $sortArray = array(); 
          foreach($dict_arr as $dict_arr_item){ 
              foreach($dict_arr_item as $key=>$value){ 
                  if(!isset($sortArray[$key])){ 
                      $sortArray[$key] = array(); 
                  } 
                  $sortArray[$key][] = $value; 
              } 
          } 

          $orderby = "weight"; //change this to whatever key you want from the array 

          array_multisort($sortArray[$orderby],SORT_DESC,$dict_arr); 

          $dict_arr= array_slice($dict_arr,0,50);

            //dd($dict_arr);
            $dict_arr = json_encode($dict_arr);
            // dd($dict_arr);

            $query="SELECT posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')".
            "created_time,cmts.id cmt_id,cmts.message comment_message,cmts.post_id post_id,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p')  comment_date,".
            " cmts.checked_sentiment cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict,cmts.tags ".
            " FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
            " WHERE  cmts.post_id IS NOT NULL   AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages .
            " ORDER by timestamp(posts.created_time) DESC,posts.id,timestamp(cmts.created_time) DESC";
          
            
            $data = DB::select($query);
            // print_r($data);
            // return;

           
              $post_id='';
              $post_sr_no=0;
              $post_data_array=[];
              $post_id_array=[];
              $comment_data_array=[];
              $comment_cid_array=[];
              $comment_pid_array=[];
              $topics = [];
              foreach($data as $result)
              {
                 
                if($post_id == $result->id)
                {
                    if($result->parent == '')
                    {
                      $comment_data_array[]=preg_replace('/(\r\n|\r|\n)+/', " ",$result->comment_message);
                      $comment_cid_array[]=$result->cmt_id;
                      $comment_pid_array[]=$result->id;
                    }
                    
               
                }
                else
                {
                   
                    $post_data_array[]=preg_replace('/(\r\n|\r|\n)+/', " ",$result->message);
                    $post_id_array[]=$result->id;
                    
                    if($result->parent == '')
                    {
                        $comment_data_array[]=preg_replace('/(\r\n|\r|\n)+/', " ",$result->comment_message);
                        $comment_cid_array[]=$result->cmt_id;
                        $comment_pid_array[]=$result->post_id;
                    }
    
                    $post_id = $result->id;
                   
                }
               
              }
    
              if(count($post_data_array)>0)
            {
            
            
            $client = new Client(['base_uri' => 'http://35.185.97.177:5001/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
            $uri_keyword = 'wordcloud_get';
            
                $formData = array(
                // 'keywords' => $keyword_param,
                'post' =>  $post_data_array,
                'pid' =>  $post_id_array,
                'comment' =>  $comment_data_array,
                'cmt_id'  =>  $comment_cid_array,
                'cmt_pid' =>  $comment_pid_array,
                'ntopics' => '100'
               
            );
            //dd($comment_cid_array);
            $formData = json_encode($formData);
            // dd($formData);
            $path = storage_path('app/data_output/KeywordWithNewFormat29Mar.json');
            $this->doJson($path,$formData);
            return;
            $topics=[];
            try {
            $keyword_response = $client->post($uri_keyword, [
                'form_params' => [
                    'raw' =>  $formData,
                ],
            ]);
            
            $keyword_result = $keyword_response->getBody()->getContents();
            $topics = json_decode($keyword_result, true);
            // print_r($keyword_response->getStatusCode());
            // dd($keyword_response->getStatusMessage());
            }
            catch (\Exception $e) {
              
            $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());
            
            $error_message = explode('<html>',$error_message);
            // $error_message='Server error: `POST http://35.185.97.177:5001/wordcloud_get` resulted in a `504 Gateway Time-out` response: <html> <head><title>504 Gateway Time-out</title></head> <body bgcolor="white"> <center><h1>504 Gateway Time-out</h1><(truncated)';
            //dd($error_message[0]);
            $error_message=$error_message[0];
            
            //$error_message ='test';
            $sms_data=json_encode([
              'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
              'group_id' =>2,
            ]);
            $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
              'headers' => ['Content-Type' => 'application/json'],
              'body' => $sms_data
            ]);
             //dd($sms_response->getBody()->getContents());
              // return false;
            }
    
            // dd($topics);

          foreach($topics as $key => $val)
          {

          $found=array_search($val['topic_name'], $removed_keyword) ;
             if($found === $val['topic_name'])
              {

                 unset($topics[$key]);
              }
          }
            
          usort($topics, function($a, $b) {
            if($a['weight']==$b['weight']) return 0;
            return $a['weight'] < $b['weight']?1:-1;
          });//Decending

            
        
        $topics = array_slice($topics, 0, 50, true);
        // $topics_test =implode(', ', array_map(function ($entry) {
        //   return $entry['tag_name'];
        // }, $input));
        //dd($topics_test);
        //dd( $filter_page_name );
        // dd($topics);

          $json_code_data = json_encode($topics);
          // dd($json_code_data);
          WordCloudCache::create([
              'from_date' =>$dateBegin,
              'to_date' => $dateEnd,
              'json_data' => $json_code_data,
              'brand_id' => $brand_id,
              'page_name'=>$filter_page_name
            
          ]);

                }
              }
       echo $json_code_data;

   }
     public function getHighlighted_frequent()
	{
         //getDataFromTable
         $brand_id = Input::get('brand_id');
         
          $filter_page_name = '';
          $filter_pages='';
          
          if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
          {
           $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
           $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
            foreach($customName as $name) $filter_page_name = $name->origin_name;
           $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
          }
          else
          {
            $filter_pages = " 1=1 ";
          }

	        $today = date('d-m-Y h:i:s');
	        $today=date('Y-m-d',strtotime($today));
	        // $today=date('Y-m-d',strtotime('2019-07-27'));
	        $yest = date('Y-m-d', strtotime('-1 day', strtotime($today)));
	            
	        $query = "Select Distinct word,comment_id,weight from highlighted_voices where page_name = '".$filter_page_name
	         ."' and brand_id = ".$brand_id." and Date(today_date) BETWEEN '".$today."' and '".$today."'";
	         $res = DB::select($query);
	       
	        if(empty($res))
	        {
	          // $yest = date('Y-m-d',strtotime('2019-07-27'));
	          // dd($filter_page_name);
	            $query = "Select Distinct word,comment_id,weight from highlighted_voices where page_name = '".$filter_page_name
	         ."' and brand_id = ".$brand_id." and Date(today_date) BETWEEN '".$yest."' and '".$yest."'";
	         $res = DB::select($query);
	        }
      
        	$dict_arr = [];
	        foreach ($res as $key => $value) {
	          $dictWord = $value->word;
	          $word_str = $value->comment_id;
	          $weight = $value->weight;
	          $word_str_arr = explode(",",$word_str);

              // if(count($word_count) > 0 ){

                  $formData = array(
                    'topic_name' => $dictWord,
                    'weight' => $weight,
                    'comment_id' => $word_str_arr
                );
                $dict_arr[] = $formData;
              // }
	        }

		    if(!empty($dict_arr)){
		    $sortArray = array(); 
		    foreach($dict_arr as $dict_arr_item){ 
		        foreach($dict_arr_item as $key=>$value){ 
		            if(!isset($sortArray[$key])){ 
		                $sortArray[$key] = array(); 
		            } 
		            $sortArray[$key][] = $value; 
		        } 
		    } 

		    $orderby = "weight"; //change this to whatever key you want from the array 

		    array_multisort($sortArray[$orderby],SORT_DESC,$dict_arr); 

		    $dict_arr= array_slice($dict_arr,0,50);

		  }
          $dict_arr = json_encode($dict_arr);
          echo $dict_arr;
  	}

//    public function getHighlighted_frequent()
//    {
//          $brand_id = Input::get('brand_id');
//          //$brand_id =34;
//          if(null !== Input::get('fday'))
//           {
//            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
//           }
//           else
//           {
//           $dateBegin=date('Y-m-d');
//           }
//           if(null !==Input::get('sday'))
//           {

//             $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
//           }

//           else
//           {
//             $dateEnd=date('Y-m-d');
//           }
//           $filter_page_name = '';
//           $filter_pages='';
          
//           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
//           {
//            $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
//            $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
//             foreach($customName as $name) $filter_page_name = $name->origin_name;
//            $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
//           }
//           else
//           {
//             $filter_pages = " 1=1 ";
//           }


//           $dict_arr =array();
//           $word_dict = DB::select("SELECT DISTINCT word FROM word_dict;");
//           // dd($word_dict);
//           //dd($filter_page_name);
//           //$brand_name = DB::select("SELECT ");

//           //$dict_arr[];
//           foreach($word_dict as $word_dict_result)
//           {

//               //$remove_space =preg_replace('/\s+/', ' ', $foo);
//               if(strtolower(str_replace(' ','',$word_dict_result->word)) == strtolower(str_replace(' ','',$filter_page_name)))
//               {
//                 continue;
//               }
//                 $dictWord =  strtolower(str_replace(' ','',$word_dict_result->word));
                 
//               $query = "SELECT cmts.id FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
//               " WHERE  cmts.post_id IS NOT NULL AND  cmts.parent='' AND  LOWER(REPLACE(cmts.message,' ','')) like '%".$dictWord."%'  AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages .
//               " ORDER by timestamp(cmts.created_time) DESC";

//               $word_count = DB::select($query);
// // dd($query);
//              $word_str = '';
//              for($i = 0; $i < count($word_count); $i++){
//                if($i == count($word_count) -1 ){
//                 $word_str = $word_str . $word_count[$i]->id ;
//                }
//                else{
//                   $word_str = $word_str . $word_count[$i]->id . ",";
//                }

//              }

//               $word_str_arr = explode(",",$word_str);

//               if(count($word_count) > 0 ){

//                   $formData = array(
//                     'topic_name' => $dictWord,
//                     'weight' => count($word_count),
//                     'comment_id' => $word_str_arr
//                 );
//                 $dict_arr[] = $formData;
//               }
//           }


// if(!empty($dict_arr)){
//         $sortArray = array(); 
//         foreach($dict_arr as $dict_arr_item){ 
//             foreach($dict_arr_item as $key=>$value){ 
//                 if(!isset($sortArray[$key])){ 
//                     $sortArray[$key] = array(); 
//                 } 
//                 $sortArray[$key][] = $value; 
//             } 
//         } 

//         $orderby = "weight"; //change this to whatever key you want from the array 

//         array_multisort($sortArray[$orderby],SORT_DESC,$dict_arr); 

//         $dict_arr= array_slice($dict_arr,0,50);

//           //dd($dict_arr);
//       }
//           $dict_arr = json_encode($dict_arr);


//          echo $dict_arr;
//    }



   public function getKeyword()
   {
    $brand_id=Input::get('brand_id');
    // $brand_id=34;
         if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

      if(null !==Input::get('sday'))
      {
       $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      
      else
      {
       $dateEnd=date('Y-m-d');
      }
  // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 12 15')));
  // $dateEnd  =date('Y-m-d', strtotime(str_replace(' ','-','2019 01 15')));

         $keyword_param=[];
         $removed_keyword=[];

      $removed_query = "SELECT keyword FROM  keywords_removed where brand_id=".$brand_id;
      $removed_result = DB::select($removed_query);
      foreach ($removed_result as  $key => $row) {
                $removed_keyword[$row->keyword] =$row->keyword ;
                }

      $keyword_query = "SELECT main_keyword FROM  project_keywords where project_id=".$brand_id;
      $keyword_result = DB::select($keyword_query);

       foreach ($keyword_result as  $key => $row) {
          $keyword_param[] =$row->main_keyword ;
          }

          if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
          {
           $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
           $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
            foreach($customName as $name) $filter_page_name = $name->origin_name;
           $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
          }
          else
          {
             $my_pages= $this->getOwnPage($brand_id);
             $my_pages=explode(',', $my_pages);
             $filter_pages=$this->getPageWhereGen($my_pages);

          }
// if($type == 'all')
      $query = " SELECT cmts.id,cmts.wb_message wb_message FROM temp_inbound_comments cmts LEFT JOIN temp_inbound_posts posts ".
      " on cmts.post_id=posts.id ".
      " WHERE posts.id IS NOT NULL and cmts.parent = ''  and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages  ;

$data_message=[];
$query_result=[];
$query_result = DB::select($query);
// dd($query_result);

do {
   $query_result = DB::select($query);
 /*dd($query_result);
   return ;*/
 foreach ($query_result as  $key => $row) {
   if(isset($row->wb_message))
  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row->wb_message);

  }

  }while (count($data_message)<=0 && count($query_result)>0 );
  //do while condition is for while data is not exist , we dont want to show blank to wordcloud.

$topics=[];

if(count($data_message)>0)
{
// dd($data_message);

$client = new Client(['base_uri' => 'http://13.251.100.148:5003/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
$uri_keyword = 'keyword_get';

    $formData = array(
    // 'keywords' => $keyword_param,
    'messages' =>  $data_message,
    'ntopics' => '100'
   
);

$formData = json_encode($formData);
// dd($formData);
// $path = storage_path('app/data_output/KeywordOldFormat_29May2019.json');
// $this->doJson($path,$formData);
// return;
$topics = [];
try
{
  $keyword_response = $client->post($uri_keyword, [
    'form_params' => [
        'raw' =>  $formData,
    ],
]);
// dd($formData);
$keyword_result = $keyword_response->getBody()->getContents();

// dd($keyword_result);

$topics = json_decode($keyword_result, true);
// echo "string";
// dd($topics);
}
catch (\Exception $e)
{
  
  $error_message =preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage());
    
  $error_message = explode('<html>',$error_message);
  
  $error_message="CMS_Keyword : " . $error_message[0];
  
  $sms_data=json_encode([
    'message_body' =>$error_message,//preg_replace('/(\r\n|\r|\n)+/', " ", $e->getMessage()),
    'group_id' =>2,
  ]);
  $sms_response = $client->post('https://bagankeyboard.com/bkb_api/sms_sent_for_server_failure/sent_sms', [
    'headers' => ['Content-Type' => 'application/json'],
    'body' => $sms_data
  ]);
}

// dd($topics);
foreach($topics as $key => $val)
{
  
$found=array_search($val['topic_name'], $removed_keyword) ;
   if($found === $val['topic_name'])
    {

       unset($topics[$key]);
    }
}
  

  }

  usort($topics, function($a, $b) {
    if($a['weight']==$b['weight']) return 0;
    return $a['weight'] < $b['weight']?1:-1;
});//Decending
// dd($topics);
  $topics = array_slice($topics, 0, 50, true);
   echo json_encode($topics);
   }
   public function getHighlighted()
{
 

   $brand_id=Input::get('brand_id');
   $type=Input::get('type');
  //  $brand_id=34;
         if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
  $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2019 01 22')));
  $dateEnd  =date('Y-m-d', strtotime(str_replace(' ','-','2019 03 22')));

   $keyword_param=[];
   $removed_keyword=[];

$removed_query = "SELECT keyword FROM  keywords_removed where brand_id=".$brand_id;
$removed_result = DB::select($removed_query);
foreach ($removed_result as  $key => $row) {
          $removed_keyword[$row->keyword] =$row->keyword ;
          }

$keyword_query = "SELECT main_keyword FROM  project_keywords where project_id=".$brand_id;
$keyword_result = DB::select($keyword_query);

       foreach ($keyword_result as  $key => $row) {
          $keyword_param[] =$row->main_keyword ;
          }

          if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
          {
           $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
           $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
          }
          else
          {
             $my_pages= $this->getOwnPage($brand_id);
             $my_pages=explode(',', $my_pages);
             $filter_pages=$this->getPageWhereGen($my_pages);

          }
// if($type == 'all')
$query = " SELECT cmts.id,cmts.message wb_message FROM temp_inbound_comments cmts LEFT JOIN temp_inbound_posts posts ".
" on cmts.post_id=posts.id ".
" WHERE posts.id IS NOT NULL and cmts.parent = '' and cmts.interest = 1 and (cmts.sentiment='pos' or cmts.sentiment='neg') and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages  ;
// else
// $query = " SELECT cmts.id,cmts.wb_message FROM temp_inbound_comments cmts LEFT JOIN temp_inbound_posts posts ".
// " on cmts.post_id=posts.id ".
// " WHERE posts.id IS NOT NULL and cmts.parent = '' and cmts.interest = 1 and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages  ;

/*echo $query;
return;*/
// dd($query);
$data_message=[];
$query_result=[];
$query_result = DB::select($query);


do {
   $query_result = DB::select($query);
 /*dd($query_result);
   return ;*/
 foreach ($query_result as  $key => $row) {
   if(isset($row->wb_message))
  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row->wb_message);

  }

  }while (count($data_message)<=0 && count($query_result)>0 );
  //do while condition is for while data is not exist , we dont want to show blank to wordcloud.

$topics=[];

if(count($data_message)>0)
{


$client = new Client(['base_uri' => 'http://35.185.97.177:5001/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
$uri_keyword = 'wordcloud_get';

    $formData = array(
    // 'keywords' => $keyword_param,
    'messages' =>  $data_message,
    'ntopics' => '100'
   
);


$formData = json_encode($formData);
//dd($formData);
// $path = storage_path('app/data_output/KeywordOldFormat.json');
// $this->doJson($path,$formData);
// return;

$keyword_response = $client->post($uri_keyword, [
    'form_params' => [
        'raw' =>  $formData,
    ],
]);

$keyword_result = $keyword_response->getBody()->getContents();


$topics = json_decode($keyword_result, true);

foreach($topics as $key => $val)
{

$found=array_search($val['topic_name'], $removed_keyword) ;
   if($found === $val['topic_name'])
    {

       unset($topics[$key]);
    }
}
  

  }

  usort($topics, function($a, $b) {
    if($a['weight']==$b['weight']) return 0;
    return $a['weight'] < $b['weight']?1:-1;
});//Decending

  $topics = array_slice($topics, 0, 50, true);
   echo json_encode($topics);



}

function doJson($path,$json)
{
    $fp = fopen($path, 'w');
   fwrite($fp, $json);
   fclose($fp);
}
public function getinboundPostReaction()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

     // $brand_id=17;
     // $groupType='day';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 09 15')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 21')));

 // $groupType="month";
           //check for comparison
          //  if(null !== Input::get('filter_page_name'))
          //  {
          //   $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
          //   $filter_pages= " page_name in ('".$filter_page_name."') ";
          
          //  }
           // dd(Input::get('admin_page'));
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
            $customName = DB::table('page_rename')->where('origin_name',Input::get('admin_page'))->orWhere('custom_name',Input::get('admin_page'))->get();
            foreach($customName as $name) {$page_name = $name->origin_name;}

            $filter_page_name=$this->Format_Page_name_single($page_name);
            $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
            // dd($filter_page_name);
            $color = $this->getbrandcolor($filter_page_name);
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);

           }
             

 $group_period ="";
 $add_con="";
 $add_comment_con ="";

if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}
if($filter_sentiment !== '')
{
  $add_con .= " and sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.checked_sentiment  = '".$filter_sentiment."'";
}
$add_post_con='';
 if(null !== Input::get('tags')){
      $add_post_con = " and " . $this->getTagWhereOrGen(Input::get('tags'),'posts'); 
           }

  // $query_comment = "SELECT count(*) total_comment ".
  // "FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts ".
  // " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  // " AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";
  // $query_comment_result = DB::select($query_comment);

$query = " SELECT sum(Liked) Liked,sum(Love) Love,sum(Haha) Haha,sum(Angry) Angry,sum(Sad) Sad,sum(Wow) Wow,count(*)  post_count, DATE_FORMAT(posts.created_time, '".$group_period."') created_time ".
" FROM temp_inbound_posts posts WHERE posts.visitor <> 1 AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ". $filter_pages.  $add_con . $add_post_con .
" GROUP BY DATE_FORMAT(posts.created_time, '".$group_period."') ORDER BY DATE(created_time) DESC " ;

// echo $query;
$query_result = DB::select($query);
// dd($query_result);

   $data = [];
   $total=0;

    foreach ($query_result as  $key => $row) {

                $datetime = $row ->created_time;
                $periodLabel ="";
                $weekcount ="";
                if($groupType == "week")
                {
                  $weekperiod=$this ->rangeWeek ($datetime);
                  $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
                 if (!array_key_exists($periodLabel , $data)) {
                  $data[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'Wow' =>  $row ->Wow,
                'Love' => $row ->Love,
                'Angry' => $row ->Angry,
                'Sad' => $row ->Sad,
                'Haha' => $row ->Haha,
                'Like' =>$row ->Liked,
                'periods' => $periodLabel,
                'post_count' =>$row ->post_count,
            );
                                                                }
                else
                {
$data[$periodLabel]['Wow'] = (int) $data[$periodLabel]['Wow'] + (int) $row ->Wow;
$data[$periodLabel]['Love'] = (int) $data[$periodLabel]['Love'] + (int)$row ->Love;
$data[$periodLabel]['Angry'] = (int) $data[$periodLabel]['Angry'] +(int) $row ->Angry;
$data[$periodLabel]['Sad'] = (int) $data[$periodLabel]['Sad'] + (int) $row ->Sad;
$data[$periodLabel]['Haha'] = (int) $data[$periodLabel]['Haha'] + (int) $row ->Haha;
$data[$periodLabel]['Like'] = (int) $data[$periodLabel]['Like'] + (int) $row ->Liked;
$data[$periodLabel]['periods'] =  $datetime;
$data[$periodLabel]['post_count'] =(int) $data[$periodLabel]['post_count'] +(int)$row ->post_count;

                }
   
 
                }
    else 
    {
                 $data[$datetime]['Wow'] = $row ->Wow;
                 $data[$datetime]['Love'] =$row ->Love;
                 $data[$datetime]['Angry'] =$row ->Angry;
                 $data[$datetime]['Sad'] = $row ->Sad;
                 $data[$datetime]['Haha'] =$row ->Haha;
                 $data[$datetime]['Like'] =$row ->Liked;
                 $data[$datetime]['periods'] =  $datetime;
                 $data[$datetime]['periodLabel'] =  $datetime;
                 $data[$datetime]['post_count'] =$row ->post_count;
    }
                
              
                 
               
            }


//$data = $this->unique_multidim_array($data,'periodLabel'); 
 

    echo json_encode(array($data,$color));


}

public function getinboundReaction()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

     // $brand_id=17;
     // $groupType='day';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 09 15')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 21')));

 // $groupType="month";
           //check for comparison
          //  if(null !== Input::get('filter_page_name'))
          //  {
          //   $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
          //   $filter_pages= " page_name in ('".$filter_page_name."') ";
          
          //  }
           $color = [];
       if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
       {
          $customName = DB::table('page_rename')->where('origin_name',Input::get('admin_page'))->orWhere('custom_name',Input::get('admin_page'))->get();
          foreach($customName as $name) {$page_name = $name->origin_name;}

        $filter_page_name=$this->Format_Page_name_single($page_name);
        $filter_pages= " posts.page_name in ('". $filter_page_name."') ";

        $color = $this->getbrandcolor($filter_page_name);

          // $customName= DB::select("Select * from page_rename where origin_name = '".$filter_page_name."' OR custom_name = '".$filter_page_name."'");
          // foreach($customName as $name) {$color = $name->color;}

       }
       else
       {
          $my_pages= $this->getOwnPage($brand_id);
          $my_pages=explode(',', $my_pages);
          $filter_pages=$this->getPageWhereGen($my_pages);

       }
             

 $group_period ="";
 $add_con="";
 $add_comment_con ="";

if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}
if($filter_sentiment !== '')
{
  $add_con .= " and sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.checked_sentiment  = '".$filter_sentiment."'";
}

  $query_comment = "SELECT count(*) total_comment ".
  "FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";
  $query_comment_result = DB::select($query_comment);

$query = " SELECT sum(Liked) Liked,sum(Love) Love,sum(Haha) Haha,sum(Angry) Angry,sum(Sad) Sad,sum(Wow) Wow,count(*)  post_count, DATE_FORMAT(posts.created_time, '".$group_period."') created_time, ".
" SUM(replace(shared, ',', '')) shared FROM temp_inbound_posts posts WHERE posts.visitor <> 1 AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ". $filter_pages.  $add_con . 
" GROUP BY DATE_FORMAT(posts.created_time, '".$group_period."') ORDER BY DATE(created_time) DESC " ;

// echo $query;
$query_result = DB::select($query);


   $data = [];
   $total=0;

    foreach ($query_result as  $key => $row) {

                $datetime = $row ->created_time;
                $periodLabel ="";
                $weekcount ="";
                if($groupType == "week")
                {
                  $weekperiod=$this ->rangeWeek ($datetime);
                  $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
                 if (!array_key_exists($periodLabel , $data)) {
                  $data[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'Wow' =>  $row ->Wow,
                'Love' => $row ->Love,
                'Angry' => $row ->Angry,
                'Sad' => $row ->Sad,
                'Haha' => $row ->Haha,
                'Like' =>$row ->Liked,
                'periods' => $periodLabel,
                'shared' =>$row ->shared,
                'post_count' =>$row ->post_count,
            );
                                                                }
                else
                {
$data[$periodLabel]['Wow'] = (int) $data[$periodLabel]['Wow'] + (int) $row ->Wow;
$data[$periodLabel]['Love'] = (int) $data[$periodLabel]['Love'] + (int)$row ->Love;
$data[$periodLabel]['Angry'] = (int) $data[$periodLabel]['Angry'] +(int) $row ->Angry;
$data[$periodLabel]['Sad'] = (int) $data[$periodLabel]['Sad'] + (int) $row ->Sad;
$data[$periodLabel]['Haha'] = (int) $data[$periodLabel]['Haha'] + (int) $row ->Haha;
$data[$periodLabel]['Like'] = (int) $data[$periodLabel]['Like'] + (int) $row ->Liked;
$data[$periodLabel]['periods'] =  $datetime;
$data[$periodLabel]['shared'] =(int) $data[$periodLabel]['shared'] +(int)$row ->shared;
$data[$periodLabel]['post_count'] =(int) $data[$periodLabel]['post_count'] +(int)$row ->post_count;

                }
   
 
                }
    else 
    {
                 $data[$datetime]['Wow'] = $row ->Wow;
                 $data[$datetime]['Love'] =$row ->Love;
                 $data[$datetime]['Angry'] =$row ->Angry;
                 $data[$datetime]['Sad'] = $row ->Sad;
                 $data[$datetime]['Haha'] =$row ->Haha;
                 $data[$datetime]['Like'] =$row ->Liked;
                 $data[$datetime]['periods'] =  $datetime;
                 $data[$datetime]['periodLabel'] =  $datetime;
                 $data[$datetime]['shared'] =$row ->shared;
                 $data[$datetime]['post_count'] =$row ->post_count;

    }
                
              
                 
               
            }


//$data = $this->unique_multidim_array($data,'periodLabel'); 

    echo json_encode(array($data,$query_comment_result,$color));


}

public function getInboundsentiment()
    {
     //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');

      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      // $brand_id=17;
      // $groupType='day';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

   }
          //   if(null !== Input::get('filter_page_name'))
          //  {
          //   $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
          //   $filter_pages= " page_name in ('".$filter_page_name."') ";
          //  }
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
            $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
            $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
           }
           else
           {

              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

 // dd($filter_pages);
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 09 17')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 17')));

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con="";
if($groupType=="month")
{
$group_period =" DATE_FORMAT(cmts.created_time, '%Y-%m')"; 
 }

   else 
 {
$group_period =" DATE_FORMAT(cmts.created_time, '%Y-%m-%d')";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and cmts.message Like '%".$filter_keyword."%'";
}

$query = "SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,".$group_period." created_time  " .
 "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND ".$filter_pages.
" AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con.
"GROUP BY ".$group_period .
" ORDER BY DATE(cmts.created_time) ASC";
// echo $query;
// return;

$query_result = DB::select($query);

   $data = [];
   $total=0;

   $data_sentiment = [];
   $data_mention = [];
   $total=0;
   $data_message = [];

    foreach ($query_result as  $key => $row) {
          
              $utcdatetime = $row->created_time;
         
               
               $pos =$row->positive;
               $neg =$row->negative;
               $nan =$row->neutral;
          
                $request['periodLabel'] ="";
      
                if($groupType == "week")
{
    $weekperiod=$this ->rangeWeek ($utcdatetime);
    $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
    
 if (!array_key_exists($periodLabel , $data_sentiment)) {
    $data_sentiment[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'positive' => $pos,
                'negative' => $neg,
                'neutral' => $nan,
                'periods' =>$utcdatetime,
            );
}
else
{
 $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
 $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
 $data_sentiment[$periodLabel]['neutral'] = $data_sentiment[$periodLabel]['neutral'] + $nan;
 $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
}
 
                  


}
 else
{
 $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
 $data_sentiment[$utcdatetime]['positive'] = $pos;
 $data_sentiment[$utcdatetime]['negative'] = $neg;
 $data_sentiment[$utcdatetime]['neutral'] =  $nan;
 $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

}

                
   }

 //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 

    echo json_encode($data_sentiment);


}

      
      public function getTagmultipleGen($tag,$tbl)
      {
        $filter_tag='';
         $tag=explode(',', $tag);
           foreach ($tag as $key => $value) {
            
               if($filter_tag == '')
            {
              $filter_tag .=" FIND_IN_SET('".$value."', ".$tbl.".checked_tags_id) >0";
            }
            else
            {
              
              $filter_tag .=" and FIND_IN_SET('".$value."', ".$tbl.".checked_tags_id) >0";
             
            }
            
           }
           $filter_tag  = "  (" . $filter_tag . ")"; 
           return $filter_tag;

      }

      public function getPostTagQty()
      {
          $brand_id=Input::get('brand_id');
          $page_id = '';
          $groupType=Input::get('periodType');
          $filter_keyword ='';
          if(null !== Input::get('keyword'))
          $filter_keyword=Input::get('keyword'); 
          if(null !== Input::get('fday'))
          {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
            $dateBegin=date('Y-m-d');
          }
          if(null !==Input::get('sday'))
          {

           $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          else
          {
            $dateEnd=date('Y-m-d');
          }
         $color = [];
         if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
         {
          $customName= DB::select("Select * from page_rename where origin_name = '".Input::get('admin_page')."' OR custom_name = '".Input::get('admin_page')."'");
          foreach($customName as $name) {$page_name = $name->origin_name;}

          $filter_page_name=$this->Format_Page_name_single($page_name);
          $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
          $color = $this->getbrandcolor($filter_page_name);



          $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);

          foreach ($InboundPages_result as  $key => $row) {
              if(isset($row['id'])) $page_id= $row['id'];
          }
         }
         else
         {
            $my_pages= $this->getOwnPage($brand_id);
            $my_pages=explode(',', $my_pages);
            $filter_pages=$this->getPageWhereGen($my_pages);
         }

         $dateformat ="";
         $add_post_con=" and 1=1 ";
         $add_cmt_con =" and 1=1 ";
         $add_comment_con="";
          if($groupType=="month")
          {
            $dateformat ="%Y-%m"; 
          }
          else 
          {
            $dateformat ="%Y-%m-%d";   
          }
          if(null !== Input::get('tags')){

            $tags_id=Input::get('tags');//implode(",",Input::post('checked_tags_id'));

            $add_post_con .= " and " . $this->getTagWhereOrGen($tags_id,'posts');
            //$add_cmt_con .= " and " . $this->getTagWhereGen($tags_id,'cmts');
            /*$add_post_con .= " and " . $this->getTagWhereGen(Input::get('tag_id'),'posts'); 
            $add_cmt_con .= " and " . $this->getTagWhereGen(Input::get('tag_id'),'cmts'); */
           }

           $query = "SELECT count(*) tagcount,DATE_FORMAT(posts.created_time,'".$dateformat."') created_time " .
                    "FROM temp_inbound_posts posts ".
                    " WHERE  posts.id IS NOT NULL AND posts.visitor <> 1 AND ".$filter_pages.
                    " AND  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_post_con .
                    " ";
           $query_result = DB::select($query);
             
           $data = [];
           // dd($groupType);
           foreach ($query_result as  $key => $row) {
              // $request['periods'] =$row ->created_time;
              $request['qty_count'] =$row ->tagcount;
              $request['color'] =$color;

              // $request['periodLabel'] ="";
            
 

          }
          $data[] = $request;
            //  usort($data, function($a1, $a2) {
            //  $value1 = strtotime($a1['periods']);
            //  $value2 = strtotime($a2['periods']);
            //  return $value1 - $value2;
            // });

            echo json_encode($data);
      }

public function getTagQty()
    {
        
          $brand_id=Input::get('brand_id');
          $page_id = '';
          $groupType=Input::get('periodType');
          $filter_keyword ='';
          if(null !== Input::get('keyword'))
           $filter_keyword=Input::get('keyword'); 


             // $brand_id=34;
              // $groupType='day';
              if(null !== Input::get('fday'))
              {
                $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
                $dateBegin=date('Y-m-d');
              }
            

              if(null !==Input::get('sday'))
              {

               $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
              }
              
              else
              {
                $dateEnd=date('Y-m-d');
              }
            $color = [];
      
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {

            $customName= DB::select("Select * from page_rename where origin_name = '".Input::get('admin_page')."' OR custom_name = '".Input::get('admin_page')."'");
            foreach($customName as $name) {$page_name = $name->origin_name;}

            $filter_page_name=$this->Format_Page_name_single($page_name);
            $color = $this->getbrandcolor($filter_page_name);



            $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
            $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                        $page_id = '';$imgurl = '';
                        foreach ($InboundPages_result as  $key => $row) {
                         
                          if(isset($row['id'])) $page_id= $row['id'];
                          if(isset($row['imageurl'])) $imgurl= $row['imageurl'];



                        }
               
           }
           else
           {

              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

               // dd($filter_pages);
              // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
              // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));

             $dateformat ="";
             $add_post_con=" and 1=1 ";
             $add_cmt_con =" and 1=1 ";
             $add_comment_con="";
              if($groupType=="month")
              {
                $dateformat ="%Y-%m"; 
              }

              else 
              {
                $dateformat ="%Y-%m-%d";                
                

              }
            
              if(null !== Input::get('tags')){
              $add_post_con .= " and " . $this->getTagWhereOrGen(Input::get('tags'),'posts'); 
             $add_cmt_con .= " and " . $this->getTagWhereOrGen(Input::get('tags'),'cmts'); 
           }
// dd($add_post_con);

          

            $query = "SELECT DATE_FORMAT(cmts.created_time,'".$dateformat."') created_time, sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA  " .
             "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
              " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND   ".$filter_pages.
              " AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_cmt_con .
              "Group By DATE_FORMAT(cmts.created_time,'".$dateformat."')";
            

              $query_result = DB::select($query);
              // dd($query_result);
              $data = [];
              foreach ($query_result as  $key => $row) {
              
              $request['periods'] =$row ->created_time;
              $request['qty_count'] =(int)$row ->positive+(int)$row ->negative+(int)$row ->neutral+(int)$row ->NA;
              $request['periodLabel'] ="";
              $request['color'] =$color;

              
              
              
              if($groupType == "week")
              {
                $weekperiod=$this ->rangeWeek ($request['periods']);
                $request['periodLabel'] =$weekperiod['start'] . " - " . $weekperiod['end'] ;
                $periodLabel = $request['periodLabel'];
                if (!array_key_exists($periodLabel , $data)) {
                  $data[$periodLabel] = array(
                    'periodLabel' => $periodLabel,
                    'mention' =>$request['qty_count'],
                    'periods' =>$request['periods'],
                    'page_id' =>$page_id,
                    'imgurl' =>$imgurl,
                    'color' =>$color,
                  );
                }
                else
                {
                 $data_mention[$periodLabel]['qty_count'] = (int) $data_mention[$periodLabel]['qty_count'] +  $request["qty_count"];
                 $data_mention[$periodLabel]['periods'] = $request['periods'];
               }

             }
             else
             {
               $request['periodLabel'] =$request['periods'] ;
               $periodLabel = $request['periodLabel'];
               if (!array_key_exists($periodLabel , $data)) {
                $data[$periodLabel] = array(
                  'periodLabel' => $periodLabel,
                  'qty_count' =>   $request['qty_count'],
                  'periods' =>  $request['periods'],
                  'page_id' =>$page_id,
                  'imgurl' =>$imgurl,
                  'color' =>$color,

                );
              }
              else
              {
               $data_mention[$periodLabel]['qty_count'] = (int) $data_mention[$periodLabel]['qty_count'] +  $request["qty_count"];
               $data_mention[$periodLabel]['periods'] = $request['periods'];
               $data_mention[$periodLabel]['color'] = $request['periods'];

             }
             
            }
            }
            usort($data, function($a1, $a2) {
             $value1 = strtotime($a1['periods']);
             $value2 = strtotime($a2['periods']);
             return $value1 - $value2;
            });

            echo json_encode($data);


}
public function getCampaignTagSentiment()
{
            //get keywork
        //  $keyword_data = ProjectKeyword::find(1);//projectid
          $brand_id=Input::get('brand_id');
          $campaign_filter_con = '';
          if(null !== Input::get('campaign_id'))
          {
            $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
              if(!empty($campaign_keyword)){
              $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
              $campaign_filter_con = " and  ".$campaign_filter_con;
             }
          }

          $groupType=Input::get('periodType');
          $filter_keyword ='';
          // if(null !== Input::get('keyword'))
          //  $filter_keyword=Input::get('keyword'); 


             //$brand_id=17;
              // $groupType='day';
           if(null !== Input::get('fday'))
              {
                $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
                $dateBegin=date('Y-m-d');
              }
            

            if(null !==Input::get('sday'))
            {

               $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
            
            else
            {
               $dateEnd=date('Y-m-d');
           }
   

         
         $add_con=" AND 1=1";
         $add_comment_con="";

        if($filter_keyword !== '')
        {
          $add_con .= " and cmts.message Like '%".$filter_keyword."%'";
        }
        if(null !== Input::get('tag_id'))
        $add_con .= " and " . $this->getTagWhereGen(Input::get('tag_id'),'cmts'); 

        // $query = "SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA,checked_tags_id tags  " .
        //  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        //   " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.tag_flag=1 AND checked_tags_id<>'' ".
        // " AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con . $campaign_filter_con .
        // " Group By checked_tags_id";

         /*$query = "SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA,cmts.checked_tags_id tags  " .
         "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
          " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND posts.campaign is not null AND cmts.parent='' AND cmts.tag_flag=1 AND cmts.checked_tags_id<>'' ".
        " AND  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con . $campaign_filter_con .
        " Group By cmts.checked_tags_id";*/

        $query = "SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA,cmts.checked_tags_id tags  " .
         "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
          " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND posts.campaign is not null AND cmts.parent='' AND cmts.tag_flag=1 AND cmts.checked_tags_id<>'' ".
          $add_con . $campaign_filter_con . " Group By cmts.checked_tags_id";

        // echo $query;
        //  return;

      $query_result = DB::select($query);
      // dd($query_result);
         $data = [];
         $total=0;

         $data_tag = [];
         $total=0;
        if(count($query_result)>0)
        {
                foreach ($query_result as  $key => $row) {
                      
                                      
                           $pos =$row->positive;
                           $neg =$row->negative;
                           $neutral =(int)$row->neutral + (int)$row->NA;
                           $tags =$row->tags;
                    // if( (int) $pos !== 0 || (int) $neg !== 0)
                    // {
                        $arr_tag=explode(',', $tags);
                      
                           $request['tagLabel'] ="";
                  foreach ($arr_tag as  $key => $tag) {
                    if($tag <> '')
                    {
                      $tagInfo=$this->gettagsByid($tag);
                      if(count($tagInfo)>0)
                      {
                        foreach ($tagInfo as  $key => $taginfo) {
                        if (!array_key_exists($taginfo->id , $data_tag)) {
                          $data_tag[$tag] = array(
                                      'tagLabel' => $taginfo->name,
                                      'tagId' => $taginfo->id,
                                      'positive' => $pos,
                                      'negative' => $neg,
                                      'neutral' => $neutral,
                                      
                                     
                                  );
                        }
                        else
                        {
                         $data_tag[$tag]['positive'] = (int) $data_tag[$tag]['positive'] + $pos;
                         $data_tag[$tag]['negative'] = (int) $data_tag[$tag]['negative'] + $neg;
                         $data_tag[$tag]['neutral'] = (int) $data_tag[$tag]['neutral'] + $neutral;
                     
                        }
                      }
                     }
                    }
                  }
                    // }
                         

          

       


                      
         }
        } 
        else
        {
            $data_tag['notag'] = array(
                                      'tagLabel' => '',
                                      'tagId' => '',
                                      'positive' => 0,
                                      'negative' => 0,
                                      'neutral' => 0,
                                      // 'page_id' => $page_id,
                                     
                                  );
        } 

          

   //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 

      // dd($data_tag);
      echo json_encode($data_tag);
}
public function getTagSentiment()
    {
         //get keywork
        //  $keyword_data = ProjectKeyword::find(1);//projectid
          $brand_id=Input::get('brand_id');
          $page_id = '';
          $groupType=Input::get('periodType');
          $filter_keyword ='';
          if(null !== Input::get('keyword'))
           $filter_keyword=Input::get('keyword'); 


             //$brand_id=17;
              // $groupType='day';
           if(null !== Input::get('fday'))
              {
                $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
                $dateBegin=date('Y-m-d');
              }
            

            if(null !==Input::get('sday'))
            {

               $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
            
            else
            {
               $dateEnd=date('Y-m-d');
           }
          //   if(null !== Input::get('filter_page_name'))
          //  {
          //   $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
          //   $filter_pages= " page_name in ('".$filter_page_name."') ";
          //  }
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
            $customName = DB::table('page_rename')->where('origin_name',Input::get('admin_page'))->orWhere('custom_name',Input::get('admin_page'))->get();
            foreach($customName as $name) {$page_name = $name->origin_name;}

            $filter_page_name=$this->Format_Page_name_single($page_name);
            

            $filter_pages= " cmts.page_name in ('". $filter_page_name."') ";
            $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                      $page_id = '';$imgurl ='';
                        foreach ($InboundPages_result as  $key => $row) {
                         
                          if(isset($row['id'])) $page_id= $row['id'];
                          if(isset($row['imageurl'])) $imgurl= $row['imageurl'];


                        }
               
           }
           else
           {

              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

         // dd($filter_pages);
        // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
        // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));

         // $groupType="month";
         
         $add_con=" and 1=1 ";
         $add_comment_con="";

        if($filter_keyword !== '')
        {
          $add_con .= " and cmts.message Like '%".$filter_keyword."%'";
        }
        if(null !== Input::get('tags'))
        $add_con .= " and " . $this->getTagWhereOrGen(Input::get('tags'),'cmts'); 
// dd($add_con);
        $query = "SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA,cmts.checked_tags_id tags  " .
         "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
          " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND  cmts.checked_tags_id <>'' AND ".$filter_pages.
        " AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con .
        " and (cmts.checked_sentiment = 'neg' OR cmts.checked_sentiment = 'pos') Group By cmts.checked_tags_id";
        // echo $query;
        //  return;

      $query_result = DB::select($query);
      // dd($query_result);
         $data = [];
         $total=0;
          $imgurl='';
         $data_tag = [];
         $total=0;
        if(count($query_result)>0)
        {
                foreach ($query_result as  $key => $row) {
                      
                                      
                           $pos =$row->positive;
                           $neg =$row->negative;
                           $neutral =(int)$row->neutral + (int)$row->NA;
                           $tags =$row->tags;
                    // if( (int) $pos !== 0 || (int) $neg !== 0)
                    // {
                        $arr_tag=explode(',', $tags);
                      
                           $request['tagLabel'] ="";
                  foreach ($arr_tag as  $key => $tag) {
                    if($tag <> '')
                    {
                      $tagInfo=$this->gettagsByid($tag);
                      if(count($tagInfo)>0)
                      {
                        foreach ($tagInfo as  $key => $taginfo) {
                        if (!array_key_exists($taginfo->id , $data_tag)) {
                          $data_tag[$tag] = array(
                                      'tagLabel' => $taginfo->name,
                                      'tagId' => $taginfo->id,
                                      'positive' => $pos,
                                      'negative' => $neg,
                                      'neutral' => $neutral,
                                      'page_id' => $page_id,
                                      'imgurl'=>$imgurl,
                                     
                                  );
                        }
                        else
                        {
                         $data_tag[$tag]['positive'] = (int) $data_tag[$tag]['positive'] + $pos;
                         $data_tag[$tag]['negative'] = (int) $data_tag[$tag]['negative'] + $neg;
                         $data_tag[$tag]['neutral'] = (int) $data_tag[$tag]['neutral'] + $neutral;
                     
                        }
                      }
                      }
                      }
                  }
                    // }
                         

          

       


                      
         }
        } 
        else
        {
            $data_tag['notag'] = array(
                                      'tagLabel' => '',
                                      'tagId' => '',
                                      'positive' => 0,
                                      'negative' => 0,
                                      'neutral' => 0,
                                      'page_id' => $page_id,
                                      'imgurl'=>$imgurl,
                                     
                                  );
        } 

          

   //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 

      // dd($data_tag);
      echo json_encode($data_tag);


}
public function getPostTagCount(Request $request)
    {
      
      $brand_id=Input::get('brand_id');
      $campaign_filter_con = '';
      if(null !== Input::get('campaign_id'))
      {
        $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
        $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
        $campaign_filter_con = " and ".$campaign_filter_con ;
        // dd($campaign_filter_con);
      }
      else
      {
         $campaign_filter_con = ' AND 1=2';
      }
      // dd($campaign_filter_con);

          
         $period_type = '';
         $page_id='';$imgurl = '';
        $additional_filter=" AND 1=1 ";$additional_filter_post=" AND 1=1 ";
      
            if(null !==Input::get('period'))
            {
            $period=Input::get('period');
            $char_count = substr_count($period,"-");
            // dd($char_count);
            $format_type = '';
            if($char_count == 1)
            {
                $format_type ='%Y-%m';
            }
            else if($char_count == 2)
            {
                 $format_type ='%Y-%m-%d';
            }   
            else if($char_count > 2)
            {
                /*this is week*/
                $pieces = explode(" - ", $period);
                $format_type ='%Y-%m-%d';
                $period_type ='week';
            }
            // dd($period_type);

          
    if($period_type == 'week')
    {
     $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') "; 
     $additional_filter_post .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";      

    }
    else
    {
        $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
        $additional_filter_post .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
        // $additional_filter_post .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";   
    }

           }
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

       $keyword_con='';
       if('1' == Input::get('keyword_flag'))
       {
         $keyword_data = $this->getprojectkeyword($brand_id);
         if(isset($keyword_data [0]['main_keyword']))
         $keyword_con  =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'posts'). ")"; // param (data_arr,table alais)
         else
         $keyword_con = ' AND 1=2';
        // dd( $keyword_con);
       }

       
             // $brand_id=17;
              // $groupType='day';
         if(null !== Input::get('fday'))
              {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
              $dateBegin=date('Y-m-d');
              }
            

            if(null !==Input::get('sday'))
            {

         $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
            
            else
            {
        $dateEnd=date('Y-m-d');

           }
           $filter_pages = ' 1=1 ';     
      // if(null !== Input::get('filter_page_name'))
      //      {
      //         $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
      //         $filter_pages= " and page_name in ('".$filter_page_name."') ";
            
            
      //      }
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             //get all page with no filter
            if("MFS" == Input::get('admin_page'))
            {
              // $this->getInboundPages($brand_id);
               $my_pages =DB::table('projects')->select('monitor_pages')->where('id',$brand_id)->get();
               foreach($my_pages as $pg){
                $all_pages = $pg->monitor_pages;
               }

              // dd($all_pages);
              
              $pagesArr = explode(',', $all_pages);
             
               
               $industries = DB::table('industry')->where('type','MFS')->get()->toArray();
               foreach($industries as $ind){
                $ind_pages[] = $ind->origin_name;
               }
 
                $my_arr [] =array_intersect($pagesArr, $ind_pages); 
                // dd($my_arr);
                // $my_pages = "'" . implode ( "', '", $my_arr[0] ) . "'";
                // dd($my_pages);
              // $my_pages=['ongoemoney','KBZPay','WaveMoney','truemoneymyanmar','okdollarapp','mykyat.myanmar','agdpay','MABMobileBank','easypaymobilemoney','MPitesan','CBBankMobileBanking','MytelPayOfficial','MPTMoneyOfficial'];
              
              $filter_pages .= ' and ' .$this->getPageWhereGen($my_arr[0]);
              // dd($filter_pages);
            }
            elseif("Bank" == Input::get('admin_page')){
                           $my_pages =DB::table('projects')->select('monitor_pages')->where('id',$brand_id)->get();
               foreach($my_pages as $pg){
                $all_pages = $pg->monitor_pages;
               }

              // dd($all_pages);
              
              $pagesArr = explode(',', $all_pages);
             
               
               $industries = DB::table('industry')->where('type','Bank')->get()->toArray();
               foreach($industries as $ind){
                $ind_pages[] = $ind->origin_name;
               }
 
                $my_arr [] =array_intersect($pagesArr, $ind_pages); 
                // dd($my_arr);
              // $my_pages=['MABBankMyanmar','KanbawzaBank','CBBankmyanmar','ayabank','AGDBank','yomabank','jcbcardmyanmar','UnionPayInternationalMM','MyanmarPaymentUnion','VisaMM','uabbankmm','abankmyanmar','shwebank','MyanmaTourismBank','CHIDBank','MyanmarCitizensBankLtd','firstprivatebank','mobbankmyanmar','aybbank','tunfoundationbanklimited','GlobalTreasureBank','mmbmyanmar','gloryfarmerbank','MyanmaTourismBank','mineraldevelopmentbank'];
              $filter_pages .= ' and ' .$this->getPageWhereGen($my_arr[0]);

            }
            elseif("Card Network" == Input::get('admin_page')){
                           $my_pages =DB::table('projects')->select('monitor_pages')->where('id',$brand_id)->get();
               foreach($my_pages as $pg){
                $all_pages = $pg->monitor_pages;
               }

              // dd($all_pages);
              
              $pagesArr = explode(',', $all_pages);
             
               
               $industries = DB::table('industry')->where('type','Card')->get()->toArray();
               foreach($industries as $ind){
                $ind_pages[] = $ind->origin_name;
               }
 
                $my_arr [] =array_intersect($pagesArr, $ind_pages); 
                // dd($my_arr);
              // $my_pages=['MABBankMyanmar','KanbawzaBank','CBBankmyanmar','ayabank','AGDBank','yomabank','jcbcardmyanmar','UnionPayInternationalMM','MyanmarPaymentUnion','VisaMM','uabbankmm','abankmyanmar','shwebank','MyanmaTourismBank','CHIDBank','MyanmarCitizensBankLtd','firstprivatebank','mobbankmyanmar','aybbank','tunfoundationbanklimited','GlobalTreasureBank','mmbmyanmar','gloryfarmerbank','MyanmaTourismBank','mineraldevelopmentbank'];
              $filter_pages .= ' and ' .$this->getPageWhereGen($my_arr[0]);

            }
            else{
              $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));

              $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
              foreach($customName as $name) $filter_page_name = $name->origin_name;

              $filter_pages .= " and posts.page_name in ('". $filter_page_name."') ";
              $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                      
                      $page_id = '';$imgurl='';
                        foreach ($InboundPages_result as  $key => $row) {
                         
                          if(isset($row['id'])) $page_id= $row['id'];
                           if(isset($row['imageurl'])) $imgurl= $row['imageurl'];


                        }
            }

           }
           else
           {
            $my_pages = $this->getInboundPages($brand_id);
            $filter_pages .= ' and posts.page_name In('.$my_pages.')';

            // $my_pages= $this->getOwnPage($brand_id);
            // $my_pages=explode(',', $my_pages);

            // if  ('1' == Input::get('other_page'))
            // $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
            // else
            // $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages); 
           }


       // dd($filter_pages);
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));

       $add_con="";
       $add_comment_con="";
       $CmtType = "";
        if(null !== Input::get('CmtType'))
        { 
          $CmtType = " and cmts.checked_sentiment='".Input::get('CmtType')."'";
        }

       $tag_con = '';$post_tag_con='';
       if(null !== Input::get('search_tag'))
      {
        // $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
         $tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'cmts'); 
         $post_tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'posts');
      }

      if($filter_keyword !== '')
      {
        $add_con = " and cmts.message Like '%".$filter_keyword."%'";
      }

      if(null !== Input::get('post_id') )
      {
        $add_con .="   and posts.id ='" .Input::get('post_id'). "'";
        
        
      }
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = '  1=1 ';
        $date_filter_post = '  1=1 ';
      }
      else
      {
        $date_filter =" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        $date_filter_post =" AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
      }
      $tagOrder = Input::get('tagOrder');
      $fieldName = Input::get('fieldName'); 

      // if(null !== $tagOrder && null !== $fieldName){
      //   if($fieldName == 'name') $order_con = " ORDER by "

      // }
     $add_global_filter ='';
     if(null !== Input::get('tsearch_tag'))
     {
        $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'),'posts');
     }
     if(null !== Input::get('tsearch_senti'))
     {
        $add_global_filter .=" and " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
     }



        if(null !== Input::get('wordcloud_text'))
        { 
            $highlight_cmt_con='';
            $wordcloud_text = $request->session()->get( Input::get('wordcloud_text'));

            $wordcloud_text = implode(',',$wordcloud_text);
            $wordcloud_text = "'" . str_replace(",", "','", $wordcloud_text) . "'";
            $highlight_cmt_con = " and cmts.id in (".$wordcloud_text.")";
           
            $query = " SELECT cmts.checked_tags_id  tags " .
         "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
          " on posts.id=cmts.post_id  WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.checked_tags_id<>'' AND ".$filter_pages.
          " AND " . $date_filter . $add_con . $highlight_cmt_con . $CmtType . $tag_con;
        }
        elseif(null !== Input::get('campaign_name'))
        {
          // dd($additional_filter);
             $query = " SELECT cmts.checked_tags_id  tags " .
       "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.checked_tags_id<>'' ".
      " AND " . $date_filter . $campaign_filter_con . $add_con  . $additional_filter  ;
      // dd($query);
        }
        elseif(null !== Input::get('type') && 'inbound_post' == Input::get('type'))
        {
          // dd($additional_filter);
             $query = " SELECT posts.checked_tags_id  tags " .
       "FROM temp_inbound_posts posts ".
        " WHERE  posts.id IS NOT NULL  AND  posts.checked_tags_id <>'' ".
      " AND " . $filter_pages  . " AND " .  $date_filter_post  .  $add_con  .  $additional_filter_post . $post_tag_con;
      // dd($query);
        }
        else
        {
          if(null !== Input::get('highlight_text'))
        { 
          $highlight_text= str_replace(' ', '', Input::get('highlight_text'));
          $add_con = " and ( REPLACE(REPLACE(REPLACE(cmts.message, '\r', ''), '\n', ''),' ','')) Like '%".$highlight_text."%' ";
        
        }
        // dd("hey");
          $query = " SELECT posts.checked_tags_id  tags " .
       "FROM  temp_inbound_posts posts ".
        " WHERE  posts.id IS NOT NULL AND  posts.visitor=0 AND  posts.checked_tags_id <> '' AND ". $filter_pages.
      "  " . $date_filter_post . $add_con  .  $keyword_con . $additional_filter . $CmtType .$tag_con . $add_global_filter ;
      
      // dd($query);
        }
// dd($query);

      //  return;

    $query_result = DB::select($query);
// dd($query_result);
       $data = [];
       $total=0;

       $data_tag = [];
       $total=0;
      

        foreach ($query_result as  $key => $row) {
              
       
                   $tags =$row->tags;
            
                $arr_tag=explode(',', $tags);
              
                   $request['tagLabel'] ="";
      
          foreach ($arr_tag as  $key => $tag) {
            if($tag <> '')
            {
              $tagInfo=$this->gettagsByid($tag);
                if(count($tagInfo)>0)
                    {
                     

                      foreach ($tagInfo as  $key => $taginfo) {

                      if (!array_key_exists($taginfo->id, $data_tag)) { 
                        $data_tag[$taginfo->id] = array(
                                    'tagLabel' =>$taginfo->name,
                                    'tagId' =>$taginfo->id,
                                    'page_id'=>$page_id,
                                    'imgurl'=>$imgurl,
                                    'tagCount' => 1,
                                 
                                   
                                );
                        }
                        else
                      {
                          // dd($taginfo->id);
                       
                       $data_tag[$taginfo->id]['tagCount'] = (int) $data_tag[$taginfo->id]['tagCount'] + 1;
                           
                      }
                      }
                    
                      
                    }

            }
                    
                
          }
      
             

    

 


                
         }
// dd($data_tag);
       //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
// dd($fieldName);
        if(null !== $tagOrder && null !== $fieldName){

         if($fieldName == 'name' && $tagOrder == 'ASC'){
          // dd('hy');
            usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?-1:1;
          });

        }
             
          if($fieldName == 'name' && $tagOrder == 'DESC'){
               usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?1:-1;
          });
               array_reverse($data_tag);
               

           
              // dd($data_tag);
          }
           if($fieldName == 'count' && $tagOrder == 'ASC'){
             $price = array_column($data_tag, 'tagCount');
            array_multisort($price, $data_tag);

          //   usort($data_tag, function($a, $b) {
          //   if($a['tagCount']==$b['tagCount']) return 0;
          //   return $a['tagCount'] < $b['tagCount']?1:-1;
          // });

        }
                 if($fieldName == 'count' && $tagOrder == 'DESC'){
            usort($data_tag, function($a, $b) {
            if($a['tagCount']==$b['tagCount']) return 0;
            return $a['tagCount'] < $b['tagCount']?1:-1;
          });

        }

   


      }
        else{
           usort($data_tag, function($a, $b) {
          if($a['tagCount']==$b['tagCount']) return 0;
          return $a['tagCount'] < $b['tagCount']?1:-1;
      });
         }

           if(null === Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, 5, true);
           else if ("no" !== Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, (int) Input::get('limit'), true);

            
           echo json_encode($data_tag);


}
public function getcmtTagCount(Request $request)
    {
      
      $brand_id=Input::get('brand_id');
      $campaign_filter_con = '';
      if(null !== Input::get('campaign_id'))
      {
        $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
        $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'cmts');
        $campaign_filter_con = " and ".$campaign_filter_con ;
      }
      else
      {
         $campaign_filter_con = ' AND 1=2';
      }
     
         $period_type = '';
         $page_id='';$imgurl = '';
         $additional_filter=" AND 1=1 ";$additional_filter_post=" AND 1=1 ";
      
            if(null !==Input::get('period'))
            {
            $period=Input::get('period');
            $char_count = substr_count($period,"-");
            $format_type = '';
            if($char_count == 1)
            {
                $format_type ='%Y-%m';
            }
            else if($char_count == 2)
            {
                 $format_type ='%Y-%m-%d';
            }   
            else if($char_count > 2)
            {
                /*this is week*/
                $pieces = explode(" - ", $period);
                $format_type ='%Y-%m-%d';
                $period_type ='week';
            }
          
            if($period_type == 'week')
            {
              $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') "; 
              $additional_filter_post .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";      

            }
            else
            {
              $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
              $additional_filter_post .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
            
            }

           }
          $groupType=Input::get('periodType');
          $filter_keyword ='';
          if(null !== Input::get('keyword')) $filter_keyword=Input::get('keyword'); 

          $keyword_con='';
          if('1' == Input::get('keyword_flag'))
          {
             $keyword_data = $this->getprojectkeyword($brand_id);
             if(isset($keyword_data [0]['main_keyword']))
             $keyword_con  =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'cmts'). ")"; // param (data_arr,table alais)
             else
             $keyword_con = ' AND 1=2';
          }

          if(null !== Input::get('fday'))
          {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
          $dateBegin=date('Y-m-d');
          }

          if(null !==Input::get('sday'))
          {
            $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          else
          {
            $dateEnd=date('Y-m-d');
          }
           $filter_pages = ' 1=1 ';     
      
          if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
          {
            if("MFS" == Input::get('admin_page'))
            {
               // $my_pages =DB::table('projects')->select('monitor_pages')->where('id',$brand_id)->get();
               // foreach($my_pages as $pg){
               //  $all_pages = $pg->monitor_pages;
               // }
               // $pagesArr = explode(',', $all_pages);
               $industries = DB::table('industry')->where('type','MFS')->get()->toArray();
               foreach($industries as $ind){
                $my_arr[] = $ind->origin_name;
               }
                // $my_arr [] =array_intersect($pagesArr, $ind_pages); 
                // dd($my_arr);
                // $my_pages = "'" . implode ( "', '", $my_arr[0] ) . "'";
                // dd($my_pages);
                // $my_pages=['ongoemoney','KBZPay','WaveMoney','truemoneymyanmar','okdollarapp','mykyat.myanmar','agdpay','MABMobileBank','easypaymobilemoney','MPitesan','CBBankMobileBanking','MytelPayOfficial','MPTMoneyOfficial'];
              
               $filter_pages .= ' and ' .$this->getPageWhereGen_forcmt($my_arr);
               // $filter_pages .= ' and ' .$this->getPageWhereGen_forcmt($my_arr[0]);
            }
            elseif("Bank" == Input::get('admin_page')){
               
               $industries = DB::table('industry')->where('type','Bank')->get()->toArray();
               foreach($industries as $ind){
                $my_arr[] = $ind->origin_name;
               }

              // $my_pages=['MABBankMyanmar','KanbawzaBank','CBBankmyanmar','ayabank','AGDBank','yomabank','jcbcardmyanmar','UnionPayInternationalMM','MyanmarPaymentUnion','VisaMM','uabbankmm','abankmyanmar','shwebank','MyanmaTourismBank','CHIDBank','MyanmarCitizensBankLtd','firstprivatebank','mobbankmyanmar','aybbank','tunfoundationbanklimited','GlobalTreasureBank','mmbmyanmar','gloryfarmerbank','MyanmaTourismBank','mineraldevelopmentbank'];
              $filter_pages .= ' and ' .$this->getPageWhereGen_forcmt($my_arr);
              // dd($filter_pages);
            }
            elseif("Card Network" == Input::get('admin_page')){
               $industries = DB::table('industry')->where('type','Card')->get()->toArray();
               foreach($industries as $ind){
                $my_arr[] = $ind->origin_name;
               }
              // $my_pages=['MABBankMyanmar','KanbawzaBank','CBBankmyanmar','ayabank','AGDBank','yomabank','jcbcardmyanmar','UnionPayInternationalMM','MyanmarPaymentUnion','VisaMM','uabbankmm','abankmyanmar','shwebank','MyanmaTourismBank','CHIDBank','MyanmarCitizensBankLtd','firstprivatebank','mobbankmyanmar','aybbank','tunfoundationbanklimited','GlobalTreasureBank','mmbmyanmar','gloryfarmerbank','MyanmaTourismBank','mineraldevelopmentbank'];
              $filter_pages .= ' and ' .$this->getPageWhereGen_forcmt($my_arr);
            }
           
            else{
              $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));

              $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
              foreach($customName as $name) $filter_page_name = $name->origin_name;

              $filter_pages .= " and cmts.page_name in ('". $filter_page_name."') ";
              $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                      
              $page_id = '';$imgurl='';
              foreach ($InboundPages_result as  $key => $row) {
               
                if(isset($row['id'])) $page_id= $row['id'];
                if(isset($row['imageurl'])) $imgurl= $row['imageurl'];
              }
            }
          }
         else
         {
          $my_pages = $this->getInboundPages($brand_id);
          $filter_pages .= ' and cmts.page_name In('.$my_pages.')';
          
         }

        // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
        // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));
        // dd($filter_pages);
       $add_con="";
       $add_comment_con="";
       $CmtType = "";
        if(null !== Input::get('CmtType'))
        { 
          $CmtType = " and cmts.checked_sentiment='".Input::get('CmtType')."'";
        }

       $tag_con = '';$post_tag_con='';
       if(null !== Input::get('search_tag'))
       {
         $tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'cmts'); 
         $post_tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'posts');
       }

      if($filter_keyword !== '')
      {
        $add_con = " and cmts.message Like '%".$filter_keyword."%'";
      }

      if(null !== Input::get('post_id') )
      {
        $add_con .="   and posts.id ='" .Input::get('post_id'). "'";
      }
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = '  1=1 ';
        $date_filter_post = '  1=1 ';
      }
      else
      {
        $date_filter =" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        $date_filter_post =" (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
      }
      $tagOrder = Input::get('tagOrder');
      $fieldName = Input::get('fieldName'); 

       $add_global_filter ='';
       if(null !== Input::get('tsearch_tag'))
       {
          $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'),'cmts');
       }
        if(null !== Input::get('tsearch_senti'))
       {
          $add_global_filter .=" and " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
       }



        if(null !== Input::get('wordcloud_text'))
        { 
            $highlight_cmt_con='';
            $wordcloud_text = $request->session()->get( Input::get('wordcloud_text'));

            $wordcloud_text = implode(',',$wordcloud_text);
            $wordcloud_text = "'" . str_replace(",", "','", $wordcloud_text) . "'";
            $highlight_cmt_con = " and cmts.id in (".$wordcloud_text.")";
           
            $query = " SELECT cmts.checked_tags_id  tags " .
         "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
          " on posts.id=cmts.post_id  WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.checked_tags_id<>'' AND ".$filter_pages.
          " AND " . $date_filter . $add_con . $highlight_cmt_con . $CmtType . $tag_con;
        }
        elseif(null !== Input::get('campaign_name'))
        {
        
             $query = " SELECT cmts.checked_tags_id  tags " .
       "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.checked_tags_id<>'' ".
      " AND " . $date_filter . $campaign_filter_con . $add_con  . $additional_filter  ;

        }
        elseif(null !== Input::get('type') && 'inbound_post' == Input::get('type'))
        {

             $query = " SELECT posts.checked_tags_id  tags " .
       "FROM temp_inbound_posts posts ".
        " WHERE  posts.id IS NOT NULL  AND  posts.checked_tags_id <>'' ".
      " AND " . $filter_pages  . " AND " .  $date_filter_post  .  $add_con  .  $additional_filter_post . $post_tag_con;

        }
        else
        {
          if(null !== Input::get('highlight_text'))
        { 
          $highlight_text= str_replace(' ', '', Input::get('highlight_text'));
          $add_con = " and ( REPLACE(REPLACE(REPLACE(cmts.message, '\r', ''), '\n', ''),' ','')) Like '%".$highlight_text."%' ";
        
        }

          $query = " SELECT cmts.checked_tags_id  tags " .
       "FROM temp_inbound_comments cmts  WHERE   cmts.checked_tags_id<>'' AND ". $filter_pages.
      " AND " . $date_filter . $add_con  .  $keyword_con . $additional_filter . $CmtType .$tag_con . $add_global_filter ;

        }
    $query_result = DB::select($query);
       $data = [];
       $total=0;

       $data_tag = [];
       $total=0;
      

        foreach ($query_result as  $key => $row) {
          $tags =$row->tags;
          $arr_tag=explode(',', $tags);
          $request['tagLabel'] ="";
      
          foreach ($arr_tag as  $key => $tag) {
            if($tag <> '')
            {
              $tagInfo=$this->gettagsByid($tag);
              if(count($tagInfo)>0)
              {
                foreach ($tagInfo as  $key => $taginfo) {
                  if (!array_key_exists($taginfo->id, $data_tag)) { 
                    $data_tag[$taginfo->id] = array(
                                'tagLabel' =>$taginfo->name,
                                'tagId' =>$taginfo->id,
                                'page_id'=>$page_id,
                                'imgurl'=>$imgurl,
                                'tagCount' => 1,
                             
                               
                            );
                    }
                  else
                  {
                    $data_tag[$taginfo->id]['tagCount'] = (int) $data_tag[$taginfo->id]['tagCount'] + 1;
                  }
                }    
              }
            } 
          }
         }

        if(null !== $tagOrder && null !== $fieldName){

         if($fieldName == 'name' && $tagOrder == 'ASC'){
            usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?-1:1;
          });

        }
             
          if($fieldName == 'name' && $tagOrder == 'DESC'){
               usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?1:-1;
          });
               array_reverse($data_tag);
               

           
              // dd($data_tag);
          }
           if($fieldName == 'count' && $tagOrder == 'ASC'){
             $price = array_column($data_tag, 'tagCount');
            array_multisort($price, $data_tag);

          //   usort($data_tag, function($a, $b) {
          //   if($a['tagCount']==$b['tagCount']) return 0;
          //   return $a['tagCount'] < $b['tagCount']?1:-1;
          // });

        }
                 if($fieldName == 'count' && $tagOrder == 'DESC'){
            usort($data_tag, function($a, $b) {
            if($a['tagCount']==$b['tagCount']) return 0;
            return $a['tagCount'] < $b['tagCount']?1:-1;
          });

        }

   


      }
        else{
           usort($data_tag, function($a, $b) {
          if($a['tagCount']==$b['tagCount']) return 0;
          return $a['tagCount'] < $b['tagCount']?1:-1;
      });
         }

           if(null === Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, 5, true);
           else if ("no" !== Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, (int) Input::get('limit'), true);

            
           echo json_encode($data_tag);


}

public function getTagCount(Request $request)
    {
      // dd(Input::get('group_name'));
      $brand_id=Input::get('brand_id');
      $campaign_filter_con = '';
      if(null !== Input::get('campaign_id'))
      {
        $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
        $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'cmts');
        $campaign_filter_con = " and ".$campaign_filter_con ;
      }
      else
      {
         $campaign_filter_con = ' AND 1=2';
      }
      
         $period_type = '';
         $page_id='';$imgurl = '';
         $additional_filter=" AND 1=1 ";
         $additional_filter_post=" AND 1=1 ";
      
            if(null !==Input::get('period'))
            {
            $period=Input::get('period');
            $char_count = substr_count($period,"-");
            // dd($char_count);
            $format_type = '';
            if($char_count == 1)
            {
                $format_type ='%Y-%m';
            }
            else if($char_count == 2)
            {
                 $format_type ='%Y-%m-%d';
            }   
            else if($char_count > 2)
            {
                /*this is week*/
                $pieces = explode(" - ", $period);
                $format_type ='%Y-%m-%d';
                $period_type ='week';
            }
            // dd($period_type);

          
    if($period_type == 'week')
    {
     $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') "; 
     $additional_filter_post .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";      

    }
    else
    {
        $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
        $additional_filter_post .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'"; 
    }

           }
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

       $keyword_con='';
       if('1' == Input::get('keyword_flag'))
       {
         $keyword_data = $this->getprojectkeyword($brand_id);
         if(isset($keyword_data [0]['main_keyword']))
         $keyword_con  =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'cmts'). ")"; // param (data_arr,table alais)
         else
         $keyword_con = ' AND 1=2';
        // dd( $keyword_con);
       }

       
             // $brand_id=17;
              // $groupType='day';
         if(null !== Input::get('fday'))
              {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
              $dateBegin=date('Y-m-d');
              }
            

            if(null !==Input::get('sday'))
            {

         $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
            
            else
            {
        $dateEnd=date('Y-m-d');

           }
           $filter_pages = ' 1=1 ';     
      // if(null !== Input::get('filter_page_name'))
      //      {
      //         $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
      //         $filter_pages= " and page_name in ('".$filter_page_name."') ";
            
            
      //      }
    // dd( Input::get('admin_page'));
          
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             //get all page with no filter
            if("all" == Input::get('admin_page'))
            {
              $my_pages= $this->getComparisonPage($brand_id);
              $my_pages=explode(',', $my_pages[0]['monitor_pages']);
              $filter_pages .= ' and ' .$this->getPageWhereGen($my_pages);
            }

            else{
              $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));

              $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
              foreach($customName as $name) $filter_page_name = $name->origin_name;

              $filter_pages .= " and posts.page_name in ('". $filter_page_name."') ";
              $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                      
                      $page_id = '';$imgurl='';
                        foreach ($InboundPages_result as  $key => $row) {
                         
                          if(isset($row['id'])) $page_id= $row['id'];
                           if(isset($row['imageurl'])) $imgurl= $row['imageurl'];


                        }
            }

           }
              elseif(null !== Input::get('group_name')){
                $filter_pages .=" and posts.group_name In ('". Input::get('group_name')."') ";
            
           }

           else
           {
            $my_pages = $this->getInboundPages($brand_id);
            $filter_pages .= ' and posts.page_name In('.$my_pages.')';

            // $my_pages= $this->getOwnPage($brand_id);
            // $my_pages=explode(',', $my_pages);

            // if  ('1' == Input::get('other_page'))
            // $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
            // else
            // $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages); 
           }
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));
// dd($filter_pages);
       $add_con="";$add_group_con="";
       $add_comment_con="";
       $CmtType = "";
        if(null !== Input::get('CmtType'))
        { 
          $CmtType = " and cmts.checked_sentiment='".Input::get('CmtType')."'";
        }

       $tag_con = '';$post_tag_con='';
       if(null !== Input::get('search_tag'))
      {
        // $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
         $tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'cmts'); 
         $post_tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'posts');
      }

      if($filter_keyword !== '')
      {
        $add_con = " and cmts.message Like '%".$filter_keyword."%'";
        $add_group_con = " and cmts.message Like '%".$filter_keyword."%'";

      }

      if(null !== Input::get('post_id') )
      {
        $add_con .="   and posts.id ='" .Input::get('post_id'). "'";
        $add_group_con .="   and cmts.post_id ='" .Input::get('post_id'). "'";

        
        
      }
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = '  1=1 ';
        $date_filter_post = '  1=1 ';
      }
      else
      {
        $date_filter =" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        $date_filter_post =" (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
      }
      $tagOrder = Input::get('tagOrder');
      $fieldName = Input::get('fieldName'); 

      // if(null !== $tagOrder && null !== $fieldName){
      //   if($fieldName == 'name') $order_con = " ORDER by "

      // }
     $add_global_filter ='';$add_global_filter_post='';
     if(null !== Input::get('tsearch_tag'))
     {
        $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'),'cmts');
        $add_global_filter_post .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'),'posts');
     }
     if(null !== Input::get('tsearch_senti'))
     {
        $add_global_filter .=" and " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
        $add_global_filter_post .=" and " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
     }


// dd($filter_pages);

        if(null !== Input::get('wordcloud_text'))
        { 
            $highlight_cmt_con='';
            $wordcloud_text = $request->session()->get( Input::get('wordcloud_text'));

            $wordcloud_text = implode(',',$wordcloud_text);
            $wordcloud_text = "'" . str_replace(",", "','", $wordcloud_text) . "'";
            $highlight_cmt_con = " and cmts.id in (".$wordcloud_text.")";
           
            $query = " SELECT cmts.checked_tags_id  tags " .
         "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
          " on posts.id=cmts.post_id  WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.checked_tags_id<>'' AND ".$filter_pages.
          " AND " . $date_filter . $add_con . $highlight_cmt_con . $CmtType . $tag_con;
        }

        // $query = " SELECT cmts.checked_tags_id  tags " .
        //  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        //   " on posts.id=cmts.post_id  WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.checked_tags_id<>'' AND ".$filter_pages.
        //   " AND " . $add_con . $highlight_cmt_con . $CmtType . $tag_con;
        // }

        elseif(null !== Input::get('campaign_name'))
        {
             $query = " SELECT cmts.checked_tags_id  tags " .
       "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.checked_tags_id<>'' ".
      " AND " . $date_filter . $campaign_filter_con . $add_con  . $additional_filter  ;

      // $query = " SELECT cmts.checked_tags_id  tags " .
      //  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
      //   " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.checked_tags_id<>'' ". $campaign_filter_con . $add_con  ;

        }
        elseif(null !== Input::get('type') && 'inbound_post' == Input::get('type'))
        {
             $query = " SELECT posts.checked_tags_id  tags " .
       "FROM temp_inbound_posts posts ".
        " WHERE  posts.id IS NOT NULL  AND  posts.checked_tags_id <>'' ".
      " AND " . $filter_pages  . " AND " .  $date_filter_post  .  $add_con  .  $additional_filter_post . $post_tag_con;

      // $query = " SELECT posts.checked_tags_id  tags " .
      //  "FROM temp_inbound_posts posts ".
      //   " WHERE  posts.id IS NOT NULL  AND  posts.checked_tags_id <>'' ".
      // " AND " . $filter_pages .  $add_con . $post_tag_con;

        }
        elseif(null !== Input::get('group_name'))
        {
          if(Input::get('type') !== null && Input::get('type') == 'group_comment'){
            $query = " SELECT cmts.checked_tags_id  tags " .
       "FROM temp_group_comments cmts LEFT JOIN temp_group_posts posts ON posts.temp_id=cmts.post_id ".
        " WHERE  cmts.temp_id IS NOT NULL  AND  cmts.checked_tags_id <> '' ".
      "  " .$add_global_filter . " AND " . $filter_pages  . " AND " .  $date_filter  .  $add_group_con  .  $additional_filter . $tag_con;
      // dd($query);
          }
          else{
             $query = " SELECT posts.checked_tags_id  tags " .
       "FROM temp_group_posts posts ".
        " WHERE  posts.temp_id IS NOT NULL  AND  posts.checked_tags_id <> '' ".
      "  " .$add_global_filter_post . " AND " . $filter_pages  . " AND " .  $date_filter_post  .  $add_con  .  $additional_filter_post . $post_tag_con;
        }
// dd($query);
      // $query = " SELECT posts.checked_tags_id  tags " .
      //  "FROM temp_inbound_posts posts ".
      //   " WHERE  posts.id IS NOT NULL  AND  posts.checked_tags_id <>'' ".
      // " AND " . $filter_pages .  $add_con . $post_tag_con;

        }
        else
        {
          if(null !== Input::get('highlight_text'))
        { 
          $highlight_text= str_replace(' ', '', Input::get('highlight_text'));
          $add_con = " and ( REPLACE(REPLACE(REPLACE(cmts.message, '\r', ''), '\n', ''),' ','')) Like '%".$highlight_text."%' ";
        
        }

          $query = " SELECT cmts.checked_tags_id  tags " .
       "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND  cmts.checked_tags_id<>'' AND ". $filter_pages.
      " AND " . $date_filter . $add_con  .  $keyword_con . $additional_filter . $CmtType .$tag_con . $add_global_filter ;

      // $query = " SELECT cmts.checked_tags_id tags " .
      //  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
      //   " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND  cmts.checked_tags_id<>'' AND ". $filter_pages 
      //   . $add_con  .  $keyword_con . $CmtType .$tag_con . $add_global_filter ;
      
        }

// 
       // return $query;

    $query_result = DB::select($query);
// dd($query_result);
       $data = [];
       $total=0;

       $data_tag = [];
       $total=0;
      

        foreach ($query_result as  $key => $row) {
              
       
                   $tags =$row->tags;
            
                $arr_tag=explode(',', $tags);
              
                   $request['tagLabel'] ="";
      
          foreach ($arr_tag as  $key => $tag) {
            if($tag <> '')
            {
              $tagInfo=$this->gettagsByid($tag);
                if(count($tagInfo)>0)
                    {
                     

                      foreach ($tagInfo as  $key => $taginfo) {

                      if (!array_key_exists($taginfo->id, $data_tag)) { 
                        $data_tag[$taginfo->id] = array(
                                    'tagLabel' =>$taginfo->name,
                                    'tagId' =>$taginfo->id,
                                    'page_id'=>$page_id,
                                    'imgurl'=>$imgurl,
                                    'tagCount' => 1,
                                 
                                   
                                );
                        }
                        else
                      {
                          // dd($taginfo->id);
                       
                       $data_tag[$taginfo->id]['tagCount'] = (int) $data_tag[$taginfo->id]['tagCount'] + 1;
                           
                      }
                      }
                    
                      
                    }

            }
                    
                
          }
      
             

    

 


                
         }
// dd($data_tag);
       //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
// dd($fieldName);
        if(null !== $tagOrder && null !== $fieldName){

         if($fieldName == 'name' && $tagOrder == 'ASC'){
          // dd('hy');
            usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?-1:1;
          });

        }
             
          if($fieldName == 'name' && $tagOrder == 'DESC'){
               usort($data_tag, function($a, $b) {
            if(strtolower($a['tagLabel'])==strtolower($b['tagLabel'])) return 0;
            return strtolower($a['tagLabel']) < strtolower($b['tagLabel'])?1:-1;
          });
               array_reverse($data_tag);
               

           
              // dd($data_tag);
          }
           if($fieldName == 'count' && $tagOrder == 'ASC'){
             $price = array_column($data_tag, 'tagCount');
            array_multisort($price, $data_tag);

          //   usort($data_tag, function($a, $b) {
          //   if($a['tagCount']==$b['tagCount']) return 0;
          //   return $a['tagCount'] < $b['tagCount']?1:-1;
          // });

        }
                 if($fieldName == 'count' && $tagOrder == 'DESC'){
            usort($data_tag, function($a, $b) {
            if($a['tagCount']==$b['tagCount']) return 0;
            return $a['tagCount'] < $b['tagCount']?1:-1;
          });

        }

   


      }
        else{
           usort($data_tag, function($a, $b) {
          if($a['tagCount']==$b['tagCount']) return 0;
          return $a['tagCount'] < $b['tagCount']?1:-1;
      });
         }

           if(null === Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, 5, true);
           else if ("no" !== Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, (int) Input::get('limit'), true);

            
           echo json_encode($data_tag);


}
public function getTopicCount()
{
      $brand_id=Input::get('brand_id');
      $period_type = '';$page_id='';
      $additional_filter="";
      // dd(Input::get('period'));
      if(null !==Input::get('period')){
        $period=Input::get('period');
        $char_count = substr_count($period,"-");
        $format_type = '';
        if($char_count == 1){ $format_type ='%Y-%m'; }
        else if($char_count == 2){ $format_type ='%Y-%m-%d';}   
        else if($char_count > 2){
          $pieces = explode(" - ", $period);
          $format_type ='%Y-%m-%d';
          $period_type ='week';
        }
          dd($period_type);
        if($period_type == 'week'){
         $additional_filter .= " AND (DATE(created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') "; 
        }
        else{
        $additional_filter .= " AND DATE_FORMAT(created_time, '".$format_type."') = '".$period."'";
        }
     } 
// dd($additional_filter);
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      // dd(Input::get('keyword'));
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 
       $post_key_con='';$article_key_con='';
       $keyword_data = $this->getprojectkeyword($brand_id,$filter_keyword);
       // dd($keyword_data);
       $post_key_con =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'posts'). ")";
       $article_key_con =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'article'). ")";
      
       if(null !== Input::get('fday')){
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else{
        $dateBegin=date('Y-m-d');
          }

      if(null !==Input::get('sday')){
        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
      else{
        $dateEnd=date('Y-m-d');
          }
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));



        $date_filter =" (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        $query = " SELECT checked_tags_id  tags " .
                 " FROM temp_posts posts ".
                 " WHERE posts.isHide=0 AND  (posts.checked_tags_id is not null or posts.checked_tags_id <> '' ) ".
                 " AND " . $date_filter   .  $post_key_con . $additional_filter .
                 " UNION ALL 
                   SELECT checked_tags_id  tags " .
                 " FROM temp_web_mentions article ".
                 " WHERE article.isHide=0 AND  (article.checked_tags_id is not null or article.checked_tags_id <> '' ) ".
                 " AND " . $date_filter   .  $article_key_con . $additional_filter ;
      
        // dd($query);

        $query_result = DB::select($query);
        //dd($query_result);
        $data = [];
        $total=0;

        $data_tag = [];

        foreach ($query_result as  $key => $row) {
         $tags =$row->tags;
         $arr_tag=explode(',', $tags);
         $request['tagLabel'] ="";

          foreach ($arr_tag as  $key => $tag) {
            if($tag <> '')
            {
              $tagInfo=$this->gettagsByid($tag);
              if(count($tagInfo)>0)
              {
                foreach ($tagInfo as  $key => $taginfo) {
                  if (!array_key_exists($taginfo->id, $data_tag)) { 
                      $data_tag[$taginfo->id] = array(
                        'tagLabel' =>$taginfo->name,
                        'tagId' =>$taginfo->id,
                        'page_id'=>$page_id,
                        'tagCount' => 1,
                        );
                  }
                  else
                  {
                      $data_tag[$taginfo->id]['tagCount'] = (int) $data_tag[$taginfo->id]['tagCount'] + 1;
                  }
                }
              }
            }  
          }
         }

       //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 

       
           usort($data_tag, function($a, $b) {
          if($a['tagCount']==$b['tagCount']) return 0;
          return $a['tagCount'] < $b['tagCount']?1:-1;
      });

           if(null !== Input::get('limit'))
           	 $data_tag = array_slice($data_tag, 0, (int) Input::get('limit'), true);
           // $data_tag = array_slice($data_tag, 0, 5, true);
           // if ("no" !== Input::get('limit'))
            
           echo json_encode($data_tag);

}
public function getCampaignTagCount()
{
      $brand_id=Input::get('brand_id');
      $campaign_filter_con = '';
      if(null !== Input::get('campaign_name'))
      {
        $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_name'),$brand_id);
        $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'cmts');
         $campaign_filter_con = " and ".$campaign_filter_con;
        // dd($campaign_filter_con);
      }

          
         $period_type = '';
         $additional_filter=" AND 1=1 ";
      
            if(null !==Input::get('period'))
            {
           $period=Input::get('period');
            $char_count = substr_count($period,"-");
            $format_type = '';
            if($char_count == 1)
            {
                $format_type ='%Y-%m';
            }
            else if($char_count == 2)
            {
                 $format_type ='%Y-%m-%d';
            }   
            else if($char_count > 2)
            {
                /*this is week*/
                $pieces = explode(" - ", $period);
                $format_type ='%Y-%m-%d';
                $period_type ='week';
            }

          
    if($period_type == 'week')
    {
     $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

    }
    else
    {
        $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
    }

           }
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 
         if(null !== Input::get('fday'))
              {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
              $dateBegin=date('Y-m-d');
              }
            

            if(null !==Input::get('sday'))
            {

         $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
            
            else
            {
        $dateEnd=date('Y-m-d');

           }



       // dd($filter_pages);
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));

       $add_con="";
       $add_comment_con="";

      if($filter_keyword !== '')
      {
        $add_con = " and cmts.message Like '%".$filter_keyword."%'";
      }

     
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = '  1=1 ';
      }
      else
      {
        $date_filter =" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
      }

             $query = " SELECT checked_tags_id  tags " .
       "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.tag_flag=1 AND checked_tags_id <>'' ".
      " AND " . $date_filter . $campaign_filter_con . $add_con   . $additional_filter;
        


    $query_result = DB::select($query);
// dd($query_result);
       $data = [];
       $total=0;

       $data_tag = [];
       $total=0;
      

        foreach ($query_result as  $key => $row) {
              
       
                   $tags =$row->tags;
            
                $arr_tag=explode(',', $tags);
              
                   $request['tagLabel'] ="";

          foreach ($arr_tag as  $key => $tag) {
            if($tag <> '')
            {

              $tagInfo=$this->gettagsByid($tag);

                if(count($tagInfo)>0)
                    {
                     

                      foreach ($tagInfo as  $key => $taginfo) {

                      if (!array_key_exists($taginfo->id, $data_tag)) { 
                        $data_tag[$taginfo->id] = array(
                                    'tagLabel' =>$taginfo->name,
                                    'tagId' =>$taginfo->id,
                                    'tagCount' => 1,
                                 
                                   
                                );
                        }
                        else
                      {
                          // dd($taginfo->id);
                       $data_tag[$taginfo->id]['tagCount'] = (int) $data_tag[$taginfo->id]['tagCount'] + 1;
                           
                      }
                      }
                    
                      
                    }

            }
                    
                
          }
      
             

    

 


                
         }

       //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 

       
           usort($data_tag, function($a, $b) {
          if($a['tagCount']==$b['tagCount']) return 0;
          return $a['tagCount'] < $b['tagCount']?1:-1;
      });
           if(null === Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, 5, true);
           else if ("no" !== Input::get('limit'))
           $data_tag = array_slice($data_tag, 0, (int) Input::get('limit'), true);

            
           echo json_encode($data_tag);
}

     public function getInboundlatestPOST()
    {

      //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
        $brand_id=Input::get('brand_id');
       
       
        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }

              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);

 $query = "SELECT id,message,posts.page_name page_name,link,full_picture,sentiment,emotion,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p')".
 " created_time FROM  ".
 " temp_inbound_posts WHERE  (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ". $filter_pages .
 " GROUP BY DATE_FORMAT(created_time, '%Y-%m-%d  %h:%i:%s %p'),message,posts.page_name,link,full_picture,sentiment,emotion ".
 " ORDER by DATE(created_time) DESC LIMIT 5 ";


$query_result = DB::select($query);

$data = [];

 $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];

foreach ($query_result as  $key => $row) {

              $request["message"] = $row ->message;
              $request["page_name"] = $row ->page_name;
              $request["created_time"] =date('d-m-Y H:m:s', strtotime($row ->created_time));
              if(isset($row ->link))
              $request["link"] =$row ->link;
            else
              $request["link"] ='#';

             if(isset($row ->full_picture))
              $request["full_picture"] =$row ->full_picture;
            else
              $request["full_picture"] ='';

              $request["sentiment"] =$row ->sentiment;
              $request["emotion"] =$row ->emotion;
              $request["id"]=$row->id;
              $request["edit_permission"]=$edit_permission;
              $data[] = $request;

            }
         

            echo json_encode($data);
    }

    public function getInboundinterest()
{
      $brand_id=Input::get('brand_id');
 
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      // if(null !== Input::get('sentiment'))
      // $filter_sentiment=Input::get('sentiment'); 

     //  $brand_id=59;
     // $groupType='month';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " posts.page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con ="";
if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}

  $query = "SELECT sum(interest) interest_sum,count(*) comment_count, ".
  " DATE_FORMAT(cmts.created_time, '".$group_period."') periodLabel  ".
  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND   cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ". $filter_pages .  $add_con .
" GROUP BY DATE_FORMAT(cmts.created_time, '".$group_period."')" ;

$query_result = DB::select($query);
 echo json_encode($query_result);

}

public function getPostingStatus()
{


     $data = array();
      $brand_id=Input::get('brand_id');
    //$brand_id=17;
 
     // $today = date('d-m-Y');//23-02-2015
     // $today_plus=date('Y-m-d', strtotime('+1 day', strtotime($today)));
     // $date_Week_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
     // $date_End=date('Y-m-d',strtotime($today));

     // $date_Month_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));

           if(null !== Input::get('fday'))
            {
               $date_Week_Begin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            }
            else
            {
               $date_Week_Begin=date('Y-m-d');
            }
          

            if(null !==Input::get('sday'))
            {

               $date_End=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
            
            else
            {
               $date_End=date('Y-m-d');

           }
      
          // echo $date_Week_Begin;echo $date_Month_Begin;echo $date_End;
          
          if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
            $page_name = Input::get('admin_page');
            $customName = DB::table('page_rename')->where('custom_name',$page_name)->get();
            foreach($customName as $name) $page_name = $name->origin_name;
             $filter_pages= " posts.page_name in ('".$page_name."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
                //comment
        $query_comment_today = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral ".
        "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ". 
        "  AND  (DATE(cmts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";
        //  dd($query_week);
      $query_today_comment_result = DB::select($query_comment_today);

      $query_comment_week = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
        "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
        " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ". 
        " AND  (DATE(cmts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
       
      $query_week_comment_result = DB::select($query_comment_week);
// dd($query_week_comment_result);
      // return $query_comment_week;
        //post         
        // $query_post_today = "SELECT COALESCE(sum(IF(cmts.sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.sentiment = 'neg', 1, 0)),0) negative ".
        // "FROM temp_inbound_posts posts INNER JOIN temp_inbound_comments cmts ".
        // " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND ".$filter_pages." AND cmts.parent='' ".
        // " AND (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";
          //dd($query_post_today);
      $query_post_today ="select  COALESCE(sum(IF(positive>negative, 1, 0)),0) ov_positive, COALESCE(sum(IF(negative>positive, 1, 0)),0) ov_negative from (SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative  FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id 
      where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = ''  ".
      " AND   (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."') " . 
      " GROUP BY posts.id)T1";
      $query_today_post_result = DB::select($query_post_today);

      // $query_post_week ="select  COALESCE(sum(IF(positive>negative, 1, 0)),0) ov_positive, COALESCE(sum(IF(negative>positive, 1, 0)),0) ov_negative from (SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative  FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id 
      // where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = '' ".
      // " AND (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " .
      // " GROUP BY posts.id)T1";



      $query_post_week ="SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral' OR  cmts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id 
      where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = ''  ". 
      " AND   (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " .
      " GROUP BY posts.id";

// dd($query_post_week);

$query_week_post_result = DB::select($query_post_week);
$data_week_post_result=[];
$ov_positive=0;$ov_negative=0;$ov_neutral=0;
foreach ($query_week_post_result  as $key => $value) {
 // dd($value->positive);
  $ov_positive = $ov_positive+$value->positive;
  $ov_negative = $ov_negative+$value->negative;
  $ov_neutral = $ov_neutral+$value->neutral;
     # code...
}

$data_week_post_result[]=array(
                'ov_positive'     =>$ov_positive,
                'ov_negative' => $ov_negative,
                'ov_neutral' => $ov_neutral,
              );

//total post
 $query_post_tol_today = "SELECT count(Distinct posts.id) total ".
  "FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";
   // dd($query_post_tol_today);
$query_post_tol_today_result = DB::select($query_post_tol_today);
// dd($query_post_tol_today_result);
$query_post_tol_week =  "SELECT count(Distinct posts.id) total ".
  "FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND posts.visitor <> 1 AND ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
  
$query_post_tol_week_result = DB::select($query_post_tol_week);
   // dd($query_post_tol_week_result);

 echo json_encode(array($query_today_comment_result,$query_week_comment_result,$query_today_post_result,$data_week_post_result,$query_post_tol_today_result,$query_post_tol_week_result));

}


public function getEngagementStatus()
{
     $data = array();
      $brand_id=Input::get('brand_id');
  //  $brand_id=17;
 
     // $today = date('d-m-Y');//23-02-2015
     // $today_plus=date('Y-m-d', strtotime('+1 day', strtotime($today)));
     // $date_Week_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
     // $date_End=date('Y-m-d',strtotime($today));

     // $date_Month_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));
      
    // echo $date_Week_Begin;echo $date_Month_Begin;echo $date_End;
      if(null !== Input::get('fday'))
      {
    $date_Week_Begin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $date_Week_Begin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $date_End=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$date_End=date('Y-m-d');

   }
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " posts.page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
//post
  $query_post_today = "SELECT count(posts.id) total_post,COALESCE(sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry),0)  total_reaction, ".
  " COALESCE(SUM(replace(shared, ',', '')),0) shared ".
  " FROM temp_inbound_posts posts ".
  " WHERE ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";
    
  $query_today_post_result = DB::select($query_post_today);

  $query_post_week ="SELECT count(posts.id) total_post,COALESCE(sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry),0)  total_reaction, ".
  " COALESCE(SUM(replace(shared, ',', '')),0) shared ".
  " FROM temp_inbound_posts posts ".
  " WHERE ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
   // dd($query_post_week);
  $query_week_post_result = DB::select($query_post_week);

  
//comment count
  $query_today_comment = "SELECT count(*) total_comment ".
  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";

$query_today_comment_result = DB::select($query_today_comment);

$query_week_comment = "SELECT count(*) total_comment ".
  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
   // dd($query_month);
$query_week_comment_result = DB::select($query_week_comment);

         
  
   // dd($query_month);


 echo json_encode(array($query_today_post_result,$query_week_post_result,$query_today_comment_result,$query_week_comment_result));

}

public function getInboundSentiDetail()
    {
     //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $add_searchkw_con = '';
      if(null !==Input::get('kw_search'))
      {
        $kw = Input::get('kw_search');
        $add_searchkw_con = " AND (cmts.message Like '%".$kw."%')";
      }

      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
     
   if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con="";
if($groupType=="month")
{
$group_period =" DATE_FORMAT(cmts.created_time, '%Y-%m')"; 
 }

   else 
 {
$group_period =" DATE_FORMAT(cmts.created_time, '%Y-%m-%d')";                
              

 }

  if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
            $page_name = Input::get('admin_page');
            $customName = DB::table('page_rename')->where('custom_name',$page_name)->get();
            foreach($customName as $name) $page_name = $name->origin_name;
             $filter_pages= " posts.page_name in ('". $page_name."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

 $query ="SELECT COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period ." created_time  ".
  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_searchkw_con .
  " GROUP BY ".$group_period .
  " ORDER BY ".$group_period;

$query_result = DB::select($query);
// dd($query_result);
   $data = [];
   $total=0;

   $data_sentiment = [];
   $data_mention = [];
   $total=0;
   $data_message = [];

    foreach ($query_result as  $key => $row) {
          
              $utcdatetime = $row->created_time;
         
               
               $pos =$row->positive;
               $neg =$row->negative;
               $neutral =(int)$row->neutral + (int)$row->NA;
                       
                $request['periodLabel'] ="";
      
                if($groupType == "week")
{
    $weekperiod=$this ->rangeWeek ($utcdatetime);
    $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
    
 if (!array_key_exists($periodLabel , $data_sentiment)) {
    $data_sentiment[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'positive' => $pos,
                'negative' => $neg,
                'neutral' => $neutral,
                'periods' =>$utcdatetime,
            );
}
else
{
 $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
 $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
 $data_sentiment[$periodLabel]['neutral'] = $data_sentiment[$periodLabel]['neutral'] + $neutral;
 $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
}
 
                  


}
 else
{
 $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
 $data_sentiment[$utcdatetime]['positive'] = $pos;
 $data_sentiment[$utcdatetime]['negative'] = $neg;
 $data_sentiment[$utcdatetime]['neutral'] = $neutral;
 $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

}

                
   }

 $data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
    echo json_encode($data_sentiment);


}

public function getPageSummary()
{
     $data = array();
      $brand_id=Input::get('brand_id');
  //  $brand_id=17;
 
      if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
$page_id =[];$imgurl =[];
            if(null !== Input::get('admin_page'))
           {
            $customName = DB::table('page_rename')->where('origin_name',Input::get('admin_page'))->orWhere('custom_name',Input::get('admin_page'))->get();
           foreach($customName as $name) {$page_name = $name->origin_name;}

            $filter_page_name=$this->Format_Page_name_single($page_name);
            $filter_pages= " posts.page_name in ('".$filter_page_name."') ";

              $InboundPages_result=InboundPages::raw(function ($collection) use($filter_page_name) {//print_r($filter);

              return $collection->aggregate([
                  [
                  '$match' =>[
                       '$and'=> [ 
                       ['page_name'=>$filter_page_name],
                                 
                                ]
                  ] 
                             
                 ],

                 ['$sort' =>['_id'=>1]]
                  
              ]);
            })->toArray();
            
            foreach ($InboundPages_result as  $key => $row) {
             
              if(isset($row['id'])) $page_id[] = $row['id'];
              if(isset($row['imageurl'])) $imgurl[] = $row['imageurl'];


            }
          
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
//post


  $query_post ="SELECT count(posts.id) total_post,COALESCE(sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry),0)  total_reaction, ".
  " COALESCE(SUM(replace(shared, ',', '')),0) shared ".
  " FROM temp_inbound_posts posts ".
  " WHERE ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";
   // dd($query_post_week);
  $post_count = DB::select($query_post);

  $query_comment = "SELECT count(*) total_comment,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) cmt_neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) cmt_NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) cmt_positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) cmt_negative  ".
  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";
 

   // dd($query_month);
$comment_count = DB::select($query_comment);


$query_overall_PN ="SELECT cmts.checked_sentiment cmt_sentiment,parent,posts.id postid FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id 
where   ".$filter_pages."   ".
" AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";

// dd($query_post_week);

$result_overall_PN = DB::select($query_overall_PN);
$overall_PN=[];
$overall_PN_data=[];
$ov_positive=0;$ov_negative=0;$ov_neutral=0;
foreach ($result_overall_PN  as $key => $value) {
 $pos=($value->cmt_sentiment=='pos' && $value->parent=== '' )?1:0 ;
$neg=($value->cmt_sentiment=='neg' && $value->parent=== '' )?1:0;
$cmtneutral=($value->cmt_sentiment=='neutral' && $value->parent=== '' )?1:0;
$cmtNA=($value->cmt_sentiment=='NA' && $value->parent=== '' )?1:0;
$cmtneutral=(int)$cmtneutral+(int) $cmtNA;
$id = $value->postid;
 if (!array_key_exists($id, $overall_PN)) {
    $overall_PN[$id] = array(
                
                'ov_positive'=> $pos,'ov_negative'=>$neg,'ov_neutral'=>$cmtneutral,
                'postid'=>$id
                
            );
}
else
{
 $overall_PN[$id]['ov_positive'] = (int) $overall_PN[$id]['ov_positive'] +$pos;
 $overall_PN[$id]['ov_negative'] = (int) $overall_PN[$id]['ov_negative'] +$neg;
 $overall_PN[$id]['ov_neutral'] = (int) $overall_PN[$id]['ov_neutral'] +$cmtneutral;
 }
}
foreach ($overall_PN as $key => $value) {//dd($value['ov_positive']);
  if((int)$value['ov_positive'] >  (int)$value['ov_negative'])
              {
                $ov_positive = $ov_positive +1;
              }
              else   if((int)$value['ov_positive'] <  (int)$value['ov_negative'])
              {
               $ov_negative = $ov_negative +1;

              }
                 else if((int)$value['ov_positive'] ==  0 && (int)$value['ov_negative'] == 0 && (int)$value['ov_neutral'] == 0 )
              {
               $ov_neutral = $ov_neutral+1;
              }
              else   if((int)$value['ov_positive'] ==0 &&  (int)$value['ov_negative'] ==0 &&  (int)$value['ov_neutral'] <> 0)
              {
                //Neutral;
                $ov_neutral = $ov_neutral+1;
              }
              else   if((int)$value['ov_positive'] ==  (int)$value['ov_negative'] &&  (int)$value['ov_neutral'] <> 0)
              {
                 $ov_positive = $ov_positive +1;
              }
              else   if((int)$value['ov_positive'] ==  (int)$value['ov_negative'])
              {
                $ov_negative = $ov_negative +1;
              }
}
$overall_PN_data[]=array(
                'ov_positive'     =>$ov_positive,
                'ov_negative' => $ov_negative,
                'ov_neutral' => $ov_neutral,

              );

 echo json_encode(array($post_count,$comment_count,$overall_PN_data,$page_id,$imgurl));

}
function unique_multidim_array($array, $key) { 
    $temp_array = array(); 
    $i = 0; 
    $key_array = array(); 
    
    foreach($array as $val) { 
        if (!in_array($val[$key], $key_array)) { 
            $key_array[$i] = $val[$key]; 
            $temp_array[$i] = $val; 
        } 
        $i++; 
    } 
    return $temp_array; 
}

function rangeWeek ($datestr) {
   date_default_timezone_set (date_default_timezone_get());
   $dt = strtotime ($datestr);
   return array (
     "start" => date ('N', $dt) == 1 ? date ('Y-m-d', $dt) : date ('Y-m-d', strtotime ('last monday', $dt)),
     "end" => date('N', $dt) == 7 ? date ('Y-m-d', $dt) : date ('Y-m-d', strtotime ('next sunday', $dt))
   );
 }
public function getAccssToken()
{
//   echo "hihi";
//   var FbToken= Ext.create('Ext.util.DelayedTask', function() {
//     var accessToken= '12345678998754321' //sample token
//      graph.facebook.com/me?access_token=' + accessToken;
// });


}
public function interactionCount(){
 
 
  $postquery = 'SELECT SUM(Liked) + SUM(Love) + SUM(Wow) + SUM(Haha) + SUM(Sad) + SUM(Angry) reactTotal,SUM(shared) shareTotal FROM temp_inbound_posts posts';
  $postresult = DB::select($postquery);

   $cmtquery = 'SELECT count(*) cmt_count FROM temp_inbound_comments';
   $cmtresult = DB::select($cmtquery);
// dd($postresult,$cmtresult);/
   $total = (int)$postresult[0]->reactTotal + (int) $postresult[0]->shareTotal + (int) $cmtresult[0]->cmt_count;
   $array = [
            'interaction_count' => $total
            ];
    return json_encode($array);


}

}



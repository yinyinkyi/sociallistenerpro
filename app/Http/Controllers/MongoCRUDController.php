<?php

namespace BIMiner\Http\Controllers;

use Illuminate\Http\Request;
use BIMiner\MongodbData;
use BIMiner\Comment;
use BIMiner\Project;
use BIMiner\Http\Controllers\stdClass;
use BIMiner\Http\Controllers\GlobalController;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use Yajra\Datatables\Datatables;
 use Storage;
class MongoCRUDController extends Controller
{
    // 
    use GlobalController;
    public function insertHighlightedComment()
    {

     


        $projects = DB::table('projects')->select('*')->whereIn('id', [34])->get();
  // dd($projects);
        $today = date('d-m-Y h:i:s');
        $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
        $dateBegin=date('Y-m-d', strtotime('-28 days', strtotime($today_plus)));
        $dateEnd=date('Y-m-d',strtotime($today_plus));


        foreach($projects as $project)
         {
            $id = $project->id;
            $monitor_pages = $project->monitor_pages;
            $monitor_pages = explode(',', $monitor_pages);
            $monitor_pages = $this->Format_Page_Name($monitor_pages);
            foreach($monitor_pages as $page)
            {

              // dd($page);
                $filter_page_name=$this->Format_Page_name_single($page);
                // dd($filter_page_name);  
                // $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
                // foreach($customName as $name) $filter_page_name = $name->origin_name;
                $filter_pages= " cmts.page_name in ('". $filter_page_name."') ";

                  $dict_arr =array();
                  $word_dict = DB::select("SELECT DISTINCT word FROM word_dict");
                  // dd($word_dict);
                    foreach($word_dict as $word_dict_result)
                    {
                      // dd(strtolower(str_replace(' ','',$word_dict_result->word)));
                      // if(strtolower(str_replace(' ','',$word_dict_result->word)) == strtolower(str_replace(' ','',$filter_page_name)))
                      // {
                      //   continue;
                      // }
                      $dictWord =  strtolower(str_replace(' ','',$word_dict_result->word));
                    // dd($dictWord);
                      $query = "SELECT cmts.id FROM temp_inbound_comments cmts ".
                        " WHERE  cmts.post_id IS NOT NULL AND  cmts.parent='' AND  LOWER(REPLACE(cmts.message,' ','')) like '%".$dictWord."%'  AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages .
                        " ORDER by timestamp(cmts.created_time) DESC limit 1000";

                      $word_count = DB::select($query);
          // dd($word_count);
                      $word_str = '';
                     for($i = 0; $i < count($word_count); $i++){
                       if($i == count($word_count) -1 ){
                        $word_str = $word_str . $word_count[$i]->id ;
                       }
                       else{
                          $word_str = $word_str . $word_count[$i]->id . ",";
                        }
                      }

                      $word_str_arr = explode(",",$word_str);

                      if(count($word_count) > 0 ){
                       
                      
                        $formData = array(
                            'topic_name' => $dictWord,
                            'weight' => count($word_count),
                            'comment_id' => $word_str_arr
                        );
                        $dict_arr[] = $formData;

                      }

                    }
                    $dict_arr = array_unique($dict_arr, SORT_REGULAR);
                    // dd($dict_arr);/
                    if(!empty($dict_arr)){
                          $sortArray = array(); 
                          foreach($dict_arr as $dict_arr_item){ 
                            foreach($dict_arr_item as $key=>$value){ 
                                if(!isset($sortArray[$key])){ 
                                    $sortArray[$key] = array(); 
                                } 
                                $sortArray[$key][] = $value; 
                            } 
                        } 

                          $orderby = "weight"; 
                          array_multisort($sortArray[$orderby],SORT_DESC,$dict_arr); 
                          $dict_arr= array_slice($dict_arr,0,50);
                      }
                   
                       // dd($dict_arr);
                foreach($dict_arr as $row)
                {
                  
                  $comment_id = implode(',',$row['comment_id']);

                

               DB::table('highlighted_voices_test')->insertOrIgnore(['brand_id' => $id,'word' => $row['topic_name'],'weight'=>$row['weight'],'comment_id'=>$comment_id,'page_name'=>$page,'today_date'=>now()->toDateTimeString(),'created_at'=>now()->toDateTimeString()]);  
                       
                }
              
              }
             }
    }
    public function UpdateBookMarkPost()
    {
      $bookmark_array = Input::post('bookmark_array');
      $brand_id = Input::post('id');
      $bookmark_remove_array = Input::post('bookmark_remove_array');

      if(isset($bookmark_array))
      {
        foreach ($bookmark_array as $key => $value) {
        	/* $update = MongodbData::where('id' , '=' , $value)->first();
  	       $update->isBookMark = true;
  	       $update->save();*/
           DB::table('temp_'.$brand_id.'_posts')
          ->where('id', $value)  // find your user by their email
          ->limit(1)  // optional - to ensure only one record is updated.
          ->update(array('isBookMark' => 1));  // update the record in the DB. 
        }
    }
        if(isset($bookmark_remove_array))
        {
          foreach ($bookmark_remove_array as $key => $value) {
            DB::table('temp_'.$brand_id.'_posts')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 0));  // update the record in the DB. 
          	
          }
        }

            return "success";

        }

        public function UpdateBookMarkInboundPost()
    {
          $bookmark_array = Input::post('bookmark_array');
          $brand_id = Input::post('id');
          $bookmark_remove_array = Input::post('bookmark_remove_array');

          if(isset($bookmark_array))
          {
          foreach ($bookmark_array as $key => $value) {
            /* $update = MongodbData::where('id' , '=' , $value)->first();
             $update->isBookMark = true;
             $update->save();*/
             DB::table('temp_'.$brand_id.'_inbound_posts')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 1));  // update the record in the DB. 
          }
        }
        if(isset($bookmark_remove_array))
        {
          foreach ($bookmark_remove_array as $key => $value) {
            DB::table('temp_'.$brand_id.'_inbound_posts')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 0));  // update the record in the DB. 
            
          }
        }

            return "success";

        }
        public function UpdateBookMarkInboundComment()
    {
          $bookmark_array = Input::post('bookmark_comment_array');
          $brand_id = Input::post('id');
          $bookmark_remove_array = Input::post('bookmark_comment_remove_array');

          if(isset($bookmark_array))
          {
          foreach ($bookmark_array as $key => $value) {
            /* $update = MongodbData::where('id' , '=' , $value)->first();
             $update->isBookMark = true;
             $update->save();*/
             DB::table('temp_'.$brand_id.'_inbound_comments')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 1));  // update the record in the DB. 
          }
        }
        if(isset($bookmark_remove_array))
        {
          foreach ($bookmark_remove_array as $key => $value) {
            DB::table('temp_'.$brand_id.'_inbound_comments')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 0));  // update the record in the DB. 
            
          }
        }

            return "success";

        }

         public function UpdateBookMarkComment()
    {
          $bookmark_array = Input::post('bookmark_comment_array');
          $brand_id = Input::post('id');
          $bookmark_remove_array = Input::post('bookmark_comment_remove_array');

          if(isset($bookmark_array))
          {
          foreach ($bookmark_array as $key => $value) {
            /* $update = MongodbData::where('id' , '=' , $value)->first();
             $update->isBookMark = true;
             $update->save();*/
             DB::table('temp_'.$brand_id.'_comments')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 1));  // update the record in the DB. 
          }
        }
        if(isset($bookmark_remove_array))
        {
          foreach ($bookmark_remove_array as $key => $value) {
            DB::table('temp_'.$brand_id.'_comments')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 0));  // update the record in the DB. 
            
          }
        }

            return "success";

        }

//      public function commentUpdate()
//      { 
//          $comment_table = 'temp_59_inbound_comments';
//         $comments = DB::table($comment_table)->select('temp_id','wb_message')->where('sentiment','')->limit(10000)->get();
//         if(!$comments->isEmpty()){
//         foreach($comments as $comment)
//         {

//           $wb_message[] = $comment->wb_message;
//           $request['wb_message']= $comment->wb_message;
//           $request['id'] = $comment->temp_id;

//           $data[]=$request;
//         }

//         if(count($wb_message) > 0)
//         {
//           $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
//           $uri_sentiment = 'senti_emo_interest';
         
//             $formData = array(
//     'raw' =>  $wb_message,
   
// );

//            $formData = json_encode($formData);
// //            $path = storage_path('app/data_output/wunzinn_wb_msg_021018.json');
// // $this->doJson($path,$formData);
// // return;

//            $api_response = $client->post($uri_sentiment, [
//                                 'form_params' => [
//                                 'raw' =>  $formData,
//                                 ],
//                              ]);

//             $result = ($api_response->getBody()->getContents());

//             $json_result_array = json_decode($result, true);
  
//             $result = $json_result_array[0];

//             $count = (Int)count($data);

//             for($i=0;$i<$count;$i++)
//             {
              
//                 $id = $data[$i]['id'];
               
//                 $comment = DB::table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'updated_at' => now()->toDateTimeString()]);
//                  // $post = DB::table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                 
//             }

//         }
//       }
//       else{
//         echo "Cron has nothing to do ";
//       }
      
//     // }
//   }
        public function DateTest(){

          
          // dd($data);
        }

        public function commentUpdate()
        {

          
      // $projects = Project::all();
      // $data =  [];
      
      // foreach($projects as $project)
      // {
        $id = 17;
        $comment_table = "temp_".$id."_inbound_comments";
        $comments = DB::table($comment_table)->select('temp_id','wb_message')->where('sentiment','')->limit(10000)->get();
         if(!$comments->isEmpty()){
        foreach($comments as $comment)
        {

          $wb_message[] = $comment->wb_message;
          $request['wb_message']= $comment->wb_message;
          $request['id'] = $comment->temp_id;

          $data[]=$request;
        }
      
        if(count($wb_message) > 0)
        {
          $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_sentiment = 'senti_emo_interest';
         
            $formData = array(
    'raw' =>  $wb_message,
   
);

           $formData = json_encode($formData);

           $api_response = $client->post($uri_sentiment, [
                                'form_params' => [
                                'raw' =>  $formData,
                                ],
                             ]);

            $result = ($api_response->getBody()->getContents());

            $json_result_array = json_decode($result, true);
  
            $result = $json_result_array[0];

            $count = (Int)count($data);

            for($i=0;$i<$count;$i++)
            {
              
                $id = $data[$i]['id'];
               
                $comment = DB::table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'updated_at' => now()->toDateTimeString()]);
                
                 
            }

        }
      }
      // }
        }
          public function handle()
    {
             
        $projects = DB::table('projects')->select('*')->whereIn('id', [34,38,39])->get();

        $today = date('d-m-Y h:i:s');
        $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
        $dateBegin=date('Y-m-d', strtotime('-28 days', strtotime($today_plus)));
        $dateEnd=date('Y-m-d',strtotime($today_plus));
        foreach($projects as $project)
         {
            $id = $project->id;
            $monitor_pages = $project->monitor_pages;
            $monitor_pages = explode(',', $monitor_pages);
            $monitor_pages = $this->Format_Page_Name($monitor_pages);
            foreach($monitor_pages as $page)
            {


                $filter_page_name=$this->Format_Page_name_single($page);
                $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
                foreach($customName as $name) $filter_page_name = $name->origin_name;
                  $filter_pages= " posts.page_name in ('". $filter_page_name."') ";

                  $dict_arr =array();
                  $word_dict = DB::select("SELECT DISTINCT word FROM word_dict;");
                    foreach($word_dict as $word_dict_result)
                    {
                      if(strtolower(str_replace(' ','',$word_dict_result->word)) == strtolower(str_replace(' ','',$filter_page_name)))
                      {
                        continue;
                      }
                      $dictWord =  strtolower(str_replace(' ','',$word_dict_result->word));
      
                      $query = "SELECT cmts.id FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
                        " WHERE  cmts.post_id IS NOT NULL AND  cmts.parent='' AND  LOWER(REPLACE(cmts.message,' ','')) like '%".$dictWord."%'  AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages .
                        " ORDER by timestamp(cmts.created_time) DESC";

                      $word_count = DB::select($query);
                      $word_str = '';
                     for($i = 0; $i < count($word_count); $i++){
                       if($i == count($word_count) -1 ){
                        $word_str = $word_str . $word_count[$i]->id ;
                       }
                       else{
                          $word_str = $word_str . $word_count[$i]->id . ",";
                        }
                      }

                      $word_str_arr = explode(",",$word_str);

                      if(count($word_count) > 0 ){
                       
                      
                        $formData = array(
                            'topic_name' => $dictWord,
                            'weight' => count($word_count),
                            'comment_id' => $word_str_arr
                        );
                        $dict_arr[] = $formData;

                      }

                    }
                    if(!empty($dict_arr)){
                          $sortArray = array(); 
                          foreach($dict_arr as $dict_arr_item){ 
                            foreach($dict_arr_item as $key=>$value){ 
                                if(!isset($sortArray[$key])){ 
                                    $sortArray[$key] = array(); 
                                } 
                                $sortArray[$key][] = $value; 
                            } 
                        } 

                          $orderby = "weight"; 
                          array_multisort($sortArray[$orderby],SORT_DESC,$dict_arr); 
                          $dict_arr= array_slice($dict_arr,0,50);
                      }

                foreach($dict_arr as $row)
                {
                  $comment_id = implode(',',$row['comment_id']);

                 

               DB::table('highlighted_voices')->insert(['brand_id' => $id,'word' => $row['topic_name'],'weight'=>$row['weight'],'comment_id'=>$comment_id,'page_name'=>$page,'today_date'=>now()->toDateTimeString(),'created_at'=>now()->toDateTimeString()]);  
                        
                }
              }
             }
         }
  function doJson($path,$json)
{
    $fp = fopen($path, 'w');
   fwrite($fp, $json);
   fclose($fp);
}

    function fontCheck($message)
    {

   
      $firstCharacter = substr(strtolower($message), 0, 8);
      if (strpos($firstCharacter, 'zawgyi') !== false) {
            
  		$split = explode("unicode",strtolower($message));
  		$z_msg = $split[0];
  		
  		$last_space_position = strrpos($z_msg, ' ');
  		$message = substr($z_msg, 0, $last_space_position);
  		$message = str_replace("zawgyi", "", strtolower($message));
  		$first2Characters = substr(strtolower($message), 0, 2);
  		$message = trim($message,$firstCharacter);

           
        }
        else
        {
       
          $split = explode("zawgyi",strtolower($message));
    
    		  $z_msg = $split[1];
     
    		  $firstCharacter = substr(strtolower($z_msg), 0, 1);
    		  
    		  $message = trim($z_msg,$firstCharacter);
    		
            }
         
     
    
    
     return $message;
   }

    
    function doTag()
    {
    
    // $query = DB::table('projects')->select('*')->get();
      // foreach($query as $project){
      // $project_id = $project->id;
      // $table = 'temp_'.$project_id.'_inbound_comments';
    $table='temp_17_inbound_comments';
      $comments = DB::table($table)->select('temp_id','message')->get();
      $tags =$this->gettags(); 
      // dd($comments);
      foreach ($comments as $comment) {
         $message = $comment->message;
        
         $id = $comment->temp_id;
         $tag_string = "";  
      
             foreach ($tags as $key => $value) {
              
              $kw = $value->keywords;
              $kw = explode(",",$kw);

               foreach ($kw as $key_kw => $value_kw) {

                  if (strpos($message, $value_kw) !== false)
                  {
                    
                 $tag_string.= ',' .$value->name;
       
                  }
        }
      }
      if($tag_string != ""){
         $tag_string = substr($tag_string, 1); 
         }     
         // dd($tag_string);

    $tag =  DB::table($table)->where('temp_id', $id)->update(['tags' => $tag_string,'updated_at' => now()->toDateTimeString()]);
  
       
      }
  // }
    }

   //  function doJson($path,$json)
   // {
   //  $fp = fopen($path, 'w');
   // fwrite($fp, $json);
   // fclose($fp);
   // }

//     function test()
//     {

//         // $today = date('d-m-Y h:i:s');
       
//         // $today=date('Y-m-d',strtotime($today));
//         //    dd($today);
//         $projects = DB::table('projects')->select('*')->whereIn('id', [34,38,39])->get();

//         $today = date('d-m-Y h:i:s');
//         $today_plus=date('Y-m-d h:i:s', strtotime('+1 day', strtotime($today)));
//         $dateBegin=date('Y-m-d', strtotime('-28 days', strtotime($today_plus)));
//         $dateEnd=date('Y-m-d',strtotime($today_plus));
//         foreach($projects as $project)
//          {
//             $id = $project->id;
//             $monitor_pages = $project->monitor_pages;
//             $monitor_pages = explode(',', $monitor_pages);
//             $monitor_pages = $this->Format_Page_Name($monitor_pages);
//             foreach($monitor_pages as $page)
//             {


//                 $filter_page_name=$this->Format_Page_name_single($page);
//                 $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
//                 foreach($customName as $name) $filter_page_name = $name->origin_name;
//                   $filter_pages= " posts.page_name in ('". $filter_page_name."') ";

//                   $dict_arr =array();
//                   $word_dict = DB::select("SELECT DISTINCT word FROM word_dict;");
//                     foreach($word_dict as $word_dict_result)
//                     {
//                       if(strtolower(str_replace(' ','',$word_dict_result->word)) == strtolower(str_replace(' ','',$filter_page_name)))
//                       {
//                         continue;
//                       }
//                       $dictWord =  strtolower(str_replace(' ','',$word_dict_result->word));
      
//                       $query = "SELECT cmts.id FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
//                         " WHERE  cmts.post_id IS NOT NULL AND  cmts.parent='' AND  LOWER(REPLACE(cmts.message,' ','')) like '%".$dictWord."%'  AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND " . $filter_pages .
//                         " ORDER by timestamp(cmts.created_time) DESC";

//                       $word_count = DB::select($query);
//                       $word_str = '';
//                      for($i = 0; $i < count($word_count); $i++){
//                        if($i == count($word_count) -1 ){
//                         $word_str = $word_str . $word_count[$i]->id ;
//                        }
//                        else{
//                           $word_str = $word_str . $word_count[$i]->id . ",";
//                         }
//                       }

//                       $word_str_arr = explode(",",$word_str);

//                       if(count($word_count) > 0 ){
                       
                      
//                         $formData = array(
//                             'topic_name' => $dictWord,
//                             'weight' => count($word_count),
//                             'comment_id' => $word_str_arr
//                         );
//                         $dict_arr[] = $formData;

//                       }

//                     }
//                     if(!empty($dict_arr)){
//                           $sortArray = array(); 
//                           foreach($dict_arr as $dict_arr_item){ 
//                             foreach($dict_arr_item as $key=>$value){ 
//                                 if(!isset($sortArray[$key])){ 
//                                     $sortArray[$key] = array(); 
//                                 } 
//                                 $sortArray[$key][] = $value; 
//                             } 
//                         } 

//                           $orderby = "weight"; //change this to whatever key you want from the array 

//                           array_multisort($sortArray[$orderby],SORT_DESC,$dict_arr); 
//                           $dict_arr= array_slice($dict_arr,0,50);
//                       }
// // dd($dict_arr);
//                 foreach($dict_arr as $row)
//                 {

//                   $comment_id = implode(',',$row['comment_id']);

//                   // $VoiceData []=[
                     
//                   //     'brand_id' => $id,
//                   //     'word' => $row['topic_name'],
//                   //     'weight' => $row['weight'],
//                   //     'comment_id' => $comment_id,
//                   //     'page_name' => $page,
//                   //     'today_date'=>now()->toDateTimeString(),
//                   //     'created_at' => now()->toDateTimeString(),
//                   //     'updated_at' => NULL,


//                   // ];

//               // dd($row['weight'],$row['word']);

//                DB::table('highlighted_voices')->insert(['brand_id' => $id,'word' => $row['topic_name'],'weight'=>$row['weight'],'comment_id'=>$comment_id,'page_name'=>$page,'today_date'=>now()->toDateTimeString(),'created_at'=>now()->toDateTimeString()]);  
                        
//            }


//                   // $path = storage_path('app/data_output/highlightedVoices_'.$page.'_'.now()->toDateTimeString().'.csv');
//                   // if (file_exists($path)) {
//                   //     unlink($path) ;
//                   // } 
//                   // // dd($path);
//                   // $this->doCSV($path,$VoiceData);

//                   // $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE highlighted_voices FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (brand_id,word,weight,comment_id,page_name,
//                   // today_date,created_at,updated_at)";
//                   // $pdo = DB::getPdo();
//                   // $pdo->exec($query_load);
//                   // if (file_exists($path)) {
//                   //     unlink($path) ;
//                   // }
                     
//         }
//          }
         
//     }
       function doCSV($path, $array)
      {
         $fp = fopen($path, 'w');
          $i = 0;
          foreach ($array as $fields) {
              if($i == 0){
                  fputcsv($fp, array_keys($fields));
              }
              fputcsv($fp, array_values($fields));
              $i++;
           }

         fclose($fp);
      }

        
    }

    

<?php

namespace BIMiner\Http\Controllers;

use BIMiner\MongodbData;
use BIMiner\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use Yajra\Datatables\Datatables;
use BIMiner\Project;
use BIMiner\InboundPages;
class PageManageController extends Controller
{
    //
    use GlobalController;
    public function getTopComment()
           {
                $brand_id=Input::get('brand_id');
                // $limit=10;
                // $brand_id=34;
                if(null !== Input::get('limit'))
                $limit=Input::get('limit');
                if(null !== Input::get('fday'))
                  {
                     $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
                  }
                  else
                  {
                     $dateBegin=date('Y-m-d');
                  }
              

                  if(null !==Input::get('sday'))
                  {

                     $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
                  }
              
                  else
                  {
                      $dateEnd=date('Y-m-d');

                  }
                    // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2019 01 16')));
                    // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2019 02 16')));

                   
                if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
                {
                      $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
                      $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
                      foreach($customName as $name) $filter_page_name = $name->origin_name;
                      $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
                      $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                      
                        foreach ($InboundPages_result as  $key => $row) {
                         
                          if(isset($row['id'])) $page_id= $row['id'];


                        }
                    
                }
                else
                {
                      $my_pages= $this->getOwnPage($brand_id);
                      $my_pages=explode(',', $my_pages);
                      $filter_pages=$this->getPageWhereGen($my_pages);
                }
                 $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,pros.id profileID,pros.type as profileType,pros.name as profileName, ".
                 " posts.message post_message,cmts.checked_tags_id ". 
                 " tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
                  " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,action_status,action_taken ".
                  " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
                  " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
                  " WHERE posts.id IS NOT NULL".
                  " AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".
                  " and  parent='' AND ". $filter_pages .
                  " ORDER by timestamp(cmts.created_time) DESC ";
                 
                 $query_result = DB::select($query);
               
                 $data = [];
                 
                 foreach ($query_result as  $key => $row) 
                 {
                                                            
                   //$message=$this->readmore($row->message);
                     $message=$row->message;
                     $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
                     $total = (int)$row->cmtLiked + (int)$row->cmtLove + (int)$row->cmtWow + (int)$row->cmtHaha + (int)$row->cmtSad+ (int)$row->cmtAngry;
                  
                   //$message=$row->message;
                            if (isset($row ->id))
                   {
                          
                              $data[] = array(
                                          'id'     =>$row->id,
                                          'message' => $message,
                                          'created_time' =>$row->created_time,
                                          'link' =>$row->link,
                                          'cmtType'=>$row->cmtType,
                                          'cmtLink'=>$row->cmtLink,
                                          'sentiment' =>$row->sentiment,
                                          'emotion' =>$row->emotion,
                                          'liked'=>$row->cmtLiked ,
                                          'loved'=>$row->cmtLove,
                                          'haha'=>$row->cmtHaha,
                                          'wow'=>$row->cmtWow,
                                          'sad'=>$row->cmtSad,
                                          'angry'=>$row->cmtAngry,
                                          'total'=>$total,
                                          'profileID'=>$row->profileID,
                                          'profileType'=>$row->profileType,
                                          'profileName'=>$row->profileName,
                                          'page_name'=>$row->page_name,

                                          
                                      );
                          
                         
  
                  }
                       
                }

                              
                     
                                   usort($data, function($a, $b) {
                                    if($a['total']==$b['total']) return 0;
                                    return $a['total'] < $b['total']?1:-1;
                                });

                             

                            $data = array_slice($data,0,$limit); 


                            return $data;


          }    
          public function getRelatedStatus()
          {
            $add_searchkw_con = '';
            if(null !==Input::get('kw_search'))
            {
              $kw = Input::get('kw_search');
              $add_searchkw_con = " AND (posts.message Like '%".$kw."%')";
            }
             $tag_con = '';
             if(null !== Input::get('search_tag'))
            { 
              // $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
               $tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'posts'); 
            }

              $add_post_con='';
              if(null !== Input::get('tag_id')){
              $add_post_con = " and " . $this->getTagWhereGen(Input::get('tag_id'),'posts'); 
                   }

              $brand_id=Input::get('brand_id');
              $keyword_con='';
              if('1' == Input::get('keyword_flag'))
              {
                $keyword_data = $this->getprojectkeywork($brand_id);
               
                if(isset($keyword_data [0]['main_keyword']))
                $keyword_con  =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; // params (data_arr,table alais)
                else
                $keyword_con = ' AND 1=2';
               // dd( $keyword_con);
             }
              $period_type = '';
              $additional_filter=" AND 1=1 ";
      
              if(null !==Input::get('period'))
              {
                   $period=Input::get('period');
                    $char_count = substr_count($period,"-");
                    $format_type = '';
                    if($char_count == 1)
                    {
                        $format_type ='%Y-%m';
                    }
                    else if($char_count == 2)
                    {
                         $format_type ='%Y-%m-%d';
                    }   
                    else if($char_count > 2)
                    {
                        /*this is week*/
                        $pieces = explode(" - ", $period);
                        $format_type ='%Y-%m-%d';
                        $period_type ='week';
                    }

            
                  if($period_type == 'week')
                  {
                   $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

                  }
                  else
                  {
                    $additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
                  }

             }
      


                if(null !== Input::get('fday')){
                  $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
                }
                else
                {
                $dateBegin=date('Y-m-d');
                }
          

                if(null !==Input::get('sday'))  {

                  $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
                }
          
                else
                {
                  $dateEnd=date('Y-m-d');
                }

           $filter_pages = ' 1=1 ';
         

          // if(null !== Input::get('filter_page_name'))
          //  {
          //   $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
          //   $filter_pages .= " and page_name in ('".$filter_page_name."') ";
          //  }
           // dd(Input::get('admin_page'));
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
              $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
              $filter_pages .= " and  posts.page_name in ('". $filter_page_name."') ";
           }
           else  
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);

              if  ('1' == Input::get('other_page'))
              $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
              else
              $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages);
           }

           $visitor_status= " And visitor = 0";
            if(null !== Input::get('visitor') && 1 == Input::get('visitor'))
             {
              $visitor_status= " And visitor = 1";
             }

          $query='';
          $query_result=[];

         
            $query = "SELECT Liked,Love,Haha,Wow,Angry,Sad,shared , posts.id ".
          "FROM temp_inbound_posts posts ".
          " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND". $filter_pages  .  $keyword_con . $visitor_status . $add_searchkw_con . $tag_con . $additional_filter . $add_post_con .
          " ORDER by timestamp(posts.created_time) DESC ";

          $query_result = DB::select($query);

          $reaction_total = 0 ; $like = 0; $love = 0; $wow = 0; $haha = 0; $sad = 0; $angry = 0; $share = 0 ; $post_id = [];
          foreach ($query_result as $key => $value) {
            $like +=$value->Liked;
            $love +=$value->Love;
            $wow +=$value->Wow;
            $haha +=$value->Haha;
            $sad +=$value->Sad;
            $angry +=$value->Angry;
            $share +=$value->shared;
            $reaction_total = $like + $love + $wow + $haha + $sad + $angry;
            $post_id[] = $value->id;
          }
          $id_str = implode(',',  $post_id);
          $id_str = "'" . str_replace(",", "','", $id_str) . "'";
          
          $cmt_count = 0;
          $cmtquery = "SELECT count(*) cmt_count  "."FROM temp_inbound_comments "." WHERE post_id IN (".$id_str.")";
          $cmt_query_result = DB::select($cmtquery);
          foreach ($cmt_query_result as $key => $value) {
           $cmt_count =  $value->cmt_count;
          }
            echo json_encode(array($reaction_total,$cmt_count,$share));
          }
    public function getTopAndLatestPost()
    {

              //  get keywork
              //  $keyword_data = ProjectKeyword::find(1);//projectid
                $brand_id=Input::get('brand_id');
                $format_type=Input::get('format_type'); // for top post
                $page_id='';$imgurl='';
                $filter_keyword ='';
                $filter_sentiment=''; $limit=10;
                if(null !== Input::get('keyword'))
                $filter_keyword=Input::get('keyword'); 

                if(null !== Input::get('sentiment'))
                $filter_sentiment=Input::get('sentiment'); 

                if(null !== Input::get('limit'))
                $limit=Input::get('limit');

                /*$brand_id=30;*/
           
               

                  if(null !== Input::get('fday'))
                  {
                     $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
                  }
                  else
                  {
                     $dateBegin=date('Y-m-d');
                  }
              

                  if(null !==Input::get('sday'))
                  {

                     $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
                  }
              
                else
                  {
                      $dateEnd=date('Y-m-d');

                  }
                    /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2017 06 01')));
                    $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2017 06 30')));*/

                   
                if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
                {
                      $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
                      $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
                       foreach($customName as $name) $filter_page_name = $name->origin_name;
                      $filter_pages= " posts.page_name in ('". $filter_page_name."') ";
                      $InboundPages_result =$this->getPageIdfromMongo($filter_page_name);
                      
                        foreach ($InboundPages_result as  $key => $row) {
                         
                          if(isset($row['id'])) $page_id= $row['id'];
                          if(isset($row['imageurl'])) $imgurl= $row['imageurl'];


                        }
                    
                }
                else
                {
                      $my_pages= $this->getOwnPage($brand_id);
                      $my_pages=explode(',', $my_pages);
                      $filter_pages=$this->getPageWhereGen($my_pages);
                }
                 

                      $add_con='';
                      if($filter_keyword !== '')
                        $add_con = " and posts.message Like '%".$filter_keyword."%'";

                      if($filter_sentiment !== '')
                      {
                        $add_con .= " and posts.sentiment = '".$filter_sentiment."'";
                      }



                $query='';


                $query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,replace(shared, ',', '') shared,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
                " created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,posts.type post_type,posts.visitor,cmts.checked_sentiment ".
                " cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict,cmts.id cmt_id,(CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
                " ELSE 'mdi-star' ".
                " END) isBookMark ".
                "FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
                " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND". $filter_pages . $add_con .
                " ORDER by timestamp(posts.created_time) DESC ";
 
                $query_result = DB::select($query);

                 $data = [];
                 $data_mention=[];
                 foreach ($query_result as  $key => $row) 
                 {
                    
                      $pos=($row->cmt_sentiment=='pos' && $row->parent=== '' )?1:0 ;
                      $neg=($row->cmt_sentiment=='neg' && $row->parent=== '' )?1:0;
                      $anger =($row->cmt_emotion=='anger' && $row->parent==='' )?1:0 ;
                      $interest=($row->cmt_emotion=='interest' && $row->parent==='' )?1:0;
                      $disgust=($row->cmt_emotion=='disgust' && $row->parent ==='')?1:0;
                      $fear=($row->cmt_emotion=='fear' && $row->parent=== '' )?1:0;
                      $joy=($row->cmt_emotion=='joy' && $row->parent==='')?1:0;
                      $like=($row->cmt_emotion=='like' && $row->parent==='' )?1:0;
                      $love=($row->cmt_emotion=='love' && $row->parent==='' )?1:0;
                      $neutral=($row->cmt_emotion=='neutral' && $row->parent==='' )?1:0;
                      $sadness=($row->cmt_emotion=='sadness' && $row->parent==='' )?1:0;
                      $surprise=($row->cmt_emotion=='surprise' && $row->parent==='' )?1:0;
                      $trust=($row->cmt_emotion=='trust' && $row->parent==='')?1:0;
                      $comment_count=(isset($row->cmt_id) && $row->parent==='')?1:0;
                   
                   //$message=$this->readmore($row->message);
                     $message=$row->message;
                     $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
                     $total = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry+(int)$row->shared;
                     $total_react = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry;
                   //$message=$row->message;
                            if (isset($row ->id))
                   {
                                 $id= $row ->id;
                   
                          if (!array_key_exists($id , $data)) 
                          {
                              $data[$id] = array(
                                          'id'     =>$row->id,
                                          'message' => $message,
                                          'page_name' =>$row->page_name,
                                          'created_time' =>$row->created_time,
                                          'link' =>$row->link,
                                          'full_picture' =>$row->full_picture,
                                          'sentiment' =>$row->sentiment,
                                          'emotion' =>$row->emotion,
                                          'positive'=> $pos,'negative'=>$neg,
                                          'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,
                                          'sad'=>$row->Sad,'angry'=>$row->Angry,'shared'=>$row->shared,
                                          'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                                          'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                                          'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                                          'format_type'=>$format_type,
                                          'post_type' =>$row->post_type,
                                          'total'=>$total + $comment_count,
                                          'total_react'=>$total_react,
                                          'comment_count'=> $comment_count,
                                          'visitor'=>$row->visitor,
                                          'page_id'=>$page_id,
                                          'imgurl'=>$imgurl,
                                          'isBookMark'=>$row->isBookMark
                                      );
                          }
                          else
                          {
                           $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
                           $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
                           $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
                           $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
                           $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
                           $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
                           $data[$id]['like'] = (int) $data[$id]['like'] + $like;
                           $data[$id]['love'] = (int) $data[$id]['love'] + $love;
                           $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
                           $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
                           $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
                           $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;
                           $data[$id]['comment_count'] = (int) $data[$id]['comment_count'] + $comment_count;
                            $data[$id]['total'] = (int) $data[$id]['total'] + $comment_count;



                          } 
  
                          }
                       
                        }

                                  $data_mention=$data;
                      // dd($data);

                            if($format_type === "top_post")
                            {

                                   usort($data_mention, function($a, $b) {
                                    if($a['total']==$b['total']) return 0;
                                    return $a['total'] < $b['total']?1:-1;
                                });

                             }

                            $data_mention = array_slice($data_mention,0,$limit); 


                            return $data_mention;

    }
    public function getIndustryComment()
    {
            $username = auth()->user()->username;
      // dd($username);
      // dd(Input::get('highlight_text'));
      $add_searchkw_con = '';
      if(null !==Input::get('kw_search'))
      {
        $kw = Input::get('kw_search');
        $add_searchkw_con = " AND (cmts.message Like '%".$kw."%')";
      }

      $brand_id=Input::get('brand_id');
      // dd($brand_id);
      $keyword_con = '';
      if('1' == Input::get('keyword_flag'))
      {
        $keyword_data = $this->getprojectkeywork($brand_id);
        if(isset($keyword_data [0]['main_keyword']))
        $keyword_con  =  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")"; // param (data_arr,table alais)
        else
       $keyword_con = ' AND 1=2';
       // dd( $keyword_con);
    
     }
      $period_type = '';
      $additional_filter=" AND 1=1 ";
      
        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

      
            if($period_type == 'week')
            {
             $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

            }
            else
            {
                $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
            }

       }

   
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment ='';
      $is_competitor = "false";
    
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment');

      if(null !== Input::get('is_competitor'))
      $is_competitor=Input::get('is_competitor'); 
   
       $add_global_filter='';
      if(null !== Input::get('tsearch_senti'))
      {
        $add_global_filter .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
      }
       if(null !== Input::get('tsearch_tag'))
      {
        $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'),'cmts');
      }
      // dd($add_global_filter);

       if(null !== Input::get('fday'))
        {
          $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
       else
        {
          $dateBegin=date('Y-m-d');
        }
    

        if(null !==Input::get('sday'))
        {

          $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
        
        else
        {
          $dateEnd=date('Y-m-d');

        }
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));

            $filter_pages = ' 1=1 ';
         

       if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
       {
           $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
            $filterRes = DB::table('industry')->where('type',$filter_page_name)->get()->toArray();
          

             
               foreach($filterRes as $pgs){
                $industry_pages[] = $pgs->origin_name;
               }
             $industry_str = "'" . implode("','", $industry_pages) . "'";
             $industry_str = preg_replace('/(\r\n|\r|\n)+/', " ",$industry_str);
              // dd($industry_str);
        $filter_pages .= " and  cmts.page_name in (". $industry_str.") ";
       }
       else  
       {
            $my_pages = $this->getInboundPages($brand_id);
            $filter_pages .= ' and cmts.page_name In('.$my_pages.')';
        // $my_pages= $this->getOwnPage($brand_id);
        // $my_pages=explode(',', $my_pages);

        // if  ('1' == Input::get('other_page'))
        // $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
        // else
        // $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages);
       }

      $add_con='';
      $date_filter='';
      if(null !== Input::get('post_id'))
      {
        $add_con = "and cmts.post_id='".Input::get('post_id')."'"; 
        $filter_pages ='  1=1 '; //if we know post_id no need to filter pages. post_id can get data unique
      }
 
       $tags =$this->gettagsBybrandid($brand_id);
// dd($tags);
       //if comefrom == post we don't need to consider cmts created time , if not some comment can missing due to date filter that come from post page
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = ' 1=1 ';
      }
      else
      {
        $date_filter=" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
      }
      $highlight_con ='';
      $highlight_cmt_con ='';
      // dd("HT".Input::get('highlight_text'));
        // dd(Input::get('highlight_text'));
      if(null !== Input::get('wordcloud_text'))
        { 

          $wordcloud_text = $request->session()->get( Input::get('wordcloud_text'));
          $wordcloud_text = implode(',',$wordcloud_text);
          $wordcloud_text = "'" . str_replace(",", "','", $wordcloud_text) . "'";
          $highlight_cmt_con = " and cmts.id in (".$wordcloud_text.")";
         
         
          $query="SELECT SUM(cmtLiked) + SUM(cmtLove) + SUM(cmtWow) + SUM(cmtHaha) + SUM(cmtSad) + SUM(cmtAngry)reactTotal,cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,pros.id profileID,pros.type as profileType,pros.name as profileName, ".
                 " posts.message post_message,cmts.checked_tags_id ". 
                 " tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
                  " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,action_status,action_taken ".
                  " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
                  " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
                  " WHERE posts.id IS NOT NULL".
                  " and  parent='' AND tag_flag=1 AND  ".$date_filter." AND ". $filter_pages . $add_con . $add_global_filter. $highlight_cmt_con . $add_searchkw_con . 
                  " ORDER by timestamp(cmts.created_time) DESC ";
          // dd($query);
        // return $this->GenerateHighlightedcomment($query,$brand_id,$tags);
          
        
        }
      else
      {
        if(null !== Input::get('highlight_text'))
          { 
             $highlight_text= str_replace(' ', '', Input::get('highlight_text'));
           
             $highlight_con = " and ( REPLACE(REPLACE(REPLACE(cmts.message, '\r', ''), '\n', ''),' ','')) Like '%".$highlight_text."%'";
             // $highlight_con = " and cmts.message Like '%".$highlight_text."%'"; /*ZTD*/
          }
          // dd($additional_filter);
             $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,pros.id profileID,pros.type as profileType,pros.name as profileName,cmts.checked_tags_id tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
            " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,action_status,action_taken ".
            " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
            " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
            " WHERE posts.id IS NOT NULL".
            " and  parent=''  AND  ".$date_filter." AND ". $filter_pages . $add_con . $add_global_filter. $highlight_con . $keyword_con .
            $additional_filter . $add_searchkw_con .

            " ORDER by timestamp(cmts.created_time) DESC ";
      } 
 
      // dd($query);
     $data = DB::select($query);
     // dd($data);

   //   $data = [];
   // // dd($data);
   //   // $keyword_data = [];
     $keywordToHighlight = Input::get('highlight_text');
   //   foreach ($data1 as  $key => $row) {
   //      $message = $row->message;
   //      $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
   //      $message = $this->highLight($keywordToHighlight,$message);
   //      if(isset($row->post_message)){
   //      $post_message = $row->post_message;
   //      $post_message=mb_convert_encoding($post_message, 'UTF-8', 'UTF-8');
   //      $post_message = $this->highLight($keywordToHighlight,$post_message);
   //    }
   //    else
   //      $post_message = '';

   //      // dd($post_message);
   //      $data[]= array(
   //          'id'     =>$row->id,
   //          'cmtLiked'=>$row->cmtLiked,
   //          'cmtLove'=>$row->cmtLove,
   //          'cmtWow'=>$row->cmtWow,
   //          'cmtHaha'=>$row->cmtHaha,
   //          'cmtSad'=>$row->cmtSad,
   //          'cmtAngry'=>$row->cmtAngry,
   //          'cmtType'=>$row->cmtType,
   //          'cmtLink'=>$row->cmtLink,
   //          'profileID'=>$row->profileID,
   //          'profileType'=>$row->profileType,
   //          'profileName'=>$row->profileName,
   //          'post_message' => $post_message,
   //          'tags'=>$row->tags,
   //          'post_id'=>$row->post_id,
   //          'message'=>$message,
   //          'link'=>$row->link,
   //          'created_time'=>$row->created_time,
   //          'sentiment'=>$row->sentiment,
   //          'emotion'=>$row->emotion,
   //          'page_name'=>$row->page_name,
   //          'hp'=>$row->hp,
   //          'hp_tags'=>$row->hp_tags,
   //          'action_status'=>$row->action_status,
   //          'action_taken'=>$row->action_taken
   //        );
   //      }
   //      // dd($mention);
     

     $project = Project::find($brand_id);
     $monitor_pages = $project->monitor_pages;
     $monitor_pages = explode(',', $monitor_pages);

     $permission_data = $this->getPermission(); 

     $login_user = auth()->user()->id;
     $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
     foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

        return Datatables::of($data)
              
             ->addColumn('post_div', function ($data)   use($tags,$permission_data,$monitor_pages,$highlight_cmt_con ,$username,$action_permission,$keywordToHighlight) {
               $post_message_tag='';
               if($highlight_cmt_con  !== '')
               {
                $post_message=$this->readmore($data->post_message);
                $post_message=mb_convert_encoding($post_message, 'UTF-8', 'UTF-8');
                $post_message = $this->highLight($keywordToHighlight,$post_message);
                $post_message_tag='<span style="color:#1e88e5;font-size:14px;font-weight:bold">"Post"</span><p class="m-t-10">'.$post_message.' . . .</p>';
               }
              $page_name=$data->page_name ;
                if(is_numeric($page_name))
                {
                   $input = preg_quote($page_name, '~'); // don't forget to quote input string!
                   $keys = preg_grep('~' . $input . '~', $monitor_pages);
                   $vals = array();
                      foreach ( $keys as $key )
                      {
                         $page_name= $key;
                      }
                     
                }


                        $message = $data->message;
                        $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
                        $message = $this->highLight($keywordToHighlight,$message);
     



              //generate comment link
              $profilePhoto='';$ProfileName='';
              $profileLink='javascript:void(0)';
              $fb_p_id=explode('_', $data->post_id);
              $fb_p_id=$fb_p_id[1];
              $fb_c_id=explode('_', $data->id);
              $fb_c_id=$fb_c_id[1];
                if($data->profileName <> '') $ProfileName=$data->profileName; 
                else if($data->profileID <> '')  $ProfileName=$data->profileID;
                else  $ProfileName=$page_name;

              if($data->profileType === 'number')
              {
                $profilePhoto='https://graph.facebook.com/'.$data->profileID.'/picture?type=large';
                $ProfileName=$page_name;
              }
              
              if(isset($data->profileID))
              $profileLink='https://www.facebook.com/'.$data->profileID;

              $page_id='';
              $arr_page_id=explode("-",$page_name);
              if(count($arr_page_id)>0)
              {
                $last_index=$arr_page_id[count($arr_page_id)-1];
                if(is_numeric($last_index))
                $page_id = $last_index;
                else
                $page_id = $page_name;
              }
              else
              {
                $page_id=$page_name;
              }

              $comment_Link= 'https://www.facebook.com/'.$fb_c_id;
              $classforbtn='';$stylefora='';$bgcolor='';
                    if($data->sentiment=='neg')
                      {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                    else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                      else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
                      $html = '<div class="profiletimeline"><div class="sl-item">';
                      if($profilePhoto<>'')
                      $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                      else
                      /*$html .= '<div class="sl-left user-img"> <span class="round"><i class="mdi mdi-comment-account"></i></span> </div>';*/
                    $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                     
                       $html .='<div class="form-material sl-right"><div> <div>
                    <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a>';
               
                    if($data->hp <> '' && $data->hp_tags <>'')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    else if($data->hp <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                    else if($data->hp_tags <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                    $html .='<span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                     // $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                    
                     $html.='<p>Sentiment : <select id="sentiment_'.$data->id.'"  class="form-material form-control in-comment-select edit_senti ';
                     if($data->sentiment=="")$html.='"><option value="" selected="selected" ></option>';
                     if($data->sentiment=="pos")$html.='text-success" >';
                     if($data->sentiment=="neg")$html.='text-red" >';
                     if($data->sentiment=="neutral" || $data->sentiment == "NA")$html.='text-warning" >';
                  
                      $html.='<option value="pos" class="text-success"';
                      if($data->sentiment == "pos")
                      $html.= 'selected="selected"';
                      $html.= '>Positive</option><option value="neg" class="text-red"';
                      if($data->sentiment == "neg")
                      $html.= 'selected="selected"';
                      $html.= '>Negative</option><option value="neutral" class="text-warning"';
                      if($data->sentiment == "neutral" || $data->sentiment == "NA")
                      $html.= 'selected="selected"';      
                      $html.= '>Neutral</option></select>';

                     
                      $html.=' | Tag :<select id="tags_'.$data->id.'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                       $tag_array=explode(',', $data->tags);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';

                      if($action_permission == 0){
                      $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                     $html.='<a class="show_alert" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id=""><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                     }
                     else{

                      if($permission_data['edit'] === 1)
                      $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                     $html.='<a class="get-code" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id="add_tag"><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                   }
                      
                      $html.='</p><p class="m-t-10">'.$message.'</p>'.$post_message_tag.'</div>';
                       if($data->cmtType =='sticker' || $data->cmtType =='photo' || $data->cmtType =='share' || $data->cmtType =='share_large_image' || $data->cmtType =='animated_image_share' || $data->cmtType == 'video_inline' )
                       {
                          $html .= '<a class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'.
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                              '<img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">'.
                      '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                       else if($data->cmtType =='animated_image_video' || $data->cmtType =='video_share_youtube' || $data->cmtType =='video_share_highlighted')
                       {
                         $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'.$data->id.'">'.
                          '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                          '<iframe src="https://www.facebook.com/plugins/video.php?href='.$data->cmtLink.'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'.
                          '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                    
                      $html.= '<div class="sl-right">';
                    
                        if((int)$data->cmtLiked > 0)
                        $html.='👍 '.$this->thousandsCurrencyFormat($data->cmtLiked);
                       if((int)$data->cmtLove > 0)
                        $html.=' ❤️ '.$this->thousandsCurrencyFormat($data->cmtLove);
                       if((int)$data->cmtHaha > 0)
                        $html.=' 😆 '.$this->thousandsCurrencyFormat($data->cmtHaha);
                       if((int)$data->cmtWow > 0)
                        $html.=' 😮 '.$this->thousandsCurrencyFormat($data->cmtWow);
                       if((int)$data->cmtSad > 0)
                        $html.=' 😪 '.$this->thousandsCurrencyFormat($data->cmtSad);
                       if((int)$data->cmtAngry > 0)
                        $html.=' 😡 '.$this->thousandsCurrencyFormat($data->cmtAngry);
                        $html.='</div>
                      <div class="sl-right" style="float: right;">
                    <div style="display: inline-block"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green popup_post" id="'.$data->post_id.'">See Post</a></div>
                     <div style="display: inline-block"><a href="'.$comment_Link.'" class="waves-effect waves-light btn-sm btn-gray" target="_blank">Take Action</a></div>
                    <div style="display: inline-block" class="btn-group">';
                    if($data->action_status === "Action Taken")
                    $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Action Taken
                   </button>';
                   else
                   $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Require Action
                   </button>';
                $html.='<div class="dropdown-menu">
                  <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Require Action</a>
                  <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Action Taken</a>

                </div></div></div></div></div>';

                      return $html;

                  })
             ->addColumn('reaction', function ($data) {
              $reaction_total = (int)$data->cmtLiked + (int)$data->cmtLove + (int)$data->cmtWow + (int)$data->cmtAngry + (int)$data->cmtSad + (int)$data->cmtHaha ;
                      return "<span style='font-weight:bold'>" . number_format($reaction_total)."</span>";
                  }) 
            ->rawColumns(['post_div','reaction'])
         
            ->make(true);

    }

    public function getRelatedcomment(Request $request)
    {
      $username = auth()->user()->username;
      // dd($username);
      // dd(Input::get('highlight_text'));
      $add_searchkw_con = '';
      if(null !==Input::get('kw_search'))
      {
        $kw = Input::get('kw_search');
        $add_searchkw_con = " AND (cmts.message Like '%".$kw."%')";
      }

      $brand_id=Input::get('brand_id');
      // dd($brand_id);
      $keyword_con = '';
      if('1' == Input::get('keyword_flag'))
      {
        $keyword_data = $this->getprojectkeywork($brand_id);
        if(isset($keyword_data [0]['main_keyword']))
        $keyword_con  =  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'cmts') . ")"; // param (data_arr,table alais)
        else
       $keyword_con = ' AND 1=2';
       // dd( $keyword_con);
    
     }
      $period_type = '';
      $additional_filter=" AND 1=1 ";
      
        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

      
            if($period_type == 'week')
            {
             $additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

            }
            else
            {
                $additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
            }

       }

   
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment ='';
      $is_competitor = "false";
    
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment');

      if(null !== Input::get('is_competitor'))
      $is_competitor=Input::get('is_competitor'); 
   
       $add_global_filter='';
      if(null !== Input::get('tsearch_senti'))
      {
        $add_global_filter .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
      }
       if(null !== Input::get('tsearch_tag'))
      {
        $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'),'cmts');
      }
      // dd($add_global_filter);

       if(null !== Input::get('fday'))
        {
          $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
       else
        {
          $dateBegin=date('Y-m-d');
        }
    

        if(null !==Input::get('sday'))
        {

          $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
        
        else
        {
          $dateEnd=date('Y-m-d');

        }
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));

            $filter_pages = ' 1=1 ';
         

       if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
       {
        $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
        $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
        foreach($customName as $name) $filter_page_name = $name->origin_name;
        $filter_pages .= " and  cmts.page_name in ('". $filter_page_name."') ";
       }
       else  
       {
            $my_pages = $this->getInboundPages($brand_id);
            $filter_pages .= ' and cmts.page_name In('.$my_pages.')';
        // $my_pages= $this->getOwnPage($brand_id);
        // $my_pages=explode(',', $my_pages);

        // if  ('1' == Input::get('other_page'))
        // $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
        // else
        // $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages);
       }

      $add_con='';
      $date_filter='';
      if(null !== Input::get('post_id'))
      {
        $add_con = "and cmts.post_id='".Input::get('post_id')."'"; 
        $filter_pages ='  1=1 '; //if we know post_id no need to filter pages. post_id can get data unique
      }
 
       $tags =$this->gettagsBybrandid($brand_id);
// dd($tags);
       //if comefrom == post we don't need to consider cmts created time , if not some comment can missing due to date filter that come from post page
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = ' 1=1 ';
      }
      else
      {
        $date_filter="( (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        $date_filter .=" OR (cmts.created_time >='".$dateBegin."' AND cmts.parent <> '' AND cmts.parent is NOT NULL))";
      }
      $highlight_con ='';
      $highlight_cmt_con ='';
      // dd("HT".Input::get('highlight_text'));
        // dd(Input::get('highlight_text'));
      if(null !== Input::get('wordcloud_text'))
        { 

          $wordcloud_text = $request->session()->get( Input::get('wordcloud_text'));
          $wordcloud_text = implode(',',$wordcloud_text);
          $wordcloud_text = "'" . str_replace(",", "','", $wordcloud_text) . "'";
          $highlight_cmt_con = " and cmts.id in (".$wordcloud_text.")";
         
         
          $query="SELECT SUM(cmtLiked) + SUM(cmtLove) + SUM(cmtWow) + SUM(cmtHaha) + SUM(cmtSad) + SUM(cmtAngry)reactTotal,cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,local_picture,pros.id profileID,pros.type as profileType,pros.name as profileName, ".
                 " posts.message post_message,cmts.checked_tags_id ". 
                 " tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
                  " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,action_status,action_taken ".
                  " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
                  " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
                  " WHERE posts.id IS NOT NULL".
                  " and  parent='' AND   ".$date_filter." AND ". $filter_pages . $add_con . $add_global_filter. $highlight_cmt_con . $add_searchkw_con . 
                  " ORDER by timestamp(cmts.created_time) DESC ";
          // dd($query);
        // return $this->GenerateHighlightedcomment($query,$brand_id,$tags);
          
        
        }
      else
      {
        if(null !== Input::get('highlight_text'))
          { 
             $highlight_text= str_replace(' ', '', Input::get('highlight_text'));
           
             $highlight_con = " and ( REPLACE(REPLACE(REPLACE(cmts.message, '\r', ''), '\n', ''),' ','')) Like '%".$highlight_text."%'";
             // $highlight_con = " and cmts.message Like '%".$highlight_text."%'"; /*ZTD*/
          }
          // dd(Input::get('admin_page'),$dateBegin,$dateEnd);
             $query="SELECT cmts.parent,cmts.cmtLiked,cmts.cmtLove,cmts.cmtWow,cmts.cmtHaha,cmts.cmtSad,cmts.cmtAngry,cmts.cmtType,cmts.cmtLink,cmts.local_picture,pros.id profileID,pros.type as profileType,pros.name as profileName,cmts.checked_tags_id tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time,cmts.change_predict,cmts.checked_predict,".
            " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,cmts.action_status,cmts.action_taken ".
            " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
            " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
            " WHERE posts.id IS NOT NULL".
            " and  ".$date_filter." AND ". $filter_pages . $add_con . $add_global_filter. $highlight_con . $keyword_con .
            $additional_filter . $add_searchkw_con .

            " ORDER by timestamp(cmts.created_time) DESC ";
      } 

 // dd($dateBegin,$dateEnd,Input::get('admin_page'));
      // return  $query;
     $query_result = DB::select($query);
     $data = [];
     $data_child = [];


     foreach ($query_result as  $key => $row) {
      if($row->parent !== '' && $row->parent !== NULL){
        
        $data_child [] = $row;
       
      }
      else{
         $data[] = $row;
      }

    }
    $arr =[];
    foreach ($data_child as $key => $value) {
      array_push($arr, $value->parent);
    }

    $child_values = array_count_values($arr);
    // dd($child_values);
   
     
        $keywordToHighlight = Input::get('highlight_text');
      //   $message = $row->message;
      //   $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
      //   $message = $this->highLight($keywordToHighlight,$message);
      //   if(isset($row->post_message)){
      //   $post_message = $row->post_message;
      //   $post_message=mb_convert_encoding($post_message, 'UTF-8', 'UTF-8');
      //   $post_message = $this->highLight($keywordToHighlight,$post_message);
      // }
      // else
      //   $post_message = '';

      //   // dd($post_message);
      //   $data[]= array(
      //       'id'     =>$row->id,
      //       'cmtLiked'=>$row->cmtLiked,
      //       'cmtLove'=>$row->cmtLove,
      //       'cmtWow'=>$row->cmtWow,
      //       'cmtHaha'=>$row->cmtHaha,
      //       'cmtSad'=>$row->cmtSad,
      //       'cmtAngry'=>$row->cmtAngry,
      //       'cmtType'=>$row->cmtType,
      //       'cmtLink'=>$row->cmtLink,
      //       'profileID'=>$row->profileID,
      //       'profileType'=>$row->profileType,
      //       'profileName'=>$row->profileName,
      //       'post_message' => $post_message,
      //       'tags'=>$row->tags,
      //       'post_id'=>$row->post_id,
      //       'message'=>$message,
      //       'link'=>$row->link,
      //       'created_time'=>$row->created_time,
      //       'sentiment'=>$row->sentiment,
      //       'emotion'=>$row->emotion,
      //       'page_name'=>$row->page_name,
      //       'hp'=>$row->hp,
      //       'hp_tags'=>$row->hp_tags,
      //       'action_status'=>$row->action_status,
      //       'action_taken'=>$row->action_taken
      //     );
      //   }
        // dd($mention);
     

     $project = Project::find($brand_id);
     $monitor_pages = $project->monitor_pages;
     $monitor_pages = explode(',', $monitor_pages);

     $permission_data = $this->getPermission(); 

     $login_user = auth()->user()->id;
     $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
     foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

        return Datatables::of($data)
              
             ->addColumn('post_div', function ($data)   use($tags,$permission_data,$monitor_pages,$highlight_cmt_con ,$username,$action_permission,$keywordToHighlight,$child_values) {
               $post_message_tag='';
               if($highlight_cmt_con  !== '')
               {
                $post_message=$this->readmore($data->post_message);
                $post_message=mb_convert_encoding($post_message, 'UTF-8', 'UTF-8');
                $post_message = $this->highLight($keywordToHighlight,$post_message);
                $post_message_tag='<span style="color:#1e88e5;font-size:14px;font-weight:bold">"Post"</span><p class="m-t-10">'.$post_message.' . . .</p>';
               }
              $page_name=$data->page_name ;
                if(is_numeric($page_name))
                {
                   $input = preg_quote($page_name, '~'); // don't forget to quote input string!
                   $keys = preg_grep('~' . $input . '~', $monitor_pages);
                   $vals = array();
                      foreach ( $keys as $key )
                      {
                         $page_name= $key;
                      }
                     
                }


                        $message = $data->message;
                        $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
                        $message = $this->highLight($keywordToHighlight,$message);
     



              //generate comment link
              $profilePhoto='';$ProfileName='';
              $profileLink='javascript:void(0)';
              $fb_p_id=explode('_', $data->post_id);
              $fb_p_id=$fb_p_id[1];
              $fb_c_id=explode('_', $data->id);
              $fb_c_id=$fb_c_id[1];
                if($data->profileName <> '') $ProfileName=$data->profileName; 
                else if($data->profileID <> '')  $ProfileName=$data->profileID;
                else  $ProfileName=$page_name;

              if($data->profileType === 'number')
              {
                $profilePhoto='https://graph.facebook.com/'.$data->profileID.'/picture?type=large';
                $ProfileName=$page_name;
              }
              
              if(isset($data->profileID))
              $profileLink='https://www.facebook.com/'.$data->profileID;

              $page_id='';
              $arr_page_id=explode("-",$page_name);
              if(count($arr_page_id)>0)
              {
                $last_index=$arr_page_id[count($arr_page_id)-1];
                if(is_numeric($last_index))
                $page_id = $last_index;
                else
                $page_id = $page_name;
              }
              else
              {
                $page_id=$page_name;
              }

              $comment_Link= 'https://www.facebook.com/'.$fb_c_id;
              $classforbtn='';$stylefora='';$bgcolor='';
                    if($data->sentiment=='neg')
                      {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                    else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                      else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
                      $html = '<div class="profiletimeline"><div class="sl-item">';
                      if($profilePhoto<>'')
                      $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                      else
                      /*$html .= '<div class="sl-left user-img"> <span class="round"><i class="mdi mdi-comment-account"></i></span> </div>';*/
                    $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                     
                       $html .='<div class="form-material sl-right"><div> <div>
                    <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a>';
               
                    if($data->hp <> '' && $data->hp_tags <>'')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    else if($data->hp <> '' ||  $data->change_predict == 1 ||  $data->checked_predict == 1)
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                    else if($data->hp_tags <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                    $html .='<span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                     // $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                    
                     $html.='<p>Sentiment : <select id="sentiment_'.$data->id.'"  class="form-material form-control in-comment-select edit_senti ';
                     if($data->sentiment=="")$html.='"><option value="" selected="selected" ></option>';
                     if($data->sentiment=="pos")$html.='text-success" >';
                     if($data->sentiment=="neg")$html.='text-red" >';
                     if($data->sentiment=="neutral" || $data->sentiment == "NA")$html.='text-warning" >';
                  
                      $html.='<option value="pos" class="text-success"';
                      if($data->sentiment == "pos")
                      $html.= 'selected="selected"';
                      $html.= '>Positive</option><option value="neg" class="text-red"';
                      if($data->sentiment == "neg")
                      $html.= 'selected="selected"';
                      $html.= '>Negative</option><option value="neutral" class="text-warning"';
                      if($data->sentiment == "neutral" || $data->sentiment == "NA")
                      $html.= 'selected="selected"';      
                      $html.= '>Neutral</option></select>';

                     
                      $html.=' | Tag :<select id="tags_'.$data->id.'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                       $tag_array=explode(',', $data->tags);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';

                      if($action_permission == 0){
                      $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                     $html.='<a class="show_alert" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id=""><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                     }
                     else{

                      if($permission_data['edit'] === 1)
                      $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                     $html.='<a class="get-code" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id="add_tag"><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                   }
                      
                      $html.='</p><p class="m-t-10">'.$message.'</p>'.$post_message_tag.'</div>';
                       if($data->cmtType =='sticker' || $data->cmtType =='share' || $data->cmtType =='share_large_image' || $data->cmtType =='animated_image_share' || $data->cmtType == 'video_inline' )
                       {
                          $html .= '<a class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'.
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                              '<img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">'.
                      '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                       else if ( $data->cmtType =='photo') {
                         if(null !== $data->local_picture) {$picture = $data->local_picture;}
                         else{ $picture = $data->cmtLink;}

                        $html .= '<a class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'.
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                              '<img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$picture .'">'.
                      '</span><span class="postPanel_nameTextContainer"></span></a>';
                        
                       }
                       else if($data->cmtType =='animated_image_video' || $data->cmtType =='video_share_youtube' || $data->cmtType =='video_share_highlighted')
                       {
                         $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'.$data->id.'">'.
                          '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                          '<iframe src="https://www.facebook.com/plugins/video.php?href='.$data->cmtLink.'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'.
                          '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                    
                      $html.= '<div class="sl-right">';
                    
                        if((int)$data->cmtLiked > 0)
                        $html.='👍 '.$this->thousandsCurrencyFormat($data->cmtLiked);
                       if((int)$data->cmtLove > 0)
                        $html.=' ❤️ '.$this->thousandsCurrencyFormat($data->cmtLove);
                       if((int)$data->cmtHaha > 0)
                        $html.=' 😆 '.$this->thousandsCurrencyFormat($data->cmtHaha);
                       if((int)$data->cmtWow > 0)
                        $html.=' 😮 '.$this->thousandsCurrencyFormat($data->cmtWow);
                       if((int)$data->cmtSad > 0)
                        $html.=' 😪 '.$this->thousandsCurrencyFormat($data->cmtSad);
                       if((int)$data->cmtAngry > 0)
                        $html.=' 😡 '.$this->thousandsCurrencyFormat($data->cmtAngry);
                        $html.='</div>';
                      $html.='<div class="sl-right" style="float: right;">';

                if(isset($child_values) && array_key_exists($data->id, $child_values)) {
                  $html.='<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green reply_count" id="'.$data->id.'" style="margin-right:4px;"> Reply ('.$child_values[$data->id].')</a>';
                  // $html.='<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green reply_count" id="'.$data->id.'" style="float:right"><span class="mdi mdi-reply"></span> '.$child_values[$data->id].'</a></div></div>';
                // $html.='<span class="mdi mdi-comment-outline reply_count" id="'.$data->id.'" style="float:left"></span>'.$child_values[$data->id].'</div></div>background:#28a755';
              }

                    $html.='<div style="display: inline-block"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green popup_post" id="'.$data->post_id.'">See Post</a></div>
                     <div style="display: inline-block"><a href="'.$comment_Link.'" class="waves-effect waves-light btn-sm btn-gray" target="_blank">Take Action</a></div>
                    <div style="display: inline-block" class="btn-group">';
                    if($data->action_status === "Action Taken")
                    $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Action Taken
                   </button>';
                   else
                   $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Require Action
                   </button>';
                $html.='<div class="dropdown-menu">
                  <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Require Action</a>
                  <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Action Taken</a>

                </div></div></div>   </div></div>';
                // dd($child_values[$data->id]);


                      return $html;

                  })
             ->addColumn('reaction', function ($data) {
              $reaction_total = (int)$data->cmtLiked + (int)$data->cmtLove + (int)$data->cmtWow + (int)$data->cmtAngry + (int)$data->cmtSad + (int)$data->cmtHaha ;
                      return "<span style='font-weight:bold'>" . number_format($reaction_total)."</span>";
                  }) 
            ->rawColumns(['post_div','reaction'])
         
            ->make(true);

    }
    function getReplyComments(){
      $id = Input::get('id');
      $brand_id = Input::get('brand_id');

      $tags =$this->gettagsBybrandid($brand_id);
      // dd($tags);
      $query="SELECT cmts.cmtLiked,cmts.cmtLove,cmts.cmtWow,cmts.cmtHaha,cmts.cmtSad,cmts.cmtAngry,cmts.cmtType,cmts.cmtLink,cmts.local_picture,pros.id profileID,pros.type as profileType,pros.name as profileName,cmts.checked_tags_id tags,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time,cmts.change_predict,cmts.checked_predict,".
            " cmts.checked_sentiment sentiment,cmts.emotion,cmts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,cmts.action_status,cmts.action_taken ".
            " FROM temp_inbound_comments cmts ".
            " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
            " WHERE cmts.parent='".$id."' ORDER by timestamp(cmts.created_time) DESC ";
      $result =DB::select($query);
       echo json_encode(array($result,$tags));

    }

    function highLight($keywordToHighlight,$message)
    {
       

        $highligh_string  = $message;
      
        $highligh_string = str_ireplace($keywordToHighlight, "<span style='background:#FFEB3B'>".$keywordToHighlight."</span>",$highligh_string);
         

        return $highligh_string;
    }
    public function getIndustryPost()
    {
      $add_searchkw_con = '';
      if(null !==Input::get('kw_search'))
      {
        $kw = Input::get('kw_search');
        $add_searchkw_con = " AND (posts.message Like '%".$kw."%')";
      }
       $tag_con = '';
       if(null !== Input::get('search_tag'))
      {
        // $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
         $tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'posts'); 
      }

      $add_post_con='';
      if(null !== Input::get('tag_id')){
      $add_post_con = " and " . $this->getTagWhereGen(Input::get('tag_id'),'posts'); 
           }

      $brand_id=Input::get('brand_id');
      $keyword_con='';
      if('1' == Input::get('keyword_flag'))
      {
        $keyword_data = $this->getprojectkeywork($brand_id);
       
        if(isset($keyword_data [0]['main_keyword']))
        $keyword_con  =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; // params (data_arr,table alais)
        else
        $keyword_con = ' AND 1=2';
       // dd( $keyword_con);
     }
      $period_type = '';
      $additional_filter=" AND 1=1 ";
      
        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

      
            if($period_type == 'week')
            {
             $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

            }
            else
            {
                $additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
            }

       }
      


        if(null !== Input::get('fday'))
      {
          $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            }
            else
            {
            $dateBegin=date('Y-m-d');
            }
          

          if(null !==Input::get('sday'))
          {

       $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          
          else
          {
      $dateEnd=date('Y-m-d');

                 }

           $filter_pages = ' 1=1 ';
         

          // if(null !== Input::get('filter_page_name'))
          //  {
          //   $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
          //   $filter_pages .= " and page_name in ('".$filter_page_name."') ";
          //  }
           // dd(Input::get('admin_page'));
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
            $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
            $filterRes = DB::table('industry')->where('type',$filter_page_name)->get()->toArray();

               foreach($filterRes as $pgs){
                $industry_pages[] = $pgs->origin_name;
               }
             $industry_str = "'" . implode("','", $industry_pages) . "'";
             $industry_str = preg_replace('/(\r\n|\r|\n)+/', " ",$industry_str);
              // dd($industry_str);
            // foreach($customName as $name) $filter_page_name = $name->origin_name;
            $filter_pages .= " and  posts.page_name in (". $industry_str.") ";
           }
           else  
           {
            $my_pages = $this->getInboundPages($brand_id);
            $filter_pages .= ' and posts.page_name In('.$my_pages.')';
            // $my_pages= $this->getOwnPage($brand_id);
            // $my_pages=explode(',', $my_pages);
            // dd($my_pages);
            // if  ('1' == Input::get('other_page'))
            // $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
            // else
            // $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages);
          // dd($filter_pages);
           }
           $visitor_status= " And visitor = 0";
          if(null !== Input::get('visitor') && 1 == Input::get('visitor'))
           {
            $visitor_status= " And visitor = 1";
           }

            $add_global_filter='';
      if(null !== Input::get('tsearch_senti'))
      {
        $add_global_filter .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
      }
       if(null !== Input::get('tsearch_tag'))
      {
        $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'),'posts');
      }

           
// dd($filter_pages);

      $query='';
      $query_result=[];
      if ($filter_pages !== '')
      {
        $query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,replace(shared, ',', '') shared,posts.id,posts.visitor,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
      " created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,posts.isDeleted,".
      " posts.campaign,posts.checked_tags_id tags,IF(posts.tags_id <> posts.checked_tags_id || posts.checked_tags_id <> '','human predict!','') hp_tags,cmts.checked_sentiment ".
      " cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
      " ELSE 'mdi-star' ".
      " END) isBookMark ".
      "FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
      " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND". $filter_pages  .  $keyword_con . $visitor_status . $add_searchkw_con . $tag_con . $additional_filter . $add_post_con . $add_global_filter .
      " ORDER by timestamp(posts.created_time) DESC ";

// dd($filter_pages);
      $query_result = DB::select($query);


        }
        // dd($query);
              $data = [];
               $data_mention=[];

               $project = Project::find($brand_id);
               $monitor_pages = $project->monitor_pages;
               $monitor_pages = explode(',', $monitor_pages);
               
              $mongo_page = $this->Format_Page_Name($monitor_pages);
        // dd($mongo_page);
              //get page id from mongo
              // dd($mongo_page);
              $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

                return $collection->aggregate([
                    [
                    '$match' =>[
                         '$and'=> [ 
                         ['page_name'=>['$in'=>$mongo_page]],
                                   
                                  ]
                    ] 
                               
                   ],

                   ['$sort' =>['_id'=>1]]
                    
                ]);
              })->toArray();
          // dd(count($InboundPages_result));

          $mongo_page_name=[];
          $mongo_page_id=[];
          $mongo_page_imgurl=[];
          $mongo_page_full_name=[];
          foreach ($InboundPages_result as  $key => $row) {
            $id='';$page_name_new='';$full_page_name='';$imgurl = '';
            if(isset($row['id'])) $id = $row['id'];
            if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
            if(isset($row['page_name'])) $page_name = $row['page_name'];
            if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

            $mongo_page_name[] =[
               'page_name' =>$page_name,
              ];
              $mongo_page_id[] =[
                'id' => $id,
              ];
               $mongo_page_imgurl[] =[
                'imgurl' =>$imgurl,
               ];
            $mongo_page_full_name[] =[
                'full_page_name' =>$full_page_name,
               ];
            

          }
          $mongo_page_name = array_column($mongo_page_name,'page_name');
          $mongo_page_full_name = array_column($mongo_page_full_name,'full_page_name');
          $mongo_page_id = array_column($mongo_page_id,'id');
          $mongo_page_imgurl=array_column($mongo_page_imgurl,'imgurl');

           // retrieve  comment , share and reaction count

          $reaction = "SELECT Liked,Love,Haha,Wow,Angry,Sad,shared , posts.id ".
          "FROM temp_inbound_posts posts ".
          " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND". $filter_pages  .  $keyword_con . $visitor_status . $add_searchkw_con . $tag_con . $additional_filter . $add_post_con .
          " ORDER by timestamp(posts.created_time) DESC ";

          $reaction_result = DB::select($reaction);
          // dd($query_result);

          $reaction_total = 0 ; $like = 0; $love = 0; $wow = 0; $haha = 0; $sad = 0; $angry = 0; $share = 0 ; $post_id = [];
          foreach ($reaction_result as $key => $value) {
            $like +=$value->Liked;
            $love +=$value->Love;
            $wow +=$value->Wow;
            $haha +=$value->Haha;
            $sad +=$value->Sad;
            $angry +=$value->Angry;
            $share +=$value->shared;
            $reaction_total = (int)$like + (int)$love + (int)$wow + (int)$haha + (int)$sad + (int)$angry;
            $post_id[] = $value->id;
          }
          $id_str = implode(',',  $post_id);
          $id_str = "'" . str_replace(",", "','", $id_str) . "'";
          
          $cmt_count = 0;
          $cmtquery = "SELECT count(*) cmt_count  "."FROM temp_inbound_comments "." WHERE post_id IN (".$id_str.")";
          $cmt_query_result = DB::select($cmtquery);
            // dd($cmt_query_result);  
          foreach ($cmt_query_result as $key => $value) {
           $cmt_count =  $value->cmt_count;
          }
          // dd($cmt_count);

          // retrieve  comment , share and reaction count

// dd($query_result);
           foreach ($query_result as  $key => $row) {
            //change page name from id to orginal user register name.
            $page_name=$row->page_name;
            
            // dd($mongo_page_name);
            $page_mongo_id='';$url='';
            
            $key = array_search($page_name, $mongo_page_name);
            if(isset($mongo_page_id[$key]))
            $page_mongo_id=$mongo_page_id[$key];

            if(isset($mongo_page_full_name[$key]))
            $page_name=$mongo_page_full_name[$key];

           if(isset($mongo_page_imgurl[$key]))
            $url=$mongo_page_imgurl[$key];
                    
                    // dd($page_name);
            //dd($key);       


          $pos=($row->cmt_sentiment=='pos' && $row->parent=== '' )?1:0 ;
          $neg=($row->cmt_sentiment=='neg' && $row->parent=== '' )?1:0;
          $cmtneutral=($row->cmt_sentiment=='neutral' && $row->parent=== '' )?1:0;
          $cmtNA=($row->cmt_sentiment=='NA' && $row->parent=== '' )?1:0;
          $cmtneutral=(int)$cmtneutral+(int) $cmtNA;

          $anger =($row->cmt_emotion=='anger' && $row->parent==='' )?1:0 ;
          $interest=($row->cmt_emotion=='interest' && $row->parent==='' )?1:0;
          $disgust=($row->cmt_emotion=='disgust' && $row->parent ==='')?1:0;
          $fear=($row->cmt_emotion=='fear' && $row->parent=== '' )?1:0;
          $joy=($row->cmt_emotion=='joy' && $row->parent==='')?1:0;
          $like=($row->cmt_emotion=='like' && $row->parent==='' )?1:0;
          $love=($row->cmt_emotion=='love' && $row->parent==='' )?1:0;
          $neutral=($row->cmt_emotion=='neutral' && $row->parent==='' )?1:0;
          $sadness=($row->cmt_emotion=='sadness' && $row->parent==='' )?1:0;
          $surprise=($row->cmt_emotion=='surprise' && $row->parent==='' )?1:0;
          $trust=($row->cmt_emotion=='trust' && $row->parent==='')?1:0;
           
             
             $message=$this->readmore($row->message);
            $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');

             $total = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry;
             //$message=$row->message;


            

  if (isset($row ->id)){
    $comment_count=($row->parent==='')?1:0;
    $id= $row ->id;

    if (!array_key_exists($id , $data)) {
              $data[$id] = array(
                          'id'     =>$row->id,
                          'message' => $message,
                          'page_name' =>$page_name,
                          'page_mongo_id' =>$page_mongo_id,
                          'imgurl' =>$url,
                          'created_time' =>$row->created_time,
                          'link' =>$row->link,
                          'full_picture' =>$row->full_picture,
                          'sentiment' =>$row->sentiment,
                          'emotion' =>$row->emotion,
                          'positive'=> $pos,'negative'=>$neg,'cmtneutral'=>$cmtneutral,
                          'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,'shared'=>$row->shared,
                          'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                          'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                          'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                          'total'=>$total,
                          'comment_count'=>$comment_count,
                          'isDeleted'=>$row->isDeleted ,
                          'isBookMark'=>$row->isBookMark,
                          'campaign'=>$row->campaign,
                          'hp_tags' =>$row->hp_tags,
                          'tags'=>$row->tags,
                          'visitor'=>$row->visitor,
                          'reaction_total'=>$reaction_total,
                          'share_total' =>$share,
                          'comment_total' => $cmt_count
                      );
          }
      else{
            
           $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
           $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
           $data[$id]['cmtneutral'] = (int) $data[$id]['cmtneutral'] +$cmtneutral;
           $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
           $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
           $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
           $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
           $data[$id]['like'] = (int) $data[$id]['like'] + $like;
           $data[$id]['love'] = (int) $data[$id]['love'] + $love;
           $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
           $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
           $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
           $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;
           $data[$id]['comment_count'] = (int) $data[$id]['comment_count'] + $comment_count;
          }
          }
          // dd($data);
        }
                      
// dd($data); 

                       if(null !== Input::get('filter_overall'))
                      {
                        if( Input::get('filter_overall') === 'pos')//
                        {   
                          foreach ($data as $key => $value) {

                  if( ((int)$value['positive'] <  (int)$value['negative']) || ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0) )
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                          }
                        else if( Input::get('filter_overall') === 'neg')
                        {
                     foreach ($data as $key => $value) {

                      if((int)$value['positive'] >  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'] &&  (int)$value['cmtneutral'] <> 0)
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                        }
                         else if( Input::get('filter_overall') === 'neutral')
                        {
                                    foreach ($data as $key => $value) {

                                 if( ! ( ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 ) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0)))
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                        }

                      }



                      $data_mention=$data;
                      // dd($data_mention);  
                        $tags =$this->gettagsBybrandid($brand_id);



           
                      return Datatables::of($data_mention)
               
                 ->addColumn('post', function ($data_mention) use($tags){
                  $fb_p_id=explode('_', $data_mention['id']);
                  $fb_p_id=$fb_p_id[1];
                  $page_id='';
                  $arr_page_id=explode("-",$data_mention['page_name']);
                  if(count($arr_page_id)>0)
                  {
                    $last_index=$arr_page_id[count($arr_page_id)-1];
                    if(is_numeric($last_index))
                    $page_id = $last_index;
                    else
                    $page_id = $data_mention['page_name'];
                  }
                  else
                  {
                    $page_id=$data_mention['page_name'];
                  }

                $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
                $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
              //   if($data_mention['page_mongo_id'] <> '')
              //   {  $page_photo='https://graph.facebook.com/'.$data_mention['page_mongo_id'].'/picture?type=large';}
              // else
              //   { $page_photo='assets/images/unknown_photo.jpg'; }
                  if($data_mention['imgurl'] <> '')
                {  $page_photo=$data_mention['imgurl'];}
              else
                { $page_photo='assets/images/unknown_photo.jpg'; }
            
              
              

                        $total = (int) $data_mention['positive'] +(int) $data_mention['negative']+(int) $data_mention['cmtneutral'];
                        $pos_pcent='';$neg_pcent='';$neutral_pcent='';
                       
                         $html= '<div class="profiletimeline"><div class="sl-item">
                                              <div class="sl-left"> <img src="'.$page_photo.'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                              <div class="sl-right"> <div><div><a href="'.$page_Link.'" class="link"  target="_blank">'.$data_mention['page_name'].' </a>';
                        // $html.='  <label class="text-info"   for="campaign" style=""><i class="mdi mdi-flag-outline"></i></label> 
                        //            ';
                        if((int)$data_mention['campaign'] == 0)
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="false">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                        else
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="true">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                    //                      if($data->hp <> '' && $data->hp_tags <>'')
                    // $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    // else if($data->hp <> '')
                    // $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';

                     if($data_mention['hp_tags'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                        // if((int)$data_mention['isDeleted']==1)
                        // {
                        //  $html.='  <span  class="text-red">   This post is no longer available on FB. </span>';
                        // }
                        $html.='<div class="sl-right">
                        <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data_mention['created_time'].'</span> </div>
                        <div style="width:55%;display: inline-block">';
                         if((int) $data_mention['negative'] > 0)
                       { 
                        $neg_pcent=round((int) $data_mention['negative']/$total*100,2);
                        $html.='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'.$neg_pcent.'%';
                      }
                      if((int) $data_mention['positive'] > 0)
                        {
                          $pos_pcent=round((int) $data_mention['positive']/$total*100,2);
                          $html.='<span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'.$pos_pcent.'%';
                        }
                      if((int) $data_mention['cmtneutral'] > 0)
                       {
                        $neutral_pcent=round((int) $data_mention['cmtneutral']/$total*100,2);
                       $html.='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'.$neutral_pcent.'%';
                       }
                                 
                          $html.='</div>
                        </div>';
                        if($data_mention['visitor'] <> 1){
                         $html.=' <p> Tag :<select id="tags_'.$data_mention['id'].'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                        
                       $tag_array=explode(',', $data_mention['tags']);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';
                    }
                        $html.='<p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['id'].'">View More</a></p>';
                                  
                        $html.= '</div></div>
                                   
                        <div class="sl-right">';
                        if((int)$data_mention['liked'] > 0)
                        $html.='👍 '.$this->thousandsCurrencyFormat($data_mention['liked']);
                       if((int)$data_mention['loved'] > 0)
                        $html.=' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']);
                       if((int)$data_mention['haha'] > 0)
                        $html.=' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']);
                       if((int)$data_mention['wow'] > 0)
                        $html.=' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']);
                       if((int)$data_mention['sad'] > 0)
                        $html.=' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']);
                       if((int)$data_mention['angry'] > 0)
                        $html.=' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']);
                        $html.='</div><div class="sl-right" align="right"> <a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green see_comment"  id="seeComment_'.$data_mention['id'].'" >See Comment</a> <a href="'.$post_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See Post on FB</a></div></div></div>';

                          return $html;
                         

                      })
            ->addColumn('reaction', function ($data_mention) {
                          return  $data_mention['total'];
                      }) 

            ->addColumn('comment', function ($data_mention) {
                           return "<span style='font-weight:bold'>" . $data_mention['comment_count']."</span>";
                      }) 
            ->addColumn('share', function ($data_mention) {
                          return  "<span style='font-weight:bold'>" . $data_mention['shared']."</span>";
                      }) 
            // ->addColumn('overall', function ($data_mention) {
            //                if((int)$data_mention['positive'] >  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-success" style="font-weight:500">Positive</span>';
            //             }
            //             else   if((int)$data_mention['positive'] <  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-red" style="font-weight:500">Negative</span>';
            //             }
            //                else   if((int)$data_mention['positive'] ==  0 && (int)$data_mention['negative'] == 0 && (int)$data_mention['cmtneutral']  == 0 )
            //             {
            //               $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==0 &&  (int)$data_mention['negative'] ==0 &&  (int)$data_mention['cmtneutral'] <> 0)
            //             {
            //               $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'] &&  (int)$data_mention['cmtneutral'] <> 0)
            //             {
            //               $html='<span class="text-success" style="font-weight:500">Positive</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-red" style="font-weight:500">Negative</span>';
            //             }
                     
            //             return $html;
            //           }) 

                  
                // ->rawColumns(['post','overall','reaction','comment','share'])
                ->rawColumns(['post','overall','reaction','comment','share'])
             
                ->make(true);


    }

    
    public function getInboundPost()
    {
      $add_searchkw_con = '';
      if(null !==Input::get('kw_search'))
      {
        $kw = Input::get('kw_search');
        $add_searchkw_con = " AND (posts.message Like '%".$kw."%')";
      }
       $tag_con = '';
       if(null !== Input::get('search_tag'))
      {
        // $tag_con = " and posts.checked_tags_id Like '%".Input::get('search_tag')."%'";
         $tag_con = " and " . $this->getTagWhereGen(Input::get('search_tag'),'posts'); 
      }

      $add_post_con='';
      if(null !== Input::get('tag_id')){
      $add_post_con = " and " . $this->getTagWhereGen(Input::get('tag_id'),'posts'); 
           }

      $brand_id=Input::get('brand_id');
      $keyword_con='';
      if('1' == Input::get('keyword_flag'))
      {
        $keyword_data = $this->getprojectkeywork($brand_id);
       
        if(isset($keyword_data [0]['main_keyword']))
        $keyword_con  =  " AND (" . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; // params (data_arr,table alais)
        else
        $keyword_con = ' AND 1=2';
       // dd( $keyword_con);
     }
      $period_type = '';
      $additional_filter=" AND 1=1 ";
      
        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

      
            if($period_type == 'week')
            {
             $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

            }
            else
            {
                $additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
            }

       }
      


        if(null !== Input::get('fday'))
      {
          $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            }
            else
            {
            $dateBegin=date('Y-m-d');
            }
          

          if(null !==Input::get('sday'))
          {

       $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          
          else
          {
      $dateEnd=date('Y-m-d');

                 }

           $filter_pages = ' 1=1 ';
         

          // if(null !== Input::get('filter_page_name'))
          //  {
          //   $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
          //   $filter_pages .= " and page_name in ('".$filter_page_name."') ";
          //  }
           // dd(Input::get('admin_page'));
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
            $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
            $customName = DB::table('page_rename')->where('custom_name',$filter_page_name)->get();
            foreach($customName as $name) $filter_page_name = $name->origin_name;
            $filter_pages .= " and  posts.page_name in ('". $filter_page_name."') ";
           }
           else  
           {
            $my_pages = $this->getInboundPages($brand_id);
            $filter_pages .= ' and posts.page_name In('.$my_pages.')';
            // $my_pages= $this->getOwnPage($brand_id);
            // $my_pages=explode(',', $my_pages);
            // dd($my_pages);
            // if  ('1' == Input::get('other_page'))
            // $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
            // else
            // $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages);
          // dd($filter_pages);
           }
           $visitor_status= " And visitor = 0";
          if(null !== Input::get('visitor') && 1 == Input::get('visitor'))
           {
            $visitor_status= " And visitor = 1";
           }

            $add_global_filter='';
      if(null !== Input::get('tsearch_senti'))
      {
        $add_global_filter .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
      }
       if(null !== Input::get('tsearch_tag'))
      {
        $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'),'posts');
      }

           
// dd($filter_pages);

      $query='';
      $query_result=[];
      if ($filter_pages !== '')
      {
        $query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,replace(shared, ',', '') shared,posts.id,posts.visitor,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
      " created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,posts.isDeleted,".
      " posts.campaign,posts.checked_tags_id tags,IF(posts.tags_id <> posts.checked_tags_id || posts.checked_tags_id <> '','human predict!','') hp_tags,cmts.checked_sentiment ".
      " cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
      " ELSE 'mdi-star' ".
      " END) isBookMark ".
      "FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
      " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND". $filter_pages  .  $keyword_con . $visitor_status . $add_searchkw_con . $tag_con . $additional_filter . $add_post_con . $add_global_filter .
      " ORDER by timestamp(posts.created_time) DESC ";

      $query_result = DB::select($query);
      // dd($query_result);

        }
        // dd(count($query_result));
              $data = [];
               $data_mention=[];

               $project = Project::find($brand_id);
               $monitor_pages = $project->monitor_pages;
               $monitor_pages = explode(',', $monitor_pages);
               
              $mongo_page = $this->Format_Page_Name($monitor_pages);
        // dd($mongo_page);
              //get page id from mongo
              // dd($mongo_page);
              $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

                return $collection->aggregate([
                    [
                    '$match' =>[
                         '$and'=> [ 
                         ['page_name'=>['$in'=>$mongo_page]],
                                   
                                  ]
                    ] 
                               
                   ],

                   ['$sort' =>['_id'=>1]]
                    
                ]);
              })->toArray();
          // dd(count($InboundPages_result));

          $mongo_page_name=[];
          $mongo_page_id=[];
          $mongo_page_imgurl=[];
          $mongo_page_full_name=[];
          foreach ($InboundPages_result as  $key => $row) {
            $id='';$page_name_new='';$full_page_name='';$imgurl = '';
            if(isset($row['id'])) $id = $row['id'];
            if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
            if(isset($row['page_name'])) $page_name = $row['page_name'];
            if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

            $mongo_page_name[] =[
               'page_name' =>$page_name,
              ];
              $mongo_page_id[] =[
                'id' => $id,
              ];
               $mongo_page_imgurl[] =[
                'imgurl' =>$imgurl,
               ];
            $mongo_page_full_name[] =[
                'full_page_name' =>$full_page_name,
               ];
            

          }
          $mongo_page_name = array_column($mongo_page_name,'page_name');
          $mongo_page_full_name = array_column($mongo_page_full_name,'full_page_name');
          $mongo_page_id = array_column($mongo_page_id,'id');
          $mongo_page_imgurl=array_column($mongo_page_imgurl,'imgurl');

           // retrieve  comment , share and reaction count

          $reaction = "SELECT Liked,Love,Haha,Wow,Angry,Sad,shared , posts.id ".
          "FROM temp_inbound_posts posts ".
          " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND". $filter_pages  .  $keyword_con . $visitor_status . $add_searchkw_con . $tag_con . $additional_filter . $add_post_con .
          " ORDER by timestamp(posts.created_time) DESC ";

          $reaction_result = DB::select($reaction);
          // dd($query_result);

          $reaction_total = 0 ; $like = 0; $love = 0; $wow = 0; $haha = 0; $sad = 0; $angry = 0; $share = 0 ; $post_id = [];
          foreach ($reaction_result as $key => $value) {
            $like +=$value->Liked;
            $love +=$value->Love;
            $wow +=$value->Wow;
            $haha +=$value->Haha;
            $sad +=$value->Sad;
            $angry +=$value->Angry;
            $share +=$value->shared;
            $reaction_total = (int)$like + (int)$love + (int)$wow + (int)$haha + (int)$sad + (int)$angry;
            $post_id[] = $value->id;
          }
          $id_str = implode(',',  $post_id);
          $id_str = "'" . str_replace(",", "','", $id_str) . "'";
          
          $cmt_count = 0;
          $cmtquery = "SELECT count(*) cmt_count  "."FROM temp_inbound_comments "." WHERE post_id IN (".$id_str.")";
          $cmt_query_result = DB::select($cmtquery);
            // dd($cmt_query_result);  
          foreach ($cmt_query_result as $key => $value) {
           $cmt_count =  $value->cmt_count;
          }
          // dd($cmt_count);

          // retrieve  comment , share and reaction count

// dd($query_result);
           foreach ($query_result as  $key => $row) {
            //change page name from id to orginal user register name.
            $page_name=$row->page_name;
            
            // dd($mongo_page_name);
            $page_mongo_id='';$url='';
            
            $key = array_search($page_name, $mongo_page_name);
            if(isset($mongo_page_id[$key]))
            $page_mongo_id=$mongo_page_id[$key];

            if(isset($mongo_page_full_name[$key]))
            $page_name=$mongo_page_full_name[$key];

           if(isset($mongo_page_imgurl[$key]))
            $url=$mongo_page_imgurl[$key];
                    
                    // dd($page_name);
            //dd($key);       


          $pos=($row->cmt_sentiment=='pos' && $row->parent=== '' )?1:0 ;
          $neg=($row->cmt_sentiment=='neg' && $row->parent=== '' )?1:0;
          $cmtneutral=($row->cmt_sentiment=='neutral' && $row->parent=== '' )?1:0;
          $cmtNA=($row->cmt_sentiment=='NA' && $row->parent=== '' )?1:0;
          $cmtneutral=(int)$cmtneutral+(int) $cmtNA;

          $anger =($row->cmt_emotion=='anger' && $row->parent==='' )?1:0 ;
          $interest=($row->cmt_emotion=='interest' && $row->parent==='' )?1:0;
          $disgust=($row->cmt_emotion=='disgust' && $row->parent ==='')?1:0;
          $fear=($row->cmt_emotion=='fear' && $row->parent=== '' )?1:0;
          $joy=($row->cmt_emotion=='joy' && $row->parent==='')?1:0;
          $like=($row->cmt_emotion=='like' && $row->parent==='' )?1:0;
          $love=($row->cmt_emotion=='love' && $row->parent==='' )?1:0;
          $neutral=($row->cmt_emotion=='neutral' && $row->parent==='' )?1:0;
          $sadness=($row->cmt_emotion=='sadness' && $row->parent==='' )?1:0;
          $surprise=($row->cmt_emotion=='surprise' && $row->parent==='' )?1:0;
          $trust=($row->cmt_emotion=='trust' && $row->parent==='')?1:0;
           
             

             $message=$this->readmore($row->message);
            $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');

             $total = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry;
             //$message=$row->message;


            

  if (isset($row ->id)){
    $comment_count=($row->parent==='')?1:0;
    $id= $row ->id;

    if (!array_key_exists($id , $data)) {
              $data[$id] = array(
                          'id'     =>$row->id,
                          'message' => $message,
                          'page_name' =>$page_name,
                          'page_mongo_id' =>$page_mongo_id,
                          'imgurl' =>$url,
                          'created_time' =>$row->created_time,
                          'link' =>$row->link,
                          'full_picture' =>$row->full_picture,
                          'sentiment' =>$row->sentiment,
                          'emotion' =>$row->emotion,
                          'positive'=> $pos,'negative'=>$neg,'cmtneutral'=>$cmtneutral,
                          'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,'shared'=>$row->shared,
                          'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                          'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                          'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                          'total'=>$total,
                          'comment_count'=>$comment_count,
                          'isDeleted'=>$row->isDeleted ,
                          'isBookMark'=>$row->isBookMark,
                          'campaign'=>$row->campaign,
                          'hp_tags' =>$row->hp_tags,
                          'tags'=>$row->tags,
                          'visitor'=>$row->visitor,
                          'reaction_total'=>$reaction_total,
                          'share_total' =>$share,
                          'comment_total' => $cmt_count
                      );
          }
      else{
            
           $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
           $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
           $data[$id]['cmtneutral'] = (int) $data[$id]['cmtneutral'] +$cmtneutral;
           $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
           $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
           $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
           $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
           $data[$id]['like'] = (int) $data[$id]['like'] + $like;
           $data[$id]['love'] = (int) $data[$id]['love'] + $love;
           $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
           $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
           $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
           $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;
           $data[$id]['comment_count'] = (int) $data[$id]['comment_count'] + $comment_count;
          }
          }
          // dd($data);
        }
                      
// dd($data); 

                       if(null !== Input::get('filter_overall'))
                      {
                        if( Input::get('filter_overall') === 'pos')//
                        {   
                          foreach ($data as $key => $value) {

                  if( ((int)$value['positive'] <  (int)$value['negative']) || ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0) )
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                          }
                        else if( Input::get('filter_overall') === 'neg')
                        {
                     foreach ($data as $key => $value) {

                      if((int)$value['positive'] >  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'] &&  (int)$value['cmtneutral'] <> 0)
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                        }
                         else if( Input::get('filter_overall') === 'neutral')
                        {
                                    foreach ($data as $key => $value) {

                                 if( ! ( ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 ) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0)))
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                        }

                      }



                      $data_mention=$data;
                      // dd($data_mention);  
                        $tags =$this->gettagsBybrandid($brand_id);



           
                      return Datatables::of($data_mention)
               
                 ->addColumn('post', function ($data_mention) use($tags){
                  $fb_p_id=explode('_', $data_mention['id']);
                  $fb_p_id=$fb_p_id[1];
                  $page_id='';
                  $arr_page_id=explode("-",$data_mention['page_name']);
                  if(count($arr_page_id)>0)
                  {
                    $last_index=$arr_page_id[count($arr_page_id)-1];
                    if(is_numeric($last_index))
                    $page_id = $last_index;
                    else
                    $page_id = $data_mention['page_name'];
                  }
                  else
                  {
                    $page_id=$data_mention['page_name'];
                  }

                $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
                $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
              //   if($data_mention['page_mongo_id'] <> '')
              //   {  $page_photo='https://graph.facebook.com/'.$data_mention['page_mongo_id'].'/picture?type=large';}
              // else
              //   { $page_photo='assets/images/unknown_photo.jpg'; }
                  if($data_mention['imgurl'] <> '')
                {  $page_photo=$data_mention['imgurl'];}
              else
                { $page_photo='assets/images/unknown_photo.jpg'; }
            
              
              

                        $total = (int) $data_mention['positive'] +(int) $data_mention['negative']+(int) $data_mention['cmtneutral'];
                        $pos_pcent='';$neg_pcent='';$neutral_pcent='';
                       
                         $html= '<div class="profiletimeline"><div class="sl-item">
                                              <div class="sl-left"> <img src="'.$page_photo.'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                              <div class="sl-right"> <div><div><a href="'.$page_Link.'" class="link"  target="_blank">'.$data_mention['page_name'].' </a>';
                        // $html.='  <label class="text-info"   for="campaign" style=""><i class="mdi mdi-flag-outline"></i></label> 
                        //            ';
                        if((int)$data_mention['campaign'] == 0)
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="false">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                        else
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="true">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                    //                      if($data->hp <> '' && $data->hp_tags <>'')
                    // $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    // else if($data->hp <> '')
                    // $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';

                     if($data_mention['hp_tags'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                        // if((int)$data_mention['isDeleted']==1)
                        // {
                        //  $html.='  <span  class="text-red">   This post is no longer available on FB. </span>';
                        // }
                        $html.='<div class="sl-right">
                        <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data_mention['created_time'].'</span> </div>
                        <div style="width:55%;display: inline-block">';
                         if((int) $data_mention['negative'] > 0)
                       { 
                        $neg_pcent=round((int) $data_mention['negative']/$total*100,2);
                        $html.='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'.$neg_pcent.'%';
                      }
                      if((int) $data_mention['positive'] > 0)
                        {
                          $pos_pcent=round((int) $data_mention['positive']/$total*100,2);
                          $html.='<span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'.$pos_pcent.'%';
                        }
                      if((int) $data_mention['cmtneutral'] > 0)
                       {
                        $neutral_pcent=round((int) $data_mention['cmtneutral']/$total*100,2);
                       $html.='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'.$neutral_pcent.'%';
                       }
                                 
                          $html.='</div>
                        </div>';
                        if($data_mention['visitor'] <> 1){
                         $html.=' <p> Tag :<select id="tags_'.$data_mention['id'].'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                        
                       $tag_array=explode(',', $data_mention['tags']);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';
                    }
                        $html.='<p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['id'].'">View More</a></p>';
                                  
                        $html.= '</div></div>
                                   
                        <div class="sl-right">';
                        if((int)$data_mention['liked'] > 0)
                        $html.='👍 '.$this->thousandsCurrencyFormat($data_mention['liked']);
                       if((int)$data_mention['loved'] > 0)
                        $html.=' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']);
                       if((int)$data_mention['haha'] > 0)
                        $html.=' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']);
                       if((int)$data_mention['wow'] > 0)
                        $html.=' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']);
                       if((int)$data_mention['sad'] > 0)
                        $html.=' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']);
                       if((int)$data_mention['angry'] > 0)
                        $html.=' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']);
                        $html.='</div><div class="sl-right" align="right"> <a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green see_comment"  id="seeComment_'.$data_mention['id'].'" >See Comment</a> <a href="'.$post_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See Post on FB</a></div></div></div>';

                          return $html;
                         

                      })
            ->addColumn('reaction', function ($data_mention) {
                          return  $data_mention['total'];
                      }) 

            ->addColumn('comment', function ($data_mention) {
                           return "<span style='font-weight:bold'>" . $data_mention['comment_count']."</span>";
                      }) 
            ->addColumn('share', function ($data_mention) {
                          return  "<span style='font-weight:bold'>" . $data_mention['shared']."</span>";
                      }) 
            // ->addColumn('overall', function ($data_mention) {
            //                if((int)$data_mention['positive'] >  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-success" style="font-weight:500">Positive</span>';
            //             }
            //             else   if((int)$data_mention['positive'] <  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-red" style="font-weight:500">Negative</span>';
            //             }
            //                else   if((int)$data_mention['positive'] ==  0 && (int)$data_mention['negative'] == 0 && (int)$data_mention['cmtneutral']  == 0 )
            //             {
            //               $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==0 &&  (int)$data_mention['negative'] ==0 &&  (int)$data_mention['cmtneutral'] <> 0)
            //             {
            //               $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'] &&  (int)$data_mention['cmtneutral'] <> 0)
            //             {
            //               $html='<span class="text-success" style="font-weight:500">Positive</span>';
            //             }
            //             else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'])
            //             {
            //               $html='<span class="text-red" style="font-weight:500">Negative</span>';
            //             }
                     
            //             return $html;
            //           }) 

                  
                // ->rawColumns(['post','overall','reaction','comment','share'])
                ->rawColumns(['post','overall','reaction','comment','share'])
             
                ->make(true);


    }
    public function getInboundcomments()
    {
       
            $post_id=Input::get('id');
            $brand_id=Input::get('brand_id');
            $cmt_type=Input::get('cmt_type');
            $s_cmt_type='';

          

            if($cmt_type == 'Positive')
            {
             $s_cmt_type ='pos';
            }
            if($cmt_type =="Negative")
            {
              $s_cmt_type='neg';
            }
            $permission_data = $this->getPermission();
           $edit_permission=$permission_data['edit'];


                  $query="SELECT id,".$edit_permission." as edit_permission,message,checked_sentiment sentiment,emotion,DATE_FORMAT(created_time, '%d-%m-%Y %h:%i:%s %p') created_time,IF(sentiment = checked_sentiment,'','human predict!') hp ".
                  " from temp_inbound_comments where post_id='".$post_id."' and parent='' and  (checked_sentiment='".$s_cmt_type."' or emotion='".$cmt_type."') ORDER BY DATE(created_time) DESC";

                  // echo $query;
                  // return;
                  $data = DB::select($query);
                
                  echo json_encode($data);
    }
     public function getRelatedPosts()
    {
        
        $post_id=Input::get('id');
        $brand_id=Input::get('brand_id');

         $permission_data = $this->getPermission();
         $edit_permission=$permission_data['edit'];

        $query="SELECT  posts.id,1 as edit_permission,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
        " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.type) post_type,ANY_VALUE(posts.full_picture) full_picture,ANY_VALUE(posts.local_picture) local_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion, sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA,".
        " ANY_VALUE(posts.isDeleted) isDeleted from temp_inbound_posts posts  LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id".
        "  where posts.id='".$post_id."'  and cmts.parent='' GROUP BY id";

                $data = DB::select($query);
        //if no data above query we need to just show post data without comment data , in order to get post data remove cmts.parent''
                if (count($data) <=0 )
                {
                  $query="SELECT  posts.id,1 as edit_permission,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
        " created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.type) post_type,ANY_VALUE(posts.full_picture) full_picture,ANY_VALUE(posts.local_picture) local_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion, sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA, ".
        " ANY_VALUE(posts.isDeleted) isDeleted from temp_inbound_posts posts  LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id".
        "  where posts.id='".$post_id."'  GROUP BY id";
        $data = DB::select($query);
        // dd($data);
                }
                 $project = Project::find($brand_id);
                 $monitor_pages = $project->monitor_pages;
                 $monitor_pages = explode(',', $monitor_pages);
                 $mongo_page = $this->Format_Page_Name($monitor_pages);

        //get page id from mongo
        $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

          return $collection->aggregate([
              [
              '$match' =>[
                   '$and'=> [ 
                   ['page_name'=>['$in'=>$mongo_page]],
                  
                            ]
              ]  
                         
             ],
             

             ['$sort' =>['_id'=>1]]
              
          ]);
        })->toArray();
        // dd($InboundPages_result);
        $mongo_page_name=[];
        $mongo_page_id=[];
        $mongo_page_full_name=[];
        foreach ($InboundPages_result as  $key => $row) {
          $id='';$page_name_new='';$full_page_name='';$imgurl = '';
          if(isset($row['id'])) $id = $row['id'];
          if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
          if(isset($row['page_name'])) $page_name = $row['page_name'];
          if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

          $mongo_page_name[] =[
             'page_name' =>$page_name,
            ];
            $mongo_page_id[] =[
              'id' => $id,
            ];
             $mongo_page_imgurl[] =[
              'imgurl' =>$imgurl,
             ];
          $mongo_page_full_name[] =[
              'full_page_name' =>$full_page_name,
             ];
          

        }
        $mongo_page_name = array_column($mongo_page_name,'page_name');
        $mongo_page_full_name = array_column($mongo_page_full_name,'full_page_name');
        $mongo_page_id = array_column($mongo_page_id,'id');
        $mongo_page_imgurl = array_column($mongo_page_imgurl,'imgurl');

       /* echo count($data);*/
          $keyword_campaign='';
         if(null !== Input::get('campaign_name'))
          {
            $keyword_campaign= $this->getCampaignKeywordFilter(Input::get('campaign_name'),$brand_id);
          }

        echo json_encode(array($data,$mongo_page_name,$mongo_page_full_name,$mongo_page_id,$mongo_page_imgurl,$keyword_campaign));
    }

    public function getsentimentbypage()
    {
  
      $brand_id=Input::get('brand_id');
           $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 
      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

      /*$brand_id=2;*/

         if(null !== Input::get('fday'))
              {
                 $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
                 $dateBegin=date('Y-m-d');
              }
            

            if(null !==Input::get('sday'))
            {

                  $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
            
            else
            {
                  $dateEnd=date('Y-m-d');

            }
          /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
          $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));
          */
           $add_comment_con="";
          if($filter_keyword !== '')
          {
           $add_comment_con = " and (posts.message Like '%".$filter_keyword."%'";
           $add_comment_con .= " or cmt.message Like '%".$filter_keyword."%')";
          }

          if($filter_sentiment !== '')
          {
            $add_comment_con .= " and cmt.checked_sentiment  = '".$filter_sentiment."'";
          }
                    if(null !== Input::get('admin_page'))
                     {
                      $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
                      $filter_pages= " posts.page_name in ('".$filter_page_name."') ";
                     }
                     else
                     {
                        $my_pages= $this->getOwnPage($brand_id);
                        $my_pages=explode(',', $my_pages);
                        $filter_pages=$this->getPageWhereGen($my_pages);
                     }

          $query = "select posts.page_name page_name, sum(IF(cmt.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmt.checked_sentiment = 'neg', 1, 0)) negative , ".
          " sum(IF(cmt.checked_sentiment = 'pos', 1, 0)) + sum(IF(cmt.checked_sentiment = 'neg', 1, 0)) total ".
          " from temp_inbound_comments cmt left join  temp_inbound_posts posts on cmt.post_id=posts.id inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name  ".
          " WHERE posts.id IS NOT NULL AND cmt.parent='' and (DATE(cmt.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ".$filter_pages.$add_comment_con.
          " GROUP BY page_name ".
          " ORDER BY  total DESC ";


          $query_result = DB::select($query);

                echo json_encode($query_result);

    }
 public function Updatepredict()
 {
          
    $post_id = Input::post('id');
    $brand_id = Input::post('brand_id');
    $sentiment = Input::post('sentiment');
    $emotion = Input::post('emotion');
    $username = auth()->user()->username;
    $tags ='';
    $tags_id ='';
    if(null !== Input::post('type') && 'mention'== Input::post('type'))
    $type='mention_comment'; 
    else
    $type='inbound_comment'; 

    if(null !== Input::post('type') && 'mention_post'== Input::post('type'))
    $type='mention_post'; 

    if(null !== Input::post('type') && 'article'== Input::post('type'))
    $type='web_mention'; 

    if(null !== Input::post('type') && 'group_post'== Input::post('type'))
    $type='group_post'; 

    if(null !== Input::post('type') && 'inbound_post'== Input::post('type'))
    $type='inbound_post'; 
    $manual_date = new \DateTime("now", new \DateTimeZone('Asia/Rangoon') );
    $manual_date = $manual_date->format('Y-m-d H:i:s');

    $row =DB::table('predict_logs')
                ->where('id', $post_id) 
                ->where('type', $type)
                ->limit(1)
                ->get()->toArray();
    if(!empty($row))
    {
      DB::table('predict_logs')->where('id', $post_id)->where('type',$type)->delete();
    }
    if(null !== Input::post('type') && 'mention'== Input::post('type') )
    {
       $result =DB::table('temp_comments')
     
        ->where('id', $post_id)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1,'manual_date'=>$manual_date,'manual_by'=>$username));  // update the record in the DB. 
      
        DB::table('predict_logs')
        ->insert(array('id' => $post_id,'type' => $type,'checked_sentiment' => $sentiment,'change_predict'=>1,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
    }
    elseif(null !== Input::post('type') && 'article'== Input::post('type') )
    {
      if(null !== Input::post('tags'))
      {
        $tags=Input::post('tags');//implode(",",Input::post('tags'));
        $tags_id=implode(",",Input::post('checked_tags_id'));//implode(",",Input::post('checked_tags_id'));
      }
              
       $result =DB::table('temp_web_mentions')
     
      ->where('id', $post_id)  // find your user by their email
      ->limit(1)  // optional - to ensure only one record is updated.
      ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username));  // update the record in the DB. 
      
        DB::table('predict_logs')
        ->insert(array('id' => $post_id,'type' => $type,'checked_sentiment' => $sentiment,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
    }
    elseif(null !== Input::post('type') && 'mention_post'== Input::post('type') )
    {
        if(null !== Input::post('tags'))
        {
          $tags=Input::post('tags');//implode(",",Input::post('tags'));
          $tags_id=implode(",",Input::post('checked_tags_id'));//implode(",",Input::post('checked_tags_id'));
        }
       // dd($post_id);
       $result =DB::table('temp_posts')
     
        ->where('id', $post_id)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username));  // update the record in the DB. 
      
        DB::table('predict_logs')
        ->insert(array('id' => $post_id,'type' => $type,'checked_sentiment' => $sentiment,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
    }
    elseif(null !== Input::post('type') && 'inbound_post'== Input::post('type') )
    {
        if(null !== Input::post('tags'))
        {
          $tags=Input::post('tags');//implode(",",Input::post('tags'));
          $tags_id=implode(",",Input::post('checked_tags_id'));//implode(",",Input::post('checked_tags_id'));
        }
       // dd($post_id);
       $result =DB::table('temp_inbound_posts')
     
        ->where('id', $post_id)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username));  // update the record in the DB. 
        
          DB::table('predict_logs')
          ->insert(array('id' => $post_id,'type' => $type,'checked_sentiment' => $sentiment,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
    }
    elseif(null !== Input::post('type') && 'group_post'== Input::post('type') )
    {

        if(null !== Input::post('tags'))
        {
          $tags=Input::post('tags');//implode(",",Input::post('tags'));
          $tags_id=implode(",",Input::post('checked_tags_id'));//implode(",",Input::post('checked_tags_id'));
          

        }
        if($tags_id == '') $tags_id = NULL;
        if($sentiment == '') $sentiment = NULL;
       // dd($post_id);
       $result =DB::table('temp_group_posts')
     
        ->where('temp_id', $post_id)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username));  // update the record in the DB. 
        
          DB::table('predict_logs')
          ->insert(array('id' => $post_id,'type' => $type,'checked_sentiment' => $sentiment,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
    }
    elseif(null !== Input::post('type') && 'group_comment'== Input::post('type') )
    {

        if(null !== Input::post('tags'))
        {
          $tags=Input::post('tags');//implode(",",Input::post('tags'));
          $tags_id=implode(",",Input::post('checked_tags_id'));//implode(",",Input::post('checked_tags_id'));
          

        }
        if($tags_id == '') $tags_id = NULL;
        if($sentiment == '') $sentiment = NULL;
       // dd($post_id);
       $result =DB::table('temp_group_comments')
     
        ->where('temp_id', $post_id)  // find your user by their email
        ->limit(1)  // optional - to ensure only one record is updated.
        ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username));  // update the record in the DB. 
        
          DB::table('predict_logs')
          ->insert(array('id' => $post_id,'type' => $type,'checked_sentiment' => $sentiment,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
    }

    else
    {
      if(null !== Input::post('tags'))
      {
          $tags=Input::post('tags');//implode(",",Input::post('tags'));
          $tags_id=implode(",",Input::post('checked_tags_id'));//implode(",",Input::post('checked_tags_id'));
      }
          
      $result =DB::table('temp_inbound_comments')
            ->where('id', $post_id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username));  // update the record in the DB. 

          DB::table('predict_logs')
           ->insert(array('id' => $post_id,'type' => $type,'checked_sentiment' => $sentiment,'change_predict'=>1,'checked_tags'=>$tags,'checked_tags_id'=>$tags_id,'manual_date'=>$manual_date,'manual_by'=>$username,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
    }      
        return $result;

  }
     public function SetCampaign()
    {
          
          $id = Input::post('id');
          $brand_id = Input::post('brand_id');
          if(!null==Input::post('campaign_val'))
            $campaign_val = Input::post('campaign_val');
          else
             $campaign_val =NULL;
           
             $result =DB::table('temp_inbound_posts')
            ->where('id', $id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('campaign' =>$campaign_val));  // update the record in the DB. 
            $row = DB::table('predict_logs') //check row exist
                ->where('id', $id) 
                ->where('type','inbound_post')
                ->limit(1)
                ->get()->toArray();
              
              if(!empty($row))
              {
                  DB::table('predict_logs')->where('id',$id)->where('type','inbound_post')
                    ->update(array('campaign' =>$campaign_val));
              }
              else
              {
                // dd("data not exist");
                  DB::table('predict_logs')
                    ->insert(array('id' => $id,'type' => 'inbound_post','campaign' =>$campaign_val,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
              }

         
            return $result;

        }


        public function SetCampaign_mention()
        {
              //dd('dddddd');
              $id = Input::post('id');
              $brand_id = Input::post('brand_id');
              if(!null==Input::post('campaign_val'))
                $campaign_val = Input::post('campaign_val');
              else
                 $campaign_val =NULL;
               
                 $result =DB::table('temp_posts')
                ->where('id', $id)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update(array('campaign' =>$campaign_val));  // update the record in the DB. 
                $row = DB::table('predict_logs') //check row exist
                    ->where('id', $id) 
                    ->where('type','post')
                    ->limit(1)
                    ->get()->toArray();
                  
                  if(!empty($row))
                  {
                      DB::table('predict_logs')->where('id',$id)->where('type','post')
                        ->update(array('campaign' =>$campaign_val));
                  }
                  else
                  {
                    // dd("data not exist");
                      DB::table('predict_logs')
                        ->insert(array('id' => $id,'type' => 'post','campaign' =>$campaign_val,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
                  }
    
             
                return $result;
    
            }

            public function SetCampaign_article()
            {
                  //dd('dddddd');
                  $id = Input::post('id');
                  $brand_id = Input::post('brand_id');
                  if(!null==Input::post('campaign_val'))
                    $campaign_val = Input::post('campaign_val');
                  else
                     $campaign_val =NULL;
                   
                     $result =DB::table('temp_web_mentions')
                    ->where('id', $id)  // find your user by their email
                    ->limit(1)  // optional - to ensure only one record is updated.
                    ->update(array('campaign' =>$campaign_val));  // update the record in the DB. 
                    $row = DB::table('predict_logs') //check row exist
                        ->where('id', $id) 
                        ->where('type','temp_web_mentions')
                        ->limit(1)
                        ->get()->toArray();
                      
                      if(!empty($row))
                      {
                          DB::table('predict_logs')->where('id',$id)->where('type','temp_web_mentions')
                            ->update(array('campaign' =>$campaign_val));
                      }
                      else
                      {
                        // dd("data not exist");
                          DB::table('predict_logs')
                            ->insert(array('id' => $id,'type' => 'temp_web_mentions','campaign' =>$campaign_val,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
                      }
        
                 
                    return $result;
        
                }

        public function SetGroupPostCampaign()
        {
              //dd('dddddd');
              $id = Input::post('id');
              $brand_id = Input::post('brand_id');
              if(!null==Input::post('campaign_val'))
                $campaign_val = Input::post('campaign_val');
              else
                 $campaign_val =NULL;
               
                 $result =DB::table('temp_group_posts')
                ->where('temp_id', $id)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update(array('campaign' =>$campaign_val));  // update the record in the DB. 
                $row = DB::table('predict_logs') //check row exist
                    ->where('id', $id) 
                    ->where('type','group_post')
                    ->limit(1)
                    ->get()->toArray();
                  
                  if(!empty($row))
                  {
                      DB::table('predict_logs')->where('id',$id)->where('type','group_post')
                        ->update(array('campaign' =>$campaign_val));
                  }
                  else
                  {
                    // dd("data not exist");
                      DB::table('predict_logs')
                        ->insert(array('id' => $id,'type' => 'group_post','campaign' =>$campaign_val,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
                  }
    
             
                return $result;
    
            }

    public function UpdatedPostPredict ()
    {
              $post_id = Input::post('id');
              $brand_id = Input::post('brand_id');
              $sentiment = Input::post('sentiment');
              $emotion = Input::post('emotion');
      
                 $result =DB::table('temp_inbound_posts')
                ->where('id', $post_id)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update(array('sentiment' => $sentiment,'emotion' => $emotion,'change_predict'=>1));  // update the record in the DB. 
             
                return $result;
    }

    public function ActionUpdate()
    {
              $comment_id = Input::post('id');
              $brand_id = Input::post('brand_id');
              $action_status = Input::post('action_status');

               $result =DB::table('temp_inbound_comments')
                ->where('id', $comment_id)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update(array('action_status' => $action_status,'action_taken' =>Auth::user()->name)); 

                 $row = DB::table('predict_logs') //check row exist
                ->where('id', $comment_id) 
                ->where('type','inbound_comment')
                ->limit(1)
                ->get()->toArray();
              
              if(!empty($row))
              {
                  DB::table('predict_logs')->where('id',$id)->where('type','inbound_comment')
                   ->update(array('action_status' => $action_status,'action_taken' =>Auth::user()->name)); 
              }
              else
              {
                // dd("data not exist");
                  DB::table('predict_logs')
                    ->insert(array('id' => $id,'type' => 'inbound_comment','action_status' =>  $action_status,'action_taken' =>Auth::user()->name,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()));
              }

                return $result; 
    }

    

    



}

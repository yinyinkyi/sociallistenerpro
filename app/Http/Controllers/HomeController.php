<?php

namespace BIMiner\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use BIMiner\InboundPages;
use BIMiner\Project;
use BIMiner\ProjectKeyword;
use BIMiner\demo;
use BIMiner\User;
use BIMiner\Http\Controllers\GlobalController;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{

     use GlobalController;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $layout = 'layouts';
    public function __construct()
    {
       // $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

  
    public function index()
    {
    
      if($user=Auth::user())
        {
          // dd('hh');
            $login_user = auth()->user()->id;
            // dd($login_user);
            $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
            foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

            $permission_data = $this->getPermission();
            $source ='in';
            $title="Brand";
            $project_data = $this->getProject();

            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
    
          if(count($project_data)>0)
          {
            
          $project_name = $project_data[0]['name']; 
          $pid= $project_data[0]['id'];
          $ownpage=$project_data[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $count = $this->getProjectCount($project_data);
          // dd($accountPermission);
          // return \Redirect::route('post',['pid' => $pid,'source'=>$source]);
          return \Redirect::route('competitor',['pid' => $pid,'source'=>$source,'accountPermission' => $accountPermission]);

          }
          else
          {
             return \Redirect::route('brandList',['source'=>$source,'action_permission' => $action_permission]);
          }
            // $count = $this->getProjectCount($project_data);
            // return view('brandlist',compact('title','project_data','permission_data','count','source'));
       }

        
        else
        {
           
              $title="";
          
            return view('auth.login',compact('title'));
        }
        

    }
    public function chargesAndRates()
    {
      if($user=Auth::user())
        {
          
            $login_user = auth()->user()->id;
            // dd($login_user);
            $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
            foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

            $permission_data = $this->getPermission();
            $source ='in';
            $title="Brand";
            $project_data = $this->getProject();

            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
    
          if(count($project_data)>0)
          {
            
          $project_name = $project_data[0]['name']; 
          $pid= $project_data[0]['id'];
          $ownpage=$project_data[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $count = $this->getProjectCount($project_data);
          // return \Redirect::route('post',['pid' => $pid,'source'=>$source]);
          $brand_id = 38;
          $tags = DB::table('tags')->where('brand_id',$brand_id)->get();
          $page_arr = $this->getpagename($brand_id);
          $companyData=$this->getCompanyData($pid);
          foreach($page_arr as $pg){
          $page_arr = explode(',', $pg['monitor_pages']);
      
      }
          
          return view('chargesAndRatesView',compact('pid','source','accountPermission','project_data','count','title','permission_data','tags','page_arr','companyData'));
          }
          else
          {
             return \Redirect::route('brandList',['source'=>$source,'action_permission' => $action_permission]);
          }
            // $count = $this->getProjectCount($project_data);
            // return view('brandlist',compact('title','project_data','permission_data','count','source'));
       }

        
        else
        {
           
              $title="";
          
            return view('auth.login',compact('title'));
        }
    }
    public function autoTagging()
    {
    
      if($user=Auth::user())
        {
          // dd('hh');
            $login_user = auth()->user()->id;
            // dd($login_user);
            $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
            foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

            $permission_data = $this->getPermission();
            $source ='in';
            $title="Brand";
            $project_data = $this->getProject();

            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
    
          if(count($project_data)>0)
          {
            
          $project_name = $project_data[0]['name']; 
          $pid= $project_data[0]['id'];
          $ownpage=$project_data[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $count = $this->getProjectCount($project_data);
          // return \Redirect::route('post',['pid' => $pid,'source'=>$source]);
          $brand_id = 38;
          $tags = DB::table('tags')->where('brand_id',$brand_id)->get();
          $page_arr = $this->getpagename($brand_id);
          foreach($page_arr as $pg){
          $page_arr = explode(',', $pg['monitor_pages']);
      
      }
          
          
          return view('autoTagForm',compact('pid','source','accountPermission','project_data','count','title','permission_data','tags','page_arr'));
          }
          else
          {
             return \Redirect::route('brandList',['source'=>$source,'action_permission' => $action_permission]);
          }
            // $count = $this->getProjectCount($project_data);
            // return view('brandlist',compact('title','project_data','permission_data','count','source'));
       }

        
        else
        {
           
              $title="";
          
            return view('auth.login',compact('title'));
        }
        

    }
    public function companyInfo()
    {
        $title="companyInfo";
          $source='in';
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
            return view('company')->with('project_data',$project_data)
                                    ->with('permission_data',$permission_data )
                                    ->with('count',$count)
                                    ->with('source',$source)
                                    ->with('title',$title);
    }
 

    
    public function dashboard()
    {
        $pid=Input::get("pid");
      
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
        
          if(count($checked_data)>0)
          {
            $title="Dashboard";
            $source=Input::get('source');
            $companyData=$this->getCompanyData($pid);
            $project_data = $this->getProject();
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $DemoHideDiv =$this->getDemoHideDiv("Dashboard");
            $default_page ='';
            // $last_crawl_date =$this->getlastcrawldate($pid);
            // $last_crawl_date = date('d-M-Y h:i:s a',strtotime($last_crawl_date[0]));
          
            // dd($data_down_status);
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitorpage=$project_data_id[0]['monitor_pages'];
            $default_page=$project_data_id[0]['default_page'];
            $monitorpage=explode(',', $monitorpage);

            // $monitorpage = [];
            // foreach($monitorpage_exp as $value)
            // {
            //   $monitorpage[]=$value;
            // }
           
            $count = $this->getProjectCount($project_data);
            $ownpage=$project_data_id[0]['own_pages'];
            $ownpage=explode(',', $ownpage);
            $last_crawl_date='';
           // $last_crawl_date = InboundPages::where('page_name',$ownpage[0])->get();
            $result_crawl_date=InboundPages::whereRaw(['page_name' => array('$in' =>$monitorpage)])->get();
           if(null !== $result_crawl_date->max('last_crawl_date'))
            {    
                 $last_crawl_date =$result_crawl_date->max('last_crawl_date');
                 $last_crawl_date = $last_crawl_date->toDateTime();
                  $last_crawl_date=$last_crawl_date->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                  $last_crawl_date = $last_crawl_date->format('M d, Y h:i:s A');
            }
            //dd($last_crawl_date);
          //   foreach ($last_crawl_date as  $key => $row) {
          //         $last_crawl_date = $row["last_crawl_date"];
          //         if(isset( $last_crawl_date))
          //         {
          //         $last_crawl_date = $last_crawl_date->toDateTime();
          //         $last_crawl_date=$last_crawl_date->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
          //         $last_crawl_date = $last_crawl_date->format('M d, Y h:i:s A');
          //         }
          //         else
          //         {
          //           $last_crawl_date='';
          //         }
                  
          // }
          // dd($monitorpage);
            if($source === 'out')
            return view('dashboard',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','companyData'));
            else
            return view('dashboard_no_pgaccess',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','monitorpage','default_page','last_crawl_date','companyData'));
            }
  
            else
            {
              
                 return abort(404);
            }
          }
          else
          {
            
               return abort(404);
          }
         

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }
    public function updateByAutoTag()
    {
     
      $originTag = Input::get('originTag');
      $originTagID = Input::get('originTagID');
      $senti = "'".Input::get('senti')."'";
      
      $add_con ='';
      if(null !== Input::get('pages'))
      {

       $pages_arr= explode(',',Input::get('pages'));
       $pages_str = "'" . implode ( "', '", $pages_arr ) . "'";
 
       $add_con.=" and page_name In(".$pages_str.")";

      }
     
      $newTag = "'".Input::get('newTag')."'";

      $newTagID = Input::get('newTagID');

      $newTagID_str='';
      if(null !== Input::get('newTagID'))
      $newTagID_str = "'".implode(',', $newTagID)."'";
      
      // dd($newTagID_str);
      if(null !== Input::get('postID')){
      $postID = Input::get('postID');
      $postquery = "select * from temp_inbound_posts where id Like'%".$postID."%'";
      $post_id = DB::select($postquery);
      // dd($post_id);
      $full_post_id = '';
      foreach($post_id as $p_id) $full_post_id = "'".$p_id->id."'";
      $add_con.=' and post_id='.$full_post_id;
      
      }

      // dd($add_con);
      if(null !== $originTagID){
       
       $originTagID_str = "'".implode(',', $originTagID)."'";
     
       $sqlquery = "update temp_inbound_comments set checked_sentiment=1,change_predict=1,checked_tags=".$newTag." ,checked_tags_id=".$newTagID_str." ,checked_sentiment=".$senti." where checked_tags_id=".$originTagID_str .  $add_con ; 
       DB::select($sqlquery);
     
      }
      else
      {

         $sqlquery = "update temp_inbound_comments set checked_sentiment=1,change_predict=1,checked_tags=".$newTag." ,checked_tags_id=".$newTagID_str." ,checked_sentiment=".$senti." where checked_tags='' OR checked_tags Is Null ". $add_con;
        // dd($sqlquery);
         DB::select($sqlquery);

        
       }
    }

    // public function check_mm_en(){
    //   $keyword = "grab ျတနသ္ျနတသ";
    //   if (preg_match('/[က-အ]/', $keyword)) // '/[^a-z\d]/i' should also work.
    //   {
    //     return 'mm';
    //   }
    //   else{
    //     return "en";
    //   }
    // }

     public function post()
    {
     
      
     // dd("hey");
        $pid=Input::get("pid");

        if($user=Auth::user())
        {
         // dd(auth()->user()->id);
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Mentions";
            $source=Input::get('source');
            $project_data = $this->getProject();
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $DemoHideDiv =$this->getDemoHideDiv("Post");
            $companyData=$this->getCompanyData($pid);
          
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $ownpage=$project_data_id[0]['monitor_pages'];
            $default_keyword=$project_data_id[0]['default_keyword'];
            $ownpage=explode(',', $ownpage);
            $count = $this->getProjectCount($project_data);

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

           
            return view('posts',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','ownpage','companyData','default_keyword','accountPermission'));
            }
  
            else
            {
              
                 return abort(404);
            }
          }
          else
          {
            
               return abort(404);
          }
         

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }
    public function comment()
    {
     
      
     
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Comments";
          $source=Input::get('source');
          $project_data = $this->getProject();
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
          $DemoHideDiv =$this->getDemoHideDiv("Comment");
          $companyData=$this->getCompanyData($pid);
        
          if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $count = $this->getProjectCount($project_data);
          $brand_id =  $pid;
          return view('comments',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','ownpage','brand_id','companyData'));
          }

          else
          {
            
               return abort(404);
          }
          }
          else
          {
            
               return abort(404);
          }
          

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }

    public function mention()
    {
      
      $pid=Input::get("pid");
     
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Mention";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $companyData=$this->getCompanyData($pid);
             if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
         
            return view('mention',compact('title','project_data','count','permission_data','project_name','pid','source','companyData'));
            }
  
                  else
            {
              /*return 404*/
                 return abort(404);
            }
          }
          else
          {
            /*return 404*/
               return abort(404);
          }
         
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    
    }

      public function page_manage()
    {
      
      $pid=Input::get("pid");
     
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="page management";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $companyData=$this->getCompanyData($pid);
             if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $ownpage=$project_data_id[0]['monitor_pages'];
            $ownpage=explode(',', $ownpage);
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
           
            return view('page_management',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','companyData'));
            }
  
                  else
            {
              /*return 404*/
                 return abort(404);
            }
          }
          else
          {
            /*return 404*/
               return abort(404);
          }
         
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    
    }


        public function pointoutmention()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Mention";
          $source='out';
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
     
          return view('pointOutMention',compact('title','project_data','count','permission_data','project_name','pid','source','companyData'));
           }
            else
            {
              return abort(404);
            }
          }
          else
            {
              return abort(404);
            }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }

    public function pointoutcomment()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Mention";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
     
          return view('pointOutComment',compact('title','project_data','count','permission_data','project_name','pid','source','companyData'));
           }
            else
            {
              return abort(404);
            }
          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    public function relatedcomment()
    {
     
      $pid=Input::get("pid");

        if(null !== Input::get("admin_page")) $admin_page = Input::get("admin_page");
        

        $default_page = '';
        if(null !== Input::get('default_page'))
        $default_page=Input::get("default_page");

       // if(null !== Input::get('highlight_text'))
       //  $default_page=Input::get("default_page");
// dd($admin_page);
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
          $title="Related comments";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $brand_id =  $pid;

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

          //$category = tagCategory::select(['id', 'category_name'])->orderBy('category_name')->get();
          return view('showrelatedcomment',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','brand_id','companyData','admin_page','default_page','accountPermission'));
           }
            else
            {
              return abort(404);
            }
          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
        public function relatedgroupcomment()
    {
     
      $pid=Input::get("pid");

        if(null !== Input::get("admin_page")) $admin_page = Input::get("admin_page");
        

        $default_page = '';
        if(null !== Input::get('default_page'))
        $default_page=Input::get("default_page");

       // if(null !== Input::get('highlight_text'))
       //  $default_page=Input::get("default_page");
// dd($admin_page);
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
          $title="Related comments";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $brand_id =  $pid;

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

          //$category = tagCategory::select(['id', 'category_name'])->orderBy('category_name')->get();
          return view('showrelatedgroupcomment',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','brand_id','companyData','admin_page','default_page','accountPermission'));
           }
            else
            {
              return abort(404);
            }
          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    public function mentionPageList()
    {
      // if($user=Auth::user())
      // {
      //   return view('mention_pagesList');
      // }
         if($user=Auth::user())
        {
          $title="Setting";
          $companyData=[];
          $source = Input::get('source');
           $source ='in';
          //dd($source);
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

          return view('mention_pagesList',compact('title','project_data','permission_data','count','source','companyData','accountPermission'));
        }
     else
      {
          $title="Login";
          return view('auth.login',compact('title'));
      }
    }
    public function related_campaigncomment()
    {
      if($user=Auth::user())
        {
          $pid=Input::get("pid");
          if(null !== Input::get("campaign_name")) $campaign_name = Input::get("campaign_name");
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
          $title="Related Campaign comments";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
        
          
          return view('showrelatedcampaign_comment',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','companyData','campaign_name'));
           }
            else
            {
              return abort(404);
            }
          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }
       public function setSessionCmtID(Request $request)
    {

      $highlight_text=Input::post("highlight_text");
      $wordcloud_text=Input::post("wordcloud_text");
      $request->session()->put(Input::post("highlight_text"),Input::post("wordcloud_text"));
          
      return 'success';
    }
    
    public function highlightedcomment()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Related comments";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission();
            $companyData=$this->getCompanyData($pid);
            // print_r($permission_data) ; return;
             if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $ownpage=$project_data_id[0]['monitor_pages'];
            $ownpage=explode(',', $ownpage);
            $brand_id =  $pid;
            return view('showhighlightedcomment',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','brand_id','companyData'));
             }
              else
              {
                return abort(404);
              }
          }
          else
              {
                return abort(404);
              }
         
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    
    
    public function relatedcompetitorcomment()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Related comments";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $brand_id =  $pid;
          
     
          return view('showrelatedcompetitorcmt',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','brand_id','companyData'));
           }
            else
            {
              return abort(404);
            }
          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
     public function relatedpost()
    {
     
      $pid=Input::get("pid");
      

      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Related Posts";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);

          $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
     
          return view('showrelatedpost',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','companyData','accountPermission'));
           }
            else
            {
              return abort(404);
            }

          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    public function relatedcampaignmention()
    {
      $pid=Input::get("pid");

      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Related Campaign Mentions";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);

          $login_user = auth()->user()->id;
          $accountPermission = '';
          $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
          foreach($UserPermission as $up) $isDemo = $up->demo;
          if($isDemo == 2) $accountPermission = 'Demo';
     
          return view('showrelatedcampaignmention',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','companyData','accountPermission'));
           }
            else
            {
              return abort(404);
            }

          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }

      public function relatedmention()
    
    {
     
      $pid=Input::get("pid");
      if(Input::get('hidden_kw') == null){
        $hidden_kw = Input::get("keyword_group");
     
    }
      else{
         $hidden_kw=Input::get("hidden_kw");
      }
      // dd($hidden_kw);
      
      if(null !== Input::get('default_kw'))
      $default_kw=Input::get("default_kw");
      
         // dd($default_kw);
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Related Mentions";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);

          $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

     
          return view('showrelatedmention',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','companyData','hidden_kw','default_kw','accountPermission'));
           }
            else
            {
              return abort(404);
            }

          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    public function relatedarticle()
    {
      $pid=Input::get("pid");
      if(Input::get('hidden_kw') == null){
        $hidden_kw = Input::get("keyword_group");
     
    }
      else{
         $hidden_kw=Input::get("hidden_kw");
      }
      // dd($hidden_kw);
      
      if(null !== Input::get('default_kw'))
      $default_kw=Input::get("default_kw");

      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
          $title="Related Articles";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);

           $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
     
          return view('showrelatedArticle',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','companyData','hidden_kw','default_kw','accountPermission'));
           }
            else
            {
              return abort(404);
            }

          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
      public function relatedmentioncmt()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Related Mentions";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          $companyData=$this->getCompanyData($pid);
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);

          $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

     
          return view('showrelatedmentioncmt',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage','companyData','accountPermission'));
           }
            else
            {
              return abort(404);
            }

          }
          else
          {
            return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    public function pointoutpost()
    {
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Mention";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission();
            $companyData=$this->getCompanyData($pid);
            // print_r($permission_data) ; return;
             if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
       
            return view('pointOutPost',compact('title','project_data','count','permission_data','project_name','pid','source','companyData'));
             }
              else
              {
                return abort(404);
              }

          }
          else
          {
            return abort(404);
          }
        
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }
    public function gethiddendiv()
    {
         $hidden_div=[];
         if($this->getIsDemouser() === true)
         {
          $view_name=Input::get("view_name");
          $hidden_div = demo::select('*')->where('is_hide',1)->where('view_name',$view_name);
          $hidden_div=$hidden_div->orderBy('id','DESC')->get();
         }
         
          echo json_encode($hidden_div);
    }
    public function analysis()
    {
      
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Analysis";
           $source=Input::get('source');
           $project_data_id = $this->getProjectByid($pid);
           $permission_data = $this->getPermission(); 
           $companyData=$this->getCompanyData($pid);
             if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
         if($source === 'out')
        return view('analysis',compact('title','project_data','count','permission_data','project_name','pid','source','companyData'));
          else
         return view('analysis_in',compact('title','project_data','count','permission_data','project_name','pid','source','companyData'));
          }
            else
          {
            /*return 404*/
              return abort(404);
          }
          }
          else
          {
            /*return 404*/
              return abort(404);
          }
          
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }

     public function comparison()
    {
      // dd(Input::get('source'));
     
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Comparison";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $companyData=$this->getCompanyData($pid);
                if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $tags   = $this->gettags($pid);
            $tagGroup   = $this->gettagGroup($pid);
            //$project_data_exclude = $this->getProjectByExcludeid($pid);
            // $compare_pages= $this->getComparisonPage($pid);
            // $compare_pages=explode(',', $compare_pages);
            // dd($compare_pages);
            $ptags   = $this->gettags($pid);
            $ptagGroup   = $this->gettagGroup($pid);

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';


             if($source === 'out')
          return view('comparison',compact('title','project_data','count','project_name','permission_data','pid','source','companyData'));
            else
          return view('comparison_in',compact('title','project_data','count','project_name','permission_data','pid','source','companyData','tags','tagGroup','ptags','ptagGroup','accountPermission'));
  
             }
              else
            {
              /*return 404*/
                 return abort(404);
            }

          }
          else
          {
            /*return 404*/
               return abort(404);
          }
        
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    public function comparisonkeygroup()
    {
     
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Comparison";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $companyData=$this->getCompanyData($pid);
                if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];

            $default_page = auth()->user()->default_page;
            $default_keyword = auth()->user()->default_keyword;
            $project_data = $this->getProject();

            // dd($project_data);
            $count = $this->getProjectCount($project_data);
            $tags   = $this->gettags($pid);
            //$project_data_exclude = $this->getProjectByExcludeid($pid);
            // $compare_pages= $this->getComparisonPage($pid);
            // $compare_pages=explode(',', $compare_pages);
            // dd($compare_pages);
            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

         
          return view('comparisonkeygroup',compact('title','project_data','count','project_name','permission_data','pid','source','companyData','tags','default_page','default_keyword','accountPermission'));
  
             }
              else
            {
              /*return 404*/
                 return abort(404);
            }

          }
          else
          {
            /*return 404*/
               return abort(404);
          }
        
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    public function comparisonpdf()
    {
     
       $pid=Input::get("pid");
       $kw_gp=Input::get("kw_gp");
       $project_name="";
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Mention Analysis Report";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $companyData=$this->getCompanyData($pid);
                if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitorpage=$project_data_id[0]['monitor_pages'];
            $monitorpage=explode(',', $monitorpage);
  
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            //$project_data_exclude = $this->getProjectByExcludeid($pid);
            $compare_pages= $this->getComparisonPage($pid);
            $compare_pages=explode(',', $compare_pages[0]['monitor_pages']);
            // dd($compare_pages);
            $companyData=$this->getCompanyData($pid);
          return view('reports.comparisonPDF_no_pageaccess',compact('title','kw_gp','project_data','count','project_name','compare_pages','permission_data','pid','companyData','source','monitorpage','companyData'));
  
             }
              else
            {
              /*return 404*/
                 return abort(404);
            }

          }
          else
          {
            /*return 404*/
               return abort(404);
          }
        
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
     
    }
    public function pagespdf()
    {
       $pid=Input::get("pid");
       $page_name=Input::get("page_name");
       $project_name="";
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Pages Analysis Report";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $companyData=$this->getCompanyData($pid);
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitorpage=$project_data_id[0]['monitor_pages'];
            $monitorpage=explode(',', $monitorpage);
  
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            //$project_data_exclude = $this->getProjectByExcludeid($pid);
            $compare_pages= $this->getComparisonPage($pid);
            $compare_pages=explode(',', $compare_pages[0]['monitor_pages']);
            // dd($compare_pages);
            $companyData=$this->getCompanyData($pid);
          return view('reports.pagesPDF_no_pageaccess',compact('title','page_name','project_data','count','project_name','compare_pages','permission_data','pid','companyData','source','monitorpage','companyData'));
  
             }
              else
            {
              /*return 404*/
                 return abort(404);
            }

          }
          else
          {
            /*return 404*/
               return abort(404);
          }
        
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }

      public function competitor()
    {
        $pid=Input::get("pid");
        // dd($pid);
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
        // dd($checked_data);
          if(count($checked_data)>0)
          {
            $title="Pages";
            $source=Input::get('source');
            $project_data = $this->getProject();
            // dd($project_data);
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $DemoHideDiv =$this->getDemoHideDiv("Post");
            $companyData=$this->getCompanyData($pid);
          
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitor=$project_data_id[0]['monitor_pages'];
            $monitor=explode(',', $monitor);
            $ownpage=$project_data_id[0]['own_pages'];
            $ownpage=explode(',', $ownpage);
            $otherpage=array_diff($monitor,$ownpage);
            sort($otherpage);
             $default_page=$project_data_id[0]['default_page'];
            // dd($otherpage);
            $count = $this->getProjectCount($project_data);
            $brand_id =  $pid;
            $tags   = $this->gettags($brand_id);
            $tagGroup   = $this->gettagGroup($brand_id);
            // dd($otherpage);
            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

            return view('competitor',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','otherpage','brand_id','companyData','default_page','accountPermission','tags','tagGroup'));
            }
  
            else
            {
              
                 return abort(404);
            }

          }
          else
          {
            
               return abort(404);
          }
        

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }
          public function industry()
    {
        $pid=Input::get("pid");
        // dd($pid);
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
        // dd($checked_data);
          if(count($checked_data)>0)
          {
            $title="industry";
            $source=Input::get('source');
            $project_data = $this->getProject();
            // dd($project_data);
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $DemoHideDiv =$this->getDemoHideDiv("Post");
            $companyData=$this->getCompanyData($pid);
          
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitor=$project_data_id[0]['monitor_pages'];
            $monitor=explode(',', $monitor);
            $ownpage=$project_data_id[0]['own_pages'];
            $ownpage=explode(',', $ownpage);
            $otherpage=array_diff($monitor,$ownpage);
            sort($otherpage);
             $default_page=$project_data_id[0]['default_page'];
            // dd($otherpage);
            $count = $this->getProjectCount($project_data);
            $brand_id =  $pid;
            // dd($otherpage);
            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

            return view('industry',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','otherpage','brand_id','companyData','default_page','accountPermission'));
            }
  
            else
            {
              
                 return abort(404);
            }

          }
          else
          {
            
               return abort(404);
          }
        

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }
    public function otherSources()
    {
        $pid=Input::get("pid");
       
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
        // dd($checked_data);
          if(count($checked_data)>0)
          {
            $title="Other Sources";
            $source=Input::get('source');
            $project_data = $this->getProject();
            // dd($project_data);
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $DemoHideDiv =$this->getDemoHideDiv("Post");
            $companyData=$this->getCompanyData($pid);
          
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitor=$project_data_id[0]['monitor_pages'];
            $monitor=explode(',', $monitor);
            $ownpage=$project_data_id[0]['own_pages'];
            $ownpage=explode(',', $ownpage);
            $otherpage=array_diff($monitor,$ownpage);
            sort($otherpage);
             $default_page=$project_data_id[0]['default_page'];
            // dd($otherpage);
            $count = $this->getProjectCount($project_data);
            $brand_id =  $pid;
            // dd($otherpage);
            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

            return view('other_sources',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','otherpage','brand_id','companyData','default_page','accountPermission'));
            }
  
            else
            {
              
                 return abort(404);
            }

          }
          else
          {
            
               return abort(404);
          }
        

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }
     public function campaign()
    {
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Pages";
            $source=Input::get('source');
            $project_data = $this->getProject();
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $DemoHideDiv =$this->getDemoHideDiv("Post");
            $companyData=$this->getCompanyData($pid);
          
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitor=$project_data_id[0]['monitor_pages'];
            $monitor=explode(',', $monitor);
            $ownpage=$project_data_id[0]['own_pages'];
            $ownpage=explode(',', $ownpage);
            $otherpage=array_diff($monitor,$ownpage);
            sort($otherpage);
             $default_page=$project_data_id[0]['default_page'];
            // dd($otherpage);
            $count = $this->getProjectCount($project_data);
            $brand_id =  $pid;

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  

            $pages =  DB::table('campaignGroup')->distinct()->where('brand_id', '=', $pid)->get(['page_name']);
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
            
            return view('campaign',compact('pages', 'title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','otherpage','brand_id','companyData','default_page','accountPermission'));
            }
  
            else
            {
              
                 return abort(404);
            }

          }
          else
          {
            
               return abort(404);
          }
        

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }

    public function getallpages()
    {
      // called by comparison_in blade
      $brand_id=Input::get('brand_id');
      $company_id=Input::get('company_id');

      // $projects_pages = Project::select('default_page')->where('id','=',$brand_id)->get()->toArray();
      $default_page = User::select('custom_page')->where('id',auth()->user()->id)->get()->toArray();
      $compare_pages= $this->getComparisonPage_fromCmpInfo($company_id);
      // monitor pages from companyinfo and default page from user
      
      $monitor_page = $compare_pages[0]['pages_name'];
      $default_page = $default_page[0]['custom_page'];

      if($default_page <> '')
      {
      $default_pages_arr=explode(',',$default_page);
      $monitor_page_arr=explode(',',$monitor_page);
      
      $compare_pages = array_diff($monitor_page_arr,$default_pages_arr);
      usort($compare_pages, 'strnatcasecmp');
      array_unshift($compare_pages,$default_page);
// dd($default_page);
      }
      else
      {
       $compare_pages=explode(',',$monitor_page);
      }

      
      // dd($compare_pages);
      echo json_encode($compare_pages);

    }
    public function getBankType()
    {

      $brand_id=Input::get('brand_id');
      $company_id=Input::get('company_id');
      $default_page='';
      $otherpage=[];
      $pages_name = '';
      // $project_data_id = $this->getProjectByid($brand_id);
      $project_data_id = $this->getProjectByid_fromCmpInfo($company_id);
      // dd($project_data_id);
      

      if(count($project_data_id)>0)
      {
        // dd("hey");
        $monitor=$project_data_id[0]['monitor_pages'];
        $pages_name=$project_data_id[0]['pages_name'];
 // dd($monitor);
        $monitor=explode(',', $monitor);
        // dd($monitor);
        $otherpage=$monitor;
        // if(auth()->user()->id == 35)
        // {

        // }
        // $ownpage=$project_data_id[0]['own_pages'];
        // $ownpage=explode(',', $ownpage);
        // $diffpage=array_diff($monitor,$ownpage);
        // foreach ($diffpage as  $key => $row) {
        //   $otherpage[]=$row;
        // }

         // $default_page=$project_data_id[0]['default_page'];
         $username = auth()->user()->username;
         $default_page = User::select(['custom_page'])->where('username',$username)->get()->toArray();
         // dd($default_page);
         foreach($default_page as $pg) $default_page = $pg['custom_page'];
         $default_page=$this->Format_Page_name_single($default_page);
         // $default_page='MFS';
      }
    $url = asset('assets/images/unknown_photo.jpg');
    $rawdata=[];$data=[];$newdata = [];

      $data[] =[
        'id' => 'MFS',
        'imgurl' => '',
        'page_name' =>'MFS',
        'default_page'=>'Bank',
        ];

      $rawdata =[
        'id' => 'Bank',
        'imgurl' => '',
        'page_name' =>'Bank',
        'default_page'=>'Bank',
      ];

      $newdata =[
        'id' => 'Card',
        'imgurl' => '',
        'page_name' =>'Card Network',
        'default_page'=>'Bank',
      ];

        array_push($data,$rawdata,$newdata);

    

      // dd($data);
    // if pages  are not renamed , full page name will be shown
   

  
  // dd($data);
  usort($data, function($a, $b) {
    return strcmp($a["page_name"], $b["page_name"]);
});
// dd($data);
      echo json_encode($data);
    }
    public function getBankPage(){
            // called by pages blade
      $brand_id=Input::get('brand_id');
      $company_id=Input::get('company_id');
      $type=Input::get('type');

      $default_page='';
      $otherpage=[];
      $pages_name = '';
      // $project_data_id = $this->getProjectByid($brand_id);
      // $project_data_id = $this->getProjectByid_fromCmpInfo($company_id);
      $project_data_id= DB::table('industry')->where('type',$type)->get();
      $otherpage = [];$pages_name = [];



      if(count($project_data_id)>0)
      {
        foreach($project_data_id as $value){
          $otherpage[] = $value->origin_name;
          $pages_name[] =$value->page_name;
        }

         $username = auth()->user()->username;
         $default_page = User::select(['custom_page'])->where('username',$username)->get()->toArray();
         foreach($default_page as $pg) $default_page = $pg['custom_page'];
         $default_page=$this->Format_Page_name_single($default_page);
      }
      $mongo_page = $this->Format_Page_Name($otherpage);
      $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

        return $collection->aggregate([
            [
            '$match' =>[
                 '$and'=> [ 
                 ['page_name'=>['$in'=>$mongo_page]],
                
                          ]
            ]  
                       
           ],
           

          ['$sort' =>['created_at'=>-1]]
            
        ]);
    })->toArray();
      // dd($InboundPages_result);
    $rawdata=[];$data=[];
    foreach ($InboundPages_result as  $key => $row) {
      $id='';$page_name_new='';$full_page_name='';$imgurl='';
      if(isset($row['id'])) $id = $row['id'];
      if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
      if(isset($row['page_name'])) $page_name = $row['page_name'];
      if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

      // if(is_numeric($page_name))
      // {
      //   //find same name of page name by matching mongo page id and mysql page name
      //    $input = preg_quote($page_name, '~'); // don't forget to quote input string!
      //    $keys = preg_grep('~' . $input . '~', $otherpage);
      //    $vals = array();
      //       foreach ( $keys as $key )
      //       {
      //          $page_name= $key;
      //       }
           
      // }
      $rawdata[] =[
        'id' => $id,
        'imgurl' => $imgurl,
        'page_name' =>$full_page_name,
        'default_page'=>$default_page
        ];

    }

 // dd($rawdata);
    // if pages  are not renamed , full page name will be shown
    // if(empty($pages_name)){
      // dd("dhdd");
    foreach($rawdata as  $val)
    {
      $pg = $val['page_name'];
      $rename = DB::table('page_rename')->where('origin_name',$pg)->get()->toArray();
     
      foreach($rename as  $value) 
        {
          $custom_name = $value->custom_name;
          $data[] =[
            'id' => $val['id'],
            'imgurl' => $val['imgurl'],
            'page_name' => $custom_name,
            'default_page' => $val['default_page']
          ];
        }
    }
  // }
  // dd($data);
  usort($data, function($a, $b) {
    return strcmp($a["page_name"], $b["page_name"]);
});
// dd($data);
      echo json_encode($data);

    }
        public function getMfsPage(){
            // called by pages blade
      $brand_id=Input::get('brand_id');
      $company_id=Input::get('company_id');
      $type=Input::get('type');

      $default_page='';
      $otherpage=[];
      $pages_name = '';
      // $project_data_id = $this->getProjectByid($brand_id);
      // $project_data_id = $this->getProjectByid_fromCmpInfo($company_id);
      $project_data_id= DB::table('industry')->where('type',$type)->get();
      $otherpage = [];$pages_name = [];



      if(count($project_data_id)>0)
      {
        foreach($project_data_id as $value){
          $otherpage[] = $value->origin_name;
          $pages_name[] =$value->page_name;
        }

         $username = auth()->user()->username;
         $default_page = User::select(['custom_page'])->where('username',$username)->get()->toArray();
         foreach($default_page as $pg) $default_page = $pg['custom_page'];
         $default_page=$this->Format_Page_name_single($default_page);
      }
      $mongo_page = $this->Format_Page_Name($otherpage);
      $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

        return $collection->aggregate([
            [
            '$match' =>[
                 '$and'=> [ 
                 ['page_name'=>['$in'=>$mongo_page]],
                
                          ]
            ]  
                       
           ],
           

          ['$sort' =>['created_at'=>-1]]
            
        ]);
    })->toArray();
      // dd($InboundPages_result);
    $rawdata=[];$data=[];
    foreach ($InboundPages_result as  $key => $row) {
      $id='';$page_name_new='';$full_page_name='';$imgurl='';
      if(isset($row['id'])) $id = $row['id'];
      if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
      if(isset($row['page_name'])) $page_name = $row['page_name'];
      if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

      // if(is_numeric($page_name))
      // {
      //   //find same name of page name by matching mongo page id and mysql page name
      //    $input = preg_quote($page_name, '~'); // don't forget to quote input string!
      //    $keys = preg_grep('~' . $input . '~', $otherpage);
      //    $vals = array();
      //       foreach ( $keys as $key )
      //       {
      //          $page_name= $key;
      //       }
           
      // }
      $rawdata[] =[
        'id' => $id,
        'imgurl' => $imgurl,
        'page_name' =>$full_page_name,
        'default_page'=>$default_page
        ];

    }

 // dd($rawdata);
    // if pages  are not renamed , full page name will be shown
    // if(empty($pages_name)){
      // dd("dhdd");
    foreach($rawdata as  $val)
    {
      $pg = $val['page_name'];
      $rename = DB::table('page_rename')->where('origin_name',$pg)->get()->toArray();
     
      foreach($rename as  $value) 
        {
          $custom_name = $value->custom_name;
          $data[] =[
            'id' => $val['id'],
            'imgurl' => $val['imgurl'],
            'page_name' => $custom_name,
            'default_page' => $val['default_page']
          ];
        }
    }
  // }
  // dd($data);
  usort($data, function($a, $b) {
    return strcmp($a["page_name"], $b["page_name"]);
});
// dd($data);
      echo json_encode($data);

    }
        public function getCardPage(){
            // called by pages blade
      $brand_id=Input::get('brand_id');
      $company_id=Input::get('company_id');
      $type=Input::get('type');

      $default_page='';
      $otherpage=[];
      $pages_name = '';
      // $project_data_id = $this->getProjectByid($brand_id);
      // $project_data_id = $this->getProjectByid_fromCmpInfo($company_id);
      $project_data_id= DB::table('industry')->where('type',$type)->get();
      $otherpage = [];$pages_name = [];



      if(count($project_data_id)>0)
      {
        foreach($project_data_id as $value){
          $otherpage[] = $value->origin_name;
          $pages_name[] =$value->page_name;
        }

         $username = auth()->user()->username;
         $default_page = User::select(['custom_page'])->where('username',$username)->get()->toArray();
         foreach($default_page as $pg) $default_page = $pg['custom_page'];
         $default_page=$this->Format_Page_name_single($default_page);
      }
      $mongo_page = $this->Format_Page_Name($otherpage);
      $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

        return $collection->aggregate([
            [
            '$match' =>[
                 '$and'=> [ 
                 ['page_name'=>['$in'=>$mongo_page]],
                
                          ]
            ]  
                       
           ],
           

          ['$sort' =>['created_at'=>-1]]
            
        ]);
    })->toArray();
      // dd($InboundPages_result);
    $rawdata=[];$data=[];
    foreach ($InboundPages_result as  $key => $row) {
      $id='';$page_name_new='';$full_page_name='';$imgurl='';
      if(isset($row['id'])) $id = $row['id'];
      if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
      if(isset($row['page_name'])) $page_name = $row['page_name'];
      if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

      // if(is_numeric($page_name))
      // {
      //   //find same name of page name by matching mongo page id and mysql page name
      //    $input = preg_quote($page_name, '~'); // don't forget to quote input string!
      //    $keys = preg_grep('~' . $input . '~', $otherpage);
      //    $vals = array();
      //       foreach ( $keys as $key )
      //       {
      //          $page_name= $key;
      //       }
           
      // }
      $rawdata[] =[
        'id' => $id,
        'imgurl' => $imgurl,
        'page_name' =>$full_page_name,
        'default_page'=>$default_page
        ];

    }

 // dd($rawdata);
    // if pages  are not renamed , full page name will be shown
    // if(empty($pages_name)){
      // dd("dhdd");
    foreach($rawdata as  $val)
    {
      $pg = $val['page_name'];
      $rename = DB::table('page_rename')->where('origin_name',$pg)->get()->toArray();
     
      foreach($rename as  $value) 
        {
          $custom_name = $value->custom_name;
          $data[] =[
            'id' => $val['id'],
            'imgurl' => $val['imgurl'],
            'page_name' => $custom_name,
            'default_page' => $val['default_page']
          ];
        }
    }
  // }
  // dd($data);
  usort($data, function($a, $b) {
    return strcmp($a["page_name"], $b["page_name"]);
});
// dd($data);
      echo json_encode($data);

    }
    public function getOtherpage()
    {
      // called by pages blade
      $brand_id=Input::get('brand_id');
      $company_id=Input::get('company_id');
      $default_page='';
      $otherpage=[];
      $pages_name = '';
      // $project_data_id = $this->getProjectByid($brand_id);
      $project_data_id = $this->getProjectByid_fromCmpInfo($company_id);
      // dd($project_data_id);
      

      if(count($project_data_id)>0)
      {
        // dd("hey");
        $monitor=$project_data_id[0]['monitor_pages'];
        $pages_name=$project_data_id[0]['pages_name'];
 // dd($monitor);
        $monitor=explode(',', $monitor);
        // dd($monitor);
        $otherpage=$monitor;
        // if(auth()->user()->id == 35)
        // {

        // }
        // $ownpage=$project_data_id[0]['own_pages'];
        // $ownpage=explode(',', $ownpage);
        // $diffpage=array_diff($monitor,$ownpage);
        // foreach ($diffpage as  $key => $row) {
        //   $otherpage[]=$row;
        // }

         // $default_page=$project_data_id[0]['default_page'];
         $username = auth()->user()->username;
         $default_page = User::select(['custom_page'])->where('username',$username)->get()->toArray();
         // dd($default_page);
         foreach($default_page as $pg) $default_page = $pg['custom_page'];
         $default_page=$this->Format_Page_name_single($default_page);
      }
      $mongo_page = $this->Format_Page_Name($otherpage);
       // dd($mongo_page);
      $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

        return $collection->aggregate([
            [
            '$match' =>[
                 '$and'=> [ 
                 ['page_name'=>['$in'=>$mongo_page]],
                
                          ]
            ]  
                       
           ],
           

          ['$sort' =>['created_at'=>-1]]
            
        ]);
    })->toArray();
      // dd($InboundPages_result);
    $rawdata=[];$data=[];
    foreach ($InboundPages_result as  $key => $row) {
      $id='';$page_name_new='';$full_page_name='';$imgurl='';
      if(isset($row['id'])) $id = $row['id'];
      if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
      if(isset($row['page_name'])) $page_name = $row['page_name'];
      if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

      // if(is_numeric($page_name))
      // {
      //   //find same name of page name by matching mongo page id and mysql page name
      //    $input = preg_quote($page_name, '~'); // don't forget to quote input string!
      //    $keys = preg_grep('~' . $input . '~', $otherpage);
      //    $vals = array();
      //       foreach ( $keys as $key )
      //       {
      //          $page_name= $key;
      //       }
           
      // }
      $rawdata[] =[
        'id' => $id,
        'imgurl' => $imgurl,
        'page_name' =>$full_page_name,
        'default_page'=>$default_page
        ];

    }

 // dd($rawdata);
    // if pages  are not renamed , full page name will be shown
    if($pages_name <> ''){
      // dd("dhdd");
    foreach($rawdata as  $val)
    {
      $pg = $val['page_name'];
      $rename = DB::table('page_rename')->where('origin_name',$pg)->get()->toArray();
     
      foreach($rename as  $value) 
        {
          $custom_name = $value->custom_name;
          $data[] =[
            'id' => $val['id'],
            'imgurl' => $val['imgurl'],
            'page_name' => $custom_name,
            'default_page' => $val['default_page']
          ];
        }
    }
  }
  // dd($data);
  usort($data, function($a, $b) {
    return strcmp($a["page_name"], $b["page_name"]);
});
// dd($data);
      echo json_encode($data);
    }

    public function getGroupName()
    {
      // called by pages blade
      $brand_id=Input::get('brand_id');
      $company_id=Input::get('company_id');
      $default_page='';
      $otherpage=[];
      $pages_name = '';
      // $project_data_id = $this->getProjectByid($brand_id);
      // $project_data_id = $this->getProjectByid_fromCmpInfo($company_id);
      // dd($project_data_id);
      

      // if(count($project_data_id)>0)
      // {
        // // dd("hey");
        // $monitor=$project_data_id[0]['monitor_pages'];
        // $pages_name=$project_data_id[0]['pages_name'];
        // // dd($monitor);
        // $monitor=explode(',', $monitor);
        // // dd($monitor);
        // $otherpage=$monitor;

         // $username = auth()->user()->username;
         // $default_page = User::select(['custom_page'])->where('username',$username)->get()->toArray();
         // dd($default_page);
         // foreach($default_page as $pg) $default_page = $pg['custom_page'];
         // $default_page=$this->Format_Page_name_single($default_page);
        $default_page = 'We Love စားမယ်သွားမယ်';
      // }
      // $mongo_page = $this->Format_Page_Name($otherpage);
       // dd($mongo_page);
    //   $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

    //     return $collection->aggregate([
    //         [
    //         '$match' =>[
    //              '$and'=> [ 
    //              ['page_name'=>['$in'=>$mongo_page]],
                
    //                       ]
    //         ]  
                       
    //        ],
           

    //       ['$sort' =>['created_at'=>-1]]
            
    //     ]);
    // })->toArray();

      // dd($InboundPages_result);
        $res = DB::select("select Distinct group_name From temp_group_posts");
        // dd($res);
    $rawdata=[];$data=[];
    foreach ($res as  $key => $row) {
    
      // $id='';$page_name_new='';$full_page_name='';$imgurl='';
      // if(isset($row['id'])) $id = $row->temp_id;
      // if(isset($row['imageurl'])) $imgurl = $row['imageurl'];
      $group_name = $row->group_name;
      // if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

      // if(is_numeric($page_name))
      // {
      //   //find same name of page name by matching mongo page id and mysql page name
      //    $input = preg_quote($page_name, '~'); // don't forget to quote input string!
      //    $keys = preg_grep('~' . $input . '~', $otherpage);
      //    $vals = array();
      //       foreach ( $keys as $key )
      //       {
      //          $page_name= $key;
      //       }
           
      // }
      // dd($group_name);
      $rawdata[] =[
        // 'id' => $id,
        // 'imgurl' => $imgurl,
        'group_name' =>$group_name,
        'default_page'=>$default_page
        ];

    }
    $data  = $rawdata;

 // dd($data);
    // if pages  are not renamed , full page name will be shown
  //   if($pages_name <> ''){
  //     // dd("dhdd");
  //   foreach($rawdata as  $val)
  //   {
  //     $pg = $val['page_name'];
  //     $rename = DB::table('page_rename')->where('origin_name',$pg)->get()->toArray();
     
  //     foreach($rename as  $value) 
  //       {
  //         $custom_name = $value->custom_name;
  //         $data[] =[
  //           'id' => $val['id'],
  //           'imgurl' => $val['imgurl'],
  //           'page_name' => $custom_name,
  //           'default_page' => $val['default_page']
  //         ];
  //       }
  //   }
  // }
  // dd($data);
  usort($data, function($a, $b) {
    return strcmp($a["group_name"], $b["group_name"]);
});
// dd($data);
      echo json_encode($data);
    }



      public function insight()
    {
     
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Insight";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $companyData=$this->getCompanyData($pid);
                 if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $project_data_exclude = $this->getProjectByExcludeid($pid);
           
            if($source === 'out')
                return view('insight',compact('title','project_data','count','project_name','permission_data','project_data_exclude','pid','source','companyData'));
            else
              return view('insight_in',compact('title','project_data','count','project_name','permission_data','project_data_exclude','pid','source','companyData'));
  
            }
              else
            {
              /*return 404*/
                 return abort(404);
            }

          }
          else
          {
            /*return 404*/
               return abort(404);
          }
        
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }
      public function insight_facebook()
    {
     
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $title="Insight";
            $source=Input::get('source');
            $project_data_id = $this->getProjectByid($pid);
            $permission_data = $this->getPermission(); 
            $companyData=$this->getCompanyData($pid);
                 if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $ownpage=$project_data_id[0]['own_pages'];
            $ownpage=explode(',', $ownpage);
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $project_data_exclude = $this->getProjectByExcludeid($pid);
           
          
              return view('insight_facebook',compact('title','project_data','count','project_name','permission_data','project_data_exclude','pid','source','ownpage','companyData'));
  
            }
              else
            {
              /*return 404*/
                 return abort(404);
            }
          }
          else
          {
            /*return 404*/
               return abort(404);
          }
        
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }

     public function monitor()
    {
        if($user=Auth::user())
        {
          $title="Monitor-Alert";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
         
         
          return view('monitorAlert',compact('title','project_data','count'));
        }
    }
       public function brandList()
    {
      // return view('brandlist');
       
        if($user=Auth::user())
        {
          $pid ='';
          $companyData=[];
          $source =Input::get('source');
          $title="Projects";

          $login_user = auth()->user()->id;
          $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
          foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          $projects = Project::all();
          $permission_data = $this->getPermission(); 

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';


          return view('brandlist',compact('title','project_data','permission_data','count','test','source','companyData','action_permission','accountPermission'));
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }

       public function keywordGroup()
    {
      
        if($user=Auth::user())
        {
         $pid=Input::get("pid");
         $checked_data = $this->checkUserForProject($pid);

          $login_user = auth()->user()->id;
          $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
          foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

          if(count($checked_data)>0)
          {
          $companyData=[];
          $source =Input::get('source');
          $title="Projects";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $companyData=$this->getCompanyData($pid);
          $projects = Project::all();
          $permission_data = $this->getPermission(); 
          $accountPermission = '';
          return view('keyword_group',compact('title','project_data','permission_data','count','test','source','companyData','pid','action_permission','accountPermission'));
          }
          else
          {
            
               return abort(404);
          }
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }
     public function OtherkeywordGroup()
    {
      // dd(Input::get('pid'));
        if($user=Auth::user())
        {
         $pid=Input::get("pid");
         $checked_data = $this->checkUserForProject($pid);

          $login_user = auth()->user()->id;
          $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
          foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

          if(count($checked_data)>0)
          {
          $companyData=[];
          $source =Input::get('source');
          $title="Projects";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $companyData=$this->getCompanyData($pid);
          $projects = Project::all();
          $permission_data = $this->getPermission(); 
          
          $accountPermission = '';
          return view('other_keyword_group',compact('title','project_data','permission_data','count','test','source','companyData','pid','action_permission','accountPermission'));
          }
          else
          {
            
               return abort(404);
          }
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }

      public function RptSentiPredict()
    {
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
          $source =Input::get('source');
          $title="Predict Statistic";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $companyData=$this->getCompanyData($pid);
          
          $projects = Project::all();
          /* print_r($projects);
           return;*/
          $permission_data = $this->getPermission(); 

          $login_user = auth()->user()->id;
          $accountPermission = '';
          $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
          foreach($UserPermission as $up) $isDemo = $up->demo;
          if($isDemo == 2) $accountPermission = 'Demo';
          return view('reports.sentipredictcondition',compact('title','project_data','permission_data','count','source','companyData','accountPermission'));
          }
          else
          {
            return abort(404);
          }
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }
    public function RptHumanPredict()
    {
      // return view('brandlist');
         $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $source =Input::get('source');
            $title="Human Predict";
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $companyData=$this->getCompanyData($pid);
            
            $projects = Project::all();
            /* print_r($projects);
             return;*/
            $permission_data = $this->getPermission();
            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

            return view('reports.humanpreditrecords',compact('title','project_data','permission_data','count','source','companyData','accountPermission'));
          }
          else
          {
            return abort(404);
          }
        
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }
    public function RptPostExport()
    {
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $source =Input::get('source');
            $title="Post Data";
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $permission_data = $this->getPermission(); 
            $project_data_id = $this->getProjectByid($pid);
            $companyData=$this->getCompanyData($pid);
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitor=$project_data_id[0]['monitor_pages'];
            $monitor=explode(',', $monitor);
            
            }
            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

            return view('reports.postDataExport',compact('title','project_data','permission_data','monitor','count','source','companyData','accountPermission'));
          }
          else
          {
            /*return 404*/
               return abort(404);
          }
         
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }
    public function RptAnalysis()
    {
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $source =Input::get('source');
            $title="Post Data";
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $permission_data = $this->getPermission(); 
            $project_data_id = $this->getProjectByid($pid);
            $companyData=$this->getCompanyData($pid);
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitor=$project_data_id[0]['monitor_pages'];
            $monitor=explode(',', $monitor);
            
            }
            
            $keywordgrouplist = ProjectKeyword::select(['group_name','id','project_id','created_at'])->where('project_id',$pid)->groupBy('group_name')->orderBy('id')->get()->toArray();
            $default_name = Project::select(['default_keyword'])->where('id',$pid)->get()->toArray();

            $default_name = $default_name[0]['default_keyword'];

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

           
            return view('reports.analysisReport',compact('title','project_data','permission_data','monitor','count','source','companyData','keywordgrouplist','default_name','accountPermission'));
          }
          else
          {
           return abort(404); 
          }
         
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }
    public function RptPagesAnalysis()
    {
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $source =Input::get('source');
            $title="Post Data";
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $permission_data = $this->getPermission(); 
            $project_data_id = $this->getProjectByid($pid);
            $companyData=$this->getCompanyData($pid);
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitor=$project_data_id[0]['monitor_pages'];
            $monitor=explode(',', $monitor);
            
            }
            
            $pages =  $this->getInboundPages($pid);
            $pages = str_replace(array("'"), '',$pages);
            $pages=explode(',', $pages);
            // dd($pages);
            $default_page = Project::select(['default_page'])->where('id',$pid)->get()->toArray();

            $default_page = $default_page[0]['default_page'];

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
           
            return view('reports.pagesAnalysisReport',compact('title','project_data','permission_data','monitor','count','source','companyData','pages','default_page','accountPermission'));
          }
          else
          {
           return abort(404); 
          }
         
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }

    public function getCronStatus()
    {
                     $pid =Input::get('pid');
        $data_down_status = $this->check_cron_finish($pid);
        return $data_down_status;
    }

    public function showPicture()
    {

      $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
            $source =Input::get('source');
            $title="Post Data";
            $project_data = $this->getProject();
            $count = $this->getProjectCount($project_data);
            $permission_data = $this->getPermission(); 
            $project_data_id = $this->getProjectByid($pid);
            $companyData=$this->getCompanyData($pid);
            if(count($project_data_id)>0)
            {
            $project_name = $project_data_id[0]['name'];
            $monitor=$project_data_id[0]['monitor_pages'];
            $monitor=explode(',', $monitor);
            
            }
            
            $pages =  $this->getInboundPages($pid);
            $pages = str_replace(array("'"), '',$pages);
            $pages=explode(',', $pages);
            // dd($pages);
            $default_page = Project::select(['default_page'])->where('id',$pid)->get()->toArray();

            $default_page = $default_page[0]['default_page'];

            $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';
           
            return view('edit_picture',compact('title','project_data','permission_data','monitor','count','source','companyData','pages','default_page','accountPermission'));
          }
          else
          {
           return abort(404); 
          }
         
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }

    public function storePicture(Request $request)
    {

      $table = $request->get('schedule');
      $full_picture = $request->get('full_link');
      $local_picture = $request->get('local_link');

      $hasLink = DB::table($table)->where('full_picture',$full_picture)->count();

      if($hasLink > 0){

        $upatePost = DB::table($table)
                    ->where('full_picture',$full_picture)
                    ->update(['full_picture' => $local_picture, 'local_picture' => $local_picture ]);

        return Redirect::back()->with('success-message', 'Successfully Updated Link!'); 

      }else{

        return Redirect::back()->with('error-message', 'does not exit full link!');

      }

    }

        
}

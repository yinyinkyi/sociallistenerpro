<?php

namespace BIMiner\Http\Controllers;

use BIMiner\MongodbData;
use BIMiner\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use Yajra\Datatables\Datatables;
use BIMiner\Project;
use BIMiner\InboundPages;
use BIMiner\MongoFollowers;
class CampaignController extends Controller
{
    //
    use GlobalController;

    public function campaignlist()
    {
      // dd("kjkj"); 
      if($user=Auth::user())
        {
          $pid ='';
          $companyData=[];
          $source =Input::get('source');
          // dd($source);
          $title="Campaign";

          $login_user = auth()->user()->id;
          $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
          foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

          $project_data = $this->getProject();
          // foreach($project_data as $project)
          // {
          //  $monitor_pages =  $project->monitor_pages;
          //  $pages =explode(',', $monitor_pages);
          //  // dd($pages);
          // }
          // dd($pages);
          $count = $this->getProjectCount($project_data);
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          // $projects = Project::all();
          $permission_data = $this->getPermission(); 

          $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

          return view('campaignlist',compact('title','project_data','permission_data','count','test','source','companyData','action_permission','accountPermission'));
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
      
    }
    public function audience(Request $request)
    {
      // dd("hye");
       if($user=Auth::user())
        {
          // $request->validate(['percent' => 'required|numeric|min:1',]);

          $pid ='';
          $companyData=[];
          $source =Input::get('source');
          // dd($source);
          $title="Campaign";

          $login_user = auth()->user()->id;
          $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
          foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

          $project_data = $this->getProject();
          // foreach($project_data as $project)
          // {
          //  $monitor_pages =  $project->monitor_pages;
          //  $pages =explode(',', $monitor_pages);
          //  // dd($pages);
          // }
          // dd($pages);
          $count = $this->getProjectCount($project_data);
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          // $projects = Project::all();
          $permission_data = $this->getPermission(); 

          $login_user = auth()->user()->id;
            $accountPermission = '';
            $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
            foreach($UserPermission as $up) $isDemo = $up->demo;
            if($isDemo == 2) $accountPermission = 'Demo';

          return view('audience_entry',compact('title','project_data','permission_data','count','test','source','companyData','action_permission','accountPermission'));
        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }
    }
    public function audience_store()
    {
            // dd('you pass');
    }
    public function get_campaignList()
    {

        $source = Input::get('source');
        // dd($source);
        $source = 'in';
        $res = DB::table('campaignGroup')->select('*')->where('brand_id',auth()->user()->brand_id)->get();
        // dd($res);
        return Datatables::of($res)
        ->addColumn('action' ,function ($res) use($source) {
         $html ='<a href="campaign/edit/'.$res->id.'?source='.$source.'" class="btn btn-xs btn-primary"><i class="mdi mdi-table-edit"></i> Edit</a> 
            <button class="btn btn-xs btn-danger btn-delete" data-remote="' . route('campaign/delete', $res->id) . '"><i class="mdi mdi-delete"></i>Delete</button>';
            return $html;
            
        })
        ->rawColumns(['action'])
        ->make(true);
    }
  public function getCampaignSentiDetail()
    {
      $year = Input::get('fday');
      // dd(Input::get('campaign_name'));
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
     
      if(null !== Input::get('fday'))
      {
       $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
       $dateBegin=date('Y-m-d');
      }

      if(null !==Input::get('sday'))
      {
        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      else
      {
        $dateEnd=date('Y-m-d');
      }
        // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
        // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));

         
         $group_period ="";
         $group_period_post ="";
         $group_period_cmt ="";
         // $group_period_incmt = "";
         $group_period_inpost = "";
         $add_con="";
         $add_comment_con="";
        if($groupType=="month")
        {
          $group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
          $group_period_post =" DATE_FORMAT(posts.created_time, '%Y-%m')"; 
          $group_period_article =" DATE_FORMAT(article.created_time, '%Y-%m')"; 
          $group_period_cmt =" DATE_FORMAT(cmts.created_time, '%Y-%m')"; 
          // $group_period_incmt = " DATE_FORMAT(incmt.created_time, '%Y-%m')"; 
          $group_period_inpost = " DATE_FORMAT(inpost.created_time, '%Y-%m')"; 

         }

         else 
         {
          $group_period =" DATE_FORMAT(created_time, '%Y-%m-%d')";
          $group_period_post =" DATE_FORMAT(posts.created_time, '%Y-%m')"; 
          $group_period_cmt =" DATE_FORMAT(cmts.created_time, '%Y-%m')";
          $group_period_article =" DATE_FORMAT(article.created_time, '%Y-%m')"; 
          // $group_period_incmt =" DATE_FORMAT(incmt.created_time, '%Y-%m')"; 
          $group_period_inpost =" DATE_FORMAT(inpost.created_time, '%Y-%m')";    

         }

        $add_inbound_con ="";
        $inboundpages=$this->getInboundPages($brand_id);

        if ($inboundpages !== '')
        {
          $add_inbound_con = " AND (page_name  not in (".$inboundpages.") or page_name is NULL)";
        }

        $comment_key_con=" AND 1=1 ";
        $post_key_con=" AND 1=1 ";
        $article_key_con = " AND 1=1 ";
        $incmt_key_con=" AND 1=1 ";
        $inpost_key_con = ' AND 1=1';

      $campaign_keyword = '' ;
      if(null !== Input::get('campaign_id'))
      {
        // dd("hdfd");
        $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
        // dd($campaign_keyword);
        $comment_key_con .=  " AND ( " .$this->getcampaignfilter_MYSQL($campaign_keyword,'cmts'). ")";
        $post_key_con .=  " AND ( " .$this->getcampaignfilter_MYSQL($campaign_keyword,'posts'). ")";
        // dd($post_key_con);
        $article_key_con .=  " AND ( " .$this->getcampaignfilter_MYSQL($campaign_keyword,'article'). ")";
        $incmt_key_con .=  " AND ( " .$this->getcampaignfilter_MYSQL($campaign_keyword,'incmt'). ")";
        $inpost_key_con .=  " AND ( " .$this->getcampaignfilter_MYSQL($campaign_keyword,'inpost'). ")";
        //$campaign_filter_con = " and ".$campaign_filter_con;

        // dd($campaign_filter_con);
      }

      else
      {

       $comment_key_con .= ' AND 1=2';
       $post_key_con .= ' AND 1=2';
       $article_key_con = ' AND 1=2 ';
       $incmt_key_con .= ' AND 1=2';
       $inpost_key_con .= ' AND 1=2';

      }
      // dd(Input::get('campaign_id'));
     // dd($post_key_con);
     
        if($groupType == 'null')
        {
          /*$query =" SELECT  sum(positive) positive,sum(negative) negative From (SELECT COALESCE(sum(IF(posts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(posts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(posts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(posts.checked_sentiment = 'neg', 1, 0)),0) negative ".
          "FROM temp_posts posts WHERE posts.isHide=0 and  ".
          "  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con . $post_key_con. " AND posts.campaign=".Input::get('campaign_id') .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(article.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(article.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(article.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(article.checked_sentiment = 'neg', 1, 0)),0) negative ".
          "FROM temp_web_mentions article WHERE article.isHide=0 and  ".
          "  (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con . $article_key_con.  " AND article.campaign= ".Input::get('campaign_id') .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(inpost.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(inpost.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(inpost.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(inpost.checked_sentiment = 'neg', 1, 0)),0) negative ".
          "FROM temp_inbound_posts inpost WHERE  ".
          "  (DATE(inpost.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $inpost_key_con.
          // " UNION ALL ".
          // "SELECT COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(incmt.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(incmt.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(incmt.checked_sentiment = 'neg', 1, 0)),0) negative ".
          // "FROM temp_inbound_comments incmt WHERE  ".
          // "  (DATE(incmt.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $incmt_key_con.
           " ) T1 ";*/

            $query =" SELECT  sum(positive) positive,sum(negative) negative From (SELECT COALESCE(sum(IF(posts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(posts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(posts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(posts.checked_sentiment = 'neg', 1, 0)),0) negative ".
          "FROM temp_posts posts WHERE posts.isHide=0 AND YEAR(DATE(posts.created_time)) = '" .$year. "'". $add_inbound_con . $post_key_con. " AND posts.campaign=".Input::get('campaign_id') .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(article.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(article.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(article.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(article.checked_sentiment = 'neg', 1, 0)),0) negative ".
          "FROM temp_web_mentions article WHERE article.isHide=0 AND YEAR(DATE(article.created_time)) = '" .$year. "'". $add_inbound_con . $article_key_con.  " AND article.campaign= ".Input::get('campaign_id') .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(inpost.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(inpost.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(inpost.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(inpost.checked_sentiment = 'neg', 1, 0)),0) negative ".
          "FROM temp_inbound_posts inpost WHERE  YEAR(DATE(inpost.created_time)) = '" .$year. "'" .  $inpost_key_con .
          // " UNION ALL ".
          // "SELECT COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(incmt.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(incmt.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(incmt.checked_sentiment = 'neg', 1, 0)),0) negative ".
          // "FROM temp_inbound_comments incmt WHERE  ".
          // "  (DATE(incmt.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $incmt_key_con.
           " ) T1 ";
           // return $query;

        }
        else
        {
         /*$query =" SELECT  sum(positive) positive,sum(negative) negative,created_time From (SELECT COALESCE(sum(IF(posts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(posts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(posts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(posts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period_post ." created_time  ".
          "FROM temp_posts posts WHERE posts.isHide=0 and  ".
          "  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con . $post_key_con. " AND posts.campaign=".Input::get('campaign_id') .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(article.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(article.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(article.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(article.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period_article ." created_time  ".
          "FROM temp_web_mentions article WHERE article.isHide=0 and  ".
          "  (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $add_inbound_con . $article_key_con. " AND article.campaign= ".Input::get('campaign_id') .
          // " UNION ALL ".
          // "SELECT COALESCE(sum(IF(inpost.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(inpost.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(inpost.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(inpost.checked_sentiment = 'neg', 1, 0)),0) negative, ".$group_period_inpost ." created_time  ".
          // "FROM temp_inbound_posts inpost WHERE inpost.campaign is not null and  ".
          // "  (DATE(inpost.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $inpost_key_con.
          // " UNION ALL ".
          // "SELECT COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(incmt.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(incmt.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(incmt.checked_sentiment = 'neg', 1, 0)),0) negative, ".$group_period_incmt ." created_time  ".
          // "FROM temp_inbound_comments incmt WHERE   ".
          // "  (DATE(incmt.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $incmt_key_con.
          // " GROUP BY ".$group_period . 
          " ) T1 Group By created_time";*/

          $query =" SELECT  sum(positive) positive,sum(negative) negative,created_time From (SELECT COALESCE(sum(IF(posts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(posts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(posts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(posts.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period_post ." created_time  ".
          "FROM temp_posts posts WHERE posts.isHide=0 AND YEAR(DATE(posts.created_time)) = '" .$year. "'"
          . $add_inbound_con . $post_key_con. " AND posts.campaign=".Input::get('campaign_id') .
          " UNION ALL ".
          "SELECT COALESCE(sum(IF(article.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(article.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(article.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(article.checked_sentiment = 'neg', 1, 0)),0) negative,".$group_period_article ." created_time  ".
          "FROM temp_web_mentions article WHERE article.isHide=0 AND YEAR(DATE(article.created_time)) = '" .$year. "'"
          . $add_inbound_con . $article_key_con. " AND article.campaign= ".Input::get('campaign_id') .
          // " UNION ALL ".
          // "SELECT COALESCE(sum(IF(inpost.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(inpost.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(inpost.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(inpost.checked_sentiment = 'neg', 1, 0)),0) negative, ".$group_period_inpost ." created_time  ".
          // "FROM temp_inbound_posts inpost WHERE inpost.campaign is not null and  ".
          // "  (DATE(inpost.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $inpost_key_con.
          // " UNION ALL ".
          // "SELECT COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(incmt.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(incmt.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(incmt.checked_sentiment = 'neg', 1, 0)),0) negative, ".$group_period_incmt ." created_time  ".
          // "FROM temp_inbound_comments incmt WHERE   ".
          // "  (DATE(incmt.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .  $incmt_key_con.
          // " GROUP BY ".$group_period . 
          " ) T1 Group By created_time";
        }
         dd($query);

        $query_result = DB::select($query);
       //dd($query_result);
         //return;
           $data = [];
           $total=0;

           $data_sentiment = [];
           $data_mention = [];
           $total=0;
           $data_message = [];

            foreach ($query_result as  $key => $row) {
                  
                      $utcdatetime = $row->created_time;
                 
                       
                       $pos =$row->positive;
                       $neg =$row->negative;
                     //  $neutral =(int)$row->neutral + (int)$row->NA;
                               
                        $request['periodLabel'] ="";
              
                        if($groupType == "week")
        {
            $weekperiod=$this ->rangeWeek ($utcdatetime);
            $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
            
         if (!array_key_exists($periodLabel , $data_sentiment)) {
            $data_sentiment[$periodLabel] = array(
                        'periodLabel' => $periodLabel,
                        'positive' => $pos,
                        'negative' => $neg,
                        'periods' =>$utcdatetime,
                    );
        }
        else
        {
         $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
         $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
         $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
        }
         
                          


        }
         else
        {
         $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
         $data_sentiment[$utcdatetime]['positive'] = $pos;
         $data_sentiment[$utcdatetime]['negative'] = $neg;
         $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

        }

                        
           }

         //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
            usort($data_sentiment, function($a1, $a2) {
           $value1 = strtotime($a1['periods']);
           $value2 = strtotime($a2['periods']);
           return $value1 - $value2;
        });
            echo json_encode($data_sentiment);
    }
    
    public function Create()
    {
        if($user=Auth::user())
        {
          $title="Campaign";
          $companyData=[];
          $source = Input::get('source');
          $source ='in';
          //dd($source);
          $project_data = $this->getProject();
          foreach($project_data as $project)
          {
            $monitor_pages = $project->monitor_pages;
            $pages  = explode(',', $monitor_pages);
          }
          // dd($pages);

          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          $accountPermission = '';
          return view('campaign_create',compact('title','project_data','permission_data','count','source','companyData','pages','accountPermission'));
        }
    }
    public function Store()
    {
      if($user=Auth::user())
      {
          $brand_id = auth()->user()->brand_id;
          $kw_str='';
          $name = Input::post('campaign_name');
          // dd($name);
          $page_name = Input::post('page_name');
          $source = Input::post('source');
          if(null !== Input::post('keywords'))
          {
          $keywords = Input::post('keywords');
          // $kw_str = implode($keywords,',');
          }
          $id = DB::table('campaignGroup')->insertGetId(['name' => $name , 'brand_id' => $brand_id,'page_name' => $page_name,'created_at' => now()->toDateTimeString(),'updated_at' => now()->toDateTimeString()]);
            foreach($keywords as $kw)
            {
              $result = DB::table('campaignGroupList')->insert(['campaignGroup_id' => $id,'campaign_keyword' => $kw,'created_at' => now()->toDateTimeString(),'updated_at' => now()->toDateTimeString()]);
            }

            $check_cron_log="SELECT id from cron_log  WHERE brand_id=".$brand_id." AND cron_type='sync_mention' ";
            $check_cron_log = DB::select($check_cron_log);
             if( count($check_cron_log) > 0 )
            {
                  DB::table('cron_log')
                  ->where('brand_id', $brand_id)
                  ->update(['finish_status' =>0]);
            }
            else
            {
               cron_log::create([
                  'brand_id' =>$brand_id,
                  'finish_status' => 0,
                  'cron_type' =>'sync_mention',
   
              ]);
            }
      }
      else
      {

          return abort(404);
        
      }
    }
    public function Edit($id)
    {
       $keywords=[];
       $source = Input::get('source');
       $source ='in';
       $project_data = $this->getProject();
       $count = $this->getProjectCount($project_data);
       if(count($project_data)>0)
      {
      $pid = $project_data[0]['id'];
      $companyData=$this->getCompanyData($pid);
      }

       $title = "Campaign";
        foreach($project_data as $project)
        {
          $monitor_pages = $project->monitor_pages;
          $pages  = explode(',', $monitor_pages);
        }

          
       $campaign = DB::table('campaignGroup')->select('*')->where('id',$id)->where('brand_id',auth()->user()->brand_id)->get();
       foreach ($campaign as $key => $value) {
         # code...
        $campaign_name = $value->name;
        $id = $value->id;
        $page_name =$value->page_name;
       }
        $campaign_keywords = DB::table('campaignGroupList')->select('*')->where('campaignGroup_id',$id)->get();
        foreach($campaign_keywords as $value)
        {
          $keywords[] = $value->campaign_keyword;
        }
        $kw_count = count($keywords);
          $accountPermission = '';

      return view('campaign_edit',compact('campaign_name','companyData','page_name','keywords','pages','id','source','count','title','project_data','accountPermission'));

    }
    public function Update()
    {

        $brand_id = auth()->user()->brand_id;
        $id = Input::post('id');
        $keywords = [];
        $name = Input::post('campaign_name');
        $page_name = Input::post('page_name');
        $source = Input::post('source');
        if(null !== Input::post('keywords'))
        {
        $keywords = Input::post('keywords');
       
        }
        
        $res = DB::table('campaignGroup')->where('id',$id)->update(['name' => $name , 'brand_id' => $brand_id,'page_name' => $page_name,'updated_at' => now()->toDateTimeString()]);
       
         DB::table('campaignGroupList')->where('campaignGroup_id',$id)->delete();
          foreach($keywords as $kw)
          {
            $result = DB::table('campaignGroupList')->insert(['campaignGroup_id'=>$id,'campaign_keyword' => $kw,'updated_at' => now()->toDateTimeString()]);
          }
          $check_cron_log="SELECT id from cron_log  WHERE brand_id=".$brand_id." AND cron_type='sync_mention' ";
           $check_cron_log = DB::select($check_cron_log);

             if( count($check_cron_log) > 0 )
            {
                  DB::table('cron_log')
                  ->where('brand_id', $brand_id)
                  ->update(['finish_status' =>0]);
            }
            else
            {
               cron_log::create([
                  'brand_id' =>$brand_id,
                  'finish_status' => 0,
                  'cron_type' =>'sync_mention',
   
              ]);
            }
        
      
   
    }
    public function delete($id)
    {

      DB::table('campaignGroup')->where('id',$id)->delete();
      DB::table('campaignGroupList')->where('campaignGroup_id',$id)->delete();
      DB::select("Update temp_inbound_posts set campaign=NULL where campaign=".$id);
      DB::select("Update temp_posts set campaign=NULL where campaign=".$id);
      DB::select("Update temp_web_mentions set campaign=NULL where campaign=".$id);
      
      // DB::table('temp_inbound_posts')->where('campaign',$id)->update(['campaign',NULL]);
      // DB::table('temp_posts')->where('campaign',$id)->update(['campaign',NULL]);
      // DB::table('temp_web_mentions')->where('campaign',$id)->update(['campaign',NULL]);

      // dd($id);


    }
    public function getRelatedCampaign_comment()
    {
      // dd(Input::get('tsearch_senti_arr'));
      $brand_id=Input::get('brand_id');
      $post_id_filter = '';
      if(null !== Input::get('post_id')){
      $post_id=Input::get('post_id');
      $post_id_filter = " and cmts.post_id = '".$post_id."'";
    }

      $campaign_filter_con = '';
      $campaign_keyword=[];
      if(null !== Input::get('campaign_id'))
      {
        $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
        $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
         // $campaign_filter_con = " and ".$campaign_filter_con;

        // dd($campaign_filter_con);
      }
               $period_type = '';
         $page_id='';
        $additional_filter=" AND 1=1 ";
         $filter_keyword ='';
          if(null !== Input::get('keyword'))
           $filter_keyword=Input::get('keyword'); 
      
            if(null !==Input::get('period'))
            {
           $period=Input::get('period');
            $char_count = substr_count($period,"-");
            $format_type = '';
            if($char_count == 1)
            {
                $format_type ='%Y-%m';
            }
            else if($char_count == 2)
            {
                 $format_type ='%Y-%m-%d';
            }   
            else if($char_count > 2)
            {
                /*this is week*/
                $pieces = explode(" - ", $period);
                $format_type ='%Y-%m-%d';
                $period_type ='week';
            }

          
    if($period_type == 'week')
    {
     $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

    }
    else
    {
        $additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
    }

           }
                    if(null !== Input::get('fday'))
              {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
              }
              else
              {
              $dateBegin=date('Y-m-d');
              }
            

            if(null !==Input::get('sday'))
            {

         $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
            
            else
            {
        $dateEnd=date('Y-m-d');

           }
      $add_global_filter='';
  
       if(null !== Input::get('tsearch_tag'))
      {
        $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'));
      }
      if(null !== Input::get('tsearch_senti_arr'))
      {
        $add_global_filter .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti_arr'),'cmts');
      }
       if(null !== Input::get('tsearch_keyword'))
      {
        $add_global_filter .=" and " . $this->getKeywordWhereGen(Input::get('tsearch_keyword'),'cmts');
      }
            if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = ' 1=1 ';
      }
      else
      {
        $date_filter=" (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
      }

       $add_con=" and 1=1 ";
         $add_comment_con="";

        if($filter_keyword !== '')
        {
          $add_con .= " and cmts.message Like '%".$filter_keyword."%'";
        }
        if(null !== Input::get('tag_id'))
        $add_con .= " and " . $this->getTagWhereGen(Input::get('tag_id')); 


      //        $query = " SELECT checked_tags_id  tags " .
      //  "FROM temp_inbound_comments cmts INNER JOIN temp_inbound_posts posts ".
      //   " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND cmts.tag_flag=1 AND checked_tags_id<>'' ".
      // " AND " . $date_filter . $campaign_filter_con . $add_con  .  $keyword_con . $additional_filter;

       $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,pros.id profileID,pros.type as profileType,pros.name as profileName,cmts.checked_tags_id tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
            " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,action_status,action_taken ".
            " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
            " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
            " WHERE posts.id IS NOT NULL".
            " and  parent='' AND tag_flag=1 AND  checked_tags_id <>'' AND ".$date_filter.  $add_con . $campaign_filter_con . "  " . $add_global_filter  .
            $additional_filter  . $post_id_filter .

            " ORDER by timestamp(cmts.created_time) DESC ";
            // dd($query);
               $data = DB::select($query);
               // dd($data);
               $tags =$this->gettagsBybrandid($brand_id);
               $permission_data = $this->getPermission(); 
               $project = Project::find($brand_id);
               $monitor_pages = $project->monitor_pages;
               $monitor_pages = explode(',', $monitor_pages);

                $login_user = auth()->user()->id;
                $username = auth()->user()->username;
                 $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
                 foreach($userdata as $action_permission) $action_permission = $action_permission->edit;
              // dd($data);
         return Datatables::of($data)
              
             ->addColumn('post_div', function ($data)   use($tags,$permission_data,$monitor_pages ,$username,$action_permission,$campaign_keyword) {
         
                $page_name=$data->page_name ;
                if(is_numeric($page_name))
                {
                   $input = preg_quote($page_name, '~'); // don't forget to quote input string!
                   $keys = preg_grep('~' . $input . '~', $monitor_pages);
                   $vals = array();
                      foreach ( $keys as $key )
                      {
                         $page_name= $key;
                      }
                     
                }


                        $message = $data->message;
                        $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
                        $message = $this->highLightText($campaign_keyword,$message);
     



              //generate comment link
              $profilePhoto='';$ProfileName='';
              $profileLink='javascript:void(0)';
              $fb_p_id=explode('_', $data->post_id);
              $fb_p_id=$fb_p_id[1];
              $fb_c_id=explode('_', $data->id);
              $fb_c_id=$fb_c_id[1];
                if($data->profileName <> '') $ProfileName=$data->profileName; 
                else if($data->profileID <> '')  $ProfileName=$data->profileID;
                else  $ProfileName=$page_name;

              if($data->profileType === 'number')
              {
                $profilePhoto='https://graph.facebook.com/'.$data->profileID.'/picture?type=large';
                $ProfileName=$page_name;
              }
              
              if(isset($data->profileID))
              $profileLink='https://www.facebook.com/'.$data->profileID;

              $page_id='';
              $arr_page_id=explode("-",$page_name);
              if(count($arr_page_id)>0)
              {
                $last_index=$arr_page_id[count($arr_page_id)-1];
                if(is_numeric($last_index))
                $page_id = $last_index;
                else
                $page_id = $page_name;
              }
              else
              {
                $page_id=$page_name;
              }

              $comment_Link= 'https://www.facebook.com/'.$fb_c_id;
              $classforbtn='';$stylefora='';$bgcolor='';
                    if($data->sentiment=='neg')
                      {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                    else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                      else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
                      $html = '<div class="profiletimeline"><div class="sl-item">';
                      if($profilePhoto<>'')
                      $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                      else
                      /*$html .= '<div class="sl-left user-img"> <span class="round"><i class="mdi mdi-comment-account"></i></span> </div>';*/
                    $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                     
                       $html .='<div class="form-material sl-right"><div> <div>
                    <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a>';
               
                    if($data->hp <> '' && $data->hp_tags <>'')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    else if($data->hp <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                    else if($data->hp_tags <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                    $html .='<span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                     // $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                    
                     $html.='<p>Sentiment : <select id="sentiment_'.$data->id.'"  class="form-material form-control in-comment-select edit_senti ';
                     if($data->sentiment=="")$html.='"><option value="" selected="selected" ></option>';
                     if($data->sentiment=="pos")$html.='text-success" >';
                     if($data->sentiment=="neg")$html.='text-red" >';
                     if($data->sentiment=="neutral" || $data->sentiment == "NA")$html.='text-warning" >';
                  
                      $html.='<option value="pos" class="text-success"';
                      if($data->sentiment == "pos")
                      $html.= 'selected="selected"';
                      $html.= '>Positive</option><option value="neg" class="text-red"';
                      if($data->sentiment == "neg")
                      $html.= 'selected="selected"';
                      $html.= '>Negative</option><option value="neutral" class="text-warning"';
                      if($data->sentiment == "neutral" || $data->sentiment == "NA")
                      $html.= 'selected="selected"';      
                      $html.= '>Neutral</option></select>';

                     
                      $html.=' | Tag :<select id="tags_'.$data->id.'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                       $tag_array=explode(',', $data->tags);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';

                      if($action_permission == 0){
                      $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                     $html.='<a class="show_alert" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id=""><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                     }
                     else{

                      if($permission_data['edit'] === 1)
                      $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                     $html.='<a class="get-code" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id="add_tag"><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                   }
                      
                      $html.='</p><p class="m-t-10">'.$message.'</p>'.'</div>';
                       if($data->cmtType =='sticker' || $data->cmtType =='photo' || $data->cmtType =='share' || $data->cmtType =='share_large_image' || $data->cmtType =='animated_image_share'  )
                       {
                          $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'.
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                              '<img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">'.
                      '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                       else if($data->cmtType =='video_inline' || $data->cmtType =='animated_image_video' || $data->cmtType =='video_share_youtube' || $data->cmtType =='video_share_highlighted')
                       {
                         $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'.$data->id.'">'.
                          '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                          '<iframe src="https://www.facebook.com/plugins/video.php?href='.$data->cmtLink.'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'.
                          '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                      $html.= '<div class="sl-right">';
                    
                        if((int)$data->cmtLiked > 0)
                        $html.='👍 '.$this->thousandsCurrencyFormat($data->cmtLiked);
                       if((int)$data->cmtLove > 0)
                        $html.=' ❤️ '.$this->thousandsCurrencyFormat($data->cmtLove);
                       if((int)$data->cmtHaha > 0)
                        $html.=' 😆 '.$this->thousandsCurrencyFormat($data->cmtHaha);
                       if((int)$data->cmtWow > 0)
                        $html.=' 😮 '.$this->thousandsCurrencyFormat($data->cmtWow);
                       if((int)$data->cmtSad > 0)
                        $html.=' 😪 '.$this->thousandsCurrencyFormat($data->cmtSad);
                       if((int)$data->cmtAngry > 0)
                        $html.=' 😡 '.$this->thousandsCurrencyFormat($data->cmtAngry);
                        $html.='</div>
                      <div class="sl-right" style="float: right;">
                    <div style="display: inline-block"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green popup_post" id="'.$data->post_id.'">See Post</a></div>
                     <div style="display: inline-block"><a href="'.$comment_Link.'" class="waves-effect waves-light btn-sm btn-gray" target="_blank">Take Action</a></div>
                    <div style="display: inline-block" class="btn-group">';
                    if($data->action_status === "Action Taken")
                    $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Action Taken
                   </button>';
                   else
                   $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Require Action
                   </button>';
                $html.='<div class="dropdown-menu">
                  <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Require Action</a>
                  <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Action Taken</a>

                </div></div></div></div></div>';

                      return $html;

                  })
        
            ->rawColumns(['post_div'])
         
            ->make(true);

        

    }
    public function getAllArticle_forCampaign()
    {

       $senti_con = '';

       $year = Input::get('fday');

      if(null !== Input::get('sentiType'))
      {
        $senti_con = " and posts.checked_sentiment='".Input::get('sentiType')."'";
      }

      $brand_id=Input::get('brand_id');
      if(null !== Input::get('fday'))
      {
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
        $dateBegin=date('Y-m-d');
      }

      if(null !==Input::get('sday'))
      {
        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      else
      {
        $dateEnd=date('Y-m-d');
      }

      $add_inbound_con ="";
      $inboundpages=$this->getInboundPages($brand_id);
      if($inboundpages !== '')
      {
        $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
      }

      $period_type = '';
      $additional_filter=" AND 1=1 ";
      if(null !==Input::get('period'))
      {
         $period=Input::get('period');
         $char_count = substr_count($period,"-");
         $format_type = '';
         if($char_count == 1)
         {
            $format_type ='%Y-%m';
         }
         else if($char_count == 2)
         {
            $format_type ='%Y-%m-%d';
         }   
          else if($char_count > 2)
          {
            $pieces = explode(" - ", $period);
            $format_type ='%Y-%m-%d';
            $period_type ='week';
          }
          if($period_type == 'week')
          {
            //$additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";  
            //$additional_filter .= " ";  

          }
          else
          {
            //$additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
            //$additional_filter .= " ";
          }
       }
          if(null !== Input::get('tsearch_senti'))
          {
           $additional_filter .= ' AND ' . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
          }
          if(null !== Input::get('tsearch_keyword'))
          {
           $additional_filter .= ' AND ' . $this->getKeywordWhereGen(Input::get('tsearch_keyword'),'posts');
          }
           //dd($additional_filter);

            // $keyword_data = $this->getprojectkeyword($brand_id);
        
          // if(isset($keyword_data [0]['main_keyword']))

          if(null !== Input::get('campaign_id'))
          {
            $campaign_id = Input::get('campaign_id') ;
          }
          else
          {
            $campaign_id = '' ;
          }
            $campaign_keyword=[];
            $campaign_filter_con = '';

            //dd($campaign_id);
            $campaign_keyword= $this->getCampaignKeywordFilter($campaign_id,$brand_id);
            // $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
            // $campaign_filter_con = " and ".$campaign_filter_con;


            //  $keyword_data = $this->getprojectkeyword($brand_id,$keyword_group);
            //  if(isset($keyword_data [0]['main_keyword']))
            //  $additional_filter  .=  " AND ( " . $this->getkeywordfilter_MYSQL($keyword_data,'posts') . ")"; 

            // else
            //  $additional_filter .= ' AND 1=2';
    
 
         if(null !== Input::get('page_name'))
         {
            $additional_filter .= ' AND posts.page_name="' .Input::get('page_name') . '" ';
         }
        $query='';
        $query_result=[];
        // dd($add_inbound_con);

        $query = "SELECT '".$brand_id."' brand_id,IF(posts.sentiment = '','~',posts.sentiment) old_senti,IF(posts.sentiment = posts.checked_sentiment,'','human predict!') hp,IF(posts.checked_sentiment <> '',posts.checked_sentiment,posts.sentiment) sentiment,posts.id,posts.title,posts.link,posts.message,posts.campaign,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %H:%i %p') created_time FROM temp_web_mentions posts  WHERE YEAR(DATE(posts.created_time)) = '" .$year. "' AND posts.campaign = '".Input::get('campaign_id')."' AND (posts.isHide = 0 or posts.isHide is null) ". 
         $additional_filter . $campaign_filter_con . $senti_con ." ORDER by timestamp(posts.created_time) DESC ";
        // $query = "SELECT '".$brand_id."' brand_id,IF(posts.sentiment = '','~',posts.sentiment) old_senti,IF(posts.sentiment = posts.checked_sentiment,'','human predict!') hp,IF(posts.checked_sentiment <> '',posts.checked_sentiment,posts.sentiment) sentiment,posts.id,posts.title,posts.link,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %H:%i %p') created_time FROM temp_web_mentions posts  WHERE  (posts.isHide = 0 or posts.isHide is null) AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".  $additional_filter . $campaign_filter_con . $senti_con ." ORDER by timestamp(posts.created_time) DESC ";
        $query_result = DB::select($query);

        $data = [];
        $data_mention=[];


       foreach ($query_result as  $key => $row) {
        //change page name from id to orginal user register name.
        // dd($row->message);
        $message=$this->readmore($row->message,2000);
        // dd($keyword_data); 
        $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
        $message = $this->highLightText($campaign_keyword,$message);
        // dd($message);
        $data[] = array(
            'id'     =>$row->id,
            'title' => $row->title,
            'message' => $message,
            'page_name' =>$row->page_name,
            'link' => $row->link,
            'created_time' =>$row->created_time,
            'sentiment' =>$row->sentiment,
            'hp' => $row->hp,
            'old_senti' => $row->old_senti,
            'brand_id' => $row->brand_id,
            'campaign' => $row->campaign,
          );
        }

             if(null !== Input::get('filter_overall'))
            {
              if( Input::get('filter_overall') === 'pos')//
              {     foreach ($data as $key => $value) {

        if( (int)$value['positive'] <  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'])
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
                }
              else if( Input::get('filter_overall') === 'neg')
              {
           foreach ($data as $key => $value) {

            if((int)$value['positive'] >  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'] &&  (int)$value['cmtneutral'] <> 0)
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }
               else if( Input::get('filter_overall') === 'neutral')
              {
                          foreach ($data as $key => $value) {

                       if( ! ( ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 ) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0)))
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }

            }

            $data_mention=$data;
           // dd($data_mention['title']);
            return Datatables::of($data_mention)
     
             ->addColumn('post', function ($data_mention){
              $fb_p_id=explode('_', $data_mention['id']);
              $fb_p_id=$fb_p_id[1];
              $page_id='';
              $arr_page_id=explode("-",$data_mention['page_name']);
              if(count($arr_page_id)>0)
              {
                $last_index=$arr_page_id[count($arr_page_id)-1];
                if(is_numeric($last_index))
                $page_id = $last_index;
                else
                $page_id = $data_mention['page_name'];
              }
              else
              {
                $page_id=$data_mention['page_name'];
              }

            $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
            $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
            $article_Link = $data_mention['link'];
            // if(! isset($article_Link)) $article_Link = '#';      
            // <div class="profiletimeline"><div class="sl-item">
            // <div class="sl-left"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div> 

            //dd($data_mention['created_time']);

            $html= '<div class="form-material sl-right"> <a href="'.$article_Link.'" style="font-size:18px;font-weight:800px;" target="_blank">'. $data_mention['title'] .'</a>';
                                                       if((int)$data_mention['campaign'] == 0)
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="false">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                        else
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="true">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';

                    // $html.='<span style="float:right" class="sl-date">'.$data_mention['created_time'].' </span> </div>';
            if($data_mention['hp'] <> ''){
              $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input Sentiment" data-toggle="tooltip"></i></a>';
            }
            $html.='<div><p style="float: right;width: 100%;">
            <span style="float:left" class="sl-date"><i class="mdi mdi-calendar-clock"></i>'.$data_mention['created_time'].' by <a href="'.$page_Link.'" class="link"  target="_blank" >'.$data_mention['page_name'].' </a></span>';

            $html.='<select id="sentiment_'.$data_mention['id'].'" style="float: right;" class="form-material form-control in-comment-select edit_senti ';
                   if($data_mention['sentiment']=="")$html.='"><option value="" selected="selected" ></option>';
                   if($data_mention['sentiment']=="pos")$html.='text-success" >';
                   if($data_mention['sentiment']=="neg")$html.='text-red" >';
                   if($data_mention['sentiment']=="neutral" || $data_mention['sentiment'] == "NA")$html.='text-warning" >';
                
                    $html.='<option value="pos" class="text-success"';
                    if($data_mention['sentiment'] == "pos")
                    $html.= 'selected="selected"';
                    $html.= '>Positive</option><option value="neg" class="text-red"';
                    if($data_mention['sentiment'] == "neg")
                    $html.= 'selected="selected"';
                    $html.= '>Negative</option><option value="neutral" class="text-warning"';
                    if($data_mention['sentiment'] == "neutral" || $data_mention['sentiment'] == "NA")
                    $html.= 'selected="selected"';      
                    $html.= '>Neutral</option></select>';


                    // $html.= '<a class="edit_predict_post" id="'.$data_mention['id'].'" href="javascript:void(0)">
                    // <span class="mdi mdi-content-save" style="font-size:30px;"></span></a></p>';
                    $html.='<div style="display:none"  align="center" style="vertical-align: top;" id="spin_'.$data_mention['id'].'"> <img src="assets/images/ajax-loader.gif" id="loader"></div>';
                    
                    $html.='</div><p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['id'].'">View More</a></p>';
                              
                    $html.= '</div><div class="sl-right" align="right"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-gray post_hide"  id="hide_'.$data_mention['id'].'" >Hide Article</a></div></div></div></div>';
                    return $html;

                  })
            ->rawColumns(['post'])
         
            ->make(true);
    }
    public function getCampaignKwCount()
    {
      $brand_id=Input::get('brand_id');
      $campaign_keyword = '';
      if(null !== Input::get('campaign_id'))
      {
        $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
        
      }
      // dd($campaign_keyword);
        echo json_encode($campaign_keyword);
    }

    public function getCampaignPost()
    {
          $year = Input::get('fday');
          $senti_con = '';
           if(null !== Input::get('sentiType'))
          {
            $senti_con = " and posts.checked_sentiment='".Input::get('sentiType')."'";
          }
        
          $brand_id=Input::get('brand_id');
          $keyword_con='';

          //  $campaign_keyword =[];
          // if(null !== Input::get('campaign_id'))
          // {
          //   $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
           
          // }
          // dd($campaign_keyword);
    
            if(null !== Input::get('fday'))
          {
            $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
            else
          {
            $dateBegin=date('Y-m-d');
          }
          // dd($dateBegin);

          if(null !==Input::get('sday'))
          {
            $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          
          else
          {
            $dateEnd=date('Y-m-d');
          }

           $filter_pages = ' 1=1 ';

         $period_type = '';
         $additional_filter=" AND 1=1 ";

        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

            
          if($period_type == 'week')
          {
          // $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";  
           $additional_filter .= " ";    

          }
          else
          {
              //$additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
              $additional_filter .= " ";
          }

       }
         

          // if(null !== Input::get('filter_page_name'))
          //  {
          //   $filter_page_name=$this->Format_Page_name_single(Input::get('filter_page_name'));
          //   $filter_pages .= " and page_name in ('".$filter_page_name."') ";
          //  }
           // if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           // {
           //  $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
           //  $filter_pages .= " and  posts.page_name in ('". $filter_page_name."') ";
           // }
           // else  
           // {
           //  $my_pages= $this->getOwnPage($brand_id);
           //  $my_pages=explode(',', $my_pages);

           //  if  ('1' == Input::get('other_page'))
           //  $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
           //  else
           //  $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages);
           // }
           $visitor_status= " And visitor = 0";
          if(null !== Input::get('visitor') && 1 == Input::get('visitor'))
           {
            $visitor_status= " And visitor = 1";
           }
           // $additional_filter ='';
           // dd(Input::get('tsearch_keyword'));
          $add_global_filter='';
          if(null !== Input::get('tsearch_senti'))
          {
            $add_global_filter .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
          }
            // $campaign_keyword =[];
            if(null !== Input::get('tsearch_keyword')){
              $tsearch_keyword = Input::get('tsearch_keyword');
	          $add_global_filter .=' AND '.$this->getKeywordWhereGen($tsearch_keyword,'posts');
	          // $campaign_keyword = explode('| ', $tsearch_keyword);
	          
          }
          // dd($campaign_keyword);
          $add_inbound_con ="";

          $inboundpages=$this->getInboundPages($brand_id);
          if ($inboundpages !== '')
          {
            $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is not NULL)";
            // $add_con = " and (page_name  not in (".$inboundpages.") or page_name is not NULL)";
            
          }
          $campaign_filter_con='';$campaign_keyword = [];


          // if(null !==Input::get('type'))
          // {
          	    // $type = Input::get('type');
          	    // if($type == 'mention post'){
            	// 	if(null !== Input::get('campaign_name'))
	          		// {
	          		// 	$campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_name'),$brand_id);
	           	// 		$campaign_filter_con .=  " AND  " .$this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
	          		// }
             	// } 
      	    	// if($type == 'inbound post'){
          
          if(null !== Input::get('campaign_id'))
          {
        	 $campaign_filter_con .=' AND posts.campaign='.Input::get('campaign_id');
        	 $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
    	  }
          	// }   
          // }
          


// dd($campaign_filter_con);
      $query='';
      $query_result=[];
     
      $query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,replace(shared, ',', '') shared,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
      " created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,posts.isDeleted,".
      " posts.campaign,cmts.checked_sentiment ".
      " cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
      " ELSE 'mdi-star' ".
      " END) isBookMark ".
      "FROM temp_inbound_posts posts LEFT JOIN temp_inbound_comments cmts on posts.id=cmts.post_id ".
      " WHERE campaign is not null AND YEAR(DATE(posts.created_time)) = '" .$year. "' "  .  $visitor_status . $additional_filter . $campaign_filter_con  . $add_global_filter . $senti_con . 
      " ORDER by timestamp(posts.created_time) DESC ";

      // $post_query = "SELECT Distinct(id) post_id FROM temp_inbound_posts inpost WHERE  inpost.campaign is not null and (DATE(inpost.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inpost_con . $add_con . $inpost_key_con;

      $query_result = DB::select($query);
      // dd($query_result);
       
              $data = [];
               $data_mention=[];

               $project = Project::find($brand_id);
               $monitor_pages = $project->monitor_pages;
               $monitor_pages = explode(',', $monitor_pages);
               
              $mongo_page = $this->Format_Page_Name($monitor_pages);
				// dd($mongo_page);
              //get page id from mongo
              $InboundPages_result=InboundPages::raw(function ($collection) use($mongo_page) {//print_r($filter);

                return $collection->aggregate([
                    [
                    '$match' =>[
                         '$and'=> [ 
                         ['page_name'=>['$in'=>$mongo_page]],
                                   
                                  ]
                    ] 
                               
                   ],

                   ['$sort' =>['_id'=>1]]
                    
                ]);
              })->toArray();
          // dd($InboundPages_result);

          $mongo_page_name=[];
          $mongo_page_id=[];
          $mongo_page_full_name=[];
          foreach ($InboundPages_result as  $key => $row) {
            $id='';$page_name_new='';$full_page_name='';
            if(isset($row['id'])) $id = $row['id'];
            if(isset($row['page_name'])) $page_name = $row['page_name'];
            if(isset($row['full_page_name'])) $full_page_name = $row['full_page_name'];

            $mongo_page_name[] =[
               'page_name' =>$page_name,
              ];
              $mongo_page_id[] =[
                'id' => $id,
              ];
            $mongo_page_full_name[] =[
                'full_page_name' =>$full_page_name,
               ];
            

          }
          $mongo_page_name = array_column($mongo_page_name,'page_name');
          $mongo_page_full_name = array_column($mongo_page_full_name,'full_page_name');
          $mongo_page_id = array_column($mongo_page_id,'id');

              // dd($query_result);
           foreach ($query_result as  $key => $row) {
            //change page name from id to orginal user register name.
            $page_name=$row->page_name;
            $page_mongo_id='';
            
                    $key = array_search($page_name, $mongo_page_name);
                    if(isset($mongo_page_id[$key]))
                    $page_mongo_id=$mongo_page_id[$key];

                    if(isset($mongo_page_full_name[$key]))
                    $page_name=$mongo_page_full_name[$key];
                    
                    
                   // dd($page_name);

          $pos=($row->cmt_sentiment=='pos' && $row->parent=== '' )?1:0 ;
          $neg=($row->cmt_sentiment=='neg' && $row->parent=== '' )?1:0;
          $cmtneutral=($row->cmt_sentiment=='neutral' && $row->parent=== '' )?1:0;
          $cmtNA=($row->cmt_sentiment=='NA' && $row->parent=== '' )?1:0;
          $cmtneutral=(int)$cmtneutral+(int) $cmtNA;

          $anger =($row->cmt_emotion=='anger' && $row->parent==='' )?1:0 ;
          $interest=($row->cmt_emotion=='interest' && $row->parent==='' )?1:0;
          $disgust=($row->cmt_emotion=='disgust' && $row->parent ==='')?1:0;
          $fear=($row->cmt_emotion=='fear' && $row->parent=== '' )?1:0;
          $joy=($row->cmt_emotion=='joy' && $row->parent==='')?1:0;
          $like=($row->cmt_emotion=='like' && $row->parent==='' )?1:0;
          $love=($row->cmt_emotion=='love' && $row->parent==='' )?1:0;
          $neutral=($row->cmt_emotion=='neutral' && $row->parent==='' )?1:0;
          $sadness=($row->cmt_emotion=='sadness' && $row->parent==='' )?1:0;
          $surprise=($row->cmt_emotion=='surprise' && $row->parent==='' )?1:0;
          $trust=($row->cmt_emotion=='trust' && $row->parent==='')?1:0;
           
             
             $message=$this->readmore($row->message);
             $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
             $message = $this->highLightText($campaign_keyword,$message);

             $total = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry;
             //$message=$row->message;
                      if (isset($row ->id))
             {
                $comment_count=($row->parent==='')?1:0;

                           $id= $row ->id;
             
              if (!array_key_exists($id , $data)) {
              $data[$id] = array(
                          'id'     =>$row->id,
                          'message' => $message,
                          'page_name' =>$page_name,
                          'page_mongo_id' =>$page_mongo_id,
                          'created_time' =>$row->created_time,
                          'link' =>$row->link,
                          'full_picture' =>$row->full_picture,
                          'sentiment' =>$row->sentiment,
                          'emotion' =>$row->emotion,
                          'positive'=> $pos,'negative'=>$neg,'cmtneutral'=>$cmtneutral,
                          'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,'shared'=>$row->shared,
                          'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                          'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                          'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                          'total'=>$total,
                          'comment_count'=>$comment_count,
                          'isDeleted'=>$row->isDeleted ,
                          'isBookMark'=>$row->isBookMark,
                          'campaign'=>$row->campaign
                      );
          }
          else
          {
           $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
           $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
           $data[$id]['cmtneutral'] = (int) $data[$id]['cmtneutral'] +$cmtneutral;
           $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
           $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
           $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
           $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
           $data[$id]['like'] = (int) $data[$id]['like'] + $like;
           $data[$id]['love'] = (int) $data[$id]['love'] + $love;
           $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
           $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
           $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
           $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;
           $data[$id]['comment_count'] = (int) $data[$id]['comment_count'] + $comment_count;



          }


          }
           
                      }
                      // dd($data);

                       if(null !== Input::get('filter_overall'))
                      {
                        if( Input::get('filter_overall') === 'pos')//
                        {   
                          foreach ($data as $key => $value) {

                  if( ((int)$value['positive'] <  (int)$value['negative']) || ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0) )
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                          }
                        else if( Input::get('filter_overall') === 'neg')
                        {
                     foreach ($data as $key => $value) {

                      if((int)$value['positive'] >  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'] &&  (int)$value['cmtneutral'] <> 0)
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                        }
                         else if( Input::get('filter_overall') === 'neutral')
                        {
                                    foreach ($data as $key => $value) {

                                 if( ! ( ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 ) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0)))
                                 {
                                    unset($data[$key]);
                                 }
                                  # code...
                                }
                        }

                      }

                      $data_mention=$data;

                       // dd($data_mention);
                      // dd($query_result);
                      return Datatables::of($data_mention)
               
                 ->addColumn('post', function ($data_mention){
                  $fb_p_id=explode('_', $data_mention['id']);
                  $fb_p_id=$fb_p_id[1];
                  $page_id='';
                  $arr_page_id=explode("-",$data_mention['page_name']);
                  if(count($arr_page_id)>0)
                  {
                    $last_index=$arr_page_id[count($arr_page_id)-1];
                    if(is_numeric($last_index))
                    $page_id = $last_index;
                    else
                    $page_id = $data_mention['page_name'];
                  }
                  else
                  {
                    $page_id=$data_mention['page_name'];
                  }

                $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
                $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
                if($data_mention['page_mongo_id'] <> '')
                {  $page_photo='https://graph.facebook.com/'.$data_mention['page_mongo_id'].'/picture?type=large';}
              else
                { $page_photo='assets/images/unknown_photo.jpg'; }
            
              
              

                        $total = (int) $data_mention['positive'] +(int) $data_mention['negative']+(int) $data_mention['cmtneutral'];
                        $pos_pcent='';$neg_pcent='';$neutral_pcent='';
                       
                         $html= '<div class="profiletimeline"><div class="sl-item">
                                              <div class="sl-left"> <img src="'.$page_photo.'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                              <div class="sl-right"> <div><div><a href="'.$page_Link.'" class="link"  target="_blank">'.$data_mention['page_name'].' </a>';
                        // $html.='  <label class="text-info"   for="campaign" style=""><i class="mdi mdi-flag-outline"></i></label> 
                        //            ';
                        if((int)$data_mention['campaign'] == 0)
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="false">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                        else
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="true">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button><span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data_mention['created_time'].'</span></div>';
                        // if((int)$data_mention['isDeleted']==1)
                        // {
                        //  $html.='  <span  class="text-red">   This post is no longer available on FB. </span>';
                        // }
                        $html.='<div class="sl-right">
                       <div>';
                         if((int) $data_mention['negative'] > 0)
                       { 
                        $neg_pcent=round((int) $data_mention['negative']/$total*100,2);
                        $html.='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'.$neg_pcent.'%';
                      }
                      if((int) $data_mention['positive'] > 0)
                        {
                          $pos_pcent=round((int) $data_mention['positive']/$total*100,2);
                          $html.='<span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'.$pos_pcent.'%';
                        }
                      if((int) $data_mention['cmtneutral'] > 0)
                       {
                        $neutral_pcent=round((int) $data_mention['cmtneutral']/$total*100,2);
                       $html.='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'.$neutral_pcent.'%';
                       }
                                 
                          $html.='</div></div>';
                          $html.='<p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['id'].'">View More</a></p>';
                                  
                        $html.= '</div>
                                   
                        <div class="sl-right">';
                        if((int)$data_mention['liked'] > 0)
                        $html.='👍 '.$this->thousandsCurrencyFormat($data_mention['liked']);
                       if((int)$data_mention['loved'] > 0)
                        $html.=' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']);
                       if((int)$data_mention['haha'] > 0)
                        $html.=' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']);
                       if((int)$data_mention['wow'] > 0)
                        $html.=' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']);
                       if((int)$data_mention['sad'] > 0)
                        $html.=' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']);
                       if((int)$data_mention['angry'] > 0)
                        $html.=' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']);
                        $html.='<a href="javascript:void(0)"  class="see_comment" id="seeComment_'.$data_mention['id'].'"  style="float:right"><span class="postPanel_actionIconContainer" id="idb5d"><span class="postPanel_icon mdi mdi-comment-outline" title="Comments"></span><span>'.$data_mention['comment_count'].'</span></span></a>';
                        $html.='</div><div class="sl-right" align="right"><a href="'.$post_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See Post on FB</a></div></div></div>';

               
                     
                  
                          return $html;
                         

                      })
            ->addColumn('reaction', function ($data_mention) {
                          return "<span style='font-weight:bold'>" . number_format($data_mention['total'])."</span>";
                      }) 

            ->addColumn('comment', function ($data_mention) {
                           return "<span style='font-weight:bold'>" . number_format($data_mention['comment_count'])."</span>";
                      }) 
            ->addColumn('share', function ($data_mention) {
                          return  "<span style='font-weight:bold'>" . number_format($data_mention['shared'])."</span>";
                      }) 
           
                ->rawColumns(['post','overall','reaction','comment','share'])
             
                ->make(true);


    }
    public function getcampaigncomment()
    {

       $year = $year = Input::get('fday');
       $senti_con = '';
       if(null !== Input::get('sentiType')){ $senti_con = " and cmts.checked_sentiment='".Input::get('sentiType')."'"; }

       $visitor_status= " And visitor = 0";
       if(null !== Input::get('visitor') && 1 == Input::get('visitor')){ $visitor_status= " And visitor = 1";}

       $brand_id=Input::get('brand_id');

       $campaign_filter_con = '';$campaign_keyword=[];
       if(null !== Input::get('campaign_id')){
        $campaign_filter_con .=' AND posts.campaign='.Input::get('campaign_id');
        $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);

       }
       //  $campaign_filter_con_cmt = '';$campaign_filter_mention='';
       //  $campaign_keyword = [];
       // if(null !== Input::get('campaign_id')){
       // $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
       // $campaign_filter_con_cmt .=  " AND  " .$this->getcampaignfilter_MYSQL($campaign_keyword,'cmts');
       // $campaign_filter_mention .= " AND  " .$this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
       // }

      
       $period_type = '';
       $additional_filter=" AND 1=1 ";
      
       if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

      
            if($period_type == 'week')
            {
             $additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

            }
            else
            {
                $additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
            }

       }

   
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment ='';
      $is_competitor = "false";
    
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment');

      if(null !== Input::get('is_competitor'))
      $is_competitor=Input::get('is_competitor'); 
   
       $add_global_filter_p='';
       $add_global_filter_c='';
      if(null !== Input::get('tsearch_senti'))
      {
        // $add_global_filter_p .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
        $add_global_filter_c .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
      }
      if(null !== Input::get('tsearch_keyword'))
      {
        // $add_global_filter_p .= " AND " . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
        $add_global_filter_c .= " AND " . $this->getKeywordWhereGen(Input::get('tsearch_keyword'),'cmts');
      }
       if(null !== Input::get('tsearch_tag'))
      {
        $add_global_filter .=" and " . $this->getTagWhereGen(Input::get('tsearch_tag'));
      }

       if(null !== Input::get('fday'))
        {
          $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
       else
        {
          $dateBegin=date('Y-m-d');
        }
    

        if(null !==Input::get('sday'))
        {

          $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
        
        else
        {
          $dateEnd=date('Y-m-d');

        }
      // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
      // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));

            $filter_pages = ' 1=1 ';
         

       // if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
       // {
       //  $filter_page_name=$this->Format_Page_name_single(Input::get('admin_page'));
       //  $filter_pages .= " and  posts.page_name in ('". $filter_page_name."') ";
       // }
       // else  
       // {
       //  $my_pages= $this->getOwnPage($brand_id);
       //  $my_pages=explode(',', $my_pages);

       //  if  ('1' == Input::get('other_page'))
       //  $filter_pages .= ' and ' . $this->getPageWhereGenNotIn($my_pages);
       //  else
       //  $filter_pages  .= ' and ' . $this->getPageWhereGen($my_pages);
       // }

      $add_con='';
      $date_filter='';
      if(null !== Input::get('post_id'))
      {
        $add_con = "and cmts.post_id='".Input::get('post_id')."'"; 
        $filter_pages ='  1=1 '; //if we know post_id no need to filter pages. post_id can get data unique
      }
 
       $tags =$this->gettags($brand_id);

       //if comefrom == post we don't need to consider cmts created time , if not some comment can missing due to date filter that come from post page
      if(null !== Input::get('comefrom') &&  'post'==Input::get('comefrom'))
      {
        $date_filter = ' 1=1 ';
      }
      else
      {
        //$date_filter=" (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
        $date_filter=" YEAR(DATE(posts.created_time)) = '" .$year. "' ";
      }
      if(null !== Input::get('tsearch_keyword'))
          {
          $additional_filter .=' AND '.$this->getKeywordWhereGen(Input::get('tsearch_keyword'),'posts');
          }

          // if(Input::get('type') == 'inbound comment'){
             $post_id_filter = '';
     
             $query="SELECT Distinct(id) post_id FROM temp_inbound_posts posts  ".
            " WHERE posts.campaign IS NOT NULL".
            "  AND   ".$date_filter."  " . $add_con .  $campaign_filter_con  . $additional_filter . $senti_con . 

            " ORDER by timestamp(posts.created_time) DESC ";

            /*$query="SELECT Distinct(id) post_id FROM temp_inbound_posts posts  "." WHERE posts.campaign IS NOT NULL"."  " . $add_con .
                    $campaign_filter_con  . $additional_filter . $senti_con . " ORDER by timestamp(posts.created_time) DESC ";*/

               $data = DB::select($query);
               // dd($data);
               $id = [];
               $id_string = '';
               foreach ($data as $key => $value) {
                $id[] = $value->post_id;
               }
                 $id_string = implode('","', $id);
                 $id_string = '("'.$id_string.'")';
                 $post_id_filter = ' AND cmts.post_id In '.$id_string ;
         
            // dd($campaign_filter_con);
    
             $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,pros.id profileID,pros.type as profileType,pros.name as profileName,cmts.checked_tags_id tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
            " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,action_status,action_taken ".
            " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
            " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
            " WHERE posts.id IS NOT NULL ".
            " and  parent=''  AND tag_flag=1   "."  " . $add_con  . $add_global_filter_c .  $post_id_filter . $senti_con . 

            " ORDER by timestamp(cmts.created_time) DESC ";

            // $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,pros.id profileID,pros.type as profileType,pros.name as profileName,cmts.checked_tags_id tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
            // " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,action_status,action_taken ".
            // " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
            // " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
            // " WHERE posts.id IS NOT NULL AND ". $date_filter .
            // " and  parent=''  AND tag_flag=1   "."  " . $add_con  . $add_global_filter_c .  $post_id_filter . $senti_con . 

            // " ORDER by timestamp(cmts.created_time) DESC ";

        // }


//            else{
//            	 $post_id_filter = '';
     
//              $query="SELECT Distinct(id) post_id FROM temp_inbound_posts posts  ".
//             " WHERE ".
//             "      ".$date_filter."  " . $add_con .  $campaign_filter_mention  . $additional_filter . $senti_con . 

//             " ORDER by timestamp(posts.created_time) DESC ";
// // dd($query);
//                $data = DB::select($query);
//                // dd($data);
//                $id = [];
//                $id_string = '';
//                foreach ($data as $key => $value) {
//                 $id[] = $value->post_id;
//                }
//                  $id_string = implode('","', $id);
//                  $id_string = '("'.$id_string.'")';
//                  $post_id_filter = ' AND cmts.post_id In '.$id_string ;
// 				// dd($post_id_filter);
//              $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,pros.id profileID,pros.type as profileType,pros.name as profileName,cmts.checked_tags_id tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
//             " cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags_id = cmts.checked_tags_id || cmts.checked_tags_id = '','','human predict!') hp_tags,action_status,action_taken ".
//             " FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  posts on posts.id=cmts.post_id ".
//             " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
//             " WHERE posts.id IS NOT NULL ".
//             " and  parent=''  AND tag_flag=1   "."  " . $add_con  . $add_global_filter_c . $campaign_filter_con_cmt . $post_id_filter . $senti_con . 

//             " ORDER by timestamp(cmts.created_time) DESC ";
//         }
      
 
      // print_r($query);
     $data = DB::select($query);
     // dd($data);
     $project = Project::find($brand_id);
     $monitor_pages = $project->monitor_pages;
     $monitor_pages = explode(',', $monitor_pages);

     $permission_data = $this->getPermission(); 

        return Datatables::of($data)
              
             ->addColumn('post_div', function ($data)   use($tags,$permission_data,$monitor_pages,$campaign_keyword) {
              
              $page_name=$data->page_name ;
                if(is_numeric($page_name))
                {
                   $input = preg_quote($page_name, '~'); // don't forget to quote input string!
                   $keys = preg_grep('~' . $input . '~', $monitor_pages);
                   $vals = array();
                      foreach ( $keys as $key )
                      {
                         $page_name= $key;
                      }
                     
                }

                        $message = $data->message;
                        $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
                        $message = $this->highLightText($campaign_keyword,$message);

              //generate comment link
              $profilePhoto='';$ProfileName='';
              $profileLink='javascript:void(0)';
              $fb_p_id=explode('_', $data->post_id);
              $fb_p_id=$fb_p_id[1];
              $fb_c_id=explode('_', $data->id);
              $fb_c_id=$fb_c_id[1];
                if($data->profileName <> '') $ProfileName=$data->profileName; 
                else if($data->profileID <> '')  $ProfileName=$data->profileID;
                else  $ProfileName=$page_name;

              if($data->profileType === 'number')
              {
                $profilePhoto='https://graph.facebook.com/'.$data->profileID.'/picture?type=large';
                $ProfileName=$page_name;
              }
              
              if(isset($data->profileID))
              $profileLink='https://www.facebook.com/'.$data->profileID;

              $page_id='';
              $arr_page_id=explode("-",$page_name);
              if(count($arr_page_id)>0)
              {
                $last_index=$arr_page_id[count($arr_page_id)-1];
                if(is_numeric($last_index))
                $page_id = $last_index;
                else
                $page_id = $page_name;
              }
              else
              {
                $page_id=$page_name;
              }

              $comment_Link= 'https://www.facebook.com/'.$fb_c_id;
              $classforbtn='';$stylefora='';$bgcolor='';
                    if($data->sentiment=='neg')
                      {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                    else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                      else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
                      $html = '<div class="profiletimeline"><div class="sl-item">';
                      if($profilePhoto<>'')
                      $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                      else
                      /*$html .= '<div class="sl-left user-img"> <span class="round"><i class="mdi mdi-comment-account"></i></span> </div>';*/
                    $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                     
                       $html .='<div class="form-material sl-right"><div> <div>
                    <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a>';
               
                    if($data->hp <> '' && $data->hp_tags <>'')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    else if($data->hp <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                    else if($data->hp_tags <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                    $html .='<span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                     // $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                    
                     $html.='<p>Sentiment : <select id="sentiment_'.$data->id.'"  class="form-material form-control in-comment-select edit_senti ';
                     if($data->sentiment=="")$html.='"><option value="" selected="selected" ></option>';
                     if($data->sentiment=="pos")$html.='text-success" >';
                     if($data->sentiment=="neg")$html.='text-red" >';
                     if($data->sentiment=="neutral" || $data->sentiment == "NA")$html.='text-warning" >';
                  
                      $html.='<option value="pos" class="text-success"';
                      if($data->sentiment == "pos")
                      $html.= 'selected="selected"';
                      $html.= '>Positive</option><option value="neg" class="text-red"';
                      if($data->sentiment == "neg")
                      $html.= 'selected="selected"';
                      $html.= '>Negative</option><option value="neutral" class="text-warning"';
                      if($data->sentiment == "neutral" || $data->sentiment == "NA")
                      $html.= 'selected="selected"';      
                      $html.= '>Neutral</option></select>';

                     
                      $html.=' | Tag :<select id="tags_'.$data->id.'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                              style="width:350px;height:36px">';
                       $tag_array=explode(',', $data->tags);
                      foreach ($tags as $key => $value) {
                      # code...
                        //if (strpos($data->tags, $value->name) !== false)
                       if ( in_array( $value->id,$tag_array))
                       $html.= '<option value="'.$value->id.'" selected="selected">'.$value->name.'</option>';
                        else
                       $html.= '<option value="'.$value->id.'">'.$value->name.'</option>';
                      }
                      $html.='</select>';
                      if($permission_data['edit'] === 1)
                      $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                     $html.='<a class="get-code" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id="add_tag"><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                      
                      $html.='</p><p class="m-t-10">'.$message.'</p></div>';
                       if($data->cmtType =='sticker' || $data->cmtType =='photo' || $data->cmtType =='share' || $data->cmtType =='share_large_image' || $data->cmtType =='animated_image_share'  )
                       {
                          $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'.
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                              '<img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">'.
                      '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                       else if($data->cmtType =='video_inline' || $data->cmtType =='animated_image_video' || $data->cmtType =='video_share_youtube' || $data->cmtType =='video_share_highlighted')
                       {
                         $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'.$data->id.'">'.
                          '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                          '<iframe src="https://www.facebook.com/plugins/video.php?href='.$data->cmtLink.'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'.
                          '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                      $html.= '<div class="sl-right">';
                    
                        if((int)$data->cmtLiked > 0)
                        $html.='👍 '.$this->thousandsCurrencyFormat($data->cmtLiked);
                       if((int)$data->cmtLove > 0)
                        $html.=' ❤️ '.$this->thousandsCurrencyFormat($data->cmtLove);
                       if((int)$data->cmtHaha > 0)
                        $html.=' 😆 '.$this->thousandsCurrencyFormat($data->cmtHaha);
                       if((int)$data->cmtWow > 0)
                        $html.=' 😮 '.$this->thousandsCurrencyFormat($data->cmtWow);
                       if((int)$data->cmtSad > 0)
                        $html.=' 😪 '.$this->thousandsCurrencyFormat($data->cmtSad);
                       if((int)$data->cmtAngry > 0)
                        $html.=' 😡 '.$this->thousandsCurrencyFormat($data->cmtAngry);
                        $html.='</div>
                      <div class="sl-right" style="float: right;">
                    <div style="display: inline-block"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-green popup_post" id="'.$data->post_id.'">See Post</a></div>
                     <div style="display: inline-block"><a href="'.$comment_Link.'" class="waves-effect waves-light btn-sm btn-gray" target="_blank">Take Action</a></div>
                    <div style="display: inline-block" class="btn-group">';
                    if($data->action_status === "Action Taken")
                    $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Action Taken
                   </button>';
                   else
                   $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Require Action
                   </button>';
                $html.='<div class="dropdown-menu">
                  <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Require Action</a>
                  <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Action Taken</a>

                </div></div></div></div></div>';

                      return $html;

                  })
        
            ->rawColumns(['post_div'])
         
            ->make(true);

    }
    public function getcampaignmentiondetail()
    {

        $year = Input::get('fday');

        $brand_id=Input::get('brand_id');
        $groupType=Input::get('periodType');

        $filter_keyword ='';
        $filter_sentiment='';
        if(null !== Input::get('keyword'))
          $filter_keyword=Input::get('keyword'); 
        if(null !== Input::get('sentiment'))
          $filter_sentiment=Input::get('sentiment'); 
          

        if(null !== Input::get('fday'))
        {
          $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
        else
        {
          $dateBegin=date('Y-m-d');
        }
                

        if(null !==Input::get('sday'))
        {
         $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
       
        else
        {
         $dateEnd=date('Y-m-d');
        }
        /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
        $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

        $dateformat ="";
        $add_con="";
        $add_comment_con="";
        $add_article_con="";
        $add_inpost_con="";
        $add_incmt_con="";

        if($groupType=="month")
        {
          $dateformat ="%Y-%m"; 
        }

        else 
        {
          $dateformat ="%Y-%m-%d";                
          

        }
        // dd($groupType);
              $format =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
              

              if($filter_keyword !== '')
              {
                $add_con = " and posts.message Like '%".$filter_keyword."%'";
                // $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
                $add_article_con = " and article.message Like '%".$filter_keyword."%'";
                $add_inpost_con = " and inpost.message Like '%".$filter_keyword."%'";
                // $add_incmt_con = " and incmt.message Like '%".$filter_keyword."%'";
              }

              if($filter_sentiment !== '')
              {
                $add_con .= " and sentiment = '".$filter_sentiment."'";
                // $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
                $add_article_con .= " and article.sentiment  = '".$filter_sentiment."'";
                $add_inpost_con .= " and inpost.sentiment  = '".$filter_sentiment."'";
                // $add_incmt_con .= " and incmt.sentiment  = '".$filter_sentiment."'";
              }

              $add_inbound_con ="";

              $inboundpages=$this->getInboundPages($brand_id);
              if ($inboundpages !== '')
              {
                $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is not NULL)";
                // $add_con = " and (page_name  not in (".$inboundpages.") or page_name is not NULL)";
                
              }

 

              // $comment_key_con=" AND 1=1 ";
              $post_key_con=" AND 1=1 ";
              $article_key_con=" AND 1=1 ";
              $inpost_key_con=" AND 1=1 ";
              // $incmt_key_con=" AND 1=1 ";

              $campaign_keyword = [];
              // dd( Input::get('campaign_id'));
              if(null !== Input::get('campaign_id'))
              {
                $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
                // dd($campaign_keyword);
                // $comment_key_con .=  " AND  " .$this->getcampaignfilter_MYSQL($campaign_keyword,'cmts');
                // $post_key_con .= " AND " . $this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
                // $article_key_con .= " AND " . $this->getcampaignfilter_MYSQL($campaign_keyword,'article');
                // $inpost_key_con .= " AND  " .$this->getcampaignfilter_MYSQL($campaign_keyword,'inpost');
                // $incmt_key_con .= " AND  " .$this->getcampaignfilter_MYSQL($campaign_keyword,'incmt');

              }

              else
              {

               // $comment_key_con .= ' AND 1=2';
               $post_key_con .= ' AND 1=2';
               $article_key_con .=' AND 1=2 ';
               $inpost_key_con .= ' AND 1=2';
               // $incmt_key_con .= ' AND 1=2';
             }
              // dd($post_key_con);

             // if(null !== Input::get('campaign_id'))
             // {
             // 	$campaign_id = Input::get('campaign_id');
             // }

     //          $post_id_filter='';
     //          if(null !== Input::get('cmt_type'))
     //         {
     //         $post_query = "SELECT Distinct(id) post_id  FROM temp_inbound_posts inpost WHERE  inpost.campaign=".$campaign_id." and (DATE(inpost.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inpost_con  . $inpost_key_con;
     //         $data = DB::select($post_query);
     //         // dd($data);
     //           $id = [];
     //           $id_string = '';
     //           foreach ($data as $key => $value) {
     //            $id[] = $value->post_id;
     //           }
     //             $id_string = implode('","', $id);
     //             $id_string = '("'.$id_string.'")';
     //             $post_id_filter = ' AND incmt.post_id In '.$id_string ;
					// // dd($post_id_filter);
     //             $com ="SELECT * FROM temp_inbound_comments incmt WHERE  parent=''  AND tag_flag=1  ". $add_incmt_con   . $post_id_filter . $incmt_key_con ;
     //             $res = DB::select($com);
             
             /*$query = "select count(*) count,created_time  from (SELECT DATE_FORMAT(created_time,'".$dateformat."') created_time,posts.page_name FROM temp_posts posts  WHERE posts.isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .$add_inbound_con. $add_con . $post_key_con  . " AND posts.campaign = ". Input::get('campaign_id') 
              
             // UNION ALL
             // SELECT DATE_FORMAT(cmts.created_time,'".$dateformat."') created_time,cmts.page_name FROM temp_comments cmts ".
             // "WHERE cmts.isHide=0 and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_comment_con .$add_inbound_con .  $comment_key_con 
              . " 
             UNION ALL
             SELECT DATE_FORMAT(article.created_time,'".$dateformat."') created_time,article.page_name FROM temp_web_mentions article WHERE  article.isHide = 0 and  (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_article_con . $add_inbound_con . $article_key_con  . " AND article.campaign = ". Input::get('campaign_id') 
             . "
             UNION ALL
             SELECT DATE_FORMAT(inpost.created_time,'".$dateformat."') created_time,inpost.page_name FROM temp_inbound_posts inpost WHERE  inpost.campaign=".Input::get('campaign_id')." and (DATE(inpost.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inpost_con  . $inpost_key_con  
             // . "
             // UNION ALL
             // SELECT DATE_FORMAT(incmt.created_time,'".$dateformat."') created_time,incmt.page_name FROM temp_inbound_comments incmt WHERE  parent=''  AND tag_flag=1 ". $add_incmt_con   . $post_id_filter . $incmt_key_con 
             . ") t1 Group By created_time ORDER BY created_time ";*/

             /*$query = "select count(*) count,created_time  from (SELECT DATE_FORMAT(created_time,'".$dateformat."') created_time,posts.page_name FROM temp_posts posts  WHERE posts.isHide=0 and (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" .$add_inbound_con. $add_con . $post_key_con  . " AND posts.campaign = ". Input::get('campaign_id') 
              
             // UNION ALL
             // SELECT DATE_FORMAT(cmts.created_time,'".$dateformat."') created_time,cmts.page_name FROM temp_comments cmts ".
             // "WHERE cmts.isHide=0 and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_comment_con .$add_inbound_con .  $comment_key_con 
              . " 
             UNION ALL
             SELECT DATE_FORMAT(article.created_time,'".$dateformat."') created_time,article.page_name FROM temp_web_mentions article WHERE  article.isHide = 0 and  (DATE(article.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_article_con . $add_inbound_con . $article_key_con  . " AND article.campaign = ". Input::get('campaign_id') 
             . "
             UNION ALL
             SELECT DATE_FORMAT(inpost.created_time,'".$dateformat."') created_time,inpost.page_name FROM temp_inbound_posts inpost WHERE  inpost.campaign=".Input::get('campaign_id')." and (DATE(inpost.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inpost_con  . $inpost_key_con  
             // . "
             // UNION ALL
             // SELECT DATE_FORMAT(incmt.created_time,'".$dateformat."') created_time,incmt.page_name FROM temp_inbound_comments incmt WHERE  parent=''  AND tag_flag=1 ". $add_incmt_con   . $post_id_filter . $incmt_key_con 
             . ") t1 Group By created_time ORDER BY created_time ";*/

             $query = "select count(*) count,created_time  from (SELECT DATE_FORMAT(created_time,'".$dateformat."') created_time,posts.page_name FROM temp_posts posts  WHERE posts.isHide=0 AND YEAR(DATE(posts.created_time)) = '" .$year. "'" .$add_inbound_con. $add_con . $post_key_con  . " AND posts.campaign = '".Input::get('campaign_id')."' UNION ALL
             SELECT DATE_FORMAT(article.created_time,'".$dateformat."') created_time,article.page_name FROM temp_web_mentions article WHERE  article.isHide = 0 AND YEAR(DATE(article.created_time)) = '" .$year. "'". $add_article_con . $add_inbound_con . 
             $article_key_con  . " AND article.campaign = '".Input::get('campaign_id')."'

             UNION ALL
             SELECT DATE_FORMAT(inpost.created_time,'".$dateformat."') created_time,inpost.page_name FROM temp_inbound_posts inpost WHERE YEAR(DATE(inpost.created_time)) = '" .$year. "' AND inpost.campaign='".Input::get('campaign_id')."' ". $add_inpost_con  . 
             $inpost_key_con  
             // . "
             // UNION ALL
             // SELECT DATE_FORMAT(incmt.created_time,'".$dateformat."') created_time,incmt.page_name FROM temp_inbound_comments incmt WHERE  parent=''  AND tag_flag=1 ". $add_incmt_con   . $post_id_filter . $incmt_key_con 
             . ") t1 Group By created_time ORDER BY created_time ";

             $query_result = DB::select($query);
            
             // dd($query_result);
             $data_mention = [];



             foreach ($query_result as  $key => $row) {
              
              $request['periods'] =$row ->created_time;
              $request['mention'] =$row ->count;
              $request['periodLabel'] ="";
              
              
              
              if($groupType == "week")
              {
                $weekperiod=$this ->rangeWeek ($request['periods']);
                $request['periodLabel'] =$weekperiod['start'] . " - " . $weekperiod['end'] ;
                $periodLabel = $request['periodLabel'];
                if (!array_key_exists($periodLabel , $data_mention)) {
                  $data_mention[$periodLabel] = array(
                    'periodLabel' => $periodLabel,
                    'mention' =>   $request['mention'],
                    'periods' =>  $request['periods'],
                  );
                }
                else
                {
                 $data_mention[$periodLabel]['mention'] = (int) $data_mention[$periodLabel]['mention'] +  $request["mention"];
                 $data_mention[$periodLabel]['periods'] = $request['periods'];
               }

             }
             else
             {
               $request['periodLabel'] =$request['periods'] ;
               $periodLabel = $request['periodLabel'];
               if (!array_key_exists($periodLabel , $data_mention)) {
                $data_mention[$periodLabel] = array(
                  'periodLabel' => $periodLabel,
                  'mention' =>   $request['mention'],
                  'periods' =>  $request['periods'],
                );
              }
              else
              {
               $data_mention[$periodLabel]['mention'] = (int) $data_mention[$periodLabel]['mention'] +  $request["mention"];
               $data_mention[$periodLabel]['periods'] = $request['periods'];
             }
             
            }
            }
            $data_mention = $this->unique_multidim_array($data_mention,'periodLabel'); 
            usort($data_mention, function($a1, $a2) {
             $value1 = strtotime($a1['periods']);
             $value2 = strtotime($a2['periods']);
             return $value1 - $value2;
            });

            echo json_encode($data_mention);

    }
    public function getCampaignMentionStatus()
    {
         $year = Input::get('fday');
         $data = array();
         $brand_id=Input::get('brand_id');


         if(null !== Input::get('fday'))
          {
             $date_Week_Begin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
             $date_Week_Begin=date('Y-m-d');
          }
        

          if(null !==Input::get('sday'))
          {

             $date_End=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          
          else
          {
             $date_End=date('Y-m-d');

          }

         $add_inbound_con ="";
         $inboundpages=$this->getInboundPages($brand_id);

          if ($inboundpages !== '')
          {
            $add_inbound_con = " AND (page_name  not in (".$inboundpages.") or page_name is NULL)";
          }

          $comment_key_con=" AND 1=1 ";
          $post_key_con=" AND 1=1 ";
          $inpost_key_con = " AND 1=1";
          $article_key_con = " AND 1=1";
          if(null !== Input::get('campaign_id'))
          {
            $campaign_keyword=  $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
            // $post_key_con = " AND " .$this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
            // $comment_key_con = " AND " .$this->getcampaignfilter_MYSQL($campaign_keyword,'cmts');
            // $article_key_con = " AND " .$this->getcampaignfilter_MYSQL($campaign_keyword,'article');
            // $campaign_filter_con = " and ".$campaign_filter_con;
            // dd($incmt_key_con);
          } 
	        else
	        {

	         $comment_key_con .= ' AND 1=2';
	         $post_key_con .= ' AND 1=2';
	         $article_key_con.= ' And 1=2';
	        }

	      $inpost_key_con = '';
          if(null !== Input::get('campaign_id'))
          {
            $inpost_key_con = ' AND inpost.campaign='.Input::get('campaign_id');
          
          }

          $post_id_filter = '';
          // if(null !== Input::get('cmt_type'))
          // {

            //  $query="SELECT Distinct(post_id) FROM temp_inbound_comments cmts LEFT JOIN  temp_inbound_posts  inpost on inpost.id=cmts.post_id ".
            // " LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
            // " WHERE inpost.id IS NOT NULL".
            // " and  parent='' AND inpost.campaign is not null AND tag_flag=1 AND  (DATE(inpost.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')"  . $inpost_key_con;

            /* $query = "SELECT Distinct(id) post_id ".
              "FROM temp_inbound_posts inpost  WHERE inpost.campaign is not null and ".
              "  (DATE(inpost.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')"  . $inpost_key_con ;*/


             $query = "SELECT Distinct(id) post_id ".
              "FROM temp_inbound_posts inpost  WHERE inpost.campaign is not null AND YEAR(DATE(inpost.created_time)) = '" .$year. "'" . $inpost_key_con ;

            $data = DB::select($query);
            // dd($data);
               $id = [];
               $id_string = '';
               foreach ($data as $key => $value) {
                $id[] = $value->post_id;
               }
                 $id_string = implode('","', $id);
                 // dd($id_string);
                 $id_string = '("'.$id_string.'")';
                 $post_id_filter = ' AND incmt.post_id In '.$id_string ;
                 // dd($post_id_filter);
    
          // }
         // dd($post_id_filter);


            /*$query_comment = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
              "FROM temp_comments cmts  WHERE cmts.isHide=0 and ".
              "  (DATE(cmts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')" . $add_inbound_con . $comment_key_con ;*/

              $query_comment = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
              "FROM temp_comments cmts  WHERE cmts.isHide=0 AND YEAR(DATE(cmts.created_time)) = '" .$year. "' " . $add_inbound_con . $comment_key_con ;

            $query_comment_result = DB::select($query_comment);


            /*$query_post ="SELECT count(*) total,sum(IF(posts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(posts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(posts.checked_sentiment = 'neutral' OR  posts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_posts posts ".
             " WHERE posts.campaign = ".Input::get('campaign_id')." AND posts.isHide=0 and  (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " . $add_inbound_con ;*/
             $query_post ="SELECT count(*) total,sum(IF(posts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(posts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(posts.checked_sentiment = 'neutral' OR  posts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_posts posts ".
             " WHERE posts.campaign = '".Input::get('campaign_id')."' AND posts.isHide=0 AND YEAR(DATE(posts.created_time)) = '" .$year. "'" . $add_inbound_con ;


            $query_post_result = DB::select($query_post);
           // dd($query_post_result);
            /*$query_inpost = "SELECT count(*) total,COALESCE(sum(IF(inpost.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(inpost.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(inpost.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(inpost.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(inpost.checked_sentiment = 'neutral', 1, 0)),0) neutral,COALESCE(sum(Liked)) likeTotal,COALESCE(sum(Love)) loveTotal,COALESCE(sum(Haha)) hahaTotal,COALESCE(sum(Wow)) wowTotal,COALESCE(sum(Sad)) sadTotal,COALESCE(sum(Angry)) angryTotal, (SUM(Love) + SUM(Haha) + SUM(Wow) + SUM(Sad) + SUM(Angry) + SUM(Liked)) reactTotal,COALESCE(sum(shared)) shareTotal ".
              "FROM temp_inbound_posts inpost  WHERE inpost.campaign is not null and ".
              "  (DATE(inpost.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')"  . $inpost_key_con  ;*/

              $query_inpost = "SELECT count(*) total,COALESCE(sum(IF(inpost.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(inpost.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(inpost.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(inpost.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(inpost.checked_sentiment = 'neutral', 1, 0)),0) neutral,COALESCE(sum(Liked)) likeTotal,COALESCE(sum(Love)) loveTotal,COALESCE(sum(Haha)) hahaTotal,COALESCE(sum(Wow)) wowTotal,COALESCE(sum(Sad)) sadTotal,COALESCE(sum(Angry)) angryTotal, (SUM(Love) + SUM(Haha) + SUM(Wow) + SUM(Sad) + SUM(Angry) + SUM(Liked)) reactTotal,COALESCE(sum(shared)) shareTotal ".
              "FROM temp_inbound_posts inpost  WHERE inpost.campaign is not null AND YEAR(DATE(inpost.created_time)) = '" .$year. "'" . $inpost_key_con  ;

            $query_inpost_result = DB::select($query_inpost);
// dd($query_inpost_result);
             /*$query_commentcount = "SELECT count(*) total ,COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(incmt.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(incmt.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(incmt.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
              "FROM temp_inbound_comments incmt join temp_inbound_posts inpost on inpost.id=incmt.post_id WHERE parent=''  AND inpost.campaign = ".Input::get('campaign_id')." AND tag_flag=1 AND". 
              " (DATE(incmt.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')"  . $inpost_key_con ;*/
              $query_commentcount = "SELECT count(*) total ,COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(incmt.checked_sentiment = 'NA', 1, 0)),0) NA, 
              COALESCE(sum(IF(incmt.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(incmt.checked_sentiment = 'neg', 1, 0)),0) negative,
              COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
              "FROM temp_inbound_comments incmt join temp_inbound_posts inpost on inpost.id=incmt.post_id WHERE parent='' 
               AND inpost.campaign = '".Input::get('campaign_id')."' AND tag_flag=1 AND YEAR(DATE(incmt.created_time)) = '" .$year. "'". $inpost_key_con ;
              //dd($query_commentcount);

            $query_commentcount_result = DB::select($query_commentcount);
// 
        // dd($query_commentcount_result);
            // dd($query_inpost_result);
           /* $query_incomment = "SELECT count(*) total,COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(incmt.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(incmt.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(incmt.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
              "FROM temp_inbound_comments incmt  WHERE  parent=''  AND tag_flag=1 AND". 
              " (DATE(incmt.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')"  ;*/

               $query_incomment = "SELECT count(*) total,COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(incmt.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(incmt.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(incmt.checked_sentiment = 'neg', 1, 0)),0) negative,
               COALESCE(sum(IF(incmt.checked_sentiment = 'neutral', 1, 0)),0) neutral  ". "FROM temp_inbound_comments incmt  WHERE  parent=''  AND tag_flag=1 AND YEAR(DATE(incmt.created_time)) = '" .$year. "'";

            $query_incomment_result = DB::select($query_incomment);
             

            //  $query_article ="SELECT count(*) total FROM temp_web_mentions  article".
            // " WHERE (article.isHide=0 or article.isHide is null) AND (DATE(created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " .  $article_key_con;
            /*$query_article ="SELECT count(*) total FROM temp_web_mentions  article".
            " WHERE article.campaign = ".Input::get('campaign_id')." AND (article.isHide=0 or article.isHide is null) AND (DATE(created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') ";*/

            $query_article ="SELECT count(*) total FROM temp_web_mentions  article".
            " WHERE article.campaign = '".Input::get('campaign_id')."' AND (article.isHide=0 or article.isHide is null) AND YEAR(DATE(article.created_time)) = '" .$year. "' ";
         
            $query_article_result = DB::select($query_article);

            foreach($query_inpost_result as $val)
            {
              $post_total = $val->total;
            }

            foreach($query_commentcount_result as $val)
            {
              $cmt_total = $val->total;
            }
            $fan_count = 0;

               if($post_total != 0 OR $cmt_total != 0){
         

                $row = "Select page_name from campaignGroup where id='".Input::get('campaign_id')."' ";
                $res = DB::select($row);
                $page_name='';
                foreach($res as $val) $page_name = $val->page_name;
                if(null !== $page_name && '' !== $page_name)
                  $page_name=  $this->Format_Page_name_single($page_name);
          

                $InboundPages_result=InboundPages::raw(function ($collection) use($page_name) {
                return $collection->aggregate([
                    [
                    '$match' =>[
                         '$and'=> [ 
                         ['page_name'=>$page_name],
                                   
                                  ]
                    ] 
                               
                   ],

                   ['$sort' =>['_id'=>1]]
                    
                ]);
              })->toArray();

                $page_id='';
                foreach ($InboundPages_result as  $key => $row)  if(isset($row['id'])) $page_id = $row['id'];




                $MongoFan = DB::connection('mongodb')->collection('followers')->orderBy('createdAt','desc')->where('page_name',$page_name)->take(1)->get();
              
                // $fan_count=0;
                foreach ($MongoFan as  $key => $row) {
              // dd($row);
                      if(isset($MongoFan[$key]['fan_count'])){
                        $fan_count=0;
                        $fan_count = $row['fan_count'];
                       //  $filterdata[]=[
                       //  'fan' => $MongoFan[$key]['fan_count'],
                       //  'page_id'  =>$page_id,
                       // ];
                      }
            }
          }
          
            // dd($filterdata);s
            // dd($fan_count);
            // echo json_encode($filterdata);
            //endgetpagelike
            // dd(array($query_comment_result,$query_post_result,$query_article_result));
            // dd($query_article);
            // $data_week_post_result=[];
            // $ov_positive=0;$ov_negative=0;$ov_neutral=0;$ov_total=0;
            // foreach ($query_week_post_result  as $key => $value) {
            //  // dd($value->positive);
            //   $ov_positive = $ov_positive+$value->positive;
            //   $ov_negative = $ov_negative+$value->negative;
            //   $ov_neutral = $ov_neutral+$value->neutral;
            //    $ov_total = $value->total;
            //      # code...
            // }

            // $data_week_post_result[]=array(
            //                 'ov_positive'     =>$ov_positive,
            //                 'ov_negative' => $ov_negative,
            //                 'ov_neutral' => $ov_neutral,
            //                 'ov_total' => $ov_total
            //               );
             // dd($fan_count);
              //dd( json_encode(array($query_comment_result,$query_post_result,$query_incomment_result,$query_inpost_result,$query_article_result,$query_commentcount_result,$fan_count)));
            echo json_encode(array($query_comment_result,$query_post_result,$query_incomment_result,$query_inpost_result,$query_article_result,$query_commentcount_result,$fan_count));
    }

     function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 

     public function getcampaignMention()
    {
      $year = Input::get('fday');
      $senti_con = '';
       if(null !== Input::get('sentiType'))
      {
        $senti_con = " and posts.checked_sentiment='".Input::get('sentiType')."'";
      }

      $brand_id=Input::get('brand_id');
 
        if(null !== Input::get('fday'))
      {
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
        $dateBegin=date('Y-m-d');
      }
    

      if(null !==Input::get('sday'))
      {

        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      
      else
      {
        $dateEnd=date('Y-m-d');
      }


      $add_inbound_con ="";
      $inboundpages=$this->getInboundPages($brand_id);
      if ($inboundpages !== '')
      {
        $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
      }
       $period_type = '';
      $additional_filter=" AND 1=1 ";
      
        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

            
          if($period_type == 'week')
          {
           //$additional_filter .= " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";   
           

          }
          else
          {
              //$additional_filter .= " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
              
          }

       }

        if(null !== Input::get('tsearch_senti'))
        {
         $additional_filter .= ' AND ' . $this->getSentiWhereGen(Input::get('tsearch_senti'),'posts');
        }
          // $keyword_data = $this->getprojectkeyword($brand_id);
      
        // if(isset($keyword_data [0]['main_keyword']))

          $campaign_filter_con = '';
          // dd(Input::get('campaign_id'));
          if(null !== Input::get('campaign_id'))
          {
            $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
            // $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'posts');
            // $campaign_filter_con = " and ".$campaign_filter_con;
           
            // $keyword_data = $this->getCampaignKeywordFilter(Input::get('campaign_name'),$brand_id);
            // if(count($keyword_data)>0)
            // $additional_filter  .=  " AND ( " . $this->getcampaignfilter_MYSQL($keyword_data,'posts') . ")"; 
            // else
            // $additional_filter .= ' AND 1=2';
          }

// dd(Input::get('campaign_name'));

    // dd($campaign_filter_con);
 
        if(null !== Input::get('tsearch_keyword'))
      {
       $additional_filter .=' AND '.$this->getKeywordWhereGen(Input::get('tsearch_keyword'),'posts');
      }
           // dd($campaign_filter_con);   
       

      $query='';
      $query_result=[];
      //dd($campaign_filter_con);
      $query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,replace(shared, ',', '') shared,'".$brand_id."'brand_id,IF(posts.sentiment = '','~',posts.sentiment) old_senti,IF(posts.sentiment = posts.checked_sentiment,'','human predict!') hp,IF(posts.checked_sentiment <> '',posts.checked_sentiment,posts.sentiment) sentiment,posts.decided_keyword,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
      " created_time,posts.link,posts.full_picture,posts.type post_type,posts.campaign campaign,posts.emotion,posts.isDeleted,cmts.checked_sentiment ".
      " cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.id cmt_id,cmts.isHide cmt_isHide,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
      " ELSE 'mdi-star' ".
      " END) isBookMark ".
      "FROM temp_posts posts LEFT JOIN temp_comments cmts on posts.id=cmts.post_id ".
      " WHERE posts.campaign = '".Input::get('campaign_id')."' AND posts.isHide=0 and YEAR(DATE(posts.created_time)) = '" .$year. "' ". $add_inbound_con . $additional_filter .  $campaign_filter_con . $senti_con . 
      " ORDER by timestamp(posts.created_time) DESC ";


      $query_result = DB::select($query);

      

// dd($query_result);
      $data = [];
       $data_mention=[];


       foreach ($query_result as  $key => $row) {
        //change page name from id to orginal user register name.
        
      $comment_count=0;
       
      $pos=($row->cmt_sentiment=='pos' && $row->parent=== '' )?1:0 ;
      $neg=($row->cmt_sentiment=='neg' && $row->parent=== '' )?1:0;
      $cmtneutral=($row->cmt_sentiment=='neutral' && $row->parent=== '' )?1:0;
      $cmtNA=($row->cmt_sentiment=='NA' && $row->parent=== '' )?1:0;
      $cmtneutral=(int)$cmtneutral+(int) $cmtNA;
      $anger =($row->cmt_emotion=='anger' && $row->parent==='' )?1:0 ;
      $interest=($row->cmt_emotion=='interest' && $row->parent==='' )?1:0;
      $disgust=($row->cmt_emotion=='disgust' && $row->parent ==='')?1:0;
      $fear=($row->cmt_emotion=='fear' && $row->parent=== '' )?1:0;
      $joy=($row->cmt_emotion=='joy' && $row->parent==='')?1:0;
      $like=($row->cmt_emotion=='like' && $row->parent==='' )?1:0;
      $love=($row->cmt_emotion=='love' && $row->parent==='' )?1:0;
      $neutral=($row->cmt_emotion=='neutral' && $row->parent==='' )?1:0;
      $sadness=($row->cmt_emotion=='sadness' && $row->parent==='' )?1:0;
      $surprise=($row->cmt_emotion=='surprise' && $row->parent==='' )?1:0;
      $trust=($row->cmt_emotion=='trust' && $row->parent==='')?1:0;
       
         
         $message=$this->readmore($row->message);

        $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
        $message = $this->highLightText($campaign_keyword,$message);

         $total = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry;
         //$message=$row->message;
                  if (isset($row ->id))
         {
            // $comment_count=($row->parent==='')?1:0;
               if (isset($row ->cmt_id) && $row ->cmt_isHide==0 )   $comment_count=1;


                       $id= $row ->id;
         
          if (!array_key_exists($id , $data)) {
          $data[$id] = array(
                      'id'     =>$row->id,
                      'message' => $message,
                      'page_name' =>$row->page_name,
                      'created_time' =>$row->created_time,
                      'link' =>$row->link,
                      'full_picture' =>$row->full_picture,
                      'sentiment' =>$row->sentiment,
                      'hp' => $row->hp,
                      'old_senti' => $row->old_senti,
                      'emotion' =>$row->emotion,
                      'brand_id' => $row->brand_id,
                      'decided_keyword' => $row->decided_keyword,
                      'positive'=> $pos,'negative'=>$neg,'cmtneutral'=>$cmtneutral,
                      'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,'shared'=>$row->shared,
                      'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                      'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                      'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                      'total'=>$total,
                      'comment_count'=>$comment_count,
                      'post_type'=>$row->post_type,
                      'isDeleted'=>$row->isDeleted ,
                      'isBookMark'=>$row->isBookMark,
                      'campaign' => $row->campaign,
                  );
      }
      else
      {
       $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
       $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
       $data[$id]['cmtneutral'] = (int) $data[$id]['cmtneutral'] +$cmtneutral;
       $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
       $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
       $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
       $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
       $data[$id]['like'] = (int) $data[$id]['like'] + $like;
       $data[$id]['love'] = (int) $data[$id]['love'] + $love;
       $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
       $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
       $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
       $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;
       $data[$id]['comment_count'] = (int) $data[$id]['comment_count'] + $comment_count;



      }


      }
       
                  }

             if(null !== Input::get('filter_overall'))
            {
              if( Input::get('filter_overall') === 'pos')//
              {     foreach ($data as $key => $value) {

        if( (int)$value['positive'] <  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'])
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
                }
              else if( Input::get('filter_overall') === 'neg')
              {
           foreach ($data as $key => $value) {

            if((int)$value['positive'] >  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'] &&  (int)$value['cmtneutral'] <> 0)
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }
               else if( Input::get('filter_overall') === 'neutral')
              {
                          foreach ($data as $key => $value) {

                       if( ! ( ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 ) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0)))
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }

            }

            $data_mention=$data;

            return Datatables::of($data_mention)
     
             ->addColumn('post', function ($data_mention){
              $fb_p_id=explode('_', $data_mention['id']);
              $fb_p_id=$fb_p_id[1];
              $page_id='';
              $arr_page_id=explode("-",$data_mention['page_name']);
              if(count($arr_page_id)>0)
              {
                $last_index=$arr_page_id[count($arr_page_id)-1];
                if(is_numeric($last_index))
                $page_id = $last_index;
                else
                $page_id = $data_mention['page_name'];
              }
              else
              {
                $page_id=$data_mention['page_name'];
              }

            $post_Link= 'https://www.facebook.com/' . $page_id .'/posts/' . $fb_p_id ;
            $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
           

                    $total = (int) $data_mention['positive'] +(int) $data_mention['negative']+(int) $data_mention['cmtneutral'];
                    $pos_pcent='';$neg_pcent='';$neutral_pcent='';
                   
                     $html= '<div class="profiletimeline"><div class="sl-item">
                                          <div class="sl-left"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                          <div class="form-material sl-right"> <div> <div> <a href="'.$page_Link.'" class="link"  target="_blank">'.$data_mention['page_name'].' </a>';

                                           if((int)$data_mention['campaign'] == 0)
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline " data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="false">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button>';
                        else
                        $html.='<button type="button" class="btn-campaign btn btn-secondary btn-outline active" data-toggle="button"  id="campaign_'.$data_mention['id'].'" aria-pressed="true">
                                            <i class="mdi mdi-flag-outline text" aria-hidden="true"></i>
                                            <i class="mdi mdi-flag text-active text-warning" aria-hidden="true"></i>
                                        </button></div>';

                    if($data_mention['hp'] <> '')
                    $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Sentiment: '.$data_mention['old_senti'].'  => '. $data_mention['sentiment'].'" data-toggle="tooltip"></i></a>';

                    $html.='<span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data_mention['created_time'].'</span> </div>';

                    

					 $html.='<p>Sentiment : <select id="sentiment_'.$data_mention['id'].'"  class="form-material form-control in-comment-select edit_senti ';
					                   if($data_mention['sentiment']=="")$html.='"><option value="" selected="selected" ></option>';
					                   if($data_mention['sentiment']=="pos")$html.='text-success" >';
					                   if($data_mention['sentiment']=="neg")$html.='text-red" >';
					                   if($data_mention['sentiment']=="neutral" || $data_mention['sentiment'] == "NA")$html.='text-warning" >';
                
                    $html.='<option value="pos" class="text-success"';
                    if($data_mention['sentiment'] == "pos")
                    $html.= 'selected="selected"';
                    $html.= '>Positive</option><option value="neg" class="text-red"';
                    if($data_mention['sentiment'] == "neg")
                    $html.= 'selected="selected"';
                    $html.= '>Negative</option><option value="neutral" class="text-warning"';
                    if($data_mention['sentiment'] == "neutral" || $data_mention['sentiment'] == "NA")
                    $html.= 'selected="selected"';      
                    $html.= '>Neutral</option></select>';

                            $html.='<div style="display:none"  align="center" style="vertical-align: top;" id="spin_'.$data_mention['id'].'"> <img src="assets/images/ajax-loader.gif" id="loader"></div>';
             
                 
                             
                      $html.='<p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['id'].'">View More</a></p>';
                              
                    $html.= '</div></div>
                               
                    <div class="sl-right" style="color:#0092ff">';
                   
                    if((int)$data_mention['liked'] > 0)
                    $html.='👍 '.$this->thousandsCurrencyFormat($data_mention['liked']);
                   if((int)$data_mention['loved'] > 0)
                    $html.=' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']);
                   if((int)$data_mention['haha'] > 0)
                    $html.=' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']);
                   if((int)$data_mention['wow'] > 0)
                    $html.=' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']);
                   if((int)$data_mention['sad'] > 0)
                    $html.=' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']);
                   if((int)$data_mention['angry'] > 0)
                    $html.=' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']);
                   if((int)$data_mention['comment_count'] > 0)
                    $html.='<a href="javascript:void(0)"  class="see_comment" id="seeComment_'.$data_mention['id'].'"  style="float:right"><span class="postPanel_actionIconContainer" id="idb5d"><span class="postPanel_icon mdi mdi-comment-outline" title="Comments"></span><span>'.$data_mention['comment_count'].'</span></span></a>';
                    $html.=' </div><div class="sl-right" align="right"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-gray post_hide"  id="hide_'.$data_mention['id'].'" >Hide Post</a> <a href="'.$post_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See Post on FB</a></div></div> </div>';

                 
              
                      return $html;
                     

                  })
        ->addColumn('reaction', function ($data_mention) {
                      return "<span style='font-weight:bold'>" . number_format($data_mention['total'])."</span>";
                  }) 

        
        ->addColumn('share', function ($data_mention) {
                      return  "<span style='font-weight:bold'>" . number_format($data_mention['shared'])."</span>";
                  }) 
       

              
            ->rawColumns(['post','reaction','share'])
         
            ->make(true);

    }
      public function getcampaignMentionCmt()
    {
      $year = Input::get('fday');
      $senti_con = '';
       if(null !== Input::get('sentiType'))
      {
        $senti_con = " and cmts.checked_sentiment='".Input::get('sentiType')."'";
      }


      $brand_id=Input::get('brand_id');
      
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment ='';
      
      $period_type = '';
      $additional_filter=" AND 1=1 ";
      
        if(null !==Input::get('period'))
        {
             $period=Input::get('period');
              $char_count = substr_count($period,"-");
              $format_type = '';
              if($char_count == 1)
              {
                  $format_type ='%Y-%m';
              }
              else if($char_count == 2)
              {
                   $format_type ='%Y-%m-%d';
              }   
              else if($char_count > 2)
              {
                  /*this is week*/
                  $pieces = explode(" - ", $period);
                  $format_type ='%Y-%m-%d';
                  $period_type ='week';
              }

            
              if($period_type == 'week')
              {
               //$additional_filter .= " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

              }
              else
              {
                  //$additional_filter .= " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
              }

        }
    

            if(null !== Input::get('sentiment'))
            $filter_sentiment=Input::get('sentiment');

          
            // global filter
             $add_global_filter='';
     
     

              if(null !== Input::get('fday'))
            {
                $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            }
            else
            {
                $dateBegin=date('Y-m-d');
            }
          

          if(null !==Input::get('sday'))
          {

               $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
          }
          
          else
          {
              $dateEnd=date('Y-m-d');

          }



            $add_con=' 1=1';

         if(null !== Input::get('tsearch_senti'))
              {
              
                $add_con .= ' AND ' . $this->getSentiWhereGen(Input::get('tsearch_senti'),'cmts');
              }

              $keyword_data =[];

          $campaign_filter_con = '';
          $campaign_keyword = '';
          if(null !== Input::get('campaign_id'))
          {
            $campaign_keyword= $this->getCampaignKeywordFilter(Input::get('campaign_id'),$brand_id);
            // $campaign_filter_con = $this->getcampaignfilter_MYSQL($campaign_keyword,'cmts');
            // $campaign_filter_con = " and ".$campaign_filter_con;
           
            // $keyword_data = $this->getCampaignKeywordFilter(Input::get('campaign_name'),$brand_id);
            // if(count($keyword_data)>0)
            // $additional_filter  .=  " AND ( " . $this->getcampaignfilter_MYSQL($keyword_data,'posts') . ")"; 
            // else
            // $additional_filter .= ' AND 1=2';
          }
          
        

         if(null !== Input::get('tsearch_keyword'))
           { 
               $add_con .= ' AND ' . $this->getKeywordWhereGen(Input::get('tsearch_keyword'),'cmts');
           }
         
               if(null !== Input::get('tsearch_interest') && Input::get('tsearch_interest') === "true")
            {
              // echo "hihi";
                $add_con .=" and cmts.interest = 1";

               }

          if(null !== Input::get('post_id'))
           $add_con .= " and cmts.post_id='".Input::get('post_id')."'"; 

          $tags =$this->gettags($brand_id);

          $date_filter='';
          if(null !== Input::get('fday'))
          {
           
             //$date_filter=" (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
             $date_filter=" YEAR(DATE(cmts.created_time)) = '" .$year. "' ";
          }
          else
          {
            $date_filter = ' 1=1 ';
          }
          $add_inbound_con ="";
          $inboundpages=$this->getInboundPages($brand_id);
          if ($inboundpages !== '')
          {
            $add_inbound_con = " and (cmts.page_name  not in (".$inboundpages.") or cmts.page_name is NULL)";
          }

           $query="SELECT cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
          " cmts.checked_sentiment sentiment,cmts.emotion,cmts.page_name, ". 
          " IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp ".
          " FROM temp_comments cmts  ".
          " WHERE cmts.isHide=0 and ".$date_filter."  AND ". $add_con .  $add_inbound_con . $additional_filter . $campaign_filter_con . $senti_con .
          " ORDER by timestamp(cmts.created_time) DESC ".
          " LIMIT 0,1";//added by PPH for hiding mention comments;

           // dd($query);
            // print_r($query);
            // return;
          $data = DB::select($query);
// dd($data);

          $permission_data = $this->getPermission(); 
          // if($is_competitor == "false")

           

      return Datatables::of($data)
            
           ->addColumn('post_div', function ($data)   use($permission_data,$campaign_keyword ) {
             $post_message_tag='';
            
            $page_name=$data->page_name ;
            
            //generate comment link
            $profilePhoto='';$ProfileName='Unknown';
            $profileLink='javascript:void(0)';
            $fb_p_id=explode('_', $data->post_id);
            $fb_p_id=$fb_p_id[1];
            $fb_c_id=explode('_', $data->id);
            $fb_c_id=$fb_c_id[1];
            if($page_name <> '')
            $ProfileName=$page_name;

            

            $page_id='';
            $arr_page_id=explode("-",$page_name);
            if(count($arr_page_id)>0)
            {
              $last_index=$arr_page_id[count($arr_page_id)-1];
              if(is_numeric($last_index))
              $page_id = $last_index;
              else
              $page_id = $page_name;
            }
            else
            {
              $page_id=$page_name;
            }
           //  $message = $data->message;
           //  $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
           // $message = $this->highLightText($campaign_keyword,$message);

            $comment_Link= 'https://www.facebook.com/'.$fb_c_id;
            $classforbtn='';$stylefora='';$bgcolor='';
                  if($data->sentiment=='neg')
                    {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
                  else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                    else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}

                    $html = '<div  id="'.$data->id.'"  class="profiletimeline"><div class="sl-item">';
                    if($profilePhoto<>'')
                    $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                    else
                    /*$html .= '<div class="sl-left user-img"> <span class="round"><i class="mdi mdi-comment-account"></i></span> </div>';*/
                  $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                   
                     $html .='<div class="form-material sl-right"><div>  <div>
                  <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a> ' ;
                   if($data->hp <> '')
                  $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input" data-toggle="tooltip"></i></a>';

                  $html .=' <span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
                           
                   $html.='<p>Sentiment : <select id="sentiment_'.$data->id.'"  class="form-material form-control in-comment-select edit_senti ';
                   if($data->sentiment=="")$html.='"><option value="" selected="selected" ></option>';
                   if($data->sentiment=="pos")$html.='text-success" >';
                   if($data->sentiment=="neg")$html.='text-red" >';
                   if($data->sentiment=="neutral" || $data->sentiment == "NA")$html.='text-warning" >';
                
                    $html.='<option value="pos" class="text-success"';
                    if($data->sentiment == "pos")
                    $html.= 'selected="selected"';
                    $html.= '>Positive</option><option value="neg" class="text-red"';
                    if($data->sentiment == "neg")
                    $html.= 'selected="selected"';
                    $html.= '>Negative</option><option value="neutral" class="text-warning"';
                    if($data->sentiment == "neutral" || $data->sentiment == "NA")
                    $html.= 'selected="selected"';      
                    $html.= '>Neutral</option></select>';

                    $html.='</p><p class="m-t-10">'.$this->highLightText($campaign_keyword,$data->message).'</p>'.$post_message_tag.'</div>';
                    if($data->cmtType =='sticker' || $data->cmtType =='photo' || $data->cmtType =='share' || $data->cmtType =='share_large_image' || $data->cmtType =='animated_image_share'  )
                       {
                          $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'.$data->id.'">'.
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                              '<img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">'.
                      '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                       else if($data->cmtType =='video_inline' || $data->cmtType =='animated_image_video' || $data->cmtType =='video_share_youtube' || $data->cmtType =='video_share_highlighted')
                       {
                         $html .= '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'.
                          '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'.
                          '<iframe src="https://www.facebook.com/plugins/video.php?href='.$data->cmtLink.'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'.
                          '</span><span class="postPanel_nameTextContainer"></span></a>';
                       }
                    $html.='<div class="sl-right">';
                            if((int)$data->cmtLiked > 0)
                            $html.='👍 '.$this->thousandsCurrencyFormat($data->cmtLiked);
                           if((int)$data->cmtLove > 0)
                            $html.=' ❤️ '.$this->thousandsCurrencyFormat($data->cmtLove);
                           if((int)$data->cmtHaha > 0)
                            $html.=' 😆 '.$this->thousandsCurrencyFormat($data->cmtHaha);
                           if((int)$data->cmtWow > 0)
                            $html.=' 😮 '.$this->thousandsCurrencyFormat($data->cmtWow);
                           if((int)$data->cmtSad > 0)
                            $html.=' 😪 '.$this->thousandsCurrencyFormat($data->cmtSad);
                           if((int)$data->cmtAngry > 0)
                            $html.=' 😡 '.$this->thousandsCurrencyFormat($data->cmtAngry);
                            $html.='</div>
                        <div class="sl-right" style="float: right;">
                        <div style="display: inline-block"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-gray comment_hide"  id="hide_'.$data->id.'" >Hide Comment</a> <a href="'.$comment_Link.'" class="btn waves-effect waves-light btn-sm btn-green" target="_blank">See on FB</a></div>
                      <div style="display: inline-block" class="btn-group">';
                  
              $html.='</div></div></div></div>';

                    return $html;

                })
      
          ->rawColumns(['post_div'])
       
          ->make(true);

    }
     public function getCampaignGroup()
    {
       $brand_id=Input::get('brand_id');
       $page_name=Input::get('page_name');
       
       // $brand_id = 34;
     
       // $keyword_data = $this->getprojectkeywordCombine($brand_id);
       $query="select id,name from campaignGroup where brand_id=" .$brand_id ;
       $keyword_data = DB::select($query);
       echo json_encode($keyword_data);
    }
    public function getOtherCampaign()
    {
      $brand_id = Input::get('brand_id');
      $page_name = Input::get('page_name');
      $year = Input::get('year');
      //$query="select id,name from campaignGroup where page_name = " ."'$page_name'";
      $query="select id,name from campaignGroup where YEAR(DATE(created_at)) = '" .$year. "'"." and page_name = " ."'$page_name'";
      //dd($query)
      $keyword_data = DB::select($query);
       // dd($keyword_data);
       echo json_encode($keyword_data);
    }


    function highLightText($keyword_data,$message)
{
	// dd($keyword_data);
  $highligh_string  = $message;
   
  usort($keyword_data, function($a, $b) {
    return strlen($b->campaign_keyword) - strlen($a->campaign_keyword);
});

  foreach ($keyword_data as $key => $value) {
 
   $highligh_string = str_ireplace($value->campaign_keyword, "<span style='background:#FFEB3B'>".$value->campaign_keyword."</span>",$highligh_string,$i);
}
  

  return $highligh_string;
}

}
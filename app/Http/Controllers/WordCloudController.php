<?php

namespace BIMiner\Http\Controllers;

use Illuminate\Http\Request;
use BIMiner\Http\Controllers\GlobalController;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use BIMiner\WordCloudDict;
use Illuminate\Support\Facades\Input;

class WordCloudController extends Controller
{
     use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($user=Auth::user()){
          $title="Word Dictionary";
          $source='in';
          $pid=Input::get("pid");
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
          $accountPermission = '';
          return view('word_dict')->with('project_data',$project_data)
                                    ->with('permission_data',$permission_data )
                                    ->with('count',$count)
                                    ->with('source',$source)
                                    ->with('title',$title)
                                    ->with('brand_id',$pid)
                                    ->with('accountPermission',$accountPermission);


        }
        else{
          $title="";
          return view('auth.login',compact('title'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    //      $validatedData = $request->validate([
    //     'word' => 'required|unique:word_dict',
    // ]);
         WordCloudDict::create([
            'word' => $request->word,
            'brand_id' => $request->brand_id,
            'created_at' => now()->toDateTimeString(),
            'updated_at' =>now()->toDateTimeString()
          
        ]);

          $accountPermission = '';
      
      return redirect()->route('wordcloud.index',['source' => 'in','pid' => $request->brand_id,'accountPermission' => $accountPermission])
                        ->with('message','Successfully added a new word.');

    }
    public function getwordlist()
    {
         $brand_id = Input::get('brand_id');
         $wordlist = WordCloudDict::select(['id','word', 'created_at'])->where('brand_id',$brand_id)->orderBy('created_at','desc');

         return Datatables::of($wordlist)
         ->addColumn('action',function($wordlist) use($brand_id){
            return '<a href="'. route('wordcloud.edit', ['id'=>$wordlist->id,'pid' => $brand_id]) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
                 <button class="btn btn-xs btn-danger btn-delete" data-remote="' . route('wordcloud/delete',$wordlist->id) . '"><i class="mdi mdi-delete"></i>Delete</button>';
         })
         ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

          $title="Word";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission();
          $brand_id=Input::get("pid");
          // dd($brand_id);
          $checked_data = $this->checkUserForProject($brand_id);
          if(count($checked_data)>0)
          {
          $companyData=$this->getCompanyData($brand_id);
          // $data = WordCloudDict::find($id);
          $data = WordCloudDict::whereBrandIdAndId($brand_id, $id)->first();

          //$category = tagCategory::select(['id', 'category_name'])->orderBy('category_name')->get();
          $source='in';
          $accountPermission = '';

         return view('word_dict',compact('data','title','project_data','count','source','permission_data','brand_id','companyData','accountPermission'));
          }
          else
          {
            return abort(404);
          }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand_id = $request->brand_id;
        // $validatedData = $request->validate([
        // 'word' => 'required|unique:word_dict,word,'.$id.',id,brand_id,'.$request->brand_id,
        // ]);
        
        // dd($brand_id);
         WordCloudDict::where('id',$id)->update([
            'word' => $request->word,
            'updated_at' =>now()->toDateTimeString()
          
        ]);
          $accountPermission = '';
      
      return redirect()->route('wordcloud.index',['pid' => $brand_id,'accountPermission' => $accountPermission])
                        ->with('message','Successfully updated a new word.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $brand_id = Input::get('brand_id');
      WordCloudDict::where('id',$id)->delete();
          $accountPermission = '';

      return redirect()->route('wordcloud.index',['pid' => $brand_id,'accountPermission' =>$accountPermission]);
                       

    }
}

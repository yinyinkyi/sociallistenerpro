<?php

namespace BIMiner\Http\Controllers;
use Excel;
use BIMiner\MongodbData;
use BIMiner\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use BIMiner\ProjectKeyword;
use Yajra\Datatables\Datatables;
use PDF;
use Dompdf\Dompdf;
class ReportController extends Controller
{
    //
    use GlobalController;
    public function SentiPredictCondition()
    {
          $brand_id=Input::get('brand_id');
          $predict_type=Input::get('predict_type');
          if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
           if($predict_type == "tag")
          {
            $query="SELECT DATE_FORMAT(manual_date, '%d-%M-%Y') manual_date,DATE_FORMAT(created_time, '%d-%M-%Y') created_time, count(*) total, sum(IF(tags_id=checked_tags_id,1,0)) nlp,sum(IF(tags_id <>checked_tags_id,1,0)) hp".
            " FROM temp_".$brand_id."_inbound_comments WHERE (DATE(manual_date) BETWEEN '".$dateBegin."' AND '".$dateEnd."') and checked_tags_id <> ''   group by  DATE_FORMAT(manual_date, '%d-%M-%Y') order by timestamp(manual_date)  DESC";
          }
        else{
            $query="SELECT DATE_FORMAT(manual_date, '%d-%M-%Y') manual_date,DATE_FORMAT(created_time, '%d-%M-%Y') created_time, count(*) total, sum(IF(sentiment=checked_sentiment,1,0)) nlp,sum(IF(sentiment <>checked_sentiment,1,0)) hp".
            " FROM temp_".$brand_id."_inbound_comments WHERE (DATE(manual_date) BETWEEN '".$dateBegin."' AND '".$dateEnd."')   group by  DATE_FORMAT(manual_date, '%d-%M-%Y') order by timestamp(manual_date)  DESC";

           }

 $result = DB::select($query);

     return Datatables::of($result)
     ->addColumn('month', function ($result) {
                return $result->created_time;
            }) 
    ->addColumn('manual_date', function ($result) {
                return $result->manual_date;
            }) 
     ->addColumn('total', function ($result) {
                return number_format($result->total);
            }) 
     ->addColumn('nlp', function ($result) {
                return  number_format($result->nlp);
            }) 
     ->addColumn('hp', function ($result) {
                return  number_format($result->hp);
            }) 

      ->make(true);
    }

    public function HumanPredictRecord()
    {
          $brand_id=Input::get('brand_id');
          $predict_type=Input::get('predict_type');
          if(null !== Input::get('fday'))
          {
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
          }
          else
          {
          $dateBegin=date('Y-m-d');
          }
        
    
        if(null !==Input::get('sday'))
        {
    
     $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        }
        
        else
        {
    $dateEnd=date('Y-m-d');
    
               }
//  $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 12 19')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2019 01 19')));
               $data=[];
        //  $brand_id=34;
          //$predict_type='tag';
          
          if($predict_type == "tag")
          {
            $query="SELECT DATE_FORMAT(created_time, '%d-%m-%Y %h:%i %p') created_time,DATE_FORMAT(manual_date, '%d-%m-%Y %h:%i %p') manual_date,message,tags_id nlp,checked_tags_id human,decided_keyword".
            " FROM temp_".$brand_id."_inbound_comments WHERE (DATE(manual_date) BETWEEN '".$dateBegin."' AND '".$dateEnd."') and  tags_id <> checked_tags_id and checked_tags_id <> '' order by timestamp(manual_date) DESC";
            $result = DB::select($query);
           
            foreach ($result as  $key => $row) {
                $nlp ='';$hp ='';
                $nlp = $row->nlp;
                $arr_nlp=explode(',', $nlp); //string to array
                $nlp_tag =[];
                //loop contain tag in ',' sperated string
                foreach ($arr_nlp as  $key => $tag) {
                    if($tag <> '')
                    {
                        $tagInfo=$this->gettagsByid($tag); //get id  to name
                        if(count($tagInfo)>0)
                     {
                  foreach ($tagInfo as  $key => $taginfo) {
                    $nlp_tag[]=$taginfo->name ; // put result to array
                    }
                     }

                }
            }
            $nlp =implode(',',$nlp_tag); // put array to string name

            $human =  $row->human;
            $arr_human=explode(',', $human); //string to array
            $human_tag =[];
            //loop contain tag in ',' sperated string
            foreach ($arr_human as  $key => $tag) {
                if($tag <> '')
                {
                    $tagInfo=$this->gettagsByid($tag); //get id  to name
                    if(count($tagInfo)>0)
                 {
              foreach ($tagInfo as  $key => $taginfo) {
                $human_tag[]=$taginfo->name ; // put result to array
                }
                 }

            }
        }
        $human =implode(',',$human_tag); // put array to string name
                $data[] =[
                    'created_time' => $row->created_time,
                    'manual_date' => $row->manual_date,
                    'message' =>$row->message,
                    'nlp' =>$nlp,
                    'human' => $human,
                    'decided_keyword'=>$row->decided_keyword,
                ];
           
            }
            // dd($data);
          }
          else{
            $query="SELECT DATE_FORMAT(created_time, '%d-%m-%Y %h:%i %p') created_time,DATE_FORMAT(manual_date, '%d-%m-%Y %h:%i %p') manual_date,message,sentiment nlp,checked_sentiment human,decided_keyword".
" FROM temp_".$brand_id."_inbound_comments WHERE (DATE(manual_date) BETWEEN '".$dateBegin."' AND '".$dateEnd."') and  sentiment <> checked_sentiment order by timestamp(manual_date) DESC";
$result = DB::select($query);
$data = $result;
}

 

     return Datatables::of($data)
      // ->editColumn('created_time', function ($result) {
      //            if (isset($result->created_time)) {
      //           $datetime =date('Y-m-d', strtotime($result->created_time));
      //           return   $datetime;
      //       }
      //       })
            //  ->filterColumn('created_time', function ($query, $keyword) {
            //     $query->whereRaw("DATE_FORMAT(created_time,'%d/%m/%Y') like ?", ["%$keyword%"]);
            // })
    
      ->make(true);
    }
    public function getPostExcelData()
    {
        $page_name = Input::get('page_name');
        $brand_id = Input::get('brand_id');
       
           if(null !== Input::get('fday'))
            {
             $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
            }
            else
            {
            $dateBegin=date('Y-m-d');
            }
            if(null !==Input::get('sday'))
            {

              $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
            }
    
            else
            {
              $dateEnd=date('Y-m-d');
            }
            // $page_name ="wunzinn2home";
            // $brand_id = 17;
            // $dateBegin=date('Y-m-d', strtotime("2018/11/07"));
            // $dateEnd=date('Y-m-d', strtotime("2018/12/07"));

            $query="SELECT posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')".
            "created_time,cmts.message comment_message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p')  comment_date,".
            " cmts.checked_sentiment cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict,cmts.checked_tags_id tags ".
            " FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
            " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND posts.page_name='".$page_name."'" .
            " ORDER by timestamp(posts.created_time) DESC,posts.id,timestamp(cmts.created_time) DESC";
              
            $data = DB::select($query);
        //   dd($query);
         // return $query;
          $post_id='';
          $post_sr_no=0;
          $post_data_array=[];
          $comment_data_array=[];
         $post_data_array[] = array ('No','Post','Date');
         $comment_data_array[] = array ('Post No','Comment','Comment Time','Sentiment','Tags');
          foreach($data as $result)
          {
            $tags_name = $result->tags;
            $tags=explode(',', $tags_name); //string to array
            $arr_tags =[];
           
            //loop contain tag in ',' sperated string
            foreach ($tags as  $key => $tag) {
                if($tag <> '')
                {
                    $tagInfo=$this->gettagsByid($tag); //get id  to name
                    if(count($tagInfo)>0)
                 {
              foreach ($tagInfo as  $key => $taginfo) {
                $arr_tags[]=$taginfo->name ; // put result to array
                }
                 }
               
            }
        }
        $tags_name =implode(',',$arr_tags); // put array to string name
        // if($tags <> '')
        // dd($tags_name);
            if($post_id == $result->id)
            {
                if($result->parent == '')
                {
                    $comment_data_array[] =  array(
                        'Post No'=>$post_sr_no,
                        'Comment'=>$result->comment_message,
                        'Comment Time'=>$result->comment_date,
                        'Sentiment'=>$result->cmt_sentiment,
                        'Tags'=>$tags_name,
    
                      );
                }
                
           
            }
            else
            {
                $post_sr_no=$post_sr_no + 1;
                $post_data_array[] =  array(
                    'No'=>$post_sr_no,
                    'Post'=>$result->message,
                    'Date'=>$result->created_time

                  );
                    
                 
                
                if($result->parent == '')
                {
                    $comment_data_array[] =  array(
                        'Post No'=>$post_sr_no,
                        'Comment'=>$result->comment_message,
                        'Comment Time'=>$result->comment_date,
                        'Sentiment'=>$result->cmt_sentiment,
                        'Tags'=>$tags_name,
    
                      );
                    
                }

                $post_id = $result->id;
               
            }
           
          }
       
         
$path =  public_path('reports/'.$page_name . '_Post_Comment_Data_' . $dateBegin . ' to ' . $dateEnd);
if (file_exists($path)) {
    unlink($path) ;
}

         return Excel::create($page_name . '_Post_Comment_Data_' . $dateBegin . ' to ' . $dateEnd, function($excel) use ($post_data_array,$comment_data_array) {
          $excel->setTitle('Post With Comment Data');
          $excel->setCreator('admin')->setCompany('Bit');
          $excel->setDescription('Show Posts with their related comments');
          $excel->sheet('Post Data', function($sheet) use ($post_data_array,$comment_data_array)
              {
                $sheet->setWidth([
                    'B'     =>  40
                ]);
                
                $sheet->getStyle('B')->getAlignment()->setWrapText(true);
                $sheet->getStyle('A')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('B')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('C')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->fromArray($post_data_array, null, 'A1', false, false);
              });
          $excel->sheet('Comment Data', function($sheet) use ($post_data_array,$comment_data_array)
              {
                $sheet->setWidth([
                    'B'     =>  40
                ]);
                                          
                $sheet->getStyle('B')->getAlignment()->setWrapText(true);
                $sheet->getStyle('A')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('B')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('C')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('D')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $sheet->getStyle('E')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                
          $sheet->fromArray($comment_data_array, null, 'A1', false, false);
              });
        })->store('xlsx', 'public/reports/', true);
    }
    public function comparison_pdf()
    {
        $pid=17;
        $project_name="";
       
           $title="Comparison";
           $source='in';
           $view_html='                                    <div class="col-lg-12">                      <div class="card">                          <div class="card-body">                              <h4 class="card-title">Fan Growth</h4>                                <div style="" align="center" id="fan-growth-spin"> <img src="https://cms.baganintel.com/BaganSocialListener/assets\images\ajax-loader.gif" id="loader"></div>                            <div id="fan-growth-chart" style="width: 100%; height: 350px; -webkit-tap-highlight-color: transparent; user-select: none; position: relative;" _echarts_instance_="ec_1544275305309"><div style="position: relative; overflow: hidden; width: 1347px; height: 350px; padding: 0px; margin: 0px; border-width: 0px;"><canvas data-zr-dom-id="zr_0" width="1347" height="350" style="position: absolute; left: 0px; top: 0px; width: 1347px; height: 350px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); padding: 0px; margin: 0px; border-width: 0px;"></canvas></div><div></div></div>                            </div>                      </div>                  </div>                            ';
           //dd($view_html);
          
           $project_data_id = $this->getProjectByid($pid);
           $permission_data = $this->getPermission(); 
               if(count($project_data_id)>0)
           {
           $project_name = $project_data_id[0]['name'];
           $project_data = $this->getProject();
           $count = $this->getProjectCount($project_data);
           //$project_data_exclude = $this->getProjectByExcludeid($pid);
           $compare_pages= $this->getComparisonPage($pid);
           $compare_pages=explode(',', $compare_pages);
           $pdf = new Dompdf();
           $pdf->set_option("isJavascriptEnabled", true);
           $pdf->set_option('javascript-delay', 5000);
           $pdf->set_option('images', true);
           
         //   $pdf=PDF::loadView('comparisonPDF',compact('title','project_data','count','project_name','permission_data','compare_pages','pid','source'));
            //  $pdf->setOption('isJavascriptEnabled', true);
            // $pdf->setOption('javascript-delay', 5000);
            // $pdf->setOption('enable-smart-shrinking', true);
            // $pdf->setOption('no-stop-slow-scripts', true);
            $pdf=PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadHTML($view_html);
           // $pdf->render();
            return $pdf->stream('comparison.pdf');
           }
    }
}

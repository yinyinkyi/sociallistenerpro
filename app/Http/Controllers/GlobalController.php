<?php

namespace BIMiner\Http\Controllers;

use BIMiner\MongodbData;
use BIMiner\Mongodbpage;
use BIMiner\Comment;
use BIMiner\ManageDB;
use BIMiner\ProjectKeyword;
use BIMiner\Project;
use BIMiner\user_permission;
use BIMiner\demo;
use BIMiner\CompanyInfo;
use BIMiner\InboundPages;
use BIMiner\tagGrouplist;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use BIMiner\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
trait GlobalController
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function check_cron_finish($pid)
    {
      $result=DB::table('cron_log')->where('brand_id',$pid)->where('finish_status',0)->where('cron_type','sync_mention')->count();
      return $result;
    }

    public function check_mm_en($keyword){
      if (preg_match('/[က-အ]/', $keyword)) // '/[^a-z\d]/i' should also work.
      {
        return 'mm';
      }
      else{
        return "en";
      }
    }

    public function checkUserForProject($pid)
    {
          $login_user = auth()->user()->id;
          $checked_data = ManageDB::select('*')->where('user_id', $login_user)->where('brand_id', $pid);
          $checked_data=$checked_data->orderBy('id','DESC')->get();
          return  $checked_data;
    }
    public function getCompanyData($pid)
    {

        $login_user = auth()->user()->id;
        $user_data = DB::select("select company_id from users where id = ".$login_user);
        // $user_data = users::select('*')->where('user_id', $login_user)->get();  
        // get  brand id and db_name 
        $data = [];
        $companyInfo = CompanyInfo::select('*')->where('id', $user_data[0]->company_id)->get();
        
        if(count($companyInfo)>0)
          $data = $companyInfo[0];
        return $data;
    }
    public function getPermission()
    {
          $login_user = auth()->user()->id;
          $permission_data = user_permission::select('*')->where('user_id', $login_user);
          $permission_data=$permission_data->orderBy('id','DESC')->first();

          return  $permission_data;
    }
     public function getProject()
    {
          $login_user = auth()->user()->id;
          // dd($login_user);
          $user_data = ManageDB::select('*')->where('user_id', $login_user)->get();  
          // get  brand id and db_name 
          // dd($user_data);
          if(count($user_data) > 0)
          {
          $project_data = Project::select('*')->where('id', $user_data[0]['brand_id']);
          $project_data=$project_data->orderBy('id','DESC')->get();

          }
         
          return  $project_data;
    }
    //     public function getProject()
    // {
    //       $login_user = auth()->user()->id;
    //       // dd($login_user);
    //       $project_data = User::select('*')->where('id', $login_user);

    //       $project_data=$project_data->orderBy('id','DESC')->get();
    //       // dd($project_data);
    //       return  $project_data;
    // }
     public function getIsDemouser()
    {
          $login_user = auth()->user()->id;
          $demo_user= user_permission::select('*')->where('user_id', $login_user)->where('demo', 1);
          $demo_user=$demo_user->orderBy('id','DESC')->get();
          if(count($demo_user)>0)
          {
             return  true;
          }
          else
          {
              return  false;
          }
          
    }
    public function getInboundPostId($brand_id)
    {
        $id_query="select id from temp_inbound_posts";
        $id_result = DB::select($id_query);
         $id_array = array_column($id_result, 'id');
          return  $id_array;
    }

    public function getMentionPostId($brand_id)
    {
        $id_query="select id from temp_".$brand_id."_posts";
        $id_result = DB::select($id_query);
         $id_array = array_column($id_result, 'id');
          return  $id_array;
    }
     public function getlastcrawldate($brand_id)
    {
      $query="select CONVERT_TZ(created_at,'+00:00','+06:30') last_crawl_date from temp_inbound_comments ORDER BY timestamp(created_at) DESC";
       $result = DB::select($query);
       $last_crawl_date = array_column($result, 'last_crawl_date');
          return  $last_crawl_date;
    }

    public function getDemoHideDiv($view_name)
    {
          $hidden_div = demo::select('*')->where('is_hide',1)->where('view_name',$view_name);
          $hidden_div=$hidden_div->orderBy('id','DESC')->get();
          $hidden_div_name='';
          foreach ($hidden_div as $key => $value) {
            if($key === 0 )
            $hidden_div_name .=$value['div_name'];
          else
            $hidden_div_name .="," . $value['div_name'];
              
          }
          return  $hidden_div_name;
    }
    public function getInboundPages($brand_id)
    {
      //$page_query="select DISTINCT page_name from temp_inbound_posts";
     // $page_result = DB::select($page_query);
      $page_result=[];
      $project_data_id = $this->getProjectByid($brand_id);
       if(count($project_data_id)>0)
            {
            $monitorpage=$project_data_id[0]['monitor_pages'];
            $page_result=explode(',', $monitorpage);
          }
      $pages='';
    
       foreach ($page_result as  $key => $row) {
                      if ($key == 0) {
                         $pages  =  "'" . $row . "'";
                       }
                       else
                       {
                          $pages  = $pages .",'". $row. "'";
                       }
       }
     
       
       return $pages;
    }

    public function getExcludePages($brand_id)
    {
      //$page_query="select DISTINCT page_name from temp_inbound_posts";
     // $page_result = DB::select($page_query);
      $page_result=[];
      $project_data_id = $this->getProjectByid($brand_id);
       if(count($project_data_id)>0)
            {
            $monitorpage=$project_data_id[0]['exclude_pages'];
            $page_result=explode(',', $monitorpage);
          }
      $pages='';
    
       foreach ($page_result as  $key => $row) {
                      if ($key == 0) {
                         $pages  =  "'" . $row . "'";
                       }
                       else
                       {
                          $pages  = $pages .",'". $row. "'";
                       }
       }
     
       
       return $pages;
    }

    public function gettags()
    {
      $query="select name,keywords,id from tags where brand_id='".auth()->user()->brand_id."' order by name" ;
      $result = DB::select($query);
      return $result;
    }
    public function gettagsBybrandid($brand_id)
    {
      
      // because visamm is a sub project of payment .. we need to use  the same tags
      $query="select name,keywords,id from tags where brand_id = ".$brand_id." order by name" ;
      $result = DB::select($query);
      return $result;
    }
    public function gettagGroup($brand_id)
    {
      $query="select name,id from tagsGroup where brand_id=" .$brand_id . " order by name" ;
      $result = DB::select($query);
      return $result;
    }
    public function gettagbytagGroupId($tagsGroupId)
    {
      $query="select tags.name,tags.id from tagGrouplists tgl inner join tags on tgl.tag_id=tags.id ".
             " where tgl.tagGroup_id=" .$tagsGroupId . " order by tags.name " ;
      $taglist = DB::select($query);
      return $taglist;
    }
    public function gettagsByid($id)
    {
      $query="select name,keywords,id from tags where id=" .$id ;
      $result = DB::select($query);
      return $result;
    }
    public function getProjectByid($id)
    {
      
          $project_data = Project::select('*')->where('id', $id)->get();
          return  $project_data;
    }
    public function getProjectByid_fromCmpInfo($id)
    {
      // dd($id);
          $project_data = CompanyInfo::select('*')->where('id', $id)->get();
          // dd($project_data);
          return  $project_data;
    }
    public function getProjectByExcludeid($id)
    {
          $login_user = auth()->user()->id;
          $project_data = Project::select('*')->where('user_id', $login_user);
          $project_data= $project_data->where('id','<>',$id);
          $project_data=$project_data->orderBy('id','DESC')->get();

          return  $project_data;
    }

    public function getProjectCount($project_data)
    {
     
         $count = $project_data->count();
         return $count;
    }
    
    
  public function getprojectkeyword($brand_id,$group_name='')
    {

          if($group_name =='')
       $keyword_data = ProjectKeyword::select('main_keyword','require_keyword','exclude_keyword')->where('project_id','=',$brand_id)->get()->toArray();
          else
       $keyword_data = ProjectKeyword::select('main_keyword','require_keyword','exclude_keyword')->where('project_id','=',$brand_id)->where('group_name',$group_name)->get()->toArray();
     // dd($keyword_data);
       return $keyword_data;
    }
 
    public function getprojectkeywordCombine($brand_id)
    {
      $query="select main_keyword,require_keyword from project_keywords where project_id=" .$brand_id ;
      $result = DB::select($query);
      return $result;
    }

    public function getpagename($brand_id)
    {
     $page_list = Project::select('monitor_pages')->where('id','=',$brand_id)->get()->toArray();
      return $page_list;
    }
    public function getSentiWhereGen($sentiment,$table)
     {
           $filter_sentiment='';
           $sentiment=explode('| ', $sentiment);
           foreach ($sentiment as $key => $value) {
                if($value !== 'all')
                {
                   if($filter_sentiment == '')
                {
                  if($value == 'neutral')
                  $filter_sentiment .="  (".$table.".checked_sentiment = '".$value."' or ".$table.".checked_sentiment = 'NA')";
                  else
                  $filter_sentiment .="  ".$table.".checked_sentiment = '".$value."'";
                }
                else
                {
                  if($value == 'neutral')
                  $filter_sentiment .=" or (".$table.".checked_sentiment = '".$value."' or ".$table.".checked_sentiment = 'NA')";
                  else
                  $filter_sentiment .=" or ".$table.".checked_sentiment = '".$value."'";
                }
                }
           }
             $filter_sentiment  = "  (" . $filter_sentiment . ")"; 
             return $filter_sentiment;


    }

//     public function getCampaignKeywordWhereGen($keyword,$table)
//     {
//        $filter_pair='';
//        $keyword=explode('| ', $keyword);
//        foreach ($keyword as $key => $row1) {
      
//          $pair_keyword  = explode(',',$row1);
//          $temp_pair='';
//          foreach ($pair_keyword as $key => $row2) {
//            if($temp_pair == '')
//                  {
//                   $temp_pair .= "  ".$table.".message Like '%".$row2."%'";
//                  }
//             else
//                   {
//                   $temp_pair .= " AND ".$table.".message Like '%".$row2."%'";
//                   }
//          }
        
//          if($filter_pair == "")
         
            
//               $filter_pair .= " (" .$temp_pair. ") ";
       
         
//          else
//           $filter_pair .= " or (" .$temp_pair. ") ";

//      }
//     if($filter_pair <> "")
//     $filter_pair = " (" .$filter_pair. ") ";
// // dd($filter_pair);
//      return $filter_pair;

//     }
public function getKeywordWhere($keyword,$table)
{
 // sample where ((keyword1) or ( keyword1 and keyword2) or ( keyword3 and keyword4) or ( keyword5))
  // dd($keyword);
   $filter_pair='';
   $keyword=explode('| ', $keyword);
     foreach ($keyword as $key => $row1) {
      
         $pair_keyword  = explode(',',$row1);
         $temp_pair='';
         foreach ($pair_keyword as $key => $row2) {
           if($temp_pair == '')
                 {
                  if (preg_match('/[က-အ]/', $row2)) $temp_pair .= "  ".$table.".group_name Like '%".$row2."%'";
                  else $temp_pair .= "  ".$table.".group_name Like '% ".$row2." %'";
                 }
            else
                  {
                  if (preg_match('/[က-အ]/', $row2)) $temp_pair .= " AND ".$table.".group Like '%".$row2."%'";
                  else $temp_pair .= " AND ".$table.".group_name Like '% ".$row2." %'";
                  }
         }
        
         if($filter_pair == "")
         
            
              $filter_pair .= " (" .$temp_pair. ") ";
       
         
         else
          $filter_pair .= " or (" .$temp_pair. ") ";

     }
    if($filter_pair <> "")
    $filter_pair = " (" .$filter_pair. ") ";

     return $filter_pair;


}

 public function getKeywordWhereGen($keyword,$table)
{
 // sample where ((keyword1) or ( keyword1 and keyword2) or ( keyword3 and keyword4) or ( keyword5))
  // dd($keyword);
   $filter_pair='';
   $keyword=explode('| ', $keyword);
     foreach ($keyword as $key => $row1) {
      
         $pair_keyword  = explode(',',$row1);
         $temp_pair='';
         foreach ($pair_keyword as $key => $row2) {
           if($temp_pair == '')
                 {
                  if (preg_match('/[က-အ]/', $row2)) $temp_pair .= "  ".$table.".message Like '%".$row2."%'";
                  else $temp_pair .= "  ".$table.".message Like '% ".$row2." %'";
                 }
            else
                  {
                  if (preg_match('/[က-အ]/', $row2)) $temp_pair .= " AND ".$table.".message Like '%".$row2."%'";
                  else $temp_pair .= " AND ".$table.".message Like '% ".$row2." %'";
                  }
         }
        
         if($filter_pair == "")
         
            
              $filter_pair .= " (" .$temp_pair. ") ";
       
         
         else
          $filter_pair .= " or (" .$temp_pair. ") ";

     }
    if($filter_pair <> "")
    $filter_pair = " (" .$filter_pair. ") ";

     return $filter_pair;


}

public function getTagWhereGen($tag,$tbl)
{
  $filter_tag='';
   $tag=explode('| ', $tag);
    
     foreach ($tag as $key => $value) {
      
         if($filter_tag == '')
      {
        $filter_tag .=" FIND_IN_SET('".$value."', ".$tbl.".checked_tags_id) >0";
      }
      else
      {
        
        $filter_tag .=" and FIND_IN_SET('".$value."', ".$tbl.".checked_tags_id) >0";
       
      }
      
     }
     $filter_tag  = "  (" . $filter_tag . ")"; 
     return $filter_tag;


}

public function getTagWhereOrGen($tag,$tbl)
{
  $filter_tag='';
   $tag=explode('| ', $tag);
    
     foreach ($tag as $key => $value) {
      
         if($filter_tag == '')
      {
        $filter_tag .=" FIND_IN_SET('".$value."', ".$tbl.".checked_tags_id) >0";
      }
      else
      {
        
        $filter_tag .=" OR FIND_IN_SET('".$value."', ".$tbl.".checked_tags_id) >0";
       
      }
      
     }
     $filter_tag  = "  (" . $filter_tag . ")"; 
     return $filter_tag;


}
/*  public function getTagmultipleGen($tag,$tbl)
  {
    $filter_tag='';
    
    //$tag = implode(',', $tag);
    $tags = "'" . implode ( "', '", $tag ) . "'";
     //$tag=explode(',', $tag);
     // dd($tag);

      
          // $filter_tag .=" FIND_IN_SET('".$value."', ".$tbl.".checked_tags_id) >0";
        $filter_tag = $tbl.".checked_tags_id IN (".$tags.")";

       return $filter_tag;

  }*/
public function getPageWhereGen($related_pages)
{
    $pages='';
     $filter_pages = '';
     $related_pages = $this->Format_Page_Name($related_pages);
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  posts.page_name in (".$pages.")";
      }
    return  $filter_pages;
}
public function getPageWhereGen_forcmt($related_pages)
{
    $pages='';
     $filter_pages = '';
     $related_pages = $this->Format_Page_Name($related_pages);
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  cmts.page_name in (".$pages.")";
      }
    return  $filter_pages;
}
public function getPageWhereGenNotIn($related_pages)
{
    $pages='';
     $filter_pages = '';
     $related_pages = $this->Format_Page_Name($related_pages);
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="   posts.page_name not in (".$pages.")";
      }
    return  $filter_pages;
}
public function Format_Page_Name($array)
  {
      $new_array=[];
       foreach ($array as  $key => $row) {
        $arr_page_name=explode("-",$row);
        if(count($arr_page_name)>0)
        {
          $last_index=$arr_page_name[count($arr_page_name)-1];
          if(is_numeric($last_index))
          $new_array[] = $last_index;
          else
          $new_array[] = $row;
        }
        else
        {
          $new_array[]=$row;
        }
       
          
      
  }

      return  $new_array;
    }
    public function Format_Page_name_single($pagename)
    {
        $newpagename='';
        $arr_page_name=explode("-",$pagename);
        if(count($arr_page_name)>0)
        {
          $last_index=$arr_page_name[count($arr_page_name)-1];
          if(is_numeric($last_index))
          $newpagename = $last_index;
          else
          $newpagename = $pagename;
        }
        else
        {
          $newpagename=$pagename;
        }
        return  $newpagename;
    }
public function getOwnPage($brand_id)
 {
   $keyword_data = Project::select('own_pages')->where('id','=',$brand_id)->get()->toArray();
   return $keyword_data[0]['own_pages'];
 }
 public function getComparisonPage($brand_id)
 {  //echo $brand_id;
   $keyword_data = Project::select('monitor_pages')->where('id','=',$brand_id)->get()->toArray();
   // dd($keyword_data);
   $default_page = User::select('default_page')->where('id',auth()->user()->id)->get();
   // dd($default_page);
   $arr = [];
   foreach($default_page as $row) $page = $row->default_page;
   foreach($keyword_data as $key => $row)
   {
    $arr[$key]['monitor_pages'] = $row['monitor_pages'];
    $arr[$key]['default_page'] = $page;
   }
   

   return $arr;
 }
  public function getComparisonPage_fromCmpInfo($company_id)
 {  //echo $brand_id;
  // dd($company_id);
   $keyword_data = CompanyInfo::select('pages_name')->where('id','=',$company_id)->get()->toArray();
   // dd($keyword_data);
   $default_page = User::select('custom_page')->where('id',auth()->user()->id)->get();
   // dd($default_page);
   $arr = [];
   foreach($default_page as $row) $page = $row->custom_page;
   foreach($keyword_data as $key => $row)
   {
    $arr[$key]['pages_name'] = $row['pages_name'];
    $arr[$key]['default_page'] = $page;
   }

   return $arr;
 }

  

public function getpagefilter($page_data)
{
  $conditional=[];
  
  $conditional_require_or=[];
  foreach($page_data as $i =>$element)
                            {

                            $require_keyword_filter[] = [ 'page_name' =>  $element];
                            $conditional_require_or['$or']=$require_keyword_filter;
                            }
 $conditional[] =$conditional_require_or;
 
 return  $conditional;
}
public function getKeywordGroupByName($pid,$group_name)
{
  $keywordgrouplist = ProjectKeyword::select(['group_name','id','project_id','created_at'])->where('project_id',$pid)->where('group_name',$group_name)->get()->toArray();
     return $keywordgrouplist;
}
public function getkeywordfilter_MYSQL($keyword_data,$tbl)
{

  $conditional_and = '';
  $conditional_or = '';
  
  foreach ($keyword_data as $key => $row) {
    $main_con ='';$include_con = '';$exclude_con = '';$conditional_and='';


    $main_keyword = explode(",", $row['main_keyword']);
    foreach($main_keyword as $req =>$element){
    if( $element  <> ''){
      if((int)$req === 0){
        if (preg_match('/[က-အ]/', $element)) $main_con = $tbl.".message Like '%".str_replace("'","\'",$element)."%'";
        else  $main_con = $tbl.'.message  LIKE "% '.str_replace("'","\'",$element).' %"';
      }
      else{
        if (preg_match('/[က-အ]/', $element)) $main_con .="  or ".$tbl.".message Like '%".str_replace("'","\'",$element)."%'";
        else $main_con .='  or '.$tbl.'.message  LIKE "% '.str_replace("'","\'",$element).' %"';
      }
    }
   }

    // if (preg_match('/[က-အ]/', $row['main_keyword'])) $main_con = $tbl.".message Like '%".$row['main_keyword']."%'";
    // else $main_con = $tbl.".message Like '% ".$row['main_keyword']." %'";
 
    $require_keyword = explode(",", $row['require_keyword']);
    foreach($require_keyword as $req =>$element){
    if( $element  <> ''){
      if((int)$req === 0){
        if (preg_match('/[က-အ]/', $element)) $include_con = $tbl.".message Like '%".str_replace("'","\'",$element)."%'";
        else  $include_con = $tbl.'.message  LIKE "% '.str_replace("'","\'",$element).' %"';
      }
      else{
        if (preg_match('/[က-အ]/', $element)) $include_con .="  or ".$tbl.".message Like '%".str_replace("'","\'",$element)."%'";
        else $include_con .='  or '.$tbl.'.message LIKE "% '.str_replace("'","\'",$element).' %"';
      }
    }
   }

    $exclude_keyword = explode(",", $row['exclude_keyword']);
    foreach($exclude_keyword as $exc =>$element){
    if( $element <> ''){
      if((int)$exc === 0){
        if (preg_match('/[က-အ]/', $element)) $exclude_con = $tbl.".message NOT LIKE '%".str_replace("'","\'",$element)."%'";
        else  $exclude_con = $tbl.'.message NOT LIKE "% '.str_replace("'","\'",$element).' %"';
      }
      else{
        if (preg_match('/[က-အ]/', $element)) $exclude_con .="  and ".$tbl.".message NOT LIKE '%".str_replace("'","\'",$element)."%'";
        else $exclude_con .='  and '.$tbl.'.message NOT LIKE "% '.str_replace("'","\'",$element).' %"';
      }
     }
   }
       
       if(!empty($main_con)) $conditional_and .=  $main_con . "  " ;
       
       if(!empty($include_con)) $conditional_and  .= " and ( " . $include_con . " )  " ;
       
       if(!empty($exclude_con)) $conditional_and .= " and ( " . $exclude_con . " )  " ;
       

       if($conditional_and <> '')
       {
        if($conditional_or == '') $conditional_or = "( " . $conditional_and . " ) ";
        else $conditional_or .= " or ( " . $conditional_and . " ) ";
       }
    
   }
  
 return  $conditional_or;

    
}
public function getcampaignGroup($brand_id,$page_name='')
    {
       $query="select * from campaignGroup".
             " where brand_id=" .$brand_id . "  and page_name='" .$page_name . "' order by name" ;
       $result = DB::select($query);
       return $result;
    }
public function getcampaignkeyword($brand_id,$page_name='')
    {
      if($page_name =='')
      {
      $query="select campaign_keyword from campaignGroup cg inner join ".
             " campaignGroupList cgl on cg.id=cgl.campaignGroup_id " .
             " where brand_id=" .$brand_id . " order by campaign_keyword" ;
 
      }
      else
      {
      $query="select campaign_keyword from campaignGroup cg inner join ".
             " campaignGroupList cgl on cg.id=cgl.campaignGroup_id " .
             " where brand_id=" .$brand_id . " and page_name='" .$page_name . "' order by campaign_keyword" ;
      }
       $result = DB::select($query);
      //$result = array_column($result, 'campaign_keyword');
       return $result;
    }
    public function getCampaignKeywordFilter($campaign_id,$brand_id)
    {
      // dd($campaign_id,$brand_id);
      $query="select campaign_keyword from campaignGroup cg inner join ".
             " campaignGroupList cgl on cg.id=cgl.campaignGroup_id " .
             " where brand_id=" .$brand_id . " and cg.id='" .$campaign_id. "' order by campaign_keyword" ;
      $result = DB::select($query);
      // dd($result);
      return $result;
    }
public function getcampaignfilter_MYSQL($keyword_data,$tbl='')
{
 
 
  $conditional_or = '';
  if($tbl <> ''){
  foreach ($keyword_data as $key => $row) {
       if($conditional_or =='')
          $conditional_or = $tbl.".message Like '%".$row->campaign_keyword."%'";
       else
          $conditional_or .= " or ".$tbl.".message Like '%".$row->campaign_keyword."%'";
  
         
   } 
 }
else{
  foreach ($keyword_data as $key => $row) {
       if($conditional_or =='')
          $conditional_or = "message Like '%".$row->campaign_keyword."%'";
       else
          $conditional_or .= " or message Like '%".$row->campaign_keyword."%'";
  
         
   }
 }

   if($conditional_or <> '')
  $conditional_or = "(" . $conditional_or . ")";
// dd($conditional_or);

  return  $conditional_or;

    
}
public function getCampaignWhereGen($keyword,$tbl)
{
   $filter_pair='';
   $filter_key_pair='';
   $result=[];
   $keyword=explode('| ', $keyword);
     foreach ($keyword as $key => $row) {
      if( $filter_pair == '')
      $filter_pair = ' campaignGroup_id='.$row;
      else
      $filter_pair .= ' or campaignGroup_id='.$row;
        

     }
    if($filter_pair <> "")
    {
        $filter_pair = " (" .$filter_pair. ") ";
        $query="select campaign_keyword from campaignGroupList  where ".$filter_pair  ;
        $result = DB::select($query);
    }
    foreach ($result as $key => $value) {
       if($filter_key_pair == '')
                 {
                  $filter_key_pair .= "  ".$tbl.".message Like '%".$value->campaign_keyword."%'";
                 }
            else
                  {
                  $filter_key_pair .= " or ".$tbl.".message Like '%".$value->campaign_keyword."%'";
                  }
    }
      if($filter_key_pair <> "")
    {
        $filter_key_pair = " (" .$filter_key_pair. ") ";
    }


     return $filter_key_pair;
}
public function getCampaignGroupWhereGen($keyword,$tbl)
{
   $filter_pair='';
   $keyword=explode('| ', $keyword);
     foreach ($keyword as $key => $row) {
      if( $filter_pair == '')
      $filter_pair = ' campaign='.$row;
      else
      $filter_pair .= ' or campaign='.$row;
        

     }
    if($filter_pair <> "")
    {
        $filter_pair = " (" .$filter_pair. ") ";
       
    }
   


     return $filter_pair;
}

public function getKeywordforHighLight($keyword_data)
{
 
  $keyword = [];
  
  foreach ($keyword_data as $key => $row) {
    $main_con ='';$include_con = '';$exclude_con = '';$conditional_and='';

    $keyword [] =$row['main_keyword'];
 
      $require_keyword = explode(",", $row['require_keyword']);
      foreach($require_keyword as $req =>$element)
       {
        if( $element  <> '')
        {
         $keyword [] = $element;
        }
       }

      
       
       
    
    
   }
   //dd($conditional_or);
 return  $keyword;

    
}
   public function getkeywordfilter($keyword_data,$brand_id='')
  {
     $conditional=[];
     $conditional_combine=[];
     $campaign_keyword=[];
     $conditional_keyword_or=[];
     if($brand_id <>'')
     {
      $campaign_keyword = getcampaignkeyword($brand_id);
     }
     foreach ($campaign_keyword as $key => $value) {
      // Keyword 'OR' Condition
                           $keyword_filter=[];
                           if(!empty($value))
                          {
                            $keyword_filter[] = [ 'message' => new MongoDB\BSON\Regex(".*". $value['campaign_keyword'],'i'  )];
                            $conditional_keyword_or['$or']=$keyword_filter;
                          }
                         
     }
     foreach ($keyword_data as  $key => $row) {
                      
     $main_keyword_filter =[ 'message' => new MongoDB\BSON\Regex(".*" . $row['main_keyword'],'i' )];
      
                        //Require Keyword 'OR' Condition
                         $require_keyword = explode(",", $row['require_keyword']);
                          $conditional_require_or=[];
                         $require_keyword_filter=[];
                            foreach($require_keyword as $i =>$element)
                            {
                               if(!empty($element))
                                {
                                 $require_keyword_filter[] = [ 'message' => new MongoDB\BSON\Regex(".*". $element,'i'  )];
                            $conditional_require_or['$or']=$require_keyword_filter;
                              }
                           
                            }
                         
                          //Require Keyword 'AND' Condition
                          $exclude_keyword = explode(",", $row['exclude_keyword']);
                          $conditional_exclude_or=[];
                          $exclude_keyword_filter=[];
                        
                            foreach($exclude_keyword as $i =>$element)
                            {
                                if(!empty($element))
                                {
                             
                            $exclude_keyword_filter[] = [ 'message' => ['$not'=>new MongoDB\BSON\Regex(".*". $element,'i'  )]];
                            $conditional_exclude_or['$and']=$exclude_keyword_filter;
                                }
                            }
                      
                            //Create 'AND' condition between main,require and exclude keyword
                             $conditional_and['$and'] =[$main_keyword_filter];

                            if(!empty($conditional_require_or))
                            {
                                $conditional_and['$and']=[$conditional_and,$conditional_require_or];
                            }
                           
                                               
                            if(!empty($conditional_exclude_or))
                            {
                               
                               $conditional_and['$and']=[$conditional_and,$conditional_exclude_or];
                            }
                           
                          //   $conditional_and['$and'] =[$main_keyword_filter,$conditional_require_or,$conditional_exclude_or];

                        
                        $conditional[] =$conditional_and;
                       
              }
              //Combine Project Keyword and Campaign Keyword
              if(count($conditional_keyword_or)>0)
              {
                $conditional_combine['$or'] = [$conditional,$conditional_keyword_or];
                return  $conditional_combine;
              }
              else
              {
                return  $conditional;
              }
                
  }
public function getPageIdfromMongo($filter_page_name)
{
     $InboundPages_result = [];
     $InboundPages_result=InboundPages::raw(function ($collection) use($filter_page_name) {//print_r($filter);

              return $collection->aggregate([
                  [
                  '$match' =>[
                       '$and'=> [ 
                       ['page_name'=>$filter_page_name],
                                 
                                ]
                  ] 
                             
                 ],

                 ['$sort' =>['_id'=>1]]
                  
              ]);
            })->toArray();
      return $InboundPages_result;
}
public function createTable($table_name, $fields = [],$autoincrease=1,$table_type='')
    {
        // check if table is not already exists
        if (!Schema::hasTable($table_name)) {
            Schema::create($table_name, function (Blueprint $table) use ($fields, $table_name,$autoincrease,$table_type) {
              if($autoincrease <> 0)
                $table->increments('temp_id');

                if (count($fields) > 0) {
                    foreach ($fields as $field) {
                      if(isset($field['index']))
                      {
                        if(isset($field['length']))
                        $table->{$field['type']}($field['name'],$field['length'])->nullable()->index();
                       else
                        $table->{$field['type']}($field['name'])->nullable()->index();
                      }
                      else if(isset($field['unique']))
                      {
                        if(isset($field['length']))
                        $table->{$field['type']}($field['name'],$field['length'])->unique();
                       else
                        $table->{$field['type']}($field['name'])->unique();
                      }
                      else
                      {
                        if(isset($field['length']))
                        $table->{$field['type']}($field['name'],$field['length'])->nullable();
                        else
                        $table->{$field['type']}($field['name'])->nullable();
                      }
                    }
                   $table->timestamps();
                  if($table_type == 'predict_logs')
                    $table->unique(['id','type']);
                }
                


            });
 
            return response()->json(['message' => 'Given table has been successfully created!'], 200);
        }
 
        return response()->json(['message' => 'Given table is already existis.'], 400);
    }
    public function new_table_add($brand_id)
    {
       $table_inbound_profiles = 'temp_' . $brand_id . '_inbound_profiles';
       $fields_inbound_profiles = [
            ['name' => 'id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'type', 'type' => 'text'],
            
        ];

      
       return $this->createTable($table_inbound_profiles, $fields_inbound_profiles);

    }

      public function operate($brand_id)
    {
        // set dynamic table name according to your requirements
   
          $table_posts = 'temp_posts';
          $table_comments = 'temp_comments';
          $table_pages = 'temp_' . $brand_id . '_pages';
          $table_followers = 'temp_' . $brand_id . '_followers';
          $table_inbound_posts = 'temp_inbound_posts';
          $table_inbound_comments = 'temp_inbound_comments';
          $table_inbound_profiles = 'temp_profiles';
          $table_predict_logs = 'predict_' . $brand_id . '_logs';
          $table_web_mentions = 'temp_web_mentions';
        
          $fields_web_mentions = [
            ['name' => 'id', 'type' => 'string','length'=>'50','unique'=>1],
            ['name' => 'unicode', 'type' => 'string','length'=>'10'],
            ['name' => 'title','type' => 'string','length' => '50'],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'page_name', 'type' => 'string','length'=>'300','index'=>1],
            ['name' => 'link', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string'],
            ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50'],
            ['name' => 'emotion', 'type' => 'string'],
            ['name' => 'checked_emotion', 'type' => 'string','length'=>'50'],
            ['name' => 'change_predict', 'type' => 'boolean'],
            ['name' => 'isHide', 'type' => 'boolean'],
            ['name' => 'manual_by', 'type' => 'string','length'=>'100'],
            ['name' => 'manual_date', 'type' => 'timestamp'],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],

           ];
        // set your dynamic fields (you can fetch this data from database this is just an example)
          $fields_posts = [
              ['name' => 'id', 'type' => 'string','length'=>'50','unique'=>1],
              ['name' => 'full_picture', 'type' => 'text'],
              ['name' => 'link', 'type' => 'text'],
              ['name' => 'name', 'type' => 'text'],
              ['name' => 'message', 'type' => 'text'],
              ['name' => 'original_message', 'type' => 'text'],
              ['name' => 'unicode',  'type' => 'string','length'=>'50'],
              ['name' => 'type', 'type' => 'string','length'=>'50','index'=>1],
              ['name' => 'wb_message', 'type' => 'text'],
              ['name' => 'page_name', 'type' => 'string','length'=>'300','index'=>1],
              ['name' => 'shared', 'type' => 'text'],
              ['name' => 'Liked', 'type' => 'text'],
              ['name' => 'Love', 'type' => 'text'],
              ['name' => 'Haha', 'type' => 'text'],
              ['name' => 'Wow', 'type' => 'text'],
              ['name' => 'Sad', 'type' => 'text'],
              ['name' => 'Angry', 'type' => 'text'],
              ['name' => 'sentiment', 'type' => 'string','length'=>'50'],
              ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50'],
              ['name' => 'decided_keyword', 'type' => 'text'],
              ['name' => 'emotion', 'type' => 'string','length'=>'50'],
              ['name' => 'checked_emotion', 'type' => 'string','length'=>'50'],
              ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
              ['name' => 'change_predict','type'=>'boolean'],
              ['name' => 'checked_predict','type'=>'boolean'],
              ['name' => 'isBookMark', 'type' => 'boolean'],
              ['name' => 'isDeleted', 'type' => 'boolean'],
              ['name' => 'isHide', 'type' => 'boolean'],
              ['name' => 'visitor', 'type' => 'boolean'],
              ['name' => 'campaign', 'type' => 'integer'],

          ];

              $fields_comments = [
                ['name' => 'id', 'type' => 'string','length'=>'50','unique'=>1],
                ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
                ['name' => 'message', 'type' => 'text'],
                ['name' => 'wb_message', 'type' => 'text'],
                ['name' => 'original_message', 'type' => 'text'],
                ['name' => 'unicode',  'type' => 'string','length'=>'50'],
                ['name' => 'post_id', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'comment_count', 'type' => 'text'],
                ['name' => 'sentiment', 'type' => 'string','length'=>'50'],
                ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50'],
                ['name' => 'decided_keyword', 'type' => 'text'],
                ['name' => 'emotion', 'type' => 'string','length'=>'50'],
                ['name' => 'checked_emotion', 'type' => 'string','length'=>'50'],
                ['name' => 'interest', 'type' => 'boolean'],
                ['name' => 'profile', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'change_predict', 'type' => 'boolean'],
                ['name' => 'checked_predict','type'=>'boolean'],
                ['name' => 'parent', 'type' => 'string','length'=>'50'],
                ['name' => 'isBookMark', 'type' => 'boolean'],
                ['name' => 'manual_date', 'type' => 'timestamp'],
                ['name' => 'manual_by', 'type' => 'string','length'=>'100'],
                ['name' => 'isHide', 'type' => 'boolean'],
                ['name' => 'page_name', 'type' => 'string','length'=>'300','index'=>1],
                ['name' => 'cmtLiked', 'type' => 'text'],
                ['name' => 'cmtLove', 'type' => 'text'],
                ['name' => 'cmtHaha', 'type' => 'text'],
                ['name' => 'cmtWow', 'type' => 'text'],
                ['name' => 'cmtSad', 'type' => 'text'],
                ['name' => 'cmtAngry', 'type' => 'text'],
                ['name' => 'cmtType', 'type' => 'string','length'=>'50'],
                ['name' => 'cmtLink', 'type' => 'text'],
                
            ];
      
             $fields_pages = [
                ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
                ['name' => 'about', 'type' => 'text'],
                ['name' => 'name', 'type' => 'text'],
                ['name' => 'category', 'type' => 'text'],
                ['name' => 'sub_category', 'type' => 'text'],
                ['name'=>'profile','type' => 'text','length'=>'5']
            ];
             $fields_followers = [
                ['name' => 'id', 'type' => 'text'],
                ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
                ['name' => 'fan_count', 'type' => 'text'],
                ['name' => 'date', 'type' => 'text']
            ];
        $fields_inbound_posts = [
                ['name' => 'id', 'type' => 'string','length'=>'50','unique'=>1],
                ['name' => 'full_picture', 'type' => 'text'],
                ['name' => 'link', 'type' => 'text'],
                ['name' => 'name', 'type' => 'text'],
                ['name' => 'message', 'type' => 'text'],
                ['name' => 'original_message', 'type' => 'text'],
                ['name' => 'unicode',  'type' => 'string','length'=>'50'],
                ['name' => 'type', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'wb_message', 'type' => 'text'],
                ['name' => 'page_name', 'type' => 'string','length'=>'300','index'=>1],
                ['name' => 'shared', 'type' => 'text'],
                ['name' => 'Liked', 'type' => 'text'],
                ['name' => 'Love', 'type' => 'text'],
                ['name' => 'Haha', 'type' => 'text'],
                ['name' => 'Wow', 'type' => 'text'],
                ['name' => 'Sad', 'type' => 'text'],
                ['name' => 'Angry', 'type' => 'text'],
                ['name' => 'sentiment', 'type' => 'string','length'=>'50'],
                ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'decided_keyword', 'type' => 'text'],
                ['name' => 'emotion', 'type' => 'string','length'=>'50'],
                ['name' => 'checked_emotion', 'type' => 'string','length'=>'50'],
                ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
                ['name' => 'change_predict','type'=>'boolean'],
                ['name' => 'checked_predict','type'=>'boolean'],
                ['name' => 'isBookMark', 'type' => 'boolean'],
                ['name' => 'isDeleted', 'type' => 'boolean'],
                ['name' => 'visitor', 'type' => 'boolean'],

            ];

            $fields_inbound_comments = [
                ['name' => 'id', 'type' => 'string','length'=>'50','unique'=>1],
                ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
                ['name' => 'message', 'type' => 'text'],
                ['name' => 'wb_message', 'type' => 'text'],
                ['name' => 'original_message', 'type' => 'text'],
                ['name' => 'unicode',  'type' => 'string','length'=>'50'],
                ['name' => 'post_id', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'comment_count', 'type' => 'text'],
                ['name' => 'sentiment', 'type' => 'string','length'=>'50'],
                ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50'],
                ['name' => 'decided_keyword', 'type' => 'text'],
                ['name' => 'emotion', 'type' => 'string','length'=>'50'],
                ['name' => 'checked_emotion', 'type' => 'string','length'=>'50'],
                ['name' => 'interest', 'type' => 'boolean'],
                ['name' => 'profile', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'tags', 'type' => 'text'],
                ['name' => 'tags_id', 'type' => 'text'],
                ['name' => 'checked_tags', 'type' => 'text'],
                ['name' => 'checked_tags_id', 'type' => 'text'],
                ['name' => 'change_predict', 'type' => 'boolean'],
                ['name' => 'checked_predict','type'=>'boolean'],
                ['name' => 'parent', 'type' => 'string','length'=>'50'],
                ['name' => 'action_status', 'type' => 'string','length'=>'50'],
                ['name' => 'action_taken', 'type' => 'string','length'=>'100'],
                ['name' => 'isBookMark', 'type' => 'boolean'],
                ['name' => 'manual_date', 'type' => 'timestamp'],
                ['name' => 'manual_by', 'type' => 'string','length'=>'100'],
                ['name' => 'page_name', 'type' => 'string','length'=>'300','index'=>1],
                ['name' => 'cmtLiked', 'type' => 'text'],
                ['name' => 'cmtLove', 'type' => 'text'],
                ['name' => 'cmtHaha', 'type' => 'text'],
                ['name' => 'cmtWow', 'type' => 'text'],
                ['name' => 'cmtSad', 'type' => 'text'],
                ['name' => 'cmtAngry', 'type' => 'text'],
                ['name' => 'cmtType', 'type' => 'string','length'=>'50'],
                ['name' => 'cmtLink', 'type' => 'text'],
                ['name' => 'tag_flag ','type'=>'boolean'],
               
                

            ];
              $fields_inbound_profiles = [
                ['name' => 'temp_id', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'id', 'type' => 'text'],
                ['name' => 'name', 'type' => 'text'],
                ['name' => 'type', 'type' => 'text'],
                
            ];

            $fields_predict_logs = [
                ['name' => 'id', 'type' => 'string','length'=>'50'],
                ['name' => 'type', 'type' => 'string','length'=>'10'],
                ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'decided_keyword', 'type' => 'text'],
                ['name' => 'checked_emotion', 'type' => 'string','length'=>'50','index'=>1],
                ['name' => 'checked_tags', 'type' => 'text'],
                ['name' => 'checked_tags_id', 'type' => 'text'],
                ['name' => 'tag_flag', 'type' => 'boolean'],
                ['name' => 'isHide', 'type' => 'boolean'],
                ['name' => 'change_predict', 'type' => 'boolean'],
                ['name' => 'checked_predict','type'=>'boolean'],
                ['name' => 'action_status', 'type' => 'string','length'=>'50'],
                ['name' => 'action_taken', 'type' => 'string','length'=>'100'],
                ['name' => 'isBookMark', 'type' => 'boolean'],
                ['name' => 'manual_date', 'type' => 'timestamp'],
                ['name' => 'manual_by', 'type' => 'string','length'=>'100'],
                ['name' => 'campaign', 'type' => 'integer'],
                
            ];

           $this->createTable($table_web_mentions, $fields_web_mentions);
           $this->createTable($table_posts, $fields_posts);
           $this->createTable($table_pages, $fields_pages);
           $this->createTable($table_followers, $fields_followers);
            $this->createTable($table_comments, $fields_comments);
           $this->createTable($table_inbound_posts, $fields_inbound_posts);
           $this->createTable($table_inbound_profiles, $fields_inbound_profiles,0);
           $this->createTable($table_inbound_comments, $fields_inbound_comments);
           return $this->createTable($table_predict_logs, $fields_predict_logs,1,'predict_logs');//predict_logs is table type for combine unique
       
    }
    function convertKtoThousand($value)
{
      $result=0;
     
        if(!empty($value[0]))
        {

        if(strpos($value[0],'K') !== false)
            {
            $new = str_replace("K", "", $value[0]);
             $new = 1000*$new;
            }
            else
            {
             $new=$value[0];   
            }

           
           $result = $result + (float)$new;
        }
        

        return $result;
}

   
    /**
     * To delete the tabel from the database 
     * 
     * @param $table_name
     *
     * @return bool
     */
    public function removeTable($table_name)
    {
        Schema::dropIfExists($table_name); 
        
        return true;
    }

//Ready function

        function date_compare($a, $b)
{
    $t1 = strtotime($a['datetime']);
    $t2 = strtotime($b['datetime']);
    return $t1 - $t2;
} 


        function thousandsCurrencyFormat($num) 
        {

          if($num>1000) {

                $x = round($num);
                $x_number_format = number_format($x);
                $x_array = explode(',', $x_number_format);
                $x_parts = array('k', 'm', 'b', 't');
                $x_count_parts = count($x_array) - 1;
                $x_display = $x;
                $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
                $x_display .= $x_parts[$x_count_parts - 1];

                return $x_display;

         }

            return $num;
        }

       //  function readmore($string)
       // {
       //    $string = strip_tags($string);
       //    if (strlen($string) > 500) {

       //                      // truncate string
       //                      $stringCut = substr($string, 0, 500);
       //                      $endPoint = strrpos($stringCut, ' ');

       //                      //if the string doesn't contain any space then it will cut without word basis.
       //                      $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                    
       //    }
       //    return $string;


       //  }
                function readmore($string,$length='')
       {
        if($length == '') $len = 500;
        else $len = $length;
          $string = strip_tags($string);
          if (strlen($string) > $len) {

                            // truncate string
                            $stringCut = substr($string, 0, $len);
                            $endPoint = strrpos($stringCut, ' ');

                            //if the string doesn't contain any space then it will cut without word basis.
                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                    
          }
          return $string;


        }

    function getbrandcolor($page){
      $customName= DB::select("Select * from brand_colors where origin_name = '".str_replace("'","\'",$page)."' OR custom_name = '".str_replace("'","\'",$page)."' OR keyword_group='".str_replace("'","\'",$page)."'");
      $color = null;
      foreach($customName as $name) $color = $name->color;
      return $color;
    }

}

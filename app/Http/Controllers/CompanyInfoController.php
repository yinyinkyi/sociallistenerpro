<?php

namespace BIMiner\Http\Controllers;

use BIMiner\CompanyInfo;
use BIMiner\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use BIMiner\Notifications\NewTagNotification;
use Illuminate\Support\Facades\Input;
use DB;

class CompanyInfoController extends Controller
{
    use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data,$id="")
    {
        return Validator::make($data, [
            'name' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            
           
        ]);
    }
    public function index()
    {     
          $data = [];
          $title="CompanyInfo";
          $source='in';
          $pid=Input::get("pid");
          $checked_data = $this->checkUserForProject($pid);
          if(count($checked_data)>0)
          {
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
          $companyData=$this->getCompanyData($pid);

          $login_user = auth()->user()->id;
          $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
          foreach($userdata as $action_permission) $action_permission = $action_permission->edit;
          
          $accountPermission = '';
         

            return view('company')->with('project_data',$project_data)
                                    ->with('permission_data',$permission_data )
                                    ->with('action_permission',$action_permission )
                                    ->with('count',$count)
                                    ->with('source',$source)
                                    ->with('title',$title)
                                    ->with('data',$companyData)
                                    ->with('companyData',$companyData)
                                    ->with('brand_id',$pid)
                                    ->with('accountPermission',$accountPermission);

          }
          else
          return abort(404);
    }

    public function getCompanyInfo()
    {
        $data = [];
        $companyInfo = CompanyInfo::all();

          if(count($companyInfo)>0)
          $data = $companyInfo[0];
      echo json_encode(array($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
      // dd($request->logo_path);
      // $file = $request->file('image');
      // print_r($file->getClientOriginalName());
      // return;
                   
        // if (file_exists($path)) {
        //     unlink($path) ;
        // }  
   $this->validate($request, [
            'name'=> 'required',
            'phone'=> 'required',
            'address'=> 'required',
            
        ]);
          $save_path='';
          $logo_path = $request->logo_path;

         if($logo_path){
            $extension = \File::extension($request->logo_path->getClientOriginalName());
            $save_path = "companyLogo_".$request->brand_id.".". $extension;

            $folder_path=public_path('Logo/');
            $file_path1=$folder_path . $save_path;
            if (file_exists($file_path1)) {
            unlink($file_path1) ;
        }  
             
            $request->logo_path->move(public_path('Logo'), $save_path);
        }
// dd($save_path);
       CompanyInfo::create([
            'name' => $request->name,
            'monitor_pages' => $request->monitor_pages,
            'phone' => $request->phone,
            'address' => $request->address,
            'logo_path' => $save_path,
            'brand_id' =>$request->brand_id
          
        ]);
         
        /*$this->guard()->login($user);*/
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);
        $accountPermission = '';
        
       return redirect()->route('companyInfo.index',['pid' => $request->brand_id,'accountPermission' => $accountPermission])
                        ->with('message','Record update successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \BIMiner\CompanyInfo  $companyInfo
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyInfo $companyInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \BIMiner\CompanyInfo  $companyInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyInfo $companyInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \BIMiner\CompanyInfo  $companyInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // dd($request->keyword_groups);
         $this->validate($request, [
            'name'=> 'required',
            'phone'=> 'required',
            'address'=> 'required',
        ]);

//dd($request->logo_path->getClientOriginalName());
        $logo_path = $request->logo_path;

       if($logo_path){
       $item =companyInfo::find($id);
       $folder_path=public_path('Logo/');
       $file_path1=$folder_path . $item->logo_path;
       
   
        if(file_exists($file_path1) && !empty($item->logo_path))
                {
              unlink($file_path1);
                }
            $extension = \File::extension($request->logo_path->getClientOriginalName());
            $input_p['logo_path'] ="companyLogo_".$request->brand_id.".". $extension;
              
            $request->logo_path->move(public_path('Logo'), $input_p['logo_path']);
        }

        $input_p['name']=$request->name;
        $input_p['phone']=$request->phone;
        $input_p['monitor_pages']=$request->monitor_pages;
        $input_p['keyword_groups']=$request->keyword_groups;
        $input_p['address']=$request->address;
        $input_p['brand_id']=$request->brand_id;
      //  $input_p['logo_path']=$request->logo_path;
        // dd($id);
     
       DB::table('company_info')->where('id',$id)->update($input_p);
       //  CompanyInfo::find($id)->update($input_p);
       $accountPermission = '';
        return redirect()->route('companyInfo.index',['pid' => $request->brand_id,'accountPermission' => $accountPermission])
                        ->with('message','Record update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \BIMiner\CompanyInfo  $companyInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyInfo $companyInfo)
    {
        //
    }
}

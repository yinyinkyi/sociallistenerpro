<?php

namespace BIMiner\Http\Controllers;
use BIMiner\MongodbData;
use BIMiner\MongoPages;
use BIMiner\MongoFollowers;
use BIMiner\Comment;
use BIMiner\MongoInboundPost;
use BIMiner\MongoInboundComment;
use BIMiner\MongoboundPost;
use BIMiner\MongoboundComment;
use BIMiner\MongoProfile;
use BIMiner\cron_log;
use BIMiner\User;
use BIMiner\CompanyInfo;
use BIMiner\PageRename;
use MongoDB;
use Illuminate\Http\Request;
use Auth;
use BIMiner\Project;
use BIMiner\ProjectKeyword;
use BIMiner\InboundPages;
use BIMiner\Page_Info;
use GuzzleHttp\Client;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use DB;

class ProjectController extends Controller
{
       use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

     public function brandCreate()
    {
        if($user=Auth::user())
        {
          $title="Setting";
          $companyData=[];
          $source = Input::get('source');
           $source ='in';
          //dd($source);
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          $accountPermission = '';
          return view('brand_create',compact('title','project_data','permission_data','count','source','companyData','accountPermission'));
        }
    }

     public function brandEdit($id)
    {
        if($user=Auth::user())
        {
          $checked_data = $this->checkUserForProject($id);
        
          if(count($checked_data)>0)
          {
          $source=Input::get('source');
          $source ='in';
          $title="Setting";
          $companyData=[];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $project = Project::find($id);
          $permission_data = $this->getPermission(); 
          $brand_page_arr = [];
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          
          $brandPageName = '';
          $pp= $project['monitor_pages'];
          // dd($pp);
          $op= $project['own_pages'];
          $brandPageName= $project['pages_name'];
            // dd($brandPageName);
            $monitor_pages = explode(',', $pp);
            // $pages = (object) $monitor_pages;
            
            $own_pages = explode(',', $op);
            $own_pages = (object) $own_pages;
            
            if($brandPageName <> '') $brand_page_arr = explode(',', $brandPageName);
            // $brandPageName = (object) $brandPageName;
            // dd($brandPageName);
            // dd(array_merge($monitor_pages,$brandPageName));/
            // dd($brand_page_arr);
            $data = [];
            // dd($monitor_pages,$brand_page_arr);
            foreach ($monitor_pages as $key => $value) {
              # code...

                
               $data[$key]['monitor'] = $value;
               if(empty($brand_page_arr)) $data[$key]['pg_name']= '';
              foreach($brand_page_arr as $key => $val){
                // dd($val);
                if($val == '') $data[$key]['pg_name'] = '';
              else $data[$key]['pg_name'] = $val;

            }
            }

            // dd($data);
        


          $keywords = ProjectKeyword::where('project_id',$id)->get();
          $accountPermission = '';
     
          return view('brand_edit',compact('title','project_data','count','project','permission_data','keywords','pages','brandPageName','data','own_pages','source','companyData','accountPermission'));
          }
          else
          {
            return abort(404);
          }
        }
    }
// public function Format_Page_Name($array)
//     {
//       $new_array=[];
//        foreach ($array as  $key => $row) {
//         $arr_page_name=explode("-",$row);
//         if(count($arr_page_name)>0)
//         {
//           $last_index=$arr_page_name[count($arr_page_name)-1];
//           if(is_numeric($last_index))
//           $new_array[] = $last_index;
//           else
//           $new_array[] = $row;
//         }
//         else
//         {
//           $new_array[]=$row;
//         }
       
          
      
//        }

//       return  $new_array;
//     }

    public function brandStore(Request $request)
    {
        if($user=Auth::user())
        {

          $pages='';
          $arr_pages=[];
          $my_pages='';
          $other_pages='';
          $brand_page_arr = [];
          $brand_page_name = '';
          $main_key= Input::post('main_key');
          $include_key = Input::post('include_key');
          $exclude_key =Input::post('exclude_key');
          $source = Input::get('source');
          if(null !== Input::get('monitor_pages'))
          {
          $monitor_pages = Input::get('monitor_pages');
          $pages = implode($monitor_pages,',');
             
          $my_pages=Input::get('chk_val');
          if(null !== $my_pages)
          $my_pages=implode(',', $my_pages);
          $other_pages=Input::get('unchk_val');
          if(null !== $other_pages)
          $other_pages=implode(',', $other_pages);
          

          }
          if(null !== Input::get('brand_page_name'))
          {
            $brand_page_arr = Input::get('brand_page_name');
            $brand_page_name = implode($brand_page_arr,',');
          }
          // dd(Input::get('brand_page_name'));
   
// dd($data);
 
// dd($data);
       
          //Insert data to Project table
           $project_id = DB::table('projects')->insertGetId([
                'name' => Input::get('brand_name'),
                'new_mentions' => "fake 100",
                'monitor_pages' => $pages,
                'pages_name'=>$brand_page_name,
                'own_pages' => $my_pages,
                'user_id'=>auth()->user()->id,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()

             ]);

           foreach ($monitor_pages as $key => $value) {
            $data[$key]['origin_name'] = $value;
            foreach ($brand_page_arr as $key => $val) {
              # code...
               $data[$key]['custom_name'] = $val;
            }
          }
         foreach ($data as $key => $value) {
            # code...

            $origin_name = $value['origin_name'];
             $custom_name = $value['custom_name'];

             if($custom_name == Null) $custom_name = $origin_name;
              // dd($origin_name,$custom_name);
            PageRename::create(['origin_name' => $origin_name ,'custom_name' => $custom_name,'brand_id' => $project_id,'created_at'=>now()->toDateTimeString(),'updated_at'=>now()->toDateTimeString()]);
          }

           //Inserted monitor pages into companyInfo (ztd)
                $company_info = DB::table('company_info')->insert([
              
                'monitor_pages' => $pages,
                'name' => Input::get('brand_name'),
                'pages_name'=>$brand_page_name,
                'brand_id' => $project_id,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()

             ]);

           //Insert My Pages to Mongodb for inbound Data Craw
           $my_pages=explode(',', $my_pages);
           $full_pages_name=$my_pages;
           // dd($my_pages);
           $my_pages = $this->Format_Page_Name($my_pages);
          
           $other_pages=explode(',', $other_pages);
           $full_otherpages_name=$other_pages;
           $other_pages = $this->Format_Page_Name($other_pages);
           

            $my_pg_count = count($my_pages);
            $other_pg_count = count($other_pages);
            
             for($i=0;$i<$my_pg_count;$i++){
             // dd($my_pages);
             $this->check_save_mongo($my_pages[$i],$full_pages_name[$i],1);
             $this->check_save_mysql($my_pages[$i],$full_pages_name[$i],1);

                  
            }
            //echo("hihi");
             for($i=0;$i<$other_pg_count;$i++){
              //echo $request->other_pages[$i];
             // echo("hihi");
              //print_r($other_pages);
             // print_r($other_pages[$i]);
               //dd($full_otherpages_name[$i]);
             $this->check_save_mongo($other_pages[$i],$full_otherpages_name[$i],0);
             $this->check_save_mysql($other_pages[$i],$full_otherpages_name[$i],0);
             
                  
            }

          
            //Insert data to Project Keyword
             $count=count($main_key);
             for($i=0;$i<$count;$i++)
             {
             //  // dd($pages);
              ProjectKeyword::create([
                'project_id' => $project_id,
                'main_keyword'=>$main_key[$i],
                'require_keyword' => $include_key[$i],
                'exclude_keyword' => $exclude_key[$i],
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()

              ]);

              }
            // Create Table for Brand
            $response=$this->operate($project_id);
            // //Down Brand Data From Mongodb and insert to Mysql
            if(http_response_code() == "200")
            if($my_pg_count>0) 
            $this->insert_Inbound_Data($project_id,$my_pages,'inbound_page');
            if($other_pg_count>0) 
             $this->insert_Inbound_Data($project_id,$other_pages,'outbound_page'); 

            // $this->Insert_Mention_Data($project_id);
           //add cron status
              $check_cron_log="SELECT id from cron_log  WHERE brand_id=".$project_id." AND cron_type='sync_mention' ";
               $check_cron_log = DB::select($check_cron_log);

                if( count($check_cron_log) < 1 )
                {
                      cron_log::create([
                      'brand_id' =>$project_id,
                      'finish_status' => 0,
                      'cron_type' =>'sync_mention',
       
                  ]);
                }
                $accountPermission = '';
            return redirect()->route('brandList',['source'=>$source,'accountPermission' =>$accountPermission]);
        }
    }

     public function brandUpdate()
    {
    
          $arr_pages=[];
          $my_pages='';
          $other_pages='';
          $brand_page_arr=[];
          $brand_page_name='';
          $data = [];
           // print_r($request->unchk_val);
           // dd($request->chk_val);
       
           $id =Input::get('id');
          $name = Input::get('brand_name');
          // $main_key= Input::post('main_key');
          // $include_key =Input::post('include_key');
          // $exclude_key =Input::post('exclude_key');
          $source = Input::get('source');
          $monitor_pages = Input::get('monitor_pages');

          $brand_page_arr = Input::get('brand_page_name');
          $brand_page_name = implode($brand_page_arr,',');
// dd($brand_page_arr);
        foreach ($monitor_pages as $key => $value) {
            # code...
            $data[$key]['origin_name'] = $value;
            foreach ($brand_page_arr as $key => $val) {
              # code...
                // if($val == null)  $data[$key]['custom_name']= $val;
               // else 
                $data[$key]['custom_name'] = $val;
            }
          }
          // dd($data);  
          foreach ($data as $key => $value) {
            # code...
            $origin_name = $value['origin_name'];
            $custom_name = $value['custom_name'];
            
            if($custom_name == Null) $custom_name = $origin_name;
            // dd($origin_name,$custom_name);
            $found = PageRename::where('origin_name',$origin_name)->where('brand_id',$id)->get()->toArray();
            // dd($found);
            if(!empty($found)){
             
              PageRename::where('origin_name',$origin_name)->where('brand_id',$id)->update(['custom_name' => $custom_name,'origin_name' => $origin_name,'updated_at' => now()->toDateTimeString()]);
            }
            else{
          
              PageRename::create(['custom_name' => $custom_name,'origin_name' => $origin_name,'brand_id'=>$id,'updated_at' => now()->toDateTimeString()]);
            }

            // PageRename::where('origin_name',$origin_name)->updateOrCreate(['custom_name' => $custom_name,'origin_name' => $origin_name,'updated_at' => now()->toDateTimeString()]);
          }


            if(isset($monitor_pages))
          {
          $pages = implode($monitor_pages,',');
             
          $my_pages=Input::get('chk_val');
          if(null !==$my_pages)
          $my_pages=implode(',', $my_pages);
          $other_pages=Input::get('unchk_val');
          if(null !==$other_pages)
           $other_pages=implode(',', $other_pages);
          

          }
            else
            {
              $pages=NULL;

            }

          $pj = Project::find($id);
          $pj->name = $name;
          $pj->monitor_pages = $pages;
          $pj->pages_name = $brand_page_name;
          $pj->own_pages= $my_pages;
          $pj->save();

          // update in company info
          $found = DB::table('company_info')->where('brand_id',$id)->get();
          if(count($found) <> 0) DB::table('company_info')->where('brand_id',$id)->update(['monitor_pages' => $pages,'pages_name' => $brand_page_name]);
     



         
          //$keyword = DB::table('project_keywords')->where('project_id',$id)->delete();

          // where('brand_id',$id)->delete();
            $my_pages=explode(',', $my_pages);
            $full_pages_name=$my_pages;
            $my_pages = $this->Format_Page_Name($my_pages);

           $other_pages=explode(',', $other_pages);
           $full_otherpages_name=$other_pages;
           $other_pages = $this->Format_Page_Name($other_pages);

            $my_pg_count = count($my_pages);
            $other_pg_count = count($other_pages);
            // dd($my_pg_count);
          
            for($i=0;$i<$my_pg_count;$i++){
             $this->check_save_mongo($my_pages[$i],$full_pages_name[$i],1);
             $this->check_save_mysql($my_pages[$i],$full_pages_name[$i],1);
                  
            }
            // dd('hello');

             for($i=0;$i<$other_pg_count;$i++){
              $this->check_save_mongo($other_pages[$i],$full_otherpages_name[$i],0);
              $this->check_save_mysql($other_pages[$i],$full_otherpages_name[$i],0);
                  
            }
           // dd('hello');
          // $count=count($main_key);
          // for($i=0;$i<$count;$i++)
          // {

          //    ProjectKeyword::create([
          //   'project_id' => $id,
          //   'main_keyword'=>$main_key[$i],
          //   'require_keyword' => $include_key[$i],
          //   'exclude_keyword' => $exclude_key[$i],
          //   'created_at' => now()->toDateTimeString(),
          //   'updated_at' => now()->toDateTimeString()

          //   ]);    

          // }
           //  dd($source);
           //$this->TruncateTable($id);
           //$this->new_table_add($id);
          // $this->new_table_add($id);


           if($my_pg_count>0) 
            $this->insert_Inbound_Data($id,$my_pages,'inbound_page');

           // dd('hello');
            if($other_pg_count>0) 
             $this->insert_Inbound_Data($id,$other_pages,'outbound_page'); 

          //   $this->Insert_Mention_Data($id);
           // $check_cron_log="SELECT id from cron_log  WHERE brand_id=".$id." AND cron_type='sync_mention' ";
           //     $check_cron_log = DB::select($check_cron_log);
           //      if( count($check_cron_log) > 0 )
           //      {
           //            DB::table('cron_log')
           //            ->where('brand_id', $id)
           //            ->update(['finish_status' =>0]);
           //      }
           //      else
           //      {
           //         cron_log::create([
           //            'brand_id' =>$id,
           //            'finish_status' => 0,
           //            'cron_type' =>'sync_mention',
       
           //        ]);
           //      }
          $accountPermission = '';
          return redirect()->route('brandList',['source'=>$source,'accountPermission' =>$accountPermission]);
    }

    public function check_save_mongo($monitor_page,$full_otherpages_name,$isadmin)
    {
      
       $result = InboundPages::where('page_name',$monitor_page)->get();
            // dd($result);
           if($result->isEmpty())
           {
            if($monitor_page <> "")
            
          InboundPages::create(['page_name' => $monitor_page,'full_page_name'=>$full_otherpages_name,'admin' => $isadmin]);
           }
           else
           {
             $update = InboundPages::where('page_name' , '=' , $monitor_page)->first();
             $update->admin = $isadmin;
             $update->full_page_name = $full_otherpages_name;
             $update->save();
           }
        return true;
    }

    public function check_save_mysql($monitor_page,$full_monitor_page,$isadmin)
    {
      
            $result = Page_Info::where('page_name',$monitor_page)->get();
           // dd($monitor_page);
           if($result->isEmpty())
           {
            if($monitor_page <> "")
            
            Page_Info::create(['page_name' => $monitor_page,'full_page_name' => $full_monitor_page,'admin' => $isadmin]);
           }
           else
           {
             $update = Page_Info::where('page_name' , '=' , $monitor_page)->first();
             $update->admin = $isadmin;
             $update->full_page_name = $full_monitor_page;
             $update->save();
           }
        return true;
    }

    public function getPageWhere($related_pages)
{
    $pages='';
     $filter_pages = '';
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  AND page_name in (".$pages.")";
      }
    return  $filter_pages;
}
  

    public function get_table_id($project_id,$table,$require_var,$related_pages=[])
    {
     
      $filter_pages=$this->getPageWhere($related_pages);
      
      $query = "SELECT ".$require_var." from temp_".$table ."  Where 1=1 " . $filter_pages;
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }
     public function get_common_table_id($table,$require_var)
    {
     
      $id_result=[];
      $query = "SELECT Distinct ".$require_var." from ".$table ."  Where 1=1 ";
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      foreach ($id_array as $key => $value) {
        if(null !== $value)
        $id_result[]=new MongoDB\BSON\ObjectId((string)$value);
        # code...
      }
      return $id_result;
    }

    public function insert_Inbound_Data($project_id,$my_pages,$crawl_type)
    {

     
      //for edit

     
     // $keyword_data = $this->getprojectkeywork($project_id);
     // $filter['$or'] = $this->getkeywordfilter($keyword_data);

      
        $page['$or']= $this->getpagefilter($my_pages);
 
 
       $inbound_post_id_array = $this->get_table_id($project_id,"inbound_posts","id",$my_pages);

       $inbound_post_result=MongoboundPost::raw(function ($collection) use($page,$inbound_post_id_array,$crawl_type) {//print_r($filter);

        return $collection->aggregate([
            [
            '$match' =>[
                 '$and'=> [ 
                 ['id'=> ['$exists'=> true]],
                 // ['crawl_type'=>$crawl_type],
                 ['id'=>[ '$nin'=> $inbound_post_id_array ] ],
                 $page
                          ]
            ]  
                       
           ],
            
        ]);
          })->toArray();
 


       $data=[];
       $page_array=[];
       $data_message=[];


       foreach ($inbound_post_result as  $key => $row) {
        $id ='';$datetime=NULL;
        $full_picture ='';
        $link ='';
        $name ='';$visitor=0;
        $message ='';$type ='';$original_message ='';$unicode ='';
        $page_name ='';
        $share =0;
        $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                        // $reactUpdatedAt=NULL;
        $isDeleted=0;

        if(isset($row['message'])) $message = $row['message'];
        $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

        if(isset($row['original_message'])) $original_message = $row['original_message'];

        if(isset($row['unicode'])) $unicode = $row['unicode'];
        if(isset($row['created_time']))
        {
          $utcdatetime = $row["created_time"];
          $datetime = $utcdatetime->toDateTime();
          $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
          $datetime = $datetime->format('Y-m-d H:i:s');
        }


        if(isset($row['id'])) $id =$row['id'];
        if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
        if(isset($row['link'])) $link = $row['link'];
        if(isset($row['name'])) $name = $row['name'];

        if(isset($row['type'])) $type = $row['type'];
        if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
        if(isset($row['share'])) $share = $row['share'];
        if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand($row['reaction']['Like']);
        if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand($row['reaction']['Love']);
        if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand($row['reaction']['Wow']);
        if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand($row['reaction']['Haha']);
        if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand($row['reaction']['Sad']);
        if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand($row['reaction']['Angry']);
        if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
        if(isset($row['emotion'])) $emotion = $row['emotion'];

                        //  if(isset($row['UpdatedAt']))
                        // {
                        // $utcdatetime = $row["UpdatedAt"];
                        // $reactUpdatedAt = $utcdatetime->toDateTime();
                        // $reactUpdatedAt=$reactUpdatedAt->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                        // $reactUpdatedAt = $reactUpdatedAt->format('Y-m-d H:i:s');
                        // }
        if(isset($row['mark'])) 
        {
          if($row['mark'] == 'deleted')
          {
           $isDeleted =1;
         }

       }
       if(isset($row['visitor'])) 
       {
        if($row['visitor'] == '1')
        {
         $visitor =1;
       }

     }
     $data[] =[
      'id' => $id,
      'full_picture' =>$full_picture,
      'link' =>$link,
      'name' =>$name,
      'message' => $message,
      'original_message'=>$original_message,
      'unicode'=>$unicode,
      'type' =>$type,
      'wb_message' =>'',
      'page_name' =>$page_name,
      'shared' =>$share,
      'Liked' =>$Like,
      'Love' =>$Love,
      'Wow' =>$Wow,
      'Haha' =>$Haha,
      'Sad' =>$Sad,
      'Angry' =>$Angry,
      'sentiment' =>$sentiment,
      'checked_sentiment' =>$sentiment,
      'decided_keyword' =>'',
      'emotion' =>$emotion,
      'checked_emotion' =>$emotion,
      'created_time' =>$datetime,
      'change_predict'=>0,
      'checked_predict'=>0,
      'isBookMark' =>0,
                            // 'reactUpdatedAt' =>$reactUpdatedAt,
      'isDeleted' =>$isDeleted,
      'visitor'=>$visitor,
      'created_at' => now()->toDateTimeString(),
      'updated_at' => now()->toDateTimeString()
          ];                 

        }

        $path = storage_path('app/data_output/posts'.$crawl_type.'_'.$project_id.'.csv');

        if (file_exists($path)) {
          unlink($path) ;
        } 
        $this->doCSV($path,$data);

        $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_inbound_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,original_message,unicode,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,created_time,change_predict,checked_predict,isBookMark,isDeleted,visitor,created_at,updated_at)";

        $pdo = DB::connection('mysql')->getPdo();
        $pdo->exec($query_load);
        if (file_exists($path)) {
          unlink($path) ;
        }


        $post_id = $this->getInboundPostId($project_id,$my_pages);/*dd( $filter);*/
        $inbound_cmt_id_array = $this->get_table_id($project_id,"inbound_comments","id");
// dd('hello');

              $inbound_comment_result=MongoboundComment::raw(function ($collection) use($page,$inbound_cmt_id_array,$post_id) {//print_r($filter);

                return $collection->aggregate([
                  [
                    '$match' =>[
                     '$and'=> [ 
                       ['id'=> ['$exists'=> true]],
                       ['post_id'=>['$in'=>$post_id]],
                       ['page_name'=>[ '$in'=> ['onepaymm','nearme1stop'] ] ],
                       // ['id'=>[ '$nin'=> $inbound_cmt_id_array ] ],
                       

                     ]
                   ]  

                 ],

               ]);
              })->toArray();
// dd($inbound_comment_result);
              $profile_id=[];
              $data_comment=[];
              foreach ($inbound_comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';
                $comment_count =0;$original_message ='';$unicode ='';$profile='';$page_name='';
                $cmtLiked =0;$cmtLove=0;$cmtWow=0;$cmtHaha=0;$cmtSad=0;$cmtAngry=0;$cmtType='';$cmtLink='';
                $sentiment='';$emotion='';$parent='';
                $tags='';
                if(isset($row['profile']))
                {
                  $profile = $row['profile'];
                  $profile_id[]=new MongoDB\BSON\ObjectId((string)$profile);
                } 

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['original_message'])) $original_message = $row['original_message'];
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['parent'])) $parent =  $row['parent'];
                if(isset($row['page_name'])) $page_name = $row['page_name'];

                if(isset($row['reaction']['Like'])) $cmtLiked =$this->convertKtoThousand($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $cmtLove =$this->convertKtoThousand($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $cmtWow =$this->convertKtoThousand($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $cmtHaha =$this->convertKtoThousand($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $cmtSad =$this->convertKtoThousand($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $cmtAngry =$this->convertKtoThousand($row['reaction']['Angry']);

                if(isset($row['type'])) $cmtType = $row['type'];
                if(isset($row['link'])) $cmtLink = $row['link'];

                $data_comment[] =[
                  'id' => $id,
                  'created_time' =>$datetime,
                  'message' => $message,
                  'wb_message' =>'',
                  'original_message'=>$original_message,
                  'unicode'=>$unicode,
                  'post_id' =>$post_id,
                  'comment_count' =>$comment_count,
                  'sentiment' =>$sentiment,
                  'checked_sentiment' =>$sentiment,
                  'decided_keyword' =>'',
                  'emotion' =>$emotion,
                  'checked_emotion' =>$emotion,
                  'interest' =>'',
                  'profile'=>$profile,
                  'tags'=>'',
                  'tags_id'=>'',
                  'checked_tags'=>'',
                  'checked_tags_id'=>'',
                  'change_predict'=>0,
                  'checked_predict'=>0,
                  'parent'=>$parent,
                  'action_status'=>'',
                  'action_taken'=>'',
                  'isBookMark' =>0,
                  'manual_date'=>'NULL',
                  'page_name'=>$page_name,
                  'cmtLiked' =>$cmtLiked ,
                  'cmtLove' =>$cmtLove,
                  'cmtWow' =>$cmtWow,
                  'cmtHaha' =>$cmtHaha,
                  'cmtSad' =>$cmtSad,
                  'cmtAngry' =>$cmtAngry,
                  'cmtType' =>$cmtType,
                  'cmtLink' =>$cmtLink,
                  'created_at' => now()->toDateTimeString(),
                  'updated_at' => now()->toDateTimeString()
                ];                 

              }

              $path = storage_path('app/data_output/comment'.$project_id.'.csv');

              if (file_exists($path)) {
                unlink($path) ;
              } 
              $this->doCSV($path,$data_comment);
              $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_inbound_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,interest,profile,tags,tags_id,checked_tags,checked_tags_id,change_predict,checked_predict,parent,action_status,action_taken,isBookMark,manual_date,page_name,cmtLiked,cmtLove,cmtWow,cmtHaha,cmtSad,cmtAngry,cmtType,cmtLink,created_at,updated_at)";
              $pdo = DB::connection()->getPdo();
              $pdo->exec($query_load);

              if (file_exists($path)) {
                unlink($path) ;
              }

              $profile_id_array = $this->get_common_table_id("temp_profiles","temp_id");

                 $profiles_result=MongoProfile::raw(function ($collection) use($profile_id,$profile_id_array) {//print_r($filter);
                   return $collection->aggregate([
                    [
                      '$match' =>[
                        '$and'=> [ 
                          ['_id' => ['$in' => $profile_id ] ] ,
                          ['_id'=>[ '$nin'=> $profile_id_array ] ],

                        ]

                      ]  
                    ]
                    
                  ]);
                 })->toArray();

                 $data_profiles=[];
                 foreach ($profiles_result as  $key => $row) {
                  $_id ='';
                  $id ='';
                  $name ='';
                  $type ='';

                  if(isset($row['_id'])) $_id =$row['_id'];
                  if(isset($row['id'])) $id =$row['id'];
                  if(isset($row['name'])) $name =$row['name'];
                  if(isset($row['type'])) $type = $row['type'];



                  $data_profiles[] =[
                    'temp_id' => $_id,
                    'id' => $id,
                    'name' =>$name,
                    'type' => $type,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                  ];                 

                }
                $path = storage_path('app/data_output/profiles'.$project_id.'.csv');

                if (file_exists($path)) {
                  unlink($path) ;
                } 
                $this->doCSV($path,$data_profiles);

                $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_profiles FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (temp_id,id,name,type,created_at,updated_at)";

                $pdo = DB::connection()->getPdo();
                $pdo->exec($query_load);

                if (file_exists($path)) {
                  unlink($path) ;
                }  

                  // return;

                  // $follow_id_array = $this->get_table_id($project_id,"followers","page_name");
                  // $follower_result=MongoFollowers::raw(function ($collection) use($my_pages,$follow_id_array) {//print_r($filter);
                  //  return $collection->aggregate([
                  //         [
                  //         '$match' =>[
                  //              '$and'=> [ 
                  //           ['page_name' => ['$in' => $my_pages ] ],
                  //           ['page_name'=>[ '$nin'=> $follow_id_array ] ], 

                  //       ]

                  //         ]  
                  //            ]

                  //     ]);
                  // })->toArray();

                  // /* print_r($follower_result);
                  //  return;*/
                  //  $data_followers=[];
                  //  foreach ($follower_result as  $key => $row) {
                  //                 $id ='';
                  //                 $page_name ='';
                  //                 $fan_count =0;
                  //                 $date ='';


                  //                 if(isset($row['id'])) $id =$row['id'];
                  //                 if(isset($row['page_name'])) $page_name =$row['page_name'];
                  //                 if(isset($row['fan_count'])) $fan_count = $row['fan_count'];
                  //                 if(isset($row['date'])) $date = $row['date'];
                  //                 if(isset($row['reaction'])) $reaction =$row['reaction'];

                  //                  $data_followers[] =[
                  //                     'id' => $id,
                  //                     'page_name' =>$page_name,
                  //                     'fan_count' => $fan_count,
                  //                     'date' =>$date,
                  //                     'created_at' => now()->toDateTimeString(),
                  //                     'updated_at' => now()->toDateTimeString()
                  //                     ];                 

                  //   }
                  //  $path = storage_path('app/data_output/followers'.$project_id.'.csv');

                  // if (file_exists($path)) {
                  //     unlink($path) ;
                  // } 
                  // $this->doCSV($path,$data_followers);

                  // $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_followers FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,page_name,fan_count,date,created_at,updated_at)";

                  // $pdo = DB::connection()->getPdo();
                  //  $pdo->exec($query_load);
                  //  if (file_exists($path)) {
                  //     unlink($path) ;
                  // } 

                $page_id_array = $this->get_table_id($project_id,$project_id."_pages","page_name");
                 $pages_result=MongoPages::raw(function ($collection) use($my_pages,$page_id_array) {//print_r($filter);
                   return $collection->aggregate([
                    [
                      '$match' =>[
                       '$and'=> [ 
                        ['page_name' => ['$in' => $my_pages ] ] ,
                        [ 'page_name'=>[ '$nin'=> $page_id_array ] ],

                      ]

                    ]  
                  ]

                ]);
                 })->toArray();

                 $data_pages=[];
                 foreach ($pages_result as  $key => $row) {
                  $page_name ='';
                  $about ='';
                  $name ='';
                  $category ='';
                  $sub_category ='';
                  $profile='';

                  if(isset($row['page_name'])) $page_name =$row['page_name'];
                  if(isset($row['name'])) $name =$row['name'];
                  if(isset($row['about'])) $about = $row['about'];
                  if(isset($row['category'])) $category = $row['category'];
                  if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
                  if(isset($row['profile'])) $profile = $row['profile'];
                            //  if(isset($row['reaction'])) $reaction =$row['reaction'];


                  $data_pages[] =[
                    'page_name' => $page_name,
                    'name' =>$name,
                    'about' => $about,
                    'category' =>$category,
                    'sub_category' =>$sub_category,
                    'profile'=>$profile,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                  ];                 

                }
                $path = storage_path('app/data_output/pages'.$project_id.'.csv');

                if (file_exists($path)) {
                  unlink($path) ;
                } 
                $this->doCSV($path,$data_pages);

                $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_pages FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (page_name,name,about,category,sub_category,profile,created_at,updated_at)";

                $pdo = DB::connection()->getPdo();
                $pdo->exec($query_load);

                if (file_exists($path)) {
                  unlink($path) ;
                } 

                return true;

    }


function doJson($path,$json)
{
    $fp = fopen($path, 'w');
   fwrite($fp, $json);
   fclose($fp);
}
   function doCSV($path, $array)
{
   $fp = fopen($path, 'w');
    $i = 0;
    foreach ($array as $fields) {
        if($i == 0){
            fputcsv($fp, array_keys($fields));
        }
        fputcsv($fp, array_values($fields));
        $i++;
     }

   fclose($fp);
}

    function convertKtoThousand($s)
{
    if (strpos(strtoupper($s), "K") != false) {
      $s = rtrim($s, "kK");
      return floatval($s) * 1000;
    } else if (strpos(strtoupper($s), "M") != false) {
      $s = rtrim($s, "mM");
      return floatval($s) * 1000000;
    } else {
      return floatval($s);
    }
}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function TruncateTable($id)
  {
          if (Schema::hasTable('temp_posts')) {
              DB::table('temp_posts')->truncate();

             }
               if (Schema::hasTable('temp_comments')) {
              DB::table('temp_comments')->truncate();
             }
               if (Schema::hasTable('temp_followers')) {
              DB::table('temp_followers')->truncate();
             }
               if (Schema::hasTable('temp_pages')) {
              DB::table('temp_pages')->truncate();
             }
             if (Schema::hasTable('temp_inbound_posts')) {
              DB::table('temp_inbound_posts')->truncate();
             }
             if (Schema::hasTable('temp_inbound_comments')) {
              DB::table('temp_inbound_comments')->truncate();
             }
             return true;
  }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function brand_delete($id)
    {
        $delete_id = $id;
       // $prj = DB::table('projects')->where('id',$id)->delete();
    //    $keywords =  DB::table('project_keywords')->where('project_id', $id)->delete();
        // dd($id);
      //  $pages = InboundPages::where('brand_id',$id)->delete();
         if($this->DropTable($delete_id))
         {
            //dd($id);
          $prj = DB::table('projects')->where('id',$delete_id)->delete();
         }
        return redirect('brandList')->with([
        'flash_message' => 'Deleted',
        'flash_message_important' => false
  ]);
    }

    public function DropTable($id)
  {
    // Schema::dropIfExists('temp_'.$id.'_posts'); 
    // Schema::dropIfExists('temp_'.$id.'_comments'); 
    // Schema::dropIfExists('temp_'.$id.'_followers'); 
    // Schema::dropIfExists('temp_'.$id.'_pages'); 
    // Schema::dropIfExists('temp_'.$id.'_inbound_posts'); 
    // Schema::dropIfExists('temp_'.$id.'_inbound_comments'); 

    return true;
  }

     public function getProjectData ()
   {
     $source='';
     if(!null==Input::get('source'))
     $source=Input::get('source');

     $login_user = auth()->user()->id;
     $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
     foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

     $checked_data = User::select('*')->where('id', $login_user)->orderBy('id','DESC')->get();
     // $query="select  brand_id from manage_dbs  where user_id=".$login_user . " order by id";
     // $checked_data = DB::select($query);
     $brand_id = '';
     foreach ($checked_data as $key => $value) {
       $brand_id = $value->brand_id;
     }
     $permission_data = $this->getPermission();
     $projectlist = Project::select(['id', 'name', 'created_at'])
                            ->where('id',$brand_id)
                            ->orderBy('id');

     // echo $source ;
     // return;

     return Datatables::of($projectlist)
       ->addColumn('action', function ($projectlist) use($permission_data,$source,$action_permission) {
        $html='';
                // if($permission_data['setting'] === 1)
                // {
        if($action_permission == 0){ 
              $html ='<a href="" class="btn btn-xs btn-primary show_alert" data-toggle="collapse"><i class="mdi mdi-table-edit"></i> Edit</a> 
                        <button class="btn btn-xs btn-danger show_alert" data-toggle="collapse"><i class="mdi mdi-delete"></i>Delete</button>';
          }
        else{

              $html ='<a href="brandList/edit/'.$projectlist->id.'?source='.$source.'" class="btn btn-xs btn-primary"><i class="mdi mdi-table-edit"></i> Edit</a> 
            <button class="btn btn-xs btn-danger btn-delete" data-remote="' . route('brandList/delete', $projectlist->id) . '"><i class="mdi mdi-delete"></i>Delete</button>';
          }

              $html.='<a href="dashboard?pid='.$projectlist->id.'&source='.$source.'" class="btn btn-xs btn-secondary"><i class="fas fa-home"></i> Dashboard</a>';
                // }
               return $html;

            })
       ->editColumn('created_at', function ($projectlist) {
                 if (isset($projectlist->created_at)) {
                return   $projectlist->created_at->format('d/m/Y');
            }
            })
       ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })

      ->make(true);
           // ->rawColumns(['image', 'action'])


 }
 function page_delete()
 {
   $pages = InboundPages::where('brand_id',77)->delete();
 }

}

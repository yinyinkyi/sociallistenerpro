<?php

namespace BIMiner\Http\Controllers;

use BIMiner\tags;
use BIMiner\User;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use BIMiner\Notifications\NewTagNotification;
use Illuminate\Support\Facades\Input;
use Ixudra\Curl\Facades\Curl;
class tagsController extends Controller
{
     use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected function validator(array $data,$id="")
    {
        return Validator::make($data, [
        //     'name' => 'required|string|max:15|unique:tags,name,brand_id,' . $data['brand_id'].','.$id,
        //    // 'keywords' => 'required|string'
            // 'name' => 'required|string|max:15|unique:tags,name,NULL,NULL,brand_id,'.$data['brand_id'],
            // 'brand_id' => 'required|unique:tags,brand_id,NULL,NULL,name,'.$data['name'],
             'name' => 'required|string|max:80|unique:tags,name,'.$id.',id,brand_id,' . $data['brand_id'],
              // 'category_id' => 'required|integer',
           
           
           
        ]);
    }


    private function getCurlValue($filename, $contentType, $postname)
    {
        // PHP 5.5 introduced a CurlFile object that deprecates the old @filename syntax
        // See: https://wiki.php.net/rfc/curl-file-upload
        if (function_exists('curl_file_create')) {
            return curl_file_create($filename, $contentType, $postname);
        }
    
        // Use the old style if using an older version of PHP
        $value = "@{$filename};filename=" . $postname;
        if ($contentType) {
            $value .= ';type=' . $contentType;
        }
    
        return $value;
    }

    public function upload_asr(){

      $file_path = public_path('check_bill.wav');
      $cfile = $this->getCurlValue($file_path,false,'tes.wav');  

      //dd($cfile);

      $url = 'https://elb.baganasr.com/node/api/v1/get-text';
      $token_params = array(
          "token" => "Basic Nzg0MDEwZDZkNzkyY2JjYzpqRjU4by9HNlV1UVcvdm9EbG9mSlJqRFhpQWM9Cg==",
          "device_id" => "cms",
          "audio" => $cfile
      );
      $response = Curl::to($url)
      ->withData($token_params)
      ->withContentType('multipart/form-data')
      // ->withHeader('Authorization: Bearer '.$this->access_token)
      ->withHeader('Accept: application/json')
      ->containsFile()
      ->post();
      dd($response);
    }

    public function index()
    {
        
          $title="TAG";
          $source='in';
          $pid=Input::get("pid");
          $checked_data = $this->checkUserForProject($pid);

          $login_user = auth()->user()->id;
          $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
          foreach($userdata as $action_permission) $action_permission = $action_permission->edit;


          if(count($checked_data)>0)
          {
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
          $companyData=$this->getCompanyData($pid);
         // $category = tagCategory::select(['id', 'category_name'])->orderBy('category_name')->get();
          $accountPermission = '';
          return view('tag_entry')->with('project_data',$project_data)
                                    ->with('permission_data',$permission_data )
                                    ->with('action_permission',$action_permission)
                                    ->with('count',$count)
                                    ->with('source',$source)
                                    ->with('title',$title)
                                    ->with('companyData',$companyData)
                                   // ->with('category',$category)
                                    ->with('brand_id',$pid)
                                    ->with('accountPermission',$accountPermission);

          }
          else
          {
              return abort(404);
          }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validator($request->all())->validate();
        // $this->validate($request, [
        //     'name' => 'required|string|max:20|unique:tags,name,NULL,NULL,brand_id,'.$request->brand_id,
        //     'brand_id' => 'required|unique:tags,brand_id,NULL,NULL,name,'.$request->name,
            
        // ]);
       
       // $pid= $request->brand_id;

       tags::create([
            'name' => $request->name,
            // 'category_id' => $request->category_id,
            'original_name' => $request->name,
            //'keywords' =>$request->keywords,
            'brand_id' =>$request->brand_id
          
        ]);
                 /*$this->guard()->login($user);*/
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);
      
        $users = User::all();
         $login_user = auth()->user()->id;
      
      // if(\Notification::send($users,new NewTagNotification(tags::latest('id')->first())))
      //     if(\Notification::send($users,new NewTagNotification(tags::all())))
      // {
      //   return redirect('tags')->with('project_data',$project_data)
      //                               ->with('count',$count)
      //                               ->with('message', 'Successfully created a new account.');
      // }
          $accountPermission = '';

      return redirect()->route('tags.index',['pid' => $request->brand_id,'accountPermission'=>$accountPermission])
                        ->with('message','Successfully created a new tag.');

        //  return redirect('tags')->with('project_data',$project_data)
        //                             ->with('count',$count)
        //                            -> with('pid', $pid)
        //                             ->with('message', 'Successfully created a new tag.');

        
    }
    public function quick_tag()
    {
       $name= Input::post("name");
       $keywords=Input::post("keywords");
       $brand_id=Input::post("brand_id");
       // $category_id=Input::post("category_id");
if (tags::where('name', '=', $name)->where('brand_id', '=',  $brand_id)->exists()) {
   return "exist";
}
        $tags = tags::create([
            'name' => $name,
            'keywords' => $keywords,
            'brand_id' => $brand_id,
            // 'category_id' => $category_id
          
        ]);
        return  $tags->id;

    }

    public function notification()
    {
        return auth()->user()->unreadNotifications;
    }

    /**
     * Display the specified resource.
     *
     * @param  \BIMiner\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function show(tags $tags)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \BIMiner\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

         $title="Tag";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission();
          $brand_id=Input::get("pid");
            $checked_data = $this->checkUserForProject($brand_id);
        
          if(count($checked_data)>0)
          {
          $companyData=$this->getCompanyData($brand_id);
          $data = tags::find($id);
          //$category = tagCategory::select(['id', 'category_name'])->orderBy('category_name')->get();
          $source='in';
          $accountPermission = '';

         return view('tag_entry',compact('data','title','project_data','count','source','permission_data','brand_id','companyData','accountPermission'));
          }
          else
          {
            return abort(404);
          }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \BIMiner\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
         $this->validator($request->all(),$id)->validate();
        //  $this->validate($request, [
        //     'name' => 'required|string|max:15|unique:tags,name,NULL,NULL,brand_id,'.$request->brand_id,
        //     'brand_id' => 'required|unique:tags,brand_id,NULL,NULL,name,'.$request->name.','.$id,
            
        // ]);
        
        $input_p['name']=$request->name;//new tag name
        //$input_p['original_name'] = $request->name;
        // $input_p['category_id']=$request->category_id;
       // $input_p['keywords']=$request->keywords;
        $input_p['brand_id']=$request->brand_id;
        
       // before update data change in comment tags
           
        tags::find($id)->update($input_p);
          $accountPermission = '';

        return redirect()->route('tags.index',['pid' => $request->brand_id,'accountPermission' => $accountPermission])
                        ->with('message','Record update successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \BIMiner\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $brand_id=0;

        // $project_data = $this->getProject();
        // if(count($project_data)>0)
        //   {
        //   $brand_id = $project_data[0]['id'];
        //   }
          // dd($brand_id);

        $tags = tags::find($id);
        // dd($tags);  
        if(!empty($tags))
        {
        $tagName=$tags->name;
        $brand_id=$tags->brand_id;
        $con1="SELECT  id,checked_tags_id   FROM temp_inbound_comments  where checked_tags_id <> '' and checked_tags_id Like '%,".$id."%'";
        $con1_result = DB::select($con1);
        if(count($con1_result)>0)
        {
        $update="UPDATE temp_inbound_comments
                 SET checked_tags_id  = REPLACE(checked_tags_id,',".$id."','') 
                 WHERE checked_tags_id like '%,".$id."%';";
        $update_result = DB::select($update);
        }
       
        $con2="SELECT  id,checked_tags_id  FROM temp_inbound_comments  where checked_tags_id <> '' and checked_tags_id Like '%".$id.",%'";
        $con2_result = DB::select($con2);
             if(count($con2_result)>0)
            {
            $update="UPDATE temp_inbound_comments
                     SET checked_tags_id  = REPLACE(checked_tags_id,'".$id.",','') 
                     WHERE checked_tags_id like '%".$id.",%';";
            $update_result = DB::select($update);
                 
            }       

          
            $con3="SELECT  id,checked_tags_id  FROM temp_inbound_comments  where checked_tags_id <> '' and checked_tags_id Like '%".$id."%'";
            $con3_result = DB::select($con3);
                if(count($con3_result)>0)
                {
                    
                $update="UPDATE temp_inbound_comments
                         SET checked_tags_id  = REPLACE(checked_tags_id,'".$id."','') 
                         WHERE checked_tags_id like '%".$id."%';";
                $update_result = DB::select($update);
                }
            

        
         tags::find($id)->delete();
            
         
           
        }
          $accountPermission = '';
        
        return redirect()->route('tags.index',['pid' => $brand_id,'accountPermission' => $accountPermission])
                        ->with('success','Record deleted successfully');
    }
     public function gettaglist()
   {
     $source='in';
     $username = auth()->user()->username;
     $brand_id=Input::get('brand_id');
     $taglist = tags::select(['id', 'name', 'keywords'])->where('brand_id', '=', $brand_id)->orderBy('id');

     $login_user = auth()->user()->id;
     $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
     foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

    // $taglist = "SELECT tags.id ,tags.name,tags.keywords,tags.category_id  FROM tags JOIN tag_categories ON tags.category_id=tag_categories.id where brand_id = ".$brand_id." and active=1  ORDER BY tags.id";
    //  $taglist = DB::select($taglist);
     
     return Datatables::of($taglist)
       ->addColumn('action', function ($taglist) use($source,$brand_id,$username,$action_permission) {
               if($action_permission == 0 ){ 
                return '<a href="" class="btn btn-xs btn-primary show_alert" data-toggle="collapse"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
                 <button class="btn btn-xs btn-danger show_alert" data-toggle="collapse"><i class="mdi mdi-delete"></i>Delete</button>';
               }
               else
               {
                return '<a href="'. route('tags.edit', ['id'=>$taglist->id,'pid'=>$brand_id]) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
                 <button class="btn btn-xs btn-danger btn-delete" data-remote="' . route('tags/delete', $taglist->id) . '"><i class="mdi mdi-delete"></i>Delete</button>';
               }

            })

    /* ->addColumn('action', function ($booking) {
        return '<a href="'. route('deleteBooking',$booking->id) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

    })*/

     ->make(true);
           // ->rawColumns(['image', 'action'])

 }
}

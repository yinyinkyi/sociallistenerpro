<?php

namespace BIMiner\Http\Controllers;

use BIMiner\tagGrouplist;
use BIMiner\tagsGroup;
use BIMiner\User;
use BIMiner\tags;
use DB;
use Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use BIMiner\Notifications\NewTagNotification;
use Illuminate\Support\Facades\Input;

class tagsGroupController extends Controller
{
    use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data,$id="")
    {
        return Validator::make($data, [
               'name' => 'required|string|unique:tagsGroup,name,'.$id.',id,brand_id,' . $data['brand_id'],
            
        ]);
    }

    public function index()
    {
       $title="Tag Group";
          $source='in';
          $pid=Input::get("pid");
          $checked_data = $this->checkUserForProject($pid);

           $login_user = auth()->user()->id;
           $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
           foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

          if(count($checked_data)>0)
          {
              $project_data = $this->getProject();
              $permission_data = $this->getPermission(); 
              $count = $this->getProjectCount($project_data);
              $companyData=$this->getCompanyData($pid);
              $accountPermission = '';
              return view('tagGroup.tagGroupList')->with('project_data',$project_data)
                                        ->with('permission_data',$permission_data )
                                        ->with('action_permission',$action_permission )
                                        ->with('count',$count)
                                        ->with('source',$source)
                                        ->with('title',$title)
                                        ->with('companyData',$companyData)
                                        ->with('brand_id',$pid)
                                        ->with('accountPermission',$accountPermission);

          }
          else
          {
              return abort(404);
          }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
          $title="Create Group";
          $companyData=[];
          $source = Input::get('source');
          $source ='in';
          //dd($source);
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
           $taglist = tags::select(['id', 'name', 'keywords'])
                         ->where('brand_id', '=', $pid)->where('active', '=', 1)->orderBy('name')->get();
              $accountPermission = '';
          
        return view('tagGroup.tagGroupEntry',compact('title','project_data','count','pid','companyData','taglist','source','pid','accountPermission'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tagGroupid='';
        $name=Input::post('name');
        $brand_id=Input::post('brand_id');
        $selectedTag=Input::post('selectedTag');
        $tagGroupid=Input::post('id');
        $input_p['name']=$request->name;

        if($tagGroupid <> '')
        {
          $validator = Validator::make($input_p, [
            'name' => 'required|string|unique:tagsGroup,name,'.$tagGroupid.',id,brand_id,' . $brand_id,
            
          ]);
        }
        else
        {
          $validator = Validator::make($input_p, [
              'name' => 'required|string|unique:tagsGroup,name,NULL,NULL,brand_id,'.$brand_id,
            
        ]);
        }

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        DB::beginTransaction();
    try{

        if($tagGroupid <> '')
        {
        $res=tagsGroup::where('id',$tagGroupid)->delete();
        $res=tagGrouplist::where('tagGroup_id',$tagGroupid)->delete();
        }

        $tagGroupid = tagsGroup::create([
            'name' => $name,
            'brand_id' =>$brand_id,         
        ])->id;

        foreach ($selectedTag as $key => $value) {

            tagGrouplist::create([
            'tagGroup_id' => $tagGroupid,
            'tag_id' =>$value,         
             ]);
        }
        DB::commit();
        return response()->json(['success'=>'Record is successfully added']);
      }
        catch(\Exception $e){
          //if there is an error/exception in the above code before commit, it'll rollback
          DB::rollBack();
          return $e->getMessage();
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $title="Edit Group";
          $companyData=[];
          $source = Input::get('source');
          $source ='in';
          //dd($source);
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
           $tagGroup = tagsGroup::select(['id','name'])
                         ->where('id', '=', $id)->get();
             
           $taglist = tags::select(['id', 'name', 'keywords'])
                         ->where('brand_id', '=', $pid)->where('active', '=', 1)->orderBy('name')->get();
           $selecttaglist = tagGrouplist::select(['tag_id'])
                         ->where('tagGroup_id', '=', $id)->orderBy('tag_id')->get()->toArray();
              $accountPermission = '';
          
        return view('tagGroup.tagGroupEntry',compact('title','project_data','count','pid','companyData',          'tagGroup','taglist','selecttaglist','source','pid','accountPermission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       DB::beginTransaction();
    try{
      $res=tagsGroup::where('id',$id)->delete();
      $res=tagGrouplist::where('tagGroup_id',$id)->delete();
      DB::commit();
      return redirect('tagGrouplist')->with([
                            'flash_message' => 'Deleted',
                            'flash_message_important' => false
  ]);
    }
        catch(\Exception $e){
          //if there is an error/exception in the above code before commit, it'll rollback
          DB::rollBack();
          return $e->getMessage();
          }
        
    }

    public function gettagGrouplist()
    {
         $source='in';
         $brand_id = Input::get('brand_id');
         $username = auth()->user()->username;
         $login_user = auth()->user()->id;
         $permission_data = $this->getPermission();
         $tagGrouplist = tagsGroup::select(['id', 'name', 'created_at'])
                                   ->where('brand_id',$brand_id)
                                   ->orderBy('id');

        $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
        foreach($userdata as $action_permission) $action_permission = $action_permission->edit;
        
         return Datatables::of($tagGrouplist)
           ->addColumn('action', function ($tagGrouplist) use($source,$username,$action_permission) {
            $html='';
                    // if($permission_data['setting'] === 1)
                    // {
                    if($action_permission == 0){ 
                    $html ='<a href="" class="btn btn-xs btn-primary show_alert"  data-toggle="collapse"><i class="mdi mdi-table-edit"></i> Edit</a> 
                <button class="btn btn-xs btn-danger show_alert  data-toggle="collapse"><i class="mdi mdi-delete"></i>Delete</button>';
              }
              else

              {
                  $html ='<a href="tagsGroup/edit/'.$tagGrouplist->id.'?source='.$source.'" class="btn btn-xs btn-primary"><i class="mdi mdi-table-edit"></i> Edit</a> 
                <button class="btn btn-xs btn-danger btn-delete" data-remote="' . route('tagsGroup/delete', $tagGrouplist->id) . '"><i class="mdi mdi-delete"></i>Delete</button>';
              }
                    // }
                   return $html;

                })
           ->editColumn('created_at', function ($tagGrouplist) {
                     if (isset($tagGrouplist->created_at)) {
                    return   $tagGrouplist->created_at->format('d/m/Y');
                }
                })
           ->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
                })

          ->make(true);
         
    }
  public function gettagBytagGroup()
  {
       $tagGroup_id = Input::get('tagGroup_id');
       $brand_id = Input::get('brand_id');
       if($tagGroup_id <> '')
       $result = $this->gettagbytagGroupId($tagGroup_id);
       else
       $result = $this->gettagsBybrandid($brand_id);
       echo json_encode($result);
  }
}

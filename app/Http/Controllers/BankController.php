<?php

namespace BIMiner\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;

class BankController extends Controller
{
    use GlobalController;
    public function getExchangeRates(){

       if(null !== Input::get('fday'))
       {
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        $dateStart=date('Y-m-d', strtotime('-1 day', strtotime($dateBegin)));
       }
       else
       {
         $dateBegin=date('Y-m-d');
         $dateStart=date('Y-m-d', strtotime('-1 day', strtotime($dateBegin)));
       }

        if(null !==Input::get('sday'))
        {

         $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
         $dateStop=date('Y-m-d', strtotime('-1 day', strtotime($dateEnd)));
        }

       else
       {
          $dateEnd=date('Y-m-d');
          $dateStop=date('Y-m-d', strtotime('-1 day', strtotime($dateEnd)));

       }
        
        $query = 'Select * from exchange_rate where Date(updated_date) Between "'.$dateBegin.'" and "'.$dateEnd.'"';
        $data =  DB::select($query);

        if(count($data) == 0){
            // if today date not present , show yesterday date
        $query = 'Select * from exchange_rate where Date(updated_date) Between "'.$dateStart.'" and "'.$dateStop.'"';
        $data =  DB::select($query);
        }
        return Datatables::of($data)
        ->addColumn('image',function($data){
            $url = asset('Logo/bank_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
         ->addColumn('usd_margin',function($data){
            $buy  = (int) filter_var($data->usd_buy, FILTER_SANITIZE_NUMBER_INT);
            $sell = (int) filter_var($data->usd_sell, FILTER_SANITIZE_NUMBER_INT);
            $usd_margin = $sell - $buy ;
            return $usd_margin;
        })
          ->addColumn('euro_margin',function($data){
            $buy  = (int) filter_var($data->euro_buy, FILTER_SANITIZE_NUMBER_INT);
            $sell = (int) filter_var($data->euro_sell, FILTER_SANITIZE_NUMBER_INT);
            $euro_margin = $sell - $buy ;
            return $euro_margin;
        })
        ->addColumn('sgd_margin',function($data){
            $buy  = (int) filter_var($data->sgd_buy, FILTER_SANITIZE_NUMBER_INT);
            $sell = (int) filter_var($data->sgd_sell, FILTER_SANITIZE_NUMBER_INT);
            $sgd_margin = $sell - $buy ;
            return $sgd_margin;
        })    
         ->addColumn('updated_date',function($data){
             $s = $data->updated_date;


            $date = strtotime($s);
            return date('Y-m-d', $date);
            
        })           
        ->rawColumns(['image','usd_margin','euro_margin','sgd_margin','updated_date'])
        ->make(true);

    }

    public function getAllCreditCard(){
      $query = "select bank_name,logo,class,initial_fees,visa_card,mpu_card,interest_rate,platforms  from credit_card";
      $data = DB::select($query);

      return Datatables::of($data)
         ->addColumn('image',function($data){
            $url = asset('Logo/bank_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
   
        ->rawColumns(['image'])
        
        
        ->make(true);
    }

    public function getCallDeposit(){

        $data = DB::table('call_deposit')->get()->toArray();

        
        return Datatables::of($data)
         ->addColumn('image',function($data){
            $url = asset('Logo/bank_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
        ->addColumn('updated_date',function($data){
             $s = $data->updated_date;


            $date = strtotime($s);
            return date('Y-m-d', $date);
            
        }) 
        ->rawColumns(['image','updated_date'])
        
        
        ->make(true);

    }

    public function getfixedDeposit(){

        $data = DB::table('fixed_deposit')->get()->toArray();
        
        return Datatables::of($data)
         ->addColumn('image',function($data){
           $url = asset('Logo/bank_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
   
        ->rawColumns(['image'])
        
        
        ->make(true);

    }

    public function getRemit(){

        $data = DB::table('Remittance')->get()->toArray();
        
        return Datatables::of($data)
         ->addColumn('image',function($data){
           $url = asset('Logo/bank_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
        
   
        ->rawColumns(['image'])
        
        
        ->make(true);

    }

    public function get_acc2acc_transfer(){

        $data = DB::table('acc2acc_transfer')->get()->toArray();
        
        return Datatables::of($data)
         ->addColumn('image',function($data){
           $url = asset('Logo/mfs_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
        
         ->addColumn('updated_date',function($data){
             $s = $data->updated_date;


            $date = strtotime($s);
            return date('Y-m-d', $date);
            
        }) 
   
        ->rawColumns(['image','updated_date'])
        
        
        ->make(true);

    }
    
    public function get_acc2agent_transfer(){

        $data = DB::table('acc2agent_transfer')->get()->toArray();
        
        return Datatables::of($data)
         ->addColumn('image',function($data){
           $url = asset('Logo/mfs_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
         ->addColumn('updated_date',function($data){
             $s = $data->updated_date;
             $date = strtotime($s);
             return date('Y-m-d', $date);
            
        }) 
   
        ->rawColumns(['image','updated_date'])
        
        
        ->make(true);

    }

    public function get_agent2agent_transfer(){

        $data = DB::table('agent2agent_transfer')->get()->toArray();
        
        return Datatables::of($data)
         ->addColumn('image',function($data){
           $url = asset('Logo/mfs_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
         ->addColumn('updated_date',function($data){
             $s = $data->updated_date;


            $date = strtotime($s);
            return date('Y-m-d', $date);
            
        }) 
   
        ->rawColumns(['image','updated_date'])
        
        
        ->make(true);
    }
    
    public function get_withdraw_reg(){

        $data = DB::table('withdraw_reg')->get()->toArray();
        
        return Datatables::of($data)
         ->addColumn('image',function($data){
           $url = asset('Logo/mfs_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
         ->addColumn('updated_date',function($data){
             $s = $data->updated_date;


            $date = strtotime($s);
            return date('Y-m-d', $date);
            
        }) 
   
        ->rawColumns(['image','updated_date'])
        
        
        ->make(true);
    }

    public function get_withdraw_nonreg(){

        $data = DB::table('withdraw_nonreg')->get()->toArray();
        
        return Datatables::of($data)
         ->addColumn('image',function($data){
           $url = asset('Logo/mfs_logo/'.$data->logo);
            return '<img src="'.$url.'" style="width:60px;height:60px;" alt="'.$data->logo.'" title="'.$data->bank_name.'">';
        })
         ->addColumn('updated_date',function($data){
             $s = $data->updated_date;


            $date = strtotime($s);
            return date('Y-m-d', $date);
            
        }) 
   
        ->rawColumns(['image','updated_date'])
        
        
        ->make(true);

    }

    public function exchangeEntry(){

        if($user=Auth::user())
        {
          $title="ExchangeRateEntry";
          $companyData=[];
          $source = Input::get('source');
          $source ='in';

          // $dateToday=date('Y-m-d', strtotime(str_replace(' ','/',date('Y m d'))));
          // $dateYest=date('Y-m-d', strtotime('-1 day', strtotime($dateToday)));

          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          $accountPermission = '';
          // $exchangeData = DB::table('exchange_rate')->whereBetween('updated_date',[$dateToday,$dateToday])->distinct()->get(['bank_name','logo'])->toArray();

          // if(empty($exchangeData)){
          // $exchangeData = DB::table('exchange_rate')->whereBetween('updated_date',[$dateYest,$dateYest])->distinct()->get(['bank_name','logo'])->toArray();
          //  }
          // dd($exchange)

          return view('exchange_entry',compact('title','project_data','permission_data','count','source','companyData','accountPermission'));
        }
    }

    public function fixedDepositEntry(){
        if($user=Auth::user())
        {
          $title="fixedDepositEntry";
          $companyData=[];
          $source = Input::get('source');
          $source ='in';

          // $dateToday=date('Y-m-d', strtotime(str_replace(' ','/',date('Y m d'))));
          // $dateYest=date('Y-m-d', strtotime('-1 day', strtotime($dateToday)));

          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          $accountPermission = '';
          // $exchangeData = DB::table('exchange_rate')->whereBetween('updated_date',[$dateToday,$dateToday])->distinct()->get(['bank_name','logo'])->toArray();

          // if(empty($exchangeData)){
          // $exchangeData = DB::table('exchange_rate')->whereBetween('updated_date',[$dateYest,$dateYest])->distinct()->get(['bank_name','logo'])->toArray();
          //  }
          // dd($exchange)

          return view('fixedDeposit_entry',compact('title','project_data','permission_data','count','source','companyData','accountPermission'));
        }
    }

    public function bind_exchangeData(){
        
        
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        // dd($dateBegin,$dateEnd);
        // $dateStart=date('Y-m-d', strtotime('-1 day', strtotime($dateBegin)));
        // $dateStop=date('Y-m-d', strtotime('-1 day', strtotime($dateEnd)));

        // $exchangeData = DB::table('exchange_rate')->whereBetween('updated_date',[$dateBegin,$dateEnd])->distinct()->get(['bank_name','logo','usd_buy','usd_sell','euro_buy','euro_sell','sgd_buy','sgd_sell'])->toArray();

        $query = 'Select bank_name,logo,usd_buy,usd_sell,euro_buy,euro_sell,sgd_buy,sgd_sell from exchange_rate where Date(updated_date) Between "'.$dateBegin.'" and "'.$dateEnd.'"';
        $exchangeData =  DB::select($query);
 
        if(count($exchangeData) == 0){

          $exchangeData = DB::table('exchange_rate')->distinct()->get(['bank_name','logo'])->toArray();
           }

           return $exchangeData;
    }

    public function bind_fixDepoData(){


        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
        // dd($dateBegin,$dateEnd);
        // $dateStart=date('Y-m-d', strtotime('-1 day', strtotime($dateBegin)));
        // $dateStop=date('Y-m-d', strtotime('-1 day', strtotime($dateEnd)));

        // $exchangeData = DB::table('exchange_rate')->whereBetween('updated_date',[$dateBegin,$dateEnd])->distinct()->get(['bank_name','logo','usd_buy','usd_sell','euro_buy','euro_sell','sgd_buy','sgd_sell'])->toArray();


        // $query = 'Select * from exchange_rate where Date(updated_date) Between "'.$dateBegin.'" and "'.$dateEnd.'"';
        $query = 'Select * from fixed_deposit';
        $fixData =  DB::select($query);
 
        if(count($fixData) == 0){

          $fixData = DB::table('fixed_deposit')->distinct()->get(['bank_name','logo'])->toArray();
           }

        return $fixData;

    }

    public function exchange_store(){

     if(null !== Input::get('fday'))
     {
      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
     }
     else
     {
       $dateBegin=date('Y-m-d');
     }

      if(null !==Input::get('sday'))
      {

       $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }

     else
     {
        $dateEnd=date('Y-m-d');

     }

       $usd_buy = Input::get('usd_buy');
       $usd_sell = Input::get('usd_sell');
       $euro_buy = Input::get('euro_buy');
       $euro_sell = Input::get('euro_sell');
       $sgd_buy = Input::get('sgd_buy');
       $sgd_sell = Input::get('sgd_sell');

       $bank_name = Input::get('bank_name');
       $logo = Input::get('logo');
       
       $bank_name = explode(',', $bank_name[0]);
       $logo = explode(',', $logo[0]);
      
       $usd_buy = explode(',', $usd_buy[0]);
       $usd_sell = explode(',', $usd_sell[0]);
       $euro_buy = explode(',', $euro_buy[0]);
       $euro_sell = explode(',', $euro_sell[0]);
       $sgd_buy = explode(',', $sgd_buy[0]);
       $sgd_sell = explode(',', $sgd_sell[0]);
     
       $query = 'Select * from exchange_rate where Date(updated_date) Between "'.$dateBegin.'" and "'.$dateEnd.'"';
       $result = DB::select($query);
       // dd($usd_buy);
      if(count($result) == 0){
       // $updated_date = now()->toDateTimeString();
       for($i=0;$i<count($bank_name);$i++){

        $query = 'Insert into exchange_rate(bank_name,logo,usd_buy,usd_sell,euro_buy,euro_sell,sgd_buy,sgd_sell,hide,updated_date,updated_by)   VALUES ("'.$bank_name[$i].'","'.$logo[$i].'","'.$usd_buy[$i].'","'.$usd_sell[$i].'","'.$euro_buy[$i].'","'.$euro_sell[$i].'","'.$sgd_buy[$i].'","'.$sgd_sell[$i].'",NULL,"'.$dateBegin.'",NULL)';
       $row = DB::select($query);
       // dd($row);

       }
      }
      else
      {
       for($i=0;$i<count($bank_name);$i++){
        $query = 'update exchange_rate set usd_buy ="'.$usd_buy[$i].'",usd_sell ="'.$usd_sell[$i].'",euro_buy ="'.$euro_buy[$i].'",euro_sell ="'.$euro_sell[$i].'",sgd_buy ="'.$sgd_buy[$i].'",sgd_sell ="'.$sgd_sell[$i].'"
         where Date(updated_date) Between "'.$dateBegin.'" and "'.$dateEnd.'" and bank_name="'.$bank_name[$i].'"';
       $row = DB::select($query);
        
       }
        
      }
   
      return $result;
    }

    public function exchange_compare(){
      if($user=Auth::user()){
          $title="Exchange Rate Comparison";
          $companyData=[];
          $source = Input::get('source');
          $source ='in';
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          if(count($project_data)>0)
          {
          $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
          }
          $login_user = auth()->user()->id;
          $accountPermission = '';
          $UserPermission = DB::table('user_permission')->where('user_id',$login_user)->get();  
          foreach($UserPermission as $up) $isDemo = $up->demo;
          if($isDemo == 2) $accountPermission = 'Demo';
          
          $bank_names = DB::table('exchange_rate')->select('bank_name')->distinct()->get()->toArray();
          // dd($bank_names);
          return view('exchange_compare',compact('title','project_data','permission_data','count','source','companyData','accountPermission','bank_names'));
        }
    }

    public function getDailyExchangeRates(){

      $currency= Input::get('currency');
      $rate= Input::get('rate');
      $bank_arr = Input::get('page_name');
      if($rate == 'margin')
      $currencyRate = '('.$currency."_sell".')-('.$currency."_buy".')';
      else
      $currencyRate = $currency."_".$rate;
      if(null !== Input::get('fday')){
        $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else{
        $dateBegin=date('Y-m-d');
      }
      if(null !==Input::get('sday')){
        $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      else{
        $dateEnd=date('Y-m-d');
      }
  
     
      $dataExchange = [];$fan_arr=[];$data=[];$result=[];
      if($bank_arr != null){
         $bank_str = "'".implode("','", $bank_arr)."'";
        foreach($bank_arr as $bankName)
        {
          // $amount_arr=[];$date_arr=[];

         $query = "SELECT ".$currencyRate." as amount,bank_name,updated_date,logo FROM exchange_rate WHERE bank_name ='".$bankName."' AND Date(updated_date) BETWEEN '".$dateBegin."' AND '".$dateEnd."' GROUP BY bank_name,updated_date ORDER BY updated_date ASC";
         $result = DB::select($query);
          // dd($result);


        
        foreach ($result as $key => $value) {
$pointArr=[];$main_array=[];
         $pointArr[$key]['x'] =$value->updated_date; 
         $pointArr[$key]['y'] = $value->amount;
        }
         $main_array = [
            'dataPoints' => $pointArr
        ];

        // foreach ($date_arr as $key => $value) {
        //   $date [] = $value['updated_date'];
        // }

        // $date_str = implode(',',$date);
        // array_push($fan_arr, $amount_str);
        // array_push($fan_arr, $date_str);

          
        // $data[] = array('amount' => $amount_str, 'enddate' => $date_str);
       
       }


// {        
//         type: "line",
//         dataPoints: [
//         { x: 10, y: 21 },
//         { x: 20, y: 25 },
//         { x: 30, y: 20 },
//         { x: 40, y: 25 },
//         { x: 50, y: 27 },
//         { x: 60, y: 28 },
//         { x: 70, y: 28 },
//         { x: 80, y: 24 },
//         { x: 90, y: 26 }
      
//         ]
//       },

        // dd($data);
      }
      dd($main_array);
         echo json_encode($result);
  }
}


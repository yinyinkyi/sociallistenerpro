<?php

namespace BIMiner\Http\Controllers;
use BIMiner\ProjectKeyword;
use BIMiner\cron_log;
use BIMiner\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use BIMiner\CompanyInfo;
use BIMiner\User;
use BIMiner\OtherKeyword;



class OtherKeywordController extends Controller
{
    use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function create()
    {
        if($user=Auth::user())
        {
          $title="Setting";
          $companyData=[];
          $source = Input::get('source');
          $pid = Input::get('pid');
           $checked_data = $this->checkUserForProject($pid);
            
          if(count($checked_data)>0)
          {
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          $companyData=$this->getCompanyData($pid);
          $accountPermission = '';
          return view('otherkeyword-group-create',compact('title','project_data','permission_data','count','source','companyData','accountPermission'));
          }
             else
             {
              return abort(404);
             }
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if($user=Auth::user())
        {
          
          $pages='';
          $arr_pages=[];
          $my_pages='';
          $other_pages='';
          $main_key= Input::get('main_key');
          $include_key = Input::get('include_key');
          $exclude_key =Input::get('exclude_key');
          $source = Input::get('source');
          $group_name=Input::get('group_name');
          $project_id= Input::get('project_id');
        

            //Insert data to Project Keyword
             $count=count($main_key);



             for($i=0;$i<$count;$i++)
             {
              // dd($pages);
                OtherKeyword::create([
                'project_id' => $project_id,
                'company_id' => auth()->user()->company_id,
                'main_keyword'=>$main_key[$i],
                'require_keyword' => $include_key[$i],
                'exclude_keyword' => $exclude_key[$i],
                'group_name' => $group_name,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()

              ]);
             

              }
           
              cron_log::create([
                'brand_id' =>$project_id,
                'finish_status' => 0,
                'keyword_group' => $group_name,
                'created_at' => now()->toDateTimeString(),
                'cron_type' =>'sync_mention',
          ]);
              // $check_cron_log="SELECT id from cron_log  WHERE brand_id=".$project_id." AND keyword_group = '".$group_name."' AND cron_type='sync_mention' ";
              //  $check_cron_log = DB::select($check_cron_log);

              //    if( count($check_cron_log) > 0 )
              //   {
              //         DB::table('cron_log')
              //         ->where('brand_id', $project_id)
              //         ->where('keyword_group',$group_name)
              //        ->update(['finish_status' =>0,'going_on']);
              //   }
              //   else
              //   {

              //   }
          $accountPermission = '';

            return redirect()->route('other-keyword-group',['source'=>$source,'pid'=>$project_id,'accountPermission' =>$accountPermission]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \BIMiner\ProjectKeyword  $projectKeyword
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectKeyword $projectKeyword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \BIMiner\ProjectKeyword  $projectKeyword
     * @return \Illuminate\Http\Response
     */
    public function edit($group_name)
    {
         if($user=Auth::user())
        {
          //$source=Input::get('source');
           $cmp_id=Input::get('cmp_id');
           // dd($cmp_id);
           $pid = auth()->user()->brand_id;
            $checked_data = $this->checkUserForProject($pid);
        
          if(count($checked_data)>0)
          {
          $title="Setting";
          $companyData=[];
          $source = 'in';
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
         // $project = Project::find($pid);
          $permission_data = $this->getPermission(); 
         
          $companyData=$this->getCompanyData($pid);
       
            //dd($own_pages);
          $keywords = OtherKeyword::where('company_id',$cmp_id)->where('group_name',$group_name)->get();
          $accountPermission = '';
          // dd($cmp_id);
          return view('otherkeyword-group-edit',compact('title','project_data','count','permission_data','keywords','group_name','source','companyData','pid','accountPermission'));
          }
          else
          {
            return abort(404);
          }

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \BIMiner\ProjectKeyword  $projectKeyword
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
         $arr_pages=[];
          $my_pages='';
          $other_pages='';
          
          $project_id =auth()->user()->brand_id;
          $cmp_id =Input::get('cmp_id');
          $group_name = Input::get('group_name');
          $hidden_group_name = Input::get('hidden_group_name');
          $main_key= Input::get('main_key');
          $include_key =Input::get('include_key');
          $exclude_key =Input::get('exclude_key');
          $source = Input::get('source');
        
             
        $keyword = DB::table('other_keywords')->where('company_id',$cmp_id)->where('group_name',$hidden_group_name)->delete();
// dd($keyword);
  
          $count=count($main_key);
          for($i=0;$i<$count;$i++)
          {
// dd($project_id);
             OtherKeyword::create([
            'project_id' => $project_id,
            'company_id' => $cmp_id,
            'main_keyword'=>$main_key[$i],
            'require_keyword' => $include_key[$i],
            'exclude_keyword' => $exclude_key[$i],
            'group_name' => $group_name,
            'created_at' => now()->toDateTimeString(),
            'updated_at' => now()->toDateTimeString()

            ]);    

          }

          cron_log::create([
            'brand_id' =>$project_id,
            'finish_status' => 0,
            'keyword_group' =>  $group_name ,
            'created_at' => now()->toDateTimeString(),
            'cron_type' =>'sync_mention',
        ]);
// dd($project_id);

          $accountPermission = '';

          // return redirect()->route('other-keyword-group',['source'=>$source,'pid'=>$project_id,'accountPermission' => $accountPermission]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \BIMiner\ProjectKeyword  $projectKeyword
     * @return \Illuminate\Http\Response
     */
    public function keywordGroup_delete($group_name)
    {
    
        $pid = Input::get('pid');
        $cmp_id = Input::get('cmp_id');

        $keywords = DB::table('other_keywords')->where('group_name',$group_name)->where('company_id',$cmp_id)->delete();
      
        return redirect('other-keyword-group')->with([
      
        'flash_message' => 'Deleted',
        'flash_message_important' => false
  ]);
    }

    public function getkeywordgroup()
    {
     $pid='';
     if(!null==Input::get('pid'))
     $pid=Input::get('pid');
     $company_id = auth()->user()->company_id;
     $keywordgrouplist = ProjectKeyword::select(['group_name','id','project_id','created_at'])->where('project_id',$pid)->groupBy('group_name')->orderBy('id')->get()->toArray();
     // $keywordgrouplist = CompanyInfo::select(['keyword_groups','brand_id'])->where('id',$company_id)->get()->toArray();
     // dd($keywordgrouplist);
     // foreach($keywordgrouplist as $arr)
     // {
     //  $string = $arr['keyword_groups'];
     //  $kw_arr[] = explode(',', $string);
     //  array_push($kw_arr,$arr['brand_id']);
     // }
     $username = auth()->user()->username;
     $default_name = User::select(['default_keyword'])->where('username',$username)->get()->toArray();
       // $default_name = Project::select(['default_keyword'])->where('id',$pid)->get()->toArray();
       // dd($default_name);
     return [$keywordgrouplist,$default_name];
    }
    public function getallKeywords()
    {
       $pid='';
     if(!null==Input::get('pid'))
     $pid=Input::get('pid');
     
      $keywordgrouplist = ProjectKeyword::select(['group_name'])->where('project_id',$pid)->groupBy('group_name')->orderBy('group_name')->get()->toArray();
       $default_name = User::select(['default_keyword'])->where('id',auth()->user()->id)->get()->toArray();
       // dd($default_name);
       $keyword_arr=[];
      foreach ($keywordgrouplist as $key => $value) {
        $keyword_arr [] = $value['group_name'];
        # code...
      }
      $default_keyword[]= $default_name[0]['default_keyword'];
      
      if($default_keyword <> '')
      {
       $compare_keyword = array_diff($keyword_arr,$default_keyword);
       array_unshift($compare_keyword,$default_name[0]['default_keyword']);
      }
      else
      {
       $compare_keyword=explode(',',$keyword_arr);
      }
      
      echo json_encode($compare_keyword);

    }
    public function getkeywordgrouplist()
   {
    
     $pid='';
     $username = auth()->user()->username;
    
     if(!null==Input::get('pid'))
     $pid=Input::get('pid');

     $login_user = auth()->user()->id;
     $permission_data = $this->getPermission();
     $company_id = auth()->user()->company_id;
     $keywordgrouplist = OtherKeyword::select(['group_name','id','company_id','created_at'])->where('company_id',$company_id)->groupBy('group_name')->orderBy('id');

     // echo $source ;
     // return;
  
     $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
     foreach($userdata as $action_permission) $action_permission = $action_permission->edit;

     return Datatables::of($keywordgrouplist)
       ->addColumn('group_name', function ($keywordgrouplist) use($permission_data) {
      
                return $keywordgrouplist->group_name;

            })

       ->addColumn('action', function ($keywordgrouplist) use($permission_data,$pid,$username,$action_permission) {
        $html='';
                // if($permission_data['setting'] === 1)
                // {
         if($action_permission == 0 ){ 
                $html ='<a href="" class="btn btn-xs btn-primary show_alert" data-toggle="collapse"><i class="mdi mdi-table-edit"></i> Edit</a> 
            <button class="btn btn-xs btn-danger show_alert" data-toggle="collapse"><i class="mdi mdi-delete"></i>Delete</button>';
          }
          else{

                  $html ='<a href="otherkeyword-group/edit/'.$keywordgrouplist->group_name.'?pid='.$pid.'&cmp_id='.$keywordgrouplist->company_id.'" class="btn btn-xs btn-primary"><i class="mdi mdi-table-edit"></i> Edit</a> 
            <button class="btn btn-xs btn-danger btn-delete" data-remote="' . route('otherkeyword-group/delete',[$keywordgrouplist->group_name,'pid'=>$pid,'cmp_id' =>$keywordgrouplist->company_id] ) . '"><i class="mdi mdi-delete"></i>Delete</button>';
          }
                // }
               return $html;

            })
       ->editColumn('created_at', function ($keywordgrouplist) {
                 if (isset($keywordgrouplist->created_at)) {
                return   $keywordgrouplist->created_at->format('d/m/Y');
            }
            })
       ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })

      ->make(true);
           // ->rawColumns(['image', 'action'])


 }
 public function getotherkeywordgroup()
 {
     $pid='';
     if(!null==Input::get('pid'))
     $pid=Input::get('pid');
     $company_id = auth()->user()->company_id;
     $keywordgrouplist = OtherKeyword::select(['group_name','id','project_id','created_at'])->where('company_id',$company_id)->groupBy('group_name')->orderBy('id')->get()->toArray();
     // $keywordgrouplist = CompanyInfo::select(['keyword_groups','brand_id'])->where('id',$company_id)->get()->toArray();
     // dd($keywordgrouplist);
     // foreach($keywordgrouplist as $arr)
     // {
     //  $string = $arr['keyword_groups'];
     //  $kw_arr[] = explode(',', $string);
     //  array_push($kw_arr,$arr['brand_id']);
     // }
     $username = auth()->user()->username;
     $default_name = CompanyInfo::select(['default_keyword'])->where('id',$company_id)->get()->toArray();
       // $default_name = Project::select(['default_keyword'])->where('id',$pid)->get()->toArray();
       // dd($default_name);
     return [$keywordgrouplist,$default_name];
 }
}

<?php

namespace BIMiner\Http\Controllers\Auth;

use BIMiner\User;
use BIMiner\ManageDB;
use BIMiner\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use BIMiner\Http\Controllers\GlobalController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use GlobalController;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest');
    }

 

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \BIMiner\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'brand_id' => $data['brand_id'],
            'password' => Hash::make($data['password']),
        ]);
    }
       public function showRegistrationForm()
    {
          /*$this->guard()->login($user);*/

          $title="Register";
          $companyData=[];
          $source=Input::get('source');
          $pid=Input::get('pid');
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
           $count = $this->getProjectCount($project_data);
          // if(count($project_data)>0)
          // {
          // $pid = $project_data[0]['id'];
          $companyData=$this->getCompanyData($pid);
         // }
          $login_user = auth()->user()->id;
          $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
          foreach($userdata as $action_permission) $action_permission = $action_permission->edit;
           $accountPermission = '';
            return view('auth.register')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('permission_data',$permission_data)
                                    ->with('action_permission',$action_permission)
                                    ->with('source',$source)
                                    ->with('pid',$pid)
                                    ->with('companyData',$companyData)
                                    ->with('title',$title)
                                    ->with('accountPermission',$accountPermission);

                                
    }

      public function register(Request $request)
    {

        $this->validator($request->all())->validate();
        
        $user = $this->create($request->all());
        
        ManageDB::create(['user_id'=>$user->id,'brand_id'=>$request->brand_id,'db_name'=>'social_listener_wunzinn2home','created_at'=> now()->toDateTimeString(),'updated_at' => now()->toDateTimeString()]);

        /*$this->guard()->login($user);*/
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);

        $accountPermission = '';

         return \Redirect::route('register',['pid' =>$request->brand_id])->with('project_data',$project_data)
                                    ->with('accountPermission',$accountPermission)
                                    ->with('count',$count)
                                    ->with('message', 'Successfully created a new account.');
    }
    public function showOutRegistrationForm()
    {
        return view('auth.register_out');
    }
    public function register_out(Request $request)
    {
        $this->validator($request->all())->validate();

        $user=$this->create($request->all());
        auth()->login($user);
        return redirect('/');
    }
}

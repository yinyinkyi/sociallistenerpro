<?php

namespace BIMiner\Http\Controllers;

use Illuminate\Http\Request;
use BIMiner\User;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
     use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      protected function validator(array $data,$id)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users,username,'.$id,
            'email' => 'required|string|max:255|unique:users,email,'.$id,
           'password' => 'nullable|string|min:6|confirmed',
           
        ]);
    }
     protected function validatorWithoutPassword(array $data,$id)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users,username,'.$id,
            'email' => 'required|string|max:255|unique:users,email,'.$id,
           
           
        ]);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $title="Register";
          $companyData =[];
          $source=Input::get('source');
          $pid=Input::get('pid');
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
           $count = $this->getProjectCount($project_data);
          // if(count($project_data)>0)
          // {
          $companyData=$this->getCompanyData($pid);
          // }
          $userdata = User::find($id);
          $accountPermission = '';
       
        return view('auth.register',compact('userdata','title','project_data','count','source','permission_data','companyData','pid','accountPermission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         if(isset($request->password))
        {
        $this->validator($request->all(),$id)->validate();
        }
        else
        {
            $this->validatorWithoutPassword($request->all(),$id)->validate();
        }
       
        $input_p['name']=$request->name;
        $input_p['username']=$request->username;
        $input_p['email']=$request->email;
        //$input_p['brand_id']=$request->brand_id;
        if(isset($request->password))
        {
        $input_p['password']=Hash::make($request->password);
        }
    
        User::find($id)->update($input_p);
        $accountPermission = '';
      
        return \Redirect::route('register',['pid' =>$request->brand_id,'accountPermission'=>$accountPermission])->with('message','Record update successfully');
        
                        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $pid=Input::get('pid');
        User::find($id)->delete();
        $accountPermission = '';
        return redirect()->route('register',['pid' => $pid,'accountPermission' =>$accountPermission])
                        ->with('success','Record deleted successfully');
    }

    //customize function
     public function getUserData()
   {

     $source='';
     $username = auth()->user()->username;
     if(!null==Input::get('source'))
     $source=Input::get('source');      
     if(!null==Input::get('pid'))
      $pid=Input::get("pid");

     $login_user = auth()->user()->id;
     $userdata = DB::table('user_permission')->select('edit')->where('user_id',$login_user)->get();
     foreach($userdata as $action_permission) $action_permission = $action_permission->edit;


     //$userlist = User::select(['id', 'name', 'username', 'email'])->where('brand_id',$pid)->orderBy('id');
     $query="select users.id,name,username,email from users inner join manage_dbs on users.id=manage_dbs.user_id where (role is null or role ='') and manage_dbs.brand_id=".$pid;
     $userlist = DB::select($query);
     return Datatables::of($userlist)
       ->addColumn('action', function ($userlist) use($source,$pid,$username,$action_permission) {
              if($action_permission == 0){ 
                return '<a href="" class="btn btn-xs btn-primary show_alert" data-toggle="collapse"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
              <a href="" class="btn btn-xs btn-danger show_alert" data-toggle="collapse"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
          }
          else
          {
            return '<a href="'. route('users.edit', ['id'=>$userlist->id,'source'=>$source,'pid'=>$pid]) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
              <a href="'. route('deleteuser',['id'=>$userlist->id,'source'=>$source,'pid'=>$pid]) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
          }

            })

    /* ->addColumn('action', function ($booking) {
        return '<a href="'. route('deleteBooking',$booking->id) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

    })*/




     ->make(true);
           // ->rawColumns(['image', 'action'])

 }
}

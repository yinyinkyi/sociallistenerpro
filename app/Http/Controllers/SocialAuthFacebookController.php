<?php

namespace BIMiner\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Socialite;
use BIMiner\Services\SocialFacebookAccountService;

class SocialAuthFacebookController extends Controller
{
   /**
   * Create a redirect method to facebook api.
   *
   * @return void
   */
    public function redirect($provider)
    {
      
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(SocialFacebookAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
        auth()->login($user);
        return redirect()->to('/brandList');
    }
}

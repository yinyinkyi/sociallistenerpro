<?php

namespace BIMiner;

use Illuminate\Database\Eloquent\Model;

class Page_Info extends Model
{
    //
  protected $table = 'page_info';
  protected $dates = ['created_at','updated_at'];
  protected $fillable = ['page_name','full_page_name','page_id'];
}

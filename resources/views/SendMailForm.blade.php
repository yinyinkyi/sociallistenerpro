@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
 


              <header class="" id="myHeader">
                <div class="row page-titles" style="padding-bottom:15px;padding-top:15px">
                  <div class="col-md-5 col-8 align-self-center">
                      <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Mail Report</h4>
                  </div>
                </div>
              </header>


                <div class="row" style="margin-top: 10px;">
                    <div class="col-lg-6">
                        <div class="card card-outline-info">
                            <div class="card-body">
                              <div class='card-header' style="background: #fff;text-align: center;font-weight: 900;padding:0px;">Report Preview & Download</div><br>
                                <form role="form" class="form-horizontal" action="{{route('getMail')}}" method="get" id="myform">
                                    <div class="form-body">
                                     <!--    <h3 class="box-title">Mail Report</h3>
                                        <hr class="m-t-0 m-b-40"> -->
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Date Range:</label>
                                                    <div class="col-md-5">
                                                       <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                                         <input type='text' class="form-control dateranges" name="daterange" style="datepicker" />
                                                          <div class="input-group-append">
                                                              <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                              </span>
                                                          </div>
                                                       </div>
                                                    </div>
                                                     <div class="col-md-2">
                                                      <button type="submit" class="btn btn-success btn-rounded" style="float:right;">Preview</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                      </div>
                                </form>
                            </div>
                             <br>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card card-outline-info">
                           <div class='card-header' style="background: #fff;text-align: center;font-weight: 900;">Send Me a Mail !</div>
                            <div class="card-body" style="text-align: center;">
                                <form role="form" class="form-horizontal" action="{{route('SendMailReport')}}" method="get" id="mailform">
                                    <div class="form-body">
                                        <div class="form-group">
                                          <label class="col-lg-3">Who to Sent</label>
                                          <input type="text" name="email" id="email" class="form-control col-lg-6" placeholder="example.gmail.com">
                                          <br><span style="color:red" id="email-error">Please enter a valid email !</span>
                                          <br><br>
                                          <label style="font-weight: 400;color:#67757c">How would you like to receive your report?</label>
                                            <div class="input-group" style="margin-left:40%;">
                                                <ul class="icheck-list">
                                                  <li>
                                                    <input tabindex="7" type="radio" class="daily" id="minimal-radio-1" value="Daily" name="schedule" checked="checked">
                                                    <label for="minimal-radio-1">Daily</label>
                                                  </li>
                                                </ul>
                                                <ul class="icheck-list">
                                                    <li>
                                                        <input tabindex="7" type="radio" class="weekly" id="minimal-radio-2" value="Weekly" name="schedule">
                                                        <label for="minimal-radio-2">Weekly</label>
                                                    </li>
                                                </ul>
                                            </div>
                                			<div class="col-md-6" style="margin:auto;margin-left:270px;" id="hidden_field">
                                               <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                                 <input type='text' class="form-control dateranges" name="mail_daterange" style="datepicker" />
                                                  <div class="input-group-append">
                                                      <span class="input-group-text">
                                                        <span class="ti-calendar"></span>
                                                      </span>
                                                  </div>
                                               </div>
                                            </div>
                                            <br>
                                         <!--    <input type="submit" class="form-control btn btn-sm btn-success btn-flat" value="Sent" style="display: block;align-content: center;margin:auto;"> -->
                                         <button type="button" class="btn btn-success btn-rounded" id="btn_send">Sent</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                      
                    </div>
                </div>




                
              
                
       
@endsection
@push('scripts')

<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>

<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>

<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>

<script src="{{asset('js/waves.js')}}" defer></script>

<script src="{{asset('js/sidebarmenu.js')}}" defer></script>

<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>

<script src="{{asset('js/custom.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

<script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
<script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
<script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
<script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>
<script type="text/javascript">
      var startDate;var endDate;

      $(document).ready(function() {

          var GetURLParameter = function GetURLParameter(sParam) {
          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
              sURLVariables = sPageURL.split('&'),
              sParameterName,
              i;

          for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return sParameterName[1] === undefined ? true : sParameterName[1];
              }
          }
        };

        startDate = moment().subtract(1, 'month');
        endDate = moment();
        label = 'month';


      	var isUp = true;

	      $('.daily').on('click', function(){
             $('#hidden_field').hide();
             isUp=true;
	  	 });

	      $('.weekly').on('click', function(){
          	if(isUp){
	            $('#hidden_field').css({opacity:0}).slideDown("slow").animate({opacity:1});
	            isUp = false;
	      	}else{
		        $('#hidden_field').animate({opacity:0}).slideUp("slow");
		        isUp = true;
	      	}
	  	});
         
          $(document).on('click', '#btn_send', function () {

	      if ($('#email').val() === "") {

	      	$('#email-error').show();
	      }
	      else
	      {
	      	$('#mailform').submit();
	      }
	  })


         $('.singledate').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          locale: {
              format: 'DD/MM/YYYY'
          }
        },function(date) {
           
            endDate=date;
            var id =$('input[type=radio][name=period-radio]:checked').attr('id');
            if(id==="period-week")
           {
             startDate = moment(endDate).subtract(1, 'week');
             endDate = endDate;
           }
           else
           {
             startDate = moment(endDate).subtract(1, 'month');
             endDate = endDate;
           }
          
            // $(document).on('click', '#btn_send', function () {
           // ChooseDate(startDate,endDate);
           $('.fday').val()='';
           $('.fday').val()=startDate;
            $('.sday').val()='';
           $('.sday').val()=endDate;
         
        });


         function ChooseDate(start,end){

           $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate=start;
            endDate=end;
             
            sendMail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));

         }

         function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
          }

        //   $(document).on('change', '.check', function () {
        //     var schedule = $("input[name='schedule']:checked").val();
           
        //     alert(startDate.format('YYYY-MM-DD'));
        //      $.ajax({
        //      headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
        //      url:'{{ route("SendMailReport") }}',
        //      type: 'GET',
        //      data: {schedule:schedule},
        //      success: function(response) { //alert(response);
        //      //  if(response >= 0)
        //      //    swal({   
        //      //      title: "You will received Mail Report ",   
        //      //      text: schedule,   
        //      //      timer: 3000,   
        //      //      showConfirmButton: false 
        //      //    });
        //      }
        //    });
        // });

      var permission = GetURLParameter('accountPermission');
      $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
                startDate: startDate,
                endDate: endDate,
          },function(start, end,label) {
            if(permission == 'Demo'){
              var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');
             
              var fromDay = start.format('MMM DD,YYYY');
          
              // convert them as objects to compare
              var from = new Date(fromDay);
              var min = new Date(minDate); 
          
              // less than equal needs plus sign 
              // less than don't need 
              if(+from <= +min) 
            
              {
                   swal({
                  title: 'Sorry',
                  text: "You are allowed to access data for last three months only",
                  type: 'warning',
                  showCancelButton: false,
                  // confirmButtonColor: '',
                  // cancelButtonColor: '',
                  // confirmButtonText: '',
                  // buttonsStyling: false,
                })
                  startDate = moment().subtract(1, 'month');
                  endDate = moment();
       
              }


              else{
                var startDate;
                var endDate;
                label = label;
                startDate = start;
                endDate = end;

                          $('.fday').empty();
             $('.fday').val =startDate;
              $('.sday').empty();
             $('.sday').val =endDate;
                
              // ChooseDate(startDate,endDate);
             
               }
            }
           else
           {
              var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;
              // ChooseDate(startDate,endDate);

             $('.fday').empty();
             $('.fday').val =startDate;
             $('.sday').empty();
             $('.sday').val=endDate;
            
           }
        }
      )

    });
  </script>
  <style type="text/css">
  ::-webkit-input-placeholder {
    opacity: 0.2;
  }
	#hidden_field{
  		display: none;
	}
	#email-error{
		display:none;
	}
  </style>
   @endpush


<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon icon -->
        
        <!-- Bootstrap Core CSS -->
        <link href="https://bsldev.baganintel.com/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://bsldev.baganintel.com/assets/plugins/icheck/skins/all.css" rel="stylesheet">
        <link href="https://bsldev.baganintel.com/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
         <!-- range slider -->
         <link href="https://bsldev.baganintel.com/assets/plugins/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet">
        <link href="https://bsldev.baganintel.com/assets/plugins/ion-rangeslider/css/ion.rangeSlider.skinModern.css" rel="stylesheet">
        <!-- Page plugins css -->
        <link href="https://bsldev.baganintel.com/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
        <!-- Color picker plugins css -->
        
        <!-- Date picker plugins css -->
        <link href="https://bsldev.baganintel.com/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker plugins css -->
        <link href="https://bsldev.baganintel.com/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="https://bsldev.baganintel.com/assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet">
        <!-- chartist CSS -->
        <link href="https://bsldev.baganintel.com/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
        <link href="https://bsldev.baganintel.com/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
        <link href="https://bsldev.baganintel.com/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
        <link href="https://bsldev.baganintel.com/assets/plugins/css-chart/css-chart.css" rel="stylesheet">
        <!--This page css - Morris CSS -->
        <link href="https://bsldev.baganintel.com/assets/plugins/morrisjs/morris.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="https://bsldev.baganintel.com/css/style.css" rel="stylesheet">
       
        <!-- You can change the theme colors from here -->
        <link href="https://bsldev.baganintel.com/css/colors/green.css" id="theme" rel="stylesheet">

        <link href="https://bsldev.baganintel.com/assets/plugins/sweetalert2/sweetalert2.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=myanmar3' />
        <link href="https://bsldev.baganintel.com/assets/plugins/select2/dist/css/select2.min.css" id="theme" rel="stylesheet">
          <link href="https://bsldev.baganintel.com/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
         <link href="https://bsldev.baganintel.com/assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
         <!-- <link href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.dataTables.min.css" rel="stylesheet"/> -->
         <link href="https://bsldev.baganintel.com/css/fixedHeader.dataTables.min.css" rel="stylesheet"/>
         <link href="https://bsldev.baganintel.com/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
         <link href="https://bsldev.baganintel.com/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
         <link href="https://bsldev.baganintel.com/assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />

         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    </head>
    <body>
      <center>
      <div class="card" style="width:600px;margin:auto;margin-top:10px;border-radius: 10px;background-color: #fafafa">
      
        <div class="card-body" style="padding:0.7rem;">
      <table  align="center" style="margin-top:10 ;max-width: 100%;width: 90% !important;" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#fafafa">
     <tr><th><div style="font-size:140%;font-weight: bolder"> <b>{{ $reportType }}</b></div>
      <!-- <font color="red"> Testing</font>  -->
      @if($today_date == '')
      <span style="font-size:80%;" >{{ $from_date }}&nbsp; To &nbsp;{{ $to_date }}</span></th><th>
        @else
                  <span style="font-size:80%;" >{{$today_date}}</span></th><th>
                   
                  
@endif
          <img src="{{ $imgurl }}" style="width:10vmin;height:10vmin;border: none;"></th>
                  </tr>
  <tr>
    <td align="" valign="top">
    
      <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
        <tr>
          <th class="card-title" style="font-weight: bold;padding-bottom:1vmin;padding-top:3vmin;font-size:90%">Page Summary</th>
            <tr>
          <td align="" valign="top">
           <div class="btn-group btn-group-lg " role="group" aria-label="Basic example" style="margin-bottom: 2%;">
                    <div class="btn-group" role="group"><button type="button"  style="font-size: 80%;width:6.2rem;text-align: left" class="btn btn-secondary">Page Growth</button></div>
                    <div class="btn-group" role="group">
                    <button type="button"  style="font-size: 80%;padding:;width:5.6rem" class="btn btn-secondary" id="pageGrowth">{{ $page_growth }}</button></div>
                    
                    </div><br>
                      <div class="btn-group btn-group-lg" role="group" aria-label="Basic example" style="margin-bottom: 2%;">
                        <div class="btn-group" role="group"><button type="button"  style="font-size: 80%;padding:;width:6.2rem" class="btn btn-secondary">Post</button></div>
                        <div class="btn-group" role="group">
                        <button type="button"  style="font-size: 80%;padding:;width:5.6rem" class="btn btn-secondary" id="post">{{ $post_count }}</button></div>
                        
                      </div><br>
                      <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                          <div class="btn-group" role="group"><button type="button"  style="font-size: 80%;padding:;width:6.2rem" class="btn btn-secondary">Comment</button></div>
                          <div class="btn-group" role="group">
                          <button type="button"  style="font-size: 80%;padding:;width:5.6rem" class="btn btn-secondary" id="comment">{{ $comment_count }}</button></div>
                          
                      </div>
          </td>
          
        </tr>
      </table>

    </td>
        <td align="" valign="top">

      <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
        <tr>
          <th class="card-title" style="font-weight: bold;padding-bottom:1vmin;padding-top:3vmin;font-size:90%">Mentions</th>
            <tr>
          <td align="" valign="top">
         <span style="font-size:85%">You were mentioned <b><span style="font-weight: bolder;" id="mentionTotal">  {{ $mentionTotal }} </span></b> times last week!</span>
                    <br> <br>
                    <b> {{ $articleTotal }}  </b> <span style="font-size:85%" id="articleTotal">  Articles </span><br>
                    <b> {{ $postTotal }}  </b> <span style="font-size:85%" id="postTotal" > Posts </span><br>
                    <b>  {{ $cmtTotal }} </b>  <span style="font-size:85%" id="cmtTotal"> Comments </span> 
          </td>
          
        </tr>
      </table>

    </td>
  </tr>




    <tr>
    <td align="" valign="top">

      <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
        <tr>
          <th class="card-title" style="font-weight: bold;padding-bottom:1vmin;padding-top:3vmin;font-size:90%">Added Post Topics</th>
            <tr>
          <td align="" valign="top">
              <div class="post_tag_count"> 
                @if($pTagName1 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName1 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $pTagCount1 }}</span></button>
                      @else
                      <span>You have no new <span style="font-weight: bolder;">Post Topics</span> last week!</span>
                      @endif
                      @if($pTagName2 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName2 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $pTagCount2 }}</span></button>
                      @endif
                      @if($pTagName3 != '')
                       <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName3 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $pTagCount3 }}</span></button>
                      @endif
                      @if($pTagName4 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName4 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $pTagCount4 }}</span></button>
                      @endif
                      @if($pTagName5 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName5 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $pTagCount5 }}</span></button>
                      @endif
                    </div>
          </td>
          
        </tr>
      </table>

    </td>
        <td align="" valign="top">

      <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
        <tr>
          <th class="card-title" style="font-weight: bold;padding-bottom:1vmin;padding-top:3vmin;font-size:90%">Added Comment Topics</th>
            <tr>
          <td align="" valign="top">
              <div class="tag_count"> 
                     @if($cTagName1 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName1 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $cTagCount1 }}</span></button>
                      @else
                      <span>You have no new <span style="font-weight: bolder;">Comment Topics</span> last week!</span>
                      @endif
                      @if($cTagName2 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName2 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $cTagCount2 }}</span></button>
                      @endif
                      @if($cTagName3 != '')
                       <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName3 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $cTagCount3 }}</span></button>
                      @endif
                      @if($cTagName4 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName4 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $cTagCount4 }}</span></button>
                      @endif
                      @if($cTagName5 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName5 }}<span class="label label-light-info" style="padding:1.5vmin;margin-left:1vmin;line-height: 0vmin;border-radius: 10vmin;font-size: 80%">{{ $cTagCount5 }}</span></button>
                      @endif
                  </div>
          </td>
          
        </tr>
      </table>

    </td>
  </tr>




  <tr>
    <td align="" valign="top" width="50%">

      <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
        <tr>
          <th class="card-title"  style="font-weight: bolder;padding-bottom:1vmin;padding-top:3vmin;font-size:90%"><b>Brand Sentiment</b></th></tr>
            <tr>
          <td align="" valign="top">
            
                   
                      <h4 class="card-title m-b-0" style="font-size:90%;font-weight: bold">Posts</h4>
                  </td>
                      
                  </tr>
                  <tr style="text-align: left;">
                       <td>
                            <h1 class="card-title m-t-10" style="font-size:130%;font-weight: bolder" id="post_total">{{ $post_count }}</h1>
                        
                        </td></tr>
                        <tr>
                        <td>            
                        <ul class="list-inline m-b-0" style="margin-left: -11px;">
                            <li>
                                <h6 id="post_neg" class="text-red text-center" style="font-size:83%">{{ $post_neg_pcent }}%</h6>
                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnPost" style="margin:-6px" value="neg">Negative</button></div> </li>
                           <li>
                                 <h6 id="post_neutral" class="text-warning text-center" style="font-size:83%">{{ $post_neutral_pcent }}%</h6>
                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnPost"  value="neutral">Neutral</button></div> </li>
                            <li>
                                 <h6 id="post_pos" class="text-success text-center" style="font-size:83%">{{ $post_pos_pcent }}%</h6>
                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnPost"  value="pos">Positive</button></div> </li>
                        </ul>
                         </td></tr>         

                 
         
      
      </table>

    </td>
        <td align="" valign="top" width="50%">

      <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
       <tr>
          <th class="card-title"  style="font-weight: bold;padding-bottom:1vmin;padding-top:3vmin;font-size:100%">&nbsp;</th></tr>
            <tr>
          <td align="" valign="top">
         
                   
                      <h4 class="card-title m-b-0" style="font-size:90%;font-weight: bold">Comments</h4>
                  </td>
                      
                  </tr>
                  <tr style="text-align: left;">
                       <td>
                            <h1 class="card-title m-t-10" style="font-size:130%;font-weight: bolder" id="post_total">{{ $comment_count }}</h1>
                        
                        </td></tr>
                        <tr>
                        <td>            
                        <ul class="list-inline m-b-0" style="margin-left: -11px;">
                            <li>
                                <h6 id="post_neg" class="text-red text-center" style="font-size:83%">{{ $cmt_neg_pcent }}%</h6>
                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnPost" style="margin:-6px" value="neg">Negative</button></div> </li>
                           <li>
                                 <h6 id="post_neutral" class="text-warning text-center" style="font-size:83%">{{ $cmt_neutral_pcent }}%</h6>
                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnPost"  value="neutral">Neutral</button></div> </li>
                            <li>
                                 <h6 id="post_pos" class="text-success text-center" style="font-size:83%">{{ $cmt_pos_pcent }}%</h6>
                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnPost"  value="pos">Positive</button></div> </li>
                        </ul>
                         </td></tr>         

                 
         
      </table>

    </td>
  </tr>




<!--   <tr>
    <td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
  </tr> -->
</table> 


         <div class="form-group" style="margin-top: 8vmin">
              <a href='https://bsldev.baganintel.com' target="_blank"><button style="display: block;background: #1e88e5;color:#fff;font-size: 80%;padding-top: 1vmin" class="form-control btn-success">Compare with competitors</button></a>
              
             </div>
             <div style="text-align:center;font-size:60%;">
             Stay ahead of the competition with BI Miner<br>
             <span ><a href="https://bsldev.baganintel.com" target="_blank">News</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://bsldev.baganintel.com" target="_blank">Help Center</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://bsldev.baganintel.com" target="_blank">Unsubscribe</a></span><br>
             <a href="https://bsldev.baganintel.com" target="_blank"><img src="https://bsldev.baganintel.com/assets/images/fb.png" style="width:3vmin;height: 3vmin;"></a><a href="https://bsldev.baganintel.com" target="_blank"><img src="https://bsldev.baganintel.com/assets/images/linkedin.png" style="width:4.3vmin;height: 4.3vmin;"></a>
         </div>


</div></td>
</center>
    </body>
    </html>

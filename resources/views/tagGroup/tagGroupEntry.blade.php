@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
 
    <header class="" id="myHeader">
                  <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Tag Group</h4>
                    
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                               
                                 <!--  <button type="button" id="btn_add_project" class="btn waves-effect waves-light btn-block btn-green">More Brands</button> -->
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <a id="btn_save" class="btn btn-green" href="#">Save Data</a> 
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
  </header>

              
                <!-- Row -->
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <input type="hidden" class="form-control form-control-line" name="brand_id" id="brand_id"  value="{{$pid}}"placeholder="Enter group name">
       @if(!isset($tagGroup))
          <form role="form" class="form-material" action="#" method="post" id="myform">
                                            {{csrf_field()}}
                 <div class="box-body">
                   <div class="form-group">
                     <label class="control-label">Tag Group Name: </label>
                      <input type="text" class="form-control form-control-line" name="name" id="name"  placeholder="Enter group name">                          
                      
                        <span class="invalid-feedback name_alert_noti" role="alert">
                            
                        </span>
                      
                    </div> 
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" align="center">
                               <div class="row">
                                  <div class="col-lg-12 col-xlg-4"  >
                                           <span class="invalid-feedback list_alert_noti" role="alert">
                                              
                                           </span>
                                        <h5 class="box-title">Add Tag Group List</h5>
                                       
                                        <select multiple id="public-methods" name="public-methods[]" size="30" style="height: 100%;">
                                          @if (isset($taglist))
                                          @foreach($taglist as $index =>$taglist)
                                          <option value="{{ $taglist->id }}">{{ $taglist->name }}</option>
                                          @endforeach
                                          @endif
                                            
                                        </select>
                                      
                                      <div class="button-box m-t-20"> 
                                        <a id="select-all" class="btn btn-danger" href="#">select all</a>
                                        <a id="deselect-all" class="btn btn-info" href="#">deselect all</a> 
                                      </div>
                                    </div>
                               </div>  
                            </div>
                        </div>
                    </div>
                    
                    
                                           

                                                  
                </div> <!-- endof BoxBody -->
          </form> 
         
         @else
          <form role="form" class="form-material" action="#" method="post" id="myform">
                                            {{csrf_field()}}
            <div class="box-body">
              <div class="form-group">
                     <label class="control-label">Tag Group Name: </label>
                      @foreach($tagGroup as $index =>$tagGroup)
                      <input type="hidden" class="form-control form-control-line" name="tagGroupid" id="tagGroupid"  value="{{$tagGroup->id}}"placeholder="Enter group name">
                      <input type="text" class="form-control form-control-line" name="name" id="name"  value="{{ old( 'name', $tagGroup->name) }}"placeholder="Enter group name">
                      @endforeach
                      
                        <span class="invalid-feedback name_alert_noti" role="alert">
                            
                        </span>
                      
                    </div> 
                    <div class="col-md-12">
                        <div class="card">
                          <div class="card-body" align="center">
                            <div class="row">
                              <div class="col-lg-12 col-xlg-4"  >
                              <span class="invalid-feedback list_alert_noti" role="alert">
                                            
                              </span>
                              <h5 class="box-title">Add Tag Group List</h5>
                              <select multiple id="public-methods" name="public-methods[]" size="30" style="height: 100%;">
                             @if (isset($taglist))
                             @foreach($taglist as $index =>$taglist)
                             @if (array_search($taglist->id,array_column($selecttaglist,'tag_id'))!==false)
                                    <option value="{{ $taglist->id }}" selected>{{ $taglist->name }}</option>
                             @else
                                    <option value="{{ $taglist->id }}">{{ $taglist->name }}</option>
                             @endif
                             @endforeach
                             @endif
                                             
                              </select>
                                    <div class="button-box m-t-20"> 
                                        <a id="select-all" class="btn btn-danger" href="#">select all</a>
                                        <a id="deselect-all" class="btn btn-info" href="#">deselect all</a> 
                                    </div>       
                                  </div>
                               </div>    
                            </div>
                        </div>
                    </div>
                    
                  

                                           

                                                  
                </div> <!-- endof BoxBody -->     
          </form> 

         @endif
      </div>
            
    </div>
 </div>
</div>             

              
                
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js')}}" type="text/javascript"></script>
     <script type="text/javascript" src="{{asset('assets/plugins/multiselect/js/jquery.multi-select.js')}}"></script>
       <script type="text/javascript" src="{{asset('assets/plugins/multiselect/js/jquery.quicksearch.js')}}"></script>

          <script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
     <script type="text/javascript">
     $(document).ready(function() {
      
      var selectedTag=[];
          var GetURLParameter = function GetURLParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
              }
            }
          };


        var APP_URL = {!! json_encode(url('/')) !!};
        $('#btn_save').click(function() {
          $tagGroupid = '';
          selectedTag =[];
          $('#public-methods :selected').each(function(i, sel){ 
                    selectedTag.push($(sel).val());
              });
              if(selectedTag.length <=0 )
          {
             $('.list_alert_noti').empty();
             $('.list_alert_noti').show();
             $('.list_alert_noti').append('<strong>Please choose Tags!!</strong>');
             
            return false;
          }
          // console.log(selectedTag);
          // return;
          if (document.getElementById('tagGroupid') instanceof Object){
                $tagGroupid = $('#tagGroupid').val();
                
           }
          $('.list_alert_noti').hide();
               swal({
                title: 'Comfirmation',
                text: "Are you sure?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, save it!'
              }).then((result) => {
                if (result.value) {
                  swal({title:'Saving Data....',  allowOutsideClick: false});
                  swal.showLoading();
                  $.ajax({
                    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    url:'{{ route("tagstore") }}',
                    type: 'POST',
                    data: {selectedTag:selectedTag,
                           name:$('#name').val(),
                           brand_id:$('#brand_id').val(),
                           id:$tagGroupid},
                      success: function(response) { 
                        if(typeof  response.errors === 'undefined')
                        {
                          window.location = APP_URL+'/tagsGroup?pid='+$('#brand_id').val()+'&source=in';
                          
                        }
                        else
                        {
                          $('.name_alert_noti').show();
                          $('.name_alert_noti').append('<strong>'+response.errors[0]+'</strong>');
                        }
                      
                      }
                          });
                
                }
              })
         

        });
       
       
        // For multiselect
        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });
        

        $('#select-all').click(function() {
          
             var publicmethods = document.getElementsByName('public-methods[]');
            
         $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            selectedTag =[];
            return false;
        });
                $('#public-methods').multiSelect({
                   selectableHeader: "<input style='width:100%' type='text' class='search-input' autocomplete='off' placeholder='Search ...'>",
                  selectionHeader: "<input style='width:100%' type='text' class='search-input' autocomplete='off' placeholder='Search ...'>",
                  afterInit: function(ms){
                        var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
                        
                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                      if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                      }
                    });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                      if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                      }
                    });
                  },
                  afterSelect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                  },
                  afterDeselect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                  }
                 
        });


     
      });
  </script>
  <style>
  .myHeaderContent
{
   margin-right:300px;
}
.ms-list
{
  height: 300px !important;
}
.ms-container {
  text-align: left;
  width:500px;
  }
.box-title
{
  color:#4267b2;
  font-weight: 500;
}
</style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


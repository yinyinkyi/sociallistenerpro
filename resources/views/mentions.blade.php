  @extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('admin_page_filter')
<li class="nav-item" style="display: none"> 
  <div class="btn-group">
      <select id="admin_page_filter" class="form-control custom-select">
        <option value="">Admin Pages</option>
        @if (isset($ownpage))
        @foreach($ownpage as $ownpage)
        <option value="{{$ownpage}}" id="{{$ownpage}}" >{{$ownpage}}</option>
        @endforeach
        @endif                               
    </select>                                   
  </div>
</li>
                      
@endsection
@section('content')
@section('filter')
  <li  class="nav-item dropdown mega-dropdown" > <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark dropdown-pages" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search text-warning"></i> <span class="text-warning" id="select_group"></span></a>
    <div id="search_form" class="dropdown-menu scale-up-left" id="dlDropDown"  style="padding-left: 5%">
      <ul class="mega-dropdown-menu row">
        <li class="col-lg-12" >
         <form>
            <input type="hidden" value='' id ="hidden-chip" name="hidden-chip"/>
            <input type="hidden" value="{{ auth()->user()->default_keyword }}" class ="hidden-kw" name="hidden-kw"/>
             <input type="hidden" value="{{ auth()->user()->role }}" class ="hidden-role" name="hidden-kw"/>
              <div id="chip-competitor" class="col-md-12 align-self-center"  style="text-align:center;min-height:70px;padding-left:20px;padding-top:10px;border: 1px solid #f9f9f8;">
              </div>
         </form>
        </li>
      </ul>
    </div>
  </li>
  @endsection
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
  <header class="" id="myHeader">
    <div class="row page-titles">
      <div class="col-md-7 col-8 align-self-center">
        <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
          <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#dashboard" id="dashboard_tab" role="tab">Summary</a> </li>
          <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#article" id="article_tab" role="tab"><span id="title_article">Articles</span></a> </li>
          <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#mention" id="post_tab" role="tab"><span id="title_post"> Posts</span></a> </li>
          <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#comment" id="comment_tab" role="tab"><span id="title_comment"> Comments</span></a> </li>
        </ul>
        <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
      </div>
      <div class="col-md-5 col-4 align-self-center">
        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
          <div class="d-flex m-r-20 m-l-10 hidden-md-down"></div>
              <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                 <input type='text' class="form-control dateranges" style="datepicker" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <span class="ti-calendar"></span>
                      </span>
                   </div>
              </div>
        </div>
      </div>
    </div>
  </header>

 <!-- <div class="row" id="top-div">
    <div class="col-lg-12">
 <div class="card-group">
                    <div class="card">
                        <div class="card-body TopDivCardBody">
                            <div class="row">
                                <div class="col-12 TopDivPadding" >
                                    <h5><span style="font-weight: 500"> Mention Pages</span></h5>
                                    <h2><span class="text-info TopDivNumber" id="top_mention_total">0</span></h2></div>
                               
                            </div>
                        </div>
                    </div>
        
                    <div class="card">
                        <div class="card-body TopDivCardBody">
                            <div class="row">
                                <div class="col-12 TopDivPadding">
                                    <h5><span style="font-weight: 500">Mention Posts</span></h5>
                                    <h2><span class="TopDivNumber" id="top_post_total">0</span></h2></div>
                             
                            </div>
                        </div>
                    </div>
                   
                    <div class="card">
                        <div class="card-body TopDivCardBody">
                            <div class="row">
                                <div class="col-12 TopDivPadding">
                                    <h5><span style="font-weight: 500">Mention Comments</span></h5>
                                    <h2><span class="TopDivNumber" id="top_comment_total">0</span></h2></div>
                             
                            </div>
                        </div>
                    </div>
          
                    
                </div>  
                </div> 
                </div> -->      
                <div class="row" id="top-div">
                    <!-- Column -->
                    <div class="" style="width:20%;padding-right: 10px;" >
                        <div class="card card-inverse card-info">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white" id='top_mention_total'>0</h1>
                                <h6 class="text-white"> Pages</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="" style="width:20%;padding-right: 10px;">
                        <div class="card card-primary card-inverse">
                            <div class="box text-center">
                               <a href="javascript:void(0)" class="link_mention_post"><h1 class="font-light text-white" id="top_post_total">0</h1></a>
                               <a href="javascript:void(0)" class="link_mention_post"><h6 class="text-white"> Posts</h6></a>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="" style="width:20%;padding-right: 10px;">
                        <div class="card card-inverse card-success">
                            <div class="box text-center">
                               <a href="javascript:void(0)" class="link_mention_cmt"><h1 class="font-light text-white" id="top_comment_total">0</h1></a>
                                <a href="javascript:void(0)" class="link_mention_cmt"><h6 class="text-white"> Comments</h6></a>
                            </div>
                        </div>
                    </div>
                    <div class="" style="width:20%;padding-right: 10px;">
                        <div class="card card-inverse" style="background:#73b36e">
                            <div class="box text-center">
                                <h1 class="font-light text-white" id='mention_author_total'>0</h1>
                                <h6 class="text-white">Websites</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="" style="width:20%">
                        <div class="card card-inverse" style="background:#efc324">
                            <div class="box text-center">
                               <a href="javascript:void(0)" class="link_article"><h1 class="font-light text-white" id="mention_article_total">0</h1></a>
                               <a href="javascript:void(0)" class="link_article"><h6 class="text-white">Articles</h6></a>
                            </div>
                        </div>
                    </div>
                </div>        
                
        <div class="row" >
          <div class="col-lg-12" id="outer_div">
              <div class="tab-content">
                  <div class="tab-pane active" id="dashboard" role="tabpanel">
                      <div class="row" style="margin-right:-10px;margin-left:-10px">
                          <div class="col-lg-8">
                              <div class="card" style="height:450px">
                                <div class="card-header">
                                  <div class="card-actions">
                                    <div class="btn-group1" data-toggle="buttons">
                                      <label class="btn btn-secondary active">
                                          <input type="radio" name="options" id="option3" value="mention" autocomplete="off"> Mention
                                      </label>
                                      <label class="btn btn-secondary">
                                          <input type="radio" name="options" id="option2" value="sentiment" autocomplete="off"> Sentiment
                                      </label>
                                    </div>
                                  </div>
                                  <h4 class="card-title m-b-0"></h4>
                               </div>
                               <div class="card-body b-t collapse show">
                                   <div style="display:none"  align="center" style="vertical-align: top;" id="fan-growth-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                  <div id="fan-growth-chart" style="width:100%;height:350px"></div>
                               </div>
                             </div>
                          </div>
                          <div class="col-lg-4">
                            <div class="card earning-widget">
                              <div class="card-header">
                                  <div class="card-actions">
                                    <div class="btn-group" data-toggle="buttons">
                                      <label class="btn btn-secondary active">
                                          <input type="radio" name="options" id="option3" value="pages" autocomplete="off"> Pages
                                      </label>
                                      <label class="btn btn-secondary">
                                          <input type="radio" name="options" id="option2" value="webistes" autocomplete="off"> Websites
                                      </label>
                                    </div>
                                  </div>
                                  <h4 class="card-title m-b-0"></h4>
                              </div>
                              <div class="card-body b-t collapse show" style="padding-right: 2rem !important;padding-top: 1rem !important;">
                                <div style="display:none"  align="center" style="vertical-align: top;" id="page_mention_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                 <div class="page_mention_count"> </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="row" style="margin-right:-10px;margin-left:-10px">
                        <div class="col-lg-3">
                          <div class="card">
                              <div class="card-header">
                                <h4 class="card-title m-b-0">Total Mentions</h4>
                             </div>
                             <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                                <div style="display:none"  align="center" id="mention_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader">
                                </div>
                                <div class="card-body text-center " style="padding:0.5rem !important">
                                  <h1 class="card-title m-t-10" style="font-size:65px" id="mention_total">-</h1>
                                </div>
                                <div class="card-body text-center " style="padding:0rem !important">
                                  <ul class="list-inline m-b-0">
                                    <li>
                                      <h6 id="mention_neg"class="text-red" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-red btnMention" value="neg">Negative</button></div> </li>
                                    <li>
                                      <h6 id="mention_neutral" class="text-warning" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-warning btnMention" value="neutral">Neutral</button></div> </li>
                                    <li>
                                      <h6 id="mention_pos" class="text-success" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-success btnMention" value="pos">Positive</button></div> </li>
                                  </ul>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="card">
                              <div class="card-header">
                                <h4 class="card-title m-b-0">Article Mentions</h4>
                             </div>
                             <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                                <div style="display:none"  align="center" id="article_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader">
                                </div>
                                <div class="card-body text-center " style="padding:0.5rem !important">
                                  <h1 class="card-title m-t-10" style="font-size:65px" id="article_total">-</h1>
                                </div>
                                <div class="card-body text-center " style="padding:0rem !important">
                                  <ul class="list-inline m-b-0">
                                    <li>
                                      <h6 id="article_neg"class="text-red" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-red btnMention" value="neg">Negative</button></div> </li>
                                    <li>
                                      <h6 id="article_neutral" class="text-warning" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-warning btnMention" value="neutral">Neutral</button></div> </li>
                                    <li>
                                      <h6 id="article_pos" class="text-success" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-success btnMention" value="pos">Positive</button></div> </li>
                                  </ul>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="card">
                            <div class="card-header">
                                <h4 class="card-title m-b-0">Post Mentions</h4>
                            </div>
                            <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                              <div style="display:none"  align="center" id="post_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <div class="card-body text-center " style="padding:0.5rem !important">
                                  <h1 class="card-title m-t-10" style="font-size:65px" id="post_total">-</h1>
                                </div>
                                <div class="card-body text-center" style="padding:0rem !important">
                                    <ul class="list-inline m-b-0">
                                      <li>
                                        <h6 id="post_neg"class="text-red" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-red btnPost" value="neg">Negative</button></div> </li>
                                      <li>
                                        <h6 id="post_neutral" class="text-warning" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-warning btnPost" value="neutral">Neutral</button></div> </li>
                                      <li>
                                        <h6 id="post_pos" class="text-success" style="font-size:15px">0%</h6>
                                        <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-success btnPost" value="pos">Positive</button></div> </li>
                                    </ul>
                                </div>
                            </div>
                         </div>
                        </div>

                        <div class="col-lg-3">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title m-b-0">Comment Mentions</h4>
                            </div>
                            <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                              <div style="display:none"  align="center" id="cmt_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <div class="card-body text-center " style="padding:0.5rem !important">
                                    <h1 class="card-title m-t-10" style="font-size:65px" id="cmt_total">-</h1>
                                </div>
                                <div class="card-body text-center " style="padding:0rem !important">
                                  <ul class="list-inline m-b-0">
                                      <li>
                                          <h6 id="cmt_neg"class="text-red" style="font-size:15px">0%</h6>
                                          <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-xs btn-red btnComment" value="neg">Negative</button></div> </li>
                                      <li>
                                           <h6 id="cmt_neutral" class="text-warning" style="font-size:15px">0%</h6>
                                          <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-xs btn-warning btnComment" value="neutral">Neutral</button></div> </li>
                                      <li>
                                           <h6 id="cmt_pos" class="text-success" style="font-size:15px">0%</h6>
                                          <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-xs btn-success btnComment" value="pos">Positive</button></div> </li>
                                  </ul>
                                </div>
                            </div>
                          </div>
                        </div>

                      </div>
                 </div>
                <div class="tab-pane" id="article" role="tabpanel">
                  <div class="card">
                    <div class="card-body">
                        <div style="display:none"  align="center" style="vertical-align: top;" id="article-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                         <div class="table-responsive">
                            <table id="tbl_article"  class="table" style="margin-top:4px;" width="100%">
                              <thead style="display: none;"> </thead>
                            </table>
                          </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="mention" role="tabpanel">
                  <div class="card">
                    <div class="card-body">
                      <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                       <div class="table-responsive">
                        <table id="tbl_post"  class="table" style="margin-top:4px;" width="100%">
                          <thead>
                            <tr>
                              <th></th>
                              <th>Reaction</th>
                              <th>Shares</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                     </div>
                  </div>
                </div>
               <div class="tab-pane" id="comment" role="tabpanel">
                  <div class="card">
                      <div class="card-body">
                        <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                          <div class="table-responsive">
                              <table id="tbl_comment" class="table"  style="width:100%;margin-top:4px;">
                               <thead>  
                                <tr>
                                  <th></th>
                                  <th>Reaction</th>
                                </tr>
                              </thead>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
           </div>     
         </div>

         <div class="col-lg-3" id="filter_div" style="display: none">
          <div class="card earning-widget" style="height:230px">
            <div class="card-header">
              <h4 class="card-title m-b-0">Sentiment</h4>
            </div>
            <div class="card-body b-t collapse show">
              <div class="form-group">
                <div class="input-group">
                    <ul class="icheck-list sentiment-list" style="padding-top:0px">
                      <li>
                      <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="all" id="senti-all"> 
                      <label  for="senti-all" style="padding-left:30px;font-weight:500;font-size:12px">all</label>
                      </li>
                      <li>
                      <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="pos" id="senti-pos"> 
                      <label class="text-success" for="senti-pos" style="padding-left:30px;font-weight:500;font-size:12px">positive</label>
                      </li>
                      <li>
                      <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neg" id="senti-neg"> 
                      <label class="text-danger" for="senti-neg" style="padding-left:30px;font-weight:500;font-size:12px">negative</label>
                      </li>
                      <li> 
                      <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neutral" id="senti-neutral"> 
                      <label class="text-info"   for="senti-neutral" style="padding-left:30px;font-weight:500;font-size:12px">neutral</label>
                      </li>
                    </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="card earning-widget" style="height:450px">
            <div class="card-header">
                <h4 class="card-title m-b-0">Keywords</h4>
            </div>
            <div class="card-body b-t collapse show" style="overflow-y:scroll;height:450px">
               <div class="form-group">
                  <div class="input-group">
                      <ul class="icheck-list keyword-list" style="padding-top:0px"></ul>
                  </div>
              </div>
            </div>
          </div>
      </div>
    </div>     
 
   <div id="show-image" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              <div align="center" class="modal-body large_img"></div>
          </div>
      </div>  
   </div>
  <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="postModal">Article Detail</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              <div class="modal-body post_data">
              </div>
              <!-- <div class="modal-footer">
                <button style="display: none" type="button" id="btnSeeComment" class="btn btn-info waves-effect text-left" data-dismiss="modal">See Comments</button>
              </div> -->
          </div>
      </div>
  </div>
               
@endsection
@push('scripts')
    <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/icheck/icheck.min.js')}}"  defer></script>
    <script>
        window.onscroll = function() {myFunction()};

        var header = document.getElementById("myHeader");
        var myHeaderContent = document.getElementById("myHeaderContent");
        var sticky = header.offsetTop;

        function myFunction() {//alert("ho");
        // window.dispatchEvent(new Event('resize'));
          if (window.pageYOffset > sticky) {
            header.classList.add("s-topbar");
            header.classList.add("s-topbar-fix");
            myHeaderContent.classList.add("myHeaderContent");
             window.dispatchEvent(new Event('resize'));
          } else {
            header.classList.remove("s-topbar");
            header.classList.remove("s-topbar-fix");
            myHeaderContent.classList.remove("myHeaderContent");
          }
        }
    </script>
    <script type="text/javascript">
        var startDate;
        var endDate;
        /*global mention*/
        var mention_total;
        var mentionLabel = [];
        var mentions = [];
        /*Bookmark Array*/
        var bookmark_array=[];
        var bookmark_remove_array=[];
        /*global sentiment*/
        var positive = [];
        var negative = [];
        var sentimentLabel = [];
        var positive_total=0;
        var negative_total=0;
        var campaignGroup=[];
        var  colors=["#1e88e5","#dc3545","#28a745","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];

        /*var effectIndex = 2;
        var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

        $(document).ready(function() {
          //initialize
           $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
           $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
           $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
           $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
           $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
           $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
            /* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
             $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
             $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
             $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
             $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
             $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

           $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

          var GetURLParameter = function GetURLParameter(sParam) {
          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

          for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return sParameterName[1] === undefined ? true : sParameterName[1];
              }
          }
        };

          startDate = moment().subtract(1, 'month');
          endDate = moment();

           $('.singledate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        },function(date) {
          endDate=date;
          var id =$('input[type=radio][name=period-radio]:checked').attr('id');
          if(id==="period-week")
          {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
          }
          else
          {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
          }
         // alert(startDate);
         // alert(endDate);
            var admin_page=$("#admin_page_filter").val();

            ChooseDate(startDate,endDate,admin_page,'');
       
        });

       function hidden_div()
       {//alert("popular");
          var brand_id = GetURLParameter('pid');
          // var brand_id = 22;
          $( "#popular-spin" ).show();
          $("#popular").empty();
          $.ajax({
              type: "GET",
              dataType:'json',
              contentType: "application/json",
              url: "{{route('gethiddendiv')}}", // This is the URL to the API
              data: { view_name:'Dashboard'}
            })
          .done(function( data ) {//$("#popular").html('');
           for(var i in data) {
            $("#"+data[i].div_name).hide();
           }

          })
          .fail(function(xhr, textStatus, error) {
            //  console.log(xhr.statusText);
            // console.log(textStatus);
            // console.log(error);
            // If there is no communication between the server, show an error
           // alert( "error occured" );
          });
        }
        function ChooseDate(start,end,admin_page='',label='') 
        {
          $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
          var graph_option = $('.btn-group1 > .btn.active').text();
          getAllMentionPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          getAllArticle(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          getAllMentionComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          mention_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
         
          
          if (graph_option.trim()== 'Sentiment') {
            InboundSentimentDetail(GetStartDate(),GetEndDate());
           }
          else if (graph_option.trim() == 'Mention')
          {
              requestmentionData(GetStartDate(),GetEndDate());
          }
          var graph_option1 = $('.btn-group > .btn.active').text();
          if (graph_option1.trim()== 'Pages') {
            // alert('hhh');
             PagesMentionCount(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           }
          else if (graph_option1.trim() == 'Websites')
          {
              WebsiteMentionCount(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          }
          getCampaignGroup();
        }

        function getCampaignGroup()
  {
    // add-campaign
   
      var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getCampaignGroup')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          page_name:$('#hidden-chip').val(),
          }
      })
      .done(function( data ) {
        campaignGroup=data;
       
          
        
      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }

        function GetStartDate()
        {
          return startDate.format('YYYY MM DD');
        }
        function GetEndDate()
        {
          return endDate.format('YYYY MM DD');
        }

        
        


        function getAllMentionComment(fday,sday,filter_tag='')
        {
        	
            // alert(hidden_kw);
            // alert($('#hidden-chip').val());
            $(".popup_post").unbind('click');
            $(".edit_predict").unbind('click');
            $("#add_tag").unbind('click');
            $("#comment_hide").unbind('click');
            var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });
            var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
            });
      
            document.getElementById('select_group').innerHTML =$('#hidden-chip').val();
            var oTable = $('#tbl_comment').DataTable({
            "lengthChange": false,
            "searching": false,
            "processing": false,
            "serverSide": true,
            "destroy": true,
            "order":[],
            "headers": {
              'X-CSRF-TOKEN': '{{csrf_token()}}' 
            },
            "ajax": {
              "url": '{{ route('getMentionComment') }}',
              "dataSrc": function(res){
                  //var count = res.data.length;
                  //alert(count);
                  document.getElementById('title_comment').innerHTML = 'Comments ('+res.recordsTotal+")";
                  return res.data;
                },
             /*       data: function ( d ) {
                      d.fday = mailingListName;
                      d.sday = mailingListName;
                      d.brand_id = mailingListName;
                    }*/
             "data": {
              "fday": fday,
              "sday": sday,
              "keyword_flag": '1',
              "other_page":'1',
              "brand_id": GetURLParameter('pid'),
              "tsearch_senti":filter_senti_arr.join("| "),
              "tsearch_keyword":filter_keyword_arr.join("| "),
              "tsearch_tag":filter_tag,
              "keyword_group": $('#hidden-chip').val(),
            }
          },
              "initComplete": function( settings, json ) {
                //console.log(json);
             
              },
               drawCallback: function() {
               $('.select2').select2();
            },

              columns: [
              {data: 'post_div', name: 'post_div',"orderable": false},
              {data: 'reaction', name: 'reaction', "orderable": true,className: "text-center"},
              ]
        
              }).on('click', '.popup_post', function (e) {
                if (e.handled !== true) {
                  var name = $(this).attr('name');
                  //alert(name);
                  var post_id = $(this).attr('id');
                  $("#modal-spin").show();
                  $(".post_data").empty();
                  //alert(post_id);alert(name);alert(GetURLParameter('pid'));
                   $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("getRelatedMention") }}',
                   type: 'GET',
                   data: {id:post_id,brand_id:GetURLParameter('pid')},
                   success: function(response) { //console.log(response);
                   var res_array=JSON.parse(response);
                   var data=res_array[0];
                    for(var i in data) {//alert(data[i].message);
                      var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                       var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                       var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                       var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                       var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                       pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                       neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                       neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                       pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                       neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                       neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

                       var page_name=data[i].page_name;
                       var page_Link= "https://www.facebook.com/" + page_name ;                   

                       var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                                '  <div class="sl-left">'+
                                '<img src="assets/images/unknown_photo.jpg"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                                ' <div class="sl-right">'+
                                '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a> '+
                                ' <div class="sl-right"> '+
                                ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                                ' <div style="width:55%;display: inline-block">';
                                if(neg_pcent!=='')
                                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                                if(pos_pcent!=='')
                                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                                if(neutral_pcent!=='')
                                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                                html+=' </div></div></div></span></p> ' +                
                                  '   <p class="m-t-10" align="justify">' + 
                                    data[i].message +
                                    '</p>';
                                 if(data[i]['post_type'] =='photo')
                                {
                                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                                }
                                else if(data[i]['post_type'] =='video')
                                {
                                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                                }
                                  html +='</div></div>';
                                $(".post_data").append(html);
                            }


                     $("#modal-spin").hide();
                     $("#postModal").text(name);
                     $('#show-post-task').modal('show'); 
                    }
                  });
                    e.handled = true;
                }
              }).on('change', '.edit_senti', function (e) {
                var hidden_kw  = $('#hidden-chip').val();
                var default_kw = $('.hidden-kw').val();
                  var user_role = $('.hidden-role').val();
                  if(user_role == 'Admin')
                  {
                    if (e.handled !== true) {
                    var senti_id = $(this).attr('id');
                    var post_id  = senti_id.replace("sentiment_","");
                    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
                    var emotion = $('#emotion_'+post_id+' option:selected').val();
                    $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
                    if(sentiment == 'pos')
                     $("#sentiment_"+post_id).addClass('text-success');
                    else if (sentiment == 'neg')
                      $("#sentiment_"+post_id).addClass('text-red');
                    else
                      $("#sentiment_"+post_id).addClass('text-warning');
                     $.ajax({
                     headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                     url:'{{ route("setUpdatedPredict") }}',
                     type: 'POST',
                     data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,type:'mention'},
                     success: function(response) {// alert(response)
                      if(response>=0)
                      {
                        
                        var x = document.getElementById("snackbar")
                                    $("#snack_desc").html('');
                                    $("#snack_desc").html('Sentiment Changed!!');
                                      x.className = "show";

                                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       }
                    }
               });

                e.handled = true;
            }
                  }
                  else{
              	if(hidden_kw === default_kw){
                  if (e.handled !== true) {
                    var senti_id = $(this).attr('id');
                    var post_id  = senti_id.replace("sentiment_","");
                    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
                    var emotion = $('#emotion_'+post_id+' option:selected').val();
                    $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
                    if(sentiment == 'pos')
                     $("#sentiment_"+post_id).addClass('text-success');
                    else if (sentiment == 'neg')
                      $("#sentiment_"+post_id).addClass('text-red');
                    else
                      $("#sentiment_"+post_id).addClass('text-warning');
                     $.ajax({
                     headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                     url:'{{ route("setUpdatedPredict") }}',
                     type: 'POST',
                     data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,type:'mention'},
                     success: function(response) {// alert(response)
                      if(response>=0)
                      {
                        
                        var x = document.getElementById("snackbar")
                                    $("#snack_desc").html('');
                                    $("#snack_desc").html('Sentiment Changed!!');
                                      x.className = "show";

                                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       }
                    }
               });

                e.handled = true;
            }
            }
            else
            {
            	var x = document.getElementById("snackbar")
                                    $("#snack_desc").html('');
                                    $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! !!');
                                      x.className = "show";

                                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                                      return false;
            }
          }
          }).on('click', '.comment_hide', function (e) {//alert("hihi");
              if (e.handled !== true) {
                var attr_id = $(this).attr('id');
                var comment_id  = attr_id.replace("hide_","");
                var answer = confirm("Are you sure to hide this comment?")
                    if (answer) {
                       $.ajax({
                       headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                       url:'{{ route("SetHideMention") }}',
                       type: 'POST',
                       data: {id:comment_id,brand_id:GetURLParameter('pid'),table_type:'comment'},
                       success: function(response) { //alert(response)
                        if(response>=0)
                        {
                          $("#"+comment_id).closest('tr').remove();
                        }
                      }
                  });
                }
                else
                {

                }
            }
            e.handled = true;
         }).on('click', '.popup_img', function (e) {
              if (e.handled !== true) {
                   var id = $(this).attr('id');
                   var src_path = $('#'+id).attr('src');
                    $(".large_img").empty();
                     // img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
                    $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%'' src='"+src_path+"'/>");
                    $('#show-image').modal('show'); 
                   }
               });
       }


      function getAllArticle(fday,sday)
      {
          $(".popup_post").unbind('click');
            // $(".see_comment").unbind('click');
            // $(".post_hide").unbind('click');
            //alert(filter_keyword_arr.join("| "));
          var filter_senti_arr = [];
          $.each($("input[name='sentimentlist']:checked"), function(){            
              filter_senti_arr.push($(this).val());
            });
          var filter_keyword_arr = [];
          $.each($("input[name='keyword']:checked"), function(){            
              filter_keyword_arr.push($(this).val());
            });
          var oTable = $('#tbl_article').DataTable({
          // "responsive": true,
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": true,
          "serverSide": true,
          "destroy": true,
          "order": [],
          "autoWidth": false,
          "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "ajax": {
          "url": '{{ route('getAllArticle') }}',
          "dataSrc": function(res){
              var count = res.data.length;
              document.getElementById('title_article').innerHTML = 'Articles ('+res.recordsTotal+")";
              return res.data;
            },
            "data": {
            "fday": fday,
            "sday": sday,
            "brand_id": GetURLParameter('pid'),
            "tsearch_senti":filter_senti_arr.join("| "),
            "tsearch_keyword":filter_keyword_arr.join("| "),
            "keyword_group": $('#hidden-chip').val(),
          
          }

        },
        "initComplete": function( settings, json ) {
        $('#tbl_article thead tr').removeAttr('class');
        // $("#tbl_article thead").remove();
        },
        columns: [
        {data: 'post', name: 'post',"orderable": false},
        ]
        
      }).on('click', '.popup_post', function (e) {
          if (e.handled !== true) {
            var name = $(this).attr('name');
            var post_id = $(this).attr('id');
            $("#modal-spin").show();
            $(".post_data").empty();

            //alert(post_id);alert(name);alert(GetURLParameter('pid'));
             $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("getRelatedArticle") }}',
             type: 'GET',
             data: {id:post_id,brand_id:GetURLParameter('pid'),keyword_group: $('#hidden-chip').val()},
             success: function(response) { console.log(response);
              var res_array=JSON.parse(response);
              // alert(res_array);
              var data=res_array[0];
              var keyForHighLight=res_array[1];

              for(var i in data) {//alert(res_array[1]);
               var page_name=data[i].page_name;
               var link = data[i].link;
               var message = highLightText(keyForHighLight,data[i].message);
               // alert(message);
               var page_Link= "https://www.facebook.com/" + page_name ; 
               var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                        '<div class="form-material sl-left" style="padding-left:20px;">'+ 
                         '<a href="'+link+'" style="font-size:18px;font-weight:800px;" target="_blank" >' +
                         data[i].title +
                        '</a><div>' + ' <div class="sl-right"> '+
                        '<div style ="width:50%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i>' +data[i].created_time + ' by ' + '<a href="'+page_Link+'" target="_blank">' +data[i].page_name+'</a></span></div> '+
                        ' <div class="sl-right"> '+
                        ' <div style="width:55%;display: inline-block">';
                    html+=' </div></div></div> ' +                
                          '   <p class="m-t-10" align="justify">' + 
                          message +
                        
                          '</p>';
                    html +='</div></div>';
                $(".post_data").append(html);
        }
         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
      });
          e.handled = true;
    }
  }).on('change', '.edit_senti', function (e) {
        var hidden_kw  = $('#hidden-chip').val();
        var default_kw = $('.hidden-kw').val();
         var user_role = $('.hidden-role').val();
         if(user_role == 'Admin')
         {
              if (e.handled !== true) {
              var  brand_id = GetURLParameter('pid');
              var senti_id = $(this).attr('id');
              var post_id  = senti_id.replace("sentiment_","");
              var sentiment = $('#sentiment_'+post_id+' option:selected').val();
              var emotion = $('#emotion_'+post_id+' option:selected').val();
              

              $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
              if(sentiment == 'pos')
               $("#sentiment_"+post_id).addClass('text-success');
              else if (sentiment == 'neg')
                $("#sentiment_"+post_id).addClass('text-red');
              else
                $("#sentiment_"+post_id).addClass('text-warning');

              
                   $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("setArticlePredict") }}',
                   type: 'POST',
                   data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment},
                   success: function(response) {// alert(response)
                    if(response>=0)
                    {
                      //         swal({   
                      //     title: "Updated!",   
                      //     text: "Done!",   
                      //     timer: 1000,   
                      //     showConfirmButton: false 
                      // });
                      var x = document.getElementById("snackbar")
                                  $("#snack_desc").html('');
                                  $("#snack_desc").html('Sentiment Changed!!');
                                    x.className = "show";

                                    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                     }
                  }
                      });

                    e.handled = true;
                 }
         }
         else {
        if(hidden_kw === default_kw){
                if (e.handled !== true) {
                var  brand_id = GetURLParameter('pid');
                var senti_id = $(this).attr('id');
                var post_id  = senti_id.replace("sentiment_","");
                var sentiment = $('#sentiment_'+post_id+' option:selected').val();
                var emotion = $('#emotion_'+post_id+' option:selected').val();
                

                $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
                if(sentiment == 'pos')
                 $("#sentiment_"+post_id).addClass('text-success');
                else if (sentiment == 'neg')
                  $("#sentiment_"+post_id).addClass('text-red');
                else
                  $("#sentiment_"+post_id).addClass('text-warning');

                
                     $.ajax({
                     headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                     url:'{{ route("setArticlePredict") }}',
                     type: 'POST',
                     data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment},
                     success: function(response) {// alert(response)
                      if(response>=0)
                      {
                        //         swal({   
                        //     title: "Updated!",   
                        //     text: "Done!",   
                        //     timer: 1000,   
                        //     showConfirmButton: false 
                        // });
                        var x = document.getElementById("snackbar")
                                    $("#snack_desc").html('');
                                    $("#snack_desc").html('Sentiment Changed!!');
                                      x.className = "show";

                                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       }
                    }
                        });

                      e.handled = true;
                   }
         }
           else
            {
              var x = document.getElementById("snackbar")
                                    $("#snack_desc").html('');
                                    $("#snack_desc").html('You are not allowed to edit this article . Changes will not be saved !!! !!');
                                      x.className = "show";

                                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                                      return false;
            }
          }

        
            
      }).on('click', '.post_hide', function (e) {
          if (e.handled !== true) {
            var attr_id = $(this).attr('id');
            var post_id  = attr_id.replace("hide_","");
            var answer = confirm("Are you sure to hide this post?")
            if (answer) {
                 $.ajax({
                     headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                     url:'{{ route("SetHideMention") }}',
                     type: 'POST',
                     data: {id:post_id,brand_id:GetURLParameter('pid'),table_type:'web_mention'},
                     success: function(response) {// alert(response)
                      if(response>=0)
                      {
                        $("#"+post_id).closest('tr').remove();
                      }
                    }
                  });
               }
              else {
                  
              }
         }
         e.handled = true;
          }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
      // alert(btn_val);
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';
              htmltext +='<fieldset class="controls" style="text-align:left;padding-left:20px;">';

                                       
  for(var i in campaignGroup) 
          {
            // alert(campaignGroup);
            if(i==0)
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input">'+
                        '<span class="custom-control-label">'+campaignGroup[i]['name']+
                        '</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
              title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign_article") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:0,campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign_article") }}',
         type: 'POST',
         data: {id:post_id,brand_id:0},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
          }
          
            e.handled = true;
         });

        new $.fn.dataTable.FixedHeader( oTable );

         // oTable.columns.adjust().draw();
         // $('#tbl_post').css('width', '100%');
         // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
  }
  function getAllMentionPost(fday,sday)
  {
      $(".popup_post").unbind('click');
      $(".see_comment").unbind('click');
      $(".post_hide").unbind('click');

      var filter_senti_arr = [];
      $.each($("input[name='sentimentlist']:checked"), function(){            
          filter_senti_arr.push($(this).val());
        });
      var filter_keyword_arr = [];
      $.each($("input[name='keyword']:checked"), function(){            
          filter_keyword_arr.push($(this).val());
        });
      // alert(filter_keyword_arr);
   
      //alert(filter_keyword_arr.join("| "));


      var oTable = $('#tbl_post').DataTable({
  
       // "responsive": true,
        "paging": true,
        "pageLength": 10,
        "lengthChange": false,
        "searching": false,
        "processing": true,
        "serverSide": true,
        "destroy": true,
         "order": [],
          // "ordering": true   ,
          // "scrollX":        true,
          // "scrollCollapse": true,
         "autoWidth": false,
        
          //     "columnDefs": [
          //   { "targets": [0] , "orderable": false},       
          //   {"targets": [1],"orderable": true },
          //   {"targets": [2],"orderable": true }
          // ],
       
       // "order": [[ 1, 'asc' ], [ 2, 'asc' ]],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
       
          "ajax": {
            "url": '{{ route('getMentionPost') }}',
            "dataSrc": function(res){
                var count = res.data.length;
                //alert(res.recordsTotal);
                document.getElementById('title_post').innerHTML = 'Posts ('+res.recordsTotal+")";
                return res.data;
              },
             /*       data: function ( d ) {
                      d.fday = mailingListName;
                      d.sday = mailingListName;
                      d.brand_id = mailingListName;
                    }*/
            "data": {
            "fday": fday,
            "sday": sday,
            "tsearch_senti":filter_senti_arr.join("| "),
            "tsearch_keyword":filter_keyword_arr.join("| "),
            "brand_id": GetURLParameter('pid'),
            "keyword_group": $('#hidden-chip').val(),
          }
        },
            "initComplete": function( settings, json ) {
              //console.log(json);
            $('.tbl_post thead tr').removeAttr('class');
            },
            columns: [
            {data: 'post', name: 'post',"orderable": false},
            {data: 'reaction', name: 'reaction', orderable: true,className: "text-center"},
            {data: 'share', name: 'share', orderable: true, className: "text-center"},
        ]
        
      }).on('click', '.popup_post', function (e) {
          if (e.handled !== true) {
            var name = $(this).attr('name');
            //alert(name);
            var post_id = $(this).attr('id');
           // alert(post_id);
            $("#modal-spin").show();
            $(".post_data").empty();

            //alert(post_id);alert(name);alert(GetURLParameter('pid'));
             $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("getRelatedMention") }}',
             type: 'GET',
             data: {id:post_id,brand_id:GetURLParameter('pid'),keyword_group: $('#hidden-chip').val()},
             success: function(response) { //console.log(response);
              var res_array=JSON.parse(response);
              var data=res_array[0];
              var keyForHighLight=res_array[1];

             for(var i in data) {//alert(data[i].message);
               var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
               var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

               var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
               var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
               var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
               pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
               neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
               neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
               pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
               neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
               neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

               var page_name=data[i].page_name;
            
               var message = highLightText(keyForHighLight,data[i].message);
               var page_Link= "https://www.facebook.com/" + page_name ;                   

               var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                        '  <div class="sl-left">'+
                        '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                        ' <div class="sl-right">'+
                        '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a> '+
                        ' <div class="sl-right"> '+
                        ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                        ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                  html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                if(pos_pcent!=='')
                  html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                if(neutral_pcent!=='')
                  html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;
                  html+=' </div></div></div></span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                    message +
                    '</p>';
                if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                   html +='</div></div>';
                $(".post_data").append(html);
        }
         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
      });
          e.handled = true;
    }
  }).on('change', '.edit_senti', function (e) {
    // alert('hey');
    // alert(GetURLParameter('pid'));
    var hidden_kw  = $('#hidden-chip').val();
    var default_kw = $('.hidden-kw').val();
    var user_role = $('.hidden-role').val();
    if(user_role == 'Admin')
    {
      if (e.handled !== true) {

      var senti_id = $(this).attr('id');
      var post_id  = senti_id.replace("sentiment_","");
      var sentiment = $('#sentiment_'+post_id+' option:selected').val();
      var emotion = $('#emotion_'+post_id+' option:selected').val();

      $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
      if(sentiment == 'pos')
       $("#sentiment_"+post_id).addClass('text-success');
      else if (sentiment == 'neg')
        $("#sentiment_"+post_id).addClass('text-red');
      else
        $("#sentiment_"+post_id).addClass('text-warning');

    
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setMentionPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment},
         success: function(response) {// alert(response)
                  if(response>=0)
                      {
                        //         swal({   
                        //     title: "Updated!",   
                        //     text: "Done!",   
                        //     timer: 1000,   
                        //     showConfirmButton: false 
                        // });
                        var x = document.getElementById("snackbar")
                                    $("#snack_desc").html('');
                                    $("#snack_desc").html('Sentiment Changed!!');
                                      x.className = "show";

                                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       }
        }
      });
          e.handled = true;
     }
    }
    else {
    if(hidden_kw === default_kw){
      if (e.handled !== true) {

      var senti_id = $(this).attr('id');
      var post_id  = senti_id.replace("sentiment_","");
      var sentiment = $('#sentiment_'+post_id+' option:selected').val();
      var emotion = $('#emotion_'+post_id+' option:selected').val();

      $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
      if(sentiment == 'pos')
       $("#sentiment_"+post_id).addClass('text-success');
      else if (sentiment == 'neg')
        $("#sentiment_"+post_id).addClass('text-red');
      else
        $("#sentiment_"+post_id).addClass('text-warning');

    
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setMentionPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment},
         success: function(response) {// alert(response)
                  if(response>=0)
                      {
                        //         swal({   
                        //     title: "Updated!",   
                        //     text: "Done!",   
                        //     timer: 1000,   
                        //     showConfirmButton: false 
                        // });
                        var x = document.getElementById("snackbar")
                                    $("#snack_desc").html('');
                                    $("#snack_desc").html('Sentiment Changed!!');
                                      x.className = "show";

                                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       }
        }
      });
          e.handled = true;
     }
   }
           else
            {
              var x = document.getElementById("snackbar")
                                    $("#snack_desc").html('');
                                    $("#snack_desc").html('You are not allowed to edit this post . Changes will not be saved !!! !!');
                                      x.className = "show";

                                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                                      return false;
            }
          }

     
    
        
  }).on('click', '.see_comment', function (e) {//alert("hihi");
        if (e.handled !== true) {
              var attr_id = $(this).attr('id');
              var post_id  = attr_id.replace("seeComment_","");
              //alert(post_id);
              // window.open("{{ url('relatedmentioncmt?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
              window.open("{{ url('relatedmentioncmt?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&post_id="+post_id, '_blank');
               
              }
               e.handled = true;
             }).on('click', '.post_hide', function (e) {
                if (e.handled !== true) {
                  var attr_id = $(this).attr('id');
                  var post_id  = attr_id.replace("hide_","");
                  var answer = confirm("Are you sure to hide this post?")
                  if (answer) {
                       $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetHideMention") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:GetURLParameter('pid'),table_type:'post'},
                           success: function(response) {// alert(response)
                            if(response>=0)
                            {
                              $("#"+post_id).closest('tr').remove();
                            }
                          }
                        });
                     }
                    else {
                        
                    }
               }
               e.handled = true;
          }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
      // alert(btn_val);
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';
              htmltext +='<fieldset class="controls" style="text-align:left;padding-left:20px;">';

                                       
  for(var i in campaignGroup) 
          {
            // alert(campaignGroup);
            if(i==0)
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input">'+
                        '<span class="custom-control-label">'+campaignGroup[i]['name']+
                        '</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
              title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign_mention") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:0,campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign_mention") }}',
         type: 'POST',
         data: {id:post_id,brand_id:0},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
          }
          
            e.handled = true;
         });

            new $.fn.dataTable.FixedHeader( oTable );

             // oTable.columns.adjust().draw();
             // $('#tbl_post').css('width', '100%');
             // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
       }

             //         $(window).on('resize', reSize);
             //          $(window).trigger('resize');
             // function reSize() {
             //  if (document.createEvent) {

             //          window.dispatchEvent(new Event('resize'));
                
                    
             //      } else {
             //        document.body.fireEvent('onresize');
             //      }
             // }

              //   $(window).resize( function() {

              //      window.dispatchEvent(new Event('resize'));
               
              //        //  new $.fn.dataTable.FixedHeader( oTable );
              //    // $('#tbl_post').css('width', '100%');
              //     //$("#tbl_post").DataTable().columns.adjust().draw();
              //     //$('table.fixedHeader-floating').css('left', '200px');
              // });

        bindKeyword();

        function bindKeyword()
        {
           // var username = $('#hidden-user').val();
            $("#chip-competitor tbody").empty();
            var pid = GetURLParameter('pid');
                $.ajax({
               type: "GET",
               dataType:'json',
               contentType: "application/json",
               url: "{{route('getkeywordgroup')}}", // This is the URL to the API
               data: { 
               pid:pid,
                }
            })
              .done(function( combine_data ) {
                console.log(combine_data);
                // alert(combine_data);
                // alert(username);
                 var  data = combine_data [0];
                 var  default_keyword=combine_data [1][0]['default_keyword'];
                 // console.log(default_keyword);
                 for(var i in data) 
                 {
                    var photo ="{{asset('assets/images/keyword-research.png')}}";
                  
                    if(default_keyword !== '')
                    {
                      
               
                      if(default_keyword == data[i]['group_name']  )
                     { 
                      $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['group_name']+'" id="'+data[i]['id']+'" > ' +
                            ' <img src="'+photo+'" alt=""> '+
                            data[i]['group_name'] + '</div>');
                         $("#hidden-chip").val(data[i]['group_name']);
          

                     }
                      else
                      {
                        $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['group_name']+'"  id="'+data[i]['id']+'" > ' +
                              ' <img src="'+photo+'" alt=""> '+
                              data[i]['group_name'] + '</div>');
                      }
             
                   }
                   else
                   {
                     if(parseInt(i) == 0)
                      { 
                        $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['group_name']+'" id="'+data[i]['id']+'" > ' +
                              ' <img src="'+photo+'" alt=""> '+
                              data[i]['group_name'] + '</div>');
                           $("#hidden-chip").val(data[i]['group_name']); 
                      }
                      else
                      {
                        $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['group_name']+'"  id="'+data[i]['id']+'" > ' +
                              ' <img src="'+photo+'" alt=""> '+
                              data[i]['group_name'] + '</div>');
                      }
                   }
               }
         ChooseDate(startDate,endDate,'','');
         getKeyword($("#hidden-chip").val());


      })
         .fail(function(xhr, textStatus, error) {
          //  console.log(xhr.statusText);
          // console.log(textStatus);
          // console.log(error);
        
        });
      }
       
      function getKeyword($keyword_group)
      {
          $(".keyword-list").empty();
          var brand_id = GetURLParameter('pid');
              $.ajax({
              type: "GET",
              dataType:'json',
              contentType: "application/json",
              url: "{{route('getKeywordsByGroup')}}", // This is the URL to the API
              data: {brand_id:brand_id,keyword_group:$keyword_group}
            })
            .done(function( data ) {
              // console.log(data);
            for(var i in data) 
            {
              if(i<10)
              {
                $(".keyword-list").append('<li class="see-li">' +
                ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
                ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
                ' </li>');
              }
              else if (i==10)
              {
                $(".keyword-list").append('<li class="see-li">' +
                ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
                ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
                ' </li>  <li class="see-li" id="more"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see more</a></li>');
              }
              else
              {
                 $(".keyword-list").append('<li class="hidden-li" style="display:none">' +
                ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
                ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
                ' </li><br/>');
              }
            }
                $(".keyword-list").append('<li class="hidden-li" id="less" style="display:none"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see less</a></li>');
        })
         .fail(function(xhr, textStatus, error) {
           // console.log(xhr.statusText);
          // console.log(textStatus);
          // console.log(error);
        
        });
      }
      $(document).on('click', '.chip', function (e) {
          var id = $(this).attr("id");  
          var name = $(this).attr("name");
          var chipcompetitor = document.getElementById("chip-competitor");
          //alert(name);  
            $(".chip").css("background-color","#e4e4e4");
            $(".chip").css("color","rgba(0,0,0,0.6)");
             // chipcompetitor.classList.remove("myHeaderChip");
           // chipcompetitor.classList.add("myHeaderChip");
           // $('.dropdown.open .dropdown-toggle').dropdown('toggle');
           // $("#search_form").css("visibility", "hidden");
           // $("#search_form").css("display", "inline");
            $("#"+id).css("background-color","rgba(188, 138, 49, 1)");
            $("#"+id).css("color","white");
            $("#hidden-chip").val(name);
          // alert($("[data-toggle=dropdown]").attr("aria-expanded"))  ;
           $(".dropdown-pages").dropdown('toggle');
            ChooseDate(startDate,endDate,'','');
            getKeyword(name);
        });

        $(document).on('click', '#more', function () {
           $('#more').css('display','none');
            $('.hidden-li').css('display','inline');
        });
        $(document).on('click', '#less', function () {
          $('#less').css('display','none');
            $('.hidden-li').css('display','none');
             $('#more').css('display','inline');
        });

        $(document).on('click', '#see_more', function () {
           // $('mention_show').css('display','none');
           $('#see_more').css('display','none');
           $('.btn_page_mention_hide').css('display','inline');
           $('#see_less').css('display','inline');
        });
        $(document).on('click', '#see_less', function () {
          $('.btn_page_mention_hide').css('display','none');
            $('.btn_page_mention_show').css('display','inline');
            $('#see_more').css('display','inline');
            $('#see_less').css('display','none');
        });

        // $(document).on('click', '#see_more_web', function () {
        //    //$('.btn_page_mention_show').css('display','none');
        //    $('#see_more_web').css('display','none');
        //    $('.btn_website_mention_hide').css('display','inline');
        //    $('#see_less_web').css('display','inline');
        // });
        // $(document).on('click', '#see_less_web', function () {
        //     $('.btn_website_mention_hide').css('display','none');
        //     $('.btn_website_mention_show').css('display','inline');
        //     $('#see_more_web').css('display','inline');
        //     $('#see_less_web').css('display','none');
        // });

        $(document).on('click', '.keyword', function () {
           getAllMentionPost(GetStartDate(),GetEndDate());
           getAllMentionComment(GetStartDate(),GetEndDate());
           getAllArticle(GetStartDate(),GetEndDate());
        });
        $(document).on('click', '.sentimentlist', function () {
           getAllMentionPost(GetStartDate(),GetEndDate());
           getAllMentionComment(GetStartDate(),GetEndDate());
           getAllArticle(GetStartDate(),GetEndDate());
        });

          $(document).on('click','.btn_page_mention',function(){
            // alert(params);
          var page_name = $(this).val();
          var hidden_kw  = $('#hidden-chip').val();
          var default_kw = $('.hidden-kw').val();
          window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&keyword_group="+$('#hidden-chip').val()+"&page_name="+page_name+"&hidden_kw="+hidden_kw+"&default_kw="+default_kw , '_blank');
          // window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+params.name+"&keyword_group="+$('#hidden-chip').val()+"&page_name="+page_name+"&hidden_kw="+hidden_kw+"&default_kw="+default_kw , '_blank');
          })

          $(document).on('click','.btn_website_mention',function(){
            // alert('hey');
          var page_name = $(this).val();
          var hidden_kw  = $('#hidden-chip').val();
          var default_kw = $('.hidden-kw').val();
          window.open("{{ url('relatedarticle?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&keyword_group="+$('#hidden-chip').val()+"&page_name="+page_name+"&hidden_kw="+hidden_kw+"&default_kw="+default_kw , '_blank');
          })

            // $('#senti-all').click(function () {    
            //      $('input:checkbox').prop('checked', this.checked);    
            //  });
          $('#senti-all').on('click', function () {
             if ($(this).prop('checked')) {
                  $('.sentimentlist').each(function () {
                      $(this).prop('checked', true);
                  });
                  } 
                  else {
                  $('.sentimentlist').each(function () {
                      $(this).prop('checked', false);
                  });
              }
          });
          $("input:checkbox:not(.keyword)").click(function () {
              $("#senti-all").prop("checked", $("input:checkbox:not(.keyword):checked").length == 4);
            });

            $('.dateranges').daterangepicker({
                locale: {
                        format: 'MMM D, YYYY'
                    },
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                          startDate: startDate,
                          endDate: endDate,
                    },function(start, end,label) {//alert(label);
                    var startDate;
                    var endDate;
                    startDate = start;
                    endDate = end;
                    ChooseDate(startDate,endDate,'',label);
                  });
                // ChooseDate(startDate,endDate,'','');

                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                   var target = $(e.target).attr("href") // activated tab
                   window.dispatchEvent(new Event('resize'));
                  if(target == '#dashboard')
                  {
                     // var graph_option = $('.btn-group > .btn.active').text();
                     // if(graph_option.trim()=='Sentiment')
                     // InboundSentimentDetail(GetStartDate(),GetEndDate());
                     
                    $("#outer_div").removeClass("col-lg-9");
                    $("#outer_div").addClass("col-lg-12");
                    $("#filter_div").hide();
                    $("#top-div").show();
                  }
                  else
                  {
                    $("#outer_div").removeClass("col-lg-12");
                    $("#outer_div").addClass("col-lg-9");
                    $("#filter_div").show();
                     $("#top-div").hide();
                  }
            });

            hidden_div();

            $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
                 });


            $('#admin_page_filter').change(function() {
                //alert($(this).val());
                var admin_page = $(this).val();
               ChooseDate(startDate,endDate,admin_page,'');
                 // $(this).val() will work here
            });

            $("#btnSeeComment").click( function()
            {
              var post_id=$("#popup_id").val();
              window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() , '_blank');
            }
          );
          //DashBoard Data
          function PagesMentionCount(fday,sday)
          {
              $(".page_mention_count").empty();
              $("#page_mention_spin").show();
              var brand_id = GetURLParameter('pid');
              $.ajax({
                type: "GET",
                dataType:'json',
                contentType: "application/json",
                url: "{{route('getPagewithmentioncount')}}", // This is the URL to the API
                data: { fday: fday,sday:sday,brand_id:brand_id,keyword_group:$("#hidden-chip").val()}
              })
              .done(function( data ) {//console.log(data);
                $("#page_mention_spin").hide();
                $pos = 0;

               $arry_length = Object.keys(data).length;
               for(var i in data) 
               {
                 var page_name=data[i].page_name;
                 var count=data[i].count;
                 if($pos<9)
                 {
                  $(".page_mention_count").append('<div><button type="button" style="margin:0.2rem;width:100%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_page_mention_show btn_page_mention" value="'+page_name+'"><span style="float:left">'+page_name+'</span><span class="label label-light-info" style="padding:1px 5px;margin-left:5px;float:right"">'+count+'</span></button></div>');
                 }
           
                 else if($pos==9)
                 {
                   $(".page_mention_count").append('<div><button type="button" style="margin:0.2rem;width:100%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_page_mention_show btn_page_mention" value="'+page_name+'"><span style="float:left">'+page_name+'</span><span class="label label-light-info" style="padding:1px 5px;margin-left:5px;float:right">'+count+'</span></button></div>');
                     $(".page_mention_count").append('<div style="padding-left:1rem"><a style="font-weight: 700;font-size:15px" class="text-info" href="javascript:void(0)" id="see_more">see more &gt;&gt;</a></div>');
                 }
                 else
                 {
                   $(".page_mention_count").append('<div><button type="button" style="margin:0.2rem;width:100%;display:none" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_page_mention_hide btn_page_mention" value="'+page_name+'"><span style="float:left">'+page_name+'</span><span class="label label-light-info" style="padding:1px 5px;margin-left:5px;float:right">'+count+'</span></button></div>');
                 }
                if($pos == $arry_length-1)
                $(".page_mention_count").append('<div style="padding-left:1rem"><a style="font-weight: 700;font-size:15px;display:none;width:100%" class="text-info" href="javascript:void(0)" id="see_less"><< see less</a></div>');
                $pos +=1;
              }
       
                $("#top_mention_total").text($arry_length==0?'-':$arry_length);
              })

                 .fail(function(xhr, textStatus, error) {
                  //  console.log(xhr.statusText);
                  // console.log(textStatus);
                  // console.log(error);
                
                });

            }
            $(".btnComment").click(function(e){
                var commentType = $(this).attr('value');
                var hidden_kw  = $('#hidden-chip').val();
                var default_kw = $('.hidden-kw').val();
                window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid') +"&SentiType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&keyword_group="+$('#hidden-chip').val()+"&hidden_kw="+hidden_kw+"&default_kw="+default_kw , '_blank');
            })
            $(".btnPost").click(function(e){
               var hidden_kw  = $('#hidden-chip').val();
                var default_kw = $('.hidden-kw').val();
                var postType = $(this).attr('value');
                window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid') +"&SentiType="+postType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&keyword_group="+$('#hidden-chip').val()+"&hidden_kw="+hidden_kw+"&default_kw="+default_kw , '_blank');
            })
            $(".btnMention").click(function(e){
                var hidden_kw  = $('#hidden-chip').val();
                var default_kw = $('.hidden-kw').val();
                var mentionType = $(this).attr('value');
               window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid') +"&SentiType="+mentionType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&keyword_group="+$('#hidden-chip').val()+"&hidden_kw="+hidden_kw+"&default_kw="+default_kw , '_blank');
            })
            $(".link_mention_cmt").click(function(e)
              {
              
                $("#comment_tab").tab('show');
              }
              )
            $(".link_mention_post").click(function(e)
              {
              
                $("#post_tab").tab('show');
              }

              )
              $(".link_article").click(function(e)
              {
              
                $("#article_tab").tab('show');
              })


          function WebsiteMentionCount(fday,sday)
          {
              $(".pages_mention_count").empty();
              $(".website_mention_count").empty();
              $("#website_mention_spin").show();
              var brand_id = GetURLParameter('pid');
              $.ajax({
                type: "GET",
                dataType:'json',
                contentType: "application/json",
                url: "{{route('getPagewithWebsitementioncount')}}", // This is the URL to the API
                data: { fday: fday,sday:sday,brand_id:brand_id,keyword_group:$("#hidden-chip").val()}
              })
              .done(function( data ) {console.log(data);
                $("#website_mention_spin").hide();
                $(".page_mention_count").empty();
                $pos = 0;

               $arry_length = Object.keys(data).length;
               for(var i in data) 
               {
                 var page_name=data[i].page_name;
                 var count=data[i].count;
                 if($pos<9)
                 {
                  $(".page_mention_count").append('<div><button type="button" style="margin:0.2rem;width:100%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_page_mention_show btn_website_mention" value="'+page_name+'"><span style="float:left">'+page_name+'</span><span class="label label-light-info" style="padding:1px 5px;margin-left:5px;float:right"">'+count+'</span></button></div>');
                 }
           
                 else if($pos==9)
                 {
                   $(".page_mention_count").append('<div><button type="button" style="margin:0.2rem;width:100%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_page_mention_show btn_website_mention" value="'+page_name+'"><span style="float:left">'+page_name+'</span><span class="label label-light-info" style="padding:1px 5px;margin-left:5px;float:right">'+count+'</span></button></div>');
                     $(".page_mention_count").append('<div style="padding-left:1rem"><a style="font-weight: 700;font-size:15px" class="text-info" href="javascript:void(0)" id="see_more">see more &gt;&gt;</a></div>');
                 }
                 else
                 {
                   $(".page_mention_count").append('<div><button type="button" style="margin:0.2rem;width:100%;display:none" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_page_mention_hide btn_website_mention" value="'+page_name+'"><span style="float:left">'+page_name+'</span><span class="label label-light-info" style="padding:1px 5px;margin-left:5px;float:right">'+count+'</span></button></div>');
                 }
                if($pos == $arry_length-1)
                $(".page_mention_count").append('<div style="padding-left:1rem"><a style="font-weight: 700;font-size:15px;display:none;width:100%" class="text-info" href="javascript:void(0)" id="see_less"><< see less</a></div>');
                $pos +=1;
              }
       
                $("#mention_author_total").text($arry_length==0?'-':$arry_length);
              })
                 .fail(function(xhr, textStatus, error) {
                  //  console.log(xhr.statusText);
                  // console.log(textStatus);
                  // console.log(error);
                
                });
            }



              function requestmentionData(fday,sday)
              {    
                  let main = document.getElementById("fan-growth-chart");
                  let existInstance = echarts.getInstanceByDom(main);
                      if (existInstance) {
                          if (true) {
                              echarts.init(main).dispose();
                          }
                        }
                  var mentionDetailChart = echarts.init(main);
                  $("#fan-growth-spin").show();
                  $("#reaction-spin").show();       
                  var brand_id = GetURLParameter('pid');
                   /* var brand_id = 22;*/
                   // alert (brand_id);
                  $.ajax({
                    type: "GET",
                    dataType:'json',
                    contentType: "application/json",
                    url: "{{route('getmentiondetail')}}", // This is the URL to the API
                    data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'day',keyword_group:$('#hidden-chip').val()}
                  })
                  .done(function( data ) {//alert(data);
                     mention_total=0;

                     mentions=[];
                     mentionLabel=[];

                    for(var i in data) {//alert(data[0][i].mention);
                      mentions.push(data[i].mention);
                      mentionLabel.push(data[i].periodLabel);
                    }
                    $.each(mentions,function(){mention_total+=parseInt(this) || 0;});
                    option = {
                        color: colors,
                        tooltip: {
                          trigger: 'axis',
                        },
                        // grid: {
                        //     right: '20%'
                        // },
                      /*  toolbox: {
                            feature: {
                                dataView: {show: true, readOnly: false},
                                restore: {show: true},
                                saveAsImage: {show: true}
                            }
                        },*/
                     toolbox: {
                            show : true,
                            feature : {
                                mark : {show: false},
                                dataView : {show: false, readOnly: false},
                                magicType : {show: true, type: ['line','bar']},
                                restore : {show: true},
                                saveAsImage : {show: true}
                            }
                        },
                      legend: {
                          data:['Mentions'],
                            //           formatter: function (name) {
                            //             if(name === 'Mentions')
                            //             {
                            //                  return name + ': ' + mention_total;
                            //             }
                            //             return name  + ': ' + reactions_total;

   
                            // }
                        },
                        xAxis: [
                            {
                                type: 'category',
                                axisTick: {
                                    alignWithLabel: true
                                },
                                             axisLabel: {
                        formatter: function (value, index) {
                        // Formatted to be month/day; display year only in the first label
                        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        var date = new Date(value);
                        var texts = [date.getFullYear(), monthNames[date.getMonth()],date.getDate()];
                        return texts.join('-');
                    },
                  rotate:45
                },
                  data: mentionLabel
               }
             ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Mentions',
                      /*  min: 0,
                        max: 250,
                        position: 'left',
                        axisLine: {
                            lineStyle: {
                                color: colors[0]
                            }
                        }/*,
                        axisLabel: {
                            formatter: '{value}'
                        }*/
                         /* axisLabel: {
                    formatter: function (e) {
                        return kFormatter(e);
                    }*/
                
                   }
                ],
              series: [
                  {
                      name:'Mentions',
                      type:'line',
                      smooth: 0.3,
                      color:colors[0],
                      barMaxWidth:30,
                      data:mentions
                  }
              ]
          };
          $("#fan-growth-spin").hide();
              mentionDetailChart.setOption(option, true), $(function() {
              // function resize() {
              //     setTimeout(function() {
              //         mentionDetailChart.resize()
              //     }, 100)
              // }
              // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
          });
          mentionDetailChart.on('click', function (params) {
             // console.log(params);
             // console.log(params.name); // xaxis data = 2018-08
             // console.log(params.seriesName); //bar period name ="Positive"
             // console.log(params.value);//count
                var hidden_kw  = $('#hidden-chip').val();
                var default_kw = $('.hidden-kw').val();

                window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate() +"&period="+params.name+"&keyword_group="+$('#hidden-chip').val()+"&hidden_kw="+hidden_kw+"&default_kw="+default_kw, '_blank');
             
          });

          //requestmentionReactData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
 
      })
      .fail(function() {
        // If there is no communication between the server, show an error
     //  alert( "error occured" );
      });
    }

      function InboundSentimentDetail(fday,sday){//alert(fday),alert(sday);
      //alert("hihi");
  
        let main = document.getElementById("fan-growth-chart");
        let existInstance = echarts.getInstanceByDom(main);
            if (existInstance) {
                if (true) {
                    echarts.init(main).dispose();
                }
            }


        var sentiDetailChart = echarts.init(main);

            $("#fan-growth-spin").show();
              
            $.ajax({
              type: "GET",
              dataType:'json',
              contentType: "application/json",
              url: "{{route('getMentionSentiDetail')}}", // This is the URL to the API
              data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),periodType:'month',keyword_group:$('#hidden-chip').val() }
            })
            .done(function( data ) {
             
              var positive = [];
              var negative = [];
              var neutral=[];
              var sentimentLabel = [];

              var positive_total=0;
              var negative_total=0;
              var neutral_total=0;
              var all_total=0;


              for(var i in data) {//alert(data[i].mentions);
              positive.push(data[i].positive);
              negative.push(data[i].negative);
              neutral.push(data[i].neutral);
              sentimentLabel.push(data[i].periodLabel);
              
            }
              $.each(positive,function(){positive_total+=parseInt(this) || 0;});
              $.each(negative,function(){negative_total+=parseInt(this) || 0;});
              $.each(neutral,function(){neutral_total+=parseInt(this) || 0;});
              all_total=positive_total+negative_total+neutral_total;
              var positive_percentage=parseInt((positive_total/all_total)*100);
              var negative_percentage= parseInt((negative_total/all_total)*100);
              var neutral_percentage =  parseInt((neutral_total/all_total)*100);

              positive_percentage = isNaN(positive_percentage)?0:positive_percentage;
              negative_percentage = isNaN(negative_percentage)?0:negative_percentage;
              neutral_percentage = isNaN(neutral_percentage)?0:neutral_percentage;
              option = {
                color:colors,

                tooltip : {
                        trigger: 'axis',
                           /*formatter: function (params) {
                      var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
                      let rez = '<p>' + params[0].name + '</p>';
                      console.log(rez); //quite useful for debug
                      params.forEach(item => {
                           console.log("hihi"); 
                          console.log(item);
                          console.log(item.series.color); //quite useful for debug
                          var xx = '<p>'   + colorSpan(item.series.color) + ' ' + item.seriesName + ': ' + item.data  + '</p>'
                          rez += xx;
                      });

                      return rez;
                  }*/

                 },

                legend: {
                    data:['Positive','Negative'],
                   formatter: function (name) {
                    if(name === 'Positive')
                    {
                         return name + ': ' + positive_total;
                    }
                    return name  + ': ' + negative_total;
                  }
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: false},
                        dataView : {show: false, readOnly: false},
                        magicType : {show: true, type: ['line','bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
        calculable : true,
        xAxis : [
        {
           type : 'category',
           axisLabel: {
            formatter: function (value, index) {
          // Formatted to be month/day; display year only in the first label
          const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
           var date = new Date(value);
           var texts = [date.getFullYear(), monthNames[date.getMonth()]];
           return texts.join('-');
         }
    },
     data : sentimentLabel
    }
  ],
  yAxis : [
  {
      type : 'value',
      name: 'count',
  }
  ],
  series : [
  {
      name:'Positive',
      type:'bar',
      data:positive,
      barMaxWidth:30,
      color:colors[2],
      markPoint : {
          data : [
          {type : 'max', name: 'maximum'},
          {type : 'min', name: 'minimum'},

          ]
      }/*,
      markLine : {
          data : [
          {type : 'average', name: 'average'}
          ]
      }*/
  },
  {
    name:'Negative',
  /*  type:effectIndex % 2 == 0 ? 'bar' : 'line',*/
    type:'bar',
    data:negative,
    barMaxWidth:30,
    color:colors[1],
    markPoint : {
      data : [
      {type : 'max', name: 'maximum'},
      {type : 'min', name: 'minimum'}
      ]
  }/*,
  markLine : {
    data : [
    {type : 'average', name : 'average value'}
    ]
}*/
}
]
};

sentiDetailChart.setOption(option, true), $(function() {
    // function resize() {
    //     setTimeout(function() {
    //           sentiDetailChart.resize()
    //     }, 100)
    // }
    // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

$("#fan-growth-spin").hide();
 sentiDetailChart.on('click', function (params) {
   // console.log(params);
   // console.log(params.name); // xaxis data = 2018-08
   // console.log(params.seriesName); //bar period name ="Positive"
   // console.log(params.value);//count
   var pid = GetURLParameter('pid');
   var commentType ='';
   
   if(params.seriesName == 'Positive')
    commentType = 'pos'
    else
   commentType = 'neg'

var default_kw = $('.hidden-kw').val();
   if(params.name !== 'minimum' && params.name !== 'maximum' )
   window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid')+"&SentiType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name+"&keyword_group="+$('#hidden-chip').val()+"&default_kw="+default_kw, '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});
    
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }

   function mention_status(fday,sday)

{

 var brand_id = GetURLParameter('pid');

    $( "#post_spin" ).show();
    $( "#cmt_spin" ).show();
    $("#mention_spin").show();

  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getMentionStatus')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,keyword_group:$('#hidden-chip').val()}
    })
    .done(function( data ) { console.log(data);
     $( "#post_spin" ).hide();
     $( "#cmt_spin" ).hide();
      $("#mention_spin").hide();

        var cmt_total = data[0][0]['total'];
        var cmt_pos =data[0][0]['positive'] ;
        var cmt_neg =data[0][0]['negative'] ;
        var cmt_neutral = data[0][0]['neutral'] ;
        var cmt_NA = data[0][0]['NA'] ;
            cmt_neutral=parseInt(cmt_neutral)+parseInt(cmt_NA);

        var post_total = data[1][0]['total'];
        var post_pos =data[1][0]['positive'] ;
        var post_neg = data[1][0]['negative'] ;
        var post_neutral = data[1][0]['neutral'] ;

        var author_total =data[2][0]['author_total'] ;
        var article_total = data[2][0]['article_total'];
        var ar_pos =data[2][0]['positive'] ;
        var ar_neg =data[2][0]['negative'] ; 
        var ar_neutral = data[2][0]['neutral'] ;
            
        var total =  parseInt(cmt_total)+ parseInt(post_total) + parseInt(article_total) ;
        var all_total=parseInt(cmt_pos)+parseInt(cmt_neg)+parseInt(cmt_neutral)+parseInt(post_pos)+parseInt(post_neg)+parseInt(post_neutral)+parseInt(ar_pos)+parseInt(ar_neg)+parseInt(ar_neutral);

        var pos_total = parseInt(cmt_pos) + parseInt(post_pos)+parseInt(ar_pos);
        var neg_total = parseInt(cmt_neg) + parseInt(post_neg)+parseInt(ar_neg);
        var neutral_total = parseInt(cmt_neutral) + parseInt(post_neutral)+parseInt(ar_neutral);
         

        var pos_pcent=parseFloat((pos_total/all_total)*100).toFixed(2);
        var neg_pcent=parseFloat((neg_total/all_total)*100).toFixed(2);
        var neutral_pcent=parseFloat((neutral_total/all_total)*100).toFixed(2);

        pos_pcent = isNaN(pos_pcent)?'-':pos_pcent;
        neg_pcent = isNaN(pos_pcent)?'-':neg_pcent;
        neutral_pcent = isNaN(neutral_pcent)?'-':neutral_pcent;
        total = isNaN(total)?'-':total;

        
        var all_ar_total=parseInt(ar_pos)+parseInt(ar_neg)+parseInt(ar_neutral);

        var ar_pos_pcent=parseFloat((parseInt(ar_pos)/all_ar_total)*100).toFixed(2);
        var ar_neg_pcent=parseFloat((parseInt(ar_neg)/all_ar_total)*100).toFixed(2);
        var ar_neutral_pcent=parseFloat((parseInt(ar_neutral)/all_ar_total)*100).toFixed(2);

        ar_pos_pcent = isNaN(ar_pos_pcent)?'-':ar_pos_pcent;
        ar_neg_pcent = isNaN(ar_neg_pcent)?'-':ar_neg_pcent;
        ar_neutral_pcent = isNaN(ar_neutral_pcent)?'-':ar_neutral_pcent;
        article_total = isNaN(article_total)?'-':article_total;


        $("#mention_author_total").text(author_total==0?'-':author_total);
        $("#mention_article_total").text(article_total==0?'-':article_total);

      
        $("#article_neg").text(ar_neg_pcent==0?'0 %':ar_neg_pcent.replace(".00", "") + "%");
        $("#article_neutral").text(ar_neutral_pcent==0?'0 %':ar_neutral_pcent.replace(".00", "") + "%");
        $("#article_pos").text(ar_pos_pcent==0?'0 %':ar_pos_pcent.replace(".00", "") + "%");

        $("#article_total").text(article_total==0?'-':article_total);


          $("#mention_total").text(total==0?'-':total);
          $("#mention_pos").text(pos_pcent==0?'0 %':pos_pcent.replace(".00", "") + "%");
          $("#mention_neg").text(neg_pcent==0?'0 %':neg_pcent.replace(".00", "") + "%");
          $("#mention_neutral").text(neutral_pcent==0?'0 %':neutral_pcent.replace(".00", "") + "%");

        var mention_post_total =parseInt(post_pos)+parseInt(post_neg)+parseInt(post_neutral)   
        var post_pos_pcent=parseFloat((parseInt(post_pos)/mention_post_total)*100).toFixed(2);
        var post_neg_pcent=parseFloat((parseInt(post_neg)/mention_post_total)*100).toFixed(2);
        var post_neutral_pcent=parseFloat((parseInt(post_neutral) /mention_post_total)*100).toFixed(2);
        post_pos_pcent = isNaN(post_pos_pcent)?'-':post_pos_pcent;
        post_neg_pcent = isNaN(post_neg_pcent)?'-':post_neg_pcent;
        post_neutral_pcent = isNaN(post_neutral_pcent)?'-':post_neutral_pcent;
        mention_post_total = isNaN(mention_post_total)?'-':mention_post_total;

          $("#post_total").text(post_total==0?'-':post_total);
          $("#post_neg").text(post_neg_pcent==0?'0 %':post_neg_pcent.replace(".00", "") + "%");
          $("#post_neutral").text(post_neutral_pcent==0?'0 %':post_neutral_pcent.replace(".00", "") + "%");
          $("#post_pos").text(post_pos_pcent==0?'0 %':post_pos_pcent.replace(".00", "") + "%");
          $("#top_post_total").text(post_total==0?'-':post_total);


        var mention_cmt_total =parseInt(cmt_pos)+parseInt(cmt_neg)+parseInt(cmt_neutral)   
        var cmt_pos_pcent=parseFloat((parseInt(cmt_pos)/mention_cmt_total)*100).toFixed(2);
        var cmt_neg_pcent=parseFloat((parseInt(cmt_neg)/mention_cmt_total)*100).toFixed(2);
        var cmt_neutral_pcent=parseFloat((parseInt(cmt_neutral) /mention_cmt_total)*100).toFixed(2);

        cmt_pos_pcent = isNaN(cmt_pos_pcent)?'-':cmt_pos_pcent;
        cmt_neg_pcent = isNaN(cmt_neg_pcent)?'-':cmt_neg_pcent;
        cmt_neutral_pcent = isNaN(cmt_neutral_pcent)?'-':cmt_neutral_pcent;
        mention_cmt_total = isNaN(mention_cmt_total)?'-':mention_cmt_total;

          $("#cmt_total").text(cmt_total==0?'-':cmt_total);
          $("#cmt_neg").text(cmt_neg_pcent==0?'0 %':cmt_neg_pcent.replace(".00", "") + "%");
          $("#cmt_neutral").text(cmt_neutral_pcent==0?'0 %':cmt_neutral_pcent.replace(".00", "") + "%");
          $("#cmt_pos").text(cmt_pos_pcent==0?'0 %':cmt_pos_pcent.replace(".00", "") + "%");
          $("#top_comment_total").text(cmt_total==0?'-':cmt_total);


    })
    .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
}
$('input[type=radio][name=options]').change(function() {//alert(this.value);

     if (this.value == 'sentiment') {
        InboundSentimentDetail(GetStartDate(),GetEndDate());
    }
    else if (this.value == 'mention')
    {
        requestmentionData(GetStartDate(),GetEndDate());
    }
    else if (this.value == 'pages')
    {
        PagesMentionCount(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
    }
    else if (this.value == 'webistes')
    {
        WebsiteMentionCount(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
    }
});


//local function
function highLightText(keyForHighLight,message)
{
  keyForHighLight.sort(function(a, b){return b.length - a.length});
  var highligh_string  = message;
  
for(var i in keyForHighLight) {
   highligh_string = highligh_string.replace( new RegExp(keyForHighLight[i], "ig") ,'<span style="background:#FFEB3B">' + keyForHighLight[i] + '</span>')
 }
 return highligh_string;
}

function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

  function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }
          

  });

    </script>
  
    <style type="text/css">

table.fixedHeader-floating {
  clear: both;
  top: 114px !important;
  z-index:5;
   top : 0;
   left:0;
   width: 100%;
/*   margin: 0 auto;
     padding: 0 15px;*/
 /*    overflow-x: hidden;
      overflow-y: auto;*/
  
    box-sizing: border-box;
/*
  background: red;*/
/*  left:150px !important;*/
/*  margin-right:30px;*/
 
/*  width: 80% !important;*/
  /*background: transparent;*/
}
/*table.dataTable thead tr th {
    word-wrap: break-word;
    word-break: break-all;
}*/
table.dataTable tbody tr td {
    word-wrap: break-word;
    word-break: break-all;
}
.myHeaderContent
{
   margin-right:300px;
}
.TopDivPadding
{
  padding-left:10px;
}
.TopDivCardBody
{
  padding-top:1rem;
  padding-bottom:1rem;
}
.TopDivNumber
{
  font-weight: 500;
  font-size: 40px;
}
 #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


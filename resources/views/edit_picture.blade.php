@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

<div class="row page-titles">
  <div class="col-md-5 col-8 align-self-center">
      <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Update Link</h4>

  </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              @if(Session::has('error-message'))
                  <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {{ session('error-message') }}</em></div>
              @endif
               @if(Session::has('success-message'))
                  <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{ session('success-message') }}</em></div>
              @endif
                <form method="POST" action="/edit-picture">
                  @csrf
                      <div class="form-group ">
                        
                         <div class="input-group">
                            <ul class="icheck-list">
                              <li>
                                <input type="radio" class="daily" id="minimal-radio-1" value="temp_inbound_posts" name="schedule" checked="checked">
                                <label for="minimal-radio-1">Pages</label>
                              </li>
                            </ul>
                            <ul class="icheck-list">
                                <li>
                                    <input type="radio" class="weekly" id="minimal-radio-2" value="temp_posts" name="schedule">
                                    <label for="minimal-radio-2">Mention</label>
                                </li>
                            </ul>
                        </div>
                       
                      
                      </div>

                      <div class="form-group">
                        <label for="name" class="control-label">Full link</label>
                          <input id="name" type="text" class="form-control " name="full_link" autofocus>
                      </div>

                      <div class="form-group">
                        <label for="username" class="control-label">Local link</label>
                         <input id="username" type="text" class="form-control"name="local_link" >
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">  Update Link </button>
                      </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>

@endpush


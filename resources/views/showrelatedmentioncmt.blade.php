@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('admin_page_filter')
<li class="nav-item" style="display: none"> 
<div class="btn-group">
        <select id="admin_page_filter" class="form-control custom-select">
        <option value="">Admin Pages</option>
        @if (isset($ownpage))
        @foreach($ownpage as $ownpage)
        <option value="{{$ownpage}}" id="{{$ownpage}}" >{{$ownpage}}</option>
        @endforeach
        @endif
                                                        
                                                   
      </select>
 
                                        </div>
                         </li>
                      
@endsection
@section('content')

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                 <header class="" id="myHeader">
                  <div class="row page-titles">
                   <!--  <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Post</h4>
                      
                    </div> -->
                      <div class="col-md-4 col-8 align-self-center">
                        <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                          <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#comment" role="tab"><span id="title_comment"> Comments</span></a> </li>
                         
                     
                                
                            </ul>
                       <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
                    </div>
                    <div class="col-md-8 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
  </header>
             
<div class="row" >
   <div class="col-lg-9">
                       
                       
                        <div class="card">
                               <div class="tab-content">
                        
                       <div class="tab-pane active" id="comment" role="tabpanel">
                       
                       
                            <div class="card-body">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div class="table-responsive">
                               <table id="tbl_comment" class="table" style="width:100%;">
                              <thead>
                                <tr>
                                  <th></th>
                                
                                </tr>
                              </thead>

                            </table>
                            </div>
                          </div>
                   
                
                   
                  </div>
                     
                  </div>
                        </div>
                    </div>

                     <div class="col-lg-3">
                      <div class="card earning-widget" style="height:230px">
                            <div class="card-header">
                     
                                <h4 class="card-title m-b-0">Sentiment</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                          
                                 <div class="form-group">
                                                  
                                                    <div class="input-group">
                                                        <ul class="icheck-list sentiment-list" style="padding-top:0px">
                                                        <li>
                                 <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="all" id="senti-all"> 
                                  <label  for="senti-all" style="padding-left:30px;font-weight:500;font-size:12px">all</label>
                                  </li>
                                   <li>
                                  <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="pos" id="senti-pos"> 
                                   <label class="text-success" for="senti-pos" style="padding-left:30px;font-weight:500;font-size:12px">positive</label>
                                  </li>
                                  <li>
                                  <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neg" id="senti-neg"> 
                                  <label class="text-danger" for="senti-neg" style="padding-left:30px;font-weight:500;font-size:12px">negative</label>
                                  </li>
                                  <li> 
                                  <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neutral" id="senti-neutral"> 
                                  <label class="text-info"   for="senti-neutral" style="padding-left:30px;font-weight:500;font-size:12px">neutral</label>
                                  </li>
                                                           
                                                        </ul>
                                                    </div>
                                                </div>
                            </div>
                        </div>
                          <div class="card earning-widget" style="height:450px;display: none">
                            <div class="card-header">
                     
                                <h4 class="card-title m-b-0">Keywords</h4>
                            </div>
                            <div class="card-body b-t collapse show" style="overflow-y:scroll;height:450px">
                          
                                 <div class="form-group">
                                              <div class="input-group">
                                                        <ul class="icheck-list keyword-list" style="padding-top:0px">
                                                      
                                                           
                                                        </ul>
                                                    </div>
                                                </div>
                            </div>
                        </div>
                
                  
                </div>
                    </div>
                  
                 <div id="show-image" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div align="center" class="modal-body large_img">
                                                 
                                              </div>

                                          </div>
                                        
                                      </div>
                                
                   </div>


                <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="postModal">Post Detail</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body post_data">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button style="display: none" type="button" id="btnSeeComment" class="btn btn-info waves-effect text-left" data-dismiss="modal">See Comments</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
               
         
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
   <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
     <script src="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
     <script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
       <script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
/*global mention*/
var mention_total;
var mentionLabel = [];
var mentions = [];
/*Bookmark Array*/
var bookmark_array=[];
var bookmark_remove_array=[];
/*global sentiment*/
var positive = [];
var negative = [];
var sentimentLabel = [];
var positive_total=0;
var negative_total=0;
var  colors=["#1e88e5","#dc3545","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

$(document).ready(function() {



    //initialize
 $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

      var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

    startDate = moment().subtract(1, 'month');
    endDate = moment();

           $('.singledate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        },function(date) {
          endDate=date;
          var id =$('input[type=radio][name=period-radio]:checked').attr('id');
          if(id==="period-week")
         {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
         }
         else
         {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
         }
         // alert(startDate);
         // alert(endDate);
         var admin_page=$("#admin_page_filter").val();

         ChooseDate(startDate,endDate,admin_page,'');
       
      });



 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Dashboard'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}


   function ChooseDate(start, end,filter_senti,filter_keyword,comefrom) {//alert(start);
  

          $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
           if(comefrom == 'post')
            getAllMentionComment('','',filter_senti,filter_keyword);
            else

         getAllMentionComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_senti,filter_keyword);
        // tagCount();
        
        }

        function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}
 $(window).resize( function() {

     //  new $.fn.dataTable.FixedHeader( oTable );
   //  $('#tbl_post').css('width', '100%');
      //$("#tbl_post").DataTable().columns.adjust().draw();
      //$('table.fixedHeader-floating').css('left', '200px');
  });

 function getAllMentionComment(fday,sday,filter_senti='',filter_keyword='')
    {
      // alert(filter_tag);
      $(".popup_post").unbind('click');
      $(".edit_predict").unbind('click');
      $("#add_tag").unbind('click');
      $("#comment_hide").unbind('click');
      var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });
      var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
            });
      
      
 var oTable = $('#tbl_comment').DataTable({

        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": true,
        "destroy": true,
        "ordering": false   ,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        
        "ajax": {
          "url": '{{ route('getMentionComment') }}',
          "dataSrc": function(res){
              var count = res.data.length;
              //alert(count);
              document.getElementById('title_comment').innerHTML = 'Comments ('+res.recordsTotal+")";
              return res.data;
            },
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
          
         "data": {
            "fday": fday,
            "sday": sday,
            "brand_id": GetURLParameter('pid'),
            "period" :  GetURLParameter('period'),
            "post_id": GetURLParameter('post_id'),
            "tsearch_senti":filter_senti
          
           
          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
       
        },
     drawCallback: function() {
     $('.select2').select2();
  },

        columns: [
        {data: 'post_div', name: 'post_div',"orderable": false},
    
        ]
        
      }).on('change', '.edit_senti', function (e) {

  if (e.handled !== true) {

    var senti_id = $(this).attr('id');
    var post_id  = senti_id.replace("sentiment_","");
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    

    $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
    if(sentiment == 'pos')
     $("#sentiment_"+post_id).addClass('text-success');
    else if (sentiment == 'neg')
      $("#sentiment_"+post_id).addClass('text-red');
    else
      $("#sentiment_"+post_id).addClass('text-warning');

    
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,type:'mention'},
         success: function(response) {// alert(response)
          if(response>=0)
          {
        //         swal({   
        //     title: "Updated!",   
        //     text: "Done!",   
        //     timer: 1000,   
        //     showConfirmButton: false 
        // });
         var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Sentiment Changed!!');
                        x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });

          e.handled = true;
       }
     
    
        
  }).on('click', '.comment_hide', function (e) {//alert("hihi");
  if (e.handled !== true) {
    var attr_id = $(this).attr('id');
    var comment_id  = attr_id.replace("hide_","");
    //alert(comment_id);
     var answer = confirm("Are you sure to hide this comment?")
                if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetHideMention") }}',
         type: 'POST',
         data: {id:comment_id,brand_id:GetURLParameter('pid'),table_type:'comment'},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            $("#"+comment_id).closest('tr').remove();
          }
        }
            });
                }
                else
                {

                }
     
     
        }
         e.handled = true;
       }).on('click', '.popup_img', function (e) {
    if (e.handled !== true) {

         var id = $(this).attr('id');
         var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
// img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%'' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 
         }
       
      
          
    });
     
    
        
  }

  getKeyword()

function getKeyword()
{
  
     
    
    $(".keyword-list").empty();
    var brand_id = GetURLParameter('pid');
        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getKeywordsByGroup')}}", // This is the URL to the API
      data: {brand_id:brand_id,keyword_group:GetURLParameter('keyword_group')}
    })
    .done(function( data ) {//console.log(data);
          
for(var i in data) 
        {
          if(i<10)
          {
            $(".keyword-list").append('<li class="see-li">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
            ' </li>');
          }
          else if (i==10)
          {
            $(".keyword-list").append('<li class="see-li">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
            ' </li>  <li class="see-li" id="more"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see more</a></li>');
               
          }
          else
          {
             $(".keyword-list").append('<li class="hidden-li" style="display:none">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
            ' </li><br/>');

          }
                         
         


        }
        $(".keyword-list").append('<li class="hidden-li" id="less" style="display:none"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see less</a></li>');
   
    

    })
     .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    
    });

        
}

$(document).on('click', '#more', function () {
   $('#more').css('display','none');
    $('.hidden-li').css('display','inline');
});
$(document).on('click', '#less', function () {
  $('#less').css('display','none');
    $('.hidden-li').css('display','none');
     $('#more').css('display','inline');
});

$(document).on('click', '.keyword', function () {
   CallFunction();
});
$(document).on('click', '.sentimentlist', function () {
CallFunction();
});


function CallFunction()
{
     var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
              });
      var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
              });
  
   getAllMentionComment(GetStartDate(),GetEndDate(),filter_senti_arr.join("| "),filter_keyword_arr.join("| "));
}
$('#senti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.keyword)").click(function () {
    $("#senti-all").prop("checked", $("input:checkbox:not(.keyword):checked").length == 4);
  });
    var permission = GetURLParameter('accountPermission');

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end,label) {//alert(label);

                          if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) 
          
            {
                 swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }


            else{
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
              });
      var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
              });

       ChooseDate(startDate,endDate,filter_senti_arr.join("| "),filter_keyword_arr.join("| "));
             }
           }
           else
           {
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
              });
      var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
              });

       ChooseDate(startDate,endDate,filter_senti_arr.join("| "),filter_keyword_arr.join("| "));
           }
      });
var fdate = new Date(GetURLParameter('fday'));
startDate=moment(fdate);
var sdate = new Date(GetURLParameter('sday'));
endDate=moment(sdate);
ChooseDate(startDate,endDate,GetURLParameter('SentiType'),'post');

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   window.dispatchEvent(new Event('resize'));

 });

hidden_div();
// tagCount();
function tagCount()
{
    var fday=GetStartDate();
    var sday=GetEndDate();
    $("#tbl_tag_count tbody").empty();
    var brand_id = GetURLParameter('pid');
        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,limit:'no' }
    })
    .done(function( data ) {//console.log(data);
for(var i in data) 
        {
           var tagLabel=data[i].tagLabel;
           var tagId=data[i].tagId;
           var tagCount=data[i].tagCount;
           $("#tbl_tag_count tbody").append(' <tr style="border-bottom:1px solid rgba(120, 130, 140, 0.13)"> '+
                                            ' <td style="width:40px" span="2"> <button type="button" id="'+tagId+'" class="btn '+
                                            ' btn-rounded btn-block btn-green btn_tag">'+tagLabel+'</button></td> '+
                                            ' <td></td> '+
                                            '<td class="text-right"> '+
                                            ' <span class="label label-light-info">'+tagCount+'</span></td> '+
                                            ' </tr>')

        }

    })
     .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    
    });
}

$("#add_new_tag").click(function(){
//alert("hihi");
  var name = $("#name").val();
  var nameLength = name.length;
  if(name == '')
  {
    $(".invalid-name").append("<strong class='text-danger'>Please fill out name!</strong>");
    return false;
  }
  else if (nameLength > 80 )
    {
    $(".invalid-name").append("<strong class='text-danger'>The name may not be greater than 80 characters!</strong>");
    return false;
  }
 // var keywords = $("#keywords").val();
 var keywords = '';
  var brand_id = $("#brand_id").val();
//   if(keywords == '')
//   {
//   $(".invalid-keyword").append("<strong>Please fill out keyword</strong>");
//   return false;
// }
   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("quick_tag") }}',
         type: 'POST',
         data: {name:name,keywords:keywords,brand_id:brand_id},
         success: function(response) {
          //alert(response);
          if(response !== "exist")
          {
             $('.edit_tag')
         .append($("<option></option>")
                    .attr("value",name)
                    .text(name)); 
                    var x = document.getElementById("snackbar")
                    $("#snack_desc").html('');
                    $("#snack_desc").html('Tag added successfully!!');
    x.className = "show";

    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
        //             swal({   
        //     title: "Success!",   
        //     text: "Tag " + name + " is already added!" ,   
        //     timer: 500 ,   
        //     showConfirmButton: false 
        // });
          }

          else if (response == "exist")
          {
            var x = document.getElementById("snackbar")
                    $("#snack_desc").html('');
                    $("#snack_desc").html('This tag already exist !!');
    x.className = "show";

    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
     
        

//           var table = $('#tbl_comment').DataTable();
//           table.rows().eq(0).each( function ( index ) {
//             howmany4D = document.getElementById('tags_362097417670486_362293134317581');
//          console.log(howmany4D);

//     var row = table.row( index );
 
//     var data = row.data();
//     console.log(data['id']);
//     if(data['id']=='362097417670486_362160487664179')
//     alert('#tags_'+data['id']);
//     $('#tags_'+data['id'])
//          .append($("<option></option>")
//                     .attr("value",name)
//                     .text(name)); 
//     // ... do something with data(), or row.node(), etc
// } );

        
//         // Draw once all updates are done
//         table.draw();
          
             $("#name").val('');
             $("#keywords").val('');
     $('#show-add-tag').modal('toggle');
         
        }
      });

});

  $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
       });



$(document).on('click','.dropdown-menu a',function(){
var row_id=this.id;
row_id = row_id.substring(row_id.indexOf('_')+1);
// alert(row_id);
      $("#btnaction_"+row_id+":first-child").text($(this).text());
      $("#btnaction_"+row_id+":first-child").val($(this).text());
      // $("#btnaction_"+row_id).removeClass('btn-red');
      // $("#btnaction_"+row_id).removeClass('btn-success');
      // if($(this).text() === "Require Action")
      // {
      // $("#btnaction_"+row_id).addClass('btn-red');
      // }
      // else
      // {
      //   $("#btnaction_"+row_id).addClass('btn-success');
      // }

       $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setActionUpdate") }}',
         type: 'POST',
         data: {id:row_id,brand_id:GetURLParameter('pid'),action_status:$(this).text()},
         success: function(response) { //alert(response)
          if(response>=0)
          {
       
          }
        }
            });

      //save action status and taken person in database
});
  $("#btnSeeComment").click( function()
           {
                 var post_id=$("#popup_id").val();
                var tab_name = $('.nav-tabs .active').text();
               // alert(new String(tab_name).valueOf());
              if(String(tab_name).trim()  == 'Mentions')
              {
                //alert("hihi");
                window.open("{{ url('relatedcompetitorcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
              }
              else
              {
                window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
              }
            
              
           }
        );



//local function
function highLightText(keyForHighLight,message)
{
  keyForHighLight.sort(function(a, b){return b.length - a.length});
  var highligh_string  = message;

for(var i in keyForHighLight) {
   highligh_string = highligh_string.replace( new RegExp(keyForHighLight[i], "ig") ,'<span style="background:#FFEB3B">' + keyForHighLight[i] + '</span>')
 }
 return highligh_string;
}

function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

  function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

        

  });
    </script>
    <style type="text/css">

table.fixedHeader-floating {
  clear: both;
  top: 114px !important;
  z-index:5;
   top : 0;
   left:0 ;
   width: 100%;
/*   margin: 0 auto;
     padding: 0 15px;*/
 /*    overflow-x: hidden;
      overflow-y: auto;*/
  
    box-sizing: border-box;
/*
  background: red;*/
/*  left:150px !important;*/
/*  margin-right:30px;*/
 
/*  width: 80% !important;*/
  /*background: transparent;*/
}

.myHeaderContent
{
   margin-right:300px;
}

  .bootstrap-tagsinput{
    width: 100%
  }

/*.s_topbar {
    position: relative;
    z-index: 50;
    -webkit-box-shadow: 5px 0px 10px rgba(0, 0, 0, 0.5);
    box-shadow: 2px 0px 2px rgba(0, 0, 0, 0.5);
}*/
#snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: -125px;
  background-color: #7e7979;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 2;
  left: 50%;
  bottom: 30px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}
table.dataTable tbody tr td {
    word-wrap: break-word;
    word-break: break-all;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;} 
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;} 
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}
    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


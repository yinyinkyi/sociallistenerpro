@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

  <header class="" id="myHeader">
    <div class="row page-titles">
      <div class="col-md-3 col-8 align-self-center">
        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Keywords Compare</h4>
      </div>
      <div class="col-md-9 col-4 align-self-center">
        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
          <div class="d-flex m-r-20 m-l-10 hidden-md-down">
            <div class="chart-text m-r-10">
              <button type="button" id="btn_add_keyword" class="btn waves-effect waves-light btn-block btn-green">More Keyword Group</button>
            </div>
            <div class="chart-text m-r-10" >
                  <button type="button" id="btn_remove_keyword" class="btn waves-effect waves-light btn-block btn-green">Clear All</button>
            </div>
            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
               <input type='text' class="form-control dateranges" style="datepicker" />
                <div class="input-group-append">
                  <span class="input-group-text">
                    <span class="ti-calendar"></span>
                  </span>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div id="add-keywordGroup" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">           
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Add Keyword Group</h4>
          <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <div class="message-box">
            <div class="form-group has-info">
              <label class="control-label">Keyword Group</label>
                <select id="keywordgroup_id" class="form-control custom-select">
                </select>
                <small class="form-control-feedback"> Choose a Keyword Group to compare </small>
            </div>
            <hr>          
            <button type="button" id="btn_add_keygroup" class="btn btn-green"><i class="fa fa-check"></i> Add </button>
          </div>
        </div>                              
      </div>
    </div>
  </div>
                
  <div class="row" class="row s-topbar-u-div" id="page-div-1">
    <input type="hidden" value="{{ auth()->user()->default_keyword }}" class ="hidden-kw" name="hidden-kw"/>
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
              <h4 class="card-title">Mention</h4>
                <div style="display:none"  align="center" style="vertical-align: top;" id="mention-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
            <div id="mention-chart" style="width:100%;height:300px"></div>  
          </div>
        </div>
      </div>
  </div>
  <div class="row" class="row s-topbar-u-div" id="page-div-1">
      <div class="col-lg-7">
          <div class="card">
              <div class="card-body">
                  <h4 class="card-title">Sentiment</h4>
                    <div style="display:none"  align="center" style="vertical-align: top;" id="sentiment-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                <div id="sentiment-chart" style="width:100%;height:300px"></div>  
              </div>
          </div>
      </div>
        <div class="col-lg-5">
          <div class="card">
              <div class="card-body">
                  <h4 class="card-title">Mention</h4>
                    <div style="display:none"  align="center" style="vertical-align: top;" id="mention-pie-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                <div id="mention-pie-chart" style="width:100%;height:300px"></div>  
              </div>
          </div>
      </div>
  </div>
  <div class="row" class="row s-topbar-u-div" id="page-div-1">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Mentions By Media Tier</h4>
                  <div style="display:none"  align="center" style="vertical-align: top;" id="tier-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
              <div id="pageTier-chart" style="width:100%;height:300px"></div>  
            </div>
        </div>
    </div>
</div>
              
              
             
                
       
@endsection
@push('scripts')
    <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   
    <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript')}}" defer ></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript')}}" defer></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
var labelDate;
var colors=[ "#4267b2","#382448","#01ad9d","#cb73a9","#a1c652","#7b858e","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];
var colors_senti=["#28a745","#fb3a3a","#ffb22b"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/
$(document).ready(function() {
  //generate legend data
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });


var compare_count=0;
var Legend_Data=[];var keygroup_id_arr=[];
var mentionseriesList=[];var mentionLabel = [];var arr_mention_total=[];var arr_mention_label=[];
var mentionpielist=[];
var arr_mention=[];
var  senti_pos=[];var senti_neg=[];var sentiLabel=[];
var tier1=[];var tier2=[];var tier3=[];var tierLabel=[];

    startDate = moment().subtract(1, 'month');
    endDate = moment();
    var periodType= '';
       
            if(periodType == '')
            {
              periodType='month';
            }
    var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};



function Rebind_KeywordGroup()
{//alert("popular");
// alert()
    var brand_id = GetURLParameter('pid');
    //var brand_id = 35   ;
    $( "#popular-spin" ).show();
    $("#keywordgroup_id").empty();
    $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getallKeywords')}}", // This is the URL to the API
        data: { 
          pid:brand_id,
          }
      })
    .done(function( data ) {//   <option value="">Choose</option>
      // alert(data);
      // alert(data[0]);
           $('#keywordgroup_id').append($('<option>', {
            value: '',
            text: 'Choose'
        }));
     for(var i in data) {

      $('#keywordgroup_id').append($('<option>', {
            value:data[i],
            text:data[i]
        }));
     }
    })
    .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

function Bind_KeywordGroup()
{//alert("popular");
// alert()
    var brand_id = GetURLParameter('pid');
    //var brand_id = 35   ;
    $( "#popular-spin" ).show();
    $("#keywordgroup_id").empty();
    $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getallKeywords')}}", // This is the URL to the API
        data: { 
          pid:brand_id,
          }
      })
    .done(function( data ) {//   <option value="">Choose</option>
      // alert(data);
      // alert(data[0]);
           $('#keywordgroup_id').append($('<option>', {
            value: '',
            text: 'Choose'
        }));
     for(var i in data) {

      $('#keywordgroup_id').append($('<option>', {
            value:data[i],
            text:data[i]
        }));
     }
      
     keygroup_id_arr.push(data[0]);
     Legend_Data.push(data[0]);
     ChooseDate(startDate,endDate,data[0],0,labelDate);
    
     
    })
    .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
 
Bind_KeywordGroup();
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Comparison'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();
function ChooseDate(start,end,keywordGroup,sr_of_brand,label)
{
   $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
                
         // alert(start);
         var date_preset='this_month'; 
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

         MentionDetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),keywordGroup,sr_of_brand);
        SentimentDetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),keywordGroup,sr_of_brand);
        MentionTierCompare(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),keywordGroup,sr_of_brand);
        
}
function PostSentiment(fday,sday,brand_id,post_sr_of_brand)
    {
      var PostSentiChart = echarts.init(document.getElementById('post-sentiment-chart'));
      var PostSentiChartQty = echarts.init(document.getElementById('post-sentiment-qty-chart'));
      $('#post_spin').show();
     // $("#fan-growth-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id}
    })
    .done(function(data) {
       
                var ov_total=parseInt(data[2][0]['ov_positive'])+parseInt(data[2][0]['ov_negative'])+parseInt(data[2][0]['ov_neutral'])
                var pcent_positive=parseFloat((parseInt(data[2][0]['ov_positive'])/ov_total)*100).toFixed(2);
                pcent_positive = isNaN(pcent_positive)?0:pcent_positive;
                if(pcent_positive == 0)
                pcent_positive='-';
                 arr_ov_positive.push(pcent_positive);
                arr_ov_qty_positive.push(parseInt(data[2][0]['ov_positive']));
                var pcent_negative=parseFloat((parseInt(data[2][0]['ov_negative'])/ov_total)*100).toFixed(2);
                pcent_negative = isNaN(pcent_negative)?0:pcent_negative;
                if(pcent_negative == 0)
                pcent_negative='-';
                 arr_ov_negative.push(pcent_negative);
                arr_ov_qty_negative.push(parseInt(data[2][0]['ov_negative']));
                var pcent_neutrual=parseFloat((parseInt(data[2][0]['ov_neutral'])/ov_total)*100).toFixed(2);
                pcent_neutrual = isNaN(pcent_neutrual)?0:pcent_neutrual;
                if(pcent_neutrual == 0)
                pcent_neutrual='-';
                 arr_ov_neutral.push(pcent_neutrual);
                arr_ov_qty_neutral.push(parseInt(data[2][0]['ov_neutral']));
         
                if(jQuery.inArray(Legend_Data[post_sr_of_brand], OverallLabel)=='-1')
                OverallLabel.push(Legend_Data[post_sr_of_brand]);

        option= null;
        option_qty=null;
option = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    grid: {
      left: '3%',
      right: '10%',
      // bottom: '3%',
      containLabel: true
    },
    xAxis:  {
      type: 'value',
      name:'Senti Percent',
       axisLabel: {
            formatter: '{value}%'
        },
        max:100
    },
    // xAxis:  {
    // //   scale: true,
    //   type: 'value',
    //   name:'Senti Percent',
    //   min:0,
    //     //max: 5000,
    //     interval: 200
    // },
    yAxis: {
      type: 'category', 
      name:'Page',
      data:OverallLabel,

},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_neutral
}
]
};
option_qty = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    grid: {
      left: '3%',
      right: '10%',
      bottom: '3%',
      containLabel: true
    },
    
    xAxis:  {
    //   scale: true,
      type: 'value',
      name:'Quantity',
      min:0,
        //max: 5000,
        interval: 200
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      data:OverallLabel,

},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_qty_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_qty_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_qty_neutral
}
]
};
 
    $( "#post_spin" ).hide();
    
    PostSentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            PostSentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
PostSentiChartQty.setOption(option_qty, true), $(function() {
    function resize() {
        setTimeout(function() {
            PostSentiChartQty.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
     var arrayLength=brand_id_arr.length;
     post_sr_of_brand = post_sr_of_brand +1;
     if(post_sr_of_brand<arrayLength )
     {
      PostSentiment(fday,sday,brand_id_arr[post_sr_of_brand],post_sr_of_brand);
      $("#fan-growth-spin").show();

     }
     else
     {
       $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function CommentSentiment(fday,sday,brand_id,comment_sr_of_brand)
    {
      var CommentSentiChart = echarts.init(document.getElementById('comment-sentiment-chart'));
      var CommentSentiChartQty = echarts.init(document.getElementById('comment-sentiment-qty-chart'));
      $('#comment_spin').show();
      //$("#fan-growth-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id}
    })
    .done(function(data) {
       
                var comment_total=parseInt(data[1][0]['cmt_positive'])+parseInt(data[1][0]['cmt_negative'])+parseInt(data[1][0]['cmt_neutral'])+parseInt(data[1][0]['cmt_NA']);
                 // alert(comment_total);
                var neutralTotal=parseInt(data[1][0]['cmt_neutral'])+parseInt(data[1][0]['cmt_NA']);
                var pcent_positive=parseFloat((parseInt(data[1][0]['cmt_positive'])/comment_total)*100).toFixed(2);
                pcent_positive = isNaN(pcent_positive)?0:pcent_positive;
                if(pcent_positive == 0)
                pcent_positive='-';
                arr_cmt_positive.push(pcent_positive);
                arr_cmt_qty_positive.push(parseInt(data[1][0]['cmt_positive']));
                var pcent_negative=parseFloat((parseInt(data[1][0]['cmt_negative'])/comment_total)*100).toFixed(2);
                pcent_negative = isNaN(pcent_negative)?0:pcent_negative;
                if(pcent_negative == 0)
                pcent_negative='-';
                 arr_cmt_negative.push(pcent_negative);
                 arr_cmt_qty_negative.push(parseInt(data[1][0]['cmt_negative']));
                var pcent_neutrual=parseFloat((neutralTotal/comment_total)*100).toFixed(2);
                pcent_neutrual = isNaN(pcent_neutrual)?0:pcent_neutrual;
                if(pcent_neutrual == 0)
                pcent_neutrual='-';
                arr_cmt_neutral.push(pcent_neutrual);
                arr_cmt_qty_neutral.push(neutralTotal);
                 
                if(jQuery.inArray(Legend_Data[comment_sr_of_brand], CommentLabel)=='-1')
                CommentLabel.push(Legend_Data[comment_sr_of_brand]);

        option= null;
        option_qty= null;
option = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    grid: {
      left: '3%',
      right: '10%',
      bottom: '3%',
      containLabel: true
    },
    xAxis:  {
      type: 'value',
      name:'Senti Percent',
       axisLabel: {
            formatter: '{value}%'
        },
        max:100
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      data:CommentLabel
},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_neutral
}
]
};
option_qty = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    grid: {
      left: '3%',
      right: '10%',
      bottom: '3%',
      containLabel: true
    },
    xAxis:  {
    //   scale: true,
      type: 'value',
      name:'Quantity',
      min:0,
        //max: 5000,
        interval: 200
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      data:CommentLabel
},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_qty_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_qty_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_qty_neutral
}
]
};
 
  $( "#comment_spin" ).hide();

    CommentSentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            CommentSentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
CommentSentiChartQty.setOption(option_qty, true), $(function() {
    function resize() {
        setTimeout(function() {
            CommentSentiChartQty.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

      var arrayLength=brand_id_arr.length;
     comment_sr_of_brand = comment_sr_of_brand +1;
     if(comment_sr_of_brand<arrayLength )
     {
      CommentSentiment(fday,sday,brand_id_arr[comment_sr_of_brand],comment_sr_of_brand);
      $("#fan-growth-spin").show();
     }
     else
     {
      $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function PageSummary(fday,sday,brand_id,summary_sr_of_brand)
    {
      var SummaryChart = echarts.init(document.getElementById('page-summary-chart'));
      $('#summary_chart_spin').show();
      //$("#fan-growth-spin").show();
     
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id}
    })
    .done(function(data) {
       
        
      
                post.push(data[0][0]['total_post']);
                share.push(data[0][0]['shared']);
                comment.push(data[1][0]['total_comment']);
          
                if(jQuery.inArray(Legend_Data[summary_sr_of_brand], summaryLabel)=='-1')
                  summaryLabel.push(Legend_Data[summary_sr_of_brand]);


        option= null;
option = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        // axisPointer: {
        //     type: 'cross'
        // },
      
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '10%',
    bottom:  '5%',
            containLabel: true
        },
         legend: {
        data:['post','comment','share'],
      
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name: 'Page Name',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
        
            data: summaryLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Count',
            scale:true,
            position: 'left',
         
            axisLabel: {
        formatter: function (e) {
            return nFormatter(e,1,1);
        }
    }
    
     }
    ],
    series: [{
            name:'post',
            type:'bar',
            smooth: 0.2,
            color:colors[0],
            barMaxWidth:30,
            barMinHeight:2,
            data:post
          
        },{
            name:'comment',
            type:'bar',
            smooth: 0.2,
            color:colors[1],
            barMaxWidth:30,
            barMinHeight:2,
            data:comment
          
        },{
            name:'share',
            type:'bar',
            smooth: 0.2,
            color:colors[2],
            barMaxWidth:30,
            barMinHeight:2,
            data:share
          
        }]
};
 
    $( "#summary_chart_spin" ).hide();
   
    SummaryChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            SummaryChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
      var arrayLength=brand_id_arr.length;
     summary_sr_of_brand = summary_sr_of_brand +1;
     if(summary_sr_of_brand<arrayLength )
     {
      PageSummary(fday,sday,brand_id_arr[summary_sr_of_brand],summary_sr_of_brand);
      $("#fan-growth-spin").show();

     }
     else
     {
       $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function SentimentDetail(fday,sday,keywordGroup,sr_of_brand)
    {
  
      let main = document.getElementById("sentiment-chart");
      let existInstance = echarts.getInstanceByDom(main);
        if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }
var SentiChart = echarts.init(main);
      $('#sentiment-spin').show();
      $("#btn_add_keyword").attr("disabled",true);

     
     var  temp_pos = [];
     var  temp_neg = [];
     var temp_neutral = [];
      var pos_total = 0;
     var neg_total =0;
     var neutral_total =0;

    
     
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getMentionSentiDetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),keyword_group:keywordGroup,groupType:'null'}
    })
    .done(function(data) { 
       for(var i in data) 
        {
               
                temp_pos.push(Math.round(data[i].positive));
                temp_neg.push(Math.round(data[i].negative));
                temp_neutral.push(Math.round(data[i].neutral));
             
             
       
        }
$.each(temp_pos,function(){pos_total+=parseInt(this) || 0;});
$.each(temp_neg,function(){neg_total+=parseInt(this) || 0;});
$.each(temp_neutral,function(){neutral_total+=parseInt(this) || 0;});
// alert(Legend_Data);

                senti_pos.push(pos_total);
                senti_neg.push(neg_total);
      
                if(jQuery.inArray(Legend_Data[sr_of_brand], sentiLabel)=='-1')
                  sentiLabel.push(Legend_Data[sr_of_brand]);

        option= null;
      
option = {
    color: colors_senti,

    tooltip: {
        trigger: 'axis',
        // axisPointer: {
        //     type: 'cross'
        // },
      
    },
    //       grid: {
    //       top:    60,
    
    // left:   '5%',
    // right:  '10%',
    // bottom:  '5%',
    //         containLabel: true
    //     },
         legend: {
        data:['positive','negative'],
      
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name: 'Page',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
        
            data: Legend_Data
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Count',
            scale:true,
            position: 'left',
         
            axisLabel: {
        formatter: function (e) {
            return nFormatter(e,1,1);
        }
    }
    
     }
    ],
    series: [{
            name:'positive',
            type:'bar',
            smooth: 0.2,
            color:colors_senti[0],
            barMaxWidth:30,
            barMinHeight:2,
            data:senti_pos
          
        },{
            name:'negative',
            type:'bar',
            smooth: 0.2,
            color:colors_senti[1],
            barMaxWidth:30,
            barMinHeight:2,
            data:senti_neg
          
        }]
};

    $( "#sentiment-spin" ).hide();
    $('#btn_add_keyword').removeAttr('disabled');
   
    SentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            SentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   SentiChart.on('click', function (params) {
  if (params.event.handled !== true) {
   params.event.handled = true;
   // console.log("tags");
   // console.log(params);
   // console.log(params.name); // xaxis data = tag name
   // console.log(params.seriesName); //bar type name ="positive"
   // console.log(params.value);//count 8
   // alert(params.value);// count of sentiment
   // alert(params.seriesName); //positive or negative
   // alert(params.name); // Page Name (KBZ)
   // alert(params);
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var CmtType ='';
   if(params.seriesName == 'positive')
    CmtType = 'pos'
    else
   CmtType = 'neg'

  var default_kw = $('.hidden-kw').val();

  window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid')+"&SentiType="+CmtType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&keyword_group="+params.name+"&hidden_kw="+""+"&default_kw="+default_kw, '_blank');
}
});
      var arrayLength=keygroup_id_arr.length;
       sr_of_brand = sr_of_brand +1;
     if(sr_of_brand<arrayLength )
     {
      SentimentDetail(fday,sday,keygroup_id_arr[sr_of_brand],sr_of_brand);
    
     }
     

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}

function MentionTierCompare(fday,sday,keywordGroup,sr_of_brand)
    {
  
      let main = document.getElementById("pageTier-chart");
      let existInstance = echarts.getInstanceByDom(main);
        if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }
      var PageTierChart = echarts.init(main);
      $('#tier-spin').show();
      $("#btn_add_keyword").attr("disabled",true);

     
     var  temp_tier1 = [];
     var  temp_tier2 = [];
     var temp_tier3 = [];
      var tier1_total = 0;
     var tier2_total =0;
     var tier3_total =0;

    
     
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getMentionTier')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),keyword_group:keywordGroup,groupType:'null'}
    })
    .done(function(data) { 
       for(var i in data) 
        {  //alert(data[i].Tier1);
               
                temp_tier1.push(data[i].Tier1);
                temp_tier2.push(data[i].Tier2);
                temp_tier3.push(data[i].Tier3);
             
             
       
        }
        // alert(temp_tier3);
      
$.each(temp_tier1,function(){tier1_total+=parseInt(this) || 0;});
$.each(temp_tier2,function(){tier2_total+=parseInt(this) || 0;});
$.each(temp_tier3,function(){tier3_total+=parseInt(this) || 0;});
// alert(Legend_Data);

                tier1.push(tier1_total);
                tier2.push(tier2_total);
                tier3.push(tier3_total);
      
                if(jQuery.inArray(Legend_Data[sr_of_brand], sentiLabel)=='-1')
                  tierLabel.push(Legend_Data[sr_of_brand]);
var colortier = ['#c98245','#9f5125','#441f0b'];
        option= null;
      
option = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        // axisPointer: {
        //     type: 'cross'
        // },
      
    },
    //       grid: {
    //       top:    60,
    
    // left:   '5%',
    // right:  '10%',
    // bottom:  '5%',
    //         containLabel: true
    //     },
         legend: {
        data:['Tier1','Tier2','Tier3'],
      
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name: 'Page',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
        
            data: Legend_Data
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Mention Count',
            scale:true,
            position: 'left',
         
            axisLabel: {
        formatter: function (e) {
            return nFormatter(e,1,1);
        }
    }
    
     }
    ],
    series: [{
            name:'Tier1',
            type:'bar',
            smooth: 0.2,
            color:colortier[0],
            barMaxWidth:30,
            barMinHeight:2,
            data:tier1
          
        },{
            name:'Tier2',
            type:'bar',
            smooth: 0.2,
            color:colortier[1],
            barMaxWidth:30,
            barMinHeight:2,
            data:tier2
          
        },
        {
            name:'Tier3',
            type:'bar',
            smooth: 0.2,
            color:colortier[2],
            barMaxWidth:30,
            barMinHeight:2,
            data:tier3
          
        }]
};

    $( "#tier-spin" ).hide();
    $('#btn_add_keyword').removeAttr('disabled');
    
    PageTierChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            PageTierChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   PageTierChart.on('click', function (params) {
  if (params.event.handled !== true) {
   params.event.handled = true;
   // alert(params);
   // console.log(params.name); // xaxis data = tag name
   // alert(params.seriesName); //bar type name ="positive"
   // console.log(params.value);//count 8
   // alert(params.value);// count of sentiment
   // alert(params.seriesName); //positive or negative
   // alert(params.name); // Page Name (KBZ)
   // alert(params);
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var page_tier ='';
   if(params.seriesName == 'Tier1')
    page_tier = 'Tier 1';
   else if(params.seriesName == 'Tier2')
    page_tier = 'Tier 2';
   else
   page_tier = 'Tier 3';

  var default_kw = $('.hidden-kw').val();

  window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid')+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&keyword_group="+params.name+"&hidden_kw="+""+"&default_kw="+default_kw+"&tier="+page_tier, '_blank');
}
});
      var arrayLength=keygroup_id_arr.length;
       sr_of_brand = sr_of_brand +1;
     if(sr_of_brand<arrayLength )
     {
      MentionTierCompare(fday,sday,keygroup_id_arr[sr_of_brand],sr_of_brand);
    
     }
     

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
var pieChartColor=[];
function MentionDetail(fday,sday,keywordGroup,sr_of_brand)
{
// alert(keywordGroup);
    var MentionChart = echarts.init(document.getElementById('mention-chart'));
    var MentionPieChart = echarts.init(document.getElementById('mention-pie-chart'));

      $('#mention-spin').show();
      $('#mention-pie-spin').show();
      $("#btn_add_keyword").attr("disabled",true);
    var brand_id = GetURLParameter('pid');
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
            url: "{{route('getmentiondetail')}}", // This is the URL to the API
           data: {fday:fday,sday:sday,brand_id:brand_id,keyword_group:keywordGroup,periodType:'day'}
    })
    .done(function( data ) {
       var data =  data[0];
       var colorpart = data[1];
       console.log(data);
       var temp_mentions=[];
       var mentions=[];
       var new_date_arr=[];
       var mention_total =0;
       var notFound_arr = [] ; 
       var color = [];
    
         for(var i in data) {  //alert(data[i].mentions);
         // console.log(data)
                color.push(data[i].color);
                pieChartColor= colorpart;
                var cc = colorpart;
                temp_mentions.push(data[i].mention);
                 new_date_arr.push(data[i].periodLabel);
                if(jQuery.inArray(data[i].periodLabel, mentionLabel)=='-1')
                {
                  mentionLabel.push(data[i].periodLabel);
                  // alert(data);
                  if(sr_of_brand > 0)
                   {
                    notFound_arr.push(data[i].periodLabel);
                 //   console.log(fanseriesList);
                   }
                }

                var checkcolor = data[i].color;
              }

        var brandcolor = ''; var datacolor='';
        if (checkcolor == null || checkcolor == 'undefined'){
          brandcolor = colors;
          datacolor  = colors[sr_of_brand];
        }
        else{
          brandcolor = color;
          datacolor  = color;
          brandcolor = pieChartColor;
        }


        mentionLabel.sort(function(a, b){
    var dateA=new Date(a), dateB=new Date(b)
    return dateA-dateB //sort by date ascending
})
       for(var j in mentionLabel) {//alert(fanLabel[j]);
        var found =jQuery.inArray(mentionLabel[j], new_date_arr);
      //  alert(found + fanLabel[j]);
        if(found=='-1')
          mentions.push('0');
        else
          mentions.push(temp_mentions[found]);
       }
// alert(mentions);
        for(var NF in notFound_arr) {//alert(fanLabel[j]);
        var found =jQuery.inArray(notFound_arr[NF], mentionLabel);//if not found fanLabel data in new date arr put it '0', it means that new date arr of page hasn't complete data for each day.
      //  alert(found + fanLabel[j]);
        if(found > -1)
        {
          // alert(found);
          for(var FD in keygroup_id_arr)
          {
           
            if(FD < keygroup_id_arr.length-1)
            {
              mentionseriesList[FD]['data'].splice(found, 0, 0);//arr.splice(2, 0, "Lene");
              //console.log(fanseriesList[FD]['data']);
            }
            
          }
    
        }
         
       }
       // console.log(sr_of_brand);

      $.each(mentions,function(){mention_total+= parseInt(this) || 0;});
      mentionpielist.push({
        name : Legend_Data[sr_of_brand],
        value : mention_total
    }
        );
      // alert(mention_total);
      arr_mention_label.push(Legend_Data[sr_of_brand]+' : '+ formatNumber(mention_total));
      arr_mention_total.push(mention_total);
      arr_mention.push(mentions);
      // console.log(Legend_Data[sr_of_brand]);
      mentionseriesList.push({
            name:Legend_Data[sr_of_brand],
            type:'line',
            smooth: 0.2,
            color:datacolor,
            barMinHeight:2,
            data:mentions
          
        },

                           );

      // console.log("fanseriesList");
      // console.log(fanseriesList);
       // console.log(fan_abs_diff);
// alert(piebrandcolor);
        option= null;
        option_pie= null;
  option_pie = {
    color: brandcolor,
 tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
        orient: 'horizontal',
        x: 'left',
        data:Legend_Data
    },

        series: [  
        {
            // radius : '65%',
            name:'Mention',
            type:'pie',
            radius: ['40%', '50%'],
            avoidLabelOverlap: false,
            label: {
                formatter: function(params) {//console.log(params.name);
                        //console.log(JSON.stringify(params));
                        var name = params.name.substr(0, params.name.indexOf(' '));
                        var percent = params.percent;
                        return params.name + '\n' + percent + '%';
                    }
                
            },
            labelLine: {
                normal: {
                    show: true
                }
            },
            data:mentionpielist,

        }
    ]
};
$('#mention-pie-spin').hide();
 MentionPieChart.setOption(option_pie, true), $(function() {
    function resize() {
        setTimeout(function() {
            MentionPieChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
  // var default_kw = $('.hidden-kw').val();
  // alert(default_kw);
 MentionPieChart.on('click', function (params) {
  if (params.event.handled !== true) {
   params.event.handled = true;
   // console.log(params.name);
   // console.log(params);
   // console.log(params.name); // xaxis data = tag name
   // console.log(params.seriesName); //bar type name ="positive"
   // console.log(params.value);//count 8
   // alert(params.value);// count of pie
   // alert(params.seriesName); //Mention
   // alert(params.name); // Keyword (KBZ)
   // alert(params);
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var default_kw = $('.hidden-kw').val();
   // console.log(default_kw);
   window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate() +"&keyword_group="+params.name+"&hidden_kw="+""+"&default_kw="+default_kw, '_blank');
}
});

option = {
    color: brandcolor,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
           // type: 'cross'
        },
        formatter: function (params) {

        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        // let rez = '<p>' + params[0].name + '</p>';
        let rez = '';
        //console.log("test");
        //quite useful for debug
        params.forEach(item => {
            
             // console.log(item);
             // console.log(item.seriesIndex);
             // console.log(item.dataIndex);
          // var diff_amount = 0;
          //  var item_data=item.data==='-'?'-':formatNumber(item.data);
          //  // var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName +'<br>' + 'Fun Growth: ' + fan_abs_diff[item.dataIndex]  + '</p>'
          //  if(typeof arr_mention[item.seriesIndex] !== 'undefined')
          //  {
          //   diff_amount = arr_mention[item.seriesIndex][item.dataIndex];
          //  }
        var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item.data+'</span><br></p>'
              // var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span><br>' + 'Fun Growth: ' + diff_amount  + '</p>'

              // var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span></p>'
              rez += xx;
       
         }
           
        );

        return rez;
    }
    },
    //       grid: {
    // top: '5%',
    
    // left:   '5%',
    // right:  '5%',
   
    //         containLabel: true
    //     },
         legend: {
        data:Legend_Data,
        // formatter: '{name}: '+ formatNumber(arr_reach_total[sr_of_brand]),
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name : 'Day',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
                  axisLabel: {
      formatter: function (value, index) {
       const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

     var date = new Date(value);
       var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
       return xx;

},

    },
            data: mentionLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Count',
            scale:true,
            // max: 250,
            position: 'left',
            // axisLine: {
            //     lineStyle: {
            //         color: colors[0]
            //     }
            // }
                    axisLabel: {
        formatter: function (e) {
            return formatNumber(e);
        }
    }
    
     }
    ],
    series: mentionseriesList
};
$("#mention-spin").hide();
$('#btn_add_keyword').removeAttr('disabled');

 MentionChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            MentionChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
 MentionChart.on('click', function (params) {
 if (params.event.handled !== true) {
   params.event.handled = true;


   //console.log(params);
   // alert(params.name); // xaxis data = tag name
   // alert(params.seriesName); //bar type name ="positive"
   // alert(params.value);//count 8
   var default_kw = $('.hidden-kw').val();
   // alert(GetStartDate()); alert(GetEndDate());
    window.open("{{ url('relatedmention?')}}" +"pid="+ GetURLParameter('pid')+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&keyword_group="+params.seriesName+"&period="+ params.name+"&hidden_kw="+""+"&default_kw="+default_kw, '_blank');
}
});

     var arrayLength=keygroup_id_arr.length;
     sr_of_brand = sr_of_brand +1;
     if(sr_of_brand<arrayLength )
     {
      MentionDetail(fday,sday,keygroup_id_arr[sr_of_brand],sr_of_brand);
     }
    })
    .fail(function() {
              // If there is no communication between the server, show an error
             // console.log( "error occured in FAN API" );
            });
}

function GetStartDate()
  {
     //alert(startDate);
   return startDate.format('YYYY-MM-DD');
  }
  function GetEndDate()
  {
             // alert(endDate);
  return endDate.format('YYYY-MM-DD');
  }        

    var permission = GetURLParameter('accountPermission');

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,

        },function(start, end,label) {//alert(label);
        
                    if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) 
          
            {
                 swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }


            else{
              var startDate;
        var endDate;
        var labelDate;
        startDate = start;
        endDate = end;
        labelDate = label;
        
        compare_count=0;
        mentionseriesList=[];mentionLabel = [];arr_mention_total=[];arr_mention_label=[];mentionpielist=[];
        arr_mention=[];
        senti_pos=[];senti_neg=[];sentiLabel=[];
        tier1=[];tier2=[];tier3=[];tierLabel=[];
        refreshGraph(startDate,endDate,labelDate);
             }
           }
           else
           {
               var startDate;
        var endDate;
        var labelDate;
        startDate = start;
        endDate = end;
        labelDate = label;
        
        compare_count=0;
        mentionseriesList=[];mentionLabel = [];arr_mention_total=[];arr_mention_label=[];mentionpielist=[];
        arr_mention=[];
        senti_pos=[];senti_neg=[];sentiLabel=[];
        tier1=[];tier2=[];tier3=[];tierLabel=[];
        refreshGraph(startDate,endDate,labelDate);
           }
      });


function refreshGraph(startDate,endDate,labelDate)
{

    ChooseDate(startDate,endDate,keygroup_id_arr[0],0,labelDate);
  
}

$(document).on('change', '#tag_filter', function () {
      tag_pos=[];tag_neg=[];tagsentiLabel=[];total_tag=[];
      tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
});



   

$("#btn_add_keyword").click(function(){
var arrayLength=keygroup_id_arr.length;
// pieChartColor=[];
    for (var i = 0; i < arrayLength; i++) {
           var keygroup= keygroup_id_arr[i];
    //Do something
    // $('#'+brand_id).prop('disabled', true);
     $('#keywordgroup_id').children('option[value="'+keygroup+'"]').css('display','none');
}   
   /* $("select#brand_id").val(''); */
   $("select#keywordgroup_id").prop('selectedIndex', 0);
   $('#add-keywordGroup').modal('show'); 
  
  
});

$("#btn_remove_keyword").click(function(){

     compare_count=0;
     Legend_Data=[]; keygroup_id_arr=[];
     mentionseriesList=[]; mentionLabel = []; arr_mention_total=[]; arr_mention_label=[];
     mentionpielist=[];
     arr_mention=[];
      senti_pos=[]; senti_neg=[]; sentiLabel=[];
      tier1=[];tier2=[];tier3=[];tierLabel=[];


    var MentionChart = echarts.init(document.getElementById('mention-chart'));
    MentionChart.clear();

    var MentionPieChart = echarts.init(document.getElementById('mention-pie-chart'));
    MentionPieChart.clear();

    var sentimentchart = echarts.init(document.getElementById('sentiment-chart'));
    sentimentchart.clear();

    var tierChart = echarts.init(document.getElementById('pageTier-chart'));
    tierChart.clear();

    Rebind_KeywordGroup();
});

$("#btn_add_keygroup").click(function(){
   $('#add-keywordGroup').modal('hide'); 
  var selected_text=$( "#keywordgroup_id option:selected" ).text();
  var selected_value=$( "#keywordgroup_id option:selected" ).val();
  

  if(selected_value !== '')
  {

      if(keygroup_id_arr.length <=0){
        keygroup_id_arr.push(selected_value);
        Legend_Data.push(selected_value);
        ChooseDate(startDate,endDate,selected_value,0,labelDate);
      }
      else{
        compare_count=compare_count+1;
        Legend_Data.push(selected_value);
        keygroup_id_arr.push(selected_value);
        ChooseDate(startDate,endDate,selected_value,compare_count,labelDate);
      }
  }  
  //add to legend array

});


function nFormatter(num, digits ,get_zero=0) {
    if(num<=0 && get_zero==0)
      {
        return '-';
      }
      else
      {
  var si = [
    { value: 1, symbol: "" },
    { value: 1E3, symbol: "k" },
    { value: 1E6, symbol: "M" },
    { value: 1E9, symbol: "G" },
    { value: 1E12, symbol: "T" },
    { value: 1E15, symbol: "P" },
    { value: 1E18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
      }
}

 function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

 function convertKtoThousand(s)
{

    var str=s;
    str=str.toUpperCase();
    if(str === '-')
    {
      return 0;
    }
    if (str.indexOf("K") !== -1) {
    str = str.replace("K",'');
    return parseFloat(str) * 1000;
  } else if (str.indexOf("M") !== -1) {
    str = str.replace("M",'');
     return parseFloat(str) * 1000000;
  } else {
    return parseFloat(str);
  }
  
}

 function LevelCheck(s)
{

    if(parseInt(s) === 1) return "1st";else if(parseInt(s) === 2) return "2nd";else if(parseInt(s) === 3) return "3rd";
    else if(parseInt(s) > 3) return s+"th";
  
}
    function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x1F60D', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }

       
 

       
  });
    </script>
    
<style>
.modal-dialog {
    position: absolute;
    top: 200px;
    right: 100px;
    bottom: 0;
    left: 100px;
    z-index: 10040;
    /* overflow: auto; */
}
.table td, .table th {
    padding: .75rem .5rem .75rem .5rem;
    }
    
.myHeaderContent
{
   margin-right:300px;
}
</style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


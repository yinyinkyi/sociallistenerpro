  @extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
  @section('content')
  @section('filter')
  <li  class="nav-item dropdown mega-dropdown" > <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark dropdown-pages" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search text-warning"></i> <span class="text-warning" id="select_page"></span></a>
  <div id="search_form" class="dropdown-menu scale-up-left" id="dlDropDown"  style="padding-left: 5%">
  <ul class="mega-dropdown-menu row">
   
  <li class="col-lg-12" >
   <form>
      <input type="hidden" value='' class="hidden-chip" id='' name="hidden-chip"/>
      <input type="hidden" value='{{ auth()->user()->company_id }}' id="hidden-user" name="hidden-user"/>
      <input type="hidden" value="{{ auth()->user()->default_page }}" class="hidden-pg" name="hidden-pg"/>
      <input type="hidden" value="{{ auth()->user()->role }}" class ="hidden-role" name="hidden-kw"/>
                  <div id="chip-competitor" class="col-md-12 align-self-center"  style="min-height:70px;padding-left:20px;padding-top:10px;border: 1px solid #f9f9f8;">
                   
                    </div>
                 
   </form>
  </li>
  </ul>
  </div>
  </li>
  @endsection

    <!-- ============================================================== -->
                  <!-- Bread crumb and right sidebar toggle -->
                  <!-- ============================================================== -->
                
                   
                   <header class="" id="myHeader">
                    <div class="row page-titles">
                   
                      <div class="col-md-8 col-6 align-self-center">
                          <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                                 
                                   <li class="nav-item "> <a class="nav-link active" data-toggle="tab" href="#post" role="tab"><span id="title_post">Posts</span></a> </li>
                                   
                                  <li class="nav-item"> <a class="nav-link " data-toggle="tab" href="#comment" role="tab"> <span id="title_comment">Comments</span></a> </li>

                                 
                                  
                                  
                              </ul>
                              <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
            
                       
                      </div>
                      <div class="col-md-4 col-6 align-self-center">
                          <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                              <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                 <!-- <div class="chart-text m-r-10">
                                        <div class='form-material input-group'>
                                    
                                            
                                          
                              </div></div> -->

                                      
                              </div>
                              <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                 <input type='text' class="form-control dateranges" style="datepicker" />
                                  <div class="input-group-append">
                                      <span class="input-group-text">
                                              <span class="ti-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                          
                          </div>
                      </div>




                  </div>
    </header>
               
   
  
     <div class="tab-content">
        <div class="tab-pane active" id="post" role="tabpanel">
          
            <div class="row">
               <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                <div class="card">
                  <div class="card-body b-t collapse show" style="padding-top:0px">
                    <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                      <div class="table-responsive">
                        <table id="tbl_post"  class="table" style="margin-top:4px;" >
                        <thead>
                          <tr>
                            <th></th>
                            <th>Reaction</th>
                            <th>Comments</th>
                            <th>Shares</th>
                            <!-- <th>Overall</th> -->
                          </tr>
                        </thead>

                      </table>
                      </div>
                    </div>
                </div>
              </div>

               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                     <div class="card earning-widget" style="height:230px">
            <div class="card-header">
              <h4 class="card-title m-b-0">Sentiment</h4>
            </div>
          <div class="card-body b-t collapse show">
                            
           <div class="form-group">
              <div class="input-group">
                <ul class="icheck-list post-sentiment-list" style="padding-top:0px">
                  <li>
                   <input type="checkbox" class="check post-sentimentlist" name="post-sentimentlist" value="all" id="psenti-all"> 
                    <label  for="psenti-all" style="padding-left:30px;font-weight:500;font-size:12px">all</label>
                  </li>
                 <li>
                <input type="checkbox" class="check post-sentimentlist" name="post-sentimentlist" value="pos" id="psenti-pos"> 
                 <label class="text-success" for="psenti-pos" style="padding-left:30px;font-weight:500;font-size:12px">positive</label>
                </li>
                <li>
                <input type="checkbox" class="check post-sentimentlist" name="post-sentimentlist" value="neg" id="psenti-neg"> 
                <label class="text-danger" for="psenti-neg" style="padding-left:30px;font-weight:500;font-size:12px">negative</label>
                </li>
                <li> 
                <input type="checkbox" class="check post-sentimentlist" name="post-sentimentlist" value="neutral" id="psenti-neutral"> 
                <label class="text-info"   for="psenti-neutral" style="padding-left:30px;font-weight:500;font-size:12px">neutral</label>
                </li>
                                                             
               </ul>
              </div>
          </div>
        </div>
      </div>
      <div class="card earning-widget" style="height:550px">
        <div class="card-header">
          <h4 class="card-title m-b-0">Tags <span style="float: right;">
          <span  class="mdi mdi-sort-descending" id="pnameSort" style="color:#4267b2"></span>Name
          <span  class="mdi mdi-sort-descending" id="pcountSort" style="color:#4267b2"></span>Count
          </span></h4>
          
        </div>
      <div class="card-body b-t collapse show" style="overflow-y:scroll;height:550px">
        <div class="form-group">
          <div class="input-group">
              <ul class="icheck-list post-tag-list" style="padding-top:0px">
                <div style="display:none"  align="center" style="vertical-align: top;" id="post-tag-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
              </ul>
          </div>
        </div>
      </div>
    </div>
              </div>
            </div>
        
        </div> 

        <div class="tab-pane" id="comment" role="tabpanel">
         
            <div class="row">
               <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                         <div class="card">
                            <div class="card-body">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="cmt-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div class="table-responsive">
                               <table id="tbl_comment" class="table" style="width:100%;">
                              <thead>
                                <tr>
                                  <th></th>
                                  <th>Reaction</th>
                                
                                </tr>
                              </thead>

                            </table>
                            </div>
                          </div>
                   
                        </div>
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                  <div class="card earning-widget" style="height:230px">
                              <div class="card-header">
                       
                                  <h4 class="card-title m-b-0">Sentiment</h4>
                              </div>
                              <div class="card-body b-t collapse show">
                            
                                   <div class="form-group">
                                                    
                                    <div class="input-group">
                                        <ul class="icheck-list sentiment-list" style="padding-top:0px">
                                        <li>
                                   <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="all" id="senti-all"> 
                                    <label  for="senti-all" style="padding-left:30px;font-weight:500;font-size:12px">all</label>
                                    </li>
                                     <li>
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="pos" id="senti-pos"> 
                                     <label class="text-success" for="senti-pos" style="padding-left:30px;font-weight:500;font-size:12px">positive</label>
                                    </li>
                                    <li>
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neg" id="senti-neg"> 
                                    <label class="text-danger" for="senti-neg" style="padding-left:30px;font-weight:500;font-size:12px">negative</label>
                                    </li>
                                    <li> 
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neutral" id="senti-neutral"> 
                                    <label class="text-info"   for="senti-neutral" style="padding-left:30px;font-weight:500;font-size:12px">neutral</label>
                                    </li>
                                                             
                                                          </ul>
                                                      </div>
                                                  </div>
                              </div>
                          </div>
                      <div class="card earning-widget" style="height:550px">
                        <div class="card-header">
                          <h4 class="card-title m-b-0">Tags <span style="float: right;">
                          <span  class="mdi mdi-sort-descending" id="nameSort" style="color:#4267b2"></span>Name
                          <span  class="mdi mdi-sort-descending" id="countSort" style="color:#4267b2"></span>Count
                          </span></h4>
                          
                        </div>
                      <div class="card-body b-t collapse show" style="overflow-y:scroll;height:550px">
                        <div class="form-group">
                          <div class="input-group">
                              <ul class="icheck-list tag-list" style="padding-top:0px">
                                <div style="display:none"  align="center" style="vertical-align: top;" id="tag-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              </ul>
                          </div>
                        </div>
                      </div>
                    </div>
               </div>
             </div></div>







    </div>
                        
                  

                    
                    <div id="show-image" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div align="center" class="modal-body large_img">
                                                 
                                              </div>

                                          </div>
                                        
                                      </div>
                                
                   </div>
                  <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <h4 class="modal-title" id="postModal">Post Detail</h4>
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div class="modal-body post_data">
                                                 
                                              </div>
                                                <div class="modal-footer">
                                                  <button type="button" id="btnSeeComment" class="btn btn-green waves-effect text-left" data-dismiss="modal">See Comments</button>
                                              </div>
                                          </div>
                                          <!-- /.modal-content -->
                                      </div>
                                      <!-- /.modal-dialog -->
                   </div>
                 <div id="show-add-tag" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tag_title" aria-hidden="true" style="display: none;">
                          <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title" id="tag_title">Add New Tag</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  </div>
                                  <div class="modal-body add-tag">
                                              
                          <div class="form-group">
                                 <input type="hidden" id="brand_id" name="brand_id" value="{{ $brand_id }}" >
                                 <label for="name" class="control-label">{{ __('Name') }}</label>
                                 <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                 
                                <span class="invalid-name" role="alert">
                                  
                                </span>
                               
                          </div>
      
                          <div class="form-group">
                              <button type="button" id="add_new_tag" class="btn btn-primary">  {{ __('Save') }}</button>
                            </div>
        
                      </div>
                                
                    </div>
                                     
                </div>
                          
          </div>
           <div id="show-add-campaign" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tag_title" aria-hidden="true" style="display: none;">
                          <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title" id="tag_title">Add Campaign</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="form-group">
                                        <label>Input Select</label>
                                        <select class="form-control add-campaign">
                                            
                                        </select>
                                    </div>
                                   <div class="form-group">
                                    <button type="button" id="add_campaign" class="btn btn-primary">Add</button>
                                  </div>
                             </div>
                          </div>
                          
          </div>
  @endsection
  @push('scripts')
  <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
      <!-- slimscrollbar scrollbar JavaScript -->
      <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
      <!--Wave Effects -->
      <script src="{{asset('js/waves.js')}}" defer></script>
      <!--Menu sidebar -->
      <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
      <!--stickey kit -->
      <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
      <!--Custom JavaScript -->
      <script src="{{asset('js/custom.min.js')}}" defer></script>
      <!-- ============================================================== -->
      <!-- This page plugins -->
      <!-- ============================================================== -->
     
     <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
       <script src="{{asset('js/morris-data.js')}}" ></script>-->
      
      <!-- Chart JS -->
      <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
      <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
      <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
      <!-- Flot Charts JavaScript -->
      <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
      
   <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
      <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

      <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
          <!-- Date range Plugin JavaScript -->
      <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
      <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
     <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
     <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
      <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" defer></script>
     <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
     <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/icheck/icheck.min.js')}}"  defer></script>
   <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}" defer></script>

<!--    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script> -->
  <!-- <script src="{{asset('assets/plugins/materialize/js/materialize.min.js')}}" defer></script> -->
   
     <script>
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myHeader");
  var myHeaderContent = document.getElementById("myHeaderContent");
  // var chipcompetitor = document.getElementById("chip-competitor");
  var tabHeader = document.getElementById("tabHeader");
  var tabHeader_sticky = tabHeader.offsetTop;

  var sticky = header.offsetTop;

  function myFunction() {//alert("ho");
    if (window.pageYOffset > sticky) {
      header.classList.add("s-topbar");
      header.classList.add("s-topbar-fix");
      myHeaderContent.classList.add("myHeaderContent");
      // chipcompetitor.classList.add("myHeaderScoll");
    } else {
      header.classList.remove("s-topbar");
      header.classList.remove("s-topbar-fix");
      myHeaderContent.classList.remove("myHeaderContent");
      // chipcompetitor.classList.remove("myHeaderScoll");
    }

    //  if (window.pageYOffset > tabHeader_sticky) {//alert("hi");
    //   tabHeader.classList.add("tabcontrolHeader");
    
    // } else {
    //     tabHeader.classList.remove("tabcontrolHeader");
    // }
  }
  </script>
   
  <script type="text/javascript">
  var startDate;
  var endDate;
  var label;
  /*global mention*/
  var mention_total;
  var mentionLabel = [];
  var mentions = [];
  /*Bookmark Array*/
  var bookmark_array=[];
  var bookmark_remove_array=[];
  /*global sentiment*/
  var positive = [];
  var negative = [];
  var sentimentLabel = [];
  var positive_total=0;
  var negative_total=0;
  var campaignGroup=[];
 var  colors=["#1e88e5","#dc3545","#28a745","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


  /*var effectIndex = 2;
  var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

  $(document).ready(function() {

    // var brand_id = $('#hidden-user').val();

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });


      //initialize
   $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
  /* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

   $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

        var GetURLParameter = function GetURLParameter(sParam) {
      var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : sParameterName[1];
          }
      }
  };

      startDate = moment().subtract(1, 'month');
      endDate = moment();
      label = 'month';

             $('.singledate').daterangepicker({
              singleDatePicker: true,
              showDropdowns: true,
              locale: {
                  format: 'DD/MM/YYYY'
              }
          },function(date) {
            // alert(date);
            endDate=date;
            var id =$('input[type=radio][name=period-radio]:checked').attr('id');
            if(id==="period-week")
           {
             startDate = moment(endDate).subtract(1, 'week');
             endDate = endDate;
           }
           else
           {
             startDate = moment(endDate).subtract(2, 'day');
             endDate = endDate;
           }
          
           var admin_page=$('.hidden-chip').val();
            // alert(admin_page);     
            // alert('helo');
             var kw_search = $("#kw_search").val();
             // alert(kw_search);
             // alert(startDate);
           ChooseDate(startDate,endDate,admin_page,'','',kw_search);
         
        });

             $('#kw_search').keyup(function () { 
               var kw_search = $(this).val();

               // alert(kw_search);
            $('.dateranges').val(startDate.format('MMM D, YYYY') + ' - ' + endDate.format('MMM D, YYYY'));
            startDate=startDate;
            endDate=endDate;
            // alert(startDate);
            filter_tag = '';
            var graph_option = $('.btn-group > .btn.active').text();
          
           var date_preset = Calculate_DatePreset(label);
           getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag,kw_search);
           getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           getAllVisitorPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);

            if (graph_option.trim()== 'Sentiment') {
              InboundSentimentDetail(GetStartDate(),GetEndDate());
            }
            // else if (graph_option.trim() == 'Page Like')
            // {
            //     PageGrowth(GetStartDate(),GetEndDate(),date_preset);
            // }tartDate=startDate;
            endDate=endDate;
               

               // ChooseDate(startDate,endDate,'','',kw_search)
            });

      var kw_search = $("#kw_search").val();
      // alert(kw_search);



   function hidden_div()
  {//alert("popular");
      // var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
    // var brand_id = 22;
      $( "#popular-spin" ).show();
      $("#popular").empty();
    $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('gethiddendiv')}}", // This is the URL to the API
        data: { view_name:'Dashboard'}
      })
      .done(function( data ) {//$("#popular").html('');
       for(var i in data) {
        $("#"+data[i].div_name).hide();
       }

      })
      .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
        // If there is no communication between the server, show an error
       // alert( "error occured" );
      });

  }



function Calculate_DatePreset(label)
{
         var date_preset='this_month';
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
          if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

        return date_preset;
}

     function ChooseDate(start, end,label='',filter_tag='',kw_search) {//alert(start);
            // alert(start);
            $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate=start;
            endDate=end;
             var graph_option = $('.btn-group > .btn.active').text();
          
           var date_preset = Calculate_DatePreset(label);
           getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag,kw_search);
           getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           tagCount();posttagCount();

          }

          function GetStartDate()
  {
     //alert(startDate);
   return startDate.format('YYYY-MM-DD');
  }
  function GetEndDate()
  {
             // alert(endDate);
  return endDate.format('YYYY-MM-DD');
  }
    function getAllPost(fday,sday,kw_search)
    {

        $(".popup_post").unbind('click');
        $(".see_comment").unbind('click');
        var brand_id = GetURLParameter('pid');

        var post_filter_tag_arr = [];
        $.each($("input[name='ptaglist']:checked"), function(){            
            post_filter_tag_arr.push($(this).val());
          });
        var post_filter_senti_arr = [];
        $.each($("input[name='post-sentimentlist']:checked"), function(){            
            post_filter_senti_arr.push($(this).val());
        });


      	var admin_page=$('.hidden-chip').val();

      	document.getElementById('select_page').innerHTML =admin_page;
	   	$("#post-spin").show();
	   	var oTable = $('#tbl_post').DataTable({
    
         	// "responsive": true,
          "paging": true,
           "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": true,
          "destroy": true,
          "ordering": true   ,
          "autoWidth": false,
          // "columnDefs" : [
          //      { "width": "60%", "targets": 0 },
          //      { "width": "10%", "targets": 1 },
          //      { "width": "10%", "targets": 2 },
          //      { "width": "10%", "targets": 3 },
          //      { "width": "10%", "targets": 4 }
          // ] ,
          "order": [],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          
          "ajax": {
            "url": '{{ route('getIndustryPost') }}',
            "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              document.getElementById('title_post').innerHTML = 'Posts ('+res.recordsTotal+")";
              return res.data;
            },
            
             "data": {
              "fday": fday,
              "sday": sday,
              // "other_page":'1',
              "kw_search" : kw_search,
              "brand_id": brand_id,
              "tsearch_senti":post_filter_senti_arr.join("| "),
              "tsearch_tag":post_filter_tag_arr.join("| "),
              "admin_page":admin_page
            
            }

          },
          "initComplete": function( settings, json ) {
          $('.tbl_post thead tr').removeAttr('class');
           $("#post-spin").hide();
          },
           drawCallback: function() {
               $('.select2').select2();
            },

   

          columns: [
          {data: 'post', name: 'post',"orderable": false},
          {data: 'reaction', name: 'reaction', orderable: true,className: "reaction-weight"},
          {data: 'comment', name: 'comment', orderable: true, className: "text-center"},
          {data: 'share', name: 'share', orderable: true, className: "text-center"},
        //   {data: 'overall', name: 'overall', orderable: true,className: "text-center"},


          ]
          
        }).on('change', '.edit_tag', function (e) {
          var hidden_pg  = $('.hidden-chip').val();
          var default_pg = $('.hidden-pg').val();
          var user_role = $('.hidden-role').val();
          if(user_role == 'Admin' || hidden_pg === default_pg){
            if (e.handled !== true) {
              var senti_id = $(this).attr('id');
              var post_id  = senti_id.replace("tags_","");
              var sentiment = $('#sentiment_'+post_id+' option:selected').val();
              var emotion = $('#emotion_'+post_id+' option:selected').val();
              var tags = $('#tags_'+post_id+' option:selected').map(function () {
                  return $(this).text();
              }).get().join(',');

              var tags_id = $('#tags_'+post_id).val();
              var brand_id = GetURLParameter('pid');
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id,type:'inbound_post'},
           success: function(response) {
            posttagCount();
            if(response>=0){
         
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Tag Changed!!');
                x.className = "show";

              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
          }
        });
        e.handled = true;
       }
      }
     else{
      var x = document.getElementById("snackbar")
      $("#snack_desc").html('');
      $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }  
  }).on('click', '.popup_post', function (e) {
          // alert ('hlllll');
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:brand_id},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];
            var monitor_page_imgurl=res_array[4];

               for(var i in data) {
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
                 var page_name=data[i].page_name;

               
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];

                if (typeof monitor_page_imgurl [found_index] !== 'undefined') 
                var page_photo = monitor_page_imgurl [found_index];
                
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                // var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large';
                //  var page_photo= $mongo_page_imgurl 
        
                page_Link= 'https://www.facebook.com/' + page_name ;   
                var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                         '  <div class="sl-left">'+
                         '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                         
                          ' <div class="sl-right">'+
                          '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                          ' <div class="sl-right"> '+
                          ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                          ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                html+=' </div></div></div></span></p> ' +                
                      '   <p class="m-t-10" align="justify">' + 
                        data[i].message +
                    
                      '</p> ';
                   if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }
          
           
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.see_comment', function (e) {//alert("hihi");
    if (e.handled !== true) {
      var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("seeComment_","");
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
      // var admin_page=$('#hidden-chip').val();
     
        var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
          //  alert(admin_page);
          var accountPermission = GetURLParameter('accountPermission');
      window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&accountPermission="+accountPermission, '_blank');
       
       
          }
          


            e.handled = true;
         }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
      // alert(btn_val);
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';
              htmltext +='<fieldset class="controls" style="text-align:left;padding-left:20px;">';

                                       
  for(var i in campaignGroup) 
          {
            // alert(campaignGroup);
            if(i==0)
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
              title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:brand_id,campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:brand_id},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
    
        
       
          }
          
           

            e.handled = true;
         });
      new $.fn.dataTable.FixedHeader( oTable );
     // oTable.columns.adjust().draw();
     // $('#tbl_post').css('width', '100%');
     // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
          
    }

      $(window).resize( function() {

       //  new $.fn.dataTable.FixedHeader( oTable );
        $('#tbl_post').css('width', '100%');
         $('#tbl_visitorpost').css('width', '100%');
        //$("#tbl_post").DataTable().columns.adjust().draw();
        //$('table.fixedHeader-floating').css('left', '200px');
    });
      function getAllVisitorPost(fday,sday,kw_search)
      {
        var brand_id = GetURLParameter('pid');
        $(".popup_post").unbind('click');
        $(".see_comment").unbind('click');

   
      // var admin_page=$('#hidden-chip').val();
       var admin_page_id=$('.hidden-chip').attr('id');
       var admin_page=$('.hidden-chip').val();

         var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
   
         document.getElementById('select_page').innerHTML =admin_page;
   var oTable = $('#tbl_visitorpost').DataTable({
    
         // "responsive": true,
          "paging": true,
           "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": false,
          "destroy": true,
          "ordering": true   ,
          "autoWidth": false,
          // "columnDefs" : [
          //      { "width": "60%", "targets": 0 },
          //      { "width": "10%", "targets": 1 },
          //      { "width": "10%", "targets": 2 },
          //      { "width": "10%", "targets": 3 },
          //      { "width": "10%", "targets": 4 }
          // ] ,
          "order": [],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          
          "ajax": {
            "url": '{{ route('getInboundPost') }}',
            "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              document.getElementById('title_visitor').innerHTML = 'Visitor Posts ('+res.recordsTotal+")";
              return res.data;
            },
     /*       data: function ( d ) {
              d.fday = mailingListName;
              d.sday = mailingListName;
              d.brand_id = mailingListName;
            }*/
             "data": {
              "fday": fday,
              "sday": sday,
              "admin_page":admin_page,
              "admin_page_id":admin_page_id,
              "other_page":'1',
              "visitor" : 1,
              "kw_search":$("#kw_search").val(),
              "brand_id": brand_id,
            
            }

          },
          "initComplete": function( settings, json ) {
            //console.log(json);
          $('.tbl_visitorpost thead tr').removeAttr('class');
          },
   

          columns: [
          {data: 'post', name: 'post',"orderable": false},
          {data: 'reaction', name: 'reaction', orderable: true,className: "text-center"},
          {data: 'comment', name: 'comment', orderable: true, className: "text-center"},
          {data: 'share', name: 'share', orderable: true, className: "text-center"},
        //   {data: 'overall', name: 'overall', orderable: true,className: "text-center"},


          ]
          
        }).on('click', '.popup_post', function (e) {
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:brand_id},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

               for(var i in data) {//alert(data[i].message);
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
                 var page_name=data[i].page_name;

                // if(isNaN(page_name)==false) //it is number
                // {
                 
                //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
                //   page_name = b[0];
                 
                   
                // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
                
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large'; 
        
         page_Link= 'https://www.facebook.com/' + page_name ;   
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      data[i].message +
                    
                      '</p>';
                   if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }
          
           
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.see_comment', function (e) {//alert("hihi");
    if (e.handled !== true) {
      var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("seeComment_","");
     // alert(post_id);
      // var admin_page=$('#hidden-chip').val();
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
     
        var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
          //  alert(admin_page);
 var accountPermission = GetURLParameter('accountPermission');
      window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&accountPermission="+accountPermission, '_blank');
       
       
          }
          


            e.handled = true;
         }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';
              htmltext +='<fieldset class="controls" style="text-align:left;padding-left:20px;">';

                                       
  for(var i in campaignGroup) 
          {
            if(i==0)
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
              title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:brand_id,campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:brand_id},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
    
        
       
          }
          
           

            e.handled = true;
         });
      new $.fn.dataTable.FixedHeader( oTable );
     // oTable.columns.adjust().draw();
     // $('#tbl_post').css('width', '100%');
     // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
          
    }

    

      $("#btnSeeComment").click( function()
             {
               var post_id=$("#popup_id").val();
               // var brand_id = $('#hidden-user').val();
               var brand_id = GetURLParameter('pid');
               // var admin_page=$('#hidden-chip').val();
                var admin_page_id=$('.hidden-chip').attr('id');
                var admin_page=$('.hidden-chip').val();

               var split_name = admin_page.split("-");
                  if(split_name.length > 0)
                  {
                    if(isNaN(split_name[split_name.length-1]) == false )
                      admin_page = split_name[split_name.length-1];
                  }
           // alert(admin_page);
                // window.open("{{ url('relatedcompetitorcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
                 // window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+admin_page , '_blank');
                 var accountPermission = GetURLParameter('accountPermission');
                   window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&accountPermission="+accountPermission, '_blank');
             }
          );

   function getAllComment(fday, sday,filter_tag='',kw_search)
      {
        $(".popup_post").unbind('click');
        $(".edit_predict").unbind('click');
        $("#add_tag").unbind('click');
        var brand_id = GetURLParameter('pid');
  
        var filter_tag_arr = [];
              $.each($("input[name='taglist']:checked"), function(){            
                  filter_tag_arr.push($(this).val());
                });
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });

	    var admin_page_id=$('.hidden-chip').attr('id');
	    var admin_page=$('.hidden-chip').val();

        $("#cmt-spin").show();
		var oTable = $('#tbl_comment').DataTable({
          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": true,   
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": true,
          "ordering": true   ,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
          // "order":[1,"desc"],  sort column 1 by desc order
         
          "ajax": {
            "url": '{{ route('getIndustryComment') }}',
            "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              document.getElementById('title_comment').innerHTML = 'Comments ('+res.recordsTotal+")";
              return res.data;
            },
   
             "data": {
              "fday": fday,
              "sday": sday,
              
              "brand_id" : brand_id,
              "kw_search":kw_search,
              "tsearch_senti":filter_senti_arr.join("| "),
              "tsearch_tag":filter_tag_arr.join("| "),
              "is_competitor":"true",
              "admin_page":admin_page,
            }

          },
          "initComplete": function( settings, json ) {
            //console.log(json);
              $("#cmt-spin").hide();
         
          },
       drawCallback: function() {
       $('.select2').select2();
    },

          columns: [
          {data: 'post_div', name: 'post_div',"orderable": false},
          {data: 'reaction', name: 'reaction',"orderable": true,className: "text-center"},
      


          ]
          
        }).on('click', '.show_alert', function (e) {
           e.preventDefault();
           $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
            // var url = $(this).data('remote');
            swal({
                title: 'Sorry',
                text: "This account has no permission to do this!",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })

        }).on('click', '.popup_post', function (e) {
          // alert(brand_id);
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:brand_id},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

            

              for(var i in data) {//alert(data[i].message);
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
        
        var page_name=data[i].page_name;
        // if(isNaN(page_name)==false)
        // {
          
        //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
        //  page_name = b[0];
        
        // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
        
        var page_Link= 'https://www.facebook.com/' + page_name ;  
        var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large';                         
  //data[i].full_picture
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      data[i].message +
                    
                      '</p>';
                   if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }


           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.edit_predict', function (e) {
      // alert(brand_id);
    if (e.handled !== true) {

      var post_id = $(this).attr('id');

  /*    var sentiment = $('input[name=sentiment]').val();
      var emotion = $('input[name=emotion_'+post_id+']').val();*/
      var sentiment = $('#sentiment_'+post_id+' option:selected').val();
      var emotion = $('#emotion_'+post_id+' option:selected').val();
      var tags = $('#tags_'+post_id).val();
    //   alert(sentiment);
    // alert(tags);
    // return;
     
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags},
           success: function(response) {// alert(response)
            if(response>=0)
            {
                  swal({   
              title: "Updated!",   
              text: "Done!",   
              timer: 1000,   
              showConfirmButton: false 
          });
            }
          }
              });

            e.handled = true;
         }
       
      
          
    }).on('change', '.edit_senti', function (e) {

      var hidden_pg  = $('.hidden-chip').val();
      var default_pg = $('.hidden-pg').val();
      var user_role = $('.hidden-role').val();

      if(user_role == 'Admin' || hidden_pg === default_pg){
        if (e.handled !== true) {
          var senti_id = $(this).attr('id');
          var post_id  = senti_id.replace("sentiment_","");
          var sentiment = $('#sentiment_'+post_id+' option:selected').val();
          var emotion = $('#emotion_'+post_id+' option:selected').val();
          var tags = $('#tags_'+post_id+' option:selected').map(function () {
              return $(this).text();
          }).get().join(',');

          var tags_id = $('#tags_'+post_id).val();
          var brand_id = GetURLParameter('pid');;
      
      $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
        if(sentiment == 'pos')  $("#sentiment_"+post_id).addClass('text-success');
        else if (sentiment == 'neg') $("#sentiment_"+post_id).addClass('text-red');
        else $("#sentiment_"+post_id).addClass('text-warning');
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {
            if(response>=0)
            {
               var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Sentiment Changed!!');
                x.className = "show";

                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }

          }
          });
            e.handled = true;
         }
      }

         else{
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
                x.className = "show";
              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
           }
          
    }).on('change', '.edit_tag', function (e) {
      var hidden_pg  = $('.hidden-chip').val();
      var default_pg = $('.hidden-pg').val();
      var user_role = $('.hidden-role').val();
      if(user_role == 'Admin' || hidden_pg === default_pg){
        if (e.handled !== true) {
          var senti_id = $(this).attr('id');
          var post_id  = senti_id.replace("tags_","");
          var sentiment = $('#sentiment_'+post_id+' option:selected').val();
          var emotion = $('#emotion_'+post_id+' option:selected').val();
          var tags = $('#tags_'+post_id+' option:selected').map(function () {
              return $(this).text();
          }).get().join(',');

          var tags_id = $('#tags_'+post_id).val();
          var brand_id = GetURLParameter('pid');
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {
            tagCount();
             // alert(response)
            if(response>=0)
            {
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Tag Changed!!');
                x.className = "show";

                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
          }
        });
        e.handled = true;
       }
      }       

     else{
      var x = document.getElementById("snackbar")
      $("#snack_desc").html('');
      $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
        x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    } 
    }).on('click', '#add_tag', function (e) {
    if (e.handled !== true) {
           $('#show-add-tag').modal('show'); 
          }
            
            e.handled = true;
         
       
      
          
    }).on('click', '.popup_img', function (e) {
    if (e.handled !== true) {

         var id = $(this).attr('id');
         var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
// img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
          $(".large_img").append("<div class='img-container'><img class='postPanel_previewImage' style='width:30%;height:50%;' src='"+src_path+"'/></div>");
          $('#show-image').modal('show'); 
         }
       
      
          
    });
       
      
          
    }
    $('#show-add-tag').on('shown.bs.modal', function () {
      $('#name').focus();
  })  



    var permission = GetURLParameter('accountPermission');
  $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
                startDate: startDate,
                endDate: endDate,
          },function(start, end,label) {
            if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) 
          
            {
                 swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }


            else{
              var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;
              var kw_search = $("#kw_search").val();
              ChooseDate(startDate,endDate,label,'',kw_search);
              tagCount();
             }
           }
           else
           {
                var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;
              var kw_search = $("#kw_search").val();
              ChooseDate(startDate,endDate,label,'',kw_search);
              tagCount();
           }
        }
      )

  // to acess only three month data for demo account 

    //     $(".dateranges").on("change",function(){

         
        
    // });


   //ChooseDate(startDate,endDate,'','');

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
     //window.dispatchEvent(new Event('resize'));
  // if(target=="#dashboard")
  //   {
      window.dispatchEvent(new Event('resize'));
  //  }

 



   });

  hidden_div();
  function getCampaignGroup()
  {
    // add-campaign
   
      var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getCampaignGroup')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          page_name:$('.hidden-chip').val(),
          }
      })
      .done(function( data ) {
        campaignGroup=data;
       
          
        
      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
  $('#nameSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'name';

      tagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
      var tagOrder = 'DESC' ;
      var fieldName = 'name';

      tagCount(tagOrder,fieldName);
    }
    

  });
    $('#pnameSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'name';

      posttagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
      var tagOrder = 'DESC' ;
      var fieldName = 'name';

      posttagCount(tagOrder,fieldName);
    }
    

  });
    $('#countSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'count';

      tagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
       var tagOrder = 'DESC' ;
      var fieldName = 'count';

       tagCount(tagOrder,fieldName);
    }
    

  });
        $('#pcountSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'count';

      posttagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
       var tagOrder = 'DESC' ;
      var fieldName = 'count';

       posttagCount(tagOrder,fieldName);
    }
    

  });
   var selected_tag = [];
  function tagCount(tagOrder='',fieldName='')
  {
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
// alert(admin_page);
      var filter_tag_arr = [];
      $.each($("input[name='taglist']:checked"), function(){            
          filter_tag_arr.push($(this).val());
          selected_tag.push($(this).val()); 
        });
      // alert(selected_tag);
       var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });

      var fday=GetStartDate();
      var sday=GetEndDate();
      $(".tag-list").empty();
      var brand_id = GetURLParameter('pid');
      $('#tag-spin').show();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getcmtTagCount')}}", // This is the URL to the API
        data: { 
          fday: fday,
          sday:sday,
          brand_id:brand_id,
          tagOrder:tagOrder,
          fieldName:fieldName,
          tsearch_tag:filter_tag_arr.join("| "),
          tsearch_senti:filter_senti_arr.join("| "),
          admin_page:admin_page,
          // admin_page_id:admin_page_id,
          limit:'no'}
      })
      .done(function( data ) {//console.log(data);
        $('#tag-spin').hide();
  for(var i in data) 
          {
             var tagLabel=data[i].tagLabel;
             var tagId=data[i].tagId;
             var tagCount=data[i].tagCount;
             var ans = findInArray(selected_tag,tagId);
              if(ans  !== -1){
                $(".tag-list").append(' <li>' +
              ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'" checked>'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
              }
              else{
              $(".tag-list").append(' <li>' +
              ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'">'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
              }

          }
          selected_tag = [];
      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
  var post_selected_tag = [];
    function posttagCount(tagOrder='',fieldName='')
  {
      // alert('helo');
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
// alert(admin_page);
      var post_filter_tag_arr = [];
      $.each($("input[name='ptaglist']:checked"), function(){            
          post_filter_tag_arr.push($(this).val());
          post_selected_tag.push($(this).val()); 
        });
        // alert("heoo");
      var post_filter_senti_arr = [];
      $.each($("input[name='post-sentimentlist']:checked"), function(){            
          post_filter_senti_arr.push($(this).val());
      });

      var fday=GetStartDate();
      var sday=GetEndDate();
      $(".post-tag-list").empty();
      var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
      $('#post-tag-spin').show();

          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getPostTagCount')}}", // This is the URL to the API
        data: { 
          fday: fday,
          sday:sday,
          brand_id:brand_id,
          tagOrder:tagOrder,
          fieldName:fieldName,
          tsearch_tag:post_filter_tag_arr.join("| "),
          tsearch_senti:post_filter_senti_arr.join("| "),
          admin_page:admin_page,
          // admin_page_id:admin_page_id,
          limit:'no'}
      })
      .done(function( data ) {//console.log(data);
      $('#post-tag-spin').hide();

  for(var i in data) 
          {
             var tagLabel=data[i].tagLabel;
             var tagId=data[i].tagId;
             var tagCount=data[i].tagCount;
             var res = findInArray(post_selected_tag, tagId);
              if(res !== -1){
                $(".post-tag-list").append(' <li>' +
              ' <input type="checkbox" class="check ptaglist" name="ptaglist" value="'+tagId+'" id="post_'+tagId+'" checked>'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="post_'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
              }
              else{
              $(".post-tag-list").append(' <li>' +
              ' <input type="checkbox" class="check ptaglist" name="ptaglist" value="'+tagId+'" id="post_'+tagId+'">'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="post_'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
              }

          }
          post_selected_tag = [];
      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }

    function findInArray(ar, val) {
    for (var i = 0,len = ar.length; i < len; i++) {
        if ( ar[i] == val ) {
            return i;
        }
    }
    return -1;
}

 
  $(document).on('click', '.taglist', function () {
    // alert('hello');
    getAllComment(GetStartDate(),GetEndDate());
   
    tagCount();
  });
  $(document).on('click', '.sentimentlist', function () {
    // alert('helo');   
    getAllComment(GetStartDate(),GetEndDate());
    tagCount();
});

    $(document).on('click', '.ptaglist', function () {
   // alert('posttagclick');
    getAllPost(GetStartDate(),GetEndDate());
    posttagCount();
  });
  $(document).on('click', '.post-sentimentlist', function () {
    // alert('hll');
    getAllPost(GetStartDate(),GetEndDate());
    posttagCount();
  
});


  $(document).on('click','.top_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:80%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
    $(document).on('click','.top_cmt_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
  $('#senti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.taglist)").click(function () {
    $("#senti-all").prop("checked", $("input:checkbox:not(.taglist):checked").length == 4);
  });

  $('#psenti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.post-sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.post-sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.ptaglist)").click(function () {
    $("#psenti-all").prop("checked", $("input:checkbox:not(.ptaglist):checked").length == 4);
  });

  bindOtherPage();

  function bindOtherPage()
  {
      var company_id = $("#hidden-user").val();
 
      $("#chip-competitor tbody").empty();
      var brand_id = GetURLParameter('pid');
           // alert(company_id);
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getBankType')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          company_id:company_id,
          }
      })
      .done(function( data ) {//alert(data);
       // console.log("hey"+JSON.stringify(data));
// var default_page = data[0]['default_page'];
  for(var i in data) 
          {
           // alert(i);    
           var default_page=data[i]['default_page'];
           // alert(default_page);
           var id=data[i]['id'];
          // alert(id);
         //   if(data[i]['id'] !== '')
         //   var photo='https://graph.facebook.com/'+data[i]['id']+'/picture?type=large';
         // else
         //  var photo ="{{asset('assets/images/competitor1.png')}}";
         if(data[i]['imgurl'] !== '') var photo = data[i]['imgurl'];
         else photo ="{{asset('assets/images/competitor1.png')}}";

         if(default_page !== '')
         {
    // alert(default_page);
            if(default_page == data[i]['page_name'])
            { 
              // alert('equal');
              
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['id']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');
             
                 $(".hidden-chip").val(data[i]['page_name']); 
                 $( ".hidden-chip" ).attr("id",data[i]['id']);
                 // $("#hidden-chip").(data[i]['page_name']);
              
            }
            else
            {
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['id']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');

            }
    

         }
         else
         {
           if(parseInt(i) == 0)
            { 
              
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['id']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');
                 
                 $(".hidden-chip").val(data[i]['page_name']); 
                 $( ".hidden-chip" ).attr("id",data[i]['id']);
                 // $("#hidden-chip").val(data[i]['page_name']); 
              
            }
            else
            {
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['id']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');
            }

         }
         


          }
           var kw_search = $("#kw_search").val();
          ChooseDate(startDate,endDate,'','',kw_search);
          tagCount();
          posttagCount();
       

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
  $("#add_new_tag").click(function(){
  //alert("hihi");
    var name = $("#name").val();
    var nameLength = name.length;
    // var category_id = $("#category_id").val();

    if(name == '')
    {
      $(".invalid-name").append("<strong class='text-danger'>Please fill out name!</strong>");
      return false;
    }
    else if (nameLength > 80 )
      {
      $(".invalid-name").append("<strong class='text-danger'>The name may not be greater than 80 characters!</strong>");
      return false;
    }
    //  if(category_id == '')
    // {
    //   $(".invalid-category").append("<strong class='text-danger'>Please choose category!</strong>");
    //   return false;
    // }
    //var keywords = $("#keywords").val();
    var brand_id = $("#brand_id").val();
    
   
    var keywords = '';

  //   if(keywords == '')
  //   {
  //   $(".invalid-keyword").append("<strong>Please fill out keyword</strong>");
  //   return false;
  // }
     $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("quick_tag") }}',
           type: 'POST',
           data: {name:name,keywords:keywords,brand_id:brand_id},
           success: function(response) {//alert(response);
            //alert(response);
            if(response !== "exist")
            {
               $('.edit_tag')
           .append($("<option></option>")
                      .attr("value",response)
                      .text(name)); 
                      var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Tag added successfully!!');
      x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          //             swal({   
          //     title: "Success!",   
          //     text: "Tag " + name + " is already added!" ,   
          //     timer: 500 ,   
          //     showConfirmButton: false 
          // });
            }

            else if (response == "exist")
            {
              var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('This tag already exist !!');
      x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
               $("#name").val('');
               $("#keywords").val('');
       $('#show-add-tag').modal('toggle');
           
          }
        });

  });

    $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
         });


  $('#admin_page_filter').change(function() {
      //alert($(this).val());
      var admin_page = $(this).val();
      var kw_search = $("#kw_search").val();
      ChooseDate(startDate,endDate,'','',kw_search);
     
       // $(this).val() will work here
  });

  // $(document).on('click', '.btn_tag', function () {
  //     var filter_tag=this.id;
  // var admin_page = $('#hidden-chip').val();

  //     getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag);
  // });
  $(document).on('click','.dropdown-menu a',function(){
  var row_id=this.id;
  row_id = row_id.substring(row_id.indexOf('_')+1);
  // alert(row_id);
        $("#btnaction_"+row_id+":first-child").text($(this).text());
        $("#btnaction_"+row_id+":first-child").val($(this).text());
        // $("#btnaction_"+row_id).removeClass('btn-red');
        // $("#btnaction_"+row_id).removeClass('btn-success');
        // if($(this).text() === "Require Action")
        // {
        // $("#btnaction_"+row_id).addClass('btn-red');
        // }
        // else
        // {
        //   $("#btnaction_"+row_id).addClass('btn-success');
        // }

         $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setActionUpdate") }}',
           type: 'POST',
           data: {id:row_id,brand_id:GetURLParameter('pid'),action_status:$(this).text()},
           success: function(response) { //alert(response)
            if(response>=0)
            {
         
            }
          }
              });

        //save action status and taken person in database
  });
  //dashboard function




        $(document).on('click', '#see_more', function () {
           // $('mention_show').css('display','none');
           $('#see_more').css('display','none');
           $('.btn_post_tag_hide').css('display','inline');
           $('#see_less').css('display','inline');
        });
        $(document).on('click', '#see_less', function () {
          $('.btn_post_tag_hide').css('display','none');
            $('.btn_post_tag_show').css('display','inline');
            $('#see_more').css('display','inline');
            $('#see_less').css('display','none');
        });

            $(document).on('click', '#more', function () {
           // $('mention_show').css('display','none');
           $('#more').css('display','none');
           $('.btn_cmt_tag_hide').css('display','inline');
           $('#less').css('display','inline');
        });
        $(document).on('click', '#less', function () {
          $('.btn_cmt_tag_hide').css('display','none');
            $('.btn_cmt_tag_show').css('display','inline');
            $('#more').css('display','inline');
            $('#less').css('display','none');
        });

          $(document).on('click', '.chip', function (e) {
    // alert('hello');
    var id = $(this).attr("id");  
    var name = $(this).attr("name");
    // alert(id);
    var chipcompetitor = document.getElementById("chip-competitor");
    //alert(name);  
      $(".chip").css("background-color","#e4e4e4");
      $(".chip").css("color","rgba(0,0,0,0.6)");
       // chipcompetitor.classList.remove("myHeaderChip");
     // chipcompetitor.classList.add("myHeaderChip");
     // $('.dropdown.open .dropdown-toggle').dropdown('toggle');
     // $("#search_form").css("visibility", "hidden");
     // $("#search_form").css("display", "inline");
      $("#"+id).css("background-color","rgba(188, 138, 49, 1)");
      $("#"+id).css("color","white");
      $(".hidden-chip").val(name);
      $(".hidden-chip").attr("id",id);
      // you don't understand and i can't explain
    // alert($("[data-toggle=dropdown]").attr("aria-expanded"))  ;
     $(".dropdown-pages").dropdown('toggle');
      var kw_search = $("#kw_search").val();
      ChooseDate(startDate,endDate,'','',kw_search);
      tagCount();
      posttagCount();
      
        });


      function InboundSentimentDetail(fday,sday){

    var brand_id = GetURLParameter('pid');
    let main = document.getElementById("fan-growth-chart");
    let existInstance = echarts.getInstanceByDom(main);
        if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }

      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

var sentiDetailChart = echarts.init(main);

    $("#fan-growth-spin").show();
      
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInboundSentiDetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id,periodType:'month',kw_search:$("#kw_search").val() }
    })
    .done(function( data ) {
     // console.log(data);
      var positive = [];
      var negative = [];
      var neutral=[];
      var sentimentLabel = [];

      var positive_total=0;
      var negative_total=0;
      var neutral_total=0;
      var all_total=0;


        for(var i in data) {//alert(data[i].mentions);
        positive.push(data[i].positive);
        negative.push(data[i].negative);
        neutral.push(data[i].neutral);
        sentimentLabel.push(data[i].periodLabel);
        
      }
      // alert(positive);


$.each(positive,function(){positive_total+=parseInt(this) || 0;});
$.each(negative,function(){negative_total+=parseInt(this) || 0;});
$.each(neutral,function(){neutral_total+=parseInt(this) || 0;});
all_total=positive_total+negative_total+neutral_total;
var positive_percentage=parseInt((positive_total/all_total)*100);
var negative_percentage= parseInt((negative_total/all_total)*100);
var neutral_percentage =  parseInt((neutral_total/all_total)*100);

          positive_percentage = isNaN(positive_percentage)?0:positive_percentage;
          negative_percentage = isNaN(negative_percentage)?0:negative_percentage;
          neutral_percentage = isNaN(neutral_percentage)?0:neutral_percentage;
         

    option = {
        color:colors,
      

 tooltip : {
            trigger: 'axis',
             /*formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
        console.log(rez); //quite useful for debug
        params.forEach(item => {
             console.log("hihi"); 
            console.log(item);value
            console.log(item.series.color); //quite useful for debug
            var xx = '<p>'   + colorSpan(item.series.color) + ' ' + item.seriesName + ': ' + item.data  + '</p>'
            rez += xx;
        });

        return rez;
    }*/

        },

        legend: {
            data:['Positive','Negative'],
           formatter: function (name) {
            if(name === 'Positive')
            {
                 return name + ': ' + positive_total;
            }
            return name  + ': ' + negative_total;

   
}
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
             axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
     var date = new Date(value);
     //  console.log(date);
      var texts = [date.getFullYear(), monthNames[date.getMonth()]];

    return texts.join('-');

}
    },
           data : sentimentLabel
        }
        ],
        yAxis : [
        {
            type : 'value',
            name: 'comment count',
        }
        ],
        series : [
        {
            name:'Positive',
            type:'bar',
            data:positive,
            barMaxWidth:30,
            color:colors[2],
            markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }/*,
            markLine : {
                data : [
                {type : 'average', name: 'average'}
                ]
            }*/
        },
        {
            name:'Negative',
          /*  type:effectIndex % 2 == 0 ? 'bar' : 'line',*/
            type:'bar',
            data:negative,
            barMaxWidth:30,
            color:colors[1],
            markPoint : {
              data : [
              {type : 'max', name: 'maximum'},
              {type : 'min', name: 'minimum'}
              ]
          }/*,
          markLine : {
            data : [
            {type : 'average', name : 'average value'}
            ]
        }*/
    }
    ]
};
$("#fan-growth-spin").hide();
sentiDetailChart.setOption(option, true), $(function() {
    // function resize() {
    //     setTimeout(function() {
    //         sentiDetailChart.resize()
    //     }, 100)
    // }
    // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

 sentiDetailChart.on('click', function (params) {
   // console.log(params);
   // console.log(params.name); // xaxis data = 2018-08
   // console.log(params.seriesName); //bar period name ="Positive"
   // console.log(params.value);//count
   // var pid = GetURLParameter('pid');
   var brand_id = GetURLParameter('pid');
   var admin_page_id=$('.hidden-chip').attr('id');
   var admin_page=$('.hidden-chip').val();

   var commentType ='';
   
   if(params.seriesName == 'Positive')
    commentType = 'pos'
    else
   commentType = 'neg'
   var default_page = $('.hidden-pg').val();
   var accountPermission = GetURLParameter('accountPermission');
  if(params.name !== 'minimum' && params.name !== 'maximum' )
   window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission, '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});
    
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
  

  $('input[type=radio][name=options]').change(function() {

    if (this.value == 'page') {
     var date_preset= Calculate_DatePreset(label);
        PageGrowth(GetStartDate(),GetEndDate(),date_preset);
        
    }
    else if (this.value == 'sentiment') {
        InboundSentimentDetail(GetStartDate(),GetEndDate());
    }
    
});
  $(".btnComment").click(function(e){
    var commentType = $(this).attr('value');
    var default_page = $('.hidden-pg').val();
    var admin_page_id=$('.hidden-chip').attr('id');
    var admin_page=$('.hidden-chip').val();

   var accountPermission = GetURLParameter('accountPermission');
    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
})
$(".btnPost").click(function(e){
    var postType = $(this).attr('value');
    var admin_page_id=$('.hidden-chip').attr('id');
    var admin_page=$('.hidden-chip').val();
    var accountPermission = GetURLParameter('accountPermission');
    window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&overall="+postType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&accountPermission="+accountPermission , '_blank');
})
  $(document).on('click', '.btn_frequent_tag', function() {
     var admin_page_id=$('.hidden-chip').attr('id');
     var admin_page=$('.hidden-chip').val();
     var TagName = $(this).attr('value');
     var default_page = $('.hidden-pg').val();
        
        var accountPermission=GetURLParameter('accountPermission');
      window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
  })
    $(document).on('click', '.btn_post_tag', function() {
     var TagName = $(this).attr('value');
     var default_page = $('.hidden-pg').val();
     var admin_page_id=$('.hidden-chip').attr('id');
     var admin_page=$('.hidden-chip').val();
      var accountPermission = GetURLParameter('accountPermission');
      // alert(accountPermission); 
      window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
  })
  
  //local function

  function numberWithCommas(n) {
      var parts=n.toString().split(".");
      var res = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
      if(res == '')
        res = 0

      return res;
  }

    function kFormatter(num) {
      return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
  }
    function readmore(message){
        // alert("hi hi ");
          var string = String(message);
          var length = string.length; 
           // alert(length);
                  if (length > 500) {
            // alert("length is greater than 500");

              // truncate

              var stringCut = string.substr(0, 500);
               // alert(stringCut);
              // var endPoint = stringCut.indexOf(" ");

              //if the string doesn't contain any space then it will cut without word basis.
               
              // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
              string =stringCut.substr(0,length);
              // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
              // alert(string);
          }
          return string;


          }

          

    });
      </script>
      <style type="text/css">
        .reaction-weight{
          text-align: center;
          font-weight:900;
        }
        .img-container {
  position: relative;
}

.img-container:after {
  content: " ";
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 99;
}

table.fixedHeader-floating {
     clear: both;
     position: fixed;
    top: 114px !important;
    z-index:5;
     top :0;
     left:0 !important;
     width: 100% !important;
  /*   margin: 0 auto;
       padding: 0 15px;*/
   /*    overflow-x: hidden;
        overflow-y: auto;*/
    
      box-sizing: border-box;
  /*
    background: red;*/
  /*  left:150px !important;*/
  /*  margin-right:30px;*/
   
  /*  width: 80% !important;*/
    /*background: transparent;*/
  } 
table.dataTable tbody tr td {
    word-wrap: break-word;
    word-break: break-all;
}

  .tabcontrolHeader {
     clear: both;
     position: fixed;
    top:120px !important;
    z-index:20;
   
  }


  .myHeaderContent
  {
     margin-right:300px;
  }
  .myHeaderChip
  {
    padding-right:100px;
  }
  .myHeaderScoll
  {
     padding-right:300px;
  }

  .dataTables_wrapper {
      padding-top: 0px;
  }
  img {
      border-style: none;
  }
  #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
  .btn-secondary:not(:disabled):not(.disabled).active, .btn-secondary:not(:disabled):not(.disabled):active, .show>.btn-secondary.dropdown-toggle {
    color: #fff !important;
    background-color: #545b62;
    border-color: #4e555b;
}
.btn {
    padding: 2px 6px;
    font-size: 14px;
    cursor: pointer;
}


      </style>
  <link href="{{asset('css/own.css')}}" rel="stylesheet">
  @endpush

  <!-- table.fixedHeader-floating {
     clear: both;
     position: fixed;
    top: 200px !important;
    z-index:5;
     top :0;
     left:0 !important;
     width: 100% !important;
  /*   margin: 0 auto;
       padding: 0 15px;*/
   /*    overflow-x: hidden;
        overflow-y: auto;*/
    
      box-sizing: border-box;
  /*
    background: red;*/
  /*  left:150px !important;*/
  /*  margin-right:30px;*/
   
  /*  width: 80% !important;*/
    /*background: transparent;*/
  } -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/bi-logo-fit.png')}}">
        <title>{{ config('app.name', 'Laravel') }} - @if (isset($title)) {{$title}} @endif</title>
        
        <!-- Bootstrap Core CSS -->
        <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/icheck/skins/all.css')}}" rel="stylesheet">
         <link href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
         <!-- range slider -->
         <link href="{{asset('assets/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/ion-rangeslider/css/ion.rangeSlider.skinModern.css')}}" rel="stylesheet">
        <!-- Page plugins css -->
        <link href="{{asset('assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
        <!-- Color picker plugins css -->
        <link href="{{asset('assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
        <!-- Date picker plugins css -->
        <link href="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Daterange picker plugins css -->
        <link href="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet">
        <!-- chartist CSS -->
        <link href="{{asset('assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/css-chart/css-chart.css')}}" rel="stylesheet">
        <!--This page css - Morris CSS -->
        <link href="{{asset('assets/plugins/morrisjs/morris.css')}}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
       
        <!-- You can change the theme colors from here -->
        <link href="{{asset('css/colors/green.css')}}" id="theme" rel="stylesheet">

        <link href="{{asset('assets/plugins/sweetalert2/sweetalert2.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=myanmar3' />
        <link href="{{asset('assets/plugins/select2/dist/css/select2.min.css')}}" id="theme" rel="stylesheet">
          <link href="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />
         <link href="{{asset('assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
         <!-- <link href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.dataTables.min.css" rel="stylesheet"/> -->
         <link href="{{asset('css/fixedHeader.dataTables.min.css')}}" rel="stylesheet"/>
         <link href="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
         <link href="{{asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" />
         <link href="{{asset('assets/plugins/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />

         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

         <!-- <link href="{{asset('assets/plugins/materialize/css/materialize.min.css')}}" rel="stylesheet" /> -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">

            .btn-group1, .btn-group-vertical1 
            {
                position: relative;
                display: -ms-inline-flexbox;
                display: inline-flex;
                vertical-align: middle;
            }
            .searchform
            {
                border: 0;
                border-bottom: 1px solid #8e8982;
                outline: 0;
                background:transparent;
                color:#fff;
                margin-top:17px;
            }
            .user-profile .profile-text{
                /* padding-top: 0; */
                padding-top: 10px;
                position: relative; }
                .user-profile .profile-img {
                width: 50%;
                margin-left: 0;
                /* height : 100px; */
                padding: 10px 0;
                padding-bottom: 0;
                border-radius: 100%; 
                margin : 0 auto;
            }
             .user-profile .profile-img::before {
              -webkit-animation: 2.5s blow 0s linear infinite;
              animation: 0s  0s  ;
              position: absolute;
              content: '';
              width: 50px;
              height: 50px;
              top: 35px;
              margin: 0 auto;
              border-radius: 50%;
              z-index: 0; }
              .user-profile .profile-text > a:after {
                position: absolute;
                right: 20px;
                top: 45px; }

            button.btn.btn-info.dropdown-toggle.dropdown-toggle-split::after {
                display: inline-block;
                width: 0;
                height: 0;
                margin-left: .255em;
                vertical-align: .255em;
                content: "";
                border-top: .3em solid;
                border-right: .3em solid transparent;
                border-bottom: 0;
                border-left: .3em solid transparent;
            }
                   /* .dropdown-menu {
                position: absolute;
                top: 100%;
                left: 0;
                 z-index: 1000; 
                display: none;
                float: left;
                min-width: 10rem;
                padding: .5rem 0;
                margin: .125rem 0 0;
                font-size: 1rem;
                color: #212529;
                text-align: left;
                list-style: none;
                background-color: #383f48;
                background-clip: padding-box;
                border: 1px solid rgba(0,0,0,.15);
                border-radius: .25rem;
            }*/


            /*    .label-light-success {
                background-color: #e8fdeb;
                color: #fcb22e;
            }*/
            .search { position: relative; }
            .search input { text-indent: 135px;padding-left:25px}
            .search .text { 
                font-size: 1rem;
              position: absolute;
              top: 9px;
              left: 7px;
              font-size: 15px;
            }
        </style>
    </head>

    <body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="app">
    <div id="main-wrapper">
      
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" style="background:rgba(0, 0, 0, 0.05)">
                  <!--   <a class="navbar-brand" href="" style="color:#e8ebec;"> -->
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="{{asset('assets/images/favicon.png')}}" alt="homepage" class="dark-logo" /> -->
                            <!-- Light Logo icon -->
                            <img src="{{asset('assets/images/biminer_white_fit.png')}}" style ="width:120px;height:52px;" alt="logo" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <!-- <b> -->
                        <!-- <span style="font-size:15px;padding-right: 100px;"> -->
                        <!-- style="color:#FFFFFF;font-size:18px"  -->
                         <!-- dark Logo text -->
                        <!--  <img src="{{asset('assets/images/logo-text.png')}}" alt="homepage" class="dark-logo" /> -->
                         <!-- Light Logo text -->  
                        <!--  <img src="{{asset('assets/images/individual/Tharapa_Social_Listener_white_180x19.png')}}" class="light-logo" style="width:180px" alt="logo text" /> -->
                        <!-- BI Miner -->
                       <!-- </span> </a> -->
                         <!-- Tharapa Social Listener</span> </a> -->
                        <!-- </b> -->
                </div>

                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
            


                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                      @yield('filter')
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
           
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                       @yield('admin_page_filter')
                            <li class="nav-item" style="display:none"> 
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info">  @if (isset($source) and $source === 'out' )
                                        Other Pages
                                        @else
                                        My Pages
                                        @endif
                                    </button>
                                    <button value="{{$source}}" id="btn_source" type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{route('brandList', ['source' => 'in'])}}">My Pages</a>
                                        <a class="dropdown-item" href="{{route('brandList', ['source' => 'out'])}}">Other Pages</a>
                                    </div>
                                </div>
                            </li>
                    </ul>
                   
                       
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                         @yield('comparison') 
                         <form action=""  <?php if(request()->is('competitor')) {?> style="display: inline" <?php } else{?> style="display: none;" <?php }?>>
                        <div class="form-group">
                           <i class="ti-search" style="color:#8e8982"></i>  <input type="text" placeholder="Search by keyword...." class="searchform" id="kw_search">
                        </div>
                    </form>
                        <li class="nav-item dropdown" id="cron_status_text" style="display: none">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                <span style="font-size: 12px">Data Updating ...</span>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('assets/images/users/9.jpg')}}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{asset('assets/images/users/9.jpg')}}" alt="user"></div>
                                                <div class="u-text">
                                                    <h4>{{ Auth::user()->name }}</h4>
                                                    <p class="text-muted">{{ Auth::user()->email }}</p>
                                                     @if (isset($project_data) && count($project_data)>0)
                                                     <a href="{{route('users.edit', ['id' => Auth::user()->id,'source'=>$source,'pid'=>$project_data[0]['id']])}}" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
                                                     @else
                                                     <a href="javascript:void(0)" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
                                                     @endif
                                                </div>
                                        </div>
                                    </li>
                                    <!--     <li role="separator" class="divider"></li>
                                        <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                        <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                        <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                                    <li role="separator" class="divider"></li>
                                    @if (isset($project_data) && count($project_data)>0)
                                    <li><a href="{{route('companyInfo.index', ['pid'=>$project_data[0]['id']])}}"><i class="ti-settings"></i> Company Information</a></li>
                                    @endif
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" ><i class="fa fa-power-off"></i> Logout</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </div>
                        </li>
        
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url({{asset('')}}) no-repeat;text-align:center;border-bottom:1px solid rgba(0,0,0,.1)">
                       <!--  <div class="user-profile" id="user-profile" style=""> -->
                            <!-- User profile image -->
                        @if (isset($companyData))
                        @if (isset($companyData) && count($companyData)>0)
                        @if (auth()->user()->username == 'wavemoney')
                        <div class="profile-img"> <img  src="{{asset('Logo/wavemoney.png')}}"  style="width:40px;height:40px"/> </div>
                        @else
                        <div class="profile-img"> <img  src="{{asset('Logo/'.$companyData['logo_path'])}}"  style="width:40px;height:40px"/> </div>
                        @endif
                        @else
                        <div class="profile-img"> <img  src="{{asset('Logo/companyLogo_sample.png')}}" /> </div>
                        @endif
                        @endif
                        <!-- User profile text-->
                        <!-- class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" -->
                        <!-- <div class="profile-text" > <a href="#" > @if (isset($project_data) && count($project_data)>0) {{$project_data[0]['name']}} @endif</a>
                            <div class="dropdown-menu animated flipInY">
                                <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                                <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                                <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                                <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                              
                            </div>
                        </div> -->
                         @if (auth()->user()->username == 'wavemoney')
                      <div style="padding:5px 0" class="profile-text">  <span style="color: #455a64;font-weight: 500;"> @if (isset($project_data) && count($project_data)>0) Wave Money @endif</span></div>
                      @else
                      @if(isset($companyData['name']))
                      <div style="padding:5px 0" class="profile-text">  <span style="color: #455a64;font-weight: 500;"> @if (isset($project_data) && count($project_data)>0) {{$companyData['name']}} @endif</span></div>
                      @endif
                      @endif
                </div>
              
                <!-- End User profile text-->
                <!-- Sidebar navigation-->

                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                @if (isset($project_data))
                @foreach($project_data as $project_data)
                    <!--  <li class="{{ request()->is($project_data->id) ? 'active' : '' }}">
                   <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu"> {{ $project_data->name }} </span></a> -->
                                <!--     <ul aria-expanded="false" class="collapse"> -->
                     <!-- <li class="{{ request()->is('dashboard') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('dashboard', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-home"></i><span class="hide-menu"> Dashboard</span></a></li>

                    <li class="nav-devider"></li> -->
                    <li class="nav-small-cap">Data</li>
                    <li class="{{ request()->is('competitor','relatedcomment','relatedpost') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('competitor', ['pid' => $project_data->id,'source'=>$source,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-view-parallel"></i><span class="hide-menu">Pages</span></a></li>
                       
                    <li class="{{ request()->is('post','relatedmention','relatedarticle') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('post', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-view-module"></i><span class="hide-menu">Mentions</span></a></li>

                     <li class="{{ request()->is('industry') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('industry', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-view-module"></i><span class="hide-menu">Industry</span></a></li>

                      <li class="{{ request()->is('otherSources') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('otherSources', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-view-module"></i><span class="hide-menu">Other Sources</span></a></li>

                   <!--  <li class="{{ request()->is('comment','highlightedcomment','relatedcomment','relatedcompetitorcomment') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('comment', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-tooltip-text"></i><span class="hide-menu">Comment</span></a></li> -->
                  
                   
                    

                    <li class="{{ request()->is('campaign') || request()->is('relatedcampaignmention') ? 'active' : '' }}"  class="nav-small-cap" style="display: ;"><a href="{{route('campaign', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-view-parallel"></i><span class="hide-menu">Campaign</span></a></li>

                    <li class="{{ request()->is('group_post')  ? 'active' : '' }}"  class="nav-small-cap" style="display: ;"><a href="{{route('group_post', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-view-parallel"></i><span class="hide-menu">Group Posts</span></a></li>
                    @if(auth()->user()->brand_id !== 40)
                     <li class="{{ request()->is('chargesAndRates') || request()->is('exchange_compare') || request()->is('exchangeEntry') ? 'active' : '' }}"  class="nav-small-cap" style="display: ;"><a href="{{route('chargesAndRates', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-view-parallel"></i><span class="hide-menu">Charges and Rates</span></a></li>
                     @endif

                    <li class="nav-devider"></li>
                    <li class="nav-small-cap">Compare</li>
                    <li class="{{ request()->is('keygroupcomparison') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('keygroupcomparison', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-compare"></i><span class="hide-menu">Mentions Compare</span></a></li>
                    <li class="{{ request()->is('comparison') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('comparison', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-compare"></i><span class="hide-menu">Pages Compare</span></a></li>
                    <li class="{{ request()->is('benchmarking') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('benchmarking', ['pid' => $project_data->id,'source'=>$source,'accountPermission' => $accountPermission])}}"><i class="mdi mdi-compare"></i><span class="hide-menu">Benchmarking</span></a></li>
      
           
                    <!--  <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Setting</a></li> -->
                         <!--    </ul> -->
                       <!--  </li> -->
                @endforeach
                @endif  
           
                   <!--      <li class="nav-devider"></li>
                        <li class="nav-small-cap">Administration</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Authentication</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('register', ['source'=>$source])}}">User Registration</a></li>
                                
                            </ul>
                        </li> -->
                     
                    <!-- {{ Request::is('tags*')}} -->       
                      
                    <li  class="nav-devider"></li>
                    <!--     <li class="nav-small-cap">Setting</li> -->
                    <li class="@if (Request::is('brandList/*') || Request::is('keyword-group/*') || Request::is('tags/*') || Request::is('tagsGroup/*') || Request::is('users/*')) {{'active'}}  @endif" > <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Setting</span></a>
                        <ul aria-expanded="false"  class="collapse">
                          <!-- <li class="{{ request()->is('brandList/*') ? 'active' : '' }}" class="nav-small-cap">  <a class="{{ request()->is('brandList/*') ? 'active' : '' }}" href="{{route('brandList', ['source'=>$source])}}">
                            <i class="mdi mdi-plus-box"></i><span class="hide-menu"> Brand List</span>
                          </a></li> -->
                            <li class="nav-item {{ str_is('brandList*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class=" {{ str_is('brandList*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('brandList', ['source'=>'in','accountPermission' => $accountPermission])}}"><i class="mdi mdi-tag"></i> Page Group </a></li>
                            <li class="nav-item {{ str_is('campaign*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class=" {{ str_is('campaign*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('campaignlist', ['source'=>'in','accountPermission'=>$accountPermission])}}"><i class="mdi mdi-tag"></i> Campaign Group </a></li>
                             <li class="nav-item {{ str_is('audience*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class=" {{ str_is('audience*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('audience', ['source'=>'in','accountPermission'=>$accountPermission])}}"><i class="mdi mdi-tag"></i> Campaign Audience </a></li>
                              <li class="nav-item"  class="nav-small-cap"><a class="" href="{{route('mentionPageList', ['source'=>'in','accountPermission'=>$accountPermission])}}"><i class="mdi mdi-tag"></i> Mention Page Tier </a></li>
                             
                            @if (isset($project_data) && count($project_data)>0)
                              <li class="nav-item"  class="nav-small-cap"><a class="" href="{{route('wordcloud.index', ['source'=>'in','pid'=>$project_data->id,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-tag"></i> WordCloud Entry </a></li>
                            <li class="nav-item {{ str_is('keyword-group*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class=" {{ str_is('keyword-group*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('keyword-group',['pid'=>$project_data->id,'source'=>'in','accountPermission'=>$accountPermission])}}"><i class="mdi mdi-tag"></i> Mention Group </a></li>
                           <li class="nav-item {{ str_is('other-keyword-group*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class=" {{ str_is('other-keyword-group*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('other-keyword-group',['pid'=>$project_data->id,'source'=>'in','accountPermission'=>$accountPermission])}}"><i class="mdi mdi-tag"></i> Other Mention Group </a></li>
                            <li class="nav-item {{ Request::segment(1)=='tags' ? 'active' : '' }}"  class="nav-small-cap">
                            <a class="{{ Request::segment(1)=='tags' ? 'active' : '' }}" href="{{route('tags.index', ['pid'=>$project_data->id,'source'=>'in','accountPermission' =>$accountPermission])}}"><i class="mdi mdi-tag"></i> Tags</a></li>
                            <li class="nav-item {{ Request::segment(1)=='tagsGroup' ? 'active' : '' }}"  class="nav-small-cap">
                            <a class=" {{ Request::segment(1)=='tagsGroup' ? 'active' : '' }}"  href="{{route('tagsGroup.index', ['pid'=>$project_data->id,'source'=>'in','accountPermission'=>$accountPermission])}}"><i class="mdi mdi-tag"></i> Tags Group</a>
                            </li>
                            <!-- <li class="nav-item {{ str_is('auto_tagging*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap">
                            <a class=" {{ str_is('auto_tagging*', Request::route()->getName()) ? 'active' : '' }}"  href="{{route('autoTagging', ['pid'=>$project_data->id,'source'=>'in','accountPermission'=>$accountPermission])}}"><i class="mdi mdi-tag"></i> Beverage Auto Tagging</a>
                            </li> -->
                            <li class="nav-item {{ str_is('companyInfo*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class="nav-item {{ str_is('companyInfo*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('companyInfo.index', ['pid'=>$project_data->id,'source'=>'in','accountPermission'=>$accountPermission])}}"><i class="mdi mdi-bank"></i> Company Info</a></li>

                             <li class="nav-item {{ str_is('', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap">
                                <a class="nav-item {{ str_is('', Request::route()->getName()) ? 'active' : '' }}" 
                                    href="{{route('edit-picture', ['pid'=>$project_data->id,'source'=>'in','accountPermission'=>$accountPermission])}}">
                                    <i class="mdi mdi-tag"></i> Edit Picture
                                </a>
                            </li>

                            <li class="nav-item {{ str_is('users*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class="nav-item {{ str_is('users*', Request::route()->getName()) ? 'active' : '' }}"  href="{{route('register', ['source'=>$source,'pid'=>$project_data->id,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-account-plus"></i> User </a></li>
                            @endif

                    </ul>
                </li>
                   
                <li class="nav-devider"></li>
                    <!--     <li class="nav-small-cap">Setting</li> -->
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-chart-line"></i><span class="hide-menu">Report</span></a>
                    @if (isset($project_data) && count($project_data)>0)
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('sentMail', ['pid' => $project_data->id,'source'=>$source,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-note-text"></i> Mail Reporting</a></li>
                      <!--   <li><a href="{{route('rpt-sentipredict', ['pid' => $project_data->id,'source'=>$source,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-note-text"></i> Manual Vs Predict</a></li>
                        <li><a href="{{route('rpt-humanpredict', ['pid' => $project_data->id,'source'=>$source,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-note-text"></i> Manual Input</a></li>
                        <li><a href="{{route('rpt-postDetail-export', ['pid' => $project_data->id,'source'=>$source,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-note-text"></i> Export Data</a></li>
                        <li><a href="{{route('rpt-analysis', ['pid' => $project_data->id,'source'=>$source,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-note-text"></i> Print Mention Data</a></li>
                        <li><a href="{{route('rpt-pagesAnalysis', ['pid' => $project_data->id,'source'=>$source,'accountPermission'=>$accountPermission])}}"><i class="mdi mdi-note-text"></i> Print Pages Data</a></li> -->
                    </ul>
                    @endif
                </li>
            </ul>
        </nav>
    </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
    <div class="sidebar-footer" style="display:none">
        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
        <!-- item-->
        <a href="#" class="link" data-toggle="tooltip" title="Logout" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a>
    </div>
</aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                @yield('content')
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 BI Miner by Bagan Innovation Technology</footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
</div>
    <!-- ============================================================== -->
   <!-- defer -->
    <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('.sidebartoggler').on('click', function() {
                 if(document.body.classList.contains('mini-sidebar')){
                   $('.light-logo').css('display', 'none');
                   // add your small img
                  
                   $('.navbar-header').append('<b><img src="{{asset("assets/images/biminer_white_fit.png")}}" style ="width:120px;height:52px;" alt="logo" class="light-logo"></b>');
                  }

                   else{
                    $('.light-logo').css('display', 'none');
                    // add your big img
                     $('.navbar-header').append('<b><img src="{{asset("assets/images/Bi_White.png")}}" style ="width:70px;height:60px;" alt="logo" class="light-logo"></b>');
                    }
   
            });

              var GetURLParameter = function GetURLParameter(sParam) {
              var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                  sURLVariables = sPageURL.split('&'),
                  sParameterName,
                  i;

              for (i = 0; i < sURLVariables.length; i++) {
                  sParameterName = sURLVariables[i].split('=');

                  if (sParameterName[0] === sParam) {
                      return sParameterName[1] === undefined ? true : sParameterName[1];
                  }
              }
          };

            // getCronStatus();

            function getCronStatus()
            {
                  $.ajax({
                  type: "GET",
                  dataType:'json',
                  contentType: "application/json",
                  url: "{{route('getCronStatus')}}",
                  data: { pid:GetURLParameter('pid')}
                })
                .done(function( data ) {//$("#popular").html('');
                   // alert(data);
                   if(parseInt(data)>0)
                   {
                    $('#cron_status_text').show();
                   }
                   else
                   {
                     $('#cron_status_text').hide();
                   }
                 
                })
                .fail(function(xhr, textStatus, error) {

                });
            }
                // function getCompanyInfo()
                // {

                //      $.ajax({
                //       type: "GET",
                //       dataType:'json',
                //       contentType: "application/json",
                //       url: "{{route('getCompanyInfo')}}"
                //     })
                //     .done(function( data ) {//$("#popular").html('');
                //         console.log(data);
                //        for(var i in data) {
                //       var image_name=data[i].logo_path;
                    
                //         $('#user-profile').css({"background-image": "url({{asset('Logo')}}/"+image_name+")", 'background-repeat': 'no-repeat'});
                //      }
                //     })
                //     .fail(function(xhr, textStatus, error) {

                //     });

                // }
                // getCompanyInfo();
                 
                // $('#user-profile').css({"background-image":"url(../storage/app/   Logo/1543393588.wdXssepUmwyY8aAsuZDoj4jUmAswJv2mjam5v5UX.png)  "});

            });

    </script>
    @stack('scripts')

 </body>
 </html>
@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
  @section('content')
  @section('filter')

  <li>
    <div class="col-lg-12">
	   	<select class="form-control" id="page_name" style="width: auto;margin-top:12px;">
	   		@foreach($pages as $page)
         @if($page->page_name == 'YKKO')
          <option value="{{ $page->page_name }}" selected> {{ $page->page_name }} </option>
         @else
          <option value="{{ $page->page_name }}"> {{ $page->page_name }} </option>
         @endif

				
			 @endforeach   		
	   	</select>
    </div>
</li>

<li>
    <div class="col-lg-12">
	   	<select class="form-control" id="year" style="width: auto;margin-top:12px;">
				<option value="2018"> 2018 </option>  
				<option value="2019" selected> 2019 </option>
				<option value="2020"> 2020 </option>		
	   	</select>
    </div>
</li>

  <li  class="nav-item dropdown mega-dropdown col-lg-12" > <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark dropdown-pages" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search text-warning"></i> <span class="text-warning" id="select_page"></span></a>
    <div id="search_form" class="dropdown-menu scale-up-left" id="dlDropDown"  style="padding-left: 5%">
      <ul class="mega-dropdown-menu row">
        <li class="col-lg-12" >
           <form>
              <input type="hidden" value='' id="hidden-chip" name="hidden-chip"/>
               <input type="hidden" value="{{ auth()->user()->username }}" id="hidden-user" name="hidden-user"/>
                  <div id="chip-competitor" class="col-md-12 align-self-center"  style="text-align:center;min-height:70px;padding-left:20px;padding-top:10px;border: 1px solid #f9f9f8;">
                  </div>
           </form>
        </li>
      </ul>
    </div>
</li>

 

 @endsection

    <!-- ============================================================== -->
                  <!-- Bread crumb and right sidebar toggle -->
                  <!-- ============================================================== -->
                
                   
                   <header class="" id="myHeader">
                    <div class="row page-titles">
                      <div class="col-md-7 col-6 align-self-center">
                          <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                                   <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#dashboard" role="tab">Summary</a> </li>
                                   <li class="nav-item"> <a class="nav-link " data-toggle="tab" id="pages_tab" href="#pages" role="tab"><span id="title_pages">Pages</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" id="mention_tab" href="#mention" role="tab"><span id="title_mentions">Mentions</span></a> </li>
                                    <input type="hidden"  id="PostTotal">
                                    <input type="hidden"  id="CommentTotal">
                                  
                              </ul>
                              <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
                      </div>
                      <div class="col-md-5 col-6 align-self-center">
                        <!-- <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                          <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <div class="chart-text m-r-10">
                              <div class='form-material input-group'>
                              </div></div>
                          </div>
                          <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                             <input type='text' class="form-control dateranges" style="datepicker" />
                              <div class="input-group-append">
                                <span class="input-group-text">
                                <span class="ti-calendar"></span>
                                </span>
                              </div>
                          </div>
                        </div> -->
                      </div>
                    </div>
                  </header>
                              <div class="row" id="top-div" >
                    <div  style="width: 25%;padding-right: 10px;" >
                        <div class="card card-inverse " style="background: #efc324">
                            <div class="box text-center" >
                                <a href="javascript:void(0)" class="link_page_post"><h1 class="font-light text-white" id='pages_post_total'>0</h1></a>
                                <a href="javascript:void(0)" class="link_page_post"><h6 class="text-white"> Post total</h6></a>
                            </div>
                        </div>
                    </div>
                    <div class="" style="width: 25%;padding-right: 10px;" >
                        <div class="card card-inverse card-info">
                            <div class="box bg-info text-center">
                                <a href="javascript:void(0)" class="link_page_comment"><h1 class="font-light text-white" id='pages_comment_total'>0</h1></a>
                                <a href="javascript:void(0)" class="link_page_comment"><h6 class="text-white"> Comment total</h6></a>
                            </div>
                        </div>
                    </div>
                 
                     <div class="" style="width: 25%;padding-right: 10px;">
                        <div class="card card-inverse" style="background:#73b36e">
                            <div class="box text-center">
                                <a href="javascript:void(0)" class="link_article"><h1 class="font-light text-white" id='article_total'>0</h1></a>
                                <a href="javascript:void(0)" class="link_article"><h6 class="text-white">Articles</h6></a>
                            </div>
                        </div>
                    </div>
                    <div class="" style="width: 25%;padding-right: 10px;">
                        <div class="card card-primary card-inverse">
                            <div class="box text-center">
                               <a href="javascript:void(0)" class="link_mention_post"><h1 class="font-light text-white" id="top_post_total">0</h1></a>
                               <a href="javascript:void(0)" class="link_mention_post"><h6 class="text-white">Mentioned Posts</h6></a>
                            </div>
                        </div>
                    </div>
                   
                    <!-- <div class="" style="width: 20%;visibility:collapse;">
                        <div class="card card-inverse card-success">
                            <div class="box text-center">
                               <a href="javascript:void(0)" class="link_mention_cmt"><h1 class="font-light text-white" id="top_comment_total">0</h1></a>
                                <a href="javascript:void(0)" class="link_mention_cmt"><h6 class="text-white">Mentioned Comments</h6></a>
                            </div>
                        </div>
                    </div> -->
                  </div>



               
   <div class="row">
   <div class="col-lg-12" id="outer_div">
      <div class="tab-content">
        <div class="tab-pane active" id="dashboard" role="tabpanel">
          <div class="row" style="margin-right:-10px;margin-left:-10px;">
<!--             <div class="col-lg-4" style="display: none;">
              <div class="card">
                <div class="card-header">
                    <h4 class="card-title m-b-0">Most Frequently Tags</h4>
                </div>
                <div class="card-body b-t collapse show " style="padding:0.5rem !important">
                    <div style="display:none"  align="center" style="vertical-align: top;" id="tag-count-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                   <div class="tag_count"> </div>
               </div>
             </div>
            </div> -->
             <div class="col-lg-6" >
             <div class="card" style="height:200px">
                <div class="card-header">
                  <h4 class="card-title m-b-0">Total Engagement</h4>
                </div>
                <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                  <div style="display:none"  align="center" id="cmt_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                    <div class="card-body text-center " style="padding:0.5rem !important">
                      <h1 class="card-title m-t-10" style="font-size:40px" id="engage_total">0</h1>
                    </div>
                    <div class="card-body text-center" style="padding:0rem !important">
                        <ul class="list-inline m-b-0">
                           <li style="padding:0 2px;">
                               <h6 id="reaction_total" class="text-warning" style="font-size:13px">0</h6>
                              <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-warning " style="font-size:10px" value="neutral">Reaction</button></div> </li>
                          <li style="padding:0 2px;">
                               <h6 id="share_total" class="text-success" style="font-size:13px">0</h6>
                              <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-success " style="font-size:10px" value="pos">Share</button></div> </li>
                        </ul>
                    </div>
                </div>
              </div>
          </div>      
           <div class="col-lg-4" style="display: none;">
             <div class="card" style="height:200px">
                <div class="card-header">
                  <h4 class="card-title m-b-0">Estimated Audience</h4>
                </div>
                <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                  <div style="display:none"  align="center" id="cmt_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                    <div class="card-body text-center " style="padding:0.5rem !important">
                      <h1 class="card-title m-t-10" style="font-size:40px" id="audience_total">0</h1>
                    </div>
                    <!-- <div class="card-body text-center" style="padding:0rem !important">
                        <ul class="list-inline m-b-0">
                          <li>
                              <h6 id="audience_reaction"class="text-warning" style="font-size:15px">0</h6>
                              <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnMentionCmt" value="neg">Reaction</button></div> </li>
                          <li>
                               <h6 id="audience_share" class="text-success" style="font-size:15px">0</h6>
                              <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnMentionCmt" value="neutral">Share</button></div> </li>  
                          <li>
                               <h6 id="audience_comment" class="text-red" style="font-size:15px">0</h6>
                              <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnMentionCmt" value="pos">Comment</button></div> </li>
                        </ul>
                    </div> -->
                </div>
              </div>
          </div>      
            <div class="col-lg-6">
              <div class="card" style="height:200px">
                <div class="card-header">
                  <h4 class="card-title m-b-0">Page Comments</h4>
                </div>
                <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                  <div style="display:none"  align="center" id="post_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                    <div class="card-body text-center " style="padding:0.5rem !important">
                      <h1 class="card-title m-t-10" style="font-size:40px" id="pagecmt_total">0</h1>
                    </div>
                    <div class="card-body text-center " style="padding:0rem !important">
                      <ul class="list-inline m-b-0">
                        <li style="padding:0 2px;">
                          <h6 id="incmt_neg" class="text-red" style="font-size:13px">0%</h6>
                          <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnPageCmt" style="font-size:10px" value="neg">Negative</button></div> </li>
                         <li style="padding:0 2px;">
                           <h6 id="incmt_neutral" class="text-warning" style="font-size:13px">0%</h6>
                          <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnPageCmt" style="font-size:10px" value="neutral">Neutral</button></div> </li>
                          <li style="padding:0 2px;">
                           <h6 id="incmt_pos" class="text-success" style="font-size:13px">0%</h6>
                          <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnPageCmt" style="font-size:10px" value="pos">Positive</button></div> </li>
                      </ul>
                    </div>
                </div>
              </div>
           </div>
           <!-- <div class="col-lg-3">
             <div class="card" style="height:200px">
                <div class="card-header">
                  <h4 class="card-title m-b-0">Mention Comments</h4>
                </div>
                <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                  <div style="display:none"  align="center" id="cmt_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                    <div class="card-body text-center " style="padding:0.5rem !important">
                      <h1 class="card-title m-t-10" style="font-size:40px" id="mentioncmt_total">0</h1>
                    </div>
                    <div class="card-body text-center" style="padding:0rem !important">
                        <ul class="list-inline m-b-0">
                          <li style="padding:0 2px;">
                              <h6 id="cmt_neg"class="text-red" style="font-size:13px">0%</h6>
                              <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnMentionCmt" style="font-size:10px"  value="neg">Negative</button></div> </li>
                          <li style="padding:0 2px;">
                               <h6 id="cmt_neutral" class="text-warning" style="font-size:13px">0%</h6>
                              <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnMentionCmt" style="font-size:10px"  value="neutral">Neutral</button></div> </li>
                          <li style="padding:0 2px;">
                               <h6 id="cmt_pos" class="text-success" style="font-size:13px">0%</h6>
                              <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnMentionCmt" style="font-size:10px"  value="pos">Positive</button></div> </li>
                        </ul>
                    </div>
                </div>
              </div>
          </div>       -->
        </div>
        <div class="row" style="margin-right:-10px;margin-left:-10px">
            <div class="col-lg-12">
            <div class="card" style="height:450px">
              <div class="card-header">
                <div class="card-actions">
                  <div class="btn-group1" data-toggle="buttons">
                    <label class="btn btn-secondary active">
                        <input type="radio" name="options" id="option3" value="mention" autocomplete="off"> Mention
                    </label>
                    <label class="btn btn-secondary">
                        <input type="radio" name="options" id="option2" value="sentiment" autocomplete="off"> Sentiment
                    </label>
                  </div>
                </div>
                <h4 class="card-title m-b-0"></h4>
             </div>
             <div class="card-body b-t collapse show">
                 <div style="display:none"  align="center" style="vertical-align: top;" id="fan-growth-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                <div id="fan-growth-chart" style="width:100%;height:350px"></div>
             </div>
           </div>
         </div>
      </div>
       <div class="row" style="margin-right:-10px;margin-left:-10px;display: none;">
        <div class="col-xlg-12 col-lg-12 col-md-12">
          <div class="card" >
            <div class="card-header">
              <div class="row">
                <div class="col-md-8 col-4 align-self-center" style="padding-left:1.25rem">
                  <h4 class="card-title m-b-0">Tag Sentiment</h4>
                </div>
              </div>
            </div>
            <div class="card-body b-t collapse show">
              <div style="display:none"  align="center" style="vertical-align: top;" id="tag-senti-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
              <div id="tag-senti-chart" style="width:100%;height:320px"></div>
            </div>
          </div>
        </div>
      </div>
    </div>  
    <div class="tab-pane" id="pages" role="tabpanel">
      <div class="row" style="margin-right:-10px;margin-left:-10px">
        <div class="col-lg-12" id="post_div">
          <div class="card" >
           <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;">
             <li class="nav-item" style="width:50%;text-align:center"> <a class="nav-link active" data-toggle="tab" href="#post_post" role="tab"><span id="title_post">Posts</span></a> </li>
             <li class="nav-item" style="width:50%;text-align:center"> <a class="nav-link" data-toggle="tab" href="#post_comment" role="tab"><span id="title_comment">Comments</span></a> </li>
          </ul>
          <div class="card-body b-t collapse show" style="padding-top:0px">
            <div class="tab-content">
              <div class="tab-pane active" id="post_post" role="tabpanel">
                <div class="row" >
                  <div class="col-lg-12">
                    <div class="card-body b-t collapse show" style="padding-top:0px">
                      <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                        <div class="table-responsive">
                        <table id="tbl_post"  class="table" style="margin-top:4px;" >
                        <thead>
                          <tr>
                            <th></th>
                            <th>Reaction</th>
                            <th>Shares</th>
                            <!-- <th>Overall</th> -->
                          </tr>
                        </thead>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
             </div>
             <div class="tab-pane" id="post_comment" role="tabpanel">
              <div class="row" >
               <div class="col-lg-12">
                <div class="card-body b-t collapse show" style="padding-top:0px">
                  <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                    <div class="table-responsive">
                      <table id="tbl_comment" class="table" style="width:100%;">
                        <thead>
                          <tr>
                            <th></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
</div>


    <div class="tab-pane" id="mention" role="tabpanel">
      <div class="row" style="margin-right:-10px;margin-left:-10px">
        <div class="col-lg-12" id="post_div">
          <div class="card" >
           <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;">
             <li class="nav-item" style="width:50%;text-align:center;"> <a class="nav-link" data-toggle="tab" href="#article" id="article_tab" role="tab"><span id="title_article">Articles</span></a> </li>
             <li class="nav-item" style="width:50%;text-align:center;"> <a class="nav-link active" data-toggle="tab" href="#mention_post" role="tab"><span id="title_mention_post">Posts</span></a> </li>
             <!-- <li class="nav-item" style="width:33%;text-align:center;visibility:collapse;"> <a class="nav-link" data-toggle="tab" href="#mention_comment" role="tab"><span id="title_mention_comment">Comments</span></a> </li> -->
            </ul>
           
              <div class="tab-content">
                <div class="tab-pane" id="article" role="tabpanel">
                  <div class="card-body">
                    <div style="display:none"  align="center" style="vertical-align: top;" id="article-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                      <div class="table-responsive">
                        <table id="tbl_article"  class="table" style="margin-top:4px;" width="100%">
                          <thead style="display: none;"> </thead>
                        </table>
                      </div>
                  </div>
              </div>  
                <div class="tab-pane active" id="mention_post" role="tabpanel">
                      <div class="card-body b-t collapse show" style="padding-top:0px">
                        <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                        <div class="table-responsive">
                          <table id="tbl_mention_post"  class="table" style="margin-top:4px;" >
                          <thead>
                            <tr>
                              <th></th>
                              <th>Reaction</th>
                              <th>Shares</th>
                              <!-- <th>Overall</th> -->
                            </tr>
                          </thead>
                         </table>
                        </div>
                     </div>
                 </div>
                 <div class="tab-pane" id="mention_comment" role="tabpanel">
                      <div class="card-body b-t collapse show" style="padding-top:0px">
                        <div style="display:none"  align="center" style="vertical-align: top;" id="mention-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           <div class="table-responsive">
                              <table id="tbl_mention_comment" class="table" style="width:100%;">
                              <thead>
                                <tr>
                                  <th></th>
                                </tr>
                              </thead>
                            </table>
                          </div>
                       </div>
                     </div>
                 </div>
             </div>
            </div>
          </div> 
        </div>       
      </div>
    </div>
                <!-- filter -->
    <div class="col-lg-3" id="filter_div" style="display: none">
      <div class="card earning-widget" style="height:230px">
        <div class="card-header">
          <h4 class="card-title m-b-0">Sentiment</h4>
        </div>
        <div class="card-body b-t collapse show">
          <div class="form-group">
            <div class="input-group">
            <ul class="icheck-list sentiment-list" style="padding-top:0px">
             <li>
              <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="all" id="senti-all"> 
              <label  for="senti-all" style="padding-left:30px;font-weight:500;font-size:12px">all</label>
              </li>
              <li>
                <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="pos" id="senti-pos"> 
                <label class="text-success" for="senti-pos" style="padding-left:30px;font-weight:500;font-size:12px">positive</label>
              </li>
              <li>
               <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neg" id="senti-neg"> 
                <label class="text-danger" for="senti-neg" style="padding-left:30px;font-weight:500;font-size:12px">negative</label>
              </li>
              <li> 
              <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neutral" id="senti-neutral"> 
              <label class="text-info"   for="senti-neutral" style="padding-left:30px;font-weight:500;font-size:12px">neutral</label>
              </li>                           
            </ul>
           </div>
         </div>
       </div>
     </div>

    <div class="card earning-widget" style="height:550px;visibility: collapse;">
      <div class="card-header">
        <h4 class="card-title m-b-0">Keywords</h4>
     </div>
      <div class="card-body b-t collapse show" style="overflow-y:scroll;height:550px">
        <div class="form-group">
          <div class="input-group">
              <ul class="icheck-list keyword-list" style="padding-top:0px">
              </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
          <div id="show-image" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div align="center" class="modal-body large_img">
                    </div>
                </div>
            </div>  
         </div>
        <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title" id="postModal">Post Detail</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  </div>
                  <div class="modal-body post_data">
                  </div>
                    <!-- <div class="modal-footer">
                      <button type="button" id="btnSeeComment" class="btn btn-green waves-effect text-left" data-dismiss="modal">See Comments</button>
                  </div> -->
              </div>
          </div> 
        </div>
         <div id="show-add-tag" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tag_title" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="tag_title">Add New Tag</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body add-tag">
                      <div class="form-group">
                        <input type="hidden" id="brand_id" name="brand_id" value="{{ $brand_id }}" >
                        <label for="name" class="control-label">{{ __('Name') }}</label>
                        <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                        <span class="invalid-name" role="alert">
                        </span>
                      </div>
                      <div class="form-group">
                      <button type="button" id="add_new_tag" class="btn btn-primary">  {{ __('Save') }}</button>
                      </div>
                    </div>
                  </div>
              </div>     
            </div>
      @endsection
      @push('scripts')
      <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
      <!-- slimscrollbar scrollbar JavaScript -->
      <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
      <!--Wave Effects -->
      <script src="{{asset('js/waves.js')}}" defer></script>
      <!--Menu sidebar -->
      <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
      <!--stickey kit -->
      <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
      <!--Custom JavaScript -->
      <script src="{{asset('js/custom.min.js')}}" defer></script>
      <!-- ============================================================== -->
      <!-- This page plugins -->
      <!-- ============================================================== -->
     
     <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
       <script src="{{asset('js/morris-data.js')}}" ></script>-->
      
      <!-- Chart JS -->
      <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
      <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
      <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
      <!-- Flot Charts JavaScript -->
      <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
      
      <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
      <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

      <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
          <!-- Date range Plugin JavaScript -->
      <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
      <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
      <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
      <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
      <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
      <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
      <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/icheck/icheck.min.js')}}"  defer></script>
      <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}" defer></script>

      <!--    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script> -->
        <!-- <script src="{{asset('assets/plugins/materialize/js/materialize.min.js')}}" defer></script> -->
   
     <script>
        window.onscroll = function() {myFunction()};

        var header = document.getElementById("myHeader");
        var myHeaderContent = document.getElementById("myHeaderContent");
        var tabHeader = document.getElementById("tabHeader");
        var tabHeader_sticky = tabHeader.offsetTop;

        var sticky = header.offsetTop;
        function myFunction() {
          if (window.pageYOffset > sticky) {
            header.classList.add("s-topbar");
            header.classList.add("s-topbar-fix");
            myHeaderContent.classList.add("myHeaderContent");
            
          } else {
            header.classList.remove("s-topbar");
            header.classList.remove("s-topbar-fix");
            myHeaderContent.classList.remove("myHeaderContent");
          }
        }
     </script>
    <script type="text/javascript">
      var startDate;
      var endDate;
      var label;
      var mention_total;
      var mentionLabel = [];
      var mentions = [];
    
      // var bookmark_array=[];
      // var bookmark_remove_array=[];
     
      var positive = [];
      var negative = [];
      var sentimentLabel = [];
      var positive_total=0;
      var negative_total=0;
      var  colors=["#1e88e5","#dc3545","#28a745","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


      /*var effectIndex = 2;
      var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

      $(document).ready(function() {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
              $('.js-switch').each(function() {
                  new Switchery($(this)[0], $(this).data());
              });
        var tmp_articleTotal = 0; var tmp_postTotal=0;
        //initialize
         $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
         $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
         $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
         $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
         $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
         $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
        /* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
         $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
         $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
         $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
         $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
         $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

         $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

         var GetURLParameter = function GetURLParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

          for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return sParameterName[1] === undefined ? true : sParameterName[1];
              }
          }
        };

        startDate = moment().subtract(6, 'month');
        endDate = moment();
        label = 'month';

         $('.singledate').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          locale: {
              format: 'DD/MM/YYYY'
          }
        },function(date) {
          endDate=date;
          var id =$('input[type=radio][name=period-radio]:checked').attr('id');
          if(id==="period-week")
         {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
         }
         else
         {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
         }
          var admin_page=$('#hidden-chip').val();
         ChooseDate(startDate,endDate,admin_page,'','');
      });

       function hidden_div(){
        var brand_id = GetURLParameter('pid');
        $( "#popular-spin" ).show();
        $("#popular").empty();
        $.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
            url: "{{route('gethiddendiv')}}", // This is the URL to the API
            data: { view_name:'Dashboard'}
          })
          .done(function( data ) {//$("#popular").html('');
           for(var i in data) {
            $("#"+data[i].div_name).hide();
           }

          })
          .fail(function(xhr, textStatus, error) {
            //  console.log(xhr.statusText);
            // console.log(textStatus);
            // console.log(error);
            // If there is no communication between the server, show an error
           // alert( "error occured" );
          });
        }

        $(".link_mention_cmt").click(function(e){ $("#mention_tab").tab('show');})
        $(".link_article").click(function(e){ $("#mention_tab").tab('show');})
        $(".link_mention_post").click(function(e){$("#mention_tab").tab('show');})
        $(".link_page_post").click(function(e){$("#pages_tab").tab('show');})
        $(".link_page_comment").click(function(e){$("#pages_tab").tab('show');})

        $(".btnMentionCmt").click(function(e){
        var commentType = $(this).attr('value');
        var campaign_name  = $('#hidden-chip').val();
        window.open("{{ url('relatedcampaignmention?')}}" +"pid="+ GetURLParameter('pid') +"&SentiType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&campaign_name="+campaign_name, '_blank');
        })
        // $(".btnPageCmt").click(function(e){
        // var campaign_name  = $('#hidden-chip').val();
        // var campaign_id  = $("*[name='"+campaign_name+"']").attr('id');
        // var postType = $(this).attr('value');
        // window.open("{{ url('relatedcampaignmention?')}}" +"pid="+ GetURLParameter('pid') +"&SentiType="+postType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&campaign_id="+campaign_id+"&campaign_name="+campaign_name, '_blank');
        // })

       function highlighted(fday,sday){
        let main = document.getElementById("hightlighted");
        let existInstance = echarts.getInstanceByDom(main);
          if (existInstance) {
              if (true) {
                  echarts.init(main).dispose();
              }
          }

        var chart = echarts.init(main);
        var brand_id = GetURLParameter('pid');
      
        $( "#hightlighted-spin" ).show();
        $.ajax({
          type: "GET",
          dataType:'json',
          contentType: "application/json",
          url: "{{route('getInHighlighted')}}", // This is the URL to the API
          data: { fday: fday,sday:sday,type:"all",brand_id:brand_id,admin_page:$('#hidden-chip').val()}
        })
        .done(function( data ) {
            
        $( "#hightlighted-spin" ).hide();
          if(data.length>0){
           var data = genData(data);
           // console.log(data);
           var cmtObj=data.commentID;
           chart.clear();
           option=null;
           var option = {
            textStyle: {
              fontFamily: 'mm3',},
              tooltip: { 
                textStyle: {
                            fontFamily: 'mm3',}
                        },
              series: [ {
                         type: 'wordCloud',
                         left:'center',
                         top:'center',
                         width: '70%',
                         height: '80%',
                         right:null,
                         bottom:null,
                         sizeRange: [12, 50],
                         rotationRange: [0,0,0,0],
                         // rotationStep: 90,
                         shape: 'ratangle',//pentagon ratangle
                         gridSize:8,
                          // height: 500,
                         drawOutOfBound: false,
                         textStyle: {
                            normal: {
                                fontFamily: 'mm3',
                                color: function () {
                                    var lightcolor = 'rgb(' + [
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        2
                                    ].join(',') + ')';
                                    console.log(lightcolor);
                                    return lightcolor;
                                }
                            },
                          emphasis: {
                                shadowBlur: 10,
                                shadowColor: '#333'
                            }
                        },
                          data: data.seriesData
                      }
                    ]  
                 };

                function genData(data) {
                  var seriesData = [];
                  var comment_array={};
                  var i = 0;
                   for(var key in data) 
                      {
                       var name =data[key].topic_name;
                       var value=data[key].weight;
                       var arr_cmt=data[key].comment_id;
                       var kvalue=parseFloat(value);
                     
                        comment_array[name]=arr_cmt;
                        seriesData.push({name: name,value:value});
                      
                        
                      }
                   return {
                        seriesData: seriesData,
                        commentID:comment_array,
                    };
                 };

              chart.setOption(option,true), $(function() {
              function resize() {
                  setTimeout(function() {
                      chart.resize()
                  }, 100)
              }
          }
        );
        chart.on('click', function (params) {
          //    console.log(params);
          //    console.log(params.name); 
          //    console.log(params.seriesName); 
          //    console.log(params.value);//count
        var cmt_arr=cmtObj[params.name];
              $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("session-cmtID") }}',
             type: 'GET',
             data: {highlight_text:params.name,wordcloud_text:cmt_arr},
             success: function(response) {// alert(response)
                window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid')+"&highlight_text="+ params.name+"&source="+GetURLParameter('source')+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&wordcloud_text="+params.name+"&admin_page="+$('#hidden-chip').val(), '_blank'); 
            }
          });
       });
      }
    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
   }
    function Calculate_DatePreset(label)
    {
        var date_preset='this_month';
             var start = new Date(startDate);
             var end = new Date(endDate);
             var current_Date = new Date();
             var current_year =new Date().getFullYear();
             var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
             var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
              if(label === 'Today' )  date_preset = 'today';
            else if (label === 'Yesterday')  date_preset = 'yesterday';
            else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
            else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
            else if (label === 'This Quarter')  date_preset = 'this_quarter';
            else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
            else if (label === 'This Year')  date_preset = 'this_year';
            else if (label === 'Last Year')  date_preset = 'last_year';
            else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

            return date_preset;
    }

     function ChooseDate(start, end,label='',filter_tag='') {

           tmp_articleTotal = 0;tmp_postTotal = 0;
            $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate = start;
            endDate=end;
           var graph_option = $('.btn-group1 > .btn.active').text();
           var date_preset = Calculate_DatePreset(label);
           getAllArticle(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag);
           getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           getAllMentionPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           getAllMentionComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           // posting_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           MostFrequentlyTag(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           mention_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           // highlighted(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           tagCount();
           // getKeyword();
           // getTopPost();
           // getTopComment();
          if (graph_option.trim()== 'Sentiment') { InboundSentimentDetail(GetStartDate(),GetEndDate());}
          else if (graph_option.trim() == 'Mention'){requestmentionData(GetStartDate(),GetEndDate());}
       }

       function EmptyChooseDate(start, end,label='',filter_tag='') {

           tmp_articleTotal = 0;tmp_postTotal = 0;
            $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate = start;
            endDate=end;
           var graph_option = $('.btn-group1 > .btn.active').text();
           var date_preset = Calculate_DatePreset(label);
           getAllArticle(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag);
           getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           getAllMentionPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          // getAllMentionComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           // posting_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           MostFrequentlyTag(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           mention_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           // highlighted(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           tagCount();
           // getKeyword();
           // getTopPost();
           // getTopComment();
          if (graph_option.trim()== 'Sentiment') { InboundSentimentDetail(GetStartDate(),GetEndDate());}
          else if (graph_option.trim() == 'Mention'){requestmentionData(GetStartDate(),GetEndDate());}
       }

        function GetStartDate(){ return startDate.format('YYYY-MM-DD');}
        function GetEndDate(){return endDate.format('YYYY-MM-DD');}



        function mention_status(fday,sday){
         var brand_id = GetURLParameter('pid');
         fday = $("#year").val();
            $( "#post_spin" ).show();
            $( "#cmt_spin" ).show();
            $("#mention_spin").show();
            var campaign_name = $('#hidden-chip').val();
            var campaign_id = $("*[name='"+campaign_name+"']").attr('id');

           
            $.ajax({
                type: "GET",
                dataType:'json',
                contentType: "application/json",
                url: "{{route('getCampaignMentionStatus')}}", // This is the URL to the API
                data: { fday: fday,sday:sday,brand_id:brand_id,campaign_id:campaign_id}
              })
              .done(function( data ) { console.log(data);
               $( "#post_spin" ).hide();
               $( "#cmt_spin" ).hide();
                $("#mention_spin").hide();

                  var cmt_total = data[0][0]['total'];
                  var cmt_pos =data[0][0]['positive'] ;
                  var cmt_neg =data[0][0]['negative'] ;
                  var cmt_neutral = data[0][0]['neutral'] ;
                  var cmt_NA = data[0][0]['NA'] ;
                      cmt_neutral=parseInt(cmt_neutral)+parseInt(cmt_NA);

                  var post_total = data[1][0]['total'];
                  var post_pos =data[1][0]['positive'] ;
                  var post_neg = data[1][0]['negative'] ;
                  var post_neutral = data[1][0]['neutral'] ;

                  var incmt_total =data[5][0]['total'] ;
                  var incmt_pos =data[2][0]['positive'] ;
                  var incmt_neg =data[2][0]['negative'] ; 
                  var incmt_neutral = data[2][0]['neutral'] ;
                 
                  var incmt_NA = data[2][0]['NA'] ;
                      incmt_neutral=parseInt(incmt_neutral)+parseInt(incmt_NA);

                  var article_total = data[4][0]['total'] ;
                      article_total = parseInt(article_total);
                  $('#article_total').text(article_total==0?'0':article_total);
                   
                  var reaction_total =data[3][0]['likeTotal']+data[3][0]['loveTotal']+data[3][0]['hahaTotal']+data[3][0]['wowTotal']+data[3][0]['sadTotal']+data[3][0]['angryTotal'];
                  $('#reaction_total').text(reaction_total==0?'0':reaction_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0}));

                  var share_total = data[3][0]['shareTotal'];
                  if(share_total==null)
                  share_total = 0;

                  var react_total = data[3][0]['reactTotal'];
                  if(react_total==null)
                  react_total = 0;

                  // alert(share_total);
                  var inbount_comment_total = data[5][0]['total'];
                  // var formatter = new Intl.NumberFormat('en-IN', { minimumFractionDigits: 0,});
                  //     audience_total = formatter.format(audience_total);

                      // test
                  
                      var pageLike = parseInt(data[6]) * (0.0078);
                      // alert(pageLike);
                      var eng_react_total = react_total * (20);
                      // alert(eng_react_total);
                      var eng_share_total = share_total * (40);
                      var eng_inbound_comment_total = inbount_comment_total * (120);

                      var audience_total = parseInt(pageLike)+parseInt(eng_react_total)+parseInt(eng_share_total)+parseInt(eng_inbound_comment_total);
                      audience_total = audience_total.toLocaleString('en');
                      // var formatter = new Intl.NumberFormat('en-IN', { minimumFractionDigits: 0,});
                      // audience_total = formatter.format(audience_total);
                      // alert(audience_total);
                      // end test
 

                  $('#audience_total').text(audience_total=0?'0':audience_total);
                  $('#audience_reaction').text(reaction_total=0?'0':reaction_total);
                  $('#audience_share').text(share_total=0?'0':share_total);
                  $('#audience_comment').text(inbount_comment_total=0?'0':inbount_comment_total);

                  //alert(share_total);
                  $('#share_total').text(share_total==0?'0':share_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0}));

                  var engage_total = parseInt(share_total) + parseInt(reaction_total);
                 
                  engage_total = engage_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0});
               
                  $('#engage_total').text(engage_total==0?'0':engage_total=='NaN'?'0':engage_total);


                  var inpost_total =data[3][0]['total'] ;
                  var inpost_pos =data[3][0]['positive'] ;
                  var inpost_neg =data[3][0]['negative'] ; 
                  var inpost_neutral = data[3][0]['neutral'] ;
                  var inpost_NA = data[3][0]['NA'] ;
                      inpost_neutral=parseInt(inpost_neutral)+parseInt(inpost_NA);
                      
                  var total =  parseInt(cmt_total)+ parseInt(post_total) + parseInt(incmt_total) + parseInt(inpost_total);
                  var all_total=parseInt(cmt_pos)+parseInt(cmt_neg)+parseInt(cmt_neutral)+parseInt(post_pos)+parseInt(post_neg)+parseInt(post_neutral)+parseInt(incmt_pos)+parseInt(incmt_neg)+parseInt(incmt_neutral)+parseInt(inpost_pos)+parseInt(inpost_neg)+parseInt(inpost_neutral);

                  var pos_total = parseInt(cmt_pos) + parseInt(post_pos)+parseInt(incmt_pos)+parseInt(inpost_pos);
                  var neg_total = parseInt(cmt_neg) + parseInt(post_neg)+parseInt(incmt_neg)+parseInt(inpost_neg);
                  var neutral_total = parseInt(cmt_neutral) + parseInt(post_neutral)+parseInt(incmt_neutral) +parseInt(inpost_neutral);
                   

                  var pos_pcent=parseFloat((pos_total/all_total)*100).toFixed(2);
                  var neg_pcent=parseFloat((neg_total/all_total)*100).toFixed(2);
                  var neutral_pcent=parseFloat((neutral_total/all_total)*100).toFixed(2);

                  pos_pcent = isNaN(pos_pcent)?'0':pos_pcent;
                  neg_pcent = isNaN(pos_pcent)?'0':neg_pcent;
                  neutral_pcent = isNaN(neutral_pcent)?'0':neutral_pcent;
                  total = isNaN(total)?'0':total;

        
                  var mention_total = parseInt(cmt_total) + parseInt(post_total);
                  $("#mention_total").text(mention_total==0?'0':mention_total);

                  var pages_total = parseInt(incmt_total) + parseInt(inpost_total);
                  $("#pages_total").text(pages_total==0?'0':pages_total);

                   $("#pages_post_total").text(inpost_total==0?'0':inpost_total);
                   incmt_total = incmt_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0});
                  $("#pages_comment_total").text(incmt_total==0?'0':incmt_total);
         
                  var incmt_total =parseInt(incmt_pos)+parseInt(incmt_neg)+parseInt(incmt_neutral);
                 
                  var incmt_pos_pcent=parseFloat((parseInt(incmt_pos)/incmt_total)*100).toFixed(2);
                  var incmt_neg_pcent=parseFloat((parseInt(incmt_neg)/incmt_total)*100).toFixed(2);
                  var incmt_neutral_pcent=parseFloat((parseInt(incmt_neutral) /incmt_total)*100).toFixed(2);
                  incmt_pos_pcent = isNaN(incmt_pos_pcent)?'0':incmt_pos_pcent;
                  incmt_neg_pcent = isNaN(incmt_neg_pcent)?'0':incmt_neg_pcent;
                  incmt_neutral_pcent = isNaN(incmt_neutral_pcent)?'0':incmt_neutral_pcent;
                  incmt_total = isNaN(incmt_total)?'-':incmt_total;
                  incmt_total = incmt_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0});

                    $("#pagecmt_total").text(inbount_comment_total==0?'0':inbount_comment_total);
                      var incom_pos =data[5][0]['positive'] ;
                      var incom_neg =data[5][0]['negative'] ; 
                      var incom_neutral = data[5][0]['neutral'] ;
                      var  incom_NA = data[5][0]['NA'] ;
                      incom_neutral=parseInt(incom_neutral)+parseInt(incom_NA);

                      var incom_total =parseInt(incom_pos)+parseInt(incom_neg)+parseInt(incom_neutral);
                 
                  var incom_pos_pcent=parseFloat((parseInt(incom_pos)/incom_total)*100).toFixed(2);
                  var incom_neg_pcent=parseFloat((parseInt(incom_neg)/incom_total)*100).toFixed(2);
                  var incom_neutral_pcent=parseFloat((parseInt(incom_neutral) /incom_total)*100).toFixed(2);
                  incom_pos_pcent = isNaN(incom_pos_pcent)?'0':incom_pos_pcent;
                  incom_neg_pcent = isNaN(incom_neg_pcent)?'0':incom_neg_pcent;

                    $("#incmt_neg").text(incom_neg_pcent==0?'0 %':incom_neg_pcent.replace(".00", "") + "%");
                    $("#incmt_neutral").text(incom_neutral_pcent==0?'0 %':incom_neutral_pcent.replace(".00", "") + "%");
                    $("#incmt_pos").text(incom_pos_pcent==0?'0 %':incom_pos_pcent.replace(".00", "") + "%");
                    $("#top_post_total").text(post_total==0?'0':post_total);


                  var mention_cmt_total =parseInt(cmt_pos)+parseInt(cmt_neg)+parseInt(cmt_neutral)   
                  var cmt_pos_pcent=parseFloat((parseInt(cmt_pos)/mention_cmt_total)*100).toFixed(2);
                  var cmt_neg_pcent=parseFloat((parseInt(cmt_neg)/mention_cmt_total)*100).toFixed(2);
                  var cmt_neutral_pcent=parseFloat((parseInt(cmt_neutral) /mention_cmt_total)*100).toFixed(2);

                  cmt_pos_pcent = isNaN(cmt_pos_pcent)?'0':cmt_pos_pcent;
                  cmt_neg_pcent = isNaN(cmt_neg_pcent)?'0':cmt_neg_pcent;
                  cmt_neutral_pcent = isNaN(cmt_neutral_pcent)?'0':cmt_neutral_pcent;
                  mention_cmt_total = isNaN(mention_cmt_total)?'0':mention_cmt_total;

                    $("#mentioncmt_total").text(cmt_total==0?'0':cmt_total);
                    $("#cmt_neg").text(cmt_neg_pcent==0?'0 %':cmt_neg_pcent.replace(".00", "") + "%");
                    $("#cmt_neutral").text(cmt_neutral_pcent==0?'0 %':cmt_neutral_pcent.replace(".00", "") + "%");
                    $("#cmt_pos").text(cmt_pos_pcent==0?'0 %':cmt_pos_pcent.replace(".00", "") + "%");
                    $("#top_comment_total").text(cmt_total==0?'0':cmt_total);


                })
              .fail(function(xhr, textStatus, error) {
                //  console.log(xhr.statusText);
                // console.log(textStatus);
                // console.log(error);
                // If there is no communication between the server, show an error
               // alert( "error occured" );
              });
            }
            $('input[type=radio][name=options]').change(function() {//alert(this.value);

                 if (this.value == 'sentiment') {
                    InboundSentimentDetail(GetStartDate(),GetEndDate());
                }
                else if (this.value == 'mention')
                {
                    requestmentionData(GetStartDate(),GetEndDate());
                }
            });

          function getAllPost(fday,sday){
          	fday = $("#year").val();
            $(".popup_post").unbind('click');
            $(".see_comment").unbind('click');
            var campaign_name = $('#hidden-chip').val();
            var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
              // alert(campaign_name);
              // var filter_keyword_arr = [];
              //   $.each($("input[name='keyword']:checked"), function(){            
              //       filter_keyword_arr.push($(this).attr('id').replace('K_',''));
              //     });

              //            var filter_senti_arr = [];
  			      // $.each($("input[name='sentimentlist']:checked"), function(){            
  			      //     filter_senti_arr.push($(this).val());
  			      //   });
  				     //  var filter_keyword_arr = [];
  				     //  $.each($("input[name='keyword']:checked"), function(){            
  				     //      filter_keyword_arr.push($(this).val());
  				     //    });
            var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
              });
            var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
              });
        				
            var oTable = $('#tbl_post').DataTable({
      
            // "responsive": true,
            "paging": true,
            "pageLength": 10,
            "lengthChange": false,
            "searching": false,
            "processing": false,
            "serverSide": false,
            "destroy": true,
            "ordering": true   ,
            "autoWidth": false,
            "order": [],
            "headers": {
              'X-CSRF-TOKEN': '{{csrf_token()}}' 
            },
          
            "ajax": {
            "url": '{{ route('getCampaignPost') }}',
            "dataSrc": function(res){
              document.getElementById('title_post').innerHTML = 'Posts ('+res.recordsTotal+")";
              document.getElementById('title_pages').innerHTML = 'Pages ('+res.recordsTotal+")";
              return res.data;
            },
     
             "data": {
              "fday": fday,
              "sday": sday,
              "campaign_name":campaign_name,
              "campaign_id":campaign_id,
              "type":"inbound post",
              "other_page":'1',
              "brand_id": GetURLParameter('pid'),
              "tsearch_senti":filter_senti_arr.join("| "),
              "tsearch_keyword":filter_keyword_arr.join("| "),
            }
          },
          "initComplete": function( settings, json ) {
          $('.tbl_post thead tr').removeAttr('class');
          },
          columns: [
          {data: 'post', name: 'post',"orderable": false},
          {data: 'reaction', name: 'reaction', orderable: true,className: "text-center"},
          {data: 'share', name: 'share', orderable: true, className: "text-center"},
          ]
        }).on('click', '.popup_post', function (e) {
          if (e.handled !== true) {
            var name = $(this).attr('name');
            var post_id = $(this).attr('id');
            $("#modal-spin").show();
            $(".post_data").empty();

             $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("getRelatedPosts") }}',
             type: 'GET',
             data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_name:$('#hidden-chip').val()},
             success: function(response) { 
              // console.log(response);
              var res_array=JSON.parse(response);
              var data=res_array[0];

              var keyForHighLight = res_array[5];
              // alert(keyForHighLight);
              var imgurl = res_array[4];
              var monitor=res_array[1];
              var monitor_full_name=res_array[2];
              var monitor_page_id=res_array[3];
               // alert(data);
             for(var i in data) {
              // alert(data[i].message);
               var message = highLightText(keyForHighLight,data[i].message);
// alert(message);
               var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
               var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

               var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
               var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
               var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
               pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
               neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
               neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
               pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
               neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
               neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
               var page_name=data[i].page_name;

              var found_index = monitor.indexOf(page_name);
              if (typeof monitor_page_id [found_index] !== 'undefined') 
              var page_photo_id = monitor_page_id [found_index];

              if (typeof monitor_full_name [found_index] !== 'undefined') 
              var page_name = monitor_full_name [found_index];
              
              var page_Link= 'https://www.facebook.com/' + page_name ;  
              var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large'; 
      
              page_Link= 'https://www.facebook.com/' + page_name ;   
              var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                       '  <div class="sl-left">'+
                       '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                       ' <div class="sl-right">'+
                       '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                       ' <div class="sl-right"> '+
                       ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                       ' <div style="width:55%;display: inline-block">';
                        if(neg_pcent!=='')
                        html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                        if(pos_pcent!=='')
                        html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                        if(neutral_pcent!=='')
                        html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                        '   <p class="m-t-10" align="justify">' + 
                         message +
                        '</p> ';

                   if(data[i]['post_type'] =='photo'){
                  html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                          '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                          '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                          '</span><span class="postPanel_nameTextContainer"></span></a>';
                  }
                  else if(data[i]['post_type'] =='video')
                  {
                  html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                          '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                          '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                          '</span><span class="postPanel_nameTextContainer"></span></a>';
                  }
                  html +='</div></div>';

                 $(".post_data").append(html);
                }   
                 $("#modal-spin").hide();
                 $("#postModal").text(name);
                 $('#show-post-task').modal('show'); 
                }
              });
                  e.handled = true;
             }
          }).on('click', '.see_comment', function (e) {
              if (e.handled !== true) {
                var attr_id = $(this).attr('id');
                var post_id  = attr_id.replace("seeComment_","");
                window.open("{{ url('related_campaigncomment?')}}" +"pid="+ GetURLParameter('pid') +"&campaign_name="+$('#hidden-chip').val()+"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post", '_blank');
                }
              e.handled = true;
           }).on('click', '.btn-campaign', function (e) {
              if (e.handled !== true) {
                var attr_id = $(this).attr('id');
                var post_id  = attr_id.replace("campaign_","");
                var btn_val = $("#"+attr_id).attr('aria-pressed');
                
                if(btn_val == "false") // previous false next true
                {
                  var answer = confirm("Are you sure  to add campaign ?")
                  if (answer) {
                     $.ajax({
                       headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                       url:'{{ route("SetCampaign") }}',
                       type: 'POST',
                       data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_val:1},
                       success: function(response) { //alert(response)
                        if(response>=0)
                        {
                         var x = document.getElementById("snackbar")
                          $("#snack_desc").html('');
                          $("#snack_desc").html('Campaign added successfully!!');
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                        }
                      }
                    });
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline active");
                    // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                     // $("#"+attr_id).attr("aria-pressed", "false");
                     // $("#"+attr_id).removeAttr("class");
                     // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                    
                  }
              }
              else
              {
                var answer = confirm("Are you sure to remove campaign?")
                if (answer) {
                 $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("SetCampaign") }}',
                   type: 'POST',
                   data: {id:post_id,brand_id:GetURLParameter('pid')},
                   success: function(response) { //alert(response)
                    if(response>=0)
                    {
                      var x = document.getElementById("snackbar")
                        $("#snack_desc").html('');
                        $("#snack_desc").html('Campaign remove successfully!!');
                        x.className = "show";

                       setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       $("#"+post_id).closest('tr').remove();
                    }
                  }
                });
              }
              else
              {
                 $("#"+attr_id).attr("aria-pressed", "true");
                 $("#"+attr_id).removeAttr("class");
                 $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline  active focus");
              }      
            }
          }
            e.handled = true;
        });
      new $.fn.dataTable.FixedHeader( oTable );
    }
      $(window).resize( function() {$('#tbl_post').css('width', '100%');});

      function tagsentimentData(fday,sday){
      	fday = $("#year").val();
        let main = document.getElementById("tag-senti-chart");
        let existInstance = echarts.getInstanceByDom(main);
          if (existInstance) {
              if (true) {
                  echarts.init(main).dispose();
              }
          }
        var sentiChart = echarts.init(main);
        var campaign_name = $('#hidden-chip').val();
        var campaign_id = $("*[name='"+campaign_name+"']").attr('id');

        $("#tag-senti-spin").show();
        var brand_id = GetURLParameter('pid');
        $.ajax({
          type: "GET",
          dataType:'json',
          contentType: "application/json",
          url: "{{route('getCampaignTagSentiment')}}", // This is the URL to the API
          data: { fday: fday,sday:sday,brand_id:brand_id,campaign_name:campaign_name,campaign_id:campaign_id,cmt_type :'page_comment' }
        })
        .done(function( data ) {
          var xAxisData = [];
          var data1 = [];
          var data2 = [];
          var tag_arr= genData(data);
          var tag_Obj=tag_arr.tag_id;
          for(var i in data) {
              xAxisData.push(data[i].tagLabel);
              data1.push(Math.round(data[i].positive));
              if(data[i].negative >0 )
              {
              data2.push(Math.round(-data[i].negative));
              }
              else
              {
               data2.push(0);
              }
            }

          var itemStyle = {
              normal: {
                  fontFamily: 'mm3',
              },
              emphasis: {
                  barBorderWidth: 1,
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowOffsetY: 0,
                  shadowColor: 'rgba(0,0,0,0.5)'
              }
          };

          option = {
                color:colors,
                textStyle: {
                            fontFamily: 'mm3',},
                backgroundColor: 'rgba(0, 0, 0, 0)',
                dataZoom:[  {
                      type: 'slider',
                      show: true,
                      xAxisIndex: [0],
                      start: 70,
                      end: 100
                  },
                   {
                      type: 'inside',
                      xAxisIndex: [0],
                      start: 1,
                      end: 35
                   }
                ],
                calculable : true,
                legend: {
                    
                    data: ['positive', 'negative'],
                       x: 'center',
                         y: 'top',
                          padding :0,
                },

              toolbox: {
                      show : true,
                      feature : {
                          mark : {show: true},
                          dataView : {show: false, readOnly: false},
                          magicType : {show: false, type: ['stack','tiled']},
                          restore : {show: false},
                          saveAsImage : {show: false}
                      }
                  },
              tooltip: {   textStyle: {
                              
                              fontFamily: 'mm3',}
                          },
              calculable : true,
              dataZoom : {
                  show : true,
                  realtime : true,
                  start : 0,
                  end : 100
              },
              xAxis: {
                  data: xAxisData,
                  axisLabel:{
                rotate:45
              },
                  name: 'Tags',
                  silent: false,
                  axisLine: {onZero: true},
                  splitLine: {show: false},
                  splitArea: {show: false},
                },
              yAxis: {
                  inverse: false,
                  splitArea: {show: false}
              },
             grid: {
                      top: '12%',
                      left: '3%',
                      right: '5%',
                      containLabel: true
                  },
  
            series: [
                {
                    name: 'positive',
                    type: 'bar',
                    stack: 'one',
                    color:colors[2],
                    barMaxWidth:30,
                    itemStyle: itemStyle,
                    data: data1
                },
                {
                    name: 'negative',
                    type: 'bar',
                    stack: 'one',
                    color:colors[1],
                    barMaxWidth:30,
                    itemStyle: itemStyle,
                    data: data2
                }
              ]
          };
          function genData(data) {
              var tag_array={};
              var i = 0;
               for(var key in data) 
                  {
                   var name =data[key].tagLabel;
                   var id=data[key].tagId;
                   tag_array[name]=id;
                  }
               return {
                       tag_id:tag_array,
              };
          };

       $("#tag-senti-spin").hide();

      sentiChart.setOption({
          title: {
              backgroundColor: '#333',
            /*  text: 'SELECTED DATA INDICES: \n' + brushed.join('\n'),*/
              bottom: 0,
              right: 0,
              width: 100,
              textStyle: {
                  fontSize: 12,
                  color: '#fff'
              }
          }
      });

      sentiChart.setOption(option, true), $(function() {
      function resize() {
          setTimeout(function() {
              sentiChart.resize()
          }, 100)
      }
      $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });
   
     sentiChart.on('click', function (params) {
       var source = GetURLParameter('source');
       var CmtType ='';
       if(params.seriesName == 'positive')
        CmtType = 'pos'
        else
        CmtType = 'neg'

       var campaign_name = $('#hidden-chip').val();
      
      window.open("{{ url('related_campaigncomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+tag_Obj[params.name]+"&CmtType="+CmtType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&campaign_name="+campaign_name, '_blank');
    });
   })
   .fail(function(xhr, textStatus, error) {
    //  console.log(xhr.statusText);
    // console.log(textStatus);
    // console.log(error);
    // If there is no communication between the server, show an error
   // alert( "error occured" );
  });
  }
      // $("#btnSeeComment").click( function(){
      //  var post_id=$("#popup_id").val();
      //  var admin_page=$('#hidden-chip').val();
      //  var split_name = admin_page.split("-");
      //   if(split_name.length > 0)
      //   {
      //     if(isNaN(split_name[split_name.length-1]) == false )
      //       admin_page = split_name[split_name.length-1];
      //   }
          
      //    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&campaign_name="+$('#hidden-chip').val()+"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+null+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post", '_blank');
      //     }
      //   );

   function getAllComment(fday, sday,filter_tag='')
      {
      	fday = $("#year").val();
        $(".popup_post").unbind('click');
        $(".edit_predict").unbind('click');
        $("#add_tag").unbind('click');

        var filter_tag_arr = [];
              $.each($("input[name='taglist']:checked"), function(){            
                  filter_tag_arr.push($(this).val());
                });
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });
        var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
              });
        var campaign_name = $('#hidden-chip').val();
        var campaign_id = $("*[name='"+campaign_name+"']").attr('id');

        var oTable = $('#tbl_comment').DataTable({

          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": true,
          "destroy": true,
          "ordering": false   ,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
         
          "ajax": {
            "url": '{{ route('getcampaigncomment') }}',
            "dataSrc": function(res){
              document.getElementById('title_comment').innerHTML = 'Comments ('+res.recordsTotal+")";
              return res.data;
            },
             /*       data: function ( d ) {
                      d.fday = mailingListName;
                      d.sday = mailingListName;
                      d.brand_id = mailingListName;
                    }*/
             "data": {
              "fday": fday,
              "sday": sday,
              "campaign_name":campaign_name,
              "campaign_id":campaign_id,
              "type" :"inbound comment",
              "brand_id": GetURLParameter('pid'),
              "sentiType": GetURLParameter('SentiType'),
              "tsearch_senti":filter_senti_arr.join("| "),
              "tsearch_keyword":filter_keyword_arr.join("| "),
              "tsearch_tag":filter_tag_arr.join("| "),
              "is_competitor":"true",
            }

          },
          "initComplete": function( settings, json ) {
            
          },
       drawCallback: function() {
       $('.select2').select2();
     },

          columns: [
          {data: 'post_div', name: 'post_div',"orderable": false},
          ]
          
        }).on('click', '.popup_post', function (e) {
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:GetURLParameter('pid')},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];
              for(var i in data) {
                 var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
        
                 var page_name=data[i].page_name;
       
                 var found_index = monitor.indexOf(page_name);
                 if (typeof monitor_page_id [found_index] !== 'undefined') 
                 var page_photo_id = monitor_page_id [found_index];

                 if (typeof monitor_full_name [found_index] !== 'undefined') 
                 var page_name = monitor_full_name [found_index];
        
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large';                         
    
                var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                         '  <div class="sl-left">'+
                         '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                         ' <div class="sl-right">'+
                         '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                         ' <div class="sl-right"> '+
                         ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                         ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                    html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                if(pos_pcent!=='')
                    html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                if(neutral_pcent!=='')
                    html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                    html+=' </div></div></div></span></p> ' +'   <p class="m-t-10" align="justify">' + data[i].message +'</p>';
                if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                  html +='</div></div>';
               $(".post_data").append(html);
          }
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
        });
            e.handled = true;
       }
    }).on('change', '.edit_senti', function (e) {
        if (e.handled !== true) {

        var senti_id = $(this).attr('id');
        var post_id  = senti_id.replace("sentiment_","");
        var sentiment = $('#sentiment_'+post_id+' option:selected').val();
        var emotion = $('#emotion_'+post_id+' option:selected').val();
        var tags = $('#tags_'+post_id+' option:selected').map(function () {
            return $(this).text();
        }).get().join(',');

        var tags_id = $('#tags_'+post_id).val();
      
        $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
        if(sentiment == 'pos')
          $("#sentiment_"+post_id).addClass('text-success');
        else if (sentiment == 'neg')
          $("#sentiment_"+post_id).addClass('text-red');
        else
          $("#sentiment_"+post_id).addClass('text-warning');
     
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {
            if(response>=0)
            {
                //         swal({   
                //     title: "Updated!",   
                //     text: "Done!",   
                //     timer: 1000,   
                //     showConfirmButton: false 
                // });
                           var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Sentiment Changed!!');
                        x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                    }
                  }
              });

            e.handled = true;
          }
        }).on('change', '.edit_tag', function (e) {
            if (e.handled !== true) {
              var senti_id = $(this).attr('id');
              var post_id  = senti_id.replace("tags_","");
              var sentiment = $('#sentiment_'+post_id+' option:selected').val();
              var emotion = $('#emotion_'+post_id+' option:selected').val();
              var tags = $('#tags_'+post_id+' option:selected').map(function () {
                  return $(this).text();
              }).get().join(',');

              var tags_id = $('#tags_'+post_id).val();
    
             $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("setUpdatedPredict") }}',
             type: 'POST',
             data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
             success: function(response) {
              tagCount();
              if(response>=0)
              {
                //         swal({   
                //     title: "Updated!",   
                //     text: "Done!",   
                //     timer: 1000,   
                //     showConfirmButton: false 
                // });
                var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Tag Changed!!');
                        x.className = "show";
                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                    }
                  }
              });
             
            e.handled = true;
          }
          
      }).on('click', '#add_tag', function (e) {
        if (e.handled !== true) {
               $('#show-add-tag').modal('show'); 
              }
                e.handled = true;
        }).on('click', '.popup_img', function (e) {
          if (e.handled !== true) {

             var id = $(this).attr('id');
             var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          // img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 
         }
          
    });
       
      
          
    }
    $('input[type=radio][name=options]').change(function() {
      if (this.value == 'sentiment') { InboundSentimentDetail(GetStartDate(),GetEndDate());}
      else if (this.value == 'mention'){ requestmentionData(GetStartDate(),GetEndDate());}
      else if (this.value == 'pages'){ PagesMentionCount(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));}
      else if (this.value == 'webistes'){WebsiteMentionCount(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));}
   });


      function requestmentionData(fday,sday) {    
         
        fday = $("#year").val();
        let main = document.getElementById("fan-growth-chart");
        let existInstance = echarts.getInstanceByDom(main);
        if (existInstance) {
          if (true) { echarts.init(main).dispose();}
              }
        var mentionDetailChart = echarts.init(main);
        $("#fan-growth-spin").show();
        $("#reaction-spin").show();       
        var brand_id = GetURLParameter('pid');

        var campaign_name = $('#hidden-chip').val();
        var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
                  
        $.ajax({
          type: "GET",
          dataType:'json',
          contentType: "application/json",
          url: "{{route('getcampaignmentiondetail')}}",
          data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'day',campaign_id:campaign_id}
        })
        .done(function( data ) {
           mention_total=0;
           mentions=[];
           mentionLabel=[];

          for(var i in data) {
            mentions.push(data[i].mention);
            mentionLabel.push(data[i].periodLabel);
          }
          $.each(mentions,function(){mention_total+=parseInt(this) || 0;});
          option = {
              color: colors,
              tooltip: {
                trigger: 'axis',
              },
              // grid: {
              //     right: '20%'
              // },
            /*  toolbox: {
                  feature: {
                      dataView: {show: true, readOnly: false},
                      restore: {show: true},
                      saveAsImage: {show: true}
                  }
              },*/
             toolbox: {
                    show : true,
                    feature : {
                        mark : {show: false},
                        dataView : {show: false, readOnly: false},
                        magicType : {show: true, type: ['line','bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
              legend: {
                  data:['Pages And Mentions'],
                    //           formatter: function (name) {
                    //             if(name === 'Mentions')
                    //             {
                    //                  return name + ': ' + mention_total;
                    //             }
                    //             return name  + ': ' + reactions_total;


                    // }
                },
                xAxis: [
                    {
                        type: 'category',
                        axisTick: {
                            alignWithLabel: true
                        },
                                     axisLabel: {
                formatter: function (value, index) {
                // Formatted to be month/day; display year only in the first label
                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var date = new Date(value);
                var texts = [date.getFullYear(), monthNames[date.getMonth()],date.getDate()];
                return texts.join('-');
              },
                rotate:45
                },
                data: mentionLabel
               }
             ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Pages And Mentions',
                      /*  min: 0,
                        max: 250,
                        position: 'left',
                        axisLine: {
                            lineStyle: {
                                color: colors[0]
                            }
                        }/*,
                        axisLabel: {
                            formatter: '{value}'
                        }*/
                         /* axisLabel: {
                    formatter: function (e) {
                        return kFormatter(e);
                    }*/
                
                   }
                ],
              series: [
                  {
                      name:'Pages And Mentions',
                      type:'line',
                      smooth: 0.3,
                      color:colors[0],
                      barMaxWidth:30,
                      data:mentions
                  }
              ]
          };
          $("#fan-growth-spin").hide();
              mentionDetailChart.setOption(option, true), $(function() {
              // function resize() {
              //     setTimeout(function() {
              //         mentionDetailChart.resize()
              //     }, 100)
              // }
              // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
          });
          mentionDetailChart.on('click', function (params) {
            var campaign_name = $('#hidden-chip').val();
            var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
            window.open("{{ url('relatedcampaignmention?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate() +"&period="+params.name+"&campaign_name="+campaign_name+"&campaign_id="+campaign_id, '_blank');
          });
        })
        .fail(function() {
      
        });
      }

       function InboundSentimentDetail(fday,sday){
    	fday = $("#year").val();
        let main = document.getElementById("fan-growth-chart");
        let existInstance = echarts.getInstanceByDom(main);
          if (existInstance) { if (true) { echarts.init(main).dispose();} }
        var campaign_name = $('#hidden-chip').val();
        var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
        var sentiDetailChart = echarts.init(main);
        $("#fan-growth-spin").show();


              
        $.ajax({
          type: "GET",
          dataType:'json',
          contentType: "application/json",
          url: "{{route('getCampaignSentiDetail')}}", // This is the URL to the API
          data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),periodType:'month',campaign_name:campaign_name,campaign_id:campaign_id }
        })
        .done(function( data ) {
         
          var positive = [];
          var negative = [];
          var neutral=[];
          var sentimentLabel = [];

          var positive_total=0;
          var negative_total=0;
          var neutral_total=0;
          var all_total=0;


          for(var i in data) {
          positive.push(data[i].positive);
          negative.push(data[i].negative);
          neutral.push(data[i].neutral);
          sentimentLabel.push(data[i].periodLabel);
          
        }
          $.each(positive,function(){positive_total+=parseInt(this) || 0;});
          $.each(negative,function(){negative_total+=parseInt(this) || 0;});
          $.each(neutral,function(){neutral_total+=parseInt(this) || 0;});
          all_total=positive_total+negative_total+neutral_total;
          var positive_percentage=parseInt((positive_total/all_total)*100);
          var negative_percentage= parseInt((negative_total/all_total)*100);
          var neutral_percentage =  parseInt((neutral_total/all_total)*100);

          positive_percentage = isNaN(positive_percentage)?0:positive_percentage;
          negative_percentage = isNaN(negative_percentage)?0:negative_percentage;
          neutral_percentage = isNaN(neutral_percentage)?0:neutral_percentage;
          option = {
            color:colors,

            tooltip : {
                    trigger: 'axis',
                       /*formatter: function (params) {
                  var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
                  let rez = '<p>' + params[0].name + '</p>';
                  console.log(rez); //quite useful for debug
                  params.forEach(item => {
                       console.log("hihi"); 
                      console.log(item);
                      console.log(item.series.color); //quite useful for debug
                      var xx = '<p>'   + colorSpan(item.series.color) + ' ' + item.seriesName + ': ' + item.data  + '</p>'
                      rez += xx;
                  });

                  return rez;
              }*/

             },

              legend: {
                  data:['Positive','Negative'],
                 formatter: function (name) {
                  if(name === 'Positive')
                  {
                       return name + ': ' + positive_total;
                  }
                  return name  + ': ' + negative_total;
                }
              },
              toolbox: {
                  show : true,
                  feature : {
                      mark : {show: false},
                      dataView : {show: false, readOnly: false},
                      magicType : {show: true, type: ['line','bar']},
                      restore : {show: true},
                      saveAsImage : {show: true}
                  }
              },
        calculable : true,
        xAxis : [
        {
           type : 'category',
           axisLabel: {
            formatter: function (value, index) {
          // Formatted to be month/day; display year only in the first label
           const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
           var date = new Date(value);
           var texts = [date.getFullYear(), monthNames[date.getMonth()]];
           return texts.join('-');
         }
        },
        data : sentimentLabel
      }
    ],
      yAxis : [
      {
          type : 'value',
          name: 'count',
      }
      ],
      series : [
      {
          name:'Positive',
          type:'bar',
          data:positive,
          barMaxWidth:30,
          color:colors[2],
          markPoint : {
              data : [
              {type : 'max', name: 'maximum'},
              {type : 'min', name: 'minimum'},

              ]
          }/*,
            markLine : {
                data : [
                {type : 'average', name: 'average'}
                ]
            }*/
        },
        {
          name:'Negative',
        /*  type:effectIndex % 2 == 0 ? 'bar' : 'line',*/
          type:'bar',
          data:negative,
          barMaxWidth:30,
          color:colors[1],
          markPoint : {
            data : [
            {type : 'max', name: 'maximum'},
            {type : 'min', name: 'minimum'}
            ]
        }/*,
          markLine : {
            data : [
            {type : 'average', name : 'average value'}
            ]
        }*/
      }
    ]
  };

    sentiDetailChart.setOption(option, true), $(function() {
        // function resize() {
        //     setTimeout(function() {
        //           sentiDetailChart.resize()
        //     }, 100)
        // }
        // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });

    $("#fan-growth-spin").hide();
     sentiDetailChart.on('click', function (params) {
       var pid = GetURLParameter('pid');
       var commentType ='';
       if(params.seriesName == 'Positive') commentType = 'pos';
       else commentType = 'neg';

       if(params.name !== 'minimum' && params.name !== 'maximum' )
       window.open("{{ url('relatedcampaignmention?')}}" +"pid="+ GetURLParameter('pid')+"&SentiType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name+"&campaign_name="+$('#hidden-chip').val(), '_blank');
     });
    })
    .fail(function() {
    });
  }

    function getAllMentionPost(fday,sday){
      fday = $("#year").val();
      $(".popup_post").unbind('click');
      $(".see_comment").unbind('click');
      $(".post_hide").unbind('click');

      var filter_senti_arr = [];
        $.each($("input[name='sentimentlist']:checked"), function(){            
            filter_senti_arr.push($(this).val());
          });
      var filter_keyword_arr = [];
        $.each($("input[name='keyword']:checked"), function(){            
            filter_keyword_arr.push($(this).val());
          });
   
       var campaign_name = $('#hidden-chip').val();
       var campaign_id = $("*[name='"+campaign_name+"']").attr('id');

       var oTable = $('#tbl_mention_post').DataTable({
        
           // "responsive": true,
          "paging": true,
           "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": true,
          "serverSide": true,
          "destroy": true,
           "order": [],
              // "ordering": true   ,
              // "scrollX":        true,
              // "scrollCollapse": true,
           "autoWidth": false,
          
              //     "columnDefs": [
              //   { "targets": [0] , "orderable": false},       
              //   {"targets": [1],"orderable": true },
              //   {"targets": [2],"orderable": true }
              // ],
               
               // "order": [[ 1, 'asc' ], [ 2, 'asc' ]],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
             
          "ajax": {
            "url": '{{ route('getcampaignMention') }}',
            "dataSrc": function(res){
                  var count = res.data.length;
                  document.getElementById('title_mention_post').innerHTML = 'Posts ('+res.recordsTotal+")";
                  tmp_postTotal = res.recordsTotal;
                  document.getElementById('title_mentions').innerHTML = 'Mentions ('+(tmp_postTotal+tmp_articleTotal)+")";
                  return res.data;
               },
              "data": {
              "fday": fday,
              "sday": sday,
              "tsearch_senti":filter_senti_arr.join("| "),
              "tsearch_keyword":filter_keyword_arr.join("| "),
              "brand_id": GetURLParameter('pid'),
              "campaign_name": campaign_name,
              "campaign_id": campaign_id,
              "type":'mention post',
            }
          },
          "initComplete": function( settings, json ) {
            //console.log(json);
          $('.tbl_post thead tr').removeAttr('class');
          },
          columns: [
          {data: 'post', name: 'post',"orderable": false},
          {data: 'reaction', name: 'reaction', orderable: true,className: "text-center"},
          {data: 'share', name: 'share', orderable: true, className: "text-center"},
          ]
        }).on('click', '.popup_post', function (e) {
            if (e.handled !== true) {
              var name = $(this).attr('name');
              var post_id = $(this).attr('id');
              $("#modal-spin").show();
              $(".post_data").empty();
               $.ajax({
               headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
               url:'{{ route("getRelatedMention") }}',
               type: 'GET',
               data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_name: $('#hidden-chip').val()},
               success: function(response) { //console.log(response);
                var res_array=JSON.parse(response);
                var data=res_array[0];
                var keyForHighLight=res_array[2];

                for(var i in data) {
                   var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                   var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                   var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                   var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                   var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                   pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                   neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                   neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                   pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                   neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                   neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

                   var page_name=data[i].page_name;
                
                   var message = highLightText(keyForHighLight,data[i].message);
                   var page_Link= "https://www.facebook.com/" + page_name ;                   

                   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                             '  <div class="sl-left">'+
                             '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                             
                              ' <div class="sl-right">'+
                              '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a> '+
                              ' <div class="sl-right"> '+
                               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                                ' <div style="width:55%;display: inline-block">';
                    if(neg_pcent!=='')
                        html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                    if(pos_pcent!=='')
                        html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                    if(neutral_pcent!=='')
                        html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;
                        html+=' </div></div></div></span></p> ' +'   <p class="m-t-10" align="justify">' + message +'</p>';
                    if(data[i]['post_type'] =='photo') {
                        html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id'      ]+'">'+
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                                '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                                '</span><span class="postPanel_nameTextContainer"></span></a>';
                      } 
                    else if(data[i]['post_type'] =='video'){
                         html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                                '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                                '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                                '</span><span class="postPanel_nameTextContainer"></span></a>';
                            }
                          html +='</div></div>';
                   $(".post_data").append(html);
              }
                   $("#modal-spin").hide();
                   $("#postModal").text(name);
                   $('#show-post-task').modal('show'); 
               }
            });
                  e.handled = true;
           }
        }).on('change', '.edit_senti', function (e) {
            if (e.handled !== true) {
            var senti_id = $(this).attr('id');
            var post_id  = senti_id.replace("sentiment_","");
            var sentiment = $('#sentiment_'+post_id+' option:selected').val();
            var emotion = $('#emotion_'+post_id+' option:selected').val();

              $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
            if(sentiment == 'pos')
              $("#sentiment_"+post_id).addClass('text-success');
            else if (sentiment == 'neg')
              $("#sentiment_"+post_id).addClass('text-red');
            else
              $("#sentiment_"+post_id).addClass('text-warning');
               $.ajax({
               headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
               url:'{{ route("setMentionPredict") }}',
               type: 'POST',
               data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment},
               success: function(response) {
                if(response>=0)
                {
                  //         swal({   
                  //     title: "Updated!",   
                  //     text: "Done!",   
                  //     timer: 1000,   
                  //     showConfirmButton: false 
                  // });
                  var x = document.getElementById("snackbar")
                          $("#snack_desc").html('');
                          $("#snack_desc").html('Sentiment Changed!!');
                            x.className = "show";

                            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                    }
                 }
                });
                  e.handled = true;
              }
        
            }).on('click', '.see_comment', function (e) {
  if (e.handled !== true) {
        var attr_id = $(this).attr('id');
        var post_id  = attr_id.replace("seeComment_","");
        //alert(post_id);
        // window.open("{{ url('relatedmentioncmt?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
        window.open("{{ url('relatedmentioncmt?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&post_id="+post_id, '_blank');
        
         
        }
         e.handled = true;
          
       }).on('click', '.btn-campaign', function (e) {
              if (e.handled !== true) {
                var attr_id = $(this).attr('id');
                var post_id  = attr_id.replace("campaign_","");
                var btn_val = $("#"+attr_id).attr('aria-pressed');
                
                if(btn_val == "false") // previous false next true
                {
                  var answer = confirm("Are you sure  to add campaign ?")
                  if (answer) {
                     $.ajax({
                       headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                       url:'{{ route("SetCampaign_mention") }}',
                       type: 'POST',
                       data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_val:1},
                       success: function(response) { //alert(response)
                        if(response>=0)
                        {
                         var x = document.getElementById("snackbar")
                          $("#snack_desc").html('');
                          $("#snack_desc").html('Campaign added successfully!!');
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                        }
                      }
                    });
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline active");
                    // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                     // $("#"+attr_id).attr("aria-pressed", "false");
                     // $("#"+attr_id).removeAttr("class");
                     // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                    
                  }
              }
              else
              {
                var answer = confirm("Are you sure to remove campaign?")
                if (answer) {
                 $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("SetCampaign_mention") }}',
                   type: 'POST',
                   data: {id:post_id,brand_id:GetURLParameter('pid')},
                   success: function(response) { //alert(response)
                    if(response>=0)
                    {
                      var x = document.getElementById("snackbar")
                        $("#snack_desc").html('');
                        $("#snack_desc").html('Campaign remove successfully!!');
                        x.className = "show";

                       setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       $("#"+post_id).closest('tr').remove();
                    }
                  }
                });
              }
              else
              {
                 $("#"+attr_id).attr("aria-pressed", "true");
                 $("#"+attr_id).removeAttr("class");
                 $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline  active focus");
              }      
            }
          }
            e.handled = true;
        }).on('click', '.post_hide', function (e) {//alert("hihi");
  if (e.handled !== true) {
    var attr_id = $(this).attr('id');
    var post_id  = attr_id.replace("hide_","");
    //alert(post_id);
    var answer = confirm("Are you sure to hide this post?")
                if (answer) {
                     $.ajax({
                         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                         url:'{{ route("SetHideMention") }}',
                         type: 'POST',
                         data: {id:post_id,brand_id:GetURLParameter('pid'),table_type:'post'},
                         success: function(response) {// alert(response)
                          if(response>=0)
                          {
                            $("#"+post_id).closest('tr').remove();
                          }
                        }
                            });
                }
                else {
                    //some code
                }
     
     
     
        }
         e.handled = true;
       });

    new $.fn.dataTable.FixedHeader( oTable );

   // oTable.columns.adjust().draw();
   // $('#tbl_post').css('width', '100%');
   // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
  }

  function getAllMentionComment(fday,sday,filter_tag='')
    {
      fday = $("#year").val();
      $(".popup_post").unbind('click');
      $(".edit_predict").unbind('click');
      $("#add_tag").unbind('click');
      $("#comment_hide").unbind('click');
      var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });
      var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
            });
            var campaign_name = $('#hidden-chip').val();
            var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
      
       document.getElementById('select_page').innerHTML =$('#hidden-chip').val();
 var oTable = $('#tbl_mention_comment').DataTable({

        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": true,
        "destroy": true,
        "ordering": false   ,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },

        "ajax": {
          "url": '{{ route('getcampaignMentionCmt') }}',
          "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              //document.getElementById('title_mention_comment').innerHTML = "Comments (0)";
              // tmp_postTotal = tmp_postTotal +res.recordsTotal;
              // document.getElementById('title_mention').innerHTML = 'Mentions ('+tmp_postTotal+")";

              // document.getElementById('title_mention').innerHTML = 'Mentions ('+tmp_postTotal+")";
               // document.getElementById('title_mention').innerHTML = 'Mentions ('+res.recordsTotal+")";
              return res.data;
            },
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
           "data": {
            "fday": fday,
            "sday": sday,
            "keyword_flag": '1',
            "other_page":'1',
            "brand_id": GetURLParameter('pid'),
            "tsearch_senti":filter_senti_arr.join("| "),
            "tsearch_keyword":filter_keyword_arr.join("| "),
            "tsearch_tag":filter_tag,
            "campaign_name":campaign_name ,
            "campaign_id":campaign_id ,
            "type":'mention comment',
          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
       
        },
     drawCallback: function() {
     $('.select2').select2();
  },

        columns: [
        {data: 'post_div', name: 'post_div',"orderable": false},
    


        ]
        
      }).on('click', '.popup_post', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".post_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getRelatedMention") }}',
         type: 'GET',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { //console.log(response);
         var res_array=JSON.parse(response);
          var data=res_array[0];

       

            for(var i in data) {//alert(data[i].message);
              var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
               var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

               var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
               var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
               var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
               pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
               neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
               neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
               pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
               neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
               neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

               var page_name=data[i].page_name;
            

             var page_Link= "https://www.facebook.com/" + page_name ;                   

 var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
               '  <div class="sl-left">'+
               '<img src="assets/images/unknown_photo.jpg"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
                ' <div class="sl-right">'+
                '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a> '+
                ' <div class="sl-right"> '+
             ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
              ' <div style="width:55%;display: inline-block">';
              if(neg_pcent!=='')
              html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
            if(pos_pcent!=='')
              html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
            if(neutral_pcent!=='')
              html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                html+=' </div></div></div></span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                    data[i].message +
                  
                    '</p>';
                 if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
 $(".post_data").append(html);
     
     
        }


         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('change', '.edit_senti', function (e) {

  if (e.handled !== true) {

    var senti_id = $(this).attr('id');
    var post_id  = senti_id.replace("sentiment_","");
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    

    $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
    if(sentiment == 'pos')
     $("#sentiment_"+post_id).addClass('text-success');
    else if (sentiment == 'neg')
      $("#sentiment_"+post_id).addClass('text-red');
    else
      $("#sentiment_"+post_id).addClass('text-warning');

    
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,type:'mention'},
         success: function(response) {// alert(response)
          if(response>=0)
          {
        //         swal({   
        //     title: "Updated!",   
        //     text: "Done!",   
        //     timer: 1000,   
        //     showConfirmButton: false 
        // });
          var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Sentiment Changed!!');
                        x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });

          e.handled = true;
       }
     
    
        
  }).on('click', '.comment_hide', function (e) {//alert("hihi");
  if (e.handled !== true) {
    var attr_id = $(this).attr('id');
    var comment_id  = attr_id.replace("hide_","");
    //alert(comment_id);

     var answer = confirm("Are you sure to hide this comment?")
                if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetHideMention") }}',
         type: 'POST',
         data: {id:comment_id,brand_id:GetURLParameter('pid'),table_type:'comment'},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            $("#"+comment_id).closest('tr').remove();
          }
        }
            });
                }
                else
                {

                }
     
     
     
        }
         e.handled = true;
       }).on('click', '.popup_img', function (e) {
    if (e.handled !== true) {

         var id = $(this).attr('id');
         var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
// img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%'' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 
         }
       
      
          
    });
     
    
        
  }




      function getAllArticle(fday,sday)
      {
      	  fday = $("#year").val();
          $(".popup_post").unbind('click');
          var filter_senti_arr = [];
          $.each($("input[name='sentimentlist']:checked"), function(){            
              filter_senti_arr.push($(this).val());
            });
          var filter_keyword_arr = [];
          $.each($("input[name='keyword']:checked"), function(){            
              filter_keyword_arr.push($(this).val());
            });
          // alert(filter_senti_arr);
          var campaign_name = $('#hidden-chip').val();
          var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
          var oTable = $('#tbl_article').DataTable({
          // "responsive": true,
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": true,
          "serverSide": true,
          "destroy": true,
          "order": [],
          "autoWidth": false,
          "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "ajax": {
          "url": '{{ route('getAllArticle_forCampaign') }}',
          "dataSrc": function(res){
              var count = res.data.length;
              document.getElementById('title_article').innerHTML = 'Articles ('+res.recordsTotal+")";
              // tmp_postTotal = tmp_postTotal +res.recordsTotal;
              tmp_articleTotal = res.recordsTotal;
              document.getElementById('title_mentions').innerHTML = 'Mentions ('+(tmp_postTotal+tmp_articleTotal)+")";

              return res.data;
            },
            "data": {
            "fday": fday,
            "sday": sday,
            "brand_id": GetURLParameter('pid'),
            "tsearch_senti":filter_senti_arr.join("| "),
            "tsearch_keyword":filter_keyword_arr.join("| "),
            "campaign_name": campaign_name,
            "campaign_id": campaign_id,
          
          }

        },
        "initComplete": function( settings, json ) {
        $('#tbl_article thead tr').removeAttr('class');
        // $("#tbl_article thead").remove();
        },
        columns: [
        {data: 'post', name: 'post',"orderable": false},
        ]
        
      }).on('click', '.popup_post', function (e) {
          if (e.handled !== true) {
            var name = $(this).attr('name');
            var post_id = $(this).attr('id');
            $("#modal-spin").show();
            $(".post_data").empty();

            //alert(post_id);alert(name);alert(GetURLParameter('pid'));
             $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("getRelatedArticle") }}',
             type: 'GET',
             data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_name: $('#hidden-chip').val()},
             success: function(response) { //console.log(response);
              var res_array=JSON.parse(response);
              // console.log(//res_array[1]);
              var data=res_array[0];
              var keyForHighLight=res_array[2];
// alert(keyForHighLight);
              for(var i in data) {//alert(res_array[1]);
               var page_name=data[i].page_name;
               var link = data[i].link;
               var message = highLightText(keyForHighLight,data[i].message);
               // alert(message);
               var page_Link= "https://www.facebook.com/" + page_name ; 
               var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                        '<div class="form-material sl-left" style="padding-left:20px;">'+ 
                         '<a href="'+link+'" style="font-size:18px;font-weight:800px;" target="_blank" >' +
                         data[i].title +
                        '</a><div>' + ' <div class="sl-right"> '+
                        '<div style ="width:50%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i>' +data[i].created_time + ' by ' + '<a href="'+page_Link+'" target="_blank">' +data[i].page_name+'</a></span></div> '+
                        ' <div class="sl-right"> '+
                        ' <div style="width:55%;display: inline-block">';
                    html+=' </div></div></div> ' +                
                          '   <p class="m-t-10" align="justify">' + 
                          message +
                        
                          '</p>';
                    html +='</div></div>';
                $(".post_data").append(html);
        }
         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
      });
          e.handled = true;
    }
  }).on('click', '.btn-campaign', function (e) {
              if (e.handled !== true) {
                var attr_id = $(this).attr('id');
                var post_id  = attr_id.replace("campaign_","");
                var btn_val = $("#"+attr_id).attr('aria-pressed');
                
                if(btn_val == "false") // previous false next true
                {
                  var answer = confirm("Are you sure  to add campaign ?")
                  if (answer) {
                     $.ajax({
                       headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                       url:'{{ route("SetCampaign_article") }}',
                       type: 'POST',
                       data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_val:1},
                       success: function(response) { //alert(response)
                        if(response>=0)
                        {
                         var x = document.getElementById("snackbar")
                          $("#snack_desc").html('');
                          $("#snack_desc").html('Campaign added successfully!!');
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                        }
                      }
                    });
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline active");
                    // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                     // $("#"+attr_id).attr("aria-pressed", "false");
                     // $("#"+attr_id).removeAttr("class");
                     // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                    
                  }
              }
              else
              {
                var answer = confirm("Are you sure to remove campaign?")
                if (answer) {
                 $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("SetCampaign_article") }}',
                   type: 'POST',
                   data: {id:post_id,brand_id:GetURLParameter('pid')},
                   success: function(response) { //alert(response)
                    if(response>=0)
                    {
                      var x = document.getElementById("snackbar")
                        $("#snack_desc").html('');
                        $("#snack_desc").html('Campaign remove successfully!!');
                        x.className = "show";

                       setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       $("#"+post_id).closest('tr').remove();
                    }
                  }
                });
              }
              else
              {
                 $("#"+attr_id).attr("aria-pressed", "true");
                 $("#"+attr_id).removeAttr("class");
                 $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline  active focus");
              }      
            }
          }
            e.handled = true;
        }).on('change', '.edit_senti', function (e) {
      
              if (e.handled !== true) {
              var  brand_id = GetURLParameter('pid');
              var senti_id = $(this).attr('id');
              var post_id  = senti_id.replace("sentiment_","");
              var sentiment = $('#sentiment_'+post_id+' option:selected').val();
              var emotion = $('#emotion_'+post_id+' option:selected').val();
              

              $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
              if(sentiment == 'pos')
               $("#sentiment_"+post_id).addClass('text-success');
              else if (sentiment == 'neg')
                $("#sentiment_"+post_id).addClass('text-red');
              else
                $("#sentiment_"+post_id).addClass('text-warning');

              
                   $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("setArticlePredict") }}',
                   type: 'POST',
                   data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment},
                   success: function(response) {// alert(response)
                    if(response>=0)
                    {
                      //         swal({   
                      //     title: "Updated!",   
                      //     text: "Done!",   
                      //     timer: 1000,   
                      //     showConfirmButton: false 
                      // });
                      var x = document.getElementById("snackbar")
                                  $("#snack_desc").html('');
                                  $("#snack_desc").html('Sentiment Changed!!');
                                    x.className = "show";

                                    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                     }
                  }
                      });

                    e.handled = true;
                 }
         
         

         
           // else
           //  {
           //    var x = document.getElementById("snackbar")
           //                          $("#snack_desc").html('');
           //                          $("#snack_desc").html('You are not allowed to edit this article . Changes will not be saved !!! !!');
           //                            x.className = "show";

           //                            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
           //                            return false;
           //  }
          

        
            
      }).on('click', '.post_hide', function (e) {
          if (e.handled !== true) {
            var attr_id = $(this).attr('id');
            var post_id  = attr_id.replace("hide_","");
            var answer = confirm("Are you sure to hide this post?")
            if (answer) {
                 $.ajax({
                     headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                     url:'{{ route("SetHideMention") }}',
                     type: 'POST',
                     data: {id:post_id,brand_id:GetURLParameter('pid'),table_type:'web_mention'},
                     success: function(response) {// alert(response)
                      if(response>=0)
                      {
                        $("#"+post_id).closest('tr').remove();
                      }
                    }
                  });
               }
              else {
                  
              }
         }
         e.handled = true;
          });

        new $.fn.dataTable.FixedHeader( oTable );

         // oTable.columns.adjust().draw();
         // $('#tbl_post').css('width', '100%');
         // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
  }
    $('#show-add-tag').on('shown.bs.modal', function () {
      $('#name').focus();
  })  
    // alert(startDate);
    var permission = GetURLParameter('accountPermission');

  $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
                startDate: startDate,
                endDate: endDate,
          },function(start, end,label) {
     
            if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) 
          
            {
                 swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }


            else{
                  var startDate;
          var endDate;
          label = label;
          startDate = start;
          endDate = end;
          ChooseDate(startDate,endDate,label,'');
             }
           }
           else
           {
                    var startDate;
          var endDate;
          label = label;
          startDate = start;
          endDate = end;
          ChooseDate(startDate,endDate,label,'');
           }
        });
   //ChooseDate(startDate,endDate,'','');

//   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
//     var target = $(e.target).attr("href") // activated tab
//      //window.dispatchEvent(new Event('resize'));
//   // if(target=="#dashboard")
//   //   {
      
//   //  }

//   //    if(target == '#comment')
//   // {
//   //   $("#outer_div").removeClass("col-lg-12");
//   //   $("#outer_div").addClass("col-lg-9");
//   //   $("#filter_div").show();
   
//   // }
//   // else
//   // {
//   //    $("#outer_div").removeClass("col-lg-9");
//   //   $("#outer_div").addClass("col-lg-12");
//   //   $("#filter_div").hide();
//   // }
  
//     if(target == '#dashboard')
//      {
//        $("#outer_div").removeClass("col-lg-9");
//        $("#outer_div").addClass("col-lg-12");
//        $("#filter_div").hide();
//      }
//      else
//      {

//       $("#outer_div").removeClass("col-lg-12");
//        $("#outer_div").addClass("col-lg-9");
//        $("#filter_div").show();
//         if(target == '#mention')
//       {
//         $("#tag_filter").hide();
       
//       }
//        else if(target == '#pages')
//        {
//         $("#tag_filter").show();
        
        
//        }


//      }


// window.dispatchEvent(new Event('resize'));

//    });
              $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                   var target = $(e.target).attr("href") // activated tab
                   window.dispatchEvent(new Event('resize'));
                  if(target == '#dashboard')
                  {
                     // var graph_option = $('.btn-group > .btn.active').text();
                     // if(graph_option.trim()=='Sentiment')
                     // InboundSentimentDetail(GetStartDate(),GetEndDate());
                     
                    $("#outer_div").removeClass("col-lg-9");
                    $("#outer_div").addClass("col-lg-12");
                    $("#filter_div").hide();
                    $("#top-div").show();
                  }
                  else
                  {
                    $("#outer_div").removeClass("col-lg-12");
                    $("#outer_div").addClass("col-lg-9");
                    $("#filter_div").show();
                     $("#top-div").hide();
                  }
            });

  hidden_div();
    // alert($('#PostTotal').val());
  function MostFrequentlyTag(fday,sday)
  {
    // alert($("#hidden-chip").val());
    fday = $("#year").val();
    $(".tag_count").empty();
    $("#tag-count-spin").show();
    var brand_id = GetURLParameter('pid');

        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,campaign_name:$("#hidden-chip").val(),limit:10}
    })
    .done(function( data ) {//console.log(data);
      $(".tag_count").empty();
        $("#tag-count-spin").hide();
for(var i in data) 
        {
           var tagLabel=data[i].tagLabel;
           var tagId=data[i].tagId;
           var tagCount=data[i].tagCount;
           $(".tag_count").append('<button type="button" style="margin:0.2rem" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_frequent_tag" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>');

        }
    })
     .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
    
    });
  }

  function tagCount()
  {
    
      var fday=GetStartDate();
      var sday=GetEndDate();
      // $("#tbl_tag_count tbody").empty();
      $(".tag-list").empty();
      //alert("hihi");
      var campaign_name = $('#hidden-chip').val();
      var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
      var brand_id = GetURLParameter('pid');
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getCampaignKwCount')}}", // This is the URL to the API
        data: { 
          fday: fday,
          sday:sday,
          brand_id:brand_id,
          // filter_page_name:$('#admin_page_filter option:selected').val(),
          campaign_name:campaign_name,
          campaign_id:campaign_id,
          limit:'no'}
      })
      .done(function( data ) {//console.log(data);
      for(var i in data) 
          {
             var tagLabel=data[i].tagLabel;
             var tagId=data[i].tagId;
             var tagCount=data[i].tagCount;
            
              $(".tag-list").append(' <li>' +
              ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'">'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');

          }

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
  function getKeyword()
{
          $(".keyword-list").empty();
          var brand_id = GetURLParameter('pid');
              $.ajax({
              type: "GET",
              dataType:'json',
              contentType: "application/json",
              url: "{{route('getKeywordsByGroup')}}", // This is the URL to the API
              data: {brand_id:brand_id,campaign_name:$('#hidden-chip').val()}
            })
            .done(function( data ) {
            	console.log(data);
            for(var i in data) 
            {
              if(i<10)
              {
                $(".keyword-list").append('<li class="see-li">' +
                ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
                ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
                ' </li>');
              }
              else if (i==10)
              {
                $(".keyword-list").append('<li class="see-li">' +
                ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
                ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
                ' </li>  <li class="see-li" id="more"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see more</a></li>');
              }
              else
              {
                 $(".keyword-list").append('<li class="hidden-li" style="display:none">' +
                ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" id="K_'+data[i]+'_'+i+'">'+
                ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'_'+i+'">'+data[i]+'</label> '+
                ' </li><br/>');
              }
            }
                $(".keyword-list").append('<li class="hidden-li" id="less" style="display:none"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see less</a></li>');
        })
         .fail(function(xhr, textStatus, error) {
           // console.log(xhr.statusText);
          // console.log(textStatus);
          // console.log(error);
        
        });

        
}
$(document).on('click', '#more', function () {
   $('#more').css('display','none');
    $('.hidden-li').css('display','inline');
});
$(document).on('click', '#less', function () {
  $('#less').css('display','none');
    $('.hidden-li').css('display','none');
     $('#more').css('display','inline');
});

  $(document).on('click', '.taglist', function () {
    getAllComment(GetStartDate(),GetEndDate());
    getAllPost(GetStartDate(),GetEndDate());
    getAllMentionPost(GetStartDate(),GetEndDate());
    getAllMentionComment(GetStartDate(),GetEndDate());
  });
  $(document).on('click', '.sentimentlist', function () {
     tmp_articleTotal=0;tmp_postTotal=0;
    getAllComment(GetStartDate(),GetEndDate());
    getAllPost(GetStartDate(),GetEndDate());
    getAllMentionPost(GetStartDate(),GetEndDate());
    getAllMentionComment(GetStartDate(),GetEndDate());
    getAllArticle(GetStartDate(),GetEndDate());
   // tagCount();
});
  $(document).on('click', '.keyword', function () {
    tmp_articleTotal=0;tmp_postTotal=0;
   getAllMentionPost(GetStartDate(),GetEndDate());
   getAllMentionComment(GetStartDate(),GetEndDate());
   getAllPost(GetStartDate(),GetEndDate());
   getAllComment(GetStartDate(),GetEndDate());
   getAllArticle(GetStartDate(),GetEndDate());
});
  $(document).on('click','.top_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:80%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
    $(document).on('click','.top_cmt_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
  $('#senti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.taglist)").click(function () {
    $("#senti-all").prop("checked", $("input:checkbox:not(.taglist):checked").length == 4);
  });

  bindOtherPage();
  $(document).on('click', '.chip', function (e) {
  	// alert('jee');
    var id = $(this).attr("id");  
    var name = $(this).attr("name");
    // alert(name);
    var chipcompetitor = document.getElementById("chip-competitor");
    //alert(name);  
      $(".chip").css("background-color","#e4e4e4");
      $(".chip").css("color","rgba(0,0,0,0.6)");
       // chipcompetitor.classList.remove("myHeaderChip");
     // chipcompetitor.classList.add("myHeaderChip");
     // $('.dropdown.open .dropdown-toggle').dropdown('toggle');
     // $("#search_form").css("visibility", "hidden");
     // $("#search_form").css("display", "inline");
      $("#"+id).css("background-color","rgba(188, 138, 49, 1)");
      $("#"+id).css("color","white");
      // $('#title_page').text(0)
      $("#hidden-chip").val(name);
    // alert($("[data-toggle=dropdown]").attr("aria-expanded"))  ;
     $(".dropdown-pages").dropdown('toggle');
      getKeyword();
      ChooseDate(startDate,endDate,'','');
     
      
        });
 // $("#btn_pages").on('click', function () {
 //  $("#search_form").css("visibility", "visible");
 //  $("#search_form").css("display", "inline");
 // });
//  $("[data-toggle=dropdown]").prop('onclick', null).off('click');
// $("[data-toggle=dropdown]").click(function (e) {
//     e.preventDefault();
//     // e.stopPropagation();
//     // console.log($(this));
//     let el = $(this);
//     let expanded = el.attr("aria-expanded") || false;
//     alert(expanded);
//     if (!expanded) {
//         $(this).dropdown('toggle');
//     }

//     el.attr("aria-expanded", !expanded);
// })
// var campaign_name = $('#hidden-chip').val();
// alert (campaign_name);
 // var campaign_name = $('#hidden-chip').val();
 // alert(campaign_name);

 $("#page_name").on('change', function() {

	 bindOtherPage();

 })

 $("#year").on('change', function() {

    bindOtherPage();

 })

  function bindOtherPage()
  {
    var page_name = ''; var username = ''; var year = '';

  	page_name = $("#page_name").val();
    username = $("#hidden-user").val();
    year = $("#year").val();
    $("#chip-competitor").empty();
    $("#select_page").text('');
    $("#hidden-chip").val('');
    var brand_id = GetURLParameter('pid');
      console.log(brand_id);
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getOtherCampaign')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id, page_name:page_name, year:year
          }
      })
      .done(function( data ) {//alert(data);
       console.log(data);
      if ($.trim(data)){   
        for(var i in data) 
          {
             var photo ="{{asset('assets/images/competitor1.png')}}";
         
             if(parseInt(i) == 0)
              { 
                
                $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['name']+'" id="'+data[i]['id']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['name'] + '</div>');
             
                   $("#hidden-chip").val(data[i]['name']); 
                
              }
              else
              {
                $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['name']+'"  id="'+data[i]['id']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['name'] + '</div>');
              }

          }
          ChooseDate(startDate,endDate,'','');
          getKeyword();
        }else{

          EmptyChooseDate(startDate,endDate,'','');
          getKeyword();

        }

        

      })

       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);

      
      });

  }


    $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
      // alert(picker.startDate);
        $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
         });


  $('#admin_page_filter').change(function() {
      //alert($(this).val());
      var admin_page = $(this).val();
      ChooseDate(startDate,endDate,'','');

     
       // $(this).val() will work here
  });


  $('input[type=radio][name=options]').change(function() {

    if (this.value == 'page') {
     var date_preset= Calculate_DatePreset(label);
        PageGrowth(GetStartDate(),GetEndDate(),date_preset);
        
    }
    else if (this.value == 'sentiment') {
        InboundSentimentDetail(GetStartDate(),GetEndDate());
    }
    
});
  $(".btnComment").click(function(e){
    var commentType = $(this).attr('value');
    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ $('#hidden-chip').val() , '_blank');
})
$(".btnPost").click(function(e){
    var postType = $(this).attr('value');
    window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&overall="+postType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ $('#hidden-chip').val() , '_blank');
})
  $(document).on('click', '.btn_frequent_tag', function() {
     var TagName = $(this).attr('value');
     // alert(TagName);
      // window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&campaign_name="+ $('#hidden-chip').val() , '_blank');
       window.open("{{ url('related_campaigncomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&campaign_name="+ $('#hidden-chip').val() , '_blank');

  })
  
  //local function
  function highLightText(keyForHighLight,message)
{
  // alert(keyForHighLight['campaign_keyword']);
// console.log(keyForHighLight);
  keyForHighLight.sort(function(a, b){return b['campaign_keyword'].length - a['campaign_keyword'].length});
  var highligh_string  = message;
  
for(var i in keyForHighLight) {
   highligh_string = highligh_string.replace( new RegExp(keyForHighLight[i]['campaign_keyword'], "ig") ,'<span style="background:#FFEB3B">' + keyForHighLight[i]['campaign_keyword'] + '</span>')
 }
 return highligh_string;
}

  function numberWithCommas(n) {
      var parts=n.toString().split(".");
      var res = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
      if(res == '')
        res = 0

      return res;
  }

    function kFormatter(num) {
      return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
  }
    function readmore(message){
        // alert("hi hi ");
          var string = String(message);
          var length = string.length; 
           // alert(length);
                  if (length > 500) {
            // alert("length is greater than 500");

              // truncate

              var stringCut = string.substr(0, 500);
               // alert(stringCut);
              // var endPoint = stringCut.indexOf(" ");

              //if the string doesn't contain any space then it will cut without word basis.
               
              // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
              string =stringCut.substr(0,length);
              // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
              // alert(string);
          }
          return string;


          }

          

    });
      </script>
<style type="text/css">

table.fixedHeader-floating {
  clear: both;
  top: 114px !important;
  z-index:5;
   top : 0;
   left:0;
   width: 100%;
/*   margin: 0 auto;
     padding: 0 15px;*/
 /*    overflow-x: hidden;
      overflow-y: auto;*/
  
    box-sizing: border-box;
/*
  background: red;*/
/*  left:150px !important;*/
/*  margin-right:30px;*/
 
/*  width: 80% !important;*/
  /*background: transparent;*/
}
/*table.dataTable thead tr th {
    word-wrap: break-word;
    word-break: break-all;
}*/
table.dataTable tbody tr td {
    word-wrap: break-word;
    word-break: break-all;
}
.myHeaderContent
{
   margin-right:300px;
}
.TopDivPadding
{
  padding-left:10px;
}
.TopDivCardBody
{
  padding-top:1rem;
  padding-bottom:1rem;
}
.TopDivNumber
{
  font-weight: 500;
  font-size: 40px;
}
 #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }
  .btn {
    padding: 2px 6px;
    font-size: 14px;
    cursor: pointer;
}


  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
  @endpush

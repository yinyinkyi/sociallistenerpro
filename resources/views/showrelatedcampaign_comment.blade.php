@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])

@section('content')

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                 <header class="" id="myHeader">
                  <div class="row page-titles">

                  
                     <input type="hidden" value="{{ auth()->user()->role }}" class ="hidden-role" name="hidden-kw"/>
                   

                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500"><span id="title_comment">Campaign Comments</span></h4>
                        <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <!-- <div class="chart-text m-r-10">
                                      <div class='form-material input-group'>
                                    <select class="form-material form-control" id="global_senti">
                                              <option value="">Filter Sentiment</option>
                                              <option value="pos">Positive</option>
                                              <option value="neg">Negative</option>
                                              <option value="neutral">Neutral</option>
                                            </select>
                            </div></div> -->
                               <!--  <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div> -->
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
  </header>
               
             
 <div class="row" >
                        <div class="col-lg-9">
                       
                     <div class="card">
                            <div class="card-body">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div class="table-responsive">
                               <table id="tbl_comment" class="table" style="width:100%;">
                              <thead>
                                <tr>
                                  <th></th>
                                
                                </tr>
                              </thead>

                            </table>
                            </div>
                          </div>
                   
                        </div>
                      </div>
                      <div class="col-lg-3">
                          <div class="card earning-widget" style="height:230px">
                              <div class="card-header">
                       
                                  <h4 class="card-title m-b-0">Sentiment</h4>
                              </div>
                              <div class="card-body b-t collapse show">
                            
                                   <div class="form-group">
                                                    
                                                      <div class="input-group">
                                                          <ul class="icheck-list sentiment-list" style="padding-top:0px">
                                                          <li>
                                   <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="all" id="senti-all"> 
                                    <label  for="senti-all" style="padding-left:30px;font-weight:500;font-size:12px">all</label>
                                    </li>
                                     <li>
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="pos" id="senti-pos"> 
                                     <label class="text-success" for="senti-pos" style="padding-left:30px;font-weight:500;font-size:12px">positive</label>
                                    </li>
                                    <li>
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neg" id="senti-neg"> 
                                    <label class="text-danger" for="senti-neg" style="padding-left:30px;font-weight:500;font-size:12px">negative</label>
                                    </li>
                                    <li> 
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neutral" id="senti-neutral"> 
                                    <label class="text-info"   for="senti-neutral" style="padding-left:30px;font-weight:500;font-size:12px">neutral</label>
                                    </li>
                                                             
                                                          </ul>
                                                      </div>
                                                  </div>
                              </div>
                          </div>
 <div class="card earning-widget" style="height:550px">
      <div class="card-header">
        <h4 class="card-title m-b-0">Keywords</h4>
     </div>
      <div class="card-body b-t collapse show" style="overflow-y:scroll;height:550px">
        <div class="form-group">
          <div class="input-group">
              <ul class="icheck-list keyword-list" style="padding-top:0px">
              </ul>
          </div>
        </div>
      </div>
    </div>
                          <!-- <div class="card-body inbox-panel " style="background-color:#f3f6f9;margin-right:5px">
                        <div class="card-header" style="background-color:#fff;border-left:1px solid #dee2e6;border-right:1px solid #dee2e6;border-top:1px solid #dee2e6">
                        
                                <h4 class="card-title m-b-0">Tags</h4>
                            </div>
                     <div class="jsgrid-grid-body" style="overflow-y:scroll;background-color:#fff;border:1px solid #dee2e6;height:450px">
                                <table id="tbl_tag_count" class="table v-middle  no-border">
                                    <tbody>
                                      
                                    </tbody>
                                </table>
                            </div>
                        </div> -->
                      </div>
                      </div>
                                          <div id="show-image" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div align="center" class="modal-body large_img">
                                                 
                                              </div>

                                          </div>
                                        
                                      </div>
                                
                   </div>
                  
                <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="postModal">Post Detail</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body post_data">
                                               
                                            </div>
                                        <!--     <div class="modal-footer">
                                                <button type="button" id="btnSeeComment" class="btn btn-info waves-effect text-left" data-dismiss="modal">See Comments</button>
                                            </div> -->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
               <div id="show-add-tag" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tag_title" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="tag_title">Add New Tag</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body add-tag">
                                               <!-- <form method="POST" role="form" action="{{ route('tags.store') }}" aria-label="{{ __('Tags') }}"> -->
                <!-- {{csrf_field()}} -->
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                   
                        <span class="invalid-name" role="alert">
                          
                        </span>
                 
                </div>
<!--                               <div class="form-group">
                  <label for="category_id" class="control-label">{{ __('Category') }}</label>
                          <select class="form-control custom-select" id="category_id" name="category_id">
                          <option value=""  selected>Choose Category</option>

                          @if (isset($category))
                          @foreach($category as $index =>$category)
                          <option value="{{$category->id}}" >{{$category->category_name}}</option>
                          @endforeach
                          @endif
                         </select>
                     
                          <span class="invalid-category" role="alert">
                            
                          </span>
                   
              </div> -->
            <!-- <div class="form-group">
                <label for="keywords" class="control-label">{{ __('Keywords') }}</label>
                 <input id="keywords" type="text"
               class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old('keywords') }}" required>
                
                    <span class="invalid-keyword">
                       
                    </span>
               
            </div> -->
 
            <div class="form-group">
                <button type="button" id="add_new_tag" class="btn btn-primary">  {{ __('Save') }}</button>
              </div>
             <!-- </form>  -->
                                            </div>
                              
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
</div>
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
   <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/icheck/icheck.min.js')}}"  defer></script>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
/*global mention*/
var mention_total;
var mentionLabel = [];
var mentions = [];
/*Bookmark Array*/
var bookmark_array=[];
var bookmark_remove_array=[];
/*global sentiment*/
var positive = [];
var negative = [];
var sentimentLabel = [];
var positive_total=0;
var negative_total=0;
var  colors=["#1e88e5","#dc3545","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

$(document).ready(function() {



    //initialize
 $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
/* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

 $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

      var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

    startDate = moment().subtract(1, 'month');
    endDate = moment();

           $('.singledate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        },function(date) {
          endDate=date;
          var id =$('input[type=radio][name=period-radio]:checked').attr('id');
          if(id==="period-week")
         {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
         }
         else
         {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
         }
         // alert(startDate);
         // alert(endDate);
         var admin_page=$("#admin_page_filter").val();

         ChooseDate(startDate,endDate,admin_page,'');
       
      });



 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Dashboard'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}


   function ChooseDate(start, end,admin_page='',label='',comefrom='') {//alert(start);

         // var filter_senti=$('#global_senti option:selected').val();
           var filter_senti_arr = [];
                $.each($("input[name='sentimentlist']:checked"), function(){            
                    filter_senti_arr.push($(this).val());
                  });
          var filter_tag_arr = [];
                $.each($("input[name='taglist']:checked"), function(){            
                    filter_tag_arr.push($(this).val());
                  });
            if(start !== '')
            {

              $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
                  startDate=start;
                  endDate=end;
                  
                 getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag_arr.join("| "),filter_senti_arr.join("| "),comefrom);
            }
            else
              getAllComment('','',filter_tag_arr.join("| "),filter_senti_arr.join("| "),comefrom); // if come from post -> see comment that can know post id , just select with post id
                 
                   
              // tagCount(comefrom);
}

        function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}

 function getAllComment(fday,sday,filter_tag='',filter_senti='',comefrom='')
    {
      // alert(GetURLParameter('campaign_name'));
      // alert(filter_senti);
      $(".popup_post").unbind('click');
      $(".edit_predict").unbind('click');
      $("#add_tag").unbind('click');
      if(filter_tag == '' )
      {
      filter_tag=GetURLParameter('search_tag');
      // alert(filter_tag);

      }
      if(filter_senti == '')
      {
      filter_senti=GetURLParameter('CmtType');
      }
      if(comefrom == '')
      {
        comefrom=GetURLParameter('comefrom');
      }
      var filter_senti_arr = [];
        $.each($("input[name='sentimentlist']:checked"), function(){            
            filter_senti_arr.push($(this).val());
          });
        // alert(filter_senti_arr.join("| "));

       var filter_keyword_arr = [];
          $.each($("input[name='keyword']:checked"), function(){            
              filter_keyword_arr.push($(this).val());
            });

           var campaign_name = GetURLParameter('campaign_name');
            var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
          // alert(filter_keyword_arr);

      // alert(GetURLParameter('highlight_text'));
// alert(filter_tag);
      var oTable = $('#tbl_comment').DataTable({

        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": true,
        "destroy": true,
        "ordering": false   ,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        
        "ajax": {
          "url": '{{ route('getRelatedCampaign_comment') }}',
          "dataSrc": function(res){
              //var count = res.data.length;
              // alert(GetURLParameter('highlight_text'));
              document.getElementById('title_comment').innerHTML = 'Campaign Comments ('+res.recordsTotal+")";
              return res.data;
            },
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
           "data": {
            "fday": fday,
            "sday": sday,
            "comefrom" : comefrom,
            "campaign_name":campaign_name,
            "campaign_id":campaign_id,
            "brand_id": GetURLParameter('pid'),
            "post_id":GetURLParameter('post_id'),
            // "tsearch_senti":filter_senti,
            "tsearch_tag":filter_tag,
            "tsearch_senti_arr":filter_senti_arr.join("| "),
            "tsearch_keyword":filter_keyword_arr.join("| "),

          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
       
        },
     drawCallback: function() {
     $('.select2').select2();
  },

        columns: [
        {data: 'post_div', name: 'post_div',"orderable": false},
    


        ]
        
      }).on('click', '.show_alert', function (e) {
           e.preventDefault();
           $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
            // var url = $(this).data('remote');
            swal({
                title: 'Sorry',
                text: "This account has no permission to do this!",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })

        }).on('click', '.popup_post', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    // alert(post_id);
    $("#modal-spin").show();
    $(".post_data").empty();
    
    // alert(GetURLParameter('highlight_text'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getRelatedPosts") }}',
         type: 'GET',
         data: {id:post_id,brand_id:GetURLParameter('pid'), campaign_name:GetURLParameter('campaign_name')},
         success: function(response) { //console.log(response);
         var res_array=JSON.parse(response);
          var data=res_array[0];
          var monitor=res_array[1];
          var monitor_full_name=res_array[2];
          var monitor_page_id=res_array[3];

            for(var i in data) {//alert(data[i].message);
              var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
               var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

               var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
               var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
               var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
               pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
               neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
               neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
               pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
               neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
               neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

               var page_name=data[i].page_name;
              // if(isNaN(page_name)==false)
              // {
              //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
              //  page_name = b[0];
              // }

              var found_index = monitor.indexOf(page_name);
              if (typeof monitor_page_id [found_index] !== 'undefined') 
              var page_photo_id = monitor_page_id [found_index];

              if (typeof monitor_full_name [found_index] !== 'undefined') 
              var page_name = monitor_full_name [found_index];
              
              var page_Link= 'https://www.facebook.com/' + page_name ;  
                if(page_photo_id !== '')
              var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large'; 
              else 
              var page_photo ='assets/images/unknown_photo.jpg';  


             // var campaign_name =GetURLParameter('campaign_name');
             
             //  var message = highLight(keywordToHighlight,data[i].message);
             var message = data[i].message;


 var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
               '  <div class="sl-left">'+
               '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
                ' <div class="sl-right">'+
                '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a>';
                // if(parseInt(data[i].isDeleted) == 1)
                // html+=' <span class="text-red">This post is no longer available on FB</span> ';
                html+=' <div class="sl-right"> '+
             ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
              ' <div style="width:55%;display: inline-block">';
              if(neg_pcent!=='')
              html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
            if(pos_pcent!=='')
              html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
            if(neutral_pcent!=='')
              html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                html+=' </div></div></div></span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                    message +
                  
                    '</p>';
                  if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';  
 $(".post_data").append(html);
     
     
        }


         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.edit_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    var tags = $('#tags_'+post_id).val();
  //   alert(sentiment);
  // alert(tags);
  // return;
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags},
         success: function(response) {// alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  }).on('change', '.edit_senti', function (e) {
  
    var hidden_chip  = $('#hidden-chip').val();
    var default_page = $('.hidden-pg').val();
     var user_role = $('.hidden-role').val();
    if(user_role == 'Admin' || hidden_chip === default_page){
      if (e.handled !== true) {

    var senti_id = $(this).attr('id');
    var post_id  = senti_id.replace("sentiment_","");
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    var tags = $('#tags_'+post_id+' option:selected').map(function () {
        return $(this).text();
    }).get().join(',');

    var tags_id = $('#tags_'+post_id).val();

    $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
     if(sentiment == 'pos')
     $("#sentiment_"+post_id).addClass('text-success');
    else if (sentiment == 'neg')
      $("#sentiment_"+post_id).addClass('text-red');
    else
      $("#sentiment_"+post_id).addClass('text-warning');
  
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
         success: function(response) { //alert(response)
          if(response>=0)
          {
         var x = document.getElementById("snackbar")
        $("#snack_desc").html('');
        $("#snack_desc").html('Sentiment Changed!!');
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }

        }
            });

          e.handled = true;
       }
    }
       else{
        var x = document.getElementById("snackbar")
        $("#snack_desc").html('');
        $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        }
  }).on('change', '.edit_tag', function (e) {
    var hidden_chip  = $('#hidden-chip').val();
    var default_page = $('.hidden-pg').val();
    var user_role = $('.hidden-role').val();
    if(user_role == 'Admin' || hidden_chip === default_page){
      if (e.handled !== true) {
    var senti_id = $(this).attr('id');
    var post_id  = senti_id.replace("tags_","");
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    var tags = $('#tags_'+post_id+' option:selected').map(function () {
        return $(this).text();
    }).get().join(',');

    var tags_id = $('#tags_'+post_id).val();
    
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
         success: function(response) {
          tagCount('comment');
          if(response>=0){
          var x = document.getElementById("snackbar")
          $("#snack_desc").html('');
          $("#snack_desc").html('Tag Changed!!');
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
      });
          e.handled = true;
       }
     }
  else{
    var x = document.getElementById("snackbar")
    $("#snack_desc").html('');
    $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }
  }).on('click', '#add_tag', function (e) {
  if (e.handled !== true) {
         $('#show-add-tag').modal('show'); 
        }
          
          e.handled = true;
       
     
    
        
  }).on('click', '.popup_img', function (e) {
    if (e.handled !== true) {

         var id = $(this).attr('id');
         var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
// img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 
         }
       
      
          
    });
     
    
        
  }
  $('#show-add-tag').on('shown.bs.modal', function () {
    $('#name').focus();
})  
$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end,label) {//alert(label);
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'',label,'comment');
      });

var fdate = new Date(GetURLParameter('fday'));
startDate=moment(fdate);
var sdate = new Date(GetURLParameter('sday'));
endDate=moment(sdate);
ChooseDate(startDate,endDate,'','','');

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   window.dispatchEvent(new Event('resize'));

 });

hidden_div();

    function highLight(keywordToHighlight,message)
    {
       // keywordToHighlight.sort(function(a, b){return b.length - a.length});
        highligh_string  = message;
       
       // dd($message);
        
         highligh_string = highligh_string.replace(new RegExp(keywordToHighlight, "ig") , "<span style='background:#FFEB3B'>"+keywordToHighlight+"</span>");
          // highligh_string = highligh_string.replace( new RegExp(keywordToHighlight, "ig") ,'<span style="background:#FFEB3B">' + keyForHighLight[i] + '</span>')
         // str_replace("facts", "truth", $my_str);
      
        //dd($highligh_string );

        return highligh_string;


         

    }
    getKeyword();
  function getKeyword()
{
  // alert('hey');
     $(".keyword-list").empty();
    var brand_id = GetURLParameter('pid');
        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getKeywordsByGroup')}}", // This is the URL to the API
      data: {brand_id:brand_id,campaign_name:GetURLParameter('campaign_name')}
    })
    .done(function( data ) {console.log(data);
          
      for(var i in data) 
        {
          if(i<4)
          {
            $(".keyword-list").append('<li class="see-li">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" '+
            ' id="K_'+data[i]+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'">'+data[i]+'</label> '+
            ' </li>');
          }
          else if (i==4)
          {
            $(".keyword-list").append('<li class="see-li">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" '+
            ' id="K_'+data[i]+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'">'+data[i]+'</label> '+
            ' </li>  <li class="see-li" id="more"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see more</a></li>');
               
          }
          else
          {
             $(".keyword-list").append('<li class="hidden-li" style="display:none">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'"'+
            ' id="K_'+data[i]+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'">'+data[i]+'</label> '+
            ' </li><br/>');

          }
                         
         


        }
        $(".keyword-list").append('<li class="hidden-li" id="less" style="display:none"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see less</a></li>');
   
    

    })
     .fail(function(xhr, textStatus, error) {
       // console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
    
    });

        
}


// function tagCount(comefrom)
// {

//     // var fday = moment().subtract(1, 'month');
//     // var sday = moment();
//     // fday=fday.format('YYYY MM DD');
//     // sday=sday.format('YYYY MM DD');
//     var fday=GetStartDate();
//     var sday=GetEndDate();
//     //alert(fday);
    
//     if(comefrom == '')
// {
//   comefrom=GetURLParameter('comefrom');
// }
  
//   //  $("#tbl_tag_count tbody").empty();
//     $(".tag-list").empty();
//     var brand_id = GetURLParameter('pid');
//         $.ajax({
//       type: "GET",
//       dataType:'json',
//       contentType: "application/json",
//       url: "{{route('getCampaignTagCount')}}", // This is the URL to the API
//       data: { 
//         fday: fday,
//         sday:sday,
//         brand_id:brand_id,
//         campaign_name:GetURLParameter('campaign_name'),
//         period:GetURLParameter('period'),
//         limit:'no' 
      
//       }
//     })
//     .done(function( data ) {//console.log(data);
// for(var i in data) 
//         {
//            var tagLabel=data[i].tagLabel;
//            var tagId=data[i].tagId;
//            var tagCount=data[i].tagCount;
//            // $("#tbl_tag_count tbody").append(' <tr style="border-bottom:1px solid rgba(120, 130, 140, 0.13)"> '+
//            //                                  ' <td style="width:40px" span="2"> <button type="button" id="'+tagId+'" class="btn '+
//            //                                  ' btn-rounded btn-block btn-green btn_tag">'+tagLabel+'</button></td> '+
//            //                                  ' <td></td> '+
//            //                                  '<td class="text-right"> '+
//            //                                  ' <span class="label label-light-info">'+tagCount+'</span></td> '+
//            //                                  ' </tr>');
//            $(".tag-list").append(' <li>' +
//               ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'">'+
//               ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
//               ' </li>');

//         }

//     })
//      .fail(function(xhr, textStatus, error) {
//        console.log(xhr.statusText);
//       console.log(textStatus);
//       console.log(error);
    
//     });
// }
  $(document).on('click', '.taglist', function () {
      var filter_tag_arr = [];
              $.each($("input[name='taglist']:checked"), function(){            
                  filter_tag_arr.push($(this).val());
                });
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });
            
  getAllComment(GetStartDate(),GetEndDate(),filter_tag_arr.join("| "),filter_senti_arr.join("| "),'comment');
  
  });
  $(document).on('click', '.sentimentlist', function () {//alert("hi");
     var filter_tag_arr = [];
              $.each($("input[name='taglist']:checked"), function(){            
                  filter_tag_arr.push($(this).val());
                });
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });
            // alert('ok');
     getAllComment(GetStartDate(),GetEndDate(),filter_tag_arr.join("| "),filter_senti_arr.join("| "),'comment');
  //   tagCount('comment');
});
    $(document).on('click', '.keyword', function () {
   getAllComment(GetStartDate(),GetEndDate());
});
  $('#senti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.taglist)").click(function () {
    $("#senti-all").prop("checked", $("input:checkbox:not(.taglist):checked").length == 4);
  });

$("#add_new_tag").click(function(){
  //alert("hihi");
    var name = $("#name").val();
    var nameLength = name.length;
   // var category_id = $("#category_id").val();

    if(name == '')
    {
      $(".invalid-name").append("<strong class='text-danger'>Please fill out name!</strong>");
      return false;
    }
    else if (nameLength > 80 )
      {
      $(".invalid-name").append("<strong class='text-danger'>The name may not be greater than 80 characters!</strong>");
      return false;
    }
    //  if(category_id == '')
    // {
    //   $(".invalid-category").append("<strong class='text-danger'>Please choose category!</strong>");
    //   return false;
    // }
    //var keywords = $("#keywords").val();
    var brand_id = $("#brand_id").val();
    
   
    var keywords = '';

  //   if(keywords == '')
  //   {
  //   $(".invalid-keyword").append("<strong>Please fill out keyword</strong>");
  //   return false;
  // }
     $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("quick_tag") }}',
           type: 'POST',
           data: {name:name,keywords:keywords,brand_id:brand_id},
           success: function(response) {//alert(response);
            //alert(response);
            if(response !== "exist")
            {
               $('.edit_tag')
           .append($("<option></option>")
                      .attr("value",response)
                      .text(name)); 
                      var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Tag added successfully!!');
      x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          //             swal({   
          //     title: "Success!",   
          //     text: "Tag " + name + " is already added!" ,   
          //     timer: 500 ,   
          //     showConfirmButton: false 
          // });
            }

            else if (response == "exist")
            {
              var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('This tag already exist !!');
      x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
               $("#name").val('');
               $("#keywords").val('');
       $('#show-add-tag').modal('toggle');
           
          }
        });

  });

  $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
       });


$('#admin_page_filter').change(function() {
    //alert($(this).val());
    var admin_page = $(this).val();
   ChooseDate(startDate,endDate,admin_page,'');
     // $(this).val() will work here
});

// $(document).on('click', '.btn_tag', function () {
//     var filter_tag=this.id;
//     var filter_senti=$('#global_senti option:selected').val();
//     getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag,filter_senti,'comment');
// });
$(document).on('click','.dropdown-menu a',function(){
var row_id=this.id;
row_id = row_id.substring(row_id.indexOf('_')+1);
// alert(row_id);
      $("#btnaction_"+row_id+":first-child").text($(this).text());
      $("#btnaction_"+row_id+":first-child").val($(this).text());
      // $("#btnaction_"+row_id).removeClass('btn-red');
      // $("#btnaction_"+row_id).removeClass('btn-success');
      // if($(this).text() === "Require Action")
      // {
      // $("#btnaction_"+row_id).addClass('btn-red');
      // }
      // else
      // {
      //   $("#btnaction_"+row_id).addClass('btn-success');
      // }

       $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setActionUpdate") }}',
         type: 'POST',
         data: {id:row_id,brand_id:GetURLParameter('pid'),action_status:$(this).text()},
         success: function(response) { //alert(response)
          if(response>=0)
          {
       
          }
        }
            });

      //save action status and taken person in database
});
// $('#global_senti').change(function(){
//   var filter_senti=$('#global_senti option:selected').val();
//   getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),'',filter_senti,'comment');
//   tagCount('comment');

// });

//local function

function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

  function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

        

  });
    </script>
    <style type="text/css">
.table td, .table th {
    padding: .75rem;
    vertical-align: middle;
    border-top: 1px solid #dee2e6;
}
.table thead
{
  color:#26c6da;
}
.myHeaderContent
{
   margin-right:300px;
}
/*.s_topbar {
    position: relative;
    z-index: 50;
    -webkit-box-shadow: 5px 0px 10px rgba(0, 0, 0, 0.5);
    box-shadow: 2px 0px 2px rgba(0, 0, 0, 0.5);
}*/
#snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: -125px;
  background-color: #7e7979;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 2;
  left: 50%;
  bottom: 30px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;} 
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;} 
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}
    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('admin_page_filter')
<li class="nav-item" style="display: none"> 
<div class="btn-group">
        <select id="admin_page_filter" class="form-control custom-select">
        <option value="">Admin Pages</option>
        @if (isset($ownpage))
        @foreach($ownpage as $ownpage)
        <option value="{{$ownpage}}" id="{{$ownpage}}" >{{$ownpage}}</option>
        @endforeach
        @endif                                
      </select>
    </div>
  </li>
                      
@endsection
@section('content')

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
       <header class="" id="myHeader">
          <div class="row page-titles">
            <div class="col-md-7 col-6 align-self-center">
              <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                 <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#pages" role="tab"><span id="title_page">Pages</span></a> </li>
                 <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#mention" role="tab"><span id="title_mention">Mentions</span></a> </li>
                  
                  <input type="hidden"  id="PostTotal">
                  <input type="hidden"  id="CommentTotal">
                  
                </ul>
                <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
            </div>
            <div class="col-md-5 col-6 align-self-center">
              <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                  <div class="chart-text m-r-10">
                    <div class='form-material input-group'>
                    </div></div>
                </div>
                <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                   <input type='text' class="form-control dateranges" style="datepicker" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                      <span class="ti-calendar"></span>
                      </span>
                    </div>
                </div>
              </div>
            </div>
        </div>
      </header>
             
      <div class="row" >
       <div class="col-lg-9">
          <div class="card">
            <div class="tab-content">
            <div class="tab-pane active" id="pages" role="tabpanel">
            <div class="row" style="margin-right:-10px;margin-left:-10px">
              <div class="col-lg-12" id="post_div">
                 <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                   <li class="nav-item" style="width:50%;text-align:center"> <a class="nav-link active" data-toggle="tab" href="#pages_post" role="tab"><span id="title_post">Posts</span></a> </li>
                  </ul>
                  <div class="card-body b-t collapse show" style="padding-top:0px">
                    <div class="tab-content">
                      <div class="tab-pane active" id="pages_post" role="tabpanel">
                        <div class="row" >
                          <div class="col-lg-12">
                            <div class="card-body b-t collapse show" style="padding-top:0px">
                              <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                  <div class="table-responsive">
                                    <table id="tbl_post"  class="table" style="margin-top:4px;" >
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>Reaction</th>
                                        <th>Shares</th>
                                        <!-- <th>Overall</th> -->
                                      </tr>
                                    </thead>

                                  </table>
                                  </div>
                              </div>
                           </div>
                        </div>
                      </div>
                       
                     </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="tab-pane " id="mention" role="tabpanel">
            <div class="row" style="margin-right:-10px;margin-left:-10px">
              <div class="col-lg-12" id="post_div">
                 <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                   <li class="nav-item" style="width:50%;text-align:center"> <a class="nav-link" data-toggle="tab" href="#article" id="article_tab" role="tab"><span id="title_article">Articles</span></a> </li>
                   <li class="nav-item" style="width:50%;text-align:center"> <a class="nav-link active" data-toggle="tab" href="#mention_post" role="tab"><span id="title_mention_post">Posts</span></a> </li>
                  <!--  <li class="nav-item" style="width:33%;text-align:center"> <a class="nav-link" data-toggle="tab" href="#mention_comment" role="tab"><span id="title_mention_comment">Comments</span></a> </li> -->
                  </ul>
                  <div class="card-body b-t collapse show" style="padding-top:0px">
                    <div class="tab-content">
                      <div class="tab-pane" id="article" role="tabpanel">
                          <div class="card-body">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="article-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div class="table-responsive">
                                <table id="tbl_article"  class="table" style="margin-top:4px;" width="100%">
                                  <thead style="display: none;"> </thead>
                                </table>
                              </div>
                          </div>
                      </div> 
                      <div class="tab-pane active" id="mention_post" role="tabpanel">
                        <div class="row" >
                          <div class="col-lg-12">
                            <div class="card-body b-t collapse show" style="padding-top:0px">
                              <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                  <div class="table-responsive">
                                    <table id="tbl_mention_post"  class="table" style="margin-top:4px;" >
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>Reaction</th>
                                        <th>Shares</th>
                                        <!-- <th>Overall</th> -->
                                      </tr>
                                    </thead>

                                  </table>
                                  </div>
                              </div>
                           </div>
                        </div>
                      </div>
                       <div class="tab-pane" id="mention_comment" role="tabpanel">
                        <div class="row" >
                         <div class="col-lg-12">
                          <div class="card-body b-t collapse show" style="padding-top:0px">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="mention-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                 <div class="table-responsive">
                                    <table id="tbl_mention_comment" class="table" style="width:100%;">
                                    <thead>
                                      <tr>
                                        <th></th>
                                      
                                      </tr>
                                    </thead>

                                  </table>
                                </div>
                            </div>
                          </div>
                        </div>
                       </div>
                     </div>
                  </div>
              </div>
            </div>
          </div>
         </div>
        </div>
      </div>

       <div class="col-lg-3">
        <div class="card earning-widget" style="height:230px">
              <div class="card-header">
       
                  <h4 class="card-title m-b-0">Sentiment</h4>
              </div>
              <div class="card-body b-t collapse show">
            
                   <div class="form-group">
                                    
                                      <div class="input-group">
                                          <ul class="icheck-list sentiment-list" style="padding-top:0px">
                                          <li>
                   <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="all" id="senti-all"> 
                    <label  for="senti-all" style="padding-left:30px;font-weight:500;font-size:12px">all</label>
                    </li>
                     <li>
                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="pos" id="senti-pos"> 
                     <label class="text-success" for="senti-pos" style="padding-left:30px;font-weight:500;font-size:12px">positive</label>
                    </li>
                    <li>
                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neg" id="senti-neg"> 
                    <label class="text-danger" for="senti-neg" style="padding-left:30px;font-weight:500;font-size:12px">negative</label>
                    </li>
                    <li> 
                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neutral" id="senti-neutral"> 
                    <label class="text-info"   for="senti-neutral" style="padding-left:30px;font-weight:500;font-size:12px">neutral</label>
                    </li>
                                             
                                          </ul>
                                      </div>
                                  </div>
              </div>
          </div>
<!--     <div class="card earning-widget" style="height:550px">
      <div class="card-header">
        <h4 class="card-title m-b-0">Keywords</h4>
     </div>
      <div class="card-body b-t collapse show" style="overflow-y:scroll;height:550px">
        <div class="form-group">
          <div class="input-group">
              <ul class="icheck-list keyword-list" style="padding-top:0px">
              </ul>
          </div>
        </div>
      </div> 
    </div> -->
  
    
  </div>
                    </div>
                  
                 <div id="show-image" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div align="center" class="modal-body large_img">
                                                 
                                              </div>

                                          </div>
                                        
                                      </div>
                                
                   </div>


                <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="postModal">Article Detail</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body post_data">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button style="display: none" type="button" id="btnSeeComment" class="btn btn-info waves-effect text-left" data-dismiss="modal">See Comments</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
               
         
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
   <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
     <script src="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
     <script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
          <script>
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myHeader");
  var myHeaderContent = document.getElementById("myHeaderContent");
  // var chipcompetitor = document.getElementById("chip-competitor");
  var tabHeader = document.getElementById("tabHeader");
  var tabHeader_sticky = tabHeader.offsetTop;

  var sticky = header.offsetTop;
  var tmp_postTotal = 0;
  function myFunction() {//alert("ho");
    if (window.pageYOffset > sticky) {
      header.classList.add("s-topbar");
      header.classList.add("s-topbar-fix");
      myHeaderContent.classList.add("myHeaderContent");
      // chipcompetitor.classList.add("myHeaderScoll");
    } else {
      header.classList.remove("s-topbar");
      header.classList.remove("s-topbar-fix");
      myHeaderContent.classList.remove("myHeaderContent");
      // chipcompetitor.classList.remove("myHeaderScoll");
    }

    //  if (window.pageYOffset > tabHeader_sticky) {//alert("hi");
    //   tabHeader.classList.add("tabcontrolHeader");
    
    // } else {
    //     tabHeader.classList.remove("tabcontrolHeader");
    // }
  }
  </script>
   
      <script type="text/javascript">
  var startDate;
  var endDate;
  var label;
  /*global mention*/
  var mention_total;
  var mentionLabel = [];
  var mentions = [];
  /*Bookmark Array*/
  var bookmark_array=[];
  var bookmark_remove_array=[];
  /*global sentiment*/
  var positive = [];
  var negative = [];
  var sentimentLabel = [];
  var positive_total=0;
  var negative_total=0;
 var  colors=["#1e88e5","#dc3545","#28a745","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


  /*var effectIndex = 2;
  var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

  $(document).ready(function() {

    // var ref_this = $(".tag_filter .active").text();
    // alert(ref_this);

  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });


 var tmp_mentionTotal=0;var tmp_pageTotal=0;

      //initialize
   $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
  /* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

   $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

        var GetURLParameter = function GetURLParameter(sParam) {
          // alert(sParam);
      var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : sParameterName[1];
          }
      }
  };

  // alert(GetURLParameter('period'));

      startDate = moment().subtract(6, 'month');
      endDate = moment();
      label = 'month';

             $('.singledate').daterangepicker({
              singleDatePicker: true,
              showDropdowns: true,
              locale: {
                  format: 'DD/MM/YYYY'
              }
          },function(date) {
            endDate=date;
            var id =$('input[type=radio][name=period-radio]:checked').attr('id');
            if(id==="period-week")
           {
             startDate = moment(endDate).subtract(1, 'week');
             endDate = endDate;
           }
           else
           {
             startDate = moment(endDate).subtract(1, 'month');
             endDate = endDate;
           }
           // alert(startDate);
           // alert(endDate);
           var admin_page=$('#hidden-chip').val();
                 

           ChooseDate(startDate,endDate,admin_page,'','');
         
        });



   function hidden_div()
  {//alert("popular");
      var brand_id = GetURLParameter('pid');
    // var brand_id = 22;
      $( "#popular-spin" ).show();
      $("#popular").empty();
    $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('gethiddendiv')}}", // This is the URL to the API
        data: { view_name:'Dashboard'}
      })
      .done(function( data ) {//$("#popular").html('');
       for(var i in data) {
        $("#"+data[i].div_name).hide();
       }

      })
      .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
        // If there is no communication between the server, show an error
       // alert( "error occured" );
      });

  }

  function getTopPost()
  {//alert("popular");
      var brand_id = GetURLParameter('pid');
      var fday=GetStartDate();
      var sday=GetEndDate();
      
      $(".top_post_div").empty();
      $(".top_post_div_right").empty();
      $("#top-post-spin").show();
      
    $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('getTopAndLatestPost')}}", // This is the URL to the API
        data: { fday: fday,
                sday:sday,
                brand_id:brand_id,
                admin_page:$('#hidden-chip').val(),
                limit:'6',
                format_type:'top_post'
              }
      })
      .done(function( data ) {
      // var data = Array.from(Object.keys(data), k=>data[k]);
        //console.log(data);
      //  data = data.reverse();
       $("#top-post-spin").hide();
      
      for (var i in data) {
         var html="";
        if(data[i]['visitor']==1)
        {
          var visitor_text= '<p class="text-danger">This is visitor post!</p>';
        }
        {
          var visitor_text ='';
        }
        
        html +='  <div class="postPanel_container cf  postPanel_withPicture "><div class="postPanel_contentContainer"> ' +
                '<div id="idb4b" style="display:none"></div> '+
                '<div class="postPanel_header"> '+
                '<div class="postPanel_userImageContainer" id="idb4c">'+
                '<img src="https://graph.facebook.com/'+data[i]['page_id']+'/picture?type=large">'+
                '</div><a target="_blank" class="postPanel_linkToPage" href="http://www.facebook.com/'+data[i]['page_id']+'">'+data[i]['page_name']+'</a>'+
                '<span> shared a </span>'+
                '<a target="_blank" href="http://www.facebook.com/'+data[i]['id']+'"> post </a>'+
                '<span></span><span> - </span><a target="_blank" class="postPanel_date" href="http://www.facebook.com/'+data[i]['id']+'">'+data[i]['created_time']+'</a>'+
                '</div>'+
                '<div class="postPanel_textContainer">'+visitor_text+'<p>'+data[i]['message']+'</p></div>';
                if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img id="img_'+data[i]['id']+'" class="top_popup_img postPanel_previewImage" style="width:50%;height:150px" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
         html += '</div>' ;
         html += '<div class="postPanel_dataContainer"style="background-color:#fff" id="idb4e">'+
                   '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Total reactions"></span><span>Total reactions: '+numberWithCommas(data[i]['total_react'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb5d"><span class="postPanel_icon mdi mdi-comment-outline" title="Comments"></span><span>'+numberWithCommas(data[i]['comment_count'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb5e"><span class="postPanel_icon mdi mdi-share-variant" title="Shares"></span><span>'+numberWithCommas(data[i]['shared'])+'</span></span>'+
                 
                  '</div>';
          html += '<div class="postPanel_dataContainer" id="idb4e">'+
               
                  '<span class="postPanel_actionIconContainer" id="idb52"><span class="postPanel_icon mdi mdi-thumb-up-outline" title="Likes"></span><span>'+numberWithCommas(data[i]['liked'])+'</span></span>'+
                  '<span id="idb53" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb54"><span class="postPanel_icon  mdi mdi-heart-outline" title="Love"></span><span>'+numberWithCommas(data[i]['loved'])+'</span></span>'+
                  '<span id="idb55" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb56"><span class="postPanel_icon icon-post-haha" title="Haha" >😆</span><span>'+numberWithCommas(data[i]['haha'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb57"><span class="postPanel_icon icon-post-wow" title="Wow">😮</span><span>'+numberWithCommas(data[i]['wow'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb58"><span class="postPanel_icon icon-post-sad" title="Sad">😪</span><span>'+numberWithCommas(data[i]['sad'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Angry">😡</span><span>'+numberWithCommas(data[i]['angry'])+'</span></span>'+
                                
                  '</div>';
          html += ' </div>';
                 
       if(i % 2 == 0)
         $(".top_post_div").append(html);
       else
         $(".top_post_div_right").append(html);
      
       
         
       }
      
      
       
//        Element.prototype.appendAfter = function (element) {
//   element.parentNode.insertBefore(this, element.nextSibling);
// },false;
     

      })
      .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
        
      });

  }
  function getTopComment()
  {//alert("popular");
      var brand_id = GetURLParameter('pid');
      var fday=GetStartDate();
      var sday=GetEndDate();
      
      $(".top_comment_div").empty();
      $(".top_comment_div_right").empty();
     // $("#top-comment-spin").show();
      
    $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('getTopComment')}}", // This is the URL to the API
        data: { fday: fday,
                sday:sday,
                brand_id:brand_id,
                admin_page:$('#hidden-chip').val(),
                limit:'6',
                
              }
      })
      .done(function( data ) {
      // var data = Array.from(Object.keys(data), k=>data[k]);
      //  console.log(data);
      //  data = data.reverse();
      // $("#top-comment-spin").hide();
    
      for (var i in data) {

           var html="";var ProfileName='';var profilePhoto='';var profileLink='';
             if(data[i]['profileName'] !== '' && data[i]['profileName'] !== null) 
              ProfileName=data[i]['profileName']; 
             else if(data[i]['profileID'] !== '' && data[i]['profileName'] !== null)  
              ProfileName=data[i]['profileID'];
             else  ProfileName=data[i]['page_name'];
             
              if(data[i]['profileType'] === 'number')
              {
                profilePhoto='https://graph.facebook.com/'+data[i]['profileID']+'/picture?type=large';
                ProfileName=data[i]['page_name'];
              }
              
              if(data[i]['profileID']!=='undefined'  && data[i]['profileName'] !== null)
              profileLink='https://www.facebook.com/'+data[i]['profileID'];
               if(profilePhoto=='')
               {
                profilePhoto ='assets/images/unknown_photo.jpg';
               }
              
        html +='  <div class="postPanel_container cf  postPanel_withPicture "><div class="postPanel_contentContainer"> ' +
                '<div id="idb4b" style="display:none"></div> '+
                '<div class="postPanel_header"> '+
                '<div class="postPanel_userImageContainer" id="idb4c">'+
                '<img src="'+profilePhoto+'">'+
                '</div><a target="_blank" class="postPanel_linkToPage" href="'+profileLink+'">'+ProfileName+'</a>'+
                '<span></span><span> - </span><a target="_blank" class="postPanel_date" href="http://www.facebook.com/'+data[i]['id']+'">'+data[i]['created_time']+'</a>'+
                '</div>'+
                '<div class="postPanel_textContainer"><p>'+data[i]['message']+'</p></div>';
                if(data[i]['cmtType'] =='sticker' || data[i]['cmtType'] =='photo' || data[i]['cmtType'] =='share' || data[i]['cmtType'] =='share_large_image' || data[i]['cmtType'] =='animated_image_share'  )
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img id="img_'+data[i]['id']+'" class="top_cmt_popup_img postPanel_previewImage" style="width:20%;height:50%" src="'+data[i]['cmtLink']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['cmtType'] =='video_inline' || data[i]['cmtType'] =='animated_image_video' || data[i]['cmtType'] =='video_share_youtube' || data[i]['cmtType'] =='video_share_highlighted')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['cmtLink']+'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
         html += '</div>' ;
         html += '<div class="postPanel_dataContainer"style="background-color:#fff" id="idb4e">'+
                   '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Total reactions"></span><span>Total reactions: '+numberWithCommas(data[i]['total'])+'</span></span>'+
                   '<span class="postPanel_actionIconContainer" id="idb5d"><span class="postPanel_icon mdi mdi-comment-outline" title="Comments"></span><span>0</span></span>'+
                                 
                  '</div>';
          html += '<div class="postPanel_dataContainer" id="idb4e">'+
               
                  '<span class="postPanel_actionIconContainer" id="idb52"><span class="postPanel_icon mdi mdi-thumb-up-outline" title="Likes"></span><span>'+numberWithCommas(data[i]['liked'])+'</span></span>'+
                  '<span id="idb53" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb54"><span class="postPanel_icon  mdi mdi-heart-outline" title="Love"></span><span>'+numberWithCommas(data[i]['loved'])+'</span></span>'+
                  '<span id="idb55" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb56"><span class="postPanel_icon icon-post-haha" title="Haha" >😆</span><span>'+numberWithCommas(data[i]['haha'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb57"><span class="postPanel_icon icon-post-wow" title="Wow">😮</span><span>'+numberWithCommas(data[i]['wow'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb58"><span class="postPanel_icon icon-post-sad" title="Sad">😪</span><span>'+numberWithCommas(data[i]['sad'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Angry">😡</span><span>'+numberWithCommas(data[i]['angry'])+'</span></span>'+
                                
                  '</div>';
          html += ' </div>';
                 
       if(i % 2 == 0)
        $(".top_comment_div").append(html);
       else
        $(".top_comment_div_right").append(html);
       
       
         
       }
  

      })
      .fail(function(xhr, textStatus, error) {

      });

  }
  function highlighted(fday,sday)
{
        let main = document.getElementById("hightlighted");
        let existInstance = echarts.getInstanceByDom(main);
            if (existInstance) {
                if (true) {
                    echarts.init(main).dispose();
                }
            }

        var chart = echarts.init(main);
        var brand_id = GetURLParameter('pid');
        //var brand_id = 22;
       $( "#hightlighted-spin" ).show();

       //$("#highlighted").empty();
      $.ajax({
          type: "GET",
          dataType:'json',
          contentType: "application/json",
          url: "{{route('getInHighlighted')}}", // This is the URL to the API
          data: { fday: fday,sday:sday,type:"all",brand_id:brand_id,admin_page:$('#hidden-chip').val()}
        })
        .done(function( data ) {
            
    $( "#hightlighted-spin" ).hide();
         
          if(data.length>0)
          {
         //var data =JSON.parse(data);
         var data = genData(data);
         console.log(data);
        
         var cmtObj=data.commentID;
        
        
         chart.clear();
         option=null;

           var option = {
            textStyle: {
                        
                        fontFamily: 'mm3',},
                    tooltip: { 
                         textStyle: {
                        
                                fontFamily: 'mm3',}
                            
                        },
                    series: [ {
                        type: 'wordCloud',
                           left:'center',
                            top:'center',
                            width: '70%',
                            height: '80%',
                            right:null,
                            bottom:null,
                        sizeRange: [12, 50],
                        rotationRange: [0,0,0,0],
                        // rotationStep: 90,
                        shape: 'ratangle',//pentagon ratangle
                        gridSize:8,
                        // height: 500,
                        drawOutOfBound: false,
                        textStyle: {
                            normal: {
                                fontFamily: 'mm3',
                                color: function () {
                                    var lightcolor = 'rgb(' + [
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        2
                                    ].join(',') + ')';
                                    console.log(lightcolor);
                                    return lightcolor;
                                   /* return colors[0];*/

                                }
                            },
                           
                            emphasis: {
                                shadowBlur: 10,
                                shadowColor: '#333'
                            }
                        },
                        data: data.seriesData
                    }
                    ]
                        
                };
        function genData(data) {
        var seriesData = [];
      
        var comment_array={};
        var i = 0;
         for(var key in data) 
            {
             var name =data[key].topic_name;
             var value=data[key].weight;
             var arr_cmt=data[key].comment_id;
             var kvalue=parseFloat(value);
           
              comment_array[name]=arr_cmt;
              seriesData.push({name: name,value:value});
            
              
            }


      /* jsonString=jsonString.split(["[","]"])*/
         return {
            
            seriesData: seriesData,
            commentID:comment_array,
           
            
        };

        
    };





              chart.setOption(option,true), $(function() {
        function resize() {
            setTimeout(function() {
                chart.resize()
            }, 100)
        }
    }
    );
        chart.on('click', function (params) {//alert("hhi");
    //    console.log(params);
    //    console.log(params.name); 
    //    console.log(params.seriesName); 
    //    console.log(params.value);//count
    var cmt_arr=cmtObj[params.name];
        console.log(cmt_arr);
            // console.log('cmt_arr');
              $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("session-cmtID") }}',
             type: 'GET',
             data: {highlight_text:params.name,wordcloud_text:cmt_arr},
             success: function(response) {// alert(response)
                window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid')+"&highlight_text="+ params.name+"&source="+GetURLParameter('source')+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&wordcloud_text="+params.name+"&admin_page="+$('#hidden-chip').val(), '_blank'); 
            }
                });

       
       
    });

          }
          


        })
        .fail(function(xhr, textStatus, error) {
           console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
          // If there is no communication between the server, show an error
         // alert( "error occured" );
        });

}
function Calculate_DatePreset(label)
{
    var date_preset='this_month';
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
          if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

        return date_preset;
}

     function ChooseDate(start, end,label='',filter_tag='') {//alert(start);
     tmp_mentionTotal = 0;tmp_pageTotal=0;
      // alert('hyy');
      // alert(GetURLParameter(period));
            $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate=start;
            endDate=end;
             var graph_option = $('.btn-group1 > .btn.active').text();
             // alert(graph_option);
          
           var date_preset = Calculate_DatePreset(label);
           // getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag);
           getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           getAllMentionPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           // getAllMentionComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           // posting_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
            getAllArticle(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           // MostFrequentlyTag(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          
           // highlighted(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           getKeyword();
           // tagCount();
           // getTopPost();
           // getTopComment();
          // if (graph_option.trim()== 'Sentiment') {
          //   InboundSentimentDetail(GetStartDate(),GetEndDate());
          //  }
          // else if (graph_option.trim() == 'Mention')
          // {
          //   // dd("HII");
          //     requestmentionData(GetStartDate(),GetEndDate());
          // }
      }

          function GetStartDate()
  {
     //alert(startDate);
   return startDate.format('YYYY MM DD');
  }
  function GetEndDate()
  {
             // alert(endDate);
  return endDate.format('YYYY MM DD');
  }
    function getAllArticle(fday,sday)
      {
        // alert('hhhh');
          $(".popup_post").unbind('click');
          var filter_senti_arr = [];
          $.each($("input[name='sentimentlist']:checked"), function(){            
              filter_senti_arr.push($(this).val());
            });
          var filter_keyword_arr = [];
          $.each($("input[name='keyword']:checked"), function(){            
              filter_keyword_arr.push($(this).val());
            });
          // alert(filter_senti_arr);
          var campaign_name = GetURLParameter('campaign_name');
          var campaign_id = GetURLParameter('campaign_id');
          var oTable = $('#tbl_article').DataTable({
          // "responsive": true,
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": true,
          "serverSide": true,
          "destroy": true,
          "order": [],
          "autoWidth": false,
          "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "ajax": {
          "url": '{{ route('getAllArticle_forCampaign') }}',
          "dataSrc": function(res){
              var count = res.data.length;
              document.getElementById('title_article').innerHTML = 'Articles ('+res.recordsTotal+")";
              tmp_mentionTotal = tmp_mentionTotal + res.recordsTotal; 
              document.getElementById('title_mention').innerHTML = 'Mentions('+tmp_mentionTotal+')';
              return res.data;
            },
            "data": {
            "fday": fday,
            "sday": sday,
            "brand_id": GetURLParameter('pid'),
            "tsearch_senti":filter_senti_arr.join("| "),
            "tsearch_keyword":filter_keyword_arr.join("| "),
            "campaign_name":campaign_name,
            "campaign_id":campaign_id,
            "period":GetURLParameter('period'),
            "sentiType":GetURLParameter('SentiType'),
          
          }

        },
        "initComplete": function( settings, json ) {
        $('#tbl_article thead tr').removeAttr('class');
        // $("#tbl_article thead").remove();
        },
        drawCallback: function() {
         $('.select2').select2();
      },
        columns: [
        {data: 'post', name: 'post',"orderable": false},
        ]
        
      }).on('click', '.popup_post', function (e) {
          if (e.handled !== true) {
            var name = $(this).attr('name');
            var post_id = $(this).attr('id');
            $("#modal-spin").show();
            $(".post_data").empty();

            //alert(post_id);alert(name);alert(GetURLParameter('pid'));
             $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("getRelatedArticle") }}',
             type: 'GET',
             data: {id:post_id,brand_id:GetURLParameter('pid'),keyword_group: $('#hidden-chip').val()},
             success: function(response) { console.log(response);
              var res_array=JSON.parse(response);
              // alert(res_array);
              var data=res_array[0];
              var keyForHighLight=res_array[1];

              for(var i in data) {//alert(res_array[1]);
               var page_name=data[i].page_name;
               var link = data[i].link;
               var message = highLightText(keyForHighLight,data[i].message);
               // alert(message);
               var page_Link= "https://www.facebook.com/" + page_name ; 
               var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                        '<div class="form-material sl-left" style="padding-left:20px;">'+ 
                         '<a href="'+link+'" style="font-size:18px;font-weight:800px;" target="_blank" >' +
                         data[i].title +
                        '</a><div>' + ' <div class="sl-right"> '+
                        '<div style ="width:50%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i>' +data[i].created_time + ' by ' + '<a href="'+page_Link+'" target="_blank">' +data[i].page_name+'</a></span></div> '+
                        ' <div class="sl-right"> '+
                        ' <div style="width:55%;display: inline-block">';
                    html+=' </div></div></div> ' +                
                          '   <p class="m-t-10" align="justify">' + 
                          message +
                        
                          '</p>';
                    html +='</div></div>';
                $(".post_data").append(html);
        }
         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
      });
          e.handled = true;
    }
  }).on('click', '.btn-campaign', function (e) {
              if (e.handled !== true) {
                var attr_id = $(this).attr('id');
                var post_id  = attr_id.replace("campaign_","");
                var btn_val = $("#"+attr_id).attr('aria-pressed');
                
                if(btn_val == "false") // previous false next true
                {
                  var answer = confirm("Are you sure  to add campaign ?")
                  if (answer) {
                     $.ajax({
                       headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                       url:'{{ route("SetCampaign") }}',
                       type: 'POST',
                       data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_val:1},
                       success: function(response) { //alert(response)
                        if(response>=0)
                        {
                         var x = document.getElementById("snackbar")
                          $("#snack_desc").html('');
                          $("#snack_desc").html('Campaign added successfully!!');
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                        }
                      }
                    });
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline active");
                    // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                     // $("#"+attr_id).attr("aria-pressed", "false");
                     // $("#"+attr_id).removeAttr("class");
                     // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                    
                  }
              }
              else
              {
                var answer = confirm("Are you sure to remove campaign?")
                if (answer) {
                 $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("SetCampaign") }}',
                   type: 'POST',
                   data: {id:post_id,brand_id:GetURLParameter('pid')},
                   success: function(response) { //alert(response)
                    if(response>=0)
                    {
                      var x = document.getElementById("snackbar")
                        $("#snack_desc").html('');
                        $("#snack_desc").html('Campaign remove successfully!!');
                        x.className = "show";

                       setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       $("#"+post_id).closest('tr').remove();
                    }
                  }
                });
              }
              else
              {
                 $("#"+attr_id).attr("aria-pressed", "true");
                 $("#"+attr_id).removeAttr("class");
                 $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline  active focus");
              }      
            }
          }
            e.handled = true;
        }).on('change', '.edit_senti', function (e) {
      
              if (e.handled !== true) {
              var  brand_id = GetURLParameter('pid');
              var senti_id = $(this).attr('id');
              var post_id  = senti_id.replace("sentiment_","");
              var sentiment = $('#sentiment_'+post_id+' option:selected').val();
              var emotion = $('#emotion_'+post_id+' option:selected').val();
              

              $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
              if(sentiment == 'pos')
               $("#sentiment_"+post_id).addClass('text-success');
              else if (sentiment == 'neg')
                $("#sentiment_"+post_id).addClass('text-red');
              else
                $("#sentiment_"+post_id).addClass('text-warning');

              
                   $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("setArticlePredict") }}',
                   type: 'POST',
                   data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment},
                   success: function(response) {
                    if(response>=0){
                      var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Sentiment Changed!!');
                      x.className = "show";
                      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                     }
                  }
                });
                    e.handled = true;
                 }
         
         

         
           // else
           //  {
           //    var x = document.getElementById("snackbar")
           //                          $("#snack_desc").html('');
           //                          $("#snack_desc").html('You are not allowed to edit this article . Changes will not be saved !!! !!');
           //                            x.className = "show";

           //                            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
           //                            return false;
           //  }
          

        
            
      }).on('click', '.post_hide', function (e) {
          if (e.handled !== true) {
            var attr_id = $(this).attr('id');
            var post_id  = attr_id.replace("hide_","");
            var answer = confirm("Are you sure to hide this post?")
            if (answer) {
                 $.ajax({
                     headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                     url:'{{ route("SetHideMention") }}',
                     type: 'POST',
                     data: {id:post_id,brand_id:GetURLParameter('pid'),table_type:'web_mention'},
                     success: function(response) {// alert(response)
                      if(response>=0)
                      {
                        $("#"+post_id).closest('tr').remove();
                      }
                    }
                  });
               }
              else {
                  
              }
         }
         e.handled = true;
          });

        new $.fn.dataTable.FixedHeader( oTable );

         // oTable.columns.adjust().draw();
         // $('#tbl_post').css('width', '100%');
         // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
  }
  function getAllPost(fday,sday)
      {
        // alert(fday);
        // alert(GetURLParameter(period));
        // alert(filter_keyword_arr.join("| "));
        $(".popup_post").unbind('click');
        $(".see_comment").unbind('click');

     
      // var admin_page=$('#hidden-chip').val();
       var campaign_name = GetURLParameter('campaign_name');
       var campaign_id = GetURLParameter('campaign_id');

       // alert(campaign_name);
         // var split_name = admin_page.split("-");
         //    if(split_name.length > 0)
         //    {
         //      if(isNaN(split_name[split_name.length-1]) == false )
         //        admin_page = split_name[split_name.length-1];
         //    }
       var filter_senti_arr = [];
          $.each($("input[name='sentimentlist']:checked"), function(){            
              filter_senti_arr.push($(this).val());
            });

       var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).val());
              });
// alert(filter_senti_arr);
      // document.getElementById('select_page').innerHTML =admin_page;
         // GetURLParameter('period')
   var oTable = $('#tbl_post').DataTable({
    
         // "responsive": true,
          "paging": true,
           "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": false,
          "destroy": true,
          "ordering": true   ,
          "autoWidth": false,
          // "columnDefs" : [
          //      { "width": "60%", "targets": 0 },
          //      { "width": "10%", "targets": 1 },
          //      { "width": "10%", "targets": 2 },
          //      { "width": "10%", "targets": 3 },
          //      { "width": "10%", "targets": 4 }
          // ] ,
          "order": [],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          
          "ajax": {
            "url": '{{ route('getCampaignPost') }}',
            "dataSrc": function(res){
              document.getElementById('title_post').innerHTML = 'Posts ('+res.recordsTotal+")";
              tmp_pageTotal = tmp_pageTotal + res.recordsTotal;
              document.getElementById('title_page').innerHTML = 'Pages ('+tmp_pageTotal +")";
              return res.data;
            },
 
             "data": {
              "fday": fday,
              "sday": sday,
              "campaign_name":campaign_name,
              "period" : GetURLParameter('period'),
              "other_page":'1',
              "brand_id": GetURLParameter('pid'),
              "campaign_id":campaign_id,
              // "type" :"mention post",
              "sentiType": GetURLParameter('SentiType'),
              "tsearch_senti":filter_senti_arr.join("| "),
              "tsearch_keyword":filter_keyword_arr.join("| "),
            }

          },
          "initComplete": function( settings, json ) {
          $('.tbl_post thead tr').removeAttr('class');
          },
   

          columns: [
          {data: 'post', name: 'post',"orderable": false},
          {data: 'reaction', name: 'reaction', orderable: true,className: "text-center"},
          {data: 'share', name: 'share', orderable: true, className: "text-center"},
        //   {data: 'overall', name: 'overall', orderable: true,className: "text-center"},


          ]
          
        }).on('click', '.popup_post', function (e) {
    if (e.handled !== true) {
      var name = $(this).attr('name');
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_name:GetURLParameter('campaign_name')},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];
            var keyForHighLight = res_array[4];
               for(var i in data) {
                 var message = highLightText(keyForHighLight,data[i].message);

                 var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
                 var page_name=data[i].page_name;

                // if(isNaN(page_name)==false) //it is number
                // {
                 
                //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
                //   page_name = b[0];
                 
                   
                // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
                
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large'; 
        
         page_Link= 'https://www.facebook.com/' + page_name ;   
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      message +
                    
                      '</p> ';
                   if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }
          
           
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.see_comment', function (e) {//alert("hihi");
    if (e.handled !== true) {
      var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("seeComment_","");
     // alert(post_id);
      var campaign_name=$('#hidden-chip').val();
     
      //   var split_name = admin_page.split("-");
      //       if(split_name.length > 0)
      //       {
      //         if(isNaN(split_name[split_name.length-1]) == false )
      //           admin_page = split_name[split_name.length-1];
      //       }
          //  alert(admin_page);

      window.open("{{ url('related_campaigncomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&campaign_name="+campaign_name, '_blank');
       
       
          }
          


            e.handled = true;
         }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
      var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
      // alert(btn_val);
      if(btn_val == "false") // previous false next true
      {
        var answer = confirm("Are you sure  to add campaign ?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_val:1},
         success: function(response) { //alert(response)
          if(response>=0)
          {
           var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign added successfully!!');
                     x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {
                   $("#"+attr_id).attr("aria-pressed", "false");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline active");
                  // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   // $("#"+attr_id).attr("aria-pressed", "false");
                   // $("#"+attr_id).removeAttr("class");
                   // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  
                }
              
      }
      else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
             $("#"+post_id).closest('tr').remove();
          }
        }
            });
                }
                else
                {
                   $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline  active focus");
                }
               
      }
      
       
       
          }
          
           

            e.handled = true;
         });
      new $.fn.dataTable.FixedHeader( oTable );
     // oTable.columns.adjust().draw();
     // $('#tbl_post').css('width', '100%');
     // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
          
    }

      $(window).resize( function() {

       //  new $.fn.dataTable.FixedHeader( oTable );
        $('#tbl_post').css('width', '100%');
        
    });

      function tagsentimentData(fday,sday){//alert(fday);
    let main = document.getElementById("tag-senti-chart");
    let existInstance = echarts.getInstanceByDom(main);
        if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }


var sentiChart = echarts.init(main);


$("#tag-senti-spin").show();
        
    // var brand_id = GetURLParameter('pid');
    var brand_id = GetURLParameter('pid');
       /* var brand_id = 22;*/
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getCampaignTagSentiment')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,campaign_name:$('#hidden-chip').val() }
    })
    .done(function( data ) {//alert(data);
         // console.log("tag senti");
    //console.log(data);//mention
    var xAxisData = [];
    var data1 = [];
    var data2 = [];
   var tag_arr= genData(data);
   var tag_Obj=tag_arr.tag_id;

for(var i in data) 
        {
    xAxisData.push(data[i].tagLabel);
    data1.push(Math.round(data[i].positive));
    if(data[i].negative >0 )
    {
    data2.push(Math.round(-data[i].negative));
    }
    else
    {
     data2.push(0);
    }
  
 
}


var itemStyle = {
    normal: {
        fontFamily: 'mm3',
    },
    emphasis: {
        barBorderWidth: 1,
        shadowBlur: 10,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        shadowColor: 'rgba(0,0,0,0.5)'
    }
};


option = {
      color:colors,
      textStyle: {
                    
                    fontFamily: 'mm3',},
    backgroundColor: 'rgba(0, 0, 0, 0)',
     dataZoom:[  {
            type: 'slider',
            show: true,
            xAxisIndex: [0],
            start: 70,
            end: 100
        },
         {
            type: 'inside',
            xAxisIndex: [0],
            start: 1,
            end: 35
        }
    ],
        calculable : true,
    legend: {
        
        data: ['positive', 'negative'],
           x: 'center',
             y: 'top',
              padding :0,
    }/*,
    brush: {
        toolbox: ['rect', 'polygon', 'lineX', 'lineY', 'keep', 'clear'],
        xAxisIndex: 0
    }*/,

    toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['stack','tiled']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },
    tooltip: {   textStyle: {
                    
                    fontFamily: 'mm3',}
                },
    calculable : true,
    dataZoom : {
        show : true,
        realtime : true,
        start : 0,
        end : 100
    },
    xAxis: {
        data: xAxisData,
        axisLabel:{
      rotate:45
    },
        name: 'Tags',
        silent: false,
        axisLine: {onZero: true},
        splitLine: {show: false},
        splitArea: {show: false},
       /*  axisLabel: {
            textStyle: {
                color: '#fff'
            }
        },*/
    },
    yAxis: {
        inverse: false,
        splitArea: {show: false}
    },
   grid: {
            top: '12%',
            left: '3%',
            right: '5%',
            containLabel: true
        },
  /*  visualMap: {
        type: 'continuous',
        dimension: 1,
        text: ['High', 'Low'],
        inverse: true,
        itemHeight: 200,
        calculable: true,
        min: -2,
        max: 6,
        top: 60,
        left: 10,
        inRange: {
            colorLightness: [0.4, 0.8]
        },
        outOfRange: {
            color: '#bbb'
        },
        controller: {
            inRange: {
                color: '#2f4554'
            }
        }
    },*/
    series: [
        {
            name: 'positive',
            type: 'bar',
            stack: 'one',
            color:colors[2],
            barMaxWidth:30,
            itemStyle: itemStyle,
            data: data1
        },
        {
            name: 'negative',
            type: 'bar',
            stack: 'one',
            color:colors[1],
            barMaxWidth:30,
            itemStyle: itemStyle,
            data: data2
        }
    ]
};
function genData(data) {
    var tag_array={};
    var i = 0;
     for(var key in data) 
        {
         var name =data[key].tagLabel;
         var id=data[key].tagId;
               
         tag_array[name]=id;
              
        }

     return {
             tag_id:tag_array,
       
        
    };

    
};

$("#tag-senti-spin").hide();
/*categoryChart.on('brushSelected', renderBrushed);

function renderBrushed(params) {
    var brushed = [];
    var brushComponent = params.batch[0];

    for (var sIdx = 0; sIdx < brushComponent.selected.length; sIdx++) {
        var rawIndices = brushComponent.selected[sIdx].dataIndex;
        brushed.push('[Series ' + sIdx + '] ' + rawIndices.join(', '));
    }
}
*/
    sentiChart.setOption({
        title: {
            backgroundColor: '#333',
          /*  text: 'SELECTED DATA INDICES: \n' + brushed.join('\n'),*/
            bottom: 0,
            right: 0,
            width: 100,
            textStyle: {
                fontSize: 12,
                color: '#fff'
            }
        }
    });

    sentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            sentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    // alert($('.hidden-pg').val());
   sentiChart.on('click', function (params) {
   // console.log("tags");
   // console.log(params);
   // console.log(params.name); // xaxis data = tag name
   // console.log(params.seriesName); //bar type name ="positive"
   // console.log(params.value);//count 8
   // var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var CmtType ='';
   if(params.seriesName == 'positive')
    CmtType = 'pos'
    else
    CmtType = 'neg'

     var campaign_name = $('#hidden-chip').val();
     // var default_page = $('.hidden-pg').val();
     // alert('default_page');

   // window.open("{{ url('relatedcomment?')}}" +"pid="+ pid +"&source="+ source+"&fday="+ GetStartDate()+
   // "&sday="+GetEndDate()+"&type="+ params.seriesName +"&tag="+ params.name +"&from_graph=sentiment&admin_page="+admin_page  , '_blank');
   window.open("{{ url('related_campaigncomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+tag_Obj[params.name]+"&CmtType="+CmtType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&campaign_name="+campaign_name, '_blank');
});


   

  
    })
     .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

  }
      

    

      $("#btnSeeComment").click( function()
             {
               var post_id=$("#popup_id").val();
               // var admin_page=$('#hidden-chip').val();

               // var split_name = admin_page.split("-");
               //    if(split_name.length > 0)
               //    {
               //      if(isNaN(split_name[split_name.length-1]) == false )
               //        admin_page = split_name[split_name.length-1];
               //    }
           // alert(admin_page);
                // window.open("{{ url('relatedcompetitorcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
                 // window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+admin_page , '_blank');
                   window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post", '_blank');
             }
          );

   function getAllComment(fday, sday,filter_tag='')
      {
        // alert(GetURLParameter('SentiType'));
        $(".popup_post").unbind('click');
        $(".edit_predict").unbind('click');
        $("#add_tag").unbind('click');
   //var admin_page=$('#admin_page_filter option:selected').val();
   //alert($('#hidden-chip').val());
        var filter_tag_arr = [];
              $.each($("input[name='taglist']:checked"), function(){            
                  filter_tag_arr.push($(this).val());
                });
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });
         var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).attr('id').replace('K_',''));
              });
   var campaign_name = GetURLParameter('campaign_name');
// alert(campaign_name);
        // var split_name = admin_page.split("-");
        //     if(split_name.length > 0)
        //     {
        //       if(isNaN(split_name[split_name.length-1]) == false )
        //         admin_page = split_name[split_name.length-1];
        //     }
       //  alert(filter_tag_arr.join("| "));

   var oTable = $('#tbl_comment').DataTable({

          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": true,
          "destroy": true,
          "ordering": false   ,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
         
          "ajax": {
            "url": '{{ route('getcampaigncomment') }}',
            "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              // document.getElementById('title_comment').innerHTML = 'Comments ('+res.recordsTotal+")";
              // tmp_pageTotal = tmp_pageTotal + res.recordsTotal;
              // document.getElementById('title_page').innerHTML = 'Pages ('+tmp_pageTotal+")";

              return res.data;
            },
     /*       data: function ( d ) {
              d.fday = mailingListName;
              d.sday = mailingListName;
              d.brand_id = mailingListName;
            }*/
             "data": {
              "fday": fday,
              "sday": sday,
              "campaign_name":campaign_name,
              "sentiType":GetURLParameter('SentiType'),
              "type" :"mention comment",
              "brand_id": GetURLParameter('pid'),
              "period": GetURLParameter('period'),
              "tsearch_senti":filter_senti_arr.join("| "),
              "tsearch_keyword":filter_keyword_arr.join("| "),
              "tsearch_tag":filter_tag_arr.join("| "),
              "is_competitor":"true",
            }

          },
          "initComplete": function( settings, json ) {
            //console.log(json);
         
          },
       drawCallback: function() {
       $('.select2').select2();
    },

          columns: [
          {data: 'post_div', name: 'post_div',"orderable": false},
      


          ]
          
        }).on('click', '.popup_post', function (e) {
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_name:GetURLParameter('campaign_name')},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];
            var keyForHighLight = res_array[4];

            

              for(var i in data) {//alert(data[i].message);
                var message = highLightText(keyForHighLight,data[i].message);
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
        
        var page_name=data[i].page_name;
        // if(isNaN(page_name)==false)
        // {
          
        //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
        //  page_name = b[0];
        
        // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
        
        var page_Link= 'https://www.facebook.com/' + page_name ;  
        var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large';                         
  //data[i].full_picture
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      message +
                    
                      '</p>';
                   if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }


           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.edit_predict', function (e) {
    if (e.handled !== true) {

      var post_id = $(this).attr('id');
  /*    var sentiment = $('input[name=sentiment]').val();
      var emotion = $('input[name=emotion_'+post_id+']').val();*/
      var sentiment = $('#sentiment_'+post_id+' option:selected').val();
      var emotion = $('#emotion_'+post_id+' option:selected').val();
      var tags = $('#tags_'+post_id).val();
    //   alert(sentiment);
    // alert(tags);
    // return;
     
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags},
           success: function(response) {// alert(response)
            if(response>=0)
            {
                  swal({   
              title: "Updated!",   
              text: "Done!",   
              timer: 1000,   
              showConfirmButton: false 
          });
            }
          }
              });

            e.handled = true;
         }
       
      
          
    }).on('change', '.edit_senti', function (e) {
      if (e.handled !== true) {
      var senti_id = $(this).attr('id');
      var post_id  = senti_id.replace("sentiment_","");
      var sentiment = $('#sentiment_'+post_id+' option:selected').val();
      var emotion = $('#emotion_'+post_id+' option:selected').val();
      var tags = $('#tags_'+post_id+' option:selected').map(function () {
          return $(this).text();
      }).get().join(',');

      var tags_id = $('#tags_'+post_id).val();
      
      $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
        if(sentiment == 'pos')
       $("#sentiment_"+post_id).addClass('text-success');
      else if (sentiment == 'neg')
        $("#sentiment_"+post_id).addClass('text-red');
      else
        $("#sentiment_"+post_id).addClass('text-warning');
      // alert(tags);

           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {// alert(response)
            if(response>=0)
            {
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Sentiment Changed!!');
              x.className = "show";
              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
          }
        });
            e.handled = true;
         }
    }).on('change', '.edit_tag', function (e) {
      if (e.handled !== true) {
      var senti_id = $(this).attr('id');
      var post_id  = senti_id.replace("tags_","");
      var sentiment = $('#sentiment_'+post_id+' option:selected').val();
      var emotion = $('#emotion_'+post_id+' option:selected').val();
      var tags = $('#tags_'+post_id+' option:selected').map(function () {
          return $(this).text();
      }).get().join(',');

      var tags_id = $('#tags_'+post_id).val();
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {
            tagCount();
            if(response>=0){
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Tag Changed!!');
              x.className = "show";
              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
          }
        });
          e.handled = true;
       }
    }).on('click', '#add_tag', function (e) {
    if (e.handled !== true) {
           $('#show-add-tag').modal('show'); 
          }
            
            e.handled = true;
          
    }).on('click', '.popup_img', function (e) {
    if (e.handled !== true) {

         var id = $(this).attr('id');
         var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
// img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 
         }
    }); 
    }
                  function requestmentionData(fday,sday)
              {    
                // alert($('#hidden-chip').val());
                  let main = document.getElementById("fan-growth-chart");
                  let existInstance = echarts.getInstanceByDom(main);
                      if (existInstance) {
                          if (true) {
                              echarts.init(main).dispose();
                          }
                        }
                  var campaign_name = $('#hidden-chip').val();
                  var campaign_id = $("*[name='"+campaign_name+"']").attr('id');
                  var mentionDetailChart = echarts.init(main);
                  $("#fan-growth-spin").show();
                  $("#reaction-spin").show();       
                  var brand_id = GetURLParameter('pid');
                   /* var brand_id = 22;*/
                   // alert (brand_id);
                  $.ajax({
                    type: "GET",
                    dataType:'json',
                    contentType: "application/json",
                    url: "{{route('getcampaignmentiondetail')}}", // This is the URL to the API
                    data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'day',campaign_name:campaign_name,campaign_id:campaign_id}
                  })
                  .done(function( data ) {//alert(data);
                     mention_total=0;

                     mentions=[];
                     mentionLabel=[];

                    for(var i in data) {//alert(data[0][i].mention);
                      mentions.push(data[i].mention);
                      mentionLabel.push(data[i].periodLabel);
                    }
                    $.each(mentions,function(){mention_total+=parseInt(this) || 0;});
                    option = {
                        color: colors,
                        tooltip: {
                          trigger: 'axis',
                        },
                        // grid: {
                        //     right: '20%'
                        // },
                      /*  toolbox: {
                            feature: {
                                dataView: {show: true, readOnly: false},
                                restore: {show: true},
                                saveAsImage: {show: true}
                            }
                        },*/
                     toolbox: {
                            show : true,
                            feature : {
                                mark : {show: false},
                                dataView : {show: false, readOnly: false},
                                magicType : {show: true, type: ['line','bar']},
                                restore : {show: true},
                                saveAsImage : {show: true}
                            }
                        },
                      legend: {
                          data:['Mentions'],
                            //           formatter: function (name) {
                            //             if(name === 'Mentions')
                            //             {
                            //                  return name + ': ' + mention_total;
                            //             }
                            //             return name  + ': ' + reactions_total;

   
                            // }
                        },
                        xAxis: [
                            {
                                type: 'category',
                                axisTick: {
                                    alignWithLabel: true
                                },
                                             axisLabel: {
                        formatter: function (value, index) {
                        // Formatted to be month/day; display year only in the first label
                        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        var date = new Date(value);
                        var texts = [date.getFullYear(), monthNames[date.getMonth()],date.getDate()];
                        return texts.join('-');
                    },
                  rotate:45
                },
                  data: mentionLabel
               }
             ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Mentions',
                      /*  min: 0,
                        max: 250,
                        position: 'left',
                        axisLine: {
                            lineStyle: {
                                color: colors[0]
                            }
                        }/*,
                        axisLabel: {
                            formatter: '{value}'
                        }*/
                         /* axisLabel: {
                    formatter: function (e) {
                        return kFormatter(e);
                    }*/
                
                   }
                ],
              series: [
                  {
                      name:'Mentions',
                      type:'line',
                      smooth: 0.3,
                      color:colors[0],
                      barMaxWidth:30,
                      data:mentions
                  }
              ]
          };
          $("#fan-growth-spin").hide();
              mentionDetailChart.setOption(option, true), $(function() {
              // function resize() {
              //     setTimeout(function() {
              //         mentionDetailChart.resize()
              //     }, 100)
              // }
              // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
          });
          mentionDetailChart.on('click', function (params) {
             // console.log(params);
             // console.log(params.name); // xaxis data = 2018-08
             // console.log(params.seriesName); //bar period name ="Positive"
             // console.log(params.value);//count
                // var hidden_kw  = $('#hidden-chip').val();
                // var default_kw = $('.hidden-kw').val();

                window.open("{{ url('relatedcampaignmention?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate() +"&period="+params.name+"&campaign_name="+$('#hidden-chip').val(), '_blank');
             
          });

          //requestmentionReactData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
 
      })
      .fail(function() {
        // If there is no communication between the server, show an error
     //  alert( "error occured" );
      });
    }


    function getAllMentionPost(fday,sday)
    {
     // alert('hey');
      $(".popup_post").unbind('click');
      $(".see_comment").unbind('click');
      $(".post_hide").unbind('click');

      var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
              });
      var filter_keyword_arr = [];
            $.each($("input[name='keyword']:checked"), function(){            
                filter_keyword_arr.push($(this).attr('id').replace('K_',''));
              });
            var campaign_name = GetURLParameter('campaign_name');
            var campaign_id = GetURLParameter('campaign_id');
   // alert(campaign_id);
   // alert(filter_keyword_arr.join("| "));

       var oTable = $('#tbl_mention_post').DataTable({
        
             // "responsive": true,
              "paging": true,
               "pageLength": 10,
              "lengthChange": false,
              "searching": false,
              "processing": true,
              "serverSide": true,
              "destroy": true,
               "order": [],
              // "ordering": true   ,
              // "scrollX":        true,
              // "scrollCollapse": true,
               "autoWidth": false,
              
            //     "columnDefs": [
            //   { "targets": [0] , "orderable": false},       
            //   {"targets": [1],"orderable": true },
            //   {"targets": [2],"orderable": true }
            // ],
             
             // "order": [[ 1, 'asc' ], [ 2, 'asc' ]],
              "headers": {
                'X-CSRF-TOKEN': '{{csrf_token()}}' 
              },
             
              "ajax": {
                "url": '{{ route('getcampaignMention') }}',
                "dataSrc": function(res){
                    var count = res.data.length;
                    // alert(res.recordsTotal);
                    document.getElementById('title_mention_post').innerHTML = 'Posts ('+res.recordsTotal+")";
                
              tmp_mentionTotal = tmp_mentionTotal + res.recordsTotal; 
             document.getElementById('title_mention').innerHTML = 'Mentions('+tmp_mentionTotal+')';
                    // alert(document.getElementById('PostTotal').value = res.recordsTotal);
                
                    return res.data;
                  },
         /*       data: function ( d ) {
                  d.fday = mailingListName;
                  d.sday = mailingListName;
                  d.brand_id = mailingListName;
                }*/
                 "data": {
                  "fday": fday,
                  "sday": sday,
                  "tsearch_senti":filter_senti_arr.join("| "),
                  "tsearch_keyword":filter_keyword_arr.join("| "),
                  "brand_id": GetURLParameter('pid'),
                  "period": GetURLParameter('period'),
                  "sentiType": GetURLParameter('SentiType'),
                  "campaign_name": campaign_name,
                  "campaign_id":campaign_id,
                  // "type":'mention post',
                
                }

              },
              "initComplete": function( settings, json ) {
                // console.log(json);
              $('.tbl_mention_post thead tr').removeAttr('class');
              },
              drawCallback: function() {
               $('.select2').select2();
            },

              columns: [
              {data: 'post', name: 'post',"orderable": false},
              {data: 'reaction', name: 'reaction', orderable: true,className: "text-center"},
              {data: 'share', name: 'share', orderable: true, className: "text-center"},
            


              ]
              
            }).on('click', '.popup_post', function (e) {
              // alert('jje');
        if (e.handled !== true) {
          var name = $(this).attr('name');
          //alert(name);
          var post_id = $(this).attr('id');
         // alert(post_id);
          $("#modal-spin").show();
          $(".post_data").empty();

          //alert(post_id);alert(name);alert(GetURLParameter('pid'));
               $.ajax({
               headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
               url:'{{ route("getRelatedMention") }}',
               type: 'GET',
               data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_name:GetURLParameter('campaign_name')},
               success: function(response) { //console.log(response);
                var res_array=JSON.parse(response);
                var data=res_array[0];
                var keyForHighLight=res_array[2];

                  for(var i in data) {//alert(data[i].message);
                    var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                     var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                     var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                     var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                     var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                     pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                     neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                     neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                     pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                     neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                     neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

                     var page_name=data[i].page_name;
                  
                     var message = highLightText(keyForHighLight,data[i].message);
                     var page_Link= "https://www.facebook.com/" + page_name ;                   

       var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                     '  <div class="sl-left">'+
                     '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                     
                      ' <div class="sl-right">'+
                      '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a> '+
                      ' <div class="sl-right"> '+
                   ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                    ' <div style="width:55%;display: inline-block">';
                    if(neg_pcent!=='')
                    html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                  if(pos_pcent!=='')
                    html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                  if(neutral_pcent!=='')
                    html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                      html+=' </div></div></div></span></p> ' +                
                        '   <p class="m-t-10" align="justify">' + 
                          message +
                        
                          '</p>';
                        if(data[i]['post_type'] =='photo')
                      {
                         html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                        '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                        '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                        '</span><span class="postPanel_nameTextContainer"></span></a>';
                      }
                      else if(data[i]['post_type'] =='video')
                      {
                         html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                        '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                        '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                        '</span><span class="postPanel_nameTextContainer"></span></a>';
                      }
                            html +='</div></div>';
       $(".post_data").append(html);
           
           
              }


               $("#modal-spin").hide();
               $("#postModal").text(name);
               $('#show-post-task').modal('show'); 
              }
                  });
                e.handled = true;
             }
           
          
              
        }).on('click', '.btn-campaign', function (e) {
              if (e.handled !== true) {
                var attr_id = $(this).attr('id');
                var post_id  = attr_id.replace("campaign_","");
                var btn_val = $("#"+attr_id).attr('aria-pressed');
                
                if(btn_val == "false") // previous false next true
                {
                  var answer = confirm("Are you sure  to add campaign ?")
                  if (answer) {
                     $.ajax({
                       headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                       url:'{{ route("SetCampaign") }}',
                       type: 'POST',
                       data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_val:1},
                       success: function(response) { //alert(response)
                        if(response>=0)
                        {
                         var x = document.getElementById("snackbar")
                          $("#snack_desc").html('');
                          $("#snack_desc").html('Campaign added successfully!!');
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                        }
                      }
                    });
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline active");
                    // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                     // $("#"+attr_id).attr("aria-pressed", "false");
                     // $("#"+attr_id).removeAttr("class");
                     // $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                    
                  }
              }
              else
              {
                var answer = confirm("Are you sure to remove campaign?")
                if (answer) {
                 $.ajax({
                   headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                   url:'{{ route("SetCampaign") }}',
                   type: 'POST',
                   data: {id:post_id,brand_id:GetURLParameter('pid')},
                   success: function(response) { //alert(response)
                    if(response>=0)
                    {
                      var x = document.getElementById("snackbar")
                        $("#snack_desc").html('');
                        $("#snack_desc").html('Campaign remove successfully!!');
                        x.className = "show";

                       setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                       $("#"+post_id).closest('tr').remove();
                    }
                  }
                });
              }
              else
              {
                 $("#"+attr_id).attr("aria-pressed", "true");
                 $("#"+attr_id).removeAttr("class");
                 $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline  active focus");
              }      
            }
          }
            e.handled = true;
        }).on('change', '.edit_senti', function (e) {
            if (e.handled !== true) {

          var senti_id = $(this).attr('id');
          var post_id  = senti_id.replace("sentiment_","");
          var sentiment = $('#sentiment_'+post_id+' option:selected').val();
          var emotion = $('#emotion_'+post_id+' option:selected').val();
          $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
          if(sentiment == 'pos')
           $("#sentiment_"+post_id).addClass('text-success');
          else if (sentiment == 'neg')
            $("#sentiment_"+post_id).addClass('text-red');
          else
            $("#sentiment_"+post_id).addClass('text-warning');

          
               $.ajax({
               headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
               url:'{{ route("setMentionPredict") }}',
               type: 'POST',
               data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment},
               success: function(response) {
                if(response>=0){
                  var x = document.getElementById("snackbar")
                  $("#snack_desc").html('');
                  $("#snack_desc").html('Sentiment Changed!!');
                  x.className = "show";
                  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                }
              }
            });
                e.handled = true;
             }
  }).on('click', '.see_comment', function (e) {//alert("hihi");
  if (e.handled !== true) {
        var attr_id = $(this).attr('id');
        var post_id  = attr_id.replace("seeComment_","");
        //alert(post_id);
        // window.open("{{ url('relatedmentioncmt?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
        window.open("{{ url('relatedmentioncmt?')}}" +"pid="+ GetURLParameter('pid') +"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&post_id="+post_id, '_blank');
        
         
        }
         e.handled = true;
          
       }).on('click', '.post_hide', function (e) {//alert("hihi");
  if (e.handled !== true) {
    var attr_id = $(this).attr('id');
    var post_id  = attr_id.replace("hide_","");
    //alert(post_id);
    var answer = confirm("Are you sure to hide this post?")
                if (answer) {
                     $.ajax({
                         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                         url:'{{ route("SetHideMention") }}',
                         type: 'POST',
                         data: {id:post_id,brand_id:GetURLParameter('pid'),table_type:'post'},
                         success: function(response) {// alert(response)
                          if(response>=0)
                          {
                            $("#"+post_id).closest('tr').remove();
                          }
                        }
                            });
                }
                else {
                    //some code
                }
     
     
     
        }
         e.handled = true;
       });

    new $.fn.dataTable.FixedHeader( oTable );

   // oTable.columns.adjust().draw();
   // $('#tbl_post').css('width', '100%');
   // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
  }

//   function getAllMentionComment(fday,sday,filter_tag='')
//     {
      
//       $(".popup_post").unbind('click');
//       $(".edit_predict").unbind('click');
//       $("#add_tag").unbind('click');
//       $("#comment_hide").unbind('click');
//       var filter_senti_arr = [];
//             $.each($("input[name='sentimentlist']:checked"), function(){            
//                 filter_senti_arr.push($(this).val());
//             });
//       var filter_keyword_arr = [];
//             $.each($("input[name='keyword']:checked"), function(){            
//                 filter_keyword_arr.push($(this).attr('id').replace('K_',''));
//             });
//         var campaign_name = GetURLParameter('campaign_name');
//         var campaign_id = GetURLParameter('campaign_id');
     
//  var oTable = $('#tbl_mention_comment').DataTable({

//         "lengthChange": false,
//         "searching": false,
//         "processing": false,
//         "serverSide": true,
//         "destroy": true,
//         "ordering": false   ,
//         "headers": {
//           'X-CSRF-TOKEN': '{{csrf_token()}}' 
//         },

//         "ajax": {
//           "url": '{{ route('getcampaignMentionCmt') }}',
//           "dataSrc": function(res){
//               //var count = res.data.length;
//               //alert(count);
//               document.getElementById('title_mention_comment').innerHTML = 'Comments ('+res.recordsTotal+")";
//               tmp_mentionTotal = tmp_mentionTotal + res.recordsTotal; 
//               document.getElementById('title_mention').innerHTML = 'Mentions('+tmp_mentionTotal+')';
              

             
//                // document.getElementById('title_mention').innerHTML = 'Mentions ('+res.recordsTotal+")";
//               return res.data;
//             },
//    /*       data: function ( d ) {
//             d.fday = mailingListName;
//             d.sday = mailingListName;
//             d.brand_id = mailingListName;
//           }*/
//            "data": {
//             "fday": fday,
//             "sday": sday,
//             "keyword_flag": '1',
//             "other_page":'1',
//             "brand_id": GetURLParameter('pid'),
//             "sentiType": GetURLParameter('SentiType'),
//             "period": GetURLParameter('period'),
//             "tsearch_senti":filter_senti_arr.join("| "),
//             "tsearch_keyword":filter_keyword_arr.join("| "),
//             "tsearch_tag":filter_tag,
//             "campaign_name":campaign_name ,
//             "campaign_id":campaign_id,
//             // "type":'mention comment',
//           }

//         },
//         "initComplete": function( settings, json ) {
//           //console.log(json);
       
//         },
//      drawCallback: function() {
//      $('.select2').select2();
//   },

//         columns: [
//         {data: 'post_div', name: 'post_div',"orderable": false},
    


//         ]
        
//       }).on('click', '.popup_post', function (e) {
//   if (e.handled !== true) {
//     var name = $(this).attr('name');
//     //alert(name);
//     var post_id = $(this).attr('id');
//     $("#modal-spin").show();
//     $(".post_data").empty();

//     //alert(post_id);alert(name);alert(GetURLParameter('pid'));
//          $.ajax({
//          headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
//          url:'{{ route("getRelatedMention") }}',
//          type: 'GET',
//          data: {id:post_id,brand_id:GetURLParameter('pid')},
//          success: function(response) { //console.log(response);
//          var res_array=JSON.parse(response);
//           var data=res_array[0];

       

//             for(var i in data) {//alert(data[i].message);
//               var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
//                var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

//                var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
//                var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
//                var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
//                pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
//                neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
//                neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
//                pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
//                neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
//                neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

//                var page_name=data[i].page_name;
            

//              var page_Link= "https://www.facebook.com/" + page_name ;                   

//  var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
//                '  <div class="sl-left">'+
//                '<img src="assets/images/unknown_photo.jpg"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
//                 ' <div class="sl-right">'+
//                 '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a> '+
//                 ' <div class="sl-right"> '+
//              ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
//               ' <div style="width:55%;display: inline-block">';
//               if(neg_pcent!=='')
//               html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
//             if(pos_pcent!=='')
//               html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
//             if(neutral_pcent!=='')
//               html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

//                 html+=' </div></div></div></span></p> ' +                
//                   '   <p class="m-t-10" align="justify">' + 
//                     data[i].message +
                  
//                     '</p>';
//                  if(data[i]['post_type'] =='photo')
//                 {
//                    html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
//                   '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
//                   '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
//                   '</span><span class="postPanel_nameTextContainer"></span></a>';
//                 }
//                 else if(data[i]['post_type'] =='video')
//                 {
//                    html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
//                   '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
//                   '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
//                   '</span><span class="postPanel_nameTextContainer"></span></a>';
//                 }
//                       html +='</div></div>';
//  $(".post_data").append(html);
     
     
//         }


//          $("#modal-spin").hide();
//          $("#postModal").text(name);
//          $('#show-post-task').modal('show'); 
//         }
//             });
//           e.handled = true;
//        }
     
    
        
//   }).on('change', '.edit_senti', function (e) {

//   if (e.handled !== true) {

//     var senti_id = $(this).attr('id');
//     var post_id  = senti_id.replace("sentiment_","");
//     var sentiment = $('#sentiment_'+post_id+' option:selected').val();
//     var emotion = $('#emotion_'+post_id+' option:selected').val();
    

//     $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
//     if(sentiment == 'pos')
//      $("#sentiment_"+post_id).addClass('text-success');
//     else if (sentiment == 'neg')
//       $("#sentiment_"+post_id).addClass('text-red');
//     else
//       $("#sentiment_"+post_id).addClass('text-warning');

    
//          $.ajax({
//          headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
//          url:'{{ route("setUpdatedPredict") }}',
//          type: 'POST',
//          data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,type:'mention'},
//          success: function(response) {// alert(response)
//           if(response>=0)
//           {
//         //         swal({   
//         //     title: "Updated!",   
//         //     text: "Done!",   
//         //     timer: 1000,   
//         //     showConfirmButton: false 
//         // });
//           var x = document.getElementById("snackbar")
//                       $("#snack_desc").html('');
//                       $("#snack_desc").html('Sentiment Changed!!');
//                         x.className = "show";

//                         setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
//           }
//         }
//             });

//           e.handled = true;
//        }
     
    
        
//   }).on('click', '.comment_hide', function (e) {//alert("hihi");
//   if (e.handled !== true) {
//     var attr_id = $(this).attr('id');
//     var comment_id  = attr_id.replace("hide_","");
//     //alert(comment_id);

//      var answer = confirm("Are you sure to hide this comment?")
//                 if (answer) {
//                    $.ajax({
//          headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
//          url:'{{ route("SetHideMention") }}',
//          type: 'POST',
//          data: {id:comment_id,brand_id:GetURLParameter('pid'),table_type:'comment'},
//          success: function(response) { //alert(response)
//           if(response>=0)
//           {
//             $("#"+comment_id).closest('tr').remove();
//           }
//         }
//             });
//                 }
//                 else
//                 {

//                 }
     
     
     
//         }
//          e.handled = true;
//        }).on('click', '.popup_img', function (e) {
//     if (e.handled !== true) {

//          var id = $(this).attr('id');
//          var src_path = $('#'+id).attr('src');
//           $(".large_img").empty();
// // img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
//           $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%'' src='"+src_path+"'/>");
//           $('#show-image').modal('show'); 
//          }
       
      
          
//     });
     
    
        
//   }
    $('#show-add-tag').on('shown.bs.modal', function () {
      $('#name').focus();
  })  
  $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
                startDate: startDate,
                endDate: endDate,
          },function(start, end,label) {
          var startDate;
          var endDate;
          label = label;
          startDate = start;
          endDate = end;
          ChooseDate(startDate,endDate,label,'');
        });
   //ChooseDate(startDate,endDate,'','');

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
     //window.dispatchEvent(new Event('resize'));
  // if(target=="#dashboard")
  //   {
      
  //  }

  //    if(target == '#comment')
  // {
  //   $("#outer_div").removeClass("col-lg-12");
  //   $("#outer_div").addClass("col-lg-9");
  //   $("#filter_div").show();
   
  // }
  // else
  // {
  //    $("#outer_div").removeClass("col-lg-9");
  //   $("#outer_div").addClass("col-lg-12");
  //   $("#filter_div").hide();
  // }
  
    if(target == '#dashboard')
     {
       $("#outer_div").removeClass("col-lg-9");
       $("#outer_div").addClass("col-lg-12");
       $("#filter_div").hide();
     }
     else
     {

      $("#outer_div").removeClass("col-lg-12");
       $("#outer_div").addClass("col-lg-9");
       $("#filter_div").show();
        if(target == '#mention')
      {
        $("#tag_filter").hide();
       
      }
       else if(target == '#pages')
       {
        $("#tag_filter").show();
        
        
       }

     }


window.dispatchEvent(new Event('resize'));

   });

  hidden_div();
    // alert($('#PostTotal').val());
  function MostFrequentlyTag(fday,sday)
  {
    // alert($("#hidden-chip").val());
    $(".tag_count").empty();
    $("#tag-count-spin").show();
    var brand_id = GetURLParameter('pid');

        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,campaign_name:$("#hidden-chip").val(),limit:10}
    })
    .done(function( data ) {//console.log(data);
      $(".tag_count").empty();
        $("#tag-count-spin").hide();
for(var i in data) 
        {
           var tagLabel=data[i].tagLabel;
           var tagId=data[i].tagId;
           var tagCount=data[i].tagCount;
           $(".tag_count").append('<button type="button" style="margin:0.2rem" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_frequent_tag" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>');

        }
    })
     .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
    
    });
  }

  function tagCount()
  {
    
      var fday=GetStartDate();
      var sday=GetEndDate();
      // $("#tbl_tag_count tbody").empty();
      $(".tag-list").empty();
      //alert("hihi");
      var brand_id = GetURLParameter('pid');
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getTagCount')}}", // This is the URL to the API
        data: { 
          fday: fday,
          sday:sday,
          brand_id:brand_id,
          // filter_page_name:$('#admin_page_filter option:selected').val(),
          admin_page:$('#hidden-chip').val(),
          limit:'no'}
      })
      .done(function( data ) {//console.log(data);
  for(var i in data) 
          {
             var tagLabel=data[i].tagLabel;
             var tagId=data[i].tagId;
             var tagCount=data[i].tagCount;
            
              $(".tag-list").append(' <li>' +
              ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'">'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');

          }

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
  getKeyword();
  function getKeyword()
{
  // alert('hey');
     $(".keyword-list").empty();
    var brand_id = GetURLParameter('pid');
        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getKeywordsByGroup')}}", // This is the URL to the API
      data: {brand_id:brand_id,campaign_name:GetURLParameter('campaign_name')}
    })
    .done(function( data ) {console.log(data);
          
for(var i in data) 
        {
          if(i<4)
          {
            $(".keyword-list").append('<li class="see-li">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" '+
            ' id="K_'+data[i]+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'">'+data[i]+'</label> '+
            ' </li>');
          }
          else if (i==4)
          {
            $(".keyword-list").append('<li class="see-li">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'" '+
            ' id="K_'+data[i]+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'">'+data[i]+'</label> '+
            ' </li>  <li class="see-li" id="more"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see more</a></li>');
               
          }
          else
          {
             $(".keyword-list").append('<li class="hidden-li" style="display:none">' +
            ' <input type="checkbox" class="check keyword" name="keyword" value="'+data[i]+'"'+
            ' id="K_'+data[i]+'">'+
            ' <label class="text-info" style="padding-left:30px;font-weight:500;font-size:12px" for="K_'+data[i]+'">'+data[i]+'</label> '+
            ' </li><br/>');

          }
                         
         


        }
        $(".keyword-list").append('<li class="hidden-li" id="less" style="display:none"><a style="text-decoration: underline;font-size:13px" href="javascript:void(0);" class="link">see less</a></li>');
   
    

    })
     .fail(function(xhr, textStatus, error) {
       // console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
    
    });

        
}
$(document).on('click', '#more', function () {
   $('#more').css('display','none');
    $('.hidden-li').css('display','inline');
});
$(document).on('click', '#less', function () {
  $('#less').css('display','none');
    $('.hidden-li').css('display','none');
     $('#more').css('display','inline');
});

  $(document).on('click', '.taglist', function () {
     tmp_mentionTotal=0;tmp_pageTotal=0;
    // getAllComment(GetStartDate(),GetEndDate());
    getAllPost(GetStartDate(),GetEndDate());
    getAllMentionPost(GetStartDate(),GetEndDate());
    // getAllMentionComment(GetStartDate(),GetEndDate());
  });
  $(document).on('click', '.sentimentlist', function () {
    tmp_mentionTotal=0;tmp_pageTotal=0;
    // getAllComment(GetStartDate(),GetEndDate());
    getAllPost(GetStartDate(),GetEndDate());
    getAllMentionPost(GetStartDate(),GetEndDate());
    // getAllMentionComment(GetStartDate(),GetEndDate());
    getAllArticle(GetStartDate(),GetEndDate());
   // tagCount();
});
  $(document).on('click', '.keyword', function () {
    tmp_mentionTotal=0;tmp_pageTotal=0;
   getAllMentionPost(GetStartDate(),GetEndDate());
   // getAllMentionComment(GetStartDate(),GetEndDate());
   getAllPost(GetStartDate(),GetEndDate());
   // getAllComment(GetStartDate(),GetEndDate());
   getAllArticle(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
});
  $(document).on('click','.top_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:80%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
    $(document).on('click','.top_cmt_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
  $('#senti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.taglist)").click(function () {
    $("#senti-all").prop("checked", $("input:checkbox:not(.taglist):checked").length == 4);
  });

  bindOtherPage();
  $(document).on('click', '.chip', function (e) {
    var id = $(this).attr("id");  
    var name = $(this).attr("name");
    var chipcompetitor = document.getElementById("chip-competitor");
    //alert(name);  
      $(".chip").css("background-color","#e4e4e4");
      $(".chip").css("color","rgba(0,0,0,0.6)");
       // chipcompetitor.classList.remove("myHeaderChip");
     // chipcompetitor.classList.add("myHeaderChip");
     // $('.dropdown.open .dropdown-toggle').dropdown('toggle');
     // $("#search_form").css("visibility", "hidden");
     // $("#search_form").css("display", "inline");
      $("#"+id).css("background-color","rgba(188, 138, 49, 1)");
      $("#"+id).css("color","white");
      $("#hidden-chip").val(name);
    // alert($("[data-toggle=dropdown]").attr("aria-expanded"))  ;
     $(".dropdown-pages").dropdown('toggle');
      getKeyword();
      ChooseDate(startDate,endDate,'','');
     
      
        });
 // $("#btn_pages").on('click', function () {
 //  $("#search_form").css("visibility", "visible");
 //  $("#search_form").css("display", "inline");
 // });
//  $("[data-toggle=dropdown]").prop('onclick', null).off('click');
// $("[data-toggle=dropdown]").click(function (e) {
//     e.preventDefault();
//     // e.stopPropagation();
//     // console.log($(this));
//     let el = $(this);
//     let expanded = el.attr("aria-expanded") || false;
//     alert(expanded);
//     if (!expanded) {
//         $(this).dropdown('toggle');
//     }

//     el.attr("aria-expanded", !expanded);
// })

  function bindOtherPage()
  {
    var username = $("#hidden-user").val();
      $("#chip-competitor tbody").empty();
      var brand_id = GetURLParameter('pid');
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getOtherCampaign')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          }
      })
      .done(function( data ) {//alert(data);
       console.log(data);
      if ($.trim(data)){   
       
       
       
       

  for(var i in data) 
          {
             var photo ="{{asset('assets/images/competitor1.png')}}";
         
             if(parseInt(i) == 0)
              { 
                
                $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['name']+'" id="'+data[i]['id']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['name'] + '</div>');
             
                   $("#hidden-chip").val(data[i]['name']); 
                
              }
              else
              {
                $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['name']+'"  id="'+data[i]['id']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['name'] + '</div>');
              }

           
           


          }
          ChooseDate(startDate,endDate,'','');
          getKeyword();
        }
       

      })

       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }


    $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
         });


  $('#admin_page_filter').change(function() {
      //alert($(this).val());
      var admin_page = $(this).val();
      ChooseDate(startDate,endDate,'','');

     
       // $(this).val() will work here
  });


  $('input[type=radio][name=options]').change(function() {

    if (this.value == 'page') {
     var date_preset= Calculate_DatePreset(label);
        PageGrowth(GetStartDate(),GetEndDate(),date_preset);
        
    }
    else if (this.value == 'sentiment') {
        InboundSentimentDetail(GetStartDate(),GetEndDate());
    }
    
});
  $(".btnComment").click(function(e){
    var commentType = $(this).attr('value');
    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ $('#hidden-chip').val() , '_blank');
})
$(".btnPost").click(function(e){
    var postType = $(this).attr('value');
    window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&overall="+postType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ $('#hidden-chip').val() , '_blank');
})
  $(document).on('click', '.btn_frequent_tag', function() {
     var TagName = $(this).attr('value');
     // alert(TagName);
      // window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&campaign_name="+ $('#hidden-chip').val() , '_blank');
       window.open("{{ url('related_campaigncomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&campaign_name="+ $('#hidden-chip').val() , '_blank');

  })
  
  //local function
  function highLightText(keyForHighLight,message)
{
  keyForHighLight.sort(function(a, b){return b['campaign_keyword'].length - a['campaign_keyword'].length});
  var highligh_string  = message;
  
for(var i in keyForHighLight) {
   highligh_string = highligh_string.replace( new RegExp(keyForHighLight[i]['campaign_keyword'], "ig") ,'<span style="background:#FFEB3B">' + keyForHighLight[i]['campaign_keyword'] + '</span>')
 }
 return highligh_string;
}

  function numberWithCommas(n) {
      var parts=n.toString().split(".");
      var res = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
      if(res == '')
        res = 0

      return res;
  }

    function kFormatter(num) {
      return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
  }
    function readmore(message){
        // alert("hi hi ");
          var string = String(message);
          var length = string.length; 
           // alert(length);
                  if (length > 500) {
            // alert("length is greater than 500");

              // truncate

              var stringCut = string.substr(0, 500);
               // alert(stringCut);
              // var endPoint = stringCut.indexOf(" ");

              //if the string doesn't contain any space then it will cut without word basis.
               
              // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
              string =stringCut.substr(0,length);
              // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
              // alert(string);
          }
          return string;


          }

          

    });
      </script>
<style type="text/css">

table.fixedHeader-floating {
  clear: both;
  top: 114px !important;
  z-index:5;
   top : 0;
   left:0;
   width: 100%;
/*   margin: 0 auto;
     padding: 0 15px;*/
 /*    overflow-x: hidden;
      overflow-y: auto;*/
  
    box-sizing: border-box;
/*
  background: red;*/
/*  left:150px !important;*/
/*  margin-right:30px;*/
 
/*  width: 80% !important;*/
  /*background: transparent;*/
}
/*table.dataTable thead tr th {
    word-wrap: break-word;
    word-break: break-all;
}*/
table.dataTable tbody tr td {
    word-wrap: break-word;
    word-break: break-all;
}
.myHeaderContent
{
   margin-right:300px;
}
.TopDivPadding
{
  padding-left:10px;
}
.TopDivCardBody
{
  padding-top:1rem;
  padding-bottom:1rem;
}
.TopDivNumber
{
  font-weight: 500;
  font-size: 40px;
}
 #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }
  .btn {
    padding: 2px 6px;
    font-size: 14px;
    cursor: pointer;
}


  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
  @endpush


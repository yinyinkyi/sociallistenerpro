@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
  
  <div class="row page-titles">
    <div class="col-md-8 col-8 align-self-center">
      <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Exchange Rates Comparison</h4>

    </div>
    <div class="col-md-4 col-4 align-self-center">
      <div class="d-flex m-r-20 m-l-10 hidden-md-down" style="padding-top: 5px;">
        <input type='text' class="form-control dateranges" style="datepicker" />
          <div class="input-group-append">
            <span class="input-group-text">
              <span class="ti-calendar"></span>
            </span>
          </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-12">
      <div class="card card-outline-info">
        <div class="card-body">
          <div class="form-body">
            <div class="row col-md-12 " style="padding-left: 20px;">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Currency</label>
                  <select id="currency" class="form-control custom-select">
                    <option selected="selected" value="usd">USD</option>
                    <option value="euro">EURO</option>
                    <option value="sgd">SGD</option>
                  </select>
                  <small class="form-control-feedback"> Choose currency to compare</small> </div>
              </div>
                                           
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Rate</label>
                    <select id="rate" class="form-control custom-select">
                      <option selected="selected" value="buy">Buy</option>
                      <option  value="sell" >Sell</option>
                      <option  value="margin" >Margin</option>
                    </select>
                    <small class="form-control-feedback"> Choose rate to compare</small>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Bank</label>
                    <select id="bank" class="form-material form-control  select2" multiple="multiple" data-placeholder="Select Banks">
                      @foreach($bank_names as $bank_name)
                      <option>{{ $bank_name->bank_name }}</option>
                      @endforeach
                    </select>
                    <small class="form-control-feedback"> Choose bank to compare</small>
                </div>
              </div> 
              <div class="col-md-2">
                <button class="btn-sm btn_go">Go</button>
              </div>                     
            </div>
          </div> 
          <div class="row" style="margin-top: 10px;">
            <div style="display:none"  align="center" style="vertical-align: top;" id="exchange-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
              <div id="exchange-chart" style="width:100%;height:300px"></div>  
          </div> 
        </div>
      </div>
    </div>
  </div>    
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
  <script type="text/javascript">

    var colors=[ "#4267b2","#382448","#01ad9d","#cb73a9","#a1c652","#7b858e","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];

    $(document).ready(function() {

      var GetURLParameter = function GetURLParameter(sParam) {
      var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

      for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : sParameterName[1];
        }
      }
   };
    var APP_URL = {!! json_encode(url('/')) !!};

     $('.select2').select2();

     var compare_count=0;var brand_id_arr=[];var fanLabel = [];var Legend_Data=[];var arr_fan_label=[];
     var arr_fan_total=[];var fanseriesList = [];

    startDate = moment().subtract(1, 'month');
    endDate = moment();
    label = 'month';

   $('.singledate').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
        format: 'DD/MM/YYYY'
    }
  },function(date) {
           
    endDate=date;
    var id =$('input[type=radio][name=period-radio]:checked').attr('id');
    if(id==="period-week")
   {
     startDate = moment(endDate).subtract(1, 'week');
     endDate = endDate;
   }
   else
   {
     startDate = moment(endDate).subtract(1, 'month');
     endDate = endDate;
   }
          
     // var admin_page=$('.hidden-chip').val();
     //  // alert(admin_page);     
     //  // alert('helo');
     //   var kw_search = $("#kw_search").val();
       // alert(kw_search);
       // alert(startDate);
     // ChooseDate(startDate,endDate,admin_page,'','',kw_search);
     // refreshGraph()
         
  });



  function GetStartDate(){
    return startDate.format('YYYY-MM-DD');
  }
  function GetEndDate(){
    return endDate.format('YYYY-MM-DD');
  }

  refreshGraph(startDate,endDate,label);

  var permission = GetURLParameter('accountPermission');

  $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
                startDate: startDate,
                endDate: endDate,

          },function(start, end,label) {//alert(label);
          var startDate;
          var endDate;
          var labelDate;
          startDate = start;
          endDate = end;
          labelDate = label;

          fanseriesList=[];fanLabel = [];arr_fan_total=[]; arr_fan_label=[]; Legend_Data=[];brand_id_arr=[];

          // reachseriesList=[];reachLabel = [];arr_reach_total=[];arr_reach_label=[];
            
          // fan_abs_diff=[];
         
          // likeseriesList=[];likeLabel = [];arr_like_total=[];arr_like_label=[];likemarkpointlist=[];arr_tag_label=[];
          // post=[]; share=[]; comment=[]; summaryLabel=[];
          // tag_pos=[];tag_neg=[];tag_neutral=[];tagsentiLabel=[];pos_percent_tag=[];neg_percent_tag=[];
          // neutral_percent_tag=[];tag_qty_list=[];tagmarkpointlist=[];tagpercentmarkpointlist=[];
          // tag_qty_Label = [];tag_qty_seriesList=[];post_tag_qty_seriesList=[];

          // arr_ov_positive=[]; arr_ov_negative=[]; arr_ov_neutral=[]; OverallLabel=[];arr_ov_markpointlist=[];arr_ovQty_markpointlist=[];
          // arr_ov_qty_positive=[];arr_ov_qty_negative=[];arr_ov_qty_neutral=[];
          // arr_cmt_qty_positive=[];arr_cmt_qty_negative=[];arr_cmt_qty_neutral=[];
          // arr_cmt_positive=[]; arr_cmt_negative=[]; arr_cmt_neutral=[]; CommentLabel=[];PostLabel=[];
          // arr_cmt_markpointlist=[];arr_cmtQty_markpointlist=[];
          // Social_seriesList=[]; socialLabel = [];arr_social_total=[];
          // Senti_data1=[];Senti_data2=[];Senti_xAxisData=[];
          // RSCseriesList=[];RseriesList=[];
  
       
            if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) {
              swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }
            else{
              // Clear_reaction_table();
              // Clear_top_post_table();
              // Clear_frequent_tag_table();
              // Clear_post_frequent_tag_table();
        
                  compare_count=0;
                  refreshGraph(startDate,endDate,labelDate);
                 }
           }
           else
           {
              // Clear_reaction_table();
              // Clear_top_post_table();
              // Clear_frequent_tag_table();
              // Clear_post_frequent_tag_table();
              
                compare_count=0;
                refreshGraph(startDate,endDate,labelDate);
           }
      });

   $(document).on('click', '#currency', function() {
     // refreshGraph(startDate,endDate,label);

  })
   $(document).on('click', '#rate', function() {
     // refreshGraph(startDate,endDate,label);
  })



  function refreshGraph(startDate,endDate,labelDate){
    ChooseDate(startDate,endDate,brand_id_arr[0],0,labelDate);
  }


  function ChooseDate(start,end,brand_id,sr_of_brand,label){
    $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
    startDate=start;
    endDate=end;
    FanGrowth(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
}
// alert(brand_id_arr);
   $(document).on('click', '.btn_go', function (e) {
        // fan =[];fanLabel=[];Legend_Data=[];
        // alert(brand_id_arr);
        var selected_value = $("#bank").val();
        // alert(selected_value);
        // alert(brand_id_arr);
        if(brand_id_arr.length <=0){

        brand_id_arr.push(selected_value);

        Legend_Data.push(selected_value);

        ChooseDate(startDate,endDate,selected_value,0,label);
      }
      else{

        compare_count=compare_count+1;

        Legend_Data.push(selected_value);
          // console.log(Legend_Data);
        brand_id_arr.push(selected_value);
        ChooseDate(startDate,endDate,selected_value,compare_count,label);
      }

   })




    function FanGrowth(fday,sday,page_name,fan_sr_of_brand,date_preset){
    fanLabel=[];fan=[];
    var ExchangeChart = echarts.init(document.getElementById('exchange-chart'));
    $('#exchange-spin').show();
    var brand_id = GetURLParameter('pid');
    // var banks = $('#bank option:selected').map(function () {
    //                 return $(this).val();
    //             }).get().join(',');

    var currency = $('#currency').val();
    var rate = $('#rate').val();

    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getDailyExchangeRates')}}", // This is the URL to the API
      data: {page_name:page_name,fday:fday,sday:sday,currency:currency,rate:rate}
    })
    .done(function( data ) {

    
    var chart = new CanvasJS.Chart("exchange-chart",
    {
      title:{
      text: "Multi-Series Line Chart"  
      },
      data: [
      {        
        type: "line",
        dataPoints: [
        { x: 10, y: 21 },
        { x: 20, y: 25 },
        { x: 30, y: 20 },
        { x: 40, y: 25 },
        { x: 50, y: 27 },
        { x: 60, y: 28 },
        { x: 70, y: 28 },
        { x: 80, y: 24 },
        { x: 90, y: 26 }
      
        ]
      },
        {        
        type: "line",
        dataPoints: [
        { x: 10, y: 31 },
        { x: 20, y: 35},
        { x: 30, y: 30 },
        { x: 40, y: 35 },
        { x: 50, y: 35 },
        { x: 60, y: 38 },
        { x: 70, y: 38 },
        { x: 80, y: 34 },
        { x: 90, y: 44}
      
        ]
      },
        {        
        type: "line",
        dataPoints: [
        { x: 10, y: 45 },
        { x: 20, y: 50},
        { x: 30, y: 40 },
        { x: 40, y: 45 },
        { x: 50, y: 45 },
        { x: 60, y: 48 },
        { x: 70, y: 43 },
        { x: 80, y: 41 },
        { x: 90, y: 28}
      
        ]
      },
        {        
        type: "line",
        dataPoints: [
        { x: 10, y: 71 },
        { x: 20, y: 55},
        { x: 30, y: 50 },
        { x: 40, y: 65 },
        { x: 50, y: 95 },
        { x: 60, y: 68 },
        { x: 70, y: 28 },
        { x: 80, y: 34 },
        { x: 90, y: 14}
      
        ]
      }
      ]
    });

    chart.render();
    $('#exchange-spin').hide();
  


    //     ExchangeChart.setOption(option, true), $(function() {
    //     function resize() {
    //         setTimeout(function() {
    //             ExchangeChart.resize()
    //         }, 100)
    //     }
    //     $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    // });

    // var arrayLength=brand_id_arr.length;
    // fan_sr_of_brand = fan_sr_of_brand +1;
    // if(fan_sr_of_brand<arrayLength )
    // {
    //     FanGrowth(fday,sday,brand_id_arr[fan_sr_of_brand],fan_sr_of_brand,date_preset);
    // }
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in FAN API" );
            });

        
      }



 function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

  }); /*document ready close*/


  </script>
  <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

  <style type="text/css">
  /*multiselect box remove icon color*/
 .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
  color:#7e6b6b;
 }
  </style>
@endpush


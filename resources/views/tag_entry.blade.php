@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

            <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Tags</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>



                </div>

@if(!isset($data))
<!-- <div class="box box-warning"> -->
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        <form method="POST" role="form" class="form-material m-t-10"  action="{{ route('tags.store') }}" aria-label="{{ __('Tags') }}">
                {{csrf_field()}}
                <input type="hidden" id="brand_id" name="brand_id" value="{{ $brand_id }}" >
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
<!-- 
                    <div class="form-group">
                          <label for="category_id" class="control-label">{{ __('Category') }}</label>
                          <select class="form-control custom-select" id="category_id" name="category_id" required>
                          <option value=""  selected>Choose Category</option>

                          @if (isset($category))
                          @foreach($category as $index =>$category)
                          <option value="{{$category->id}}" >{{$category->category_name}}</option>
                          @endforeach
                          @endif
                         </select>
                          
                         @if ($errors->has('category_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('category_id') }}</strong>
                        </span>
                         @endif
                    </div> -->
            <!-- <div class="form-group">
                <label for="keywords" class="control-label">{{ __('Keywords') }}</label>
                
               <div class="tags-default">
                <input type="text" id="keywords" data-role="tagsinput" placeholder="add tags" class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old('keywords') }}" required /> </div>
                 @if ($errors->has('keywords'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('keywords') }}</strong>
                    </span>
                @endif
            </div> -->
 
            <div class="form-group">
              @if($action_permission == 0)
                <button type="button" class="btn btn-primary" id="create_alert">  {{ __('Save') }}</button>
                @else
                <button type="submit" class="btn btn-primary">  {{ __('Save') }}</button>
                @endif
              </div>
             </form>
                            </div>

                        </div>
                    </div>
                </div>
            <!-- </div> -->
            @else
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        {!! Form::model($data, ['method' => 'PATCH','class'=>'form-material m-t-10','enctype'=>'multipart/form-data','route' => ['tags.update', $data->id]]) !!}
                {{csrf_field()}}
                <input type="hidden" id="brand_id" name="brand_id" value="{{ old( 'name', $data->brand_id) }}" >
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old( 'name', $data->name) }}"  required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
<!--                     <div class="form-group">
                          <label for="category_id" class="control-label">{{ __('Category') }}</label>
                          <select class="form-control custom-select" id="category_id" name="category_id" required>
                          <option value=""  selected>Choose Category</option>

                          @if (isset($category))
                          @foreach($category as $index =>$category)
                          @if($category->id==$data->category_id)
                           <option value="{{ $category->id }}" selected="selected" >{{ $category->category_name }}</option>
                          @else
                          <option value="{{ $category->id }}" >{{ $category->category_name }}</option>
                          @endif
                          @endforeach
                          @endif
                         </select>
                          
                         @if ($errors->has('category_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('category_id') }}</strong>
                        </span>
                    @endif
                    </div> -->
            <!-- <div class="form-group">
                <label for="keywords" class="control-label">{{ __('Keywords') }}</label>
                <div class="tags-default">
                <input type="text" id="keywords" data-role="tagsinput" placeholder="add tags" class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old( 'keywords', $data->keywords) }}" required /> </div>
                 @if ($errors->has('keywords'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('keywords') }}</strong>
                    </span>
                @endif
            </div> -->
         
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Register') }}</button>
              </div>
             {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>

@endif
              
                  <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">Tags List</h4>
                            
                                 <table id="tags_table" class="table">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <!-- <th>Keywords</th> -->
                    <th></th>
                   
                  </tr>
                  </thead>
                 <!--  <tbody>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Apple</a></td>
                    <td>2018-07-06</td>

                    <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Eleven</a></td>
                    <td>2018-07-25</td>
                  <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                    
                  </tr>
             
                  
                  </tbody> -->
                </table>
                            </div>

                        </div>
                    </div>
                </div>  
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
  <script type="text/javascript">

$(document).ready(function() {
      $(document).on("click","#create_alert",function(){
       swal({
                title: 'Sorry',
                text: "This account has no permission to do this!",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
});

    var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
    //alert(GetURLParameter("pid"));
        $('#tags_table').DataTable({
       "lengthChange": true,"info": false, "searching": true,
        processing: true,
        serverSide: false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
         "ajax": {
          "url": '{{ route('gettaglist') }}',

 
         "data": {
            "brand_id":GetURLParameter("pid")
          }
 },
         columns: [
            {data: 'name', name: 'name'},
            // {data: 'keywords', name: 'keywords'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
        
    }).on('click', '.show_alert', function (e) {
           e.preventDefault();
           $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
            // var url = $(this).data('remote');
            swal({
                title: 'Sorry',
                text: "This account has no permission to do this!",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })

        }).on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
          var url = $(this).data('remote');
      
                      swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                   $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  data: {method: '_DELETE', submit: true}
              }).always(function (data) {
                   $('#tags_table').DataTable().rows().invalidate().draw();
                    window.location.reload()
                 
              });
             
              }
            })
      });


    });

</script>
<style>
  .bootstrap-tagsinput{
    width: 100%
  }
</style>
@endpush


@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                   <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">  <img src="{{asset('Logo/beerIcon.png')}}" style="width:10%;height: 10%">   Auto Tagging </h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>



                </div>

              
                <!-- Row -->
             <div class="row" >
                    <div class="col-12" >
                      
                        <div class="card" >
                            <div class="card-body">
                                <form role="form" class="" action="" method="get" id="myform">
          {{csrf_field()}}
 
      <!-- <div class="box-body"> -->
        <div class="row">
           <table class="table" style=" border-collapse:collapse">
            <!-- <td></td> -->
            <tr>

              <td >Post Link: </td>

              <td><input type="text" class="form-control" name="postID" id="postID" placeholder="post link from URL"></td> 
              <td width="10%">Page Name:</td>
              <td> <select id="pageSelect"  class="form-material form-control select2"  multiple="multiple" data-placeholder="Select Pages">    
                @foreach($page_arr as $page)
            <option value="{{$page}}">{{$page}}</option>
            @endforeach</select></td>
            </tr>
            <tr>
              <td > From: </td>
              <td>
         
          <select id="originTag"  class="form-material form-control select2"  multiple="multiple" data-placeholder="Select Tag">
            @foreach($tags as $tag)
            <option value="{{$tag->id}}">{{$tag->name}}</option>
            @endforeach
          </select>
              </td>

              <td > To: </td>
              <td>
           
          <select id="newTag" class="form-material form-control select2" multiple="multiple" data-placeholder="Select Tag"
                              style="width:350px;height:36px">
                              @foreach($tags as $tag)
            <option value="{{$tag->id}}">{{$tag->name}}</option>
            @endforeach
             </select>
        </td>
                   <td > Sentiment: </td>
              <td>
         
          <select id="senti"  class="form-material form-control" >
           
            <option value="neutral" selected="selected">neutral</option>
            <option value="pos">pos</option>
            <option value="neg">neg</option>

           
          </select>
              </td>
            </tr>
            <!-- <tr> -->
             
            <!-- </tr> -->
             
            </table>
             <button type="button" class="btn btn-flat btn-green" style="margin-left: 20px;" id="btn_save" value="">Save</button>
         </div>
   

        <!-- </div> -->
               
  <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
         


        </form> 
                            </div>
            
                        </div>
                       
                    </div>
                </div>

              
                
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>
     <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
     <script type="text/javascript">
     $(document).ready(function() {
        $(".select2").select2();
        $("select").on("select2:select", function (evt) {
          var element = evt.params.data.element;
          var $element = $(element);
          
          $element.detach();
          $(this).append($element);
          $(this).trigger("change");
        });


        // var hasFocus = $('#postID').is(':focus');
        // if(hasFocus){
        //   alert('jello');
        //     // $('#pageSelect').blur();
        //     $("#pageSelect").prop('disabled', true);

        // }
        $("#postID").change(function() {
           if (document.getElementById("postID").value)
        {
 
            $("#pageSelect").prop('disabled', true);
        }
        else
        {

          $("#pageSelect").prop('disabled', false);
        }
        })

        $("#pageSelect").change(function() {
           if (document.getElementById("pageSelect").value)
        {
 
            $("#postID").prop('disabled', true);
        }
        else
        {

          $("#postID").prop('disabled', false);
        }
        })
       

      


        $('#btn_save').on('click',function(e){
          
            var originTag = $('#originTag option:selected').map(function () {
                  return $(this).text();
              }).get().join(',');
            var originTagID = $('#originTag').val();
           
            var newTag = $('#newTag option:selected').map(function () {
                  return $(this).text();
              }).get().join(',');

             var pages = $('#pageSelect option:selected').map(function () {
                  return $(this).text();
              }).get().join(',');

            var newTagID= $('#newTag').val();
            var senti= $('#senti').val();
        
             $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("updateByAutoTag") }}',
           type: 'GET',
        

           data: {postID:$('#postID').val(),originTag:originTag,newTag:newTag,originTagID:originTagID,newTagID:newTagID,senti:senti,pages:pages},
           success: function(response) {
          
            if(response>=0){
              swal
         
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Tag Updated!!');
                x.className = "show";

              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
          }
        });
           })


       


      });
  </script>
  <style type="text/css">
  #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }
</style>
 <link href="{{asset('css/own.css')}}" rel="stylesheet">

@endpush


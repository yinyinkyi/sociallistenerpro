  @extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
  @section('content')
  @section('filter')
  <li  class="nav-item dropdown mega-dropdown" > <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark dropdown-pages" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search text-warning"></i> <span class="text-warning" id="select_page"></span></a>
  <div id="search_form" class="dropdown-menu scale-up-left" id="dlDropDown"  style="padding-left: 5%">
  <ul class="mega-dropdown-menu row">
   
  <li class="col-lg-12" >
   <form>
      <input type="hidden" value='' class="hidden-chip" id='' name="hidden-chip"/>
      <input type="hidden" value='{{ auth()->user()->company_id }}' id="hidden-user" name="hidden-user"/>
      <input type="hidden" value="{{ auth()->user()->default_page }}" class="hidden-pg" name="hidden-pg"/>
      <input type="hidden" value="{{ auth()->user()->role }}" class ="hidden-role" name="hidden-kw"/>
      @if(auth()->user()->brand_id == 34)
      <div class="row">
        <div class="col-md-4" style="font-weight: bolder">Bank</div>
        <div class="col-md-4" style="font-weight: bolder">MFS</div>
        <div class="col-md-4" style="font-weight: bolder">Card</div>


      </div>
      <div class="row">
      <div id="bank-chip" class="col-md-4" style="min-height:70px;padding-left:9px;padding-top:10px;border: 1px solid #f9f9f8;"></div>
        <div id="mfs-chip" class="col-md-4" style="min-height:70px;padding-left:9px;padding-top:10px;border: 1px solid #f9f9f8;"></div>
        <div id="card-chip" class="col-md-4" style="min-height:70px;padding-left:9px;padding-top:10px;border: 1px solid #f9f9f8;"></div>

      </div>
      @else
      <div id="chip-competitor" class="col-md-12 align-self-center"  style="min-height:70px;padding-left:20px;padding-top:10px;border: 1px solid #f9f9f8;">
      </div>
      @endif
      
   </form>
  </li>
  </ul>
  </div>
  </li>
  @endsection

    <!-- ============================================================== -->
                  <!-- Bread crumb and right sidebar toggle -->
                  <!-- ============================================================== -->
                
                   
                   <header class="" id="myHeader">
                    <div class="row page-titles">
                   
                      <div class="col-md-8 col-6 align-self-center">
                          <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                                   <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#dashboard" role="tab">Summary</a> </li>
                                   <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#post" role="tab"><span id="title_post">Posts</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#visitorpost" role="tab"><span id="title_visitor">Visitor Posts</span></a> </li>
                                  <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#comment" role="tab"> <span id="title_comment">Comments</span></a> </li>

                                 
                                  
                                  
                              </ul>
                              <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
            

                       
                      </div>
                      <div class="col-md-4 col-6 align-self-center">
                          <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                              <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                 <!-- <div class="chart-text m-r-10">
                                        <div class='form-material input-group'>
                                    
                                            
                                          
                              </div></div> -->

                                      
                              </div>
                              <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                 <input type='text' class="form-control dateranges" style="datepicker" />
                                  <div class="input-group-append">
                                      <span class="input-group-text">
                                              <span class="ti-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                          
                          </div>
                      </div>




                  </div>
    </header>
               
   <div class="row" >

   <div class="col-lg-12" id="outer_div">
                       
                     
                               <div class="tab-content">
                                <div class="tab-pane active" id="dashboard" role="tabpanel">
                            
                            <div class="row" style="margin-right:-10px;margin-left:-10px">
                                <div class="col-xlg-12 col-lg-12 col-md-12">
                                      <div class="card" style="height:425px">
                           <div class="card-header">
                            <span class="text-red" style="font-weight: 500;display: none">Like Count : </span>
                            <span class="text-red" style="font-weight: 500;display: none" id="last_like_count"></span>
                                <div class="card-actions">
                              <div class="btn-group" data-toggle="buttons">
                                               <label class="btn btn-secondary active">
                                                <input type="radio" name="options" id="option2" value="page" autocomplete="off"> Page Like
                                               
                                            </label>
                                            <label class="btn btn-secondary">
                                                <input type="radio" name="options" id="option3" value="sentiment" autocomplete="off"> Sentiment
                                            </label>
                                     
                                        </div>
                                </div>
                                <h4 class="card-title m-b-0"></h4>

                            </div>
                            <div class="card-body b-t collapse show">
                               <div style="display:none"  align="center" style="vertical-align: top;" id="fan-growth-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="fan-growth-chart" style="width:100%;height:350px"></div>
                          </div>
                        </div>
                          </div>
                        
                      </div>
                      <div class="row" style="margin-right:-10px;margin-left:-10px">
                       
                         <div class="col-lg-3">
                          <div class="card">
                       
                                <div class="card-header">
                              
                                <h4 class="card-title m-b-0">Most Frequent Post Topics</h4>
                                 
                            </div>
                            <div class="card-body b-t collapse show " style="padding:0.5rem !important">
                                   <div style="display:none"  align="center" style="vertical-align: top;" id="post-tag-count-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                   <div class="post_tag_count"> </div>
                           </div>
                        </div>
                        </div>
                              <div class="col-lg-3">
                          <div class="card">
                       
                                <div class="card-header">
                              
                                <h4 class="card-title m-b-0">Posts</h4>
                            </div>
                            <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                                             <div style="display:none"  align="center" id="post_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    <div class="card-body text-center " style="padding:0.5rem !important">
                                        <h1 class="card-title m-t-10" style="font-size:65px" id="post_total">-</h1>
                                        
                                    </div>
                                    <div class="card-body text-center ">
                                      
                                        <ul class="list-inline m-b-0">
                                            <li>
                                                <h6 id="post_neg"class="text-red" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnPost" value="neg">Negative</button></div> </li>
                                           <li>
                                                 <h6 id="post_neutral" class="text-warning" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnPost" value="neutral">Neutral</button></div> </li>
                                            <li>
                                                 <h6 id="post_pos" class="text-success" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnPost" value="pos">Positive</button></div> </li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                          
                        </div>
                         <div class="col-lg-3">
                          <div class="card">
                       
                                <div class="card-header">
                              
                                <h4 class="card-title m-b-0">Most Frequent Comment Topics</h4>
                                 
                            </div>
                            <div class="card-body b-t collapse show " style="padding:0.5rem !important">
                                   <div style="display:none"  align="center" style="vertical-align: top;" id="tag-count-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                   <div class="tag_count"> </div>
                           </div>
                        </div>
                        </div>
                         <div class="col-lg-3">
                           <div class="card">
                                <div class="card-header">
                             
                                <h4 class="card-title m-b-0">Comments</h4>
                            </div>
                            <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                                             <div style="display:none"  align="center" id="cmt_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    <div class="card-body text-center " style="padding:0.5rem !important">
                                        <h1 class="card-title m-t-10" style="font-size:65px" id="cmt_total">-</h1>
                                        
                                    </div>
                                    <div class="card-body text-center ">
                                      
                                        <ul class="list-inline m-b-0">
                                            <li>
                                                <h6 id="cmt_neg"class="text-red" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnComment" value="neg">Negative</button></div> </li>
                                            <li>
                                                 <h6 id="cmt_neutral" class="text-warning" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnComment" value="neutral">Neutral</button></div> </li>
                                            <li>
                                                 <h6 id="cmt_pos" class="text-success" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnComment" value="pos">Positive</button></div> </li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                          
                        </div>
                         
                      </div>
   <div class="row" style="margin-right:-10px;margin-left:-10px">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-2 col-6 align-self-center" style="padding-left:1.25rem">
              <h4 class="card-title m-b-0 text-info">Comment Topics</h4>
            </div>
            <div class="col-md-10 col-6" style="text-align:right;padding-right:3rem">
                  Qty <input type="checkbox" id="tag-senti-qty-type"  class="js-switch" data-color="#26c6da" data-secondary-color="#f62d51" /> Percent
            </div>
          </div>
          <div class="row" style="padding: 4px 0px;margin-top:10px;">
            <div class="col-md-6 col-6 align-self-center" style="padding-left:1.25rem">
              <select class="form-control" id="tagGroup_filter">
              <option value="">All Tags</option>
              @if (isset($tagGroup) && count($tagGroup)>0)
              @foreach($tagGroup as $tagGroup)
              <option value="{{$tagGroup->id}}">{{$tagGroup->name}}</option>
              @endforeach
              @endif
              </select>
            </div>
            <div class="col-md-6 col-6 " style="text-align:right;padding-right:3rem">
               <select class="form-control edit-tag" id="tag_filter">
              @if (isset($tags) && count($tags)>0)
              @foreach($tags as $tag)
              <option value="{{$tag->id}}">{{$tag->name}}</option>
              @endforeach
              @endif
              </select>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div style="display:none"  align="center" style="vertical-align: top;" id="tag_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
         <!--  <div id="tag-qty-line-chart"  style="width:50%;height:300px;float:left"></div> -->
          <div id="tag-percent-chart"  style="width:100%;height:300px;display:none" ></div>  
          <div id="tag-sentiment-chart"  style="width:100%;height:300px;" ></div> 
          </div>
        </div>
      </div>
    </div>
                       <div class="row" style="margin-right:-10px;margin-left:-10px">
                                <div class="col-xlg-12 col-lg-12 col-md-12">
                            <div class="card" >
                           <div class="card-header">
                           
                                <div class="row">
                                <div class="col-md-8 col-4 align-self-center" style="padding-left:1.25rem">
                                <h4 class="card-title m-b-0">Comment Topics Sentiment</h4>
                                </div>
                             

                                </div>
                            </div>
                            <div class="card-body b-t collapse show">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="tag-senti-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="tag-senti-chart" style="width:100%;height:320px"></div>
                          </div>
                        </div>
                          </div>
                      </div>
                        <div class="row" style="margin-right:-10px;margin-left:-10px">
                        <!-- <div class="col-lg-12" style="visibility: collapse;height: 0px;"> -->
                        	<div class="col-lg-12">
                       
                        <div class="card" >
                           <div class="card-header">
                                <h4 class="card-title m-b-0">Highlighted Voice</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="hightlighted-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="hightlighted" style="width:100%;height:300px"></div>
                          </div>
                        </div>
                        </div>
                    </div>
                    <div class="row" style="margin-right:-10px;margin-left:-10px;display: none">
                        <!-- <div class="col-lg-12" style="visibility: collapse;height: 0px;"> -->
                          <div class="col-lg-12">
                       
                        <div class="card" >
                           <div class="card-header">
                                <h4 class="card-title m-b-0">Frequent Comment</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="frequent-comment-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="frequent_comment_chart" style="width:100%;height:300px"></div>
                          </div>
                        </div>
                        </div>
                    </div>
                      <div class="row" style="margin-right:-10px;margin-left:-10px">
                      <div class="col-lg-12">
                      <div class="card" >
                       <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                           <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#top_post" role="tab">Top Posts</a> </li>
                           <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#top_comment" role="tab">Top Comments</a> </li>
                       
                        </ul>
                        <div class="card-body b-t collapse show">
                        <div class="tab-content">
                        <div class="tab-pane active" id="top_post" role="tabpanel">
                        <div class="row" >
                          <div class="col-lg-6" style="padding: 20px">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="top-post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           <div class="card-body  collapse show top_post_div"  style="padding:0.1rem">
                               
                           </div>
                            </div>
                                <div class="col-lg-6" style="padding: 20px">
                                <div class="card-body collapse show top_post_div_right"  style="padding:0.1rem">
                        

                                </div>
                            </div>
                           </div>
                         </div>
                         <div class="tab-pane" id="top_comment" role="tabpanel">
                        <div class="row" >
                    
                            <div class="col-lg-6" style="padding: 20px">
                              <div style="display:none"  align="center" style="vertical-align: top;" id="top-comment-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <div class="card-body  collapse show top_comment_div"  style="padding:0.1rem">
                               

                                </div>
                            </div>
                                <div class="col-lg-6" style="padding: 20px">
                                <div class="card-body collapse show top_comment_div_right"  style="padding:0.1rem">
                        

                                </div>
                            </div>
                           </div>
                         </div>
                       </div>
                     </div>
                       
                        </div>
                          </div>
                        
                      </div>

                        
               
                 </div>
                       
                        <div class="tab-pane" id="post" role="tabpanel">
                                <div class="card">
                                       <div class="card-body b-t collapse show" style="padding-top:0px">
                              <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div class="table-responsive">
                                <table id="tbl_post"  class="table" style="margin-top:4px;" >
                                <thead>
                                  <tr>
                                    <th></th>
                                    <th>Reaction</th>
                                    <th>Comments</th>
                                    <th>Shares</th>
                                    <!-- <th>Overall</th> -->
                                  </tr>
                                </thead>

                              </table>
                              </div>
                            </div>
                          </div>
               
                  </div>
                  <div class="tab-pane" id="visitorpost" role="tabpanel">
                                <div class="card">
                                       <div class="card-body b-t collapse show" style="padding-top:0px">
                              <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div class="table-responsive">
                                <table id="tbl_visitorpost"  class="table" style="margin-top:4px;" >
                                <thead>
                                  <tr>
                                    <th></th>
                                    <th>Reaction</th>
                                    <th>Comments</th>
                                    <th>Shares</th>
                                    <!-- <th>Overall</th> -->
                                  </tr>
                                </thead>

                              </table>
                              </div>
                            </div>
                          </div>
               
                  </div>
                   <div class="tab-pane" id="comment" role="tabpanel">
                       
                         <div class="card">
                            <div class="card-body">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div class="table-responsive">
                               <table id="tbl_comment" class="table" style="width:100%;">
                              <thead>
                                <tr>
                                  <th></th>
                                  <th>Reaction</th>
                                
                                </tr>
                              </thead>

                            </table>
                            </div>
                          </div>
                   
                        </div>
                   
                   
                  </div>
                     
                  </div>
                        
                    </div>

                     <div class="col-lg-3" id="filter_div" style="display: none">
                       <div class="card earning-widget" style="height:230px">
                              <div class="card-header">
                       
                                  <h4 class="card-title m-b-0">Sentiment</h4>
                              </div>
                              <div class="card-body b-t collapse show">
                            
                                   <div class="form-group">
                                                    
                                                      <div class="input-group">
                                                          <ul class="icheck-list sentiment-list" style="padding-top:0px">
                                                          <li>
                                   <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="all" id="senti-all"> 
                                    <label  for="senti-all" style="padding-left:30px;font-weight:500;font-size:12px">all</label>
                                    </li>
                                     <li>
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="pos" id="senti-pos"> 
                                     <label class="text-success" for="senti-pos" style="padding-left:30px;font-weight:500;font-size:12px">positive</label>
                                    </li>
                                    <li>
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neg" id="senti-neg"> 
                                    <label class="text-danger" for="senti-neg" style="padding-left:30px;font-weight:500;font-size:12px">negative</label>
                                    </li>
                                    <li> 
                                    <input type="checkbox" class="check sentimentlist" name="sentimentlist" value="neutral" id="senti-neutral"> 
                                    <label class="text-info"   for="senti-neutral" style="padding-left:30px;font-weight:500;font-size:12px">neutral</label>
                                    </li>
                                                             
                                                          </ul>
                                                      </div>
                                                  </div>
                              </div>
                          </div>
                      <div class="card earning-widget" style="height:550px">
                        <div class="card-header">
                          <h4 class="card-title m-b-0">Tags <span style="float: right;">
                          <span  class="mdi mdi-sort-descending" id="nameSort" style="color:#4267b2"></span>Name
                          <span  class="mdi mdi-sort-descending" id="countSort" style="color:#4267b2"></span>Count
                          </span></h4>
                          
                        </div>
                      <div class="card-body b-t collapse show" style="overflow-y:scroll;height:550px">
                        <div class="form-group">
                          <div class="input-group">
                              <ul class="icheck-list tag-list" style="padding-top:0px">
                              </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                
                  
                </div>
                    </div>
                    <div id="show-image" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div align="center" class="modal-body large_img">
                                                 
                                              </div>

                                          </div>
                                        
                                      </div>
                                
                   </div>
                  <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <h4 class="modal-title" id="postModal">Post Detail</h4>
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div class="modal-body post_data">
                                                 
                                              </div>
                                                <div class="modal-footer">
                                                  <button type="button" id="btnSeeComment" class="btn btn-green waves-effect text-left" data-dismiss="modal">See Comments</button>
                                              </div>
                                          </div>
                                          <!-- /.modal-content -->
                                      </div>
                                      <!-- /.modal-dialog -->
                   </div>
                                     <div id="show-reply-cmt" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                      <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <h4 class="modal-title" id="postModal">Replies</h4>
                                                   <div id="rp-snackbar"><div id="rp_snack_desc">A notification message..</div></div>
                                                   <div id="rptag-snackbar"><div id="rptag_snack_desc">A notification message..</div></div>

                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div class="modal-body reply_data">
                                                 
                                              </div>
                                             <!--    <div class="modal-footer">
                                                  <button type="button" id="btnSeeComment" class="btn btn-green waves-effect text-left" data-dismiss="modal">See Comments</button>
                                              </div> -->
                                          </div>
                                          <!-- /.modal-content -->
                                      </div>
                                      <!-- /.modal-dialog -->
                   </div>
                 <div id="show-add-tag" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tag_title" aria-hidden="true" style="display: none;">
                          <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title" id="tag_title">Add New Tag</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  </div>
                                  <div class="modal-body add-tag">
                                              
                          <div class="form-group">
                                 <input type="hidden" id="brand_id" name="brand_id" value="{{ $brand_id }}" >
                                 <label for="name" class="control-label">{{ __('Name') }}</label>
                                 <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                 
                                <span class="invalid-name" role="alert">
                                  
                                </span>
                               
                          </div>
      
                          <div class="form-group">
                              <button type="button" id="add_new_tag" class="btn btn-primary">  {{ __('Save') }}</button>
                            </div>
        
                      </div>
                                
                    </div>
                                     
                </div>
                          
          </div>
           <div id="show-add-campaign" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tag_title" aria-hidden="true" style="display: none;">
                          <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title" id="tag_title">Add Campaign</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="form-group">
                                        <label>Input Select</label>
                                        <select class="form-control add-campaign">
                                            
                                        </select>
                                    </div>
                                   <div class="form-group">
                                    <button type="button" id="add_campaign" class="btn btn-primary">Add</button>
                                  </div>
                             </div>
                          </div>
                          
          </div>
  @endsection
  @push('scripts')
  <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
      <!-- slimscrollbar scrollbar JavaScript -->
      <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
      <!--Wave Effects -->
      <script src="{{asset('js/waves.js')}}" defer></script>
      <!--Menu sidebar -->
      <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
      <!--stickey kit -->
      <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
      <!--Custom JavaScript -->
      <script src="{{asset('js/custom.min.js')}}" defer></script>
      <!-- ============================================================== -->
      <!-- This page plugins -->
      <!-- ============================================================== -->
     
     <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
       <script src="{{asset('js/morris-data.js')}}" ></script>-->
      
      <!-- Chart JS -->
      <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
      <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
      <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
      <!-- Flot Charts JavaScript -->
      <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
      
   <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
      <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

      <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
          <!-- Date range Plugin JavaScript -->
      <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
      <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
     <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
     <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
      <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" defer></script>
     <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
     <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/icheck/icheck.min.js')}}"  defer></script>
   <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}" defer></script>

<!--    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script> -->
  <!-- <script src="{{asset('assets/plugins/materialize/js/materialize.min.js')}}" defer></script> -->
   
     <script>
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myHeader");
  var myHeaderContent = document.getElementById("myHeaderContent");
  // var chipcompetitor = document.getElementById("chip-competitor");
  var tabHeader = document.getElementById("tabHeader");
  var tabHeader_sticky = tabHeader.offsetTop;

  var sticky = header.offsetTop;

  function myFunction() {//alert("ho");
    if (window.pageYOffset > sticky) {
      header.classList.add("s-topbar");
      header.classList.add("s-topbar-fix");
      myHeaderContent.classList.add("myHeaderContent");
      // chipcompetitor.classList.add("myHeaderScoll");
    } else {
      header.classList.remove("s-topbar");
      header.classList.remove("s-topbar-fix");
      myHeaderContent.classList.remove("myHeaderContent");
      // chipcompetitor.classList.remove("myHeaderScoll");
    }

    //  if (window.pageYOffset > tabHeader_sticky) {//alert("hi");
    //   tabHeader.classList.add("tabcontrolHeader");
    
    // } else {
    //     tabHeader.classList.remove("tabcontrolHeader");
    // }
  }
  </script>
   
  <script type="text/javascript">
  var startDate;
  var endDate;
  var label;
  /*global mention*/
  var mention_total;
  var mentionLabel = [];
  var mentions = [];
  /*Bookmark Array*/
  var bookmark_array=[];
  var bookmark_remove_array=[];
  /*global sentiment*/
  var positive = [];
  var negative = [];
  var sentimentLabel = [];
  var positive_total=0;
  var negative_total=0;
  var campaignGroup=[];
 var  colors=["#1e88e5","#dc3545","#28a745","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];
    var colors_senti=["#28a745","#fb3a3a","#ffb22b"];

  /*var effectIndex = 2;
  var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

  $(document).ready(function() {


    // var brand_id = $('#hidden-user').val();

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

        var changeCheckbox_tag = document.querySelector('#tag-senti-qty-type');

        changeCheckbox_tag.onchange = function() {
          if(changeCheckbox_tag.checked == true) // qty
          {
            $('#tag-sentiment-chart').hide();
            $('#tag-percent-chart').show();
           
           
          }
          else //sentiment
          {
            $('#tag-sentiment-chart').show();
            $('#tag-percent-chart').hide();
          }
          window.dispatchEvent(new Event('resize'));
        };  


      //initialize
   $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
  /* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

   $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

        var GetURLParameter = function GetURLParameter(sParam) {
      var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : sParameterName[1];
          }
      }
  };

      startDate = moment().subtract(1, 'month');
      // alert(startDate);
      endDate = moment();
      label = 'month';

             $('.singledate').daterangepicker({
              singleDatePicker: true,
              showDropdowns: true,
              locale: {
                  format: 'DD/MM/YYYY'
              }
          },function(date) {
            // alert(date);
            endDate=date;
            var id =$('input[type=radio][name=period-radio]:checked').attr('id');
            if(id==="period-week")
           {
             startDate = moment(endDate).subtract(1, 'week');
             endDate = endDate;
           }
           else
           {
             startDate = moment(endDate).subtract(1, 'month');
             endDate = endDate;
           }
          
           var admin_page=$('.hidden-chip').val();
            // alert(admin_page);     
            // alert('helo');
             var kw_search = $("#kw_search").val();
             // alert(kw_search);
             // alert(startDate);
           ChooseDate(startDate,endDate,admin_page,'','',kw_search);
         
        });

             $('#kw_search').keyup(function () { 
               var kw_search = $(this).val();

               // alert(kw_search);
            $('.dateranges').val(startDate.format('MMM D, YYYY') + ' - ' + endDate.format('MMM D, YYYY'));
            startDate=startDate;
            endDate=endDate;
            // alert(startDate);
            filter_tag = '';
            var graph_option = $('.btn-group > .btn.active').text();
          
           var date_preset = Calculate_DatePreset(label);
           getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag,kw_search);
           getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           getAllVisitorPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);

            if (graph_option.trim()== 'Sentiment') {
              InboundSentimentDetail(GetStartDate(),GetEndDate());
            }
            // else if (graph_option.trim() == 'Page Like')
            // {
            //     PageGrowth(GetStartDate(),GetEndDate(),date_preset);
            // }tartDate=startDate;
            endDate=endDate;
               

               // ChooseDate(startDate,endDate,'','',kw_search)
            });

      var kw_search = $("#kw_search").val();
      // alert(kw_search);



   function hidden_div()
  {//alert("popular");
      // var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
    // var brand_id = 22;
      $( "#popular-spin" ).show();
      $("#popular").empty();
    $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('gethiddendiv')}}", // This is the URL to the API
        data: { view_name:'Dashboard'}
      })
      .done(function( data ) {//$("#popular").html('');
       for(var i in data) {
        $("#"+data[i].div_name).hide();
       }

      })
      .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
        // If there is no communication between the server, show an error
       // alert( "error occured" );
      });

  }

  function getTopPost()
  {
      var brand_id = GetURLParameter('pid');
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
      var fday=GetStartDate();
      var sday=GetEndDate();
      
      $(".top_post_div").empty();
      $(".top_post_div_right").empty();
      $("#top-post-spin").show();
      
    $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('getTopAndLatestPost')}}", // This is the URL to the API
        data: { fday: fday,
                sday:sday,
                brand_id:brand_id,
                admin_page:admin_page,
                admin_page_id:admin_page_id,
                limit:'6',
                format_type:'top_post'
              }
      })
      .done(function( data ) {
      // var data = Array.from(Object.keys(data), k=>data[k]);
        //console.log(data);
      //  data = data.reverse();
     console.log("DATA"+JSON.stringify(data));
       $("#top-post-spin").hide();
      
      for (var i in data) {
         var html="";
        if(data[i]['visitor']==1)
        {
          var visitor_text= '<p class="text-danger">This is visitor post!</p>';
        }
        {
          var visitor_text ='';
        }

        
        html +='  <div class="postPanel_container cf  postPanel_withPicture "><div class="postPanel_contentContainer"> ' +
                '<div id="idb4b" style="display:none"></div> '+
                '<div class="postPanel_header"> '+
                '<div class="postPanel_userImageContainer" id="idb4c">'+
                
                 '<img src="'+data[i]['imgurl']+'""/>'+
                '</div><a target="_blank" class="postPanel_linkToPage" href="http://www.facebook.com/'+data[i]['page_id']+'">'+data[i]['page_name']+'</a>'+
                '<span> shared a </span>'+
                '<a target="_blank" href="http://www.facebook.com/'+data[i]['id']+'"> post </a>'+
                '<span></span><span> - </span><a target="_blank" class="postPanel_date" href="http://www.facebook.com/'+data[i]['id']+'">'+data[i]['created_time']+'</a>'+
                '</div>'+
                '<div class="postPanel_textContainer">'+visitor_text+'<p>'+data[i]['message']+'</p></div>';
                if(data[i]['post_type'] =='photo' )
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img id="img_'+data[i]['id']+'" class="top_popup_img postPanel_previewImage" style="width:50%;height:150px" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {

                   // alert(data[i]['link']);
                  //   html += '<a target="" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  // '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  // '<iframe src="https://www.facebook.com/v2.3/plugins/video.php?href='+data[i]['link']+'&width=400&show_text=false&locale=en_US&sdk=joey" frameborder="0" allowfullscreen></iframe>'+
                  // '</span><span class="postPanel_nameTextContainer"></span></a>';
                  
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=400&show_text=false&height=281&appId" frameborder="0" allow="encrypted-media"  allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
         html += '</div>' ;
         html += '<div class="postPanel_dataContainer"style="background-color:#fff" id="idb4e">'+
                   '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Total reactions"></span><span>Total reactions: '+numberWithCommas(data[i]['total_react'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb5d"><span class="postPanel_icon mdi mdi-comment-outline" title="Comments"></span><span>'+numberWithCommas(data[i]['comment_count'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb5e"><span class="postPanel_icon mdi mdi-share-variant" title="Shares"></span><span>'+numberWithCommas(data[i]['shared'])+'</span></span>'+
                 
                  '</div>';
          html += '<div class="postPanel_dataContainer" id="idb4e">'+
               
                  '<span class="postPanel_actionIconContainer" id="idb52"><span class="postPanel_icon mdi mdi-thumb-up-outline" title="Likes"></span><span>'+numberWithCommas(data[i]['liked'])+'</span></span>'+
                  '<span id="idb53" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb54"><span class="postPanel_icon  mdi mdi-heart-outline" title="Love"></span><span>'+numberWithCommas(data[i]['loved'])+'</span></span>'+
                  '<span id="idb55" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb56"><span class="postPanel_icon icon-post-haha" title="Haha" >😆</span><span>'+numberWithCommas(data[i]['haha'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb57"><span class="postPanel_icon icon-post-wow" title="Wow">😮</span><span>'+numberWithCommas(data[i]['wow'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb58"><span class="postPanel_icon icon-post-sad" title="Sad">😪</span><span>'+numberWithCommas(data[i]['sad'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Angry">😡</span><span>'+numberWithCommas(data[i]['angry'])+'</span></span>'+
                                
                  '</div>';
          html += ' </div>';
                 
       if(i % 2 == 0)
         $(".top_post_div").append(html);
       else
         $(".top_post_div_right").append(html);
      
       
         
       }
      
      
       
//        Element.prototype.appendAfter = function (element) {
//   element.parentNode.insertBefore(this, element.nextSibling);
// },false;
     

      })
      .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
        
      });

  }
  function getTopComment()
  {
      var brand_id = GetURLParameter('pid');
      var fday=GetStartDate();
      var sday=GetEndDate();
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
      
      $(".top_comment_div").empty();
      $(".top_comment_div_right").empty();
     // $("#top-comment-spin").show();
      
    $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('getTopComment')}}", // This is the URL to the API
        data: { fday: fday,
                sday:sday,
                brand_id:brand_id,
                admin_page:admin_page,
                admin_page_id:admin_page_id,
                limit:'6',
                
              }
      })
      .done(function( data ) {
      // var data = Array.from(Object.keys(data), k=>data[k]);
      //  console.log(data);
      //  data = data.reverse();
      // $("#top-comment-spin").hide();
    
      for (var i in data) {

           var html="";var ProfileName='';var profilePhoto='';var profileLink='';
             if(data[i]['profileName'] !== '' && data[i]['profileName'] !== null) 
              ProfileName=data[i]['profileName']; 
             else if(data[i]['profileID'] !== '' && data[i]['profileName'] !== null)  
              ProfileName=data[i]['profileID'];
             else  ProfileName=data[i]['page_name'];
             
              if(data[i]['profileType'] === 'number')
              {
                profilePhoto='https://graph.facebook.com/'+data[i]['profileID']+'/picture?type=large';
                ProfileName=data[i]['page_name'];
              }
              
              if(data[i]['profileID']!=='undefined'  && data[i]['profileName'] !== null)
              profileLink='https://www.facebook.com/'+data[i]['profileID'];
               if(profilePhoto=='')
               {
                profilePhoto ='assets/images/unknown_photo.jpg';
               }
              
        html +='  <div class="postPanel_container cf  postPanel_withPicture "><div class="postPanel_contentContainer"> ' +
                '<div id="idb4b" style="display:none"></div> '+
                '<div class="postPanel_header"> '+
                '<div class="postPanel_userImageContainer" id="idb4c">'+
                '<img src="'+profilePhoto+'">'+
                '</div><a target="_blank" class="postPanel_linkToPage" href="'+profileLink+'">'+ProfileName+'</a>'+
                '<span></span><span> - </span><a target="_blank" class="postPanel_date" href="http://www.facebook.com/'+data[i]['id']+'">'+data[i]['created_time']+'</a>'+
                '</div>'+
                '<div class="postPanel_textContainer"><p>'+data[i]['message']+'</p></div>';
                if(data[i]['cmtType'] =='video_inline' || data[i]['cmtType'] =='sticker' || data[i]['cmtType'] =='photo' || data[i]['cmtType'] =='share' || data[i]['cmtType'] =='share_large_image' || data[i]['cmtType'] =='animated_image_share'  )
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img id="img_'+data[i]['id']+'" class="top_cmt_popup_img postPanel_previewImage" style="width:20%;height:50%" src="'+data[i]['cmtLink']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['cmtType'] =='animated_image_video' || data[i]['cmtType'] =='video_share_youtube' || data[i]['cmtType'] =='video_share_highlighted')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['cmtLink']+'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
         html += '</div>' ;
         html += '<div class="postPanel_dataContainer"style="background-color:#fff" id="idb4e">'+
                   '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Total reactions"></span><span>Total reactions: '+numberWithCommas(data[i]['total'])+'</span></span>'+
                   '<span class="postPanel_actionIconContainer" id="idb5d"><span class="postPanel_icon mdi mdi-comment-outline" title="Comments"></span><span>0</span></span>'+
                                 
                  '</div>';
          html += '<div class="postPanel_dataContainer" id="idb4e">'+
               
                  '<span class="postPanel_actionIconContainer" id="idb52"><span class="postPanel_icon mdi mdi-thumb-up-outline" title="Likes"></span><span>'+numberWithCommas(data[i]['liked'])+'</span></span>'+
                  '<span id="idb53" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb54"><span class="postPanel_icon  mdi mdi-heart-outline" title="Love"></span><span>'+numberWithCommas(data[i]['loved'])+'</span></span>'+
                  '<span id="idb55" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb56"><span class="postPanel_icon icon-post-haha" title="Haha" >😆</span><span>'+numberWithCommas(data[i]['haha'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb57"><span class="postPanel_icon icon-post-wow" title="Wow">😮</span><span>'+numberWithCommas(data[i]['wow'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb58"><span class="postPanel_icon icon-post-sad" title="Sad">😪</span><span>'+numberWithCommas(data[i]['sad'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Angry">😡</span><span>'+numberWithCommas(data[i]['angry'])+'</span></span>'+
                                
                  '</div>';
          html += ' </div>';
                 
       if(i % 2 == 0)
        $(".top_comment_div").append(html);
       else
        $(".top_comment_div_right").append(html);
       
       
         
       }
  

      })
      .fail(function(xhr, textStatus, error) {

      });

  }
    function frequent_comment(fday,sday)
{
        let main = document.getElementById("frequent_comment_chart");
        let existInstance = echarts.getInstanceByDom(main);
            if (existInstance) {
                if (true) {
                    echarts.init(main).dispose();
                }
            }

        var chart = echarts.init(main);
        var brand_id = GetURLParameter('pid');
        var admin_page_id=$('.hidden-chip').attr('id');
        var admin_page=$('.hidden-chip').val();
       $( "#frequent-comment-spin" ).show();

       //$("#highlighted").empty();
      $.ajax({
          type: "GET",
          dataType:'json',
          contentType: "application/json",
          url: "{{route('getKeyword')}}", // This is the URL to the API
         
          data: { fday: fday,sday:sday,type:"all",brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id}
        })
        .done(function( data ) { console.log("frequent_comment".fontcolor('red')+JSON.stringify(data));
            
      $( "#frequent-comment-spin" ).hide();
         
          if(data.length>0)
          {
         //var data =JSON.parse(data);
         var data = genData(data);
         //console.log(data);
        
         var cmtObj=data.commentID;
        
        
         chart.clear();
         option=null;

           var option = {
            textStyle: {
                        
                        fontFamily: 'mm3',},
                    tooltip: { 
                         textStyle: {
                        
                                fontFamily: 'mm3',}
                            
                        },
                    series: [ {
                        type: 'wordCloud',
                           left:'center',
                            top:'center',
                            width: '70%',
                            height: '80%',
                            right:null,
                            bottom:null,
                        sizeRange: [12, 50],
                        rotationRange: [0,0,0,0],
                        // rotationStep: 90,
                        shape: 'ratangle',//pentagon ratangle
                        gridSize:8,
                        // height: 500,
                        drawOutOfBound: false,
                        textStyle: {
                            normal: {
                                fontFamily: 'mm3',
                                color: function () {
                                    var lightcolor = 'rgb(' + [
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        2
                                    ].join(',') + ')';
                                    console.log(lightcolor);
                                    return lightcolor;
                                   /* return colors[0];*/

                                }
                            },
                           
                            emphasis: {
                                shadowBlur: 10,
                                shadowColor: '#333'
                            }
                        },
                        data: data.seriesData
                    }
                    ]
                        
                };
        function genData(data) {
        var seriesData = [];
      
        var comment_array={};
        var i = 0;
         for(var key in data) 
            {
             var name =data[key].topic_name;
             // alert(name);
             var value=data[key].weight;
             var arr_cmt=data[key].comment_id;
             var kvalue=parseFloat(value);
           
              comment_array[name]=arr_cmt;
              seriesData.push({name: name,value:value});
            
              
            }


      /* jsonString=jsonString.split(["[","]"])*/
         return {
            
            seriesData: seriesData,
            commentID:comment_array,
           
            
        };

        
    };





              chart.setOption(option,true), $(function() {
        function resize() {
            setTimeout(function() {
                chart.resize()
            }, 100)
        }
    }
    );
        chart.on('click', function (params) {//alert("hhi");
  // var admin_page = $('#hidden-chip').val();
  var default_page = $('.hidden-pg').val();
  var admin_page_id=$('.hidden-chip').attr('id');
  var admin_page=$('.hidden-chip').val();
   
  var cmt_arr=cmtObj[params.name];
   
              $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("session-cmtID") }}',
             type: 'POST',
             data: {highlight_text:params.name,wordcloud_text:cmt_arr},
             success: function(response) {
              var accountPermission = GetURLParameter('accountPermission');

                // window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id+"&highlight_text="+ params.name+"&source="+GetURLParameter('source')+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&wordcloud_text="+params.name+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page, '_blank'); 
                 window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id+"&highlight_text="+ params.name+"&source="+GetURLParameter('source')+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission, '_blank'); 
            }
                });

       
       
    });

          }
          


        })
        .fail(function(xhr, textStatus, error) {
           console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
          // If there is no communication between the server, show an error
         // alert( "error occured" );
        });

}
  function highlighted(fday,sday)
{
        let main = document.getElementById("hightlighted");
        let existInstance = echarts.getInstanceByDom(main);
            if (existInstance) {
                if (true) {
                    echarts.init(main).dispose();
                }
            }

        var chart = echarts.init(main);
        var brand_id = GetURLParameter('pid');
        var admin_page_id=$('.hidden-chip').attr('id');
        var admin_page=$('.hidden-chip').val();
       $( "#hightlighted-spin" ).show();

      $.ajax({
          type: "GET",
          dataType:'json',
          contentType: "application/json",
          url: "{{route('getHighlighted_frequent')}}", // This is the URL to the API
         
          data: { fday: fday,sday:sday,type:"all",brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id}
        })
        .done(function( data ) { 
          
          console.log("%c JinCloud"+JSON.stringify(data),'background: #222; color: #bada55');
            
    	$( "#hightlighted-spin" ).hide();
         
          if(data.length>0)
          {
         //var data =JSON.parse(data);
         var data = genData(data);
         //console.log(data);
        
         var cmtObj=data.commentID;
        
        
         chart.clear();
         option=null;

           var option = {
            textStyle: {
                        
                        fontFamily: 'mm3',},
                    tooltip: { 
                         textStyle: {
                        
                                fontFamily: 'mm3',}
                            
                        },
                    series: [ {
                        type: 'wordCloud',
                           left:'center',
                            top:'center',
                            width: '70%',
                            height: '80%',
                            right:null,
                            bottom:null,
                        sizeRange: [12, 50],
                        rotationRange: [0,0,0,0],
                        // rotationStep: 90,
                        shape: 'ratangle',//pentagon ratangle
                        gridSize:8,
                        // height: 500,
                        drawOutOfBound: false,
                        textStyle: {
                            normal: {
                                fontFamily: 'mm3',
                                color: function () {
                                    var lightcolor = 'rgb(' + [
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        2
                                    ].join(',') + ')';
                                    console.log(lightcolor);
                                    return lightcolor;
                                   /* return colors[0];*/

                                }
                            },
                           
                            emphasis: {
                                shadowBlur: 10,
                                shadowColor: '#333'
                            }
                        },
                        data: data.seriesData
                    }
                    ]
                        
                };
        function genData(data) {
        var seriesData = [];
      
        var comment_array={};
        var i = 0;
         for(var key in data) 
            {
             var name =data[key].topic_name;
             // alert(name);
             var value=data[key].weight;
             var arr_cmt=data[key].comment_id;
             var kvalue=parseFloat(value);
           
              comment_array[name]=arr_cmt;
              seriesData.push({name: name,value:value});
            
              
            }


      /* jsonString=jsonString.split(["[","]"])*/
         return {
            
            seriesData: seriesData,
            commentID:comment_array,
           
            
        };

        
    };





              chart.setOption(option,true), $(function() {
        function resize() {
            setTimeout(function() {
                chart.resize()
            }, 100)
        }
    }
    );
        chart.on('click', function (params) {//alert("hhi");
	// var admin_page = $('#hidden-chip').val();
	var default_page = $('.hidden-pg').val();
  var admin_page_id=$('.hidden-chip').attr('id');
  var admin_page=$('.hidden-chip').val();
   
  var cmt_arr=cmtObj[params.name];
   
              $.ajax({
             headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
             url:'{{ route("session-cmtID") }}',
             type: 'POST',
             data: {highlight_text:params.name,wordcloud_text:cmt_arr},
             success: function(response) {
                var accountPermission = GetURLParameter('accountPermission');

                // window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id+"&highlight_text="+ params.name+"&source="+GetURLParameter('source')+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&wordcloud_text="+params.name+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page, '_blank'); 
                 window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id+"&highlight_text="+ params.name+"&source="+GetURLParameter('source')+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission, '_blank'); 
            }
                });

       
       
    });

          }
          


        })
        .fail(function(xhr, textStatus, error) {
           console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
          // If there is no communication between the server, show an error
         // alert( "error occured" );
        });

}
function Calculate_DatePreset(label)
{
         var date_preset='this_month';
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
          if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

        return date_preset;
}

     function ChooseDate(start, end,label='',filter_tag='',kw_search) {//alert(start);
            // alert(kw_search);
            $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate=start;
            endDate=end;
             var graph_option = $('.btn-group > .btn.active').text();
            
           var date_preset = Calculate_DatePreset(label);
           getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag,kw_search);
           getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           getAllVisitorPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           posting_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           CMTtagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           MostFrequentlyTag(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           MostFrequentlyPostTag(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           highlighted(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           frequent_comment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           getCampaignGroup();
           getTopPost();
           getTopComment();
    if (graph_option.trim()== 'Sentiment') {
        InboundSentimentDetail(GetStartDate(),GetEndDate());
    }
    else if (graph_option.trim() == 'Page Like')
    {
        PageGrowth(GetStartDate(),GetEndDate(),date_preset);
    }
          }

          function GetStartDate()
  {
     //alert(startDate);
   return startDate.format('YYYY-MM-DD');
  }
  function GetEndDate()
  {
             // alert(endDate);
  return endDate.format('YYYY-MM-DD');
  }
  function getAllPost(fday,sday,kw_search)
      {
        $(".popup_post").unbind('click');
        $(".see_comment").unbind('click');
        // var brand_id = $('#hidden-user').val();
        var brand_id = GetURLParameter('pid');
   
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
 
         var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
            // alert(admin_page);
   
         document.getElementById('select_page').innerHTML =admin_page;



   var oTable = $('#tbl_post').DataTable({
    
         // "responsive": true,
          "paging": true,
           "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": true,
          "destroy": true,
          "ordering": true   ,
          "autoWidth": false,
          // "columnDefs":[
          //  {"font-weight": "bold", "targets":1 }
          // ],
           "columnDefs": [
            {
                "targets": 1,
                "className": 'reaction-weight'
            }
          ],

             
       
          "order": [],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          
          "ajax": {
            "url": '{{ route('getInboundPost') }}',
            "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              document.getElementById('title_post').innerHTML = 'Posts ('+res.recordsTotal+")";
              return res.data;
            },
             /*       data: function ( d ) {
                      d.fday = mailingListName;
                      d.sday = mailingListName;
                      d.brand_id = mailingListName;
                    }*/
             "data": {
              "fday": fday,
              "sday": sday,
              "admin_page":admin_page,
              "admin_page_id":admin_page_id,
              "other_page":'1',
              "kw_search" : kw_search,
              "brand_id": brand_id,
            
            }

          },
          "initComplete": function( settings, json ) {
            //console.log(json);
          $('.tbl_post thead tr').removeAttr('class');
          },
                         drawCallback: function() {
               $('.select2').select2();
            },

  

          columns: [
          {data: 'post', name: 'post',"orderable": false},
          {
           data: 'reaction',
           name: 'reaction',
           orderable: true,
           // className: "reaction-weight",
           render: $.fn.dataTable.render.number( ',',  2 )

          },
          {data: 'comment', name: 'comment', orderable: true, className: "text-center"},
          {data: 'share', name: 'share', orderable: true, className: "text-center"},
        //   {data: 'overall', name: 'overall', orderable: true,className: "text-center"},


          ]


          
        }).on('change', '.edit_tag', function (e) {

          var hidden_pg  = $('.hidden-chip').val();
          var default_pg = $('.hidden-pg').val();
          var user_role = $('.hidden-role').val();
       
          if(user_role == 'Admin' || hidden_pg === default_pg){

            if (e.handled !== true) {
              var senti_id = $(this).attr('id');
              var post_id  = senti_id.replace("tags_","");
              // alert(post_id);
              var sentiment = $('#sentiment_'+post_id+' option:selected').val();
              var emotion = $('#emotion_'+post_id+' option:selected').val();
              var tags = $('#tags_'+post_id+' option:selected').map(function () {
                  return $(this).text();
              }).get().join(',');

              var tags_id = $('#tags_'+post_id).val();
              // alert(tags_id);
              var brand_id = GetURLParameter('pid');
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id,type:'inbound_post'},
           success: function(response) {
            tagCount();
            if(response>=0){
         
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Tag Changed!!');
                x.className = "show";

              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
          }
        });
        e.handled = true;
       }
      }
     else{
      // alert("hello");
      // alert(default_pg);
      var x = document.getElementById("snackbar")
      $("#snack_desc").html('');
      $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }  
  }).on('click', '.popup_post', function (e) {
          // alert ('hlllll');
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:brand_id},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];
            var monitor_page_imgurl=res_array[4];

               for(var i in data) {
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);
                 
                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
                 var page_name=data[i].page_name;

               
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];

                if (typeof monitor_page_imgurl [found_index] !== 'undefined') 
                var page_photo = monitor_page_imgurl [found_index];
                
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                // var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large';
                //  var page_photo= $mongo_page_imgurl 
                var full_picture ='';
                page_Link= 'https://www.facebook.com/' + page_name ;   

                var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                         '  <div class="sl-left">'+
                         '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                         
                          ' <div class="sl-right">'+
                          '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                          ' <div class="sl-right"> '+
                          ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                          ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
                if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
                if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                html+=' </div></div></div></span></p> ' +                
                      '   <p class="m-t-10" align="justify">' + 
                        data[i].message +
                    
                      '</p> ';
                   if(data[i]['post_type'] =='photo')
                {

                  if(!data[i].local_picture) full_picture = data[i].full_picture;
                  else full_picture = data[i].local_picture;

                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+full_picture+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {



                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }
          
           
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.see_comment', function (e) {//alert("hihi");
    if (e.handled !== true) {
      var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("seeComment_","");
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
      // var admin_page=$('#hidden-chip').val();
     
        var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
          //  alert(admin_page);
          var accountPermission = GetURLParameter('accountPermission');
      window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&accountPermission="+accountPermission, '_blank');
       
       
          }
          


            e.handled = true;
         }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
      // alert(btn_val);
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';
              htmltext +='<fieldset class="controls" style="text-align:left;padding-left:20px;">';

                                       
  for(var i in campaignGroup) 
          {
            // alert(campaignGroup);
            if(i==0)
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
              title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:brand_id,campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:brand_id},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
    
        
       
          }
          
           

            e.handled = true;
         });
      new $.fn.dataTable.FixedHeader( oTable );
     // oTable.columns.adjust().draw();
     // $('#tbl_post').css('width', '100%');
     // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
          
    }

      $(window).resize( function() {

       //  new $.fn.dataTable.FixedHeader( oTable );
        $('#tbl_post').css('width', '100%');
         $('#tbl_visitorpost').css('width', '100%');
        //$("#tbl_post").DataTable().columns.adjust().draw();
        //$('table.fixedHeader-floating').css('left', '200px');
    });
      function getAllVisitorPost(fday,sday,kw_search)
      {
        var brand_id = GetURLParameter('pid');
        $(".popup_post").unbind('click');
        $(".see_comment").unbind('click');

   
      // var admin_page=$('#hidden-chip').val();
       var admin_page_id=$('.hidden-chip').attr('id');
       var admin_page=$('.hidden-chip').val();

         var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
   
         document.getElementById('select_page').innerHTML =admin_page;
   var oTable = $('#tbl_visitorpost').DataTable({
    
         // "responsive": true,
          "paging": true,
           "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": false,
          "destroy": true,
          "ordering": true   ,
          "autoWidth": false,
          "columnDefs": [
            {
                "targets": 1,
                "className": 'reaction-weight'
            }
          ],
          // "columnDefs" : [
          //         { "font-weight": "bold", "targets": 1 },
          //      // { "width": "10%", "targets": 1 },
          //      // { "width": "10%", "targets": 2 },
          //      // { "width": "10%", "targets": 3 },
          //      // { "width": "10%", "targets": 4 }
          // ] ,
          "order": [],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          
          "ajax": {
            "url": '{{ route('getInboundPost') }}',
            "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              document.getElementById('title_visitor').innerHTML = 'Visitor Posts ('+res.recordsTotal+")";
              return res.data;
            },
     /*       data: function ( d ) {
              d.fday = mailingListName;
              d.sday = mailingListName;
              d.brand_id = mailingListName;
            }*/
             "data": {
              "fday": fday,
              "sday": sday,
              "admin_page":admin_page,
              "admin_page_id":admin_page_id,
              "other_page":'1',
              "visitor" : 1,
              "kw_search":$("#kw_search").val(),
              "brand_id": brand_id,
            
            }

          },
          "initComplete": function( settings, json ) {
            //console.log(json);
          $('.tbl_visitorpost thead tr').removeAttr('class');
          },

   

          columns: [
          {data: 'post', name: 'post',"orderable": false},
          {data: 'reaction', name: 'reaction', orderable: true,render: $.fn.dataTable.render.number( ',',  2 )},
          {data: 'comment', name: 'comment', orderable: true, className: "text-center"},
          {data: 'share', name: 'share', orderable: true, className: "text-center"},
        //   {data: 'overall', name: 'overall', orderable: true,className: "text-center"},


          ]
          
        }).on('click', '.popup_post', function (e) {
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:brand_id},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

               for(var i in data) {//alert(data[i].message);
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
                 var page_name=data[i].page_name;

                // if(isNaN(page_name)==false) //it is number
                // {
                 
                //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
                //   page_name = b[0];
                 
                   
                // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
                
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large'; 
        
         page_Link= 'https://www.facebook.com/' + page_name ;   
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      data[i].message +
                    
                      '</p>';
                   if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }
          
           
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.see_comment', function (e) {//alert("hihi");
    if (e.handled !== true) {
      var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("seeComment_","");
     // alert(post_id);
      // var admin_page=$('#hidden-chip').val();
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
     
        var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
          //  alert(admin_page);
 var accountPermission = GetURLParameter('accountPermission');
      window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&accountPermission="+accountPermission, '_blank');
       
       
          }
          


            e.handled = true;
         }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';
              htmltext +='<fieldset class="controls" style="text-align:left;padding-left:20px;">';

                                       
  for(var i in campaignGroup) 
          {
            if(i==0)
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
              title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:brand_id,campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:brand_id},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
    
        
       
          }
          
           

            e.handled = true;
         });
      new $.fn.dataTable.FixedHeader( oTable );
     // oTable.columns.adjust().draw();
     // $('#tbl_post').css('width', '100%');
     // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
          
    }

    

      $("#btnSeeComment").click( function()
             {
               var post_id=$("#popup_id").val();
               // var brand_id = $('#hidden-user').val();
               var brand_id = GetURLParameter('pid');
               // var admin_page=$('#hidden-chip').val();
                var admin_page_id=$('.hidden-chip').attr('id');
                var admin_page=$('.hidden-chip').val();

               var split_name = admin_page.split("-");
                  if(split_name.length > 0)
                  {
                    if(isNaN(split_name[split_name.length-1]) == false )
                      admin_page = split_name[split_name.length-1];
                  }
           // alert(admin_page);
                // window.open("{{ url('relatedcompetitorcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
                 // window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+admin_page , '_blank');
                 var accountPermission = GetURLParameter('accountPermission');
                   window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&accountPermission="+accountPermission, '_blank');
             }
          );

   function getAllComment(fday, sday,filter_tag='',kw_search)
      {
        $(".popup_post").unbind('click');
        $(".edit_predict").unbind('click');
        $("#add_tag").unbind('click');
        var brand_id = GetURLParameter('pid');
   //var admin_page=$('#admin_page_filter option:selected').val();
   //alert($('#hidden-chip').val());
        var filter_tag_arr = [];
              $.each($("input[name='taglist']:checked"), function(){            
                  filter_tag_arr.push($(this).val());
                });
              // alert(filter_tag_arr);
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });

      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

        // var admin_page = $('#hidden-chip').val();

        var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
       //  alert(filter_tag_arr.join("| "));
       // alert(brand_id);

   var oTable = $('#tbl_comment').DataTable({
          "lengthChange": false,
          "searching": false,
          "processing": false,
          "serverSide": true,   
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": true,
          "ordering": true   ,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
          // "order":[1,"desc"],  sort column 1 by desc order
         
          "ajax": {
            "url": '{{ route('getRelatedcomment') }}',
            "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              document.getElementById('title_comment').innerHTML = 'Comments ('+res.recordsTotal+")";
              return res.data;
            },
     /*       data: function ( d ) {
              d.fday = mailingListName;
              d.sday = mailingListName;
              d.brand_id = mailingListName;
            }*/
             "data": {
              "fday": fday,
              "sday": sday,
              "admin_page":admin_page,
              "admin_page_id":admin_page_id,
              // "brand_id": GetURLParameter('pid'),
              "brand_id" : brand_id,
              "kw_search":kw_search,
              "tsearch_senti":filter_senti_arr.join("| "),
              "tsearch_tag":filter_tag_arr.join("| "),
              "is_competitor":"true",
            }

          },
          "initComplete": function( settings, json ) {
            //console.log(json);
         
          },
       drawCallback: function() {
       $('.select2').select2();
    },

          columns: [
          {data: 'post_div', name: 'post_div',"orderable": false},
          {data: 'reaction', name: 'reaction',"orderable": true,className: "text-center"},
      


          ]
          
        }).on('click', '.show_alert', function (e) {
           e.preventDefault();
           $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
            // var url = $(this).data('remote');
            swal({
                title: 'Sorry',
                text: "This account has no permission to do this!",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })

        }).on('click', '.popup_post', function (e) {
          // alert(brand_id);
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:brand_id},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

            

              for(var i in data) {//alert(data[i].message);
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
        
        var page_name=data[i].page_name;
        // if(isNaN(page_name)==false)
        // {
          
        //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
        //  page_name = b[0];
        
        // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
        
        var page_Link= 'https://www.facebook.com/' + page_name ;  
        var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large';                         
  //data[i].full_picture
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      data[i].message +
                    
                      '</p>';
                   if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }


           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.reply_count', function (e) {
          
    if (e.handled !== true) {

      var comment_id = $(this).attr('id');
      // alert(comment_id);
      // $("#modal-spin").show();
      $(".reply_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getReplyComments") }}',
           type: 'GET',
           data: {id:comment_id,brand_id:brand_id},
           success: function(response) { console.log("REPLY"+response);

           

            var res_array=JSON.parse(response);
            var tags = [];
            var data=res_array[0];
                tags=res_array[1];
            // console.log("Tags"+tags);
            // var arr = [];
            // for(var j in tags){
            //   arr.push(tags[j].id);
            //   arr.push(tags[j].name);
            // }

// console.log('Tag ID ARRAY'+JSON.stringify(arr));
            // console.log("TTTags" +JSON.stringify(tags[0].id));
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

            

      for(var i in data) 
      {
          var page_name=data[i].page_name ;
        //generate comment link
          var profilePhoto='';var ProfileName='';
        // var profileLink='javascript:void(0)';
          var fb_p_id= data[i].post_id.split("_");
              fb_p_id=fb_p_id[1];
              fb_c_id=data[i].id.split("_");
              fb_c_id=fb_c_id[1];

          if(data[i].profileName !== '' && data[i].profileName !== null) ProfileName= data[i].profileName; 
          else if(data[i].profileID !== '' && data[i].profileID !== null) ProfileName=data[i].profileID;
          else  ProfileName = page_name;

          if(data[i].profileType === 'number')
          {
            profilePhoto='https://graph.facebook.com/'+data[i].profileID+'/picture?type=large';
            ProfileName=page_name;
          }
        
          if(data[i].profileID !== '')
          var profileLink='https://www.facebook.com/'+data[i].profileID;

          var page_id='';
          var arr_page_id=page_name.split('-');

          if(arr_page_id === undefined || arr_page_id.length == 0)
          {
            var last_index=arr_page_id[count(arr_page_id)-1];
            if(is_numeric(last_index))
            page_id = last_index;
            else
            page_id = page_name;
          }
          else
          {
            page_id=page_name;
          }

          var comment_Link= 'https://www.facebook.com/'+fb_c_id;


          var message = data[i].message;

          var classforbtn='';var stylefora='';var bgcolor='';
          if(data[i].sentiment=='neg'){
            classforbtn='label label-danger'; 
            stylefora="color:#ffffff"; 
          } 
          else if(data[i].sentiment=='pos'){
            classforbtn='label label-success';
            stylefora="color:#ffffff"; 
          } 
          else{
            classforbtn='label';
            stylefora="color:#343a40" ;
            bgcolor='background:#e9ecef';
          }


          var html = '';
          html = '<div class="profiletimeline" style="padding:10px"><div class="sl-item">';
                  if(profilePhoto !== '')
                    html += '<div class="sl-left user-img"> <img src="'+ profilePhoto+'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                  else
                    html += '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                     
                    html +='<div class="form-material sl-right"><div><div><a href="'+profileLink+'" class="link" target="_blank">'+ProfileName+'</a>';               
                    if(data[i].hp !== '' && data[i].hp_tags !== '')
                    html+='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input both" data-toggle="tooltip"></i></a>';
                    else if(data[i].hp !== '' ||  data[i].change_predict == 1 ||  data[i].checked_predict == 1)
                    html+='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input sentiment" data-toggle="tooltip"></i></a>';
                    else if(data[i].hp_tags !== '')
                    html+='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Manual Input tags" data-toggle="tooltip"></i></a>';
                    html +='<span style="float:right" class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span></div>';
                     
                     html+='<p>Sentiment : <select id="rp-sentiment_'+data[i].id+'"  class="form-material form-control in-comment-select senti-edit ';
                     if(data[i].sentiment=="")html+='"><option value="" selected="selected" ></option>';
                     if(data[i].sentiment=="pos")html+='text-success" >';
                     if(data[i].sentiment=="neg")html+='text-red" >';
                     if(data[i].sentiment=="neutral" || data[i].sentiment == "NA")html+='text-warning" >';
                  
                      html+='<option value="pos" class="text-success"';
                      if(data[i].sentiment == "pos")
                      html+= 'selected="selected"';
                      html+= '>Positive</option><option value="neg" class="text-red"';
                      if(data[i].sentiment == "neg")
                      html+= 'selected="selected"';
                      html+= '>Negative</option><option value="neutral" class="text-warning"';
                      if(data[i].sentiment == "neutral" || data[i].sentiment == "NA")
                      html+= 'selected="selected"';      
                      html+= '>Neutral</option></select>';

                     
                      html+=' | Tag :<select id="rp-tags_'+data[i].id+'" class="form-material form-control  select2 tag-edit" multiple="multiple" data-placeholder="Select tag" style="width:250px;height:36px">';
                      var tag_array=data[i].tags.split(',');
                      // alert(tag_array);
                      for(var j in tags) {

                      var res = findInArray(tag_array, tags[j].id);
                    
                       // if ( in_array( $value->id,$tag_array))
                       // if(tag_array.indexOf(tags[j].id))
                       if (res !== -1)
                       html+= '<option value="'+tags[j].id+'" selected="selected">'+tags[j].name+'</option>';
                        else
                       html += '<option value="'+tags[j].id+'">'+tags[j].name+'</option>';
                      }
                      html+='</select>';

                   //    if($action_permission == 0){
                   //    $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                   //   $html.='<a class="show_alert" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id=""><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                   //   }
                   //   else{

                   //    if($permission_data['edit'] === 1)
                   //    $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
                   //   $html.='<a class="get-code" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id="add_tag"><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                   // }
                      
                      html+='</p><p class="m-t-10">'+message+'</p></div>';
                      //  if(data[i].cmtType =='sticker' || data[i].cmtType =='share' || data[i].cmtType =='share_large_image' || data[i].cmtType =='animated_image_share' || data[i].cmtType == 'video_inline' )
                      //  {
                      //     html += '<a class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'+
                      //           '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                      //         '<img id="img_'+data[i].id+'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'+data['cmtLink']+'">'+
                      // '</span><span class="postPanel_nameTextContainer"></span></a>';
                      //  }
                      //  else if ( data[i].cmtType =='photo') {
                      //    if(null !== data[i].local_picture) { var picture = data[i].local_picture;}
                      //    else{ var picture = data[i].cmtLink;}

                      //   html += '<a class="postPanel_imageAndNameTextContainer cl cf" href="javascript:avoid(0)">'+
                      //           '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                      //         '<img id="img_'+data[i].id+'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'+$picture +'">'+
                      // '</span><span class="postPanel_nameTextContainer"></span></a>';
                        
                      //  }
                       // else if(data[i].cmtType =='animated_image_video' || data[i].cmtType =='video_share_youtube' || data[i].cmtType =='video_share_highlighted')
                       // {
                       //   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i].id+'">'+
                       //    '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                       //    '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i].cmtLink+'&width=400&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                       //    '</span><span class="postPanel_nameTextContainer"></span></a>';
                       // }
                    
                      html+= '<div class="sl-right">';
                    
                        if(parseInt(data[i].cmtLiked) > 0)
                        html+='👍 '+thousandsCurrencyFormat(data[i].cmtLiked);
                       if(parseInt(data[i].cmtLove) > 0)
                        html+=' ❤️ '+thousandsCurrencyFormat(data[i].cmtLove);
                       if(parseInt(data[i].cmtHaha) > 0)
                        html+=' 😆 '+thousandsCurrencyFormat(data[i].cmtHaha);
                       if(parseInt(data[i].cmtWow) > 0)
                        html+=' 😮 '+thousandsCurrencyFormat(data[i].cmtWow);
                       if(parseInt(data[i].cmtSad))
                        html+=' 😪 '+thousandsCurrencyFormat(data[i].cmtSad);
                       if(parseInt(data[i].cmtAngry))
                        html+=' 😡 '+thousandsCurrencyFormat(data[i].cmtAngry);
                        html+='</div><hr>';
                $(".reply_data").append(html);
      }
              $('.select2').select2();    /*to work select2 properly in modal box*/


           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-reply-cmt').modal('show'); 
          }
        });
      e.handled = true;
     }
          
    }).on('click', '.edit_predict', function (e) {
      // alert(brand_id);
    if (e.handled !== true) {

      var post_id = $(this).attr('id');

  /*    var sentiment = $('input[name=sentiment]').val();
      var emotion = $('input[name=emotion_'+post_id+']').val();*/
      var sentiment = $('#sentiment_'+post_id+' option:selected').val();
      var emotion = $('#emotion_'+post_id+' option:selected').val();
      var tags = $('#tags_'+post_id).val();
    //   alert(sentiment);
    // alert(tags);
    // return;
     
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags},
           success: function(response) {// alert(response)
            if(response>=0)
            {
                  swal({   
              title: "Updated!",   
              text: "Done!",   
              timer: 1000,   
              showConfirmButton: false 
          });
            }
          }
              });

            e.handled = true;
         }
       
      
          
    }).on('change', '.edit_senti', function (e) {
      var hidden_pg  = $('.hidden-chip').val();
	    var default_pg = $('.hidden-pg').val();
      var user_role = $('.hidden-role').val();

      if(user_role == 'Admin' || hidden_pg === default_pg){
        if (e.handled !== true) {
          var senti_id = $(this).attr('id');
          var post_id  = senti_id.replace("sentiment_","");
          var sentiment = $('#sentiment_'+post_id+' option:selected').val();
          var emotion = $('#emotion_'+post_id+' option:selected').val();
          var tags = $('#tags_'+post_id+' option:selected').map(function () {
              return $(this).text();
          }).get().join(',');

          var tags_id = $('#tags_'+post_id).val();
          var brand_id = GetURLParameter('pid');;
      
      $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
        if(sentiment == 'pos')  $("#sentiment_"+post_id).addClass('text-success');
        else if (sentiment == 'neg') $("#sentiment_"+post_id).addClass('text-red');
        else $("#sentiment_"+post_id).addClass('text-warning');
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {
            if(response>=0)
            {
               var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Sentiment Changed!!');
                x.className = "show";

                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }

          }
          });
            e.handled = true;
         }
      }

         else{
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
                x.className = "show";
              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
           }
          
    }).on('change', '.edit_tag', function (e) {
      var hidden_pg  = $('.hidden-chip').val();
	    var default_pg = $('.hidden-pg').val();
      var user_role = $('.hidden-role').val();
      if(user_role == 'Admin' || hidden_pg === default_pg){
        if (e.handled !== true) {
          var senti_id = $(this).attr('id');
          var post_id  = senti_id.replace("tags_","");
          // alert(post_id);
          var sentiment = $('#sentiment_'+post_id+' option:selected').val();
          var emotion = $('#emotion_'+post_id+' option:selected').val();
          var tags = $('#tags_'+post_id+' option:selected').map(function () {
              return $(this).text();
          }).get().join(',');

          var tags_id = $('#tags_'+post_id).val();
          var brand_id = GetURLParameter('pid');
        
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {
            tagCount();
             // alert(response)
            if(response>=0)
            {
              var x = document.getElementById("snackbar")
              $("#snack_desc").html('');
              $("#snack_desc").html('Tag Changed!!');
                x.className = "show";

                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
          }
        });
        e.handled = true;
       }
      }       

     else{
      var x = document.getElementById("snackbar")
      $("#snack_desc").html('');
      $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
        x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    } 
    }).on('click', '#add_tag', function (e) {
    if (e.handled !== true) {
           $('#show-add-tag').modal('show'); 
          }
            
            e.handled = true;
         
       
      
          
    }).on('click', '.popup_img', function (e) {
    if (e.handled !== true) {

         var id = $(this).attr('id');
         var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
// img id="img_'.$data->id.'"  class="popup_img postPanel_previewImage" style="width:20%;height:50%" src="'.$data->cmtLink.'">
          $(".large_img").append("<div class='img-container'><img class='postPanel_previewImage' style='width:30%;height:50%;' src='"+src_path+"'/></div>");
          $('#show-image').modal('show'); 
         }
       
      
          
    });
       
      
          
    }
    $('#show-add-tag').on('shown.bs.modal', function () {
      $('#name').focus();
  })  



     // for reply modal popup
  $(document).on('change','.tag-edit',function(){
    // alert('enter');
          var hidden_pg  = $('.hidden-chip').val();
          var default_pg = $('.hidden-pg').val();
          var user_role = $('.hidden-role').val();
       
          if(user_role == 'Admin' || hidden_pg === default_pg){
 // alert('allow');
              var senti_id = $(this).attr('id');
              var post_id  = senti_id.replace("rp-tags_","");
              // alert(post_id);
              var sentiment = $('#rp-sentiment_'+post_id+' option:selected').val();
              // alert(sentiment);
              var emotion = $('#emotion_'+post_id+' option:selected').val();
              var tags = $('#rp-tags_'+post_id+' option:selected').map(function () {
                  return $(this).text();
              }).get().join(',');
              // alert(tags);
              var tags_id = $('#rp-tags_'+post_id).val();
              // alert(tags_id);
              var brand_id = GetURLParameter('pid');
              // alert(brand_id);
              // alert(tags);alert(tags_id);
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {
            // tagCount();
            if(response>=0){
         
              var x = document.getElementById("rptag-snackbar");
            
              $("#rptag_snack_desc").html('');
              $("#rptag_snack_desc").html('Tag Changed!!');
                x.className = "show";

              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
          }
        });

      }
     else{
      // alert("hello");
      // alert(default_pg);
      var x = document.getElementById("rp-snackbar");
      $("#rp_snack_desc").html('');
      $("#rp_snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    } 
})


 // for reply modal popup
  $(document).on('change','.senti-edit',function(){
      var hidden_pg  = $('.hidden-chip').val();
      var default_pg = $('.hidden-pg').val();
      var user_role = $('.hidden-role').val();

      if(user_role == 'Admin' || hidden_pg === default_pg){
          var senti_id = $(this).attr('id');
          var post_id  = senti_id.replace("rp-sentiment_","");
          var sentiment = $('#rp-sentiment_'+post_id+' option:selected').val();
          var emotion = $('#emotion_'+post_id+' option:selected').val();
          var tags = $('#tags_'+post_id+' option:selected').map(function () {
              return $(this).text();
          }).get().join(',');

          var tags_id = $('#tags_'+post_id).val();
          var brand_id = GetURLParameter('pid');;

      $("#rp-sentiment_"+post_id).removeClass('text-success'); $("#rp-sentiment_"+post_id).removeClass('text-red'); $("#rp-sentiment_"+post_id).removeClass('text-warning');
        if(sentiment == 'pos')  $("#rp-sentiment_"+post_id).addClass('text-success');
        else if (sentiment == 'neg') $("#rp-sentiment_"+post_id).addClass('text-red');
        else $("#rp-sentiment_"+post_id).addClass('text-warning');
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id},
           success: function(response) {
            if(response>=0)
            {
               var x = document.getElementById("rp-snackbar");
              $("#rp_snack_desc").html('');
              $("#rp_snack_desc").html('Sentiment Changed!!');
                x.className = "show";

                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }

          }
          });
      }

         else{
              var x = document.getElementById("rp-snackbar");

              $("#rp_snack_desc").html('');
              $("#rp_snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
                x.className = "show";
              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
           }
})




    var permission = GetURLParameter('accountPermission');
    // alert(permission);
  $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
                startDate: startDate,
                endDate: endDate,
          },function(start, end,label) {
            if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) 
          
            {
                 swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }


            else{
              var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;
              var kw_search = $("#kw_search").val();
              ChooseDate(startDate,endDate,label,'',kw_search);
              tagCount();
             }
           }
           else
           {
                var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;
              var kw_search = $("#kw_search").val();
              ChooseDate(startDate,endDate,label,'',kw_search);
              tagCount();
           }
        }
      )

  // to acess only three month data for demo account 

    //     $(".dateranges").on("change",function(){

         
        
    // });


   //ChooseDate(startDate,endDate,'','');

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
     //window.dispatchEvent(new Event('resize'));
  // if(target=="#dashboard")
  //   {
      window.dispatchEvent(new Event('resize'));
  //  }

     if(target == '#comment')
  {
    $("#outer_div").removeClass("col-lg-12");
    $("#outer_div").addClass("col-lg-9");
    $("#filter_div").show();
   
  }
  else
  {
     $("#outer_div").removeClass("col-lg-9");
    $("#outer_div").addClass("col-lg-12");
    $("#filter_div").hide();
  }


   });

  hidden_div();
  function getCampaignGroup()
  {
    // add-campaign
   
      var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getCampaignGroup')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          page_name:$('.hidden-chip').val(),
          }
      })
      .done(function( data ) {
        campaignGroup=data;
       
          
        
      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
  $('#nameSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'name';

      tagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
      var tagOrder = 'DESC' ;
      var fieldName = 'name';

      tagCount(tagOrder,fieldName);
    }
    

  });
    $('#countSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'count';

      tagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
       var tagOrder = 'DESC' ;
      var fieldName = 'count';

       tagCount(tagOrder,fieldName);
    }
    

  });
    
 // var arr = ['abc','def'];
 // alert(arr);
 // alert(arr.indexOf('abc'));
    var selected_tag = [];
 
  function tagCount(tagOrder='',fieldName='')
  {
      // alert(sel);
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

      var filter_tag_arr = [];
      $.each($("input[name='taglist']:checked"), function(){            
          filter_tag_arr.push($(this).val());
          selected_tag.push($(this).val());

          // selected_tag.push($(this).next('label').text()); 
        });
// alert(selected_tag);
       var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });
       // alert(selected_tag);
       // alert(filter_tag_arr);alert(selected_tag);
      // alert(selected_tag);
      var fday=GetStartDate();
      var sday=GetEndDate();
      $(".tag-list").empty();
      var brand_id = GetURLParameter('pid');
     
      // alert(admin_page);

          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getTagCount')}}", // This is the URL to the API
        data: { 
          fday: fday,
          sday:sday,
          brand_id:brand_id,
          tagOrder:tagOrder,
          fieldName:fieldName,
          admin_page:admin_page,
          admin_page_id:admin_page_id,
          tsearch_tag:filter_tag_arr.join("| "),
          tsearch_senti:filter_senti_arr.join("| "),
          limit:'no'}
      })
      .done(function( data ){

          for(var i in data) 
          {
             var tagLabel=data[i].tagLabel;
             var tagId=data[i].tagId;
             var tagCount=data[i].tagCount;
             var res = findInArray(selected_tag, tagId);
           
            if (res !== -1 ) {

              $(".tag-list").append(' <li>' +
              ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'" checked>'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px;" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');

            }
            else{

               $(".tag-list").append(' <li>' +
              ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'" >'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
             }
            

        
           
            

          }
          selected_tag = [];

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }

  function findInArray(ar, val) {
    for (var i = 0,len = ar.length; i < len; i++) {
        if ( ar[i] == val ) {
            return i;
        }
    }
    return -1;
}
 
  $(document).on('click', '.taglist', function () {

    getAllComment(GetStartDate(),GetEndDate());
    // selected_tag = [];
    tagCount();
  });
  $(document).on('click', '.sentimentlist', function () {
    getAllComment(GetStartDate(),GetEndDate());
   tagCount();
});


  $(document).on('click','.top_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:80%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
    $(document).on('click','.top_cmt_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
  $('#senti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.taglist)").click(function () {
    $("#senti-all").prop("checked", $("input:checkbox:not(.taglist):checked").length == 4);
  });

  bindOtherPage();
  bindBankPage();
  bindMfsPage();
  bindCardPage();

  $(document).on('click', '.chip', function (e) {
    var id = $(this).attr("id");
    var name = $(this).attr("name");
    var chipcompetitor = document.getElementById("chip-competitor");
   
      $(".chip").css("background-color","#e4e4e4");
      $(".chip").css("color","rgba(0,0,0,0.6)");

      // id with space are called in javascript as below
      $('[id="'+id+'"]').css("background-color","rgba(188, 138, 49, 1)");
      $('[id="'+id+'"]').css("color","white");
      // $("#"+id).css("background-color","rgba(188, 138, 49, 1)");
      // $("#"+id).css("color","white");


      $(".hidden-chip").val(name);
      $(".hidden-chip").attr("id",id);
      // you don't understand and i can't explain
    
     $(".dropdown-pages").dropdown('toggle');
      var kw_search = $("#kw_search").val();
      ChooseDate(startDate,endDate,'','',kw_search);
      tagCount();
      
        });
 // $("#btn_pages").on('click', function () {
 //  $("#search_form").css("visibility", "visible");
 //  $("#search_form").css("display", "inline");
 // });
//  $("[data-toggle=dropdown]").prop('onclick', null).off('click');
// $("[data-toggle=dropdown]").click(function (e) {
//     e.preventDefault();
//     // e.stopPropagation();
//     // console.log($(this));
//     let el = $(this);
//     let expanded = el.attr("aria-expanded") || false;
//     alert(expanded);
//     if (!expanded) {
//         $(this).dropdown('toggle');
//     }

//     el.attr("aria-expanded", !expanded);
// })

  function bindBankPage()
  {
      var company_id = $("#hidden-user").val();
 
      $("#bank-chip").empty();
      var brand_id = GetURLParameter('pid');
     
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getBankPage')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          company_id:company_id,
          type:'Bank',
          }
      })
      .done(function( data ) {
       console.log("bindingBankPages"+JSON.stringify(data));
        for(var i in data) 
        { 
           var default_page=data[i]['default_page'];
           var id=data[i]['page_name'].replace(".", "-");
            if(data[i]['imgurl'] !== '') var photo = data[i]['imgurl'];
            else photo ="{{asset('assets/images/competitor1.png')}}";

            if(default_page !== '')
            {
              if(default_page == data[i]['page_name'])
              { 
                
                $("#bank-chip").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');

                   $(".hidden-chip").val(data[i]['page_name']); 
                   $( ".hidden-chip" ).attr("id",data[i]['id']);
                  
              }
              else
              {
                $("#bank-chip").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');

              }
            }
            else
            {
             if(parseInt(i) == 0)
              { 
                
                $("#bank-chip").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');
                   
                   $(".hidden-chip").val(data[i]['page_name']); 
                   $( ".hidden-chip" ).attr("id",data[i]['id']);
                   // $("#hidden-chip").val(data[i]['page_name']); 
                
              }
              else
              {
                $("#bank-chip").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');
              }
            }
         


          }
          //  var kw_search = $("#kw_search").val();
          // ChooseDate(startDate,endDate,'','',kw_search);
          // tagCount();
       

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
   function bindMfsPage()
  {
      var company_id = $("#hidden-user").val();
 
      $("#bank-chip").empty();
      var brand_id = GetURLParameter('pid');
     
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getMfsPage')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          company_id:company_id,
          type:'MFS',
          }
      })
      .done(function( data ) {
       console.log("bindingMFSPages"+JSON.stringify(data));
        for(var i in data) 
        { 
           var default_page=data[i]['default_page'];
           var id=data[i]['page_name'].replace(".", "-");
            if(data[i]['imgurl'] !== '') var photo = data[i]['imgurl'];
            else photo ="{{asset('assets/images/competitor1.png')}}";

            if(default_page !== '')
            {
              if(default_page == data[i]['page_name'])
              { 
                
                $("#mfs-chip").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');

                   $(".hidden-chip").val(data[i]['page_name']); 
                   $( ".hidden-chip" ).attr("id",data[i]['id']);
                  
              }
              else
              {
                $("#mfs-chip").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');

              }
            }
            else
            {
             if(parseInt(i) == 0)
              { 
                
                $("#mfs-chip").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');
                   
                   $(".hidden-chip").val(data[i]['page_name']); 
                   $( ".hidden-chip" ).attr("id",data[i]['id']);
                   // $("#hidden-chip").val(data[i]['page_name']); 
                
              }
              else
              {
                $("#mfs-chip").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');
              }
            }
         


          }
          //  var kw_search = $("#kw_search").val();
          // ChooseDate(startDate,endDate,'','',kw_search);
          // tagCount();
       

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
     function bindCardPage()
  {
      var company_id = $("#hidden-user").val();
 
      $("#card-chip").empty();
      var brand_id = GetURLParameter('pid');
     
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getCardPage')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          company_id:company_id,
          type:'Card',
          }
      })
      .done(function( data ) {
       console.log("bindingCards"+JSON.stringify(data));
        for(var i in data) 
        { 
           var default_page=data[i]['default_page'];
           var id=data[i]['page_name'].replace(".", "-");
            if(data[i]['imgurl'] !== '') var photo = data[i]['imgurl'];
            else photo ="{{asset('assets/images/competitor1.png')}}";

            if(default_page !== '')
            {
              if(default_page == data[i]['page_name'])
              { 
                
                $("#card-chip").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');

                   $(".hidden-chip").val(data[i]['page_name']); 
                   $( ".hidden-chip" ).attr("id",data[i]['id']);
                  
              }
              else
              {
                $("#card-chip").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');

              }
            }
            else
            {
             if(parseInt(i) == 0)
              { 
                
                $("#card-chip").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');
                   
                   $(".hidden-chip").val(data[i]['page_name']); 
                   $( ".hidden-chip" ).attr("id",data[i]['id']);
                   // $("#hidden-chip").val(data[i]['page_name']); 
                
              }
              else
              {
                $("#card-chip").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['page_name']+'" > ' +
                      ' <img src="'+photo+'" alt=""> '+
                      data[i]['page_name'] + '</div>');
              }
            }
         


          }
          //  var kw_search = $("#kw_search").val();
          // ChooseDate(startDate,endDate,'','',kw_search);
          // tagCount();
       

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }


  function bindOtherPage()
  {
      var company_id = $("#hidden-user").val();
 
      $("#chip-competitor tbody").empty();
      var brand_id = GetURLParameter('pid');
      // alert(company_id);
           // alert(brand_id);
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getOtherPage')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          company_id:company_id,
          }
      })
      .done(function( data ) {//alert(data);
       // console.log("hey"+JSON.stringify(data));
// var default_page = data[0]['default_page'];
  for(var i in data) 
          {
           // alert(i);    
           var default_page=data[i]['default_page'];
           // alert(data[i]['page_name']);
           var id=data[i]['page_name'].replace(".", "-");
          // alert(id);
         //   if(data[i]['id'] !== '')
         //   var photo='https://graph.facebook.com/'+data[i]['id']+'/picture?type=large';
         // else
         //  var photo ="{{asset('assets/images/competitor1.png')}}";
         if(data[i]['imgurl'] !== '') var photo = data[i]['imgurl'];
         else photo ="{{asset('assets/images/competitor1.png')}}";

         if(default_page !== '')
         {
    // alert(data[i]['page_name']);
            if(default_page == data[i]['page_name'])
            { 
            	// alert('equal');
              
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['page_name']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');

                 $(".hidden-chip").val(data[i]['page_name']); 
                 $( ".hidden-chip" ).attr("id",data[i]['id']);
                 // $("#hidden-chip").(data[i]['page_name']);
              
            }
            else
            {
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['page_name']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');

            }
    

         }
         else
         {
           if(parseInt(i) == 0)
            { 
              
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['page_name']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');
                 
                 $(".hidden-chip").val(data[i]['page_name']); 
                 $( ".hidden-chip" ).attr("id",data[i]['id']);
                 // $("#hidden-chip").val(data[i]['page_name']); 
              
            }
            else
            {
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['page_name']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');
            }

         }
         


          }
           var kw_search = $("#kw_search").val();
          ChooseDate(startDate,endDate,'','',kw_search);
          tagCount();
       

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
  $("#add_new_tag").click(function(){
  //alert("hihi");
    var name = $("#name").val();
    var nameLength = name.length;
    // var category_id = $("#category_id").val();

    if(name == '')
    {
      $(".invalid-name").append("<strong class='text-danger'>Please fill out name!</strong>");
      return false;
    }
    else if (nameLength > 80 )
      {
      $(".invalid-name").append("<strong class='text-danger'>The name may not be greater than 80 characters!</strong>");
      return false;
    }
    //  if(category_id == '')
    // {
    //   $(".invalid-category").append("<strong class='text-danger'>Please choose category!</strong>");
    //   return false;
    // }
    //var keywords = $("#keywords").val();
    var brand_id = $("#brand_id").val();
    
   
    var keywords = '';

  //   if(keywords == '')
  //   {
  //   $(".invalid-keyword").append("<strong>Please fill out keyword</strong>");
  //   return false;
  // }
     $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("quick_tag") }}',
           type: 'POST',
           data: {name:name,keywords:keywords,brand_id:brand_id},
           success: function(response) {//alert(response);
            //alert(response);
            if(response !== "exist")
            {
               $('.edit_tag')
           .append($("<option></option>")
                      .attr("value",response)
                      .text(name)); 
                      var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Tag added successfully!!');
      x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          //             swal({   
          //     title: "Success!",   
          //     text: "Tag " + name + " is already added!" ,   
          //     timer: 500 ,   
          //     showConfirmButton: false 
          // });
            }

            else if (response == "exist")
            {
              var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('This tag already exist !!');
      x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
               $("#name").val('');
               $("#keywords").val('');
       $('#show-add-tag').modal('toggle');
           
          }
        });

  });

    $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
         });


  $('#admin_page_filter').change(function() {
      //alert($(this).val());
      var admin_page = $(this).val();
      var kw_search = $("#kw_search").val();
      ChooseDate(startDate,endDate,'','',kw_search);
     
       // $(this).val() will work here
  });

  // $(document).on('click', '.btn_tag', function () {
  //     var filter_tag=this.id;
  // var admin_page = $('#hidden-chip').val();

  //     getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag);
  // });
  $(document).on('click','.dropdown-menu a',function(){
  var row_id=this.id;
  row_id = row_id.substring(row_id.indexOf('_')+1);
  // alert(row_id);
        $("#btnaction_"+row_id+":first-child").text($(this).text());
        $("#btnaction_"+row_id+":first-child").val($(this).text());
        // $("#btnaction_"+row_id).removeClass('btn-red');
        // $("#btnaction_"+row_id).removeClass('btn-success');
        // if($(this).text() === "Require Action")
        // {
        // $("#btnaction_"+row_id).addClass('btn-red');
        // }
        // else
        // {
        //   $("#btnaction_"+row_id).addClass('btn-success');
        // }

         $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setActionUpdate") }}',
           type: 'POST',
           data: {id:row_id,brand_id:GetURLParameter('pid'),action_status:$(this).text()},
           success: function(response) { //alert(response)
            if(response>=0)
            {
         
            }
          }
              });

        //save action status and taken person in database
  });
  //dashboard function
    function MostFrequentlyPostTag(fday,sday)
  {
    $(".post_tag_count").empty();
    $("#post-tag-count-spin").show();
    // var brand_id = GetURLParameter('pid');
    var brand_id = GetURLParameter('pid');
     var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id,limit:'no',type:'inbound_post'}
    })
    .done(function( data ) {console.log("post tag count " +JSON.stringify(data));
      $(".post_tag_count").empty();
        $("#post-tag-count-spin").hide();
            $pos = 0;
            $arry_length = Object.keys(data).length;

      for(var i in data) {
           var tagLabel=data[i].tagLabel;
           var tagId=data[i].tagId;
           var tagCount=data[i].tagCount;
           if($pos<9)
           {
           $(".post_tag_count").append('<button type="button" style="margin:0.2rem" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>');
            }
          else if($pos==9){
             $(".post_tag_count").append('<button type="button" style="margin:0.2rem" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>');
             $(".post_tag_count").append('<div style="padding-left:1rem"><a style="font-weight: 700;font-size:15px" class="text-info" href="javascript:void(0)" id="see_more">see more &gt;&gt;</a></div>');
               
            }
          else{
             // alert('hello');
             $(".post_tag_count").append('<button type="button" style="margin:0.2rem;display:none;" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_hide" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>');
              }
                if($pos == $arry_length-1)
                  $(".post_tag_count").append('<div style="padding-left:1rem"><a style="font-weight: 700;font-size:15px;display:none;width:100%" class="text-info" href="javascript:void(0)" id="see_less"><< see less</a></div>');
                $pos +=1;
        }
             
    })
     .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
    
    });
  }
  function MostFrequentlyTag(fday,sday)
  {
    $(".tag_count").empty();
    $("#tag-count-spin").show();
    // var brand_id = GetURLParameter('pid');
    var brand_id = GetURLParameter('pid');
     var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id,limit:'no'}
    })
    .done(function( data ) {//console.log(data);
      $(".tag_count").empty();
        $("#tag-count-spin").hide();

         $pos = 0;
         $arry_length = Object.keys(data).length;
        for(var i in data) 
        {
           var tagLabel=data[i].tagLabel;
           var tagId=data[i].tagId;
           var tagCount=data[i].tagCount;
         if($pos<9){
           $(".tag_count").append('<button type="button" style="margin:0.2rem" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_frequent_tag btn_cmt_tag_show" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>');
         }
         else if($pos==9){
          $(".tag_count").append('<button type="button" style="margin:0.2rem" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_frequent_tag btn_cmt_tag_show" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>');
          $(".tag_count").append('<div style="padding-left:1rem"><a style="font-weight: 700;font-size:15px" class="text-info" href="javascript:void(0)" id="more">see more &gt;&gt;</a></div>');
         }
         else{
          $(".tag_count").append('<button type="button" style="margin:0.2rem;display:none;" class="btn waves-effect waves-light btn-rounded btn_frequent_tag btn-sm btn-info btn_cmt_tag_hide" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>');
         }
         if($pos == $arry_length-1)
           $(".tag_count").append('<div style="padding-left:1rem"><a style="font-weight: 700;font-size:15px;display:none;width:100%" class="text-info" href="javascript:void(0)" id="less"><< see less</a></div>');
                $pos +=1;

        }
    })
     .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
    
    });
  }
  function PageGrowth(fday,sday,date_preset)
  {
    var admin_page_id=$('.hidden-chip').attr('id');
    var admin_page=$('.hidden-chip').val();

    var FanGrowthChart = echarts.init(document.getElementById('fan-growth-chart'));
    $('#fan-growth-spin').show();
    var brand_id =GetURLParameter('pid');
   
    // alert(date_preset);
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPageLike')}}", // This is the URL to the API
      data: { date_preset: date_preset,page_name:admin_page,page_id:admin_page_id,fday:fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {
      console.log('vissamm pagelike = ' + data);
        
      var fan = [];
      var fan_online = [];
      var endtime = [];
      var color = [];
         for(var i in data) {//alert(data[i].mentions);
        fan.push(data[i].fan);
        color.push(data[i].color);
        endtime.push(data[i].end_time);
         if(i == data.length-1)
          $('#last_like_count').empty();
           $('#last_like_count').append(data[i].fan);

        
      }
      if(color == undefined  || color == null){
        color = color[0];
      }
      // var min_of_array = Math.min.apply(Math, fan);
      // var min_of_array = min_of_array-10000;
        option_growth= null;
option_growth = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
            //type: 'cross'
        },
        formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
        params.forEach(item => {
            // console.log("item");
            // console.log(item.data);
           
            var item_data=item.data==='-'?'-':numberWithCommas(item.data);
            var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + item_data  + '</p>'
            rez += xx;
        });

        return rez;
    }
    },

          grid: {
          top:    60,
    
    left:   '3%',
    right:  '10%',
    bottom:  '5%',
            containLabel: true
        },
  /*  toolbox: {
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },*/
     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: false}
            }
        },

//     legend: {
//         data:['Fan'],//,'social media reach'

// //           formatter: function (name) {
// //             if(name === 'Positive')
// //             return name + ': ' + positive_total;
// //             if(name === 'Negative')
// //             return name  + ': ' + negative_total;
// //             else
// //             return name  + ': ' + social_total;

   
// // },

//     },
    xAxis: [
        {
        
            type: 'category',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
                  axisLabel: {
      formatter: function (value, index) {
       const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

     var date = new Date(value);
       var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
       return xx;

},

    },
            data: endtime
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Like Count',
          //  min: min_of_array,
            scale:true,
            // max: 250,
            position: 'left',
            // axisLine: {
            //     lineStyle: {
            //         color: colors[0]
            //     }
            // }
    //                 axisLabel: {
    //     formatter: function (e) {
    //         return kFormatter(e);
    //     }
    // }
    
     }
    ],
    series: [
        {
            name:'Like',
            type:'line',
            smooth: 0.2,
            color:color,
            barMaxWidth:30,
            data:fan
        }
    ]
};
$("#fan-growth-spin").hide();
        
 FanGrowthChart.setOption(option_growth, true), $(function() {
    // function resize() {
    //     setTimeout(function() {
    //         FanGrowthChart.resize()
    //     }, 100)
    // }
    // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

      
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in FAN API" );
            });
  }

        $(document).on('click', '#see_more', function () {
           // $('mention_show').css('display','none');
           $('#see_more').css('display','none');
           $('.btn_post_tag_hide').css('display','inline');
           $('#see_less').css('display','inline');
        });
        $(document).on('click', '#see_less', function () {
          $('.btn_post_tag_hide').css('display','none');
            $('.btn_post_tag_show').css('display','inline');
            $('#see_more').css('display','inline');
            $('#see_less').css('display','none');
        });

            $(document).on('click', '#more', function () {
           // $('mention_show').css('display','none');
           $('#more').css('display','none');
           $('.btn_cmt_tag_hide').css('display','inline');
           $('#less').css('display','inline');
        });
        $(document).on('click', '#less', function () {
          $('.btn_cmt_tag_hide').css('display','none');
            $('.btn_cmt_tag_show').css('display','inline');
            $('#more').css('display','inline');
            $('#less').css('display','none');
        });


        $(document).on('change', '#tagGroup_filter', function () {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    $('#tag_filter').empty();
     $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gettagBytagGroup')}}", // This is the URL to the API
      data: { tagGroup_id:valueSelected,brand_id:GetURLParameter('pid')}
    })
    .done(function( data ) {
     for(var i in data) {

      $('#tag_filter').append($('<option>', {
            value: data[i]['id'],
            text: data[i]['name']
        }));
     }
     //  tag_pos=[];tag_neg=[];tag_neutral=[];tagsentiLabel=[];pos_percent_tag=[];neg_percent_tag=[];
     //    neutral_percent_tag=[];tag_qty_list=[];tagmarkpointlist=[];tagpercentmarkpointlist=[];
     // tag_qty_Label = [];tag_qty_seriesList=[];
     // tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
     // TagQty(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
     CMTtagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
    
    })
    .fail(function(xhr, textStatus, error) {
     
    });

   
});
$(document).on('change', '#tag_filter', function () {
      // tag_pos=[];tag_neg=[];tag_neutral=[];tagsentiLabel=[];pos_percent_tag=[];neg_percent_tag=[];
      //   neutral_percent_tag=[];tag_qty_list=[];tagmarkpointlist=[];tagpercentmarkpointlist=[];
      // tag_qty_Label = [];tag_qty_seriesList=[];
      CMTtagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
     
    
});


        function thousandsCurrencyFormat(num) 
        {
          // alert(num);

          if(num>1000) {

                var x = Math.round(num);
                var x_number_format = x.toFixed(2);
                var x_array = x_number_format.split(",");
                var x_parts = array('k', 'm', 'b', 't');
                var x_count_parts = x_array.length - 1;
                var x_display = x;
                var x_display = x_array[0] + (parseInt(x_array[1][0]) !== 0 ? '.' + x_array[1][0] : '');
                    x_display += x_parts[x_count_parts - 1];

                return x_display;

         }

            return num;
        }


      function InboundSentimentDetail(fday,sday){

    var brand_id = GetURLParameter('pid');
    let main = document.getElementById("fan-growth-chart");
    let existInstance = echarts.getInstanceByDom(main);
        if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }

      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

var sentiDetailChart = echarts.init(main);

    $("#fan-growth-spin").show();
      
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInboundSentiDetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id,periodType:'month',kw_search:$("#kw_search").val() }
    })
    .done(function( data ) {
     // console.log(data);
      var positive = [];
      var negative = [];
      var neutral=[];
      var sentimentLabel = [];

      var positive_total=0;
      var negative_total=0;
      var neutral_total=0;
      var all_total=0;


        for(var i in data) {//alert(data[i].mentions);
        positive.push(data[i].positive);
        negative.push(data[i].negative);
        neutral.push(data[i].neutral);
        sentimentLabel.push(data[i].periodLabel);
        
      }
      // alert(positive);


$.each(positive,function(){positive_total+=parseInt(this) || 0;});
$.each(negative,function(){negative_total+=parseInt(this) || 0;});
$.each(neutral,function(){neutral_total+=parseInt(this) || 0;});
all_total=positive_total+negative_total+neutral_total;
var positive_percentage=parseInt((positive_total/all_total)*100);
var negative_percentage= parseInt((negative_total/all_total)*100);
var neutral_percentage =  parseInt((neutral_total/all_total)*100);

          positive_percentage = isNaN(positive_percentage)?0:positive_percentage;
          negative_percentage = isNaN(negative_percentage)?0:negative_percentage;
          neutral_percentage = isNaN(neutral_percentage)?0:neutral_percentage;
         

    option = {
        color:colors,
      

 tooltip : {
            trigger: 'axis',
             /*formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
        console.log(rez); //quite useful for debug
        params.forEach(item => {
             console.log("hihi"); 
            console.log(item);value
            console.log(item.series.color); //quite useful for debug
            var xx = '<p>'   + colorSpan(item.series.color) + ' ' + item.seriesName + ': ' + item.data  + '</p>'
            rez += xx;
        });

        return rez;
    }*/

        },

        legend: {
            data:['Positive','Negative'],
           formatter: function (name) {
            if(name === 'Positive')
            {
                 return name + ': ' + positive_total;
            }
            return name  + ': ' + negative_total;

   
}
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
             axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
     var date = new Date(value);
     //  console.log(date);
      var texts = [date.getFullYear(), monthNames[date.getMonth()]];

    return texts.join('-');

}
    },
           data : sentimentLabel
        }
        ],
        yAxis : [
        {
            type : 'value',
            name: 'comment count',
        }
        ],
        series : [
        {
            name:'Positive',
            type:'bar',
            data:positive,
            barMaxWidth:30,
            color:colors[2],
            markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }/*,
            markLine : {
                data : [
                {type : 'average', name: 'average'}
                ]
            }*/
        },
        {
            name:'Negative',
          /*  type:effectIndex % 2 == 0 ? 'bar' : 'line',*/
            type:'bar',
            data:negative,
            barMaxWidth:30,
            color:colors[1],
            markPoint : {
              data : [
              {type : 'max', name: 'maximum'},
              {type : 'min', name: 'minimum'}
              ]
          }/*,
          markLine : {
            data : [
            {type : 'average', name : 'average value'}
            ]
        }*/
    }
    ]
};
$("#fan-growth-spin").hide();
sentiDetailChart.setOption(option, true), $(function() {
    // function resize() {
    //     setTimeout(function() {
    //         sentiDetailChart.resize()
    //     }, 100)
    // }
    // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

 sentiDetailChart.on('click', function (params) {
   // console.log(params);
   // console.log(params.name); // xaxis data = 2018-08
   // console.log(params.seriesName); //bar period name ="Positive"
   // console.log(params.value);//count
   // var pid = GetURLParameter('pid');
   var brand_id = GetURLParameter('pid');
   var admin_page_id=$('.hidden-chip').attr('id');
   var admin_page=$('.hidden-chip').val();

   var commentType ='';
   
   if(params.seriesName == 'Positive')
    commentType = 'pos'
    else
   commentType = 'neg'
   var default_page = $('.hidden-pg').val();
   var accountPermission = GetURLParameter('accountPermission');
  if(params.name !== 'minimum' && params.name !== 'maximum' )
   window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission, '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});
    
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
  
   function posting_status(fday,sday)

{
 // var brand_id = GetURLParameter('pid');
 var brand_id = GetURLParameter('pid');
 // var fday = moment(endDate).subtract(1, 'week');
 // var sday = endDate;
 // fday=fday.format('YYYY MM DD');
 // sday=sday.format('YYYY MM DD');
  var admin_page_id=$('.hidden-chip').attr('id');
  var admin_page=$('.hidden-chip').val();

    $( "#post_spin" ).show();
    $( "#cmt_spin" ).show();

  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getPostingStatus')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id}
    })
    .done(function( data ) {
      // console.log('posting status='+JSON.stringify(data));
     $( "#post_spin" ).hide();
     $( "#cmt_spin" ).hide();
    

        var total_post =data[5][0]['total'] ;
        var overall_pos =data[3][0]['ov_positive'] ;
        // alert(overall_pos);
        var overall_neg = data[3][0]['ov_negative'] ;
        var overall_neutral = data[3][0]['ov_neutral'] ;
   
        var all_post_total=parseInt(overall_pos)+parseInt(overall_neg)+parseInt(overall_neutral);
         // alert(all_post_total);
        var post_pos_pcent=parseFloat((overall_pos/all_post_total)*100).toFixed(2);
        var post_neg_pcent=parseFloat((overall_neg/all_post_total)*100).toFixed(2);
        var post_neutral_pcent=parseFloat((overall_neutral/all_post_total)*100).toFixed(2);
        // alert(post_pos_pcent.replace(".00", ""));
        post_pos_pcent = isNaN(post_pos_pcent)?'-':post_pos_pcent;
        post_neg_pcent = isNaN(post_neg_pcent)?'-':post_neg_pcent;
        post_neutral_pcent = isNaN(post_neutral_pcent)?'-':post_neutral_pcent;
        total_post = isNaN(total_post)?'-':total_post;
        
          $("#post_total").text(total_post==0?'-':total_post);
          $("#post_pos").text(post_pos_pcent==0?'- %':post_pos_pcent.replace(".00", "") + "%");
          $("#post_neg").text(post_neg_pcent==0?'- %':post_neg_pcent.replace(".00", "") + "%");
          $("#post_neutral").text(post_neutral_pcent==0?'- %':post_neutral_pcent.replace(".00", "") + "%");

        var pos =data[1][0]['positive'] ;
        var neg =data[1][0]['negative'] ;
        var neutral = data[1][0]['neutral'] ;
        var NA = data[1][0]['NA'] ;
            neutral=parseInt(neutral)+parseInt(NA);
            
        var total_comment =  data[1][0]['total'] ;
        var all_total=parseInt(pos)+parseInt(neg)+parseInt(neutral);
        var pos_pcent=parseFloat((pos/all_total)*100).toFixed(2);
        var neg_pcent=parseFloat((neg/all_total)*100).toFixed(2);
        var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);

        pos_pcent = isNaN(pos_pcent)?'-':pos_pcent;
        neg_pcent = isNaN(pos_pcent)?'-':neg_pcent;
        neutral_pcent = isNaN(neutral_pcent)?'-':neutral_pcent;
        total_comment = isNaN(total_comment)?'-':total_comment;

          $("#cmt_total").text(total_comment==0?'-':total_comment);
          $("#cmt_pos").text(pos_pcent==0?'- %':pos_pcent.replace(".00", "") + "%");
          $("#cmt_neg").text(neg_pcent==0?'- %':neg_pcent.replace(".00", "") + "%");
          $("#cmt_neutral").text(neutral_pcent==0?'- %':neutral_pcent.replace(".00", "") + "%");

        // var w_total_post = parseInt(data[5][0]['total'])=== 0 ? '-' : data[5][0]['total'] ;
        // var w_overall_pos = parseInt(data[3][0]['ov_positive'])=== 0 ? '-' : data[3][0]['ov_positive'] ;
        // var w_overall_neg = parseInt(data[3][0]['ov_negative'])=== 0 ? '-' : data[3][0]['ov_negative'] ;
        // var w_total_comment = parseInt(data[1][0]['total'])=== 0 ? '-' : data[1][0]['total'] ;
        // var w_pos = parseInt(data[1][0]['positive'])=== 0 ? '-' : data[1][0]['positive'] ;
        // var w_neg = parseInt(data[1][0]['negative'])=== 0 ? '-' : data[1][0]['negative'] ;


        // for(var i in data) {//alert(data[i].sentiment);
          
        //                   }
  

    })
    .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
}
// new function moved from comparisonin
function CMTtagsentimentData(fday,sday)
    {
    
      let main = document.getElementById("tag-sentiment-chart");
      let existInstance = echarts.getInstanceByDom(main);

      if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }


      var TagSentiChart = echarts.init(main);

        let main1 = document.getElementById("tag-percent-chart");
        let existInstance1 = echarts.getInstanceByDom(main1);
        
        if (existInstance1) {
            if (true) {
                echarts.init(main1).dispose();
            }
        }

      var TagPercentChart = echarts.init(main1);

      $('#tag_spin').show();
      //$("#fan-growth-spin").show();
     
     var  temp_tag_pos = [];
     var  temp_tag_neg = [];
     var temp_tag_neutral = [];
     var Page_id='';
     var imgurl ='';
      var pos_total = 0;
     var neg_total =0;
     var neutral_total =0;
     var all_total=0;
     var percent_total=0;
     var  tagpcentvalue = 100;
     // var LegendData = [];
     var tag_pos=[];var tag_neg=[];var tag_neutral=[];var pos_percent_tag=[];var neg_percent_tag=[];var neutral_percent_tag=[];var tag_qty_list=[];var tagsentiLabel=[];var tagmarkpointlist=[];var tagpercentmarkpointlist=[];
     var tag_id = $( "#tag_filter" ).val();
     var tags = $('#tag_filter option:selected').val();
     var page = $('.hidden-chip').val();
     var admin_page =[];


       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getTagSentiment')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:page,tag_id:tag_id,tags:tags}
    })
    .done(function(data) { 
   console.log('cmttagsentidata'+JSON.stringify(data));

       for(var i in data) 
        {
          
           
            if (tag_id == data[i].tagId) {

               
                temp_tag_pos.push(Math.round(data[i].positive));
                temp_tag_neg.push(Math.round(data[i].negative));
                temp_tag_neutral.push(Math.round(data[i].neutral));
               

               }
              
              Page_id = data[i].page_id;
              imgurl = data[i].imgurl;

        }
        admin_page.push(page);



        $.each(temp_tag_pos,function(){pos_total+=parseInt(this) || 0;});
        $.each(temp_tag_neg,function(){neg_total+=parseInt(this) || 0;});
        $.each(temp_tag_neutral,function(){neutral_total+=parseInt(this) || 0;});
        all_total = parseInt(pos_total)+parseInt(neg_total)+parseInt(neutral_total);
        var pos_percent_total=parseFloat((pos_total/all_total)*100).toFixed(2);
        var neg_percent_total=parseFloat((neg_total/all_total)*100).toFixed(2);
        var neutral_percent_total=parseFloat((neutral_total/all_total)*100).toFixed(2);
        pos_percent_total = isNaN(pos_percent_total)?0:pos_percent_total;
        neg_percent_total = isNaN(neg_percent_total)?0:neg_percent_total;
        neutral_percent_total = isNaN(neutral_percent_total)?0:neutral_percent_total;
        var percent_total = pos_percent_total+neg_percent_total+neutral_percent_total;
        if(pos_percent_total==0 && neg_percent_total==0 && neutral_percent_total==0)
        {
          tagpcentvalue = 0;
        }



                tag_pos.push(pos_total);
                tag_neg.push(neg_total);
                tag_neutral.push(neutral_total);
                pos_percent_tag.push(pos_percent_total);
                neg_percent_tag.push(neg_percent_total);
                neutral_percent_tag.push(neutral_percent_total);

                tag_qty_list.push({
                value:all_total,
                itemStyle:{color:colors},
          
        },

                           );
         

                // if(admin_page !== tagsentiLabel)
                //   tagsentiLabel.push(admin_page);
                tagmarkpointlist.push(
              {
                name : 'Have something', 
                value : all_total, 
                xAxis: all_total, 
                yAxis: admin_page, 
                // symbol:'image://https://graph.facebook.com/'+Page_id+'/picture?type=large', 
                symbol:'image://'+imgurl,
                symbolSize:30,

              }
                      )

                 tagpercentmarkpointlist.push(
              {
                name : 'Have something', 
                value : tagpcentvalue, 
                xAxis: tagpcentvalue, 
                yAxis: admin_page, 
                // symbol:'image://https://graph.facebook.com/'+Page_id+'/picture?type=large',
                symbol:'image://'+imgurl, 
                symbolSize:30,

              }
                      )



        option= null;
        option1=null;
          option= {
              color: colors_senti,

              tooltip: {
                  trigger: 'axis',
                  // axisPointer: {
                  //     type: 'cross'
                  // },
                
              },
                    grid: {
                    top:    60,
              
              left:   '5%',
              right:  '10%',
              bottom:  '5%',
                      containLabel: true
                  },
                        legend: {
                  data:['positive','negative','neutral'],
                
             
                   padding :0,
                 
                  
                  },


               toolbox: {
                      show : true,
                      feature : {
                          mark : {show: false},
                          dataView : {show: false, readOnly: false},
                          magicType : {show: false, type: ['line','bar']},
                          restore : {show: false},
                          saveAsImage : {show: false}
                      }
                  },

              xAxis: [
                  {
                    type: 'value',
                      name: 'Count',
                      position: 'left',
                      min:0,
                               axisLabel: {
                  formatter: function (e) {
                      return nFormatter(e,1,1);
                        }
                      }

               
                     
                  }
              ],
              yAxis: [
                  {
                      type: 'category',
                      name: 'Page',
                      axisLabel:{
                                  rotate:45,
                                  interval:0,
                                },
                      boundaryGap: true,

                      axisTick: {
                          alignWithLabel: true
                      },
                  
                      data: admin_page
              
               }
              ],
              series: [{
                      name:'positive',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[0],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Qty',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:tag_pos
                    
                  },{
                      name:'negative',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[1],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Qty',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:tag_neg,
                                     
                  },
                  {
                      name:'neutral',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[2],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Qty',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:tag_neutral,
                       markPoint: {
                         // symbol:'roundRect',
                         // symbol:'image://https://graph.facebook.com/393033680732657/picture?type=large',

                          symbolSize:[50, 50],// 容器大小
                          symbolOffset:[20,-5],//位置偏移
                         data : tagmarkpointlist,
                          label: {
                              offset: [0, 0],
                              color: '#ffffff',
                              formatter: [
                                ''
                              ].join('\n'),
                          }
                      },
                      
                    
                  }

                  ]
          };

          option1 = {
              color: colors_senti,

              tooltip: {
                  trigger: 'axis',
                  // axisPointer: {
                  //     type: 'cross'
                  // },
                
              },
                    grid: {
                    top:    60,
              
              left:   '5%',
              right:  '10%',
              bottom:  '5%',
                      containLabel: true
                  },
                        legend: {
                  data:['positive','negative','neutral'],
                
             
                   padding :0,
                 
                  
                  },


               toolbox: {
                      show : true,
                      feature : {
                          mark : {show: false},
                          dataView : {show: false, readOnly: false},
                          magicType : {show: false, type: ['line','bar']},
                          restore : {show: false},
                          saveAsImage : {show: false}
                      }
                  },

              xAxis: [
                  {
                      type: 'value',
                      name:'Count',
                       axisLabel: {
                            formatter: '{value}%'
                        },
                        max:100
                  }
              ],
              yAxis: [
                  {
                         type: 'category',
                      name: 'Page',
                      axisLabel:{
                                  rotate:45,
                                  interval:0,
                                },
                      boundaryGap: true,

                      axisTick: {
                          alignWithLabel: true
                      },
                  
                      data: admin_page
              
               }
              ],
              series: [{
                      name:'positive',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[0],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Percent',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:pos_percent_tag
                    
                  },{
                      name:'negative',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[1],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Percent',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:neg_percent_tag
                    
                  },
                  {
                      name:'neutral',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[2],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Percent',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:neutral_percent_tag,
                       markPoint: {
                       
                          symbolSize:[50, 50],// 容器大小
                          symbolOffset:[20,-5],//位置偏移
                         data : tagpercentmarkpointlist,
                          label: {
                              offset: [0, 0],
                              color: '#ffffff',
                              formatter: [
                                ''
                              ].join('\n'),
                          }
                      },
                    
                  }

                  ]
          };
 
        $( "#tag_spin" ).hide();
       
        TagSentiChart.setOption(option, true), $(function() {
        function resize() {
            setTimeout(function() {
                TagSentiChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });
       TagSentiChart.on('click', function (params) {
          if (params.event.handled !== true) {
       params.event.handled = true;
       // console.log("tags");
       // console.log(params);
       // console.log(params.name); // xaxis data = tag name
       // console.log(params.seriesName); //bar type name ="positive"
       // console.log(params.value);//count 8
       // alert(params.value);// count of sentiment
       // alert(params.seriesName); //positive or negative
       // alert(params.name); // Page Name (KBZ)
       // alert(params);
       var pid = GetURLParameter('pid');
       var source = GetURLParameter('source');
       var CmtType ='';
          if(params.seriesName == 'positive')
           CmtType = 'pos'
          else if(params.seriesName == 'negative')
           CmtType = 'neg'
          else
           CmtType = 'neutral'

         var tag_id = $('#tag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');

         var default_page = $('.hidden-pg').val();

       window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+tag_id+"&CmtType="+CmtType+"&fday="+GetStartDate() +"&sday="+ GetEndDate() +"&admin_page="+ params.name+"&default_page="+default_page, '_blank');
     }
    });
        TagPercentChart.setOption(option1, true), $(function() {
        function resize() {
            setTimeout(function() {
                TagPercentChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });

         TagPercentChart.on('click', function (params) {
       if (params.event.handled !== true) {
       params.event.handled = true;
      
       var pid = GetURLParameter('pid');
       var source = GetURLParameter('source');
       var CmtType ='';
         if(params.seriesName == 'positive')
         CmtType = 'pos'
        else if(params.seriesName == 'negative')
         CmtType = 'neg'
        else
          CmtType = 'neutral'


         //var tag_id = $( "#tag_filter" ).val();
         var tag_id = $('#tag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');

       var default_page = $('.hidden-pg').val();
       window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+tag_id+"&CmtType="+CmtType+"&fday="+GetStartDate() +"&sday="+ GetEndDate() +"&admin_page="+ params.name+"&default_page="+default_page , '_blank');
     }
    });
         //  var arrayLength=brand_id_arr.length;
         // summary_sr_of_brand = summary_sr_of_brand +1;
         // if(summary_sr_of_brand<arrayLength )
         // {
         //  tagsentimentData(fday,sday,brand_id_arr[summary_sr_of_brand],summary_sr_of_brand);
         //  $("#fan-growth-spin").show();

         // }
         // else
         // {
         //   $("#fan-growth-spin").hide();
         // }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
// end here
function tagsentimentData(fday,sday){//alert(fday);
    let main = document.getElementById("tag-senti-chart");
    let existInstance = echarts.getInstanceByDom(main);
        if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }

 var admin_page_id=$('.hidden-chip').attr('id');
 var admin_page=$('.hidden-chip').val();
 var sentiChart = echarts.init(main);


$("#tag-senti-spin").show();
        
    // var brand_id = GetURLParameter('pid');
    var brand_id = GetURLParameter('pid');
       /* var brand_id = 22;*/
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagSentiment')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id }
    })
    .done(function( data ) {
    var xAxisData = [];
    var data1 = [];
    var data2 = [];
   var tag_arr= genData(data);
   var tag_Obj=tag_arr.tag_id;

for(var i in data) 
        {
    xAxisData.push(data[i].tagLabel);
    data1.push(Math.round(data[i].positive));
    if(data[i].negative >0 )
    {
    data2.push(Math.round(-data[i].negative));
    }
    else
    {
     data2.push(0);
    }
  
 
}


var itemStyle = {
    normal: {
        fontFamily: 'mm3',
    },
    emphasis: {
        barBorderWidth: 1,
        shadowBlur: 10,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        shadowColor: 'rgba(0,0,0,0.5)'
    }
};


option = {
      color:colors,
      textStyle: {
                    
                    fontFamily: 'mm3',},
    backgroundColor: 'rgba(0, 0, 0, 0)',
     dataZoom:[  {
            type: 'slider',
            show: true,
            xAxisIndex: [0],
            start: 70,
            end: 100
        },
         {
            type: 'inside',
            xAxisIndex: [0],
            start: 1,
            end: 35
        }
    ],
        calculable : true,
    legend: {
        
        data: ['positive', 'negative'],
           x: 'center',
             y: 'top',
              padding :0,
    }/*,
    brush: {
        toolbox: ['rect', 'polygon', 'lineX', 'lineY', 'keep', 'clear'],
        xAxisIndex: 0
    }*/,

    toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['stack','tiled']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },
    tooltip: {   textStyle: {
                    
                    fontFamily: 'mm3',}
                },
    calculable : true,
    dataZoom : {
        show : true,
        realtime : true,
        start : 0,
        end : 100
    },
    xAxis: {
        data: xAxisData,
        axisLabel:{
      rotate:45
    },
        name: 'Tags',
        silent: false,
        axisLine: {onZero: true},
        splitLine: {show: false},
        splitArea: {show: false},
       /*  axisLabel: {
            textStyle: {
                color: '#fff'
            }
        },*/
    },
    yAxis: {
        inverse: false,
        splitArea: {show: false}
    },
   grid: {
            top: '12%',
            left: '3%',
            right: '5%',
            containLabel: true
        },
  /*  visualMap: {
        type: 'continuous',
        dimension: 1,
        text: ['High', 'Low'],
        inverse: true,
        itemHeight: 200,
        calculable: true,
        min: -2,
        max: 6,
        top: 60,
        left: 10,
        inRange: {
            colorLightness: [0.4, 0.8]
        },
        outOfRange: {
            color: '#bbb'
        },
        controller: {
            inRange: {
                color: '#2f4554'
            }
        }
    },*/
    series: [
        {
            name: 'positive',
            type: 'bar',
            stack: 'one',
            color:colors[2],
            barMaxWidth:30,
            itemStyle: itemStyle,
            data: data1
        },
        {
            name: 'negative',
            type: 'bar',
            stack: 'one',
            color:colors[1],
            barMaxWidth:30,
            itemStyle: itemStyle,
            data: data2
        }
    ]
};
function genData(data) {
    var tag_array={};
    var i = 0;
     for(var key in data) 
        {
         var name =data[key].tagLabel;
         var id=data[key].tagId;
               
         tag_array[name]=id;
              
        }

     return {
             tag_id:tag_array,
       
        
    };

    
};

$("#tag-senti-spin").hide();
/*categoryChart.on('brushSelected', renderBrushed);

function renderBrushed(params) {
    var brushed = [];
    var brushComponent = params.batch[0];

    for (var sIdx = 0; sIdx < brushComponent.selected.length; sIdx++) {
        var rawIndices = brushComponent.selected[sIdx].dataIndex;
        brushed.push('[Series ' + sIdx + '] ' + rawIndices.join(', '));
    }
}
*/
    sentiChart.setOption({
        title: {
            backgroundColor: '#333',
          /*  text: 'SELECTED DATA INDICES: \n' + brushed.join('\n'),*/
            bottom: 0,
            right: 0,
            width: 100,
            textStyle: {
                fontSize: 12,
                color: '#fff'
            }
        }
    });

    sentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            sentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    // alert($('.hidden-pg').val());
   sentiChart.on('click', function (params) {
   // console.log("tags");
   // console.log(params);
   // console.log(params.name); // xaxis data = tag name
   // console.log(params.seriesName); //bar type name ="positive"
   // console.log(params.value);//count 8
   // var pid = GetURLParameter('pid');
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var admin_page_id=$('.hidden-chip').attr('id');
   var admin_page=$('.hidden-chip').val();
   var CmtType ='';
   if(params.seriesName == 'positive')
    CmtType = 'pos'
    else
   CmtType = 'neg'

     // var senti_admin_page = $('#hidden-chip').val();
     var default_page = $('.hidden-pg').val();
     // alert('default_page');

   // window.open("{{ url('relatedcomment?')}}" +"pid="+ pid +"&source="+ source+"&fday="+ GetStartDate()+
   // "&sday="+GetEndDate()+"&type="+ params.seriesName +"&tag="+ params.name +"&from_graph=sentiment&admin_page="+admin_page  , '_blank');
   var accountPermission = GetURLParameter('accountPermission');
   window.open("{{ url('relatedcomment?')}}" +"pid="+ pid +"&source="+GetURLParameter('source')+"&search_tag="+tag_Obj[params.name]+"&CmtType="+CmtType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission, '_blank');
});


   

  
    })
     .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

  }
  // alert(GetURLParameter('accountPermission'));
  $('input[type=radio][name=options]').change(function() {

    if (this.value == 'page') {
     var date_preset= Calculate_DatePreset(label);
        PageGrowth(GetStartDate(),GetEndDate(),date_preset);
        
    }
    else if (this.value == 'sentiment') {
        InboundSentimentDetail(GetStartDate(),GetEndDate());
    }
    
});
  $(".btnComment").click(function(e){
    var commentType = $(this).attr('value');
    var default_page = $('.hidden-pg').val();
    var admin_page_id=$('.hidden-chip').attr('id');
    var admin_page=$('.hidden-chip').val();

   var accountPermission = GetURLParameter('accountPermission');
    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
})
$(".btnPost").click(function(e){
    var postType = $(this).attr('value');
    var admin_page_id=$('.hidden-chip').attr('id');
    var admin_page=$('.hidden-chip').val();
    var accountPermission = GetURLParameter('accountPermission');
    window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&overall="+postType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&accountPermission="+accountPermission , '_blank');
})
  $(document).on('click', '.btn_frequent_tag', function() {
     var admin_page_id=$('.hidden-chip').attr('id');
     var admin_page=$('.hidden-chip').val();
     var TagName = $(this).attr('value');
     var default_page = $('.hidden-pg').val();
        
        var accountPermission=GetURLParameter('accountPermission');
      window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
  })
    $(document).on('click', '.btn_post_tag', function() {
     var TagName = $(this).attr('value');
     var default_page = $('.hidden-pg').val();
     var admin_page_id=$('.hidden-chip').attr('id');
     var admin_page=$('.hidden-chip').val();
      var accountPermission = GetURLParameter('accountPermission');
      // alert(accountPermission); 
      window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
  })
  
  //local function

  function nFormatter(num, digits ,get_zero=0) {
    if(num<=0 && get_zero==0)
      {
        return '-';
      }
      else
      {
  var si = [
    { value: 1, symbol: "" },
    { value: 1E3, symbol: "k" },
    { value: 1E6, symbol: "M" },
    { value: 1E9, symbol: "G" },
    { value: 1E12, symbol: "T" },
    { value: 1E15, symbol: "P" },
    { value: 1E18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
      }
}

  function numberWithCommas(n) {
      var parts=n.toString().split(".");
      var res = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
      if(res == '')
        res = 0

      return res;
  }

    function kFormatter(num) {
      return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
  }
    function readmore(message){
        // alert("hi hi ");
          var string = String(message);
          var length = string.length; 
           // alert(length);
                  if (length > 500) {
            // alert("length is greater than 500");

              // truncate

              var stringCut = string.substr(0, 500);
               // alert(stringCut);
              // var endPoint = stringCut.indexOf(" ");

              //if the string doesn't contain any space then it will cut without word basis.
               
              // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
              string =stringCut.substr(0,length);
              // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
              // alert(string);
          }
          return string;


          }

          

    });


      </script>
      <style type="text/css">

        .reaction-weight{
          font-weight: 900;
 
          text-align: center;
        }
       
        .img-container {
  position: relative;
}

.img-container:after {
  content: " ";
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 99;
}

table.fixedHeader-floating {
     clear: both;
     position: fixed;
    top: 114px !important;
    z-index:5;
     top :0;
     left:0 !important;
     width: 100% !important;
  /*   margin: 0 auto;
       padding: 0 15px;*/
   /*    overflow-x: hidden;
        overflow-y: auto;*/
    
      box-sizing: border-box;
  /*
    background: red;*/
  /*  left:150px !important;*/
  /*  margin-right:30px;*/
   
  /*  width: 80% !important;*/
    /*background: transparent;*/
  } 
table.dataTable tbody tr td {
    word-wrap: break-word;
    word-break: break-all;
}

  .tabcontrolHeader {
     clear: both;
     position: fixed;
    top:120px !important;
    z-index:20;
   
  }


  .myHeaderContent
  {
     margin-right:300px;
  }
  .myHeaderChip
  {
    padding-right:100px;
  }
  .myHeaderScoll
  {
     padding-right:300px;
  }

  .dataTables_wrapper {
      padding-top: 0px;
  }
  img {
      border-style: none;
  }
  #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }


  /*for reply comment toast*/
    #rp-snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    /*bottom: 30px;*/
    font-size: 17px;
  }
  #rp-snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }
    /*for reply comment toast*/
    #rptag-snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    /*bottom: 30px;*/
    font-size: 17px;
  }
  #rptag-snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
  .btn-secondary:not(:disabled):not(.disabled).active, .btn-secondary:not(:disabled):not(.disabled):active, .show>.btn-secondary.dropdown-toggle {
    color: #fff !important;
    background-color: #545b62;
    border-color: #4e555b;
}
.btn {
    padding: 2px 6px;
    font-size: 14px;
    cursor: pointer;
}


      </style>
  <link href="{{asset('css/own.css')}}" rel="stylesheet">
  @endpush

  <!-- table.fixedHeader-floating {
     clear: both;
     position: fixed;
    top: 200px !important;
    z-index:5;
     top :0;
     left:0 !important;
     width: 100% !important;
  /*   margin: 0 auto;
       padding: 0 15px;*/
   /*    overflow-x: hidden;
        overflow-y: auto;*/
    
      box-sizing: border-box;
  /*
    background: red;*/
  /*  left:150px !important;*/
  /*  margin-right:30px;*/
   
  /*  width: 80% !important;*/
    /*background: transparent;*/
  } -->
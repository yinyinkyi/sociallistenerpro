<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
       
        <link href="https://bsldev.baganintel.com/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://bsldev.baganintel.com/css/style.css" rel="stylesheet">
    </head>
    <body>
<!--       <button type="button" id="print" style="padding:7px;" class="btn waves-effect waves-light btn-block btn-info noprint">Print</button>
 -->      
 <a href="{{ route('pdfview',['download'=>'pdf']) }}">Download PDF</a>

      <center>
        <div class="card" style="margin:auto;border-radius: 10px;background-color: #fafafa">
          <div class="card-body">
            <table style="max-width: 100% !important;" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#fafafa">
              <tr>
                <th><div style="font-size:140%;font-weight: bolder"> <b>{{ $reportType }}</b></div>
                    @if($today_date == '')
                    <span style="font-size:80%;" class="daterange">{{ $from_date }}&nbsp; To &nbsp;{{ $to_date }}</span>
                </th>
                <th>
                  @else
                  <span style="font-size:80%;" class="daterange">{{$today_date}}</span>
                </th>
                <th>
                 @endif
                  <img src="{{ $imgurl }}" style="width:20%;height:20%;border: none;"></th>
              </tr>
              <tr>
                <td>
                  <div class="card-title" style="font-weight: bold;font-size:90%">Page Summary</div>
                    <div class="btn-group btn-group-lg " role="group" aria-label="Basic example" style="margin-bottom: 2%;">
                      <div class="btn-group" role="group">
                        <button type="button"  style="font-size: 80%;text-align: left" class="btn btn-secondary">Page Growth</button>
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button"  style="font-size: 80%;" class="btn btn-secondary" id="pageGrowth">{{ $page_growth }}</button>
                      </div>
                    </div><br>
                    <div class="btn-group btn-group-lg" role="group" aria-label="Basic example" style="margin-bottom: 2%;">
                      <div class="btn-group" role="group">
                        <button type="button"  style="font-size: 80%;" class="btn btn-secondary">Post</button>
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button"  style="font-size: 80%;" class="btn btn-secondary" id="post">{{ $post_count }}</button>
                      </div>
                    </div><br>
                    <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                      <div class="btn-group" role="group">
                        <button type="button"  style="font-size: 80%;" class="btn btn-secondary">Comment</button>
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button"  style="font-size: 80%;" class="btn btn-secondary" id="comment">{{ $comment_count }}</button>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="card-title" style="font-weight: bold;padding-top:3vmin;font-size:90%">Mentions</div>
                    <span style="font-size:85%">You were mentioned <b><span style="font-weight: bolder;" id="mentionTotal">  {{ $mentionTotal }} </span></b> times last week!</span>
                    <br> <br>
                    <b> {{ $articleTotal }}  </b> <span style="font-size:85%" id="articleTotal">  Articles </span><br>
                    <b> {{ $postTotal }}  </b> <span style="font-size:85%" id="postTotal" > Posts </span><br>
                    <b>  {{ $cmtTotal }} </b>  <span style="font-size:85%" id="cmtTotal"> Comments </span> 
                </td>
              </tr>
              <tr>
                <td>
                  <div class="card-title" style="font-weight: bold;font-size:90%;">Added Post Topics</div>
                    <div class="post_tag_count"> 
                      @if($pTagName1 != '')
                      <button type="button" style="font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName1 }}<span class="label label-light-info" style="font-size: 80%">{{ $pTagCount1 }}</span></button>
                      @else
                      <span>You have no new <span style="font-weight: bolder;">Post Topics</span> last week!</span>
                      @endif
                      @if($pTagName2 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName2 }}<span class="label label-light-info" style="font-size: 80%">{{ $pTagCount2 }}</span></button>
                      @endif
                      @if($pTagName3 != '')
                  
                       <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName3 }}<span class="label label-light-info" style="border-radius: 10vmin;font-size: 80%">{{ $pTagCount3 }}</span></button>
                      @endif
                      @if($pTagName4 != '')
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName4 }}<span class="label label-light-info" style="font-size: 80%">{{ $pTagCount4 }}</span></button>
                      @endif
                      @if($pTagName5 != '')
                 
                      <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $pTagName5 }}<span class="label label-light-info" style="font-size: 80%">{{ $pTagCount5 }}</span></button>
                      @endif
                    </div>
                </td>
                <td>
                  <div class="card-title" style="font-weight: bold;padding-bottom:1vmin;font-size:90%">Added Comment Topics</div>
                    <div class="tag_count"> 
                       @if($cTagName1 != '')
                        <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName1 }}<span class="label label-light-info" style="font-size: 80%">{{ $cTagCount1 }}</span></button>
                        @else
                        <span>You have no new <span style="font-weight: bolder;">Comment Topics</span> last week!</span>
                        @endif
                        @if($cTagName2 != '')
                        <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName2 }}<span class="label label-light-info" style="font-size: 80%">{{ $cTagCount2 }}</span></button>
                        @endif
                        @if($cTagName3 != '')
                   
                         <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName3 }}<span class="label label-light-info" style="font-size: 80%">{{ $cTagCount3 }}</span></button>
                        @endif
                        @if($cTagName4 != '')
                        <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName4 }}<span class="label label-light-info" style="font-size: 80%">{{ $cTagCount4 }}</span></button>
                        @endif
                        @if($cTagName5 != '')
                    
                        <button type="button" style="margin:0.1rem;font-size: 80%" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_post_tag btn_post_tag_show">{{ $cTagName5 }}<span class="label label-light-info" style="font-size: 80%">{{ $cTagCount5 }}</span></button>
                        @endif
                    </div>
                     
                </td>
              </tr>
              <tr>
                <td>
                  <div class="card-title"  style="font-weight: bolder;font-size:90%;"><b>Brand Sentiment</b></div>
                    <h4 class="card-title m-b-0" style="font-size:90%;font-weight: bold">Posts</h4>
                      <h1 class="card-title m-t-10" style="font-size:130%;font-weight: bolder;" id="post_total">{{ $post_count }}</h1>
                        <ul class="list-inline m-b-0" style="margin-left: -11px;">
                          <li>
                              <h6 id="post_neg" class="text-red text-center" style="font-size:83%">{{ $post_neg_pcent }}%</h6>
                              <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnPost" style="margin:-6px" value="neg">Negative</button></div> </li>
                          <li>
                               <h6 id="post_neutral" class="text-warning text-center" style="font-size:83%">{{ $post_neutral_pcent }}%</h6>
                              <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnPost"  value="neutral">Neutral</button></div> </li>
                          <li>
                               <h6 id="post_pos" class="text-success text-center" style="font-size:83%">{{ $post_pos_pcent }}%</h6>
                              <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnPost"  value="pos">Positive</button></div> </li>
                        </ul>
                </td>
                <td>
                  <div class="card-title"  style="font-weight: bold;font-size:100%">&nbsp;</div>
                    <h4 class="card-title m-b-0" style="font-size:90%;font-weight: bold">Comments</h4>
                      <h1 class="card-title m-t-10" style="font-size:130%;font-weight: bolder" id="post_total">{{ $comment_count }}</h1>
                        <ul class="list-inline m-b-0" style="margin-left: -11px;">
                          <li>
                              <h6 id="post_neg" class="text-red text-center" style="font-size:83%">{{ $cmt_neg_pcent }}%</h6>
                              <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnPost" style="margin:-6px" value="neg">Negative</button></div> </li>
                          <li>
                               <h6 id="post_neutral" class="text-warning text-center" style="font-size:83%">{{ $cmt_neutral_pcent }}%</h6>
                              <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnPost"  value="neutral">Neutral</button></div> </li>
                          <li>
                               <h6 id="post_pos" class="text-success text-center" style="font-size:83%">{{ $cmt_pos_pcent }}%</h6>
                              <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnPost"  value="pos">Positive</button></div> </li>
                        </ul>
                </td>
              </tr>
            </table> 


            <div class="form-group" style="">
              <a href='https://my.baganintel.ai' target="_blank"><button style="display: block;background: #1e88e5;color:#fff;font-size: 80%;" class="form-control btn-success">Compare with competitors</button></a>
            </div>
            <div style="text-align:center;font-size:60%;">
               Stay ahead of the competition with BI Miner<br>
               <span ><a href="https://bsldev.baganintel.com" target="_blank">News</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://bsldev.baganintel.com" target="_blank">Help Center</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://bsldev.baganintel.com" target="_blank">Unsubscribe</a></span><br>
               <a href="https://bsldev.baganintel.com" target="_blank"><img src="https://bsldev.baganintel.com/assets/images/fb.png" style=""></a><a href="https://bsldev.baganintel.com" target="_blank"><img src="https://bsldev.baganintel.com/assets/images/linkedin.png" style=""></a>
            </div>
          </div>
        </div>
      </center>

      <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
      <script type="text/javascript">

          $(document).ready(function() {

              $("#print").click(function(){
                $('#print').hide();
                var daterange = $('.daterange').text();
                var date_str = daterange.replace(/\s/g,'');
                document.title = "BI Miner Report-"+date_str;
                window.print();
                $('#print').show();
            }); 
        })
      </script>
      <style type="text/css" media="all">

        /*to remove page title*/
        @page { size: auto;  margin: 0mm; }

        @media print{
           .p-reportType{
              font-size:13px !important;
            }
            .p-subtitle{
              font-size:10px !important;
            }
            .p-text{
              font-size:;
            }

        }
      </style>
    </body>
  </html>

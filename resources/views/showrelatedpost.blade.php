@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('admin_page_filter')
<li class="nav-item" style="display: none"> 
<div class="btn-group">
        <select id="admin_page_filter" class="form-control custom-select">
        <option value="">Admin Pages</option>
        @if (isset($ownpage))
        @foreach($ownpage as $ownpage)
        <option value="{{$ownpage}}" id="{{$ownpage}}" >{{$ownpage}}</option>
        @endforeach
        @endif
                                                        
                                                   
      </select>
 
                                        </div>
                         </li>
@endsection
@section('content')

              
               
                  <!-- Row -->
<header class="" id="myHeader">
                  <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                      <ul class="nav nav-tabs profile-tab" role="tablist" id="tabHeader" style="border-bottom:none;padding-left:20px">
                           <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#post" role="tab"> <span id="title_post">Posts</span></a> </li>
                          <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#visitorpost" role="tab"> <span id="title_visitor">visitor Posts</span></a> </li>
                         
                     
                                
                            </ul>
                             <div id="snackbar"><div id="snack_desc">A notification message..</div></div>
                    </div>
                      <input type="hidden" value="{{ auth()->user()->role }}" class ="hidden-role"/>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <!--     <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div> -->
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
           </header>

            <div class="row">
                <div class="col-12">
                  <div class="tab-content">
                      <div class="tab-pane active" id="post" role="tabpanel">
                          <!-- reaction , share ,comment count  -->
<!--                           <div class="row" style="margin-right:-5px;margin-left:-5px" id="top-div">
                            <div class="col-4">
                                <div class="card card-inverse card-info">
                                    <div class="box bg-info text-center">
                                       <div style="display:none"  align="center" id="reaction_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                        <h1 class="font-light text-white" id='reaction_total'>0</h1>
                                        <h6 class="text-white"> Reactions</h6>
                                    </div>
                                </div>
                            </div>
                    
                            <div class="col-4" >
                                <div class="card card-primary card-inverse">
                                    <div class="box text-center">
                                       <div style="display:none"  align="center" id="comment_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                       <a href="javascript:void(0)" class="link_mention_post"><h1 class="font-light text-white" id="comment_total">0</h1></a>
                                       <a href="javascript:void(0)" class="link_mention_post"><h6 class="text-white"> Comments</h6></a>
                                    </div>
                                </div>
                            </div>
                  
                            <div class="col-4" >
                                <div class="card card-inverse card-success">
                                    <div class="box text-center">
                                       <div style="display:none"  align="center" id="share_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                       <a href="javascript:void(0)" class="link_mention_cmt"><h1 class="font-light text-white" id="share_total">0</h1></a>
                                        <a href="javascript:void(0)" class="link_mention_cmt"><h6 class="text-white"> Shares</h6></a>
                                    </div>
                                </div>
                            </div>
                          </div> -->       
                         <!-- reaction , share ,comment count  -->
                          <div class="card">
                                <div class="card-body b-t collapse show">
                                <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <div class="table-responsive">
                                  <table id="tbl_post" class="table" style="margin-top:4px;">
                                  <thead>
                                    <tr>
                                      <th></th>
                                      <!-- <span class="reaction" style="color:#67757c"></span> -->
                                      <th>Reactions <br> <span class="reaction"></span></th>
                                      <th>Comments <br> <span class="comment"></span></th>
                                      <th>Shares <br> <span class="share"></span></th>
                                      <!-- <th>Overall</th> -->
                                    </tr>
                                  </thead>

                                </table>
                                </div>
                              </div>
                          </div>
                        </div>
                                <div class="tab-pane" id="visitorpost" role="tabpanel">
                                  <div class="card">
                                <div class="card-body b-t collapse show">
                                <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <div class="table-responsive">
                                  <table id="tbl_visitorpost" class="table" style="margin-top:4px;">
                                  <thead>
                                    <tr>
                                      <th></th>
                                      <th>Reactions</th>
                                      <th>Comments</th>
                                      <th>Shares</th>
                                      <!-- <th>Overall</th> -->
                                    </tr>
                                  </thead>

                                </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        </div>
                    </div>
                    </div>
                <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="postModal">Post Detail</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body post_data">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" id="btnSeeComment" class="btn btn-green waves-effect text-left" data-dismiss="modal">See Comments</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
               
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
   <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
/*global mention*/
var mention_total;
var mentionLabel = [];
var mentions = [];
/*Bookmark Array*/
var bookmark_array=[];
var bookmark_remove_array=[];
/*global sentiment*/
var positive = [];
var negative = [];
var sentimentLabel = [];
var positive_total=0;
var negative_total=0;
var campaignGroup=[];
var  colors=["#1e88e5","#dc3545","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

$(document).ready(function() {



    //initialize
 $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
/* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

 $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

      var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

    startDate = moment().subtract(1, 'month');
    endDate = moment();

           $('.singledate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        },function(date) {
          endDate=date;
          var id =$('input[type=radio][name=period-radio]:checked').attr('id');
          if(id==="period-week")
         {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
         }
         else
         {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
         }
         // alert(startDate);
         // alert(endDate);
         var admin_page=$("#admin_page_filter").val();

         ChooseDate(startDate,endDate,admin_page,'');
       
      });



 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Dashboard'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}


   function ChooseDate(start, end,admin_page='',label='') {//alert(start);
    if(start !== '')
    {
      $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
          getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          getAllVisitorPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));

          getRelatedStatus(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
    }
    else
    {
      
          getAllPost('','','');
          getAllVisitorPost('','','');
    }
   
       
         
           
        }

        function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}

 function getAllPost(fday='',sday='',filter_overall='')
    {
      $(".popup_post").unbind('click');
        $(".see_comment").unbind('click');
 if(filter_overall == '' )
      {
        filter_overall=GetURLParameter('overall');
    
      }
      // alert(GetURLParameter('period'));
 var oTable = $('#tbl_post').DataTable({
       // "responsive": true,
        "paging": true,
         "pageLength": 20,
        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": false,
        "destroy": true,
        "ordering": true   ,
        "autoWidth": false,
        "order": [],
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        
        "ajax": {
          "url": '{{ route('getInboundPost') }}',
          "dataSrc": function(res){
            // console.log(res.data[0].reaction_total);
            // console.log(res.data[0].share_total);
            // console.log(res.data[0].comment_total);
            // console.log("tbl_post res"+JSON.stringify(res));
            // var hiddenValue = $('#reaction_total', $(res)).val();
             // var hiddenValue = res.find('#reaction_total').last().data();
              // var parsedResponse = $.parseHTML(res);
              // alert(parsedResponse);
             // var hiddenValue = $(parsedResponse).filter('#reaction_total').val();
             // hiddenValue = JSON.stringify(hiddenValue);
             // var parsed = $.parseHTML(res);
            // var hiddenValue = $(res).find("#reaction_total").html();
            // var hiddenValue = $("#reaction_total", parsed).html();
            // var hiddenValue =  $(parsed).filter('#reaction_total').attr("id");
             // var hiddenValue = $(parsed).find("#reaction_total").html();  
               // var hiddenValue =  $(res).find('#reaction_total');
                // var hiddenValue = $('#reaction_total', $(res)).val();
        //         var $response=$(res);
        // var hiddenValue = $response.filter('#reaction_total').val();
         //console.log(hiddenValue);
              //var count = res.data.length;
              //alert(count);
              console.log(res);
              $('.reaction').text('('+res.data[0].reaction_total+')');
              $('.comment').text('('+res.data[0].comment_total+')');
              $('.share').text('('+res.data[0].share_total+')');
              document.getElementById('title_post').innerHTML = 'Posts ('+res.recordsTotal+")";
              return res.data;
            },
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
           "data": {
            "fday": fday,
            "sday": sday,
            "admin_page":GetURLParameter('admin_page'),
            "brand_id": GetURLParameter('pid'),
            "filter_overall":filter_overall,
            "search_tag":GetURLParameter('search_tag'),
            "period":GetURLParameter('period'),
            "tag_id":GetURLParameter('tag_id'),
          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
        $('.tbl_post thead tr').removeAttr('class');
        },
           "drawCallback": function() {
               $('.select2').select2();
            },

        columns: [
        {data: 'post', name: 'post',"orderable": false},
        {data: 'reaction', name: 'reaction', orderable: true,className: "text-center"},
        {data: 'comment', name: 'comment', orderable: true, className: "text-center"},
        {data: 'share', name: 'share', orderable: true, className: "text-center"},
        // {data: 'overall', name: 'overall', orderable: true,className: "text-center"},


        ]
        
      }).on('change', '.edit_tag', function (e) {
      var hidden_pg  = GetURLParameter('admin_page');
      var default_pg = GetURLParameter('default_page');
      var user_role = $('.hidden-role').val();
                  if(user_role == 'Admin' || hidden_pg === default_pg)
                  {
                        if (e.handled !== true) {


      var senti_id = $(this).attr('id');
      var post_id  = senti_id.replace("tags_","");
      var sentiment = $('#sentiment_'+post_id+' option:selected').val();
      var emotion = $('#emotion_'+post_id+' option:selected').val();
      var tags = $('#tags_'+post_id+' option:selected').map(function () {
          return $(this).text();
      }).get().join(',');

      var tags_id = $('#tags_'+post_id).val();
       var brand_id = GetURLParameter('pid');
    
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setUpdatedPredict") }}',
           type: 'POST',
           data: {id:post_id,brand_id:brand_id,sentiment:sentiment,emotion:emotion,tags:tags,checked_tags_id:tags_id,type:'inbound_post'},
           success: function(response) {
            
             // alert(response)
            if(response>=0)
            {
          //         swal({   
          //     title: "Updated!",   
          //     text: "Done!",   
          //     timer: 1000,   
          //     showConfirmButton: false 
          // });
          var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Tag Changed!!');
                        x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }


          }
              });
             
            e.handled = true;
            
         }

                  }
             

     else
  {
      var x = document.getElementById("snackbar")
      $("#snack_desc").html('');
      // $("#snack_desc").html('Sorry you have no permission to change sentiment for this page!!');
       $("#snack_desc").html('You are not allowed to edit this comment . Changes will not be saved !!! ');
        x.className = "show";

        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
         

        
       
      
          
    }).on('click', '.popup_post', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".post_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getRelatedPosts") }}',
         type: 'GET',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

               for(var i in data) {//alert(data[i].message);
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
                 var page_name=data[i].page_name;

                // if(isNaN(page_name)==false) //it is number
                // {
                 
                //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
                //   page_name = b[0];
                 
                   
                // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
                
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large'; 
        
         page_Link= 'https://www.facebook.com/' + page_name ;   
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      data[i].message +
                    
                      '</p>';
                    if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }
          
           
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.see_comment', function (e) {//alert("hihi");
  if (e.handled !== true) {
    var attr_id = $(this).attr('id');
    var post_id  = attr_id.replace("seeComment_","");
  
    var admin_page = GetURLParameter('admin_page');
    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid')+"&admin_page="+admin_page+"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() , '_blank');
     
     
        }
        


          e.handled = true;
       }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';

                                       
  for(var i in campaignGroup) 
          {
            if(i==0)
            {
              htmltext +='<fieldset class="controls">'+
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +='<fieldset class="controls">'+
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
             title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
    
        
       
          }
          
           

            e.handled = true;
         });
     

     new $.fn.dataTable.FixedHeader( oTable );
   // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
  }
  function getAllVisitorPost(fday='',sday='',filter_overall='')
    {
      $(".popup_post").unbind('click');
        $(".see_comment").unbind('click');
 if(filter_overall == '' )
      {
        filter_overall=GetURLParameter('overall');
    
      }
 var oTable = $('#tbl_visitorpost').DataTable({
       // "responsive": true,
        "paging": true,
         "pageLength": 20,
        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": false,
        "destroy": true,
        "ordering": true   ,
        "autoWidth": false,
        "order": [],
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
       
        "ajax": {
          "url": '{{ route('getInboundPost') }}',
          "dataSrc": function(res){
              //var count = res.data.length;
              //alert(count);
              document.getElementById('title_visitor').innerHTML = 'Visitor Posts ('+res.recordsTotal+")";
              return res.data;
            },
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
           "data": {
            "fday": fday,
            "sday": sday,
            "admin_page":GetURLParameter('admin_page'),
            "brand_id": GetURLParameter('pid'),
            "visitor" : 1,
            "filter_overall":filter_overall,
          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
        $('.tbl_visitorpost thead tr').removeAttr('class');
        },
 

        columns: [
        {data: 'post', name: 'post',"orderable": false},
        {data: 'reaction', name: 'reaction', orderable: true,className: "text-center"},
        {data: 'comment', name: 'comment', orderable: true, className: "text-center"},
        {data: 'share', name: 'share', orderable: true, className: "text-center"},
        // {data: 'overall', name: 'overall', orderable: true,className: "text-center"},


        ]
        
      }).on('click', '.popup_post', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".post_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getRelatedPosts") }}',
         type: 'GET',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

               for(var i in data) {//alert(data[i].message);
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
                 var page_name=data[i].page_name;

                // if(isNaN(page_name)==false) //it is number
                // {
                 
                //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
                //   page_name = b[0];
                 
                   
                // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
                
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large'; 
        
         page_Link= 'https://www.facebook.com/' + page_name ;   
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      data[i].message +
                    
                      '</p>';
                    if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }
          
           
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.see_comment', function (e) {//alert("hihi");
  if (e.handled !== true) {
    var attr_id = $(this).attr('id');
    var post_id  = attr_id.replace("seeComment_","");
  

    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() , '_blank');
     
     
        }
        


          e.handled = true;
       }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';

                                       
  for(var i in campaignGroup) 
          {
            if(i==0)
            {
              htmltext +='<fieldset class="controls">'+
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +='<fieldset class="controls">'+
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
              title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:GetURLParameter('pid'),campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
    
        
       
          }
          
           

            e.handled = true;
         });

     new $.fn.dataTable.FixedHeader( oTable );
   // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
  }
      $(window).resize( function() {

     //  new $.fn.dataTable.FixedHeader( oTable );
      $('#tbl_post').css('width', '100%');
      $('#tbl_visitorpost').css('width', '100%');
      
      //$("#tbl_post").DataTable().columns.adjust().draw();
      //$('table.fixedHeader-floating').css('left', '200px');
  });
    var permission = GetURLParameter('accountPermission');

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end,label) {//alert(label);
     
                
                    if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) 
          
            {
                 swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }


            else{
   var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'',label);
             }
           }
           else
           {
   var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'',label);
           }
      });

var fdate = new Date(GetURLParameter('fday'));
startDate=moment(fdate);
var sdate = new Date(GetURLParameter('sday'));
endDate=moment(sdate);

 getCampaignGroup();
 ChooseDate(startDate,endDate,'','');

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   window.dispatchEvent(new Event('resize'));

 });

hidden_div();




  $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
       });


$('#admin_page_filter').change(function() {
    //alert($(this).val());
    var admin_page = $(this).val();
   ChooseDate(startDate,endDate,admin_page,'');
     // $(this).val() will work here
});
 $("#btnSeeComment").click( function()
             {
               var post_id=$("#popup_id").val();
               var admin_page=GetURLParameter('admin_page');

               var split_name = admin_page.split("-");
                  if(split_name.length > 0)
                  {
                    if(isNaN(split_name[split_name.length-1]) == false )
                      admin_page = split_name[split_name.length-1];
                  }
           // alert(admin_page);
                // window.open("{{ url('relatedcompetitorcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
                 // window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+admin_page , '_blank');
                   window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post", '_blank');
             }
          );
function getCampaignGroup()
  {
    // add-campaign
   
      var brand_id = GetURLParameter('pid');
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getCampaignGroup')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          page_name:GetURLParameter('admin_page'),
          }
      })
      .done(function( data ) {
        campaignGroup=data;
       
          
        
      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }


  function getRelatedStatus(fday='',sday='',filter_overall='')
  {
            var brand_id = GetURLParameter('pid');
            $( "#reaction_spin" ).show();
            $( "#comment_spin" ).show();
            $("#share_spin").show();
            var admin_page = GetURLParameter('admin_page');

            

            $.ajax({
                type: "GET",
                dataType:'json',
                contentType: "application/json",
                url: "{{route('getRelatedStatus')}}", // This is the URL to the API
                data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:brand_id,admin_page:admin_page,filter_overall:filter_overall,
                        search_tag:GetURLParameter('search_tag'),period:GetURLParameter('period'),tag_id:GetURLParameter('tag_id')}
              })
              .done(function( data ) { 
                console.log(data);
               $( "#reaction_spin" ).hide();
               $( "#comment_spin" ).hide();
                $("#share_spin").hide();

                  var reaction_total = data[0];
                  var comment_total = data[1];
                  var share_total = data[2];
            
                  $('#reaction_total').text(reaction_total==0?'0':reaction_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0}));
                  $('#comment_total').text(comment_total==0?'0':comment_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0}));
                  $('#share_total').text(share_total==0?'0':share_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0}));

                               // $('#reaction_total').text(reaction_total);
                               //  $('#comment_total').text(comment_total);




                  // var engage_total = parseInt(share_total) + parseInt(reaction_total);
                 
                  // engage_total = engage_total.toLocaleString('us', {minimumFractionDigits: 0, maximumFractionDigits: 0});
               
                  // $('#engage_total').text(engage_total==0?'0':engage_total=='NaN'?'0':engage_total);


                  // neg_pcent = isNaN(pos_pcent)?'0':neg_pcent;
                  // neutral_pcent = isNaN(neutral_pcent)?'0':neutral_pcent;
                  // total = isNaN(total)?'0':total;

                })
              .fail(function(xhr, textStatus, error) {
                //  console.log(xhr.statusText);
                // console.log(textStatus);
                // console.log(error);
                // If there is no communication between the server, show an error
               // alert( "error occured" );
              });
  }

//local function

function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

  function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

        

  });
    </script>
    <style type="text/css">

table.fixedHeader-floating {
   clear: both;
  top: 114px !important;
  z-index:5;
   top : 0;
   left:0 !important;
   width: 100% !important;
/*   margin: 0 auto;
     padding: 0 15px;*/
 /*    overflow-x: hidden;
      overflow-y: auto;*/
  
    box-sizing: border-box;
/*
  background: red;*/
/*  left:150px !important;*/
/*  margin-right:30px;*/
 
/*  width: 80% !important;*/
  /*background: transparent;*/
}
.btn {
    padding: 2px 6px;
    font-size: 14px;
    cursor: pointer;
}

table.dataTable tbody tr td {
    word-wrap: break-word;
    word-break: break-all;
}
.myHeaderContent
{
   margin-right:300px;
}
 #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


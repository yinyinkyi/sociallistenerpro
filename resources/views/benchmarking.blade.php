  @extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
  @section('content')
  @section('filter')
  <li  class="nav-item dropdown mega-dropdown" > <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark dropdown-pages" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search text-warning"></i> <span class="text-warning" id="select_page"></span></a>
  <div id="search_form" class="dropdown-menu scale-up-left" id="dlDropDown"  style="padding-left: 5%">
  <ul class="mega-dropdown-menu row">
   
  <li class="col-lg-12" >
   <form>
      <input type="hidden" value='' class="hidden-chip" id='' name="hidden-chip"/>
    
      <input type="hidden" value='{{ auth()->user()->username }}' id="username" name="hidden-user"/>
      <input type="hidden" value='{{ auth()->user()->company_id }}' id="hidden-user" name="hidden-user"/>
      <input type="hidden" value="{{ auth()->user()->default_page }}" class="hidden-pg" name="hidden-pg"/>
      <input type="hidden" value="{{ auth()->user()->role }}" class ="hidden-role" name="hidden-kw"/>
       <div id="chip-competitor" class="col-md-12 align-self-center"  style="min-height:70px;padding-left:20px;padding-top:10px;border: 1px solid #f9f9f8;">
                   
                    </div>
                 
   </form>
  </li>
  </ul>
  </div>
  </li>


  @endsection
    <header class="" id="myHeader">
      <div class="row page-titles">
<!--         <div class="col-md-8 col-6 align-self-center">


          <li  class="nav-item dropdown mega-dropdown" > <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark dropdown-pages" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-search text-warning"></i>
           <span class="text-warning" id="select_page2"></span></a>
            <div id="search_form2" class="dropdown-menu scale-up-left" id="dlDropDown"  style="padding-left: 5%">
              <ul class="mega-dropdown-menu row">
                <li class="col-lg-12" >
                 <form>
                    <input type="hidden" value='' class="hidden-chip" id='' name="hidden-chip"/>
                    <input type="hidden" value='{{ auth()->user()->company_id }}' id="hidden-user" name="hidden-user"/>
                    <input type="hidden" value="{{ auth()->user()->default_page }}" class="hidden-pg" name="hidden-pg"/>
                    <input type="hidden" value="{{ auth()->user()->role }}" class ="hidden-role" name="hidden-kw"/>
                    <div id="chip-competitor" class="col-md-12 align-self-center"  style="min-height:70px;padding-left:20px;padding-top:10px;border: 1px solid #f9f9f8;">
                    </div>
                 </form>
                </li>
              </ul>
            </div>
          </li>
                       
        </div> -->
        <div class="col-md-4 col-6 align-self-center" >
            <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                   
                </div>
                <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                   <input type='text' class="form-control dateranges" style="float:right;" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                                <span class="ti-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </header>

     <div class="bank_div"> 
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-12">
          <div class="card">
            <div class="card-body b-t collapse show" style="padding-top:0px">
              <div style="display:none"  align="center" style="vertical-align: top;" id="bank-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                <div class="table-responsive">
                  <table id="tbl_bank"  class="table" style="margin-top:4px;" >
                  <thead>
                    
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


           
  @endsection
  @push('scripts')
  <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
      <!-- slimscrollbar scrollbar JavaScript -->
      <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
      <!--Wave Effects -->
      <script src="{{asset('js/waves.js')}}" defer></script>
      <!--Menu sidebar -->
      <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
      <!--stickey kit -->
      <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
      <!--Custom JavaScript -->
      <script src="{{asset('js/custom.min.js')}}" defer></script>
      <!-- ============================================================== -->
      <!-- This page plugins -->
      <!-- ============================================================== -->
     
     <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
       <script src="{{asset('js/morris-data.js')}}" ></script>-->
      
      <!-- Chart JS -->
      <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
      <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
      <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
      <!-- Flot Charts JavaScript -->
      <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
      <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
      
   <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
      <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

      <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
          <!-- Date range Plugin JavaScript -->
      <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
      <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
     <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
     <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
      <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
      <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" defer></script>
     <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
     <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/icheck/icheck.min.js')}}"  defer></script>
   <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}" defer></script>

<!--    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script> -->
  <!-- <script src="{{asset('assets/plugins/materialize/js/materialize.min.js')}}" defer></script> -->
   
     <script>
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myHeader");
  var myHeaderContent = document.getElementById("myHeaderContent");
  // var chipcompetitor = document.getElementById("chip-competitor");
  var tabHeader = document.getElementById("tabHeader");
  var tabHeader_sticky = tabHeader.offsetTop;

  var sticky = header.offsetTop;

  function myFunction() {//alert("ho");
    if (window.pageYOffset > sticky) {
      header.classList.add("s-topbar");
      header.classList.add("s-topbar-fix");
      myHeaderContent.classList.add("myHeaderContent");
      // chipcompetitor.classList.add("myHeaderScoll");
    } else {
      header.classList.remove("s-topbar");
      header.classList.remove("s-topbar-fix");
      myHeaderContent.classList.remove("myHeaderContent");
      // chipcompetitor.classList.remove("myHeaderScoll");
    }

    //  if (window.pageYOffset > tabHeader_sticky) {//alert("hi");
    //   tabHeader.classList.add("tabcontrolHeader");
    
    // } else {
    //     tabHeader.classList.remove("tabcontrolHeader");
    // }
  }
  </script>
   
  <script type="text/javascript">
  var startDate;
  var endDate;
  var label;
  /*global mention*/
  var mention_total;
  var mentionLabel = [];
  var mentions = [];
  /*Bookmark Array*/
  var bookmark_array=[];
  var bookmark_remove_array=[];
  /*global sentiment*/
  var positive = [];
  var negative = [];
  var sentimentLabel = [];
  var positive_total=0;
  var negative_total=0;
  var campaignGroup=[];
 var  colors=["#1e88e5","#dc3545","#28a745","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


  /*var effectIndex = 2;
  var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

  $(document).ready(function() {

    // var brand_id = $('#hidden-user').val();

    // $('#msf_div').hide();
      // $('#transfer').hide();
      // $('#transfer').removeClass('active');
      // $('#transfer_link').removeClass('active');
      // alert()
      if($('#username').val() == 'paymentadmin')
      $('#EntryBtn').show();
      else
      $('#EntryBtn').hide();

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });


      //initialize
   $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
  /* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
   $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
   $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

   $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

        var GetURLParameter = function GetURLParameter(sParam) {
      var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : sParameterName[1];
          }
      }
  };

      startDate =moment().subtract(1, 'month');
      endDate = moment();
      label = 'month';

             $('.singledate').daterangepicker({
              singleDatePicker: true,
              showDropdowns: true,
              locale: {
                  format: 'DD/MM/YYYY'
              }
          },function(date) {
            // alert(date);
            endDate=date;
           //  var id =$('input[type=radio][name=period-radio]:checked').attr('id');
           //  if(id==="period-week")
           // {
             startDate = moment();
             endDate = date;
           // }
           // else
           // {
             // startDate = moment(endDate).subtract(1, 'month');
             // endDate = endDate;
           // }
          
           var admin_page=$('.hidden-chip').val();
            // alert(admin_page);     
            // alert('helo');
             var kw_search = $("#kw_search").val();
             // alert(kw_search);
             // alert(startDate);
           ChooseDate(startDate,endDate,admin_page,'','',kw_search);
         
        });

             $('#kw_search').keyup(function () { 
               var kw_search = $(this).val();

               // alert(kw_search);
            $('.dateranges').val(startDate.format('MMM D, YYYY') + ' - ' + endDate.format('MMM D, YYYY'));
            startDate=startDate;
            endDate=endDate;
            // alert(startDate);
            filter_tag = '';
            var graph_option = $('.btn-group > .btn.active').text();
          
           var date_preset = Calculate_DatePreset(label);
           getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag,kw_search);
           getAllBench(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           getAllVisitorPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           getAllRemit(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);

            if (graph_option.trim()== 'Sentiment') {
              InboundSentimentDetail(GetStartDate(),GetEndDate());
            }
            // else if (graph_option.trim() == 'Page Like')
            // {
            //     PageGrowth(GetStartDate(),GetEndDate(),date_preset);
            // }tartDate=startDate;
            endDate=endDate;
               

               // ChooseDate(startDate,endDate,'','',kw_search)
            });

      var kw_search = $("#kw_search").val();
      // alert(kw_search);



   function hidden_div()
  {//alert("popular");
      // var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
    // var brand_id = 22;
      $( "#popular-spin" ).show();
      $("#popular").empty();
    $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('gethiddendiv')}}", // This is the URL to the API
        data: { view_name:'Dashboard'}
      })
      .done(function( data ) {//$("#popular").html('');
       for(var i in data) {
        $("#"+data[i].div_name).hide();
       }

      })
      .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
        // If there is no communication between the server, show an error
       // alert( "error occured" );
      });

  }



function Calculate_DatePreset(label)
{
         var date_preset='this_month';
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
          if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

        return date_preset;
}

     function ChooseDate(start, end,label='',filter_tag='',kw_search) {//alert(start);
            // alert(kw_search);
// alert('nello');
            $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate=start;
            endDate=end;
             var graph_option = $('.btn-group > .btn.active').text();
          
           var date_preset = Calculate_DatePreset(label);
           getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           getAllBench(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           getAllVisitorPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           getAllRemit(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           get_acc2acc_transfer(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           get_acc2agent_transfer(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);get_agent2agent_transfer(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           get_withdrawReg(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);
           get_withdrawNonReg(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),kw_search);



          
          }

          function GetStartDate()
  {
     //alert(startDate);
   return startDate.format('YYYY-MM-DD');
  }
  function GetEndDate()
  {
             // alert(endDate);
  return endDate.format('YYYY-MM-DD');
  }
  function getAllBench(fday,sday,kw_search)
      {
   // $('th').width($('th').width());


   var admin_page=$('.hidden-chip').val();
     alert(fday);
     alert(sday);
     alert(admin_page); 
     var oTable = $('#tbl_bank').DataTable({
    
        
          "paging": false,
          "lengthChange": false,

          "searching": false,
          "processing": true,
          "serverSide": true,
          "destroy": true,
          "ordering": true,
          "autoWidth": false,
          
       
          "order": [],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
            "columnDefs": [
       { type: 'currency', targets: [1 ,2 ,3, 4 ,5, 6, 7, 8, 9] }
     ],
          
          "ajax": {
            "url": '{{ route('getAllBench') }}',
            "dataSrc": function (d) {
         return d.data;
       },
            
             "data": {
              fday:fday,
              sday:sday,
             
            
            }

          },
          "initComplete": function( settings, json ) {
            console.log(json);
            $('.tbl_exchange thead tr').removeAttr('class');
          },
      
   

          columns: [
          
          {data: 'image', name: 'image',class:'text-center'},
          {data: 'usd_buy', name: 'usd_buy',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},
          {data: 'usd_sell', name: 'usd_sell',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},
          {data: 'usd_margin', name: 'usd_margin',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},

          {data: 'euro_buy', name: 'euro_buy',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},
          {data: 'euro_sell', name: 'euro_sell',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},
          {data: 'euro_margin', name: 'euro_margin',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},

          {data: 'sgd_buy', name: 'sgd_buy',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},
          {data: 'sgd_sell', name: 'sgd_sell',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},
          {data: 'sgd_margin', name: 'sgd_margin',class:'text-center',render: $.fn.dataTable.render.number( ',',  2 ,'','',' Ks')},

          {data: 'updated_date', name: 'updated_date',class:'text-center'},
         


          ]
          
        });
        // new $.fn.dataTable.columns.adjust().draw();
      new $.fn.dataTable.FixedHeader( oTable );
      // new $.fn.dataTable.columns.adjust();
 
          
    }
              $(window).resize( function() {
          // oTable.fnAdjustColumnSizing();
       //  new $.fn.dataTable.FixedHeader( oTable );
        // $('#tbl_exchange').css('width', '100%');
        //  $('#tbl_visitorpost').css('width', '100%');
        //$("#tbl_post").DataTable().columns.adjust().draw();
        // $('#tbl_exchange.fixedHeader-floating').css('left', '200px');
    });


      // jQuery('#tbl_exchange').wrap('');
      function getAllVisitorPost(fday,sday,kw_search)
      {
       
   var oTable = $('#tbl_fixedDepo').DataTable({
    
         
          "paging": false,
          // "pageLength": 10,
          "lengthChange": false,
          "searching": false,
          "processing": true,
          "serverSide": true,
          "destroy": true,
          "ordering": true   ,
          "autoWidth": false,
        
          "order": [],
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          
          "ajax": {
            "url": '{{ route('getfixedDeposit') }}',
            "dataSrc": function(res){
          
             
              return res.data;
            },
    
             "data": {
             
            
            }

          },
          "initComplete": function( settings, json ) {
         
          $('.tbl_fixedDepo thead tr').removeAttr('class');
          },
   

          columns: [
          {data: 'image', name: 'image'},
          {data: 'minimum', name: 'minimum'},
          {data: 'month_1', name: 'month_1'},
          {data: 'month_2', name: 'month_2'},
          {data: 'month_3', name: 'month_3'},
          {data: 'month_6', name: 'month_6'},
          {data: 'month_9', name: 'month_9'},
          {data: 'month_12', name: 'month_12'},
          {data: 'month_24', name: 'month_24'},
          {data: 'month_36', name: 'month_36'},
          {data: 'bank_book_slip', name: 'bank_book_slip'},
          {data: 'remarks', name: 'remarks'},


      


          ]
          
        }).on('click', '.popup_post', function (e) {
    if (e.handled !== true) {
      var name = $(this).attr('name');
      //alert(name);
      var post_id = $(this).attr('id');
      $("#modal-spin").show();
      $(".post_data").empty();

      //alert(post_id);alert(name);alert(GetURLParameter('pid'));
           $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("getRelatedPosts") }}',
           type: 'GET',
           data: {id:post_id,brand_id:brand_id},
           success: function(response) { //console.log(response);
            var res_array=JSON.parse(response);
            var data=res_array[0];
            
            var monitor=res_array[1];
            var monitor_full_name=res_array[2];
            var monitor_page_id=res_array[3];

               for(var i in data) {//alert(data[i].message);
                var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
                 var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

                 var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
                 var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
                 var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
                 pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
                 neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
                 neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
                 pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
                 neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
                 neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';
                 var page_name=data[i].page_name;

                // if(isNaN(page_name)==false) //it is number
                // {
                 
                //   var b = monitor.filter(item => item.indexOf(page_name) > -1);
                //   page_name = b[0];
                 
                   
                // }
                var found_index = monitor.indexOf(page_name);
                if (typeof monitor_page_id [found_index] !== 'undefined') 
                var page_photo_id = monitor_page_id [found_index];

                if (typeof monitor_full_name [found_index] !== 'undefined') 
                var page_name = monitor_full_name [found_index];
                
                var page_Link= 'https://www.facebook.com/' + page_name ;  
                var page_photo='https://graph.facebook.com/'+page_photo_id+'/picture?type=large'; 
        
         page_Link= 'https://www.facebook.com/' + page_name ;   
   var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
                 '  <div class="sl-left">'+
                 '<img src="'+page_photo+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
                 
                  ' <div class="sl-right">'+
                  '<div><a href="'+page_Link+'" target="_blank">'+page_name+'</a> '+
                  ' <div class="sl-right"> '+
               ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
                ' <div style="width:55%;display: inline-block">';
                if(neg_pcent!=='')
                html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
              if(pos_pcent!=='')
                html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
              if(neutral_pcent!=='')
                html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                  html+=' </div></div></div></span></p> ' +                
                    '   <p class="m-t-10" align="justify">' + 
                      data[i].message +
                    
                      '</p>';
                   if(data[i]['post_type'] =='photo')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:80%;height:50%" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                else if(data[i]['post_type'] =='video')
                {
                   html += '<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
                }
                      html +='</div></div>';
   $(".post_data").append(html);
       
       
          }
          
           
           $("#modal-spin").hide();
           $("#postModal").text(name);
           $('#show-post-task').modal('show'); 
          }
              });
            e.handled = true;
         }
       
      
          
    }).on('click', '.see_comment', function (e) {//alert("hihi");
    if (e.handled !== true) {
      var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("seeComment_","");
     // alert(post_id);
      // var admin_page=$('#hidden-chip').val();
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();
     
        var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
          //  alert(admin_page);
 var accountPermission = GetURLParameter('accountPermission');
      window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&accountPermission="+accountPermission, '_blank');
       
       
          }
          


            e.handled = true;
         }).on('click', '.btn-campaign', function (e) {//alert("hihi");
    if (e.handled !== true) {
       var attr_id = $(this).attr('id');
      var post_id  = attr_id.replace("campaign_","");
      var btn_val = $("#"+attr_id).attr('aria-pressed');
    if(btn_val == "false") // previous false next true
      {
          // var htmltext="<select class='form-control add-campaign' id='select_campaign'>";
          // htmltext +="<option value=''>Choose Campaign..</option>";
          var htmltext = '<div class="form-group">';
              htmltext +='<fieldset class="controls" style="text-align:left;padding-left:20px;">';

                                       
  for(var i in campaignGroup) 
          {
            if(i==0)
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" checked="checked" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            else
            {
              htmltext +=
                        '<label class="custom-control custom-radio">'+
                        '<input type="radio" value="'+campaignGroup[i]['id']+'" name="styled_radio"  id="'+campaignGroup[i]['id']+'" class="custom-control-input"><span class="custom-control-label">'+campaignGroup[i]['name']+'</span> </label>';
                        '</fieldset>';
            }
            
         
          }
          htmltext+= "</div>";

          //generate swal alert
             Swal.fire({
              title: '<h4> Select Campaign </h4> ',
              // type: 'info',
              html:htmltext,
              showCloseButton: true,
              showCancelButton: true,
              focusConfirm: false,
              focusCancel:false,
              confirmButtonText:'Add',
              confirmButtonAriaLabel: 'Thumbs up, great!',
              cancelButtonText:'Cancel',
              cancelButtonAriaLabel: 'Thumbs down',
            }).then((result) => {
                   var campaign_id=$('input[name=styled_radio]:checked').val();
                  

                  if (result.value) {
                  $.ajax({
                           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                           url:'{{ route("SetCampaign") }}',
                           type: 'POST',
                           data: {id:post_id,brand_id:brand_id,campaign_val:campaign_id},
                           success: function(response) { //alert(response)
                            if(response>=0)
                            {
                             var x = document.getElementById("snackbar")
                                        $("#snack_desc").html('');
                                        $("#snack_desc").html('Campaign added successfully!!');
                                       x.className = "show";

                        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
                            }
                          }
                      });
                 
                  }
                  else
                  {
                     $("#"+attr_id).attr("aria-pressed", "false");
                     $("#"+attr_id).removeAttr("class");
                     $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                  }
                })
            //end swal alert
      }
       else
      {
        var answer = confirm("Are you sure to remove campaign?")
        if (answer) {
                   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("SetCampaign") }}',
         type: 'POST',
         data: {id:post_id,brand_id:brand_id},
         success: function(response) { //alert(response)
          if(response>=0)
          {
            var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Campaign remove successfully!!');
                     x.className = "show";

             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          }
        }
            });
                }
                else
                {  

                   // $("#"+attr_id).attr("aria-pressed", "true");
                   $("#"+attr_id).removeAttr("class");
                   $("#"+attr_id).addClass("btn-campaign btn btn-secondary btn-outline");
                   $("#"+attr_id).attr("aria-pressed", "true");
                }
               
      }
    
        
       
          }
          
           

            e.handled = true;
         });
      new $.fn.dataTable.FixedHeader( oTable );
     // oTable.columns.adjust().draw();
     // $('#tbl_post').css('width', '100%');
     // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
          
    }

    

      $("#btnSeeComment").click( function()
             {
               var post_id=$("#popup_id").val();
               // var brand_id = $('#hidden-user').val();
               var brand_id = GetURLParameter('pid');
               // var admin_page=$('#hidden-chip').val();
                var admin_page_id=$('.hidden-chip').attr('id');
                var admin_page=$('.hidden-chip').val();

               var split_name = admin_page.split("-");
                  if(split_name.length > 0)
                  {
                    if(isNaN(split_name[split_name.length-1]) == false )
                      admin_page = split_name[split_name.length-1];
                  }
           // alert(admin_page);
                // window.open("{{ url('relatedcompetitorcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&comefrom=post" , '_blank');
                 // window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+admin_page , '_blank');
                 var accountPermission = GetURLParameter('accountPermission');
                   window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() +"&comefrom=post"+"&accountPermission="+accountPermission, '_blank');
             }
          );



         function getAllRemit(fday, sday,filter_tag='',kw_search)
      {
       
        var brand_id = GetURLParameter('pid');
   
        
      var admin_page=$('.hidden-chip').val();

     
       

       

   var oTable = $('#tbl_remit').DataTable({
          "lengthChange": false,
          "bPaginate": false,
          "bDestroy":true,
          "searching": false,
          "processing": false,
          "serverSide": true, 
         
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": false,
          "ordering": true,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
        
         
          "ajax": {
            "url": '{{ route('getRemit') }}',
            "dataSrc": function(res){
             
            
              return res.data;
            },
   
             "data": {
             
            }

          },
          "initComplete": function( settings, json ) {
        
         
          },
                           

          columns: [
          
          {data: 'image', name: 'image',class:'text-center'},
          {data: 'fix_rate', name: 'fix_rate',class:'text-center'},
          {data: 'b2bmin', name: 'b2bmin',class:'text-center'},
          {data: 'b2bmax', name: 'b2bmax',class:'text-center'},
          {data: 'b2omin', name: 'b2omin',class:'text-center'},
          {data: 'b2omax', name: 'b2omax',class:'text-center'},
          {data: 'remark', name: 'remark',class:'text-center'},

        

         

      


          ]
          
        });
       
      
          
    }


             function get_acc2acc_transfer(fday, sday,filter_tag='',kw_search)
      {
       
        var brand_id = GetURLParameter('pid');
   
        
      var admin_page=$('.hidden-chip').val();

     
       

      $('#acc-transfer-spin').show(); 

   var oTable = $('#tbl_acc2acc_transfer').DataTable({
          "lengthChange": false,
          "bPaginate": false,
          "bDestroy":true,
          "searching": false,
          "processing": false,
          "serverSide": true, 
 
          
         
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": false,
          "ordering": true,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
        
         
          "ajax": {
            "url": '{{ route('get_acc2acc_transfer') }}',
            "dataSrc": function(res){
             
            
              return res.data;
            },
   
             "data": {
             
            }

          },
          "initComplete": function( settings, json ) {
        
         
          },

                           

          columns: [ 
          
          {data: 'image', name: 'image',class:'text-center'},
          {data: 'transfer_amount', name: 'transfer_amount',class:'NextLine'},
          {data: 'fees', name: 'fees',class:'NextLine'},
          {data: 'updated_date', name: 'updated_date',class:'text-center'},
          ]
          
        });
       
      $('#acc-transfer-spin').hide(); 
          
    }

     function get_acc2agent_transfer(fday, sday,filter_tag='',kw_search)
      {
       
        var brand_id = GetURLParameter('pid');
   
        
      var admin_page=$('.hidden-chip').val();

     
       

      $('#accagent-transfer-spin').show(); 

   var oTable = $('#tbl_acc2agent_transfer').DataTable({
          "lengthChange": false,
          "bPaginate": false,
          "bDestroy":true,
          "searching": false,
          "processing": false,
          "serverSide": true, 
          
         
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": false,
          "ordering": true,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
        
         
          "ajax": {
            "url": '{{ route('get_acc2agent_transfer') }}',
            "dataSrc": function(res){
             
            
              return res.data;
            },
   
             "data": {
             
            }

          },
          "initComplete": function( settings, json ) {
        
         
          },
                           

          columns: [ 
          
          {data: 'image', name: 'image',class:'text-center'},
          {data: 'transfer_amount', name: 'transfer_amount',class:'NextLine'},
          {data: 'fees', name: 'fees',class:'NextLine'},
          {data: 'updated_date', name: 'updated_date',class:'text-center'},
          ]
          
        });
       
      $('#accagent-transfer-spin').hide(); 
          
    }
      function get_agent2agent_transfer(fday, sday,filter_tag='',kw_search)
      {
       
        var brand_id = GetURLParameter('pid');
   
        
      var admin_page=$('.hidden-chip').val();

     
       

      $('#agent-transfer-spin').show(); 

   var oTable = $('#tbl_agent2agent_transfer').DataTable({
          "lengthChange": false,
          "bPaginate": false,
          "bDestroy":true,
          "searching": false,
          "processing": false,
          "serverSide": true, 
          
         
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": false,
          "ordering": true,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
        
         
          "ajax": {
            "url": '{{ route('get_agent2agent_transfer') }}',
            "dataSrc": function(res){
             
            
              return res.data;
            },
   
             "data": {
             
            }

          },
          "initComplete": function( settings, json ) {
        
         
          },
                           

          columns: [ 
          
          {data: 'image', name: 'image',class:'text-center'},
          {data: 'transfer_amount', name: 'transfer_amount',class:'NextLine'},
          {data: 'fees', name: 'fees',class:'NextLine'},
          {data: 'updated_date', name: 'updated_date',class:'text-center'},
          ]
          
        });
       
      $('#agent-transfer-spin').hide(); 
          
    }
      function get_withdrawReg(fday, sday,filter_tag='',kw_search)
      {
       
        var brand_id = GetURLParameter('pid');
   
        
      var admin_page=$('.hidden-chip').val();

     
       

      $('#reg-spin').show(); 

   var oTable = $('#tbl_withdraw_reg').DataTable({
          "lengthChange": false,
          "bPaginate": false,
          "bDestroy":true,
          "searching": false,
          "processing": false,
          "serverSide": true, 
          
         
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": false,
          "ordering": true,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
        
         
          "ajax": {
            "url": '{{ route('get_withdraw_reg') }}',
            "dataSrc": function(res){
             
            
              return res.data;
            },
   
             "data": {
             
            }

          },
          "initComplete": function( settings, json ) {
        
         
          },
                           

          columns: [ 
          
          {data: 'image', name: 'image',class:'text-center'},
          {data: 'withdraw_amount', name: 'withdraw_amount',class:'NextLine'},
          {data: 'fees', name: 'fees',class:'NextLine'},
          {data: 'updated_date', name: 'updated_date',class:'text-center'},
          ]
          
        });
       
      $('#reg-spin').hide(); 
          
    }
  //    $('#NonReg').click(function(){
  //   get_withdraw_nonreg();

  // })
      function get_withdrawNonReg(fday, sday,filter_tag='',kw_search)
      {
       
        var brand_id = GetURLParameter('pid');
   
        
      var admin_page=$('.hidden-chip').val();

     
       

      $('#nonreg-spin').show(); 

   var oTable = $('#tbl_withdraw_nonreg').DataTable({
          "lengthChange": false,
          "bPaginate": false,
          "bDestroy":true,
          "searching": false,
          "processing": false,
          "serverSide": true, 
          
         
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": false,
          "ordering": true,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
        
         
          "ajax": {
            "url": '{{ route('get_withdraw_nonreg') }}',
            "dataSrc": function(res){
             
            
              return res.data;
            },
   
             "data": {
             
            }

          },
          "initComplete": function( settings, json ) {
        
         
          },
                           

          columns: [ 
          
          {data: 'image', name: 'image',class:'text-center'},
          {data: 'withdraw_amount', name: 'withdraw_amount',class:'NextLine'},
          {data: 'fees', name: 'fees',class:'NextLine'},
          {data: 'updated_date', name: 'updated_date',class:'text-center'},
          ]
          
        });
       
      $('#nonreg-spin').hide(); 
          
    }


   function getAllComment(fday, sday,filter_tag='',kw_search)
      {
        // alert('fdfd');
        $(".popup_post").unbind('click');
        $(".edit_predict").unbind('click');
        $("#add_tag").unbind('click');
        var brand_id = GetURLParameter('pid');
   //var admin_page=$('#admin_page_filter option:selected').val();
   //alert($('#hidden-chip').val());
        var filter_tag_arr = [];
              $.each($("input[name='taglist']:checked"), function(){            
                  filter_tag_arr.push($(this).val());
                });
        var filter_senti_arr = [];
            $.each($("input[name='sentimentlist']:checked"), function(){            
                filter_senti_arr.push($(this).val());
            });

      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

        // var admin_page = $('#hidden-chip').val();
        document.getElementById('select_page').innerHTML =admin_page;

        var split_name = admin_page.split("-");
            if(split_name.length > 0)
            {
              if(isNaN(split_name[split_name.length-1]) == false )
                admin_page = split_name[split_name.length-1];
            }
       //  alert(filter_tag_arr.join("| "));
       // alert(brand_id);

   var oTable = $('#tbl_callDepo').DataTable({
          "lengthChange": false,
          "bPaginate": false,
          "bDestroy":true,
          "searching": false,
          "processing": false,
          "serverSide": true, 
         
          // if serverside is true,datatable will call ajax request for each pagination, if false, all data in db will b loaded at once(heavy loading time)
          "destroy": false,
          "ordering": true,
          "headers": {
            'X-CSRF-TOKEN': '{{csrf_token()}}' 
          },
          "order":[],
        
         
          "ajax": {
            "url": '{{ route('getCallDeposit') }}',
            "dataSrc": function(res){
             
            
              return res.data;
            },
   
             "data": {
             
            }

          },
          "initComplete": function( settings, json ) {
        
         
          },
                           

          columns: [
          
          {data: 'image', name: 'image'},
          {data: 'initial', name: 'initial'},
          {data: 'minimum', name: 'minimum'},
          {data: 'pa', name: 'pa'},
          {data: 'remarks', name: 'remarks'},

          {data: 'updated_date', name: 'updated_date'},
        

         

      


          ]
          
        });
       
      
          
    }
    $('#show-add-tag').on('shown.bs.modal', function () {
      $('#name').focus();
  })  



    var permission = GetURLParameter('accountPermission');
  $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                 
              },
                startDate: startDate,
                endDate: endDate,
          },function(start, end,label) {
         // alert(start);alert(end);
            var minDate = start.format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = end.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            // if(+from <= min+)
            if(from.getTime()  !== min.getTime()) 
          
            {

                 swal({
                title: 'Sorry',
                text: "Date should not exceed more than one day !",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment();
                endDate = moment();
     
            }


            else{
              // alert('eho');
              var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;
              var kw_search = $("#kw_search").val();
              ChooseDate(startDate,endDate,label,'',kw_search);
              tagCount();
             }
           
         
        }
      )

  // to acess only three month data for demo account 

    //     $(".dateranges").on("change",function(){

         
        
    // });


   //ChooseDate(startDate,endDate,'','');

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
     //window.dispatchEvent(new Event('resize'));
  // if(target=="#dashboard")
  //   {
      window.dispatchEvent(new Event('resize'));
  //  }

 



   });

  hidden_div();
  function getCampaignGroup()
  {
    // add-campaign
   
      var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getCampaignGroup')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          page_name:$('.hidden-chip').val(),
          }
      })
      .done(function( data ) {
        campaignGroup=data;
       
          
        
      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
  $('#nameSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'name';

      tagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
      var tagOrder = 'DESC' ;
      var fieldName = 'name';

      tagCount(tagOrder,fieldName);
    }
    

  });
    $('#pnameSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'name';

      posttagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
      var tagOrder = 'DESC' ;
      var fieldName = 'name';

      posttagCount(tagOrder,fieldName);
    }
    

  });
    $('#countSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'count';

      tagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
       var tagOrder = 'DESC' ;
      var fieldName = 'count';

       tagCount(tagOrder,fieldName);
    }
    

  });
        $('#pcountSort').click(function(){
   
    var text = $(this).attr('class');
    if (text.indexOf("descending") >= 0)
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-ascending');
      var tagOrder = 'ASC' ;
      var fieldName = 'count';

      posttagCount(tagOrder,fieldName);
    }
    else
    {
      $(this).removeClass();
      $(this).addClass('mdi mdi-sort-descending');
       var tagOrder = 'DESC' ;
      var fieldName = 'count';

       posttagCount(tagOrder,fieldName);
    }
    

  });
 
  function tagCount(tagOrder='',fieldName='')
  {
    // alert(tagOrder);
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

      var filter_tag_arr = [];var selected_tag = '';
      $.each($("input[name='taglist']:checked"), function(){            
          filter_tag_arr.push($(this).val());
          selected_tag = ($(this).val()); 
        });


      var fday=GetStartDate();
      var sday=GetEndDate();
      $(".tag-list").empty();
      var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getcmtTagCount')}}", // This is the URL to the API
        data: { 
          fday: fday,
          sday:sday,
          brand_id:brand_id,
          tagOrder:tagOrder,
          fieldName:fieldName,
          tsearch_tag:filter_tag_arr.join("| "),
          // admin_page:admin_page,
          // admin_page_id:admin_page_id,
          limit:'no'}
      })
      .done(function( data ) {//console.log(data);
  for(var i in data) 
          {
             var tagLabel=data[i].tagLabel;
             var tagId=data[i].tagId;
             var tagCount=data[i].tagCount;
              if(selected_tag == tagId){
                $(".tag-list").append(' <li>' +
              ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'" checked>'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
              }
              else{
              $(".tag-list").append(' <li>' +
              ' <input type="checkbox" class="check taglist" name="taglist" value="'+tagId+'" id="'+tagId+'">'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
              }

          }

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }
    function posttagCount(tagOrder='',fieldName='')
  {
      // alert(tagOrder);
      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

      var post_filter_tag_arr = [];var post_selected_tag = '';
      $.each($("input[name='ptaglist']:checked"), function(){            
          post_filter_tag_arr.push($(this).val());
          post_selected_tag = ($(this).val()); 
        });
        // alert(selected_tag); 

      var fday=GetStartDate();
      var sday=GetEndDate();
      $(".post-tag-list").empty();
      var brand_id = GetURLParameter('pid');
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getPostTagCount')}}", // This is the URL to the API
        data: { 
          fday: fday,
          sday:sday,
          brand_id:brand_id,
          tagOrder:tagOrder,
          fieldName:fieldName,
          tsearch_tag:post_filter_tag_arr.join("| "),
          // admin_page:admin_page,
          // admin_page_id:admin_page_id,
          limit:'no'}
      })
      .done(function( data ) {//console.log(data);
  for(var i in data) 
          {
             var tagLabel=data[i].tagLabel;
             var tagId=data[i].tagId;
             var tagCount=data[i].tagCount;
              if(post_selected_tag == tagId){
                $(".post-tag-list").append(' <li>' +
              ' <input type="checkbox" class="check ptaglist" name="ptaglist" value="'+tagId+'" id="post_'+tagId+'" checked>'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="post_'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
              }
              else{
              $(".post-tag-list").append(' <li>' +
              ' <input type="checkbox" class="check ptaglist" name="ptaglist" value="'+tagId+'" id="post_'+tagId+'">'+
              ' <label class="text-info" style="padding-left:30px;padding-right:5px;font-weight:500;font-size:12px" for="post_'+tagId+'">'+tagLabel+'</label><span style="font-size:10px" class="label label-light-info">'+tagCount+'</span> '+
              ' </li>');
              }

          }

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }

 
  $(document).on('click', '.taglist', function () {
    // alert('hello');
    getAllComment(GetStartDate(),GetEndDate());
   
    tagCount();
  });
  $(document).on('click', '.sentimentlist', function () {
    getAllComment(GetStartDate(),GetEndDate());
});

    $(document).on('click', '.ptaglist', function () {
   // alert('posttagclick');
    getAllPost(GetStartDate(),GetEndDate());
    posttagCount();
  });
  $(document).on('click', '.post-sentimentlist', function () {
   
    getAllPost(GetStartDate(),GetEndDate());
  
});


  $(document).on('click','.top_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:80%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
    $(document).on('click','.top_cmt_popup_img',function(){
          var id = $(this).attr('id');
          var src_path = $('#'+id).attr('src');
          $(".large_img").empty();
          $(".large_img").append("<img class='postPanel_previewImage' style='width:30%;height:50%' src='"+src_path+"'/>");
          $('#show-image').modal('show'); 

  })
  $('#senti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.taglist)").click(function () {
    $("#senti-all").prop("checked", $("input:checkbox:not(.taglist):checked").length == 4);
  });

  $('#psenti-all').on('click', function () {
   if ($(this).prop('checked')) {
        $('.post-sentimentlist').each(function () {
            $(this).prop('checked', true);
        });
        } 
        else {
        $('.post-sentimentlist').each(function () {
            $(this).prop('checked', false);
        });
    }
});
$("input:checkbox:not(.ptaglist)").click(function () {
    $("#psenti-all").prop("checked", $("input:checkbox:not(.ptaglist):checked").length == 4);
  });

  bindOtherPage();

  function bindOtherPage()
  {
      var company_id = $("#hidden-user").val();
 
      $("#chip-competitor tbody").empty();
      var brand_id = GetURLParameter('pid');
           // alert(brand_id);
      // var brand_id = $('#hidden-user').val();
          $.ajax({
        type: "GET",
         dataType:'json',
        contentType: "application/json",
        url: "{{route('getBankType')}}", // This is the URL to the API
        data: { 
          brand_id:brand_id,
          company_id:company_id,
          }
      })
      .done(function( data ) {//alert(data);
       // console.log("hey"+JSON.stringify(data));
// var default_page = data[0]['default_page'];
  for(var i in data) 
          {
           // alert(i);    
           var default_page=data[i]['default_page'];
           // alert(default_page);
           var id=data[i]['id'];
          // alert(id);
         //   if(data[i]['id'] !== '')
         //   var photo='https://graph.facebook.com/'+data[i]['id']+'/picture?type=large';
         // else
         //  var photo ="{{asset('assets/images/competitor1.png')}}";
         if(data[i]['imgurl'] !== '') var photo = data[i]['imgurl'];
         else photo ="{{asset('assets/images/competitor1.png')}}";

         if(default_page !== '')
         {
    // alert('hey');
            if(default_page == data[i]['page_name'])
            { 
              // alert('equal');
              
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['id']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');
             
                 $(".hidden-chip").val(data[i]['page_name']); 
                 $( ".hidden-chip" ).attr("id",data[i]['id']);
                 // $("#hidden-chip").(data[i]['page_name']);
              
            }
            else
            {
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['id']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');

            }
    

         }
         else
         {
           if(parseInt(i) == 0)
            { 
              
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;color:white;background-color:rgba(188, 138, 49, 1)" name="'+data[i]['page_name']+'" id="'+data[i]['id']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');
                 
                 $(".hidden-chip").val(data[i]['page_name']); 
                 $( ".hidden-chip" ).attr("id",data[i]['id']);
                 // $("#hidden-chip").val(data[i]['page_name']); 
              
            }
            else
            {
              $("#chip-competitor").append(' <div class="chip" style="cursor: pointer;" name="'+data[i]['page_name']+'"  id="'+data[i]['id']+'" > ' +
                    ' <img src="'+photo+'" alt=""> '+
                    data[i]['page_name'] + '</div>');
            }

         }
         


          }
           var kw_search = $("#kw_search").val();
          ChooseDate(startDate,endDate,'','',kw_search);
          tagCount();
          posttagCount();
       

      })
       .fail(function(xhr, textStatus, error) {
        //  console.log(xhr.statusText);
        // console.log(textStatus);
        // console.log(error);
      
      });
  }

  $(document).on('click', '.chip', function (e) {
    // alert('hello');
      var id = $(this).attr("id");  
      var name = $(this).attr("name");
    // alert(id);
      var chipcompetitor = document.getElementById("chip-competitor");
    //alert(name);  
      $(".chip").css("background-color","#e4e4e4");
      $(".chip").css("color","rgba(0,0,0,0.6)");
       
      $("#"+id).css("background-color","rgba(188, 138, 49, 1)");
      $("#"+id).css("color","white");
      $(".hidden-chip").val(name);
      $(".hidden-chip").attr("id",id);
      
     $(".dropdown-pages").dropdown('toggle');

     if(name == 'Bank'){
       $('.msf_div').hide();
       $('.bank_div').show();


      // $('#transferLi').hide();
      // $('#transfer').hide();
      // $('#transfer').removeClass('active');
      // $('#transfer_link').removeClass('active');

      // $('#calledLi').show();
      // $('#call_depo').show();
      // $('#calledLi').removeClass('active');
      // $('#call_depo').removeClass('active');

      // $('#fixedLi').show();
      // $('#fixed_depo').show();
      // $('#fixedLi').removeClass('active');
      // $('#fixed_depo').removeClass('active');
      
      // $('#remitLi').show();
      // $('#remit').show();
      // $('#remitLi').removeClass('active');
      // $('#remit').removeClass('active');


      // $('#exchange_link').addClass('active');
      // $('#exchange').addClass('active');
      // $('#exchangeRateLi').show();
      // $('#exchange').show();

     }
     else if(name == 'MFS'){

       $('.msf_div').show();
       $('.bank_div').hide();
      // $('#transferLi').show();
      // $('#transfer').show();
      // $('#transfer').addClass('active');
      // $('#transfer_link').addClass('active');

      // $('#calledLi').hide();
      // $('#call_depo').hide();
      // $('#calledLi').removeClass('active');
      // $('#call_depo').removeClass('active');

      // $('#fixedLi').hide();
      // $('#fixed_depo').hide();
      // $('#fixedLi').removeClass('active');
      // $('#fixed_depo').removeClass('active');
      
      // $('#remitLi').hide();
      // $('#remit').hide();
      // $('#remitLi').removeClass('active');
      // $('#remit').removeClass('active');


      // $('#exchange_link').removeClass('active');
      // $('#exchange').removeClass('active');
      // $('#exchangeRateLi').hide();
      // $('#exchange').hide();

      //       $('#transfer_link').addClass('active');
      //       $('#transfer').addClass('active');
      // if($('#fixed_link').hasClass('active')) {$('#fixed_link').removeClass('active');$('#fixed_depo').removeClass('active');}
      // if($('#call_link').hasClass('active')) {$('#call_link').removeClass('active');$('#call_depo').removeClass('active');}
      // if($('#remit_link').hasClass('active')) {$('#remit_link').removeClass('active');$('#remit').removeClass('active');}

      // if($('#exchange_link').hasClass('active')){$('#exchange_link').removeClass('active');$('#exchange').removeClass('active');}



      // $('#transferLi').show();
      // $('#transfer').show();
    

      // $('#exchangeRateLi').hide();
      // $('#exchange').hide();
      // $('#fixedLi').hide();
      // $('#fixed_depo').hide();
      // $('#calledLi').hide();
      // $('#call_depo').hide();
      // $('#remitLi').hide();
      // $('#remit').hide(); 
      
     }
      
      var kw_search = $("#kw_search").val();
      // alert('hello');
      ChooseDate(startDate,endDate,'','',kw_search);
      
        });
  $("#add_new_tag").click(function(){
  //alert("hihi");
    var name = $("#name").val();
    var nameLength = name.length;
    // var category_id = $("#category_id").val();

    if(name == '')
    {
      $(".invalid-name").append("<strong class='text-danger'>Please fill out name!</strong>");
      return false;
    }
    else if (nameLength > 80 )
      {
      $(".invalid-name").append("<strong class='text-danger'>The name may not be greater than 80 characters!</strong>");
      return false;
    }
    //  if(category_id == '')
    // {
    //   $(".invalid-category").append("<strong class='text-danger'>Please choose category!</strong>");
    //   return false;
    // }
    //var keywords = $("#keywords").val();
    var brand_id = $("#brand_id").val();
    
   
    var keywords = '';

  //   if(keywords == '')
  //   {
  //   $(".invalid-keyword").append("<strong>Please fill out keyword</strong>");
  //   return false;
  // }
     $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("quick_tag") }}',
           type: 'POST',
           data: {name:name,keywords:keywords,brand_id:brand_id},
           success: function(response) {//alert(response);
            //alert(response);
            if(response !== "exist")
            {
               $('.edit_tag')
           .append($("<option></option>")
                      .attr("value",response)
                      .text(name)); 
                      var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('Tag added successfully!!');
      x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
          //             swal({   
          //     title: "Success!",   
          //     text: "Tag " + name + " is already added!" ,   
          //     timer: 500 ,   
          //     showConfirmButton: false 
          // });
            }

            else if (response == "exist")
            {
              var x = document.getElementById("snackbar")
                      $("#snack_desc").html('');
                      $("#snack_desc").html('This tag already exist !!');
      x.className = "show";

      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            }
               $("#name").val('');
               $("#keywords").val('');
       $('#show-add-tag').modal('toggle');
           
          }
        });

  });

    $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
         });


  $('#admin_page_filter').change(function() {
      //alert($(this).val());
      var admin_page = $(this).val();
      var kw_search = $("#kw_search").val();
      ChooseDate(startDate,endDate,'','',kw_search);
     
       // $(this).val() will work here
  });

  // $(document).on('click', '.btn_tag', function () {
  //     var filter_tag=this.id;
  // var admin_page = $('#hidden-chip').val();

  //     getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag);
  // });
  $(document).on('click','.dropdown-menu a',function(){
  var row_id=this.id;
  row_id = row_id.substring(row_id.indexOf('_')+1);
  // alert(row_id);
        $("#btnaction_"+row_id+":first-child").text($(this).text());
        $("#btnaction_"+row_id+":first-child").val($(this).text());
        // $("#btnaction_"+row_id).removeClass('btn-red');
        // $("#btnaction_"+row_id).removeClass('btn-success');
        // if($(this).text() === "Require Action")
        // {
        // $("#btnaction_"+row_id).addClass('btn-red');
        // }
        // else
        // {
        //   $("#btnaction_"+row_id).addClass('btn-success');
        // }

         $.ajax({
           headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
           url:'{{ route("setActionUpdate") }}',
           type: 'POST',
           data: {id:row_id,brand_id:GetURLParameter('pid'),action_status:$(this).text()},
           success: function(response) { //alert(response)
            if(response>=0)
            {
         
            }
          }
              });

        //save action status and taken person in database
  });
  //dashboard function




        $(document).on('click', '#see_more', function () {
           // $('mention_show').css('display','none');
           $('#see_more').css('display','none');
           $('.btn_post_tag_hide').css('display','inline');
           $('#see_less').css('display','inline');
        });
        $(document).on('click', '#see_less', function () {
          $('.btn_post_tag_hide').css('display','none');
            $('.btn_post_tag_show').css('display','inline');
            $('#see_more').css('display','inline');
            $('#see_less').css('display','none');
        });

            $(document).on('click', '#more', function () {
           // $('mention_show').css('display','none');
           $('#more').css('display','none');
           $('.btn_cmt_tag_hide').css('display','inline');
           $('#less').css('display','inline');
        });
        $(document).on('click', '#less', function () {
          $('.btn_cmt_tag_hide').css('display','none');
            $('.btn_cmt_tag_show').css('display','inline');
            $('#more').css('display','inline');
            $('#less').css('display','none');
        });


      function InboundSentimentDetail(fday,sday){

    var brand_id = GetURLParameter('pid');
    let main = document.getElementById("fan-growth-chart");
    let existInstance = echarts.getInstanceByDom(main);
        if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }

      var admin_page_id=$('.hidden-chip').attr('id');
      var admin_page=$('.hidden-chip').val();

var sentiDetailChart = echarts.init(main);

    $("#fan-growth-spin").show();
      
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInboundSentiDetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page,admin_page_id:admin_page_id,periodType:'month',kw_search:$("#kw_search").val() }
    })
    .done(function( data ) {
     // console.log(data);
      var positive = [];
      var negative = [];
      var neutral=[];
      var sentimentLabel = [];

      var positive_total=0;
      var negative_total=0;
      var neutral_total=0;
      var all_total=0;


        for(var i in data) {//alert(data[i].mentions);
        positive.push(data[i].positive);
        negative.push(data[i].negative);
        neutral.push(data[i].neutral);
        sentimentLabel.push(data[i].periodLabel);
        
      }
      // alert(positive);


$.each(positive,function(){positive_total+=parseInt(this) || 0;});
$.each(negative,function(){negative_total+=parseInt(this) || 0;});
$.each(neutral,function(){neutral_total+=parseInt(this) || 0;});
all_total=positive_total+negative_total+neutral_total;
var positive_percentage=parseInt((positive_total/all_total)*100);
var negative_percentage= parseInt((negative_total/all_total)*100);
var neutral_percentage =  parseInt((neutral_total/all_total)*100);

          positive_percentage = isNaN(positive_percentage)?0:positive_percentage;
          negative_percentage = isNaN(negative_percentage)?0:negative_percentage;
          neutral_percentage = isNaN(neutral_percentage)?0:neutral_percentage;
         

    option = {
        color:colors,
      

 tooltip : {
            trigger: 'axis',
             /*formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
        console.log(rez); //quite useful for debug
        params.forEach(item => {
             console.log("hihi"); 
            console.log(item);value
            console.log(item.series.color); //quite useful for debug
            var xx = '<p>'   + colorSpan(item.series.color) + ' ' + item.seriesName + ': ' + item.data  + '</p>'
            rez += xx;
        });

        return rez;
    }*/

        },

        legend: {
            data:['Positive','Negative'],
           formatter: function (name) {
            if(name === 'Positive')
            {
                 return name + ': ' + positive_total;
            }
            return name  + ': ' + negative_total;

   
}
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
             axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
     var date = new Date(value);
     //  console.log(date);
      var texts = [date.getFullYear(), monthNames[date.getMonth()]];

    return texts.join('-');

}
    },
           data : sentimentLabel
        }
        ],
        yAxis : [
        {
            type : 'value',
            name: 'comment count',
        }
        ],
        series : [
        {
            name:'Positive',
            type:'bar',
            data:positive,
            barMaxWidth:30,
            color:colors[2],
            markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }/*,
            markLine : {
                data : [
                {type : 'average', name: 'average'}
                ]
            }*/
        },
        {
            name:'Negative',
          /*  type:effectIndex % 2 == 0 ? 'bar' : 'line',*/
            type:'bar',
            data:negative,
            barMaxWidth:30,
            color:colors[1],
            markPoint : {
              data : [
              {type : 'max', name: 'maximum'},
              {type : 'min', name: 'minimum'}
              ]
          }/*,
          markLine : {
            data : [
            {type : 'average', name : 'average value'}
            ]
        }*/
    }
    ]
};
$("#fan-growth-spin").hide();
sentiDetailChart.setOption(option, true), $(function() {
    // function resize() {
    //     setTimeout(function() {
    //         sentiDetailChart.resize()
    //     }, 100)
    // }
    // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

 sentiDetailChart.on('click', function (params) {
   // console.log(params);
   // console.log(params.name); // xaxis data = 2018-08
   // console.log(params.seriesName); //bar period name ="Positive"
   // console.log(params.value);//count
   // var pid = GetURLParameter('pid');
   var brand_id = GetURLParameter('pid');
   var admin_page_id=$('.hidden-chip').attr('id');
   var admin_page=$('.hidden-chip').val();

   var commentType ='';
   
   if(params.seriesName == 'Positive')
    commentType = 'pos'
    else
   commentType = 'neg'
   var default_page = $('.hidden-pg').val();
   var accountPermission = GetURLParameter('accountPermission');
  if(params.name !== 'minimum' && params.name !== 'maximum' )
   window.open("{{ url('relatedcomment?')}}" +"pid="+ brand_id +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission, '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});
    
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
  

  $('input[type=radio][name=options]').change(function() {

    if (this.value == 'page') {
     var date_preset= Calculate_DatePreset(label);
        PageGrowth(GetStartDate(),GetEndDate(),date_preset);
        
    }
    else if (this.value == 'sentiment') {
        InboundSentimentDetail(GetStartDate(),GetEndDate());
    }
    
});
  $(".btnComment").click(function(e){
    var commentType = $(this).attr('value');
    var default_page = $('.hidden-pg').val();
    var admin_page_id=$('.hidden-chip').attr('id');
    var admin_page=$('.hidden-chip').val();

   var accountPermission = GetURLParameter('accountPermission');
    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
})
$(".btnPost").click(function(e){
    var postType = $(this).attr('value');
    var admin_page_id=$('.hidden-chip').attr('id');
    var admin_page=$('.hidden-chip').val();
    var accountPermission = GetURLParameter('accountPermission');
    window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&overall="+postType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&accountPermission="+accountPermission , '_blank');
})
  $(document).on('click', '.btn_frequent_tag', function() {
     var admin_page_id=$('.hidden-chip').attr('id');
     var admin_page=$('.hidden-chip').val();
     var TagName = $(this).attr('value');
     var default_page = $('.hidden-pg').val();
        
        var accountPermission=GetURLParameter('accountPermission');
      window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
  })
    $(document).on('click', '.btn_post_tag', function() {
     var TagName = $(this).attr('value');
     var default_page = $('.hidden-pg').val();
     var admin_page_id=$('.hidden-chip').attr('id');
     var admin_page=$('.hidden-chip').val();
      var accountPermission = GetURLParameter('accountPermission');
      // alert(accountPermission); 
      window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ admin_page+"&admin_page_id="+ admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission , '_blank');
  })
  
  //local function


  function numberWithCommas(n) {
      var parts=n.toString().split(".");
      var res = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
      if(res == '')
        res = 0

      return res;
  }

    function kFormatter(num) {
      return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
  }
    function readmore(message){
        // alert("hi hi ");
          var string = String(message);
          var length = string.length; 
           // alert(length);
                  if (length > 500) {
            // alert("length is greater than 500");

              // truncate

              var stringCut = string.substr(0, 500);
               // alert(stringCut);
              // var endPoint = stringCut.indexOf(" ");

              //if the string doesn't contain any space then it will cut without word basis.
               
              // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
              string =stringCut.substr(0,length);
              // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
              // alert(string);
          }
          return string;


          }

          

    });
      </script>
      <style type="text/css">

        .sticky {
    position: fixed;
    top: 0;
    z-index: 100;
}
        .img-container {
  position: relative;
}

.img-container:after {
  content: " ";
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 99;
}

table.fixedHeader-floating {
     clear: both;
     position: fixed;
     top: 114px !important;
     z-index:5;
     top :0;
     left:300 !important;
     box-sizing: border-box;
 
  } 
  .NextLine
  {
    /*text-align:center;*/
    white-space:pre;
  }
table.dataTable tbody tr td {
    word-wrap: break-word;
    word-break: break-all;
}

  .tabcontrolHeader {
     clear: both;
     position: fixed;
    top:120px !important;
    z-index:20;
   
  }


  .myHeaderContent
  {
     margin-right:300px;
  }
  .myHeaderChip
  {
    padding-right:100px;
  }
  .myHeaderScoll
  {
     padding-right:300px;
  }

  .dataTables_wrapper {
      padding-top: 0px;
  }
  img {
      border-style: none;
  }
  #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #7e7979;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 2;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
  .btn-secondary:not(:disabled):not(.disabled).active, .btn-secondary:not(:disabled):not(.disabled):active, .show>.btn-secondary.dropdown-toggle {
    color: #fff !important;
    background-color: #545b62;
    border-color: #4e555b;
}
.btn {
    padding: 2px 6px;
    font-size: 14px;
    cursor: pointer;
}


      </style>
  <link href="{{asset('css/own.css')}}" rel="stylesheet">
  @endpush

  <!-- table.fixedHeader-floating {
     clear: both;
     position: fixed;
    top: 200px !important;
    z-index:5;
     top :0;
     left:0 !important;
     width: 100% !important;
  /*   margin: 0 auto;
       padding: 0 15px;*/
   /*    overflow-x: hidden;
        overflow-y: auto;*/
    
      box-sizing: border-box;
  /*
    background: red;*/
  /*  left:150px !important;*/
  /*  margin-right:30px;*/
   
  /*  width: 80% !important;*/
    /*background: transparent;*/
  } -->
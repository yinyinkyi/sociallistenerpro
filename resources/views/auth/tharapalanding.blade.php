<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="HekgzLAvPe7aA9Yi4Gzf8U4SX03Ho3bqWvF7Jitn">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="Shortcut Icon" type="image/png" href="https://www.tharapa-ai.com/img/1024x1024.png">

    <!-- Bootstrap core CSS -->
    <link href="https://www.tharapa-ai.com/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.tharapa-ai.com/css/mes.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://www.tharapa-ai.com/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link  href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
        rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <link href="https://www.tharapa-ai.com/css/clean-blog.min.css" rel="stylesheet">
    <title>bagan.ai</title>
    <meta charset="UTF-8">
    <script src="https://www.tharapa-ai.com/vendor/jquery/jquery.min.js"></script>

</head>
<body>

<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '225651228337535',
            xfbml: true,
            version: 'v3.2'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="https://www.tharapa-ai.com">tharapa-ai</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                                            <a class="nav-link" href="https://www.tharapa-ai.com">Home</a>
                                    </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.tharapa-ai.com/contact">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.tharapa-ai.com/privacy">Privacy Policy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="view_pages_link" href="{{route('login')}}">Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div style="margin-top:100px;">



    <style>
        header.masthead {
            margin-bottom: 50px;
            background: no-repeat center center;
            /*background-color: #868e96;*/
            background-attachment: scroll;
            position: relative;
            /*background-size: cover*/
        }
    </style>

    <div id="fb-root"></div>






    <!-- Page Header -->
    <header class="masthead"
            style="background-image: url('http://www.tharapa-ai.com/img/tharapa_aI.png');margin-top: 100px ">
        <div class="overlay"></div>
        <div class="container">
            <div class="row" style="margin-bottom: 0px">
                <div class="col-lg-8 col-md-10  col-sm-4  mx-auto">
                    <div class="site-heading">
                        <!-- <h1>Clean Blog</h1> -->
                        <!-- <span class="subheading">A Blog Theme by Start Bootstrap</span> -->
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="container justify-content-center">
        <div class="row">

            
            
            
            

            <div class="col-md-12 text-center">
                <a href="https://www.tharapa-ai.com/auth/facebook" role="button" class="btn btn-primary" style="color:white !important;">Login With Facebook</a>
            </div>


        </div>
    </div>



</div>
ိ္<!--&lt;!&ndash; Bootstrap core JavaScript &ndash;&gt;-->
<script src="https://www.tharapa-ai.com/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!--&lt;!&ndash; Custom scripts for this template &ndash;&gt;-->
<script src="https://www.tharapa-ai.com/js/clean-blog.min.js"></script>

</body>
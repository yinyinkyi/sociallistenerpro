@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

    <header class="" id="myHeader">
      <div class="row page-titles">
        <div class="col-md-3 col-8 align-self-center">
          <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Compare</h4>
        </div>
        <div class="col-md-9 col-4 align-self-center">
          <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
              <div class="chart-text m-r-10">
                <button type="button" id="btn_add_project" class="btn waves-effect waves-light btn-block btn-green">More Brands</button>
                <input type="hidden" value='{{ auth()->user()->company_id }}' id="hidden-user" name="hidden-user"/>
              </div>
              <div class="chart-text m-r-10">
                <button type="button" id="btn_remove_project" class="btn waves-effect waves-light btn-block btn-green">Clear All</button>
              </div>
              <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                <input type='text' class="form-control dateranges" style="datepicker" />
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <span class="ti-calendar"></span>
                    </span>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div id="add-project" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">        
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Add Brand</h4>
            <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <div class="message-box">
              <div class="form-group has-info">
                <label class="control-label">Brand</label>
                  <select id="brand_id" class="form-control custom-select">
                  </select>
                  <small class="form-control-feedback"> Choose a brand to compare </small> </div>
                  <hr> 
                  <button type="button" id="btn_add_brand" class="btn btn-green"><i class="fa fa-check"></i> Add </button>
            </div>
          </div>                              
        </div>
      </div>
    </div>

                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
                <!-- Row -->
    <div class="row" class="row s-topbar-u-div" id="page-div-1">
      <input type="hidden" value="{{ auth()->user()->default_page }}" class ="hidden-pg" name="hidden-pg"/>
        <div class="col-lg-6">
          <div class="card">
              <div class="card-body">
                  <h4 class="card-title text-info">Page Growth</h4>
                    <div style="display:none"  align="center" style="vertical-align: top;" id="fan-growth-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                <div id="fan-growth-chart" style="width:100%;height:300px"></div>  
              </div>
          </div>
        </div>
         <div class="col-lg-6">
          <div class="card">
              <div class="card-body">
                  <h4 class="card-title text-info">Page Like</h4>
                    <div style="display:none"  align="center" style="vertical-align: top;" id="page-like-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                <div id="page-like-chart" style="width:100%;height:300px"></div>  
              </div>
          </div>
      </div>
    </div>
            
    <div class="row" class="row s-topbar-u-div" id="page-div-1">
      <div class="col-lg-6">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title text-info">Page Summary</h4>
              <div style="display:none"  align="center" style="vertical-align: top;" id="summary_chart_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
            <div id="page-summary-chart" style="width:100%;height:300px"></div>  
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title text-info">Reactions, Comments and Shares</h4>
              <div style="display:none"  align="center" style="vertical-align: top;" id="reaction-pie-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
              <div id="reaction-pie-chart" style="width:100%;height:300px"></div>  
          </div>
        </div>
      </div>
    </div>
    <div class="row" class="row s-topbar-u-div">
    <div class="col-lg-12">
      <div class="card">
       <div class="card-header">
          <div class="row">
            <div class="col-md-2 col-6 align-self-center" style="padding-left:1.25rem">
              <h4 class="card-title m-b-0 text-info">Post Topics</h4>
            </div>
          </div>
          <div class="row" style="padding: 4px 0px">
            <div class="col-md-6 col-6 align-self-center" style="padding-left:1.25rem">
              <select class="form-control" id="ptagGroup_filter">
              <option value="">All Tags</option>
              @if (isset($ptagGroup) && count($ptagGroup)>0)
              @foreach($ptagGroup as $ptagGroup)
              <option value="{{$ptagGroup->id}}">{{$ptagGroup->name}}</option>
              @endforeach
              @endif
              </select>
            </div>
            <div class="col-md-6 col-6" style="text-align:right;padding-right:3rem">
              <select class="form-material form-control  select2 edit_tag" id="ptag_filter"  multiple="multiple" data-placeholder="Select tag">
              @if (isset($ptags) && count($ptags)>0)
              @foreach($ptags as $ptag)
              <option value="{{$ptag->id}}">{{$ptag->name}}</option>
              @endforeach
              @endif
              </select>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div style="display:none"  align="center" style="vertical-align: top;" id="post_tag_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
          <div id="post-tag-qty-line-chart"  style="width:50%;height:300px;float:left" ></div> 
          <div id="post-reaction-pie-chart" style="width:50%;height:300px;float:right;"></div>  
        </div>
        </div>
      </div>
    </div>
   <div class="row" class="row s-topbar-u-div" id="page-div-1">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-2 col-6 align-self-center" style="padding-left:1.25rem">
              <h4 class="card-title m-b-0 text-info">Comment Topics</h4>
            </div>
            <div class="col-md-10 col-6" style="text-align:right;padding-right:3rem">
                  Qty <input type="checkbox" id="tag-senti-qty-type"  class="js-switch" data-color="#26c6da" data-secondary-color="#f62d51" /> Percent
            </div>
          </div>
          <div class="row" style="padding: 4px 0px">
            <div class="col-md-6 col-6 align-self-center" style="padding-left:1.25rem">
              <select class="form-control" id="tagGroup_filter">
              <option value="">All Tags</option>
              @if (isset($tagGroup) && count($tagGroup)>0)
              @foreach($tagGroup as $tagGroup)
              <option value="{{$tagGroup->id}}">{{$tagGroup->name}}</option>
              @endforeach
              @endif
              </select>
            </div>
            <div class="col-md-6 col-6" style="text-align:right;padding-right:3rem">
               <select class="form-material form-control  select2 edit_tag" id="tag_filter"  multiple="multiple" data-placeholder="Select tag">
              @if (isset($tags) && count($tags)>0)
              @foreach($tags as $tag)
              <option value="{{$tag->id}}">{{$tag->name}}</option>
              @endforeach
              @endif
              </select>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div style="display:none"  align="center" style="vertical-align: top;" id="tag_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
          <div id="tag-qty-line-chart"  style="width:50%;height:300px;float:left"></div>
          <div id="tag-percent-chart"  style="width:50%;height:300px;float:left;display:none" ></div>  
          <div id="tag-sentiment-chart"  style="width:50%;height:300px;float:right;" ></div> 
          </div>
        </div>
      </div>
    </div>


   <div class="row" class="row s-topbar-u-div" id="page-div-1">
    <div class="col-lg-6" style="visibility: collapse;height:0px;">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-5 col-4 align-self-center" style="padding-left:1.25rem">
              <h4 class="card-title m-b-0 text-info">Post Sentiment</h4>
            </div>
            <div class="col-md-7 col-4" style="text-align:right;padding-right:3rem">
              Percent <input type="checkbox" id="post-senti-chart-type"  class="js-switch" data-color="#26c6da" data-secondary-color="#f62d51" /> Qty
            </div>
          </div>
        </div>
        <div class="card-body">
          <div style="display:none"  align="center" style="vertical-align: top;" id="post_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
            <div id="post-sentiment-chart" style="width:100%;height:300px"></div> 
            <div id="post-sentiment-qty-chart" style="width:100%;height:300px;display:none" ></div>  
        </div>
      </div>
    </div>
   <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-5 col-4 align-self-center" style="padding-left:1.25rem">
            <h4 class="card-title m-b-0 text-info">Comment Sentiment</h4>
            </div>
            <div class="col-md-7 col-4" style="text-align:right;padding-right:3rem">
            Percent <input type="checkbox" id="comment-senti-chart-type"  class="js-switch" data-color="#26c6da" data-secondary-color="#f62d51" /> Qty
            </div>
          </div>
        </div>
        <div class="card-body">
          <div style="display:none"  align="center" style="vertical-align: top;" id="comment_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
          <div id="comment-sentiment-chart" style="width:100%;height:300px"></div>  
          <div id="comment-sentiment-qty-chart" style="width:100%;height:300px;display:none" ></div> 
        </div>
      </div>
    </div>
  </div>
  <div class="row" id='comparison-div-4'>
    <div class="col-lg-12">
      <div class="card">
       <div class="card-body">
        <div style="display:none"  align="center" style="vertical-align: top;" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
           <h4 class="card-title text-info">Social Reaction</h4>
            <div id="div_reaction" ></div>
       </div>
      </div>
    </div>
  </div>
  <div class="row" id='comparison-div-5'>
    <div class="col-lg-12">
      <div class="card">
       <div class="card-body">
        <div style="display:none"  align="center" style="vertical-align: top;" id="top-post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
         <h4 class="card-title text-info">Top Posts</h4>
          <div id="div_top_post" ></div>
          </div>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div style="display:none"  align="center" style="vertical-align: top;" id="post-frequent-tag-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
          <h4 class="card-title text-info">Most Frequent Post Topics</h4>
          <div id="div_post_frequent_tag" >
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row" id='comparison-div-6'>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div style="display:none"  align="center" style="vertical-align: top;" id="frequent-tag-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
          <h4 class="card-title text-info">Most Frequent Comment Topics</h4>
          <div id="div_frequent_tag" >
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
  @push('scripts')
    <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>
     <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
     <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   
   <!--  <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript')}}" defer ></script> -->
    <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript')}}" defer></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script src="{{asset('assets/plugins/switchery/dist/switchery.min.js')}}"></script>
 <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
    
    <script>
        window.onscroll = function() {myFunction()};

        var header = document.getElementById("myHeader");
        var myHeaderContent = document.getElementById("myHeaderContent");
        var sticky = header.offsetTop;

        function myFunction() {//alert("ho");
          if (window.pageYOffset > sticky) {
            header.classList.add("s-topbar");
            header.classList.add("s-topbar-fix");
            myHeaderContent.classList.add("myHeaderContent");
          } else {
            header.classList.remove("s-topbar");
            header.classList.remove("s-topbar-fix");
            myHeaderContent.classList.remove("myHeaderContent");
          }
        }
    </script>
    <script type="text/javascript">
      var startDate;
      var endDate;
      var labelDate;
      var colors=[ "#4267b2","#382448","#01ad9d","#cb73a9","#a1c652","#7b858e","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];
      var colors_senti=["#28a745","#fb3a3a","#ffb22b"];
      

      /*var effectIndex = 2;
      var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/
      $(document).ready(function() {
        $('.select2').select2();
        //generate legend data
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
              $('.js-switch').each(function() {
                  new Switchery($(this)[0], $(this).data());
              });

      var changeCheckbox_tag = document.querySelector('#tag-senti-qty-type');

        changeCheckbox_tag.onchange = function() {
          if(changeCheckbox_tag.checked == true) // qty
          {
            $('#tag-sentiment-chart').hide();
            $('#tag-percent-chart').show();
           
           
          }
          else //sentiment
          {
            $('#tag-sentiment-chart').show();
            $('#tag-percent-chart').hide();
          }
          window.dispatchEvent(new Event('resize'));
        };

        var changeCheckbox = document.querySelector('#post-senti-chart-type');

        changeCheckbox.onchange = function() {
          if(changeCheckbox.checked == true) // qty
          {
            $('#post-sentiment-chart').hide();
            $('#post-sentiment-qty-chart').show();
           
           
          }
          else //percentage
          {
            $('#post-sentiment-qty-chart').hide();
            $('#post-sentiment-chart').show();
          }
          window.dispatchEvent(new Event('resize'));
        };

          var changeCheckbox_cmt = document.querySelector('#comment-senti-chart-type');

          changeCheckbox_cmt.onchange = function() {
            if(changeCheckbox_cmt.checked == true) // qty
            {
              $('#comment-sentiment-chart').hide();
              $('#comment-sentiment-qty-chart').show();
             
             
            }
            else //percentage
            {
              $('#comment-sentiment-qty-chart').hide();
              $('#comment-sentiment-chart').show();
            }
            window.dispatchEvent(new Event('resize'));
          };

          Clear_reaction_table();
          Clear_top_post_table();
          Clear_frequent_tag_table();
          Clear_post_frequent_tag_table();
            //Clear_page_summary_table();
           // Clear_page_ranking_table();
var compare_count=0;
var Legend_Data=[];var brand_id_arr=[];var reachseriesList=[];var reachLabel = [];var arr_reach_total=[];var arr_reach_label=[];
var fanseriesList=[];var fanLabel = [];var arr_fan_total=[];var arr_fan_label=[];var fan_abs_diff=[];// var min_fan_array=0;
var likeseriesList=[];var likeLabel = [];var arr_like_total=[];var arr_like_label=[];var likemarkpointlist=[];arr_tag_label=[];
var  post=[];var share=[];var comment=[];var summaryLabel=[];
var  tag_pos=[];var tag_neg=[];var tag_neutral=[];var tagsentiLabel=[];var pos_percent_tag=[];var neg_percent_tag=[];var tag_qty_list=[];var neutral_percent_tag=[];
var tagmarkpointlist=[] ;var tagpercentmarkpointlist=[];
var tag_qty_Label = [];var tag_qty_seriesList=[];post_tag_qty_seriesList=[];
var  arr_ov_positive=[];var arr_ov_negative=[];var arr_ov_neutral=[];var OverallLabel=[];var arr_ov_markpointlist=[];var arr_ovQty_markpointlist=[];
var  arr_ov_qty_positive=[];var arr_ov_qty_negative=[];var arr_ov_qty_neutral=[]
var  arr_cmt_positive=[];var arr_cmt_negative=[];var arr_cmt_neutral=[];var CommentLabel=[];var PostLabel=[];
var arr_cmt_markpointlist=[];var arr_cmtQty_markpointlist=[];
var  arr_cmt_qty_positive=[];var arr_cmt_qty_negative=[];var arr_cmt_qty_neutral=[];
var Social_seriesList=[];var socialLabel = [];var arr_social_total=[];
var Senti_xAxisData=[];var Senti_data1=[];var Senti_data2=[];
var RSCseriesList=[];RseriesList=[];




    startDate = moment().subtract(1, 'month');
    endDate = moment();
    var periodType= '';
       
            if(periodType == '')
            {
              periodType='month';
            }
                  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function Rebind_Pages()
{
    // alert("popular");
    var brand_id = GetURLParameter('pid');
    var company_id = $('#hidden-user').val();
     // var brand_id = $('#hidden-user').val();
    // console.log('comapny id is'+company_id);
    // alert(company_id);
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#brand_id").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getallpages')}}", // This is the URL to the API
      data: { brand_id:brand_id,company_id:company_id}
    })
    .done(function( data ) {//   <option value="">Choose</option>
       // console.log(data);
       // alert("hello");
           $('#brand_id').append($('<option>', {
            value: '',
            text: 'Choose'
        }));
     for(var i in data) {

      $('#brand_id').append($('<option>', {
            value: data[i],
            text: data[i]
        }));
     }
    })
    .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

function Bind_Pages()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
     var company_id = $('#hidden-user').val();
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#brand_id").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getallpages')}}", // This is the URL to the API
      data: { brand_id:brand_id ,company_id:company_id}
    })
    .done(function( data ) {//   <option value="">Choose</option>
       console.log("bindPages"+data);
           $('#brand_id').append($('<option>', {
            value: '',
            text: 'Choose'
        }));
     for(var i in data) {

      $('#brand_id').append($('<option>', {
            value: data[i],
            text: data[i]
        }));
     }
// alert(data[0]);
     brand_id_arr.push(data[0]);
     Legend_Data.push(data[0]);
     // alert(data[0]);
     ChooseDate(startDate,endDate,data[0],0,labelDate);
    
     
    })
    .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

Bind_Pages();
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Comparison'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
      //  console.log(xhr.statusText);
      // console.log(textStatus);
      // console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();
function ChooseDate(start,end,brand_id,sr_of_brand,label)
{
   $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
          // alert(startDate.format('YYYY MM DD'));
          // alert(endDate);
         var brand_id = brand_id;
         // alert(brand_id);
         var split_name = brand_id.split("-");
          if(split_name.length > 0)
          {
            if(isNaN(split_name[split_name.length-1]) == false )
              brand_id = split_name[split_name.length-1];
          }
         // alert(brand_id);
         var date_preset='this_month'; 
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

        FanGrowth(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand,date_preset);
        socialdetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        postReaction(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        TopPosts(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        FrequentTag(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        PostFrequentTag(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
     
        
        TagQty(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       
        PostTagQty(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        PageLike(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand,date_preset);
       
       PageSummary(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       PostSentiment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       CommentSentiment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
}
function PostSentiment(fday,sday,brand_id,post_sr_of_brand)
    {
      var PostSentiChart = echarts.init(document.getElementById('post-sentiment-chart'));
      var PostSentiChartQty = echarts.init(document.getElementById('post-sentiment-qty-chart'));
      $('#post_spin').show();
     // $("#fan-growth-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id}
    })
    .done(function(data) {
       
                var ov_total=parseInt(data[2][0]['ov_positive'])+parseInt(data[2][0]['ov_negative'])+parseInt(data[2][0]['ov_neutral'])
                var pcent_positive=parseFloat((parseInt(data[2][0]['ov_positive'])/ov_total)*100).toFixed(2);
                pcent_positive = isNaN(pcent_positive)?0:pcent_positive;
                if(pcent_positive == 0)
                pcent_positive='-';
                 arr_ov_positive.push(pcent_positive);
                arr_ov_qty_positive.push(parseInt(data[2][0]['ov_positive']));
                var pcent_negative=parseFloat((parseInt(data[2][0]['ov_negative'])/ov_total)*100).toFixed(2);
                pcent_negative = isNaN(pcent_negative)?0:pcent_negative;
                if(pcent_negative == 0)
                pcent_negative='-';
                 arr_ov_negative.push(pcent_negative);
                arr_ov_qty_negative.push(parseInt(data[2][0]['ov_negative']));
                var pcent_neutrual=parseFloat((parseInt(data[2][0]['ov_neutral'])/ov_total)*100).toFixed(2);
                pcent_neutrual = isNaN(pcent_neutrual)?0:pcent_neutrual;
                if(pcent_neutrual == 0)
                pcent_neutrual='-';
                 arr_ov_neutral.push(pcent_neutrual);
                arr_ov_qty_neutral.push(parseInt(data[2][0]['ov_neutral']));
         
                if(jQuery.inArray(Legend_Data[post_sr_of_brand], OverallLabel)=='-1')
                OverallLabel.push(Legend_Data[post_sr_of_brand]);

 arr_ov_markpointlist.push(
{
  name : 'Have something', 
  value : pcent_neutrual, 
  xAxis: 100, 
  yAxis: Legend_Data[post_sr_of_brand], 
  symbol:'image://https://graph.facebook.com/'+data[3][0]+'/picture?type=large', 
  symbolSize:30,

}
        )
 arr_ovQty_markpointlist.push({
  name : 'Have something', 
  value : parseInt(data[2][0]['ov_neutral']), 
  xAxis: ov_total, 
  yAxis: Legend_Data[post_sr_of_brand], 
  symbol:'image://https://graph.facebook.com/'+data[3][0]+'/picture?type=large', 
  symbolSize:30,

 })

        option= null;
        option_qty=null;
option = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    // grid: {
    //   left: '3%',
    //   right: '10%',
    //   bottom: '3%',
    //   containLabel: true
    // },
    xAxis:  {
      type: 'value',
      name:'Senti',
       axisLabel: {
            formatter: '{value}%',

        },
        max:100
    },
    // xAxis:  {
    // //   scale: true,
    //   type: 'value',
    //   name:'Senti Percent',
    //   min:0,
    //     //max: 5000,
    //     interval: 200
    // },
    yAxis: {
      type: 'category', 
      name:'Page Name',
       axisLabel:{
      rotate:75,
      interval:0,
    },
      data:OverallLabel,

},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_neutral,

   markPoint: {
               // symbol:'roundRect',
               // symbol:'image://https://graph.facebook.com/393033680732657/picture?type=large',

                symbolSize:[50, 50], 
                symbolOffset:[20,-5],
               data : arr_ov_markpointlist,
                label: {
                    offset: [0, 0],
                    color: '#ffffff',
                    formatter: [
                      ''
                    ].join('\n'),
                }
            },
}
]
};
option_qty = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    // grid: {
    //   left: '3%',
    //   right: '10%',
    //   bottom: '3%',
    //   containLabel: true
    // },
    
    xAxis:  {
    //   scale: true,
      type: 'value',
      name:'Qty',
      min:0,
        //max: 5000,
        //interval: 200
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      axisLabel:{
      rotate:75,
      interval:0,
    },
      data:OverallLabel,

},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_qty_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_qty_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_qty_neutral,
     markPoint: {
               // symbol:'roundRect',
               // symbol:'image://https://graph.facebook.com/393033680732657/picture?type=large',

                symbolSize:[50, 50], 
                symbolOffset:[20,-5],
               data : arr_ovQty_markpointlist,
                label: {
                    offset: [0, 0],
                    color: '#ffffff',
                    formatter: [
                      ''
                    ].join('\n'),
                }
            },
}
]
};
 
    $( "#post_spin" ).hide();
    // alert(fday);
    PostSentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            PostSentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    PostSentiChart.on('click', function (params) {
    if (params.event.handled !== true) {
   params.event.handled = true;
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var CmtType ='';
   if(params.seriesName == 'positive')
    postType = 'pos'
    else if(params.seriesName == 'negative')
   postType = 'neg'
    else
      postType = 'neutral'

     var tag_id = $( "#tag_filter" ).val();
   // alert(tag_id);
  window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&overall="+postType+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+ params.name , '_blank');
}
});
PostSentiChartQty.setOption(option_qty, true), $(function() {
    function resize() {
        setTimeout(function() {
            PostSentiChartQty.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
PostSentiChartQty.on('click', function (params) {
   if (params.event.handled !== true) {
   params.event.handled = true;
   // console.log(params);
   // alert(params.name); // KBZPay
   // alert(params.seriesName); //positive
   // alert(params.value);//count 6.51
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var CmtType ='';
    if(params.seriesName == 'positive')
    postType = 'pos'
    else if(params.seriesName == 'negative')
   postType = 'neg'
    else
      postType = 'neutral'

     var tag_id = $( "#tag_filter" ).val();
   // alert(tag_id);
  window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&overall="+postType+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+ params.name , '_blank');
}
});
     var arrayLength=brand_id_arr.length;
     post_sr_of_brand = post_sr_of_brand +1;
     if(post_sr_of_brand<arrayLength )
     {
      PostSentiment(fday,sday,brand_id_arr[post_sr_of_brand],post_sr_of_brand);
      $("#fan-growth-spin").show();

     }
     else
     {
       $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function CommentSentiment(fday,sday,brand_id,comment_sr_of_brand)
    {
      var CommentSentiChart = echarts.init(document.getElementById('comment-sentiment-chart'));
      var CommentSentiChartQty = echarts.init(document.getElementById('comment-sentiment-qty-chart'));
      $('#comment_spin').show();
      //$("#fan-growth-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id}
    })
    .done(function(data) {
                var comment_total=parseInt(data[1][0]['cmt_positive'])+parseInt(data[1][0]['cmt_negative'])+parseInt(data[1][0]['cmt_neutral'])+parseInt(data[1][0]['cmt_NA']);
                 // alert(comment_total);
                var neutralTotal=parseInt(data[1][0]['cmt_neutral'])+parseInt(data[1][0]['cmt_NA']);
                var pcent_positive=parseFloat((parseInt(data[1][0]['cmt_positive'])/comment_total)*100).toFixed(2);
                pcent_positive = isNaN(pcent_positive)?0:pcent_positive;
                if(pcent_positive == 0)
                pcent_positive='-';
                arr_cmt_positive.push(pcent_positive);
                arr_cmt_qty_positive.push(parseInt(data[1][0]['cmt_positive']));
                var pcent_negative=parseFloat((parseInt(data[1][0]['cmt_negative'])/comment_total)*100).toFixed(2);
                pcent_negative = isNaN(pcent_negative)?0:pcent_negative;
                if(pcent_negative == 0)
                pcent_negative='-';
                 arr_cmt_negative.push(pcent_negative);
                 arr_cmt_qty_negative.push(parseInt(data[1][0]['cmt_negative']));
                var pcent_neutrual=parseFloat((neutralTotal/comment_total)*100).toFixed(2);
                pcent_neutrual = isNaN(pcent_neutrual)?0:pcent_neutrual;
                if(pcent_neutrual == 0)
                pcent_neutrual='-';
                arr_cmt_neutral.push(pcent_neutrual);
                arr_cmt_qty_neutral.push(neutralTotal);
                 
                if(jQuery.inArray(Legend_Data[comment_sr_of_brand], CommentLabel)=='-1')
                CommentLabel.push(Legend_Data[comment_sr_of_brand]);
              // alert(arr_cmt_positive);
               var imgurl = data[4][0];
arr_cmt_markpointlist.push(
{
  name : 'Have something', 
  value : pcent_neutrual, 
  xAxis: 100, 
  yAxis: Legend_Data[comment_sr_of_brand], 
  // symbol:'image://https://graph.facebook.com/'+data[3][0]+'/picture?type=large', 
  symbol:'image://'+imgurl,
  symbolSize:30,

}
        )
 arr_cmtQty_markpointlist.push({
  name : 'Have something', 
  value : neutralTotal, 
  xAxis: comment_total, 
  yAxis: Legend_Data[comment_sr_of_brand], 
  // symbol:'image://https://graph.facebook.com/'+data[3][0]+'/picture?type=large', 
  symbol:'image://'+imgurl,
  symbolSize:30,

 })

        option= null;
        option_qty= null;
option = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    // grid: {
    //   left: '3%',
    //   right: '10%',
    //   //bottom: '3%',
    //   containLabel: true
    // },
    xAxis:  {
      type: 'value',
      name:'Senti',
       axisLabel: {
            formatter: '{value}%'
        },
        max:100
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      axisLabel:{
      rotate:75,
      interval:0,
    },
      data:CommentLabel
},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_neutral,
   markPoint: {
               // symbol:'roundRect',
               // symbol:'image://https://graph.facebook.com/393033680732657/picture?type=large',

                symbolSize:[50, 50], 
                symbolOffset:[20,-5],
               data : arr_cmt_markpointlist,
                label: {
                    offset: [0, 0],
                    color: '#ffffff',
                    formatter: [
                      ''
                    ].join('\n'),
                }
            },
}
]
};
option_qty = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    // grid: {
    //   left: '3%',
    //   right: '10%',
    //   bottom: '3%',
    //   containLabel: true
    // },
    xAxis:  {
    //   scale: true,
      type: 'value',
      name:'Qty',
      min:0,
        //max: 5000,
        // interval: 200
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      axisLabel:{
      rotate:75,
      interval:0,
    },
      data:CommentLabel
},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_qty_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_qty_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:8,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_qty_neutral,
   markPoint: {
               // symbol:'roundRect',
               // symbol:'image://https://graph.facebook.com/393033680732657/picture?type=large',

                symbolSize:[50, 50], 
                symbolOffset:[30,-5],
               data : arr_cmtQty_markpointlist,
                label: {
                    offset: [0, 0],
                    color: '#ffffff',
                    formatter: [
                      ''
                    ].join('\n'),
                }
            },
}
]
};
 
  $( "#comment_spin" ).hide();

  

    CommentSentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            CommentSentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
     CommentSentiChart.on('click', function (params) {
       if (params.event.handled !== true) {
   params.event.handled = true;
   //console.log(params);
   // alert(params.name); // KBZPay
   // alert(params.seriesName); //positive
   // alert(params.value);//count 6.51
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var CmtType ='';
    if(params.seriesName == 'positive')
    CmtType = 'pos'
    else if(params.seriesName == 'negative')
   CmtType = 'neg'
    else
      CmtType = 'neutral'

     var tag_id = $( "#tag_filter" ).val();
   // alert(tag_id);
    var default_page = $( ".hidden-pg" ).val();

   window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid')+"&CmtType="+CmtType+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+ params.name+"&default_page="+default_page, '_blank');
 }
});
CommentSentiChartQty.setOption(option_qty, true), $(function() {
    function resize() {
        setTimeout(function() {
            CommentSentiChartQty.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
 CommentSentiChartQty.on('click', function (params) {
   if (params.event.handled !== true) {
   params.event.handled = true;
   //console.log(params);
   // alert(params.name); // KBZPay
   // alert(params.seriesName); //positive
   // alert(params.value);//count 6.51
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var CmtType ='';
   if(params.seriesName == 'positive')
    CmtType = 'pos'
    else if(params.seriesName == 'negative')
   CmtType = 'neg'
    else
      CmtType = 'neutral'

     var tag_id = $( "#tag_filter" ).val();
      var default_page = $( ".hidden-pg" ).val();
   // alert(tag_id);
   window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid')+"&CmtType="+CmtType+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+ params.name+"&default_page="+default_page , '_blank');
 }
});

      var arrayLength=brand_id_arr.length;
     comment_sr_of_brand = comment_sr_of_brand +1;
     if(comment_sr_of_brand<arrayLength )
     {
      CommentSentiment(fday,sday,brand_id_arr[comment_sr_of_brand],comment_sr_of_brand);
      $("#fan-growth-spin").show();
     }
     else
     {
      $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function PageSummary(fday,sday,brand_id,summary_sr_of_brand)
    {
      var SummaryChart = echarts.init(document.getElementById('page-summary-chart'));
      $('#summary_chart_spin').show();
      //$("#fan-growth-spin").show();
     
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id}
    })
    .done(function(data) {
       // console.log(data);
        
      
                post.push(data[0][0]['total_post']);
                share.push(data[0][0]['shared']);
                comment.push(data[1][0]['total_comment']);
          
                if(jQuery.inArray(Legend_Data[summary_sr_of_brand], summaryLabel)=='-1')
                  summaryLabel.push(Legend_Data[summary_sr_of_brand]);


        option= null;
option = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        // axisPointer: {
        //     type: 'cross'
        // },
      
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '10%',
    bottom:  '5%',
            containLabel: true
        },
         legend: {
        data:['post','comment','share'],
      
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name: 'Page',
             boundaryGap: true,
             axisLabel:{
                        rotate:30,
                        interval:0,
                      },
            axisTick: {
                alignWithLabel: true
            },
        
            data: summaryLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Count',
            scale:true,
            position: 'left',
         
            axisLabel: {
        formatter: function (e) {
            return nFormatter(e,1,1);
        }
    }
    
     }
    ],
    series: [{
            name:'post',
            type:'bar',
            smooth: 0.2,
            color:colors[0],
            barMaxWidth:30,
            barMinHeight:2,
            data:post
          
        },{
            name:'comment',
            type:'bar',
            smooth: 0.2,
            color:colors[1],
            barMaxWidth:30,
            barMinHeight:2,
            data:comment
          
        },{
            name:'share',
            type:'bar',
            smooth: 0.2,
            color:colors[2],
            barMaxWidth:30,
            barMinHeight:2,
            data:share
          
        }]
};
 
    $( "#summary_chart_spin" ).hide();
   
    SummaryChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            SummaryChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
      var arrayLength=brand_id_arr.length;
     summary_sr_of_brand = summary_sr_of_brand +1;
     if(summary_sr_of_brand<arrayLength )
     {
      PageSummary(fday,sday,brand_id_arr[summary_sr_of_brand],summary_sr_of_brand);
      $("#fan-growth-spin").show();

     }
     else
     {
       $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function tagsentimentData(fday,sday,brand_id,summary_sr_of_brand)
    {
    
      let main = document.getElementById("tag-sentiment-chart");
      let existInstance = echarts.getInstanceByDom(main);

      if (existInstance) {
            if (true) {
                echarts.init(main).dispose();
            }
        }


      var TagSentiChart = echarts.init(main);

        let main1 = document.getElementById("tag-percent-chart");
        let existInstance1 = echarts.getInstanceByDom(main1);
        
        if (existInstance1) {
            if (true) {
                echarts.init(main1).dispose();
            }
        }

      var TagPercentChart = echarts.init(main1);

      $('#tag_spin').show();
      //$("#fan-growth-spin").show();
     
     var  temp_tag_pos = [];
     var  temp_tag_neg = [];
     var temp_tag_neutral = [];
     var Page_id='';
     var imgurl ='';
      var pos_total = 0;
     var neg_total =0;
     var neutral_total =0;
     var all_total=0;
     var percent_total=0;
     var  tagpcentvalue = 100;
     var tag_id = $( "#tag_filter" ).val();
     var tags = $('#tag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| '); 


       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getTagSentiment')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id,tag_id:tag_id,tags:tags}
    })
    .done(function(data) { 
   

       for(var i in data) 
        {
           var res = findInArray(tag_id, data[i].tagId);
           
            if (res !== -1 ) {

               
                temp_tag_pos.push(Math.round(data[i].positive));
                temp_tag_neg.push(Math.round(data[i].negative));
                temp_tag_neutral.push(Math.round(data[i].neutral));
               }
             
              Page_id = data[i].page_id;
              imgurl = data[i].imgurl;
        }
        $.each(temp_tag_pos,function(){pos_total+=parseInt(this) || 0;});
        $.each(temp_tag_neg,function(){neg_total+=parseInt(this) || 0;});
        $.each(temp_tag_neutral,function(){neutral_total+=parseInt(this) || 0;});
        all_total = parseInt(pos_total)+parseInt(neg_total)+parseInt(neutral_total);
        var pos_percent_total=parseFloat((pos_total/all_total)*100).toFixed(2);
        var neg_percent_total=parseFloat((neg_total/all_total)*100).toFixed(2);
        var neutral_percent_total=parseFloat((neutral_total/all_total)*100).toFixed(2);
        pos_percent_total = isNaN(pos_percent_total)?0:pos_percent_total;
        neg_percent_total = isNaN(neg_percent_total)?0:neg_percent_total;
        neutral_percent_total = isNaN(neutral_percent_total)?0:neutral_percent_total;
        var percent_total = pos_percent_total+neg_percent_total+neutral_percent_total;
        if(pos_percent_total==0 && neg_percent_total==0 && neutral_percent_total==0)
        {
          tagpcentvalue = 0;
        }



                tag_pos.push(pos_total);
                tag_neg.push(neg_total);
                tag_neutral.push(neutral_total);
                pos_percent_tag.push(pos_percent_total);
                neg_percent_tag.push(neg_percent_total);
                neutral_percent_tag.push(neutral_percent_total);

                tag_qty_list.push({
                value:all_total,
                itemStyle:{color:colors[summary_sr_of_brand]},
          
        },

                           );
         

                if(jQuery.inArray(Legend_Data[summary_sr_of_brand], tagsentiLabel)=='-1')
                  tagsentiLabel.push(Legend_Data[summary_sr_of_brand]);
                tagmarkpointlist.push(
              {
                name : 'Have something', 
                value : all_total, 
                xAxis: all_total, 
                yAxis: Legend_Data[summary_sr_of_brand], 
                // symbol:'image://https://graph.facebook.com/'+Page_id+'/picture?type=large', 
                symbol:'image://'+imgurl,
                symbolSize:30,

              }
                      )

                 tagpercentmarkpointlist.push(
              {
                name : 'Have something', 
                value : tagpcentvalue, 
                xAxis: tagpcentvalue, 
                yAxis: Legend_Data[summary_sr_of_brand], 
                // symbol:'image://https://graph.facebook.com/'+Page_id+'/picture?type=large',
                symbol:'image://'+imgurl, 
                symbolSize:30,

              }
                      )



        option= null;
        option1=null;
          option= {
              color: colors_senti,

              tooltip: {
                  trigger: 'axis',
                  // axisPointer: {
                  //     type: 'cross'
                  // },
                
              },
                    grid: {
                    top:    60,
              
              left:   '5%',
              right:  '10%',
              bottom:  '5%',
                      containLabel: true
                  },
                        legend: {
                  data:['positive','negative','neutral'],
                
             
                   padding :0,
                 
                  
                  },


               toolbox: {
                      show : true,
                      feature : {
                          mark : {show: false},
                          dataView : {show: false, readOnly: false},
                          magicType : {show: false, type: ['line','bar']},
                          restore : {show: false},
                          saveAsImage : {show: false}
                      }
                  },

              xAxis: [
                  {
                    type: 'value',
                      name: 'Count',
                      position: 'left',
                      min:0,
                               axisLabel: {
                  formatter: function (e) {
                      return nFormatter(e,1,1);
                        }
                      }

               
                     
                  }
              ],
              yAxis: [
                  {
                      type: 'category',
                      name: 'Page',
                      axisLabel:{
                                  rotate:45,
                                  interval:0,
                                },
                      boundaryGap: true,

                      axisTick: {
                          alignWithLabel: true
                      },
                  
                      data: Legend_Data
              
               }
              ],
              series: [{
                      name:'positive',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[0],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Qty',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:tag_pos
                    
                  },{
                      name:'negative',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[1],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Qty',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:tag_neg,
                                     
                  },
                  {
                      name:'neutral',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[2],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Qty',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:tag_neutral,
                       markPoint: {
                         // symbol:'roundRect',
                         // symbol:'image://https://graph.facebook.com/393033680732657/picture?type=large',

                          symbolSize:[50, 50],// 容器大小
                          symbolOffset:[20,-5],//位置偏移
                         data : tagmarkpointlist,
                          label: {
                              offset: [0, 0],
                              color: '#ffffff',
                              formatter: [
                                ''
                              ].join('\n'),
                          }
                      },
                      
                    
                  }

                  ]
          };

          option1 = {
              color: colors_senti,

              tooltip: {
                  trigger: 'axis',
                  // axisPointer: {
                  //     type: 'cross'
                  // },
                
              },
                    grid: {
                    top:    60,
              
              left:   '5%',
              right:  '10%',
              bottom:  '5%',
                      containLabel: true
                  },
                        legend: {
                  data:['positive','negative','neutral'],
                
             
                   padding :0,
                 
                  
                  },


               toolbox: {
                      show : true,
                      feature : {
                          mark : {show: false},
                          dataView : {show: false, readOnly: false},
                          magicType : {show: false, type: ['line','bar']},
                          restore : {show: false},
                          saveAsImage : {show: false}
                      }
                  },

              xAxis: [
                  {
                      type: 'value',
                      name:'Count',
                       axisLabel: {
                            formatter: '{value}%'
                        },
                        max:100
                  }
              ],
              yAxis: [
                  {
                         type: 'category',
                      name: 'Page',
                      axisLabel:{
                                  rotate:45,
                                  interval:0,
                                },
                      boundaryGap: true,

                      axisTick: {
                          alignWithLabel: true
                      },
                  
                      data: Legend_Data
              
               }
              ],
              series: [{
                      name:'positive',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[0],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Percent',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:pos_percent_tag
                    
                  },{
                      name:'negative',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[1],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Percent',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:neg_percent_tag
                    
                  },
                  {
                      name:'neutral',
                      type:'bar',
                      smooth: 0.2,
                      color:colors_senti[2],
                      barMaxWidth:30,
                      barMinHeight:2,
                      stack: 'Percent',
                      label: {
                        normal: {
                          show: true,
                          position: 'insideLeft'
                        }
                      },
                      data:neutral_percent_tag,
                       markPoint: {
                       
                          symbolSize:[50, 50],// 容器大小
                          symbolOffset:[20,-5],//位置偏移
                         data : tagpercentmarkpointlist,
                          label: {
                              offset: [0, 0],
                              color: '#ffffff',
                              formatter: [
                                ''
                              ].join('\n'),
                          }
                      },
                    
                  }

                  ]
          };
 
        $( "#tag_spin" ).hide();
       
        TagSentiChart.setOption(option, true), $(function() {
        function resize() {
            setTimeout(function() {
                TagSentiChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });
       TagSentiChart.on('click', function (params) {
          if (params.event.handled !== true) {
       params.event.handled = true;
       // console.log("tags");
       // console.log(params);
       // console.log(params.name); // xaxis data = tag name
       // console.log(params.seriesName); //bar type name ="positive"
       // console.log(params.value);//count 8
       // alert(params.value);// count of sentiment
       // alert(params.seriesName); //positive or negative
       // alert(params.name); // Page Name (KBZ)
       // alert(params);
       var pid = GetURLParameter('pid');
       var source = GetURLParameter('source');
       var CmtType ='';
          if(params.seriesName == 'positive')
           CmtType = 'pos'
          else if(params.seriesName == 'negative')
           CmtType = 'neg'
          else
           CmtType = 'neutral'

         var tag_id = $('#tag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');

         var default_page = $('.hidden-pg').val();

       window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+tag_id+"&CmtType="+CmtType+"&fday="+GetStartDate() +"&sday="+ GetEndDate() +"&admin_page="+ params.name+"&default_page="+default_page, '_blank');
     }
    });
        TagPercentChart.setOption(option1, true), $(function() {
        function resize() {
            setTimeout(function() {
                TagPercentChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });

         TagPercentChart.on('click', function (params) {
       if (params.event.handled !== true) {
       params.event.handled = true;
      
       var pid = GetURLParameter('pid');
       var source = GetURLParameter('source');
       var CmtType ='';
         if(params.seriesName == 'positive')
         CmtType = 'pos'
        else if(params.seriesName == 'negative')
         CmtType = 'neg'
        else
          CmtType = 'neutral'


         //var tag_id = $( "#tag_filter" ).val();
         var tag_id = $('#tag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');

       var default_page = $('.hidden-pg').val();
       window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+tag_id+"&CmtType="+CmtType+"&fday="+GetStartDate() +"&sday="+ GetEndDate() +"&admin_page="+ params.name+"&default_page="+default_page , '_blank');
     }
    });
          var arrayLength=brand_id_arr.length;
         summary_sr_of_brand = summary_sr_of_brand +1;
         if(summary_sr_of_brand<arrayLength )
         {
          tagsentimentData(fday,sday,brand_id_arr[summary_sr_of_brand],summary_sr_of_brand);
          $("#fan-growth-spin").show();

         }
         else
         {
           $("#fan-growth-spin").hide();
         }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
  function findInArray(ar, val) {
    for (var i = 0,len = ar.length; i < len; i++) {
        if ( ar[i] == val ) {
            return i;
        }
    }
    return -1;
}



function FanGrowth(fday,sday,page_name,fan_sr_of_brand,date_preset)
{
  // alert(page_name);
    var FanGrowthChart = echarts.init(document.getElementById('fan-growth-chart'));
    
    $('#fan-growth-spin').show();
    var brand_id = GetURLParameter('pid');
    
    $.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPageFanDif')}}", // This is the URL to the API
      data: { date_preset: date_preset,page_name:page_name,fday:fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {
     console.log(data);
        var fan=[];
        var fan_abs=[];
        var fan_diff=[];
        var fan_temp_diff=[];
        var fan_total=0;
        var new_date_arr=[];
        var notFound_arr = [] ; 
        var page_id='';
        var imgurl = '';
        var fanmarkpointlist =[];
// alert('hey');
        var color = [];
        for(var i in data) {
            if(parseInt(i) !== 0)
            {
                fan.push(data[i].fan);
                fan_abs.push(data[i].fan_abs_diff);
                color.push(data[i].color);
                fan_temp_diff.push(data[i].fan_diff);
                new_date_arr.push(data[i].end_time);

                if(jQuery.inArray(data[i].end_time, fanLabel)=='-1')
                {
                   fanLabel.push(data[i].end_time);
                   if(fan_sr_of_brand > 0)
                   {
                        notFound_arr.push(data[i].end_time);
                   }
                }
               page_id=  data[i].page_id; 
               imgurl=  data[i].imgurl;
               var checkcolor = data[i].color;
            }
        }
        // alert(fanLabel);
        var brandcolor = ''; var datacolor='';
        if (checkcolor == null || checkcolor == 'undefined'){
          // alert('hello');
          brandcolor = colors;
          datacolor  = colors[fan_sr_of_brand];
        }
        else{
          brandcolor = color;
          datacolor  = color;
        }
        fanLabel.sort(function(a, b){
            var dateA=new Date(a), dateB=new Date(b)
            return dateA-dateB //sort by date ascending
        })
// alert(fanLabel);
        for(var j in fanLabel) {//alert(fanLabel[j]);
            var found =jQuery.inArray(fanLabel[j], new_date_arr);//if not found fanLabel data in new date arr put it '0', it means that new date arr of page hasn't complete data for each day.
            if(found=='-1')
                fan_diff.push('0');
            else
                fan_diff.push(fan_temp_diff[found]);
       }
       
        for(var NF in notFound_arr) {//alert(fanLabel[j]);
            var found =jQuery.inArray(notFound_arr[NF], fanLabel);//if not found fanLabel data in new date arr put it '0', it means that new date arr of page hasn't complete data for each day.
            //  alert(found + fanLabel[j]);
            if(found > -1){
              // alert("FD"+FD);
                for(var FD in brand_id_arr)
                {
                    if(FD < brand_id_arr.length-1){
                        fanseriesList[FD]['data'].splice(found, 0, 0);
                    }          
                }
            }
         }
     
        $.each(fan,function(){fan_total+= parseInt(this) || 0;});
        arr_fan_label.push(Legend_Data[fan_sr_of_brand]+' : '+ formatNumber(fan_total));
        arr_fan_total.push(fan_total);
        fan_abs_diff.push(fan_abs);
 
        fanmarkpointlist.push({
            name : 'Have something', 
            value : fan_diff[fan_diff.length-1], 
            xAxis: fanLabel[fanLabel.length-1], 
            yAxis: fan_diff[fan_diff.length-1], 
            // symbol:'image://https://graph.facebook.com/'+page_id+'/picture?type=large', 
            symbol:'image://'+imgurl, 
        })

        fanseriesList.push({
            name:Legend_Data[fan_sr_of_brand],
            type:'line',
            smooth: 0.2,
            color:datacolor,
            barMinHeight:2,
            data:fan_diff,markPoint: {
                symbolSize:20,// 容器大小
                symbolOffset:[20,-5],//位置偏移
               data : fanmarkpointlist,
                label: {
                    offset: [0, 0],
                    color: '#ffffff',
                    formatter: [
                      ''
                    ].join('\n'),
                }
            },
        },);
        option_growth= null;

        option_growth = {
            color: brandcolor,
            tooltip: {
                trigger: 'axis',
                axisPointer: {},
            
            formatter: function (params) {
                var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
                let rez = '';
                params.forEach(item => {
                    var diff_amount = 0;
                    var item_data=item.data==='-'?'-':formatNumber(item.data);
                    if(typeof fan_abs_diff[item.seriesIndex] !== 'undefined'){
                        diff_amount = fan_abs_diff[item.seriesIndex][item.dataIndex];
                    }
                    var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span><br></p>'
                    rez += xx;
                });
                return rez;
            }
        },
        grid: {
            top:    60,
            left:   '5%',
            right:  '10%',
            bottom:  '5%',
            containLabel: true
        },
        legend: {
            data:Legend_Data,   
            padding :0,
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

        xAxis: [{
            type: 'category',
            name : 'Day',
            boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },

            axisLabel: {
                formatter: function (value, index) {
                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var date = new Date(value);
                    var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
                    return xx;
                },
            },
            data: fanLabel
        }],

        yAxis: [{
            type: 'value',
            name: 'Fan Count',
            scale:true,
            position: 'left',
            axisLabel: {
                formatter: function (e) {
                    return formatNumber(e);
                }
            }
        }],
        series: fanseriesList
    };
    FanGrowthChart.setOption(option_growth, true), $(function() {
        function resize() {
            setTimeout(function() {
                FanGrowthChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });

    var arrayLength=brand_id_arr.length;
    fan_sr_of_brand = fan_sr_of_brand +1;
    if(fan_sr_of_brand<arrayLength )
    {
        FanGrowth(fday,sday,brand_id_arr[fan_sr_of_brand],fan_sr_of_brand,date_preset);
    }
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in FAN API" );
            });
}


function TagQty(fday,sday,page_name,sr_of_brand)
{
  
    //alert(fan_sr_of_brand);
    var TagQtyChart = echarts.init(document.getElementById('tag-qty-line-chart'));
    $('#tag-spin').show();
    var brand_id = GetURLParameter('pid');
    var tag_id = $( "#tag_filter" ).val();
    var tags = $('#tag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');
    $.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getTagQty')}}", // This is the URL to the API
      data: {admin_page:page_name,fday:fday,sday:sday,brand_id:brand_id,tag_id:tag_id,tags:tags}
    })
    .done(function( data ) {//alert(data.length);
        var temp_qty_count=[];
        var qty_count=[];
        var new_date_arr=[];
        var page_id='';
        var imgurl='';
        var tagmarkpointlist =[];
        var notFound_arr = [] ; 
        var tagcolor = [];
        

        for(var i in data) {
                temp_qty_count.push(data[i].qty_count);
                 new_date_arr.push(data[i].periodLabel);
                if(jQuery.inArray(data[i].periodLabel, tag_qty_Label)=='-1')
                {
                  tag_qty_Label.push(data[i].periodLabel);
                  if(sr_of_brand > 0)
                   {
                    notFound_arr.push(data[i].periodLabel);
                 //   console.log(fanseriesList);
                   }
                }
                page_id=  data[i].page_id; 
                imgurl = data[i].imgurl;
                tagcolor.push(data[i].color);
                var color = data[i].color;
            
        }
        // alert(color);
        var brandcolor = '';var datacolor = '';
        if(color == null || color == 'undefined'){
          brandcolor = colors;
          datacolor = colors[sr_of_brand];

        }
        else{
          brandcolor =tagcolor;
          datacolor = tagcolor[sr_of_brand];
        }
        // alert(tagcolor);
        tag_qty_Label.sort(function(a, b){
            var dateA=new Date(a), dateB=new Date(b)
            return dateA-dateB //sort by date ascending
        })
        for(var j in tag_qty_Label) {//alert(fanLabel[j]);
        var found =jQuery.inArray(tag_qty_Label[j], new_date_arr);
      //  alert(found + fanLabel[j]);
        if(found=='-1')
          qty_count.push('0');
        else
          qty_count.push(temp_qty_count[found]);
       }
      // console.log(tag_qty_seriesList);
        for(var NF in notFound_arr) {//alert(fanLabel[j]);
        var found =jQuery.inArray(notFound_arr[NF], tag_qty_Label);//if not found Label data in new date arr put it '0', it means that new date arr of page hasn't complete data for each day.
      //  alert(found + fanLabel[j]);
        if(found > -1)
        {
          // alert(found);
          for(var FD in brand_id_arr)
          {
           
            if(FD < brand_id_arr.length-1)
            {
              if(typeof(tag_qty_seriesList[FD]) !== 'undefined')
              tag_qty_seriesList[FD]['data'].splice(found, 0, 0);//arr.splice(2, 0, "Lene");
              //console.log(fanseriesList[FD]['data']);
            }
            
          }
    
        }
         
       }
        
        tagmarkpointlist.push({
            name : 'Have something', 
            value : qty_count[qty_count.length-1], 
            xAxis: tag_qty_Label[tag_qty_Label.length-1], 
            yAxis: qty_count[qty_count.length-1], 
            // symbol:'image://https://graph.facebook.com/'+page_id+'/picture?type=large', 
            symbol:'image://'+imgurl,
        })
        
        // alert(fanmarkpointlist.count);
        //alert(Legend_Data[fan_sr_of_brand]);
        tag_qty_seriesList.push({
            name:Legend_Data[sr_of_brand],
            type:'line',
            smooth: 0.2,
            color:datacolor,
            barMinHeight:2,
            data:qty_count,
            markPoint: {
                symbolSize:20,// 容器大小
                symbolOffset:[20,-5],//位置偏移
               data : tagmarkpointlist,
                label: {
                    offset: [0, 0],
                    color: '#ffffff',
                    formatter: [
                      ''
                    ].join('\n'),
                }
            },
        },);
        option= null;

        option = {
            color: brandcolor,
            tooltip: {
                trigger: 'axis',
                axisPointer: {},
            
            formatter: function (params) {
                var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+tagcolor+';"></span>';
                let rez = '';
                params.forEach(item => {
                    var diff_amount = 0;
                    var item_data=item.data==='-'?'-':formatNumber(item.data);
                    if(typeof qty_count[item.seriesIndex] !== 'undefined'){
                        diff_amount = qty_count[item.seriesIndex][item.dataIndex];
                    }
                    var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span><br></p>'
                    rez += xx;
                });
                return rez;
            }
        },
        grid: {
            top:    60,
            left:   '5%',
            right:  '10%',
            bottom:  '5%',
            containLabel: true
        },
        legend: {
            data:Legend_Data,   
            padding :0,
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

        xAxis: [{
            type: 'category',
            name : 'Day',
            boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },

            axisLabel: {
                formatter: function (value, index) {
                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var date = new Date(value);
                    var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
                    return xx;
                },
            },
            data: tag_qty_Label
        }],

        yAxis: [{
            type: 'value',
            name: 'Tag Count',
            scale:false,
            position: 'left',
            axisLabel: {
                formatter: function (e) {
                    return formatNumber(e);
                }
            }
        }],
        series: tag_qty_seriesList
    };
    TagQtyChart.setOption(option, true), $(function() {
        function resize() {
            setTimeout(function() {
                TagQtyChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });
    TagQtyChart.on('click', function (params) {
   if (params.event.handled !== true) {
   params.event.handled = true;
   // alert(params.name);
   // alert(params.name); // KBZPay
   // alert(params.seriesName); //positive
   // alert(params.value);//count 6.51
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   // var tag_id = $( "#tag_filter" ).val();
                 var tag_id = $('#tag_filter').map(function () {
                  return $(this).val();
              }).get().join('| ');
   // alert(tag_id);
   var default_page = $('.hidden-pg').val();
   window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+tag_id+"&fday="+ GetStartDate() +"&sday="+ GetEndDate() +"&admin_page="+ params.seriesName+"&period="+ params.name+"&default_page="+default_page , '_blank');
}
});
         // alert(sr_of_brand);
         // alert(brand_id_arr.length);
         var arrayLength=brand_id_arr.length;
         sr_of_brand = sr_of_brand +1;
         if(sr_of_brand<arrayLength )
         {
         
          TagQty(fday,sday,brand_id_arr[sr_of_brand],sr_of_brand);
          PostTagQty(fday,sday,brand_id_arr[sr_of_brand],sr_of_brand);
         

         }
         $('#tag-spin').hide();
        
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Tag Qty API" );
            });
}
function PostTagQty(fday,sday,page_name,sr_of_brand)
{
    var PostTagQtyChart = echarts.init(document.getElementById('post-tag-qty-line-chart'));
    $('#post_tag_spin').show();
    var brand_id = GetURLParameter('pid');
    var tag_id = $( "#ptag_filter" ).val();
    var tags = $('#ptag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');
    $.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPostTagQty')}}", // This is the URL to the API
      data: {admin_page:page_name, fday:fday, sday:sday, brand_id:brand_id, tag_id:tag_id,tags:tags}
    })
    .done(function( data ) {

        var temp_qty_count=[];
        var mention_total = 0;
        var ptagcolor = [];

        for(var i in data) {
             if(i == data.length-1)
          temp_qty_count.push(data[i].qty_count);
           if(jQuery.inArray(page_name, PostLabel)=='-1')
              PostLabel.push(page_name);

          ptagcolor = data[i].color;
        }

        var datacolor = ''; var brandcolor = '';

        if(ptagcolor == null || ptagcolor == 'undefined'){
          brandcolor = colors;
          datacolor = colors[sr_of_brand];

        }
        else{
          brandcolor = ptagcolor;
          datacolor = ptagcolor;
        }

      $.each(temp_qty_count,function(){mention_total+= parseInt(this) || 0;});
      arr_tag_label.push(Legend_Data[sr_of_brand]+' : '+ formatNumber(mention_total));
      arr_tag_label.push(mention_total);
      post_tag_qty_seriesList.push({
          value:mention_total,
          itemStyle:{color:datacolor},
          
        },
                );

      option = null;

      option = {
    color: brandcolor,

    tooltip: {
        trigger: 'axis',
       
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '5%',
    bottom:  '5%',
            containLabel: true
        },
     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            boundaryGap: true,
            axisLabel:{
                        rotate:30,
                        interval:0,
                      },
            axisTick: {
                alignWithLabel: true
            },
          
            data: PostLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Tag Count',
            scale:true,
            position: 'left',
          axisLabel: {
        formatter: function (e) {
            return formatNumber(e);
        }
    }
    
     }
    ],
     series: [{
        
            type:'bar',
            name:'count',
            barMaxWidth:30,
            barMinHeight:2,
            data:post_tag_qty_seriesList,
          
        }]
};
 $('#post_tag_spin').hide();
    PostTagQtyChart.setOption(option, true), $(function() {
        function resize() {
            setTimeout(function() {
                PostTagQtyChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });
    PostTagQtyChart.on('click', function (params) {
      // alert(params.name);
   if (params.event.handled !== true) {
   params.event.handled = true;
   // console.log(params);
   // alert(params.name); // KBZPay
   // alert(params.seriesName); //positive
   // alert(params.value);//count 6.51
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   //var tag_id = $( "#ptag_filter" ).val();
   var tag_id = $('#ptag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');
   var default_page = $('.hidden-pg').val();
   window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+tag_id+"&fday="+ GetStartDate() +"&sday="+ GetEndDate() +"&admin_page="+ params.name+"&default_page="+default_page , '_blank');
}
});
         // alert(sr_of_brand);
         // alert(brand_id_arr.length);
         var arrayLength=brand_id_arr.length;
         sr_of_brand = sr_of_brand +1;
         if(sr_of_brand<arrayLength )
         {
        
          PostTagQty(fday,sday,brand_id_arr[sr_of_brand],sr_of_brand);
         

         }
        
        
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Tag Qty API" );
            });
}

function PageLike(fday,sday,page_name,sr_of_brand,date_preset)
{
     var PageLikeChart = echarts.init(document.getElementById('page-like-chart'));
     

      $('#page-like-spin').show();
    var brand_id = GetURLParameter('pid');
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPageLike')}}", // This is the URL to the API
      data: { date_preset: date_preset,page_name:page_name,fday:fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//alert(data.length);

     //var like=[];
     var like_total=0;
     var Page_like =[];
     var Page_id='';
     var imgurl = '';
     var color = '';
       // var fanLabel=[];
  
 // console.log('pagediff');
          // console.log(data);
         for(var i in data) {//alert(data[i].mentions);
              if(i == data.length-1)
              Page_like.push(data[i].fan);

            if(jQuery.inArray(page_name, likeLabel)=='-1')
              likeLabel.push(page_name);

            Page_id = data[i].page_id;
            imgurl = data[i].imgurl;
            color = data[i].color;



      }
      // alert(color);
      var brandcolor = ''; var datacolor='';
      if(color == 'undefined' || color == null){
      
    brandcolor = colors;
      datacolor  = colors[sr_of_brand];

      }
      else{
        brandcolor = color;
        datacolor = color;
      }

      $.each(Page_like,function(){like_total+= parseInt(this) || 0;});
      arr_like_label.push(Legend_Data[sr_of_brand]+' : '+ formatNumber(like_total));
      arr_like_total.push(like_total);
      likeseriesList.push({
          value:like_total,
          itemStyle:{color:datacolor},
          
        },
                );
      likemarkpointlist.push(
{
  name : 'Have something', 
  value : like_total, 
  xAxis: Legend_Data[sr_of_brand], 
  yAxis: like_total, 
  symbol:'image://'+imgurl, 
  symbolSize:30,

}
        )
     // console.log(likeLabel);
     //  console.log(Page_like);
     //  console.log(likeseriesList);

        option= null;

option = {
    color: brandcolor,

    tooltip: {
        trigger: 'axis',
       
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '5%',
    bottom:  '5%',
            containLabel: true
        },
        //  legend: {
        // data:['Like'],
        // // formatter: '{name}: '+ formatNumber(arr_reach_total[sr_of_brand]),
   
        //  padding :0,
       
        
        // },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            boundaryGap: true,
            axisLabel:{
                        rotate:30,
                        interval:0,
                      },
            axisTick: {
                alignWithLabel: true
            },
          
            data: likeLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Like Count',
            scale:true,
            // max: 250,
            position: 'left',
            // axisLine: {
            //     lineStyle: {
            //         color: colors[0]
            //     }
            // }
                    axisLabel: {
        formatter: function (e) {
            return formatNumber(e);
        }
    }
    
     }
    ],
     series: [{
        
            type:'bar',
            name:'Like',
            barMaxWidth:30,
            barMinHeight:2,
            data:likeseriesList,
             markPoint: {
               // symbol:'roundRect',
               // symbol:'image://https://graph.facebook.com/393033680732657/picture?type=large',

                symbolSize:[50, 50],// 容器大小
                symbolOffset:[0,-20],//位置偏移
               data : likemarkpointlist,
                label: {
                    offset: [0, 0],
                    color: '#ffffff',
                    formatter: [
                      ''
                    ].join('\n'),
                }
            },
          
        }]
};
$("#page-like-spin").hide();

 PageLikeChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            PageLikeChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

     var arrayLength=brand_id_arr.length;
     sr_of_brand = sr_of_brand +1;
     if(sr_of_brand<arrayLength )
     {
      PageLike(fday,sday,brand_id_arr[sr_of_brand],sr_of_brand,date_preset);
     }
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              //console.log( "error occured in FAN API" );
            });
}

  var pieChartColor=[];

        function socialdetail(fday,sday,brand_id,social_sr_of_brand)
    {//alert(keyword);
     /* var socialchart = document.getElementById('social-chart');
      var socialChart = echarts.init(socialchart);*/

      // $("#social-spin").show();
      
      //$("#fan-growth-spin").show();
      var ReactPieChart = echarts.init(document.getElementById('reaction-pie-chart'));
      $("#reaction-spin").show();
      $("#reaction-pie-spin").show();
      
     
        //var brand_id = 22;
       /* alert (brand_id);
       alert(periodType);*/
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getinboundReaction')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id,periodType:'month'}
    })
    .done(function(combine_data) {//alert(data);alert("hhi");
    //var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_mention_total=[];
       var color1 =[];
       var data =combine_data[0];
       var data_comment =combine_data[1];
       pieChartColor.push(combine_data[2]);
       var color = combine_data[2];
       color1 = combine_data[2];

       // alert(color1)

       var total_comment = 0;
       var RSC_total = 0 ;
       var socials = [];
       var social_total=0; var like_total=0; var love_total=0; var wow_total=0; var haha_total=0; 
       var sad_total=0; var angry_total=0; 
       var share_total=0;var post_total=0;

       if(data_comment.length > 0)
       {
        total_comment= data_comment[0]['total_comment'];
       }
       

       for(var i in data) 
       {
   
        var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].shared)+parseInt(data[i].post_count)//add share
         socials.push(Math.round(mediaReach));
         if(jQuery.inArray(data[i].periodLabel, socialLabel)=='-1')
         socialLabel.push(data[i].periodLabel);
         like_total+=parseInt(data[i].Like);love_total+=parseInt(data[i].Love);wow_total+=parseInt(data[i].Wow);
         haha_total+=parseInt(data[i].Haha);sad_total+=parseInt(data[i].Sad);angry_total+=parseInt(data[i].Angry);
         share_total+=parseInt(data[i].shared);
         //console.log(data[i].post_count);
         post_total+=parseInt(data[i].post_count);
        

       }

       // alert(pieChartColor.split(','));
       // var  array = JSON.parse("[" + pieChartColor + "]");
       // alert(array);
       var brandcolor = '';
       if(color == null || color == 'undefined' || color == ''){
      
        brandcolor = colors;

       }
       else{
        brandcolor = pieChartColor;
       }



    appendtoReactiontable(Legend_Data[social_sr_of_brand],like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,social_sr_of_brand,post_total,color1);
      RSC_total = like_total +love_total+wow_total+haha_total+sad_total+angry_total
                  +share_total+total_comment;
     RSCseriesList.push({
        name : Legend_Data[social_sr_of_brand],
        value : RSC_total
    }
        );
     // alert(Legend_Data);
     console.log(RSCseriesList);

     option_pie= null;
      option_pie = {
    color: brandcolor,
 tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
        orient: 'horizontal',
        x: 'left',
        data:Legend_Data
    },

        series: [
        {
            name:'Reactions, Comments and Shares',
            type:'pie',
            radius: ['40%', '50%'],
            avoidLabelOverlap: false,
            label: {
                formatter: function(params) {//console.log(params.name);
                        //console.log(JSON.stringify(params));
                        var name = params.name.substr(0, params.name.indexOf(' '));
                        var percent = params.percent;
                        return params.name + '\n' + percent + '%';
                    }
                
            },
            labelLine: {
                normal: {
                    show: true
                }
            },
            data:RSCseriesList,

        }
    ]
};
$('#reaction-pie-spin').hide();
 ReactPieChart.setOption(option_pie, true), $(function() {
    function resize() {
        setTimeout(function() {
            ReactPieChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
  ReactPieChart.on('click', function (params) {
    // alert('hello');
  if (params.event.handled !== true) {
   params.event.handled = true;
   // console.log("tags");
   // console.log(params);
   // console.log(params.name); // xaxis data = tag name
   // console.log(params.seriesName); //bar type name ="positive"
   // console.log(params.value);//count 8
   // alert(params.value);// count of pie
   // alert(params.seriesName); //Mention
   // alert(params.name); // Page Name (KBZPay)
   // return;

   var default_pg = $('.hidden-pg').val();

  
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&fday="+ GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+ params.name , '_blank');
}
});

     var arrayLength=brand_id_arr.length;
     social_sr_of_brand = social_sr_of_brand +1;
     if(social_sr_of_brand<arrayLength )
     {
      socialdetail(fday,sday,brand_id_arr[social_sr_of_brand],social_sr_of_brand);
      // postReaction(fday,sday,brand_id_arr[social_sr_of_brand],social_sr_of_brand);
     }
       $("#reaction-spin").hide();
       

//       

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
   postReactColor = [];
  function postReaction(fday,sday,brand_id,social_sr_of_brand)
    {
      var PostReactionPieChart = echarts.init(document.getElementById('post-reaction-pie-chart'));
      // $("#reaction-spin").show();
      // PostReactionPieChart.clear();
      var tags = $('#ptag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');
      var tag_id = $( "#ptag_filter" ).val();
      // alert(tag_id);
      $("#post_tag_spin").show();
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
        url: "{{route('getinboundpostReaction')}}", // This is the URL to the API
        data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id,periodType:'month',tag_id:tag_id,tags:tags}
        })
        .done(function(combine_data) {
          // alert("hello");
          // console.log(combine_data);
       var data =combine_data[0];
       postReactColor.push(combine_data[1]);
       var color = combine_data[1];
       // var total_comment = 0;
       var RSC_total = 0 ;
       var socials = [];
       var social_total=0; var like_total=0; var love_total=0; var wow_total=0; var haha_total=0; 
       var sad_total=0; var angry_total=0; 
       var share_total=0;var post_total=0;

       // if(data_comment.length > 0)
       // {
       //  total_comment= data_comment[0]['total_comment'];
       // }
              var brandcolor = '';
       if(color == null || color == 'undefined' || color == ''){
      
        brandcolor = colors;

       }
       else{
        brandcolor = postReactColor;
       }

       for(var i in data) 
       {
   
        // var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].shared)+parseInt(data[i].post_count)//add share
         // socials.push(Math.round(mediaReach));
         // if(jQuery.inArray(data[i].periodLabel, socialLabel)=='-1')
         // socialLabel.push(data[i].periodLabel);
         like_total+=parseInt(data[i].Like);love_total+=parseInt(data[i].Love);wow_total+=parseInt(data[i].Wow);
         haha_total+=parseInt(data[i].Haha);sad_total+=parseInt(data[i].Sad);angry_total+=parseInt(data[i].Angry);
         // share_total+=parseInt(data[i].shared);
         //console.log(data[i].post_count);
         // post_total+=parseInt(data[i].post_count);

       }

      // alert(RSC_total);
    // appendtoReactiontable(Legend_Data[social_sr_of_brand],like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,social_sr_of_brand,post_total);
      RSC_total = like_total +love_total+wow_total+haha_total+sad_total+angry_total;
      // alert(RSC_total);
      // RseriesList =[];
     
     RseriesList.push({
        name : Legend_Data[social_sr_of_brand],
        value : RSC_total
    }
        );
     console.log("LIST"+RSCseriesList);

     option_reactionpie= null;
      option_reactionpie = {
    color: brandcolor,
 tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
        orient: 'horizontal',
        x: 'left',
        data:Legend_Data
    },

        series: [
        {
            name:'Tag Post Reactions',
            type:'pie',
            radius: ['40%', '50%'],
            avoidLabelOverlap: false,
            label: {
                formatter: function(params) {//console.log(params.name);
                        //console.log(JSON.stringify(params));
                        var name = params.name.substr(0, params.name.indexOf(' '));
                        var percent = params.percent;
                        return params.name + '\n' + percent + '%';
                    }
                
            },
            labelLine: {
                normal: {
                    show: true
                }
            },
            data:RseriesList,

        }
    ]
};
$('#post_tag_spin').hide();
 PostReactionPieChart.setOption(option_reactionpie, true), $(function() {
    function resize() {
        setTimeout(function() {
            PostReactionPieChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
  PostReactionPieChart.on('click', function (params) {
    // alert('hello');
  if (params.event.handled !== true) {
   params.event.handled = true;
   // console.log("tags");
   // console.log(params);
   //var tag_id = $( "#ptag_filter" ).val();
   var tag_id = $('#ptag_filter option:selected').map(function () {
                  return $(this).val();
              }).get().join('| ');
   // console.log(params.name); // xaxis data = tag name
   // alert(tag_id); //bar type name ="positive"
   // console.log(params.value);//count 8
   // alert(params.value);// count of pie
   // alert(params.seriesName); //Mention
   // alert(params.name); // Page Name (KBZPay)
   // return;

   var default_pg = $('.hidden-pg').val();

  
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&fday="+GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ params.name+"&tag_id="+tag_id , '_blank');
}
});

     var arrayLength=brand_id_arr.length;
     social_sr_of_brand = social_sr_of_brand +1;
     if(social_sr_of_brand<arrayLength )
     {
      // socialdetail(fday,sday,brand_id_arr[social_sr_of_brand],social_sr_of_brand);
      postReaction(fday,sday,brand_id_arr[social_sr_of_brand],social_sr_of_brand);
     }
       $("#post_reaction_spin").hide();
       

//       

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}

  function TopPosts(fday,sday,brand_id,sr_of_brand)
    {
      
      $("#top-post-spin").show();
     
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getTopAndLatestPost')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id,limit:'3',
              format_type:'top_post'}
    })
    .done(function(data) {//console.log(data);
   // console.log("top post"+JSON.stringify(data));
     // var html= "<tr><td width='10%'><div class='postPanel_contentContainer' style='padding:0'><img style='width:50px;height:50px' src='https://graph.facebook.com/"+data[0]['page_id']+"/picture?type=large'></div></td>";
     var html= "<tr><td width='10%'><div class='postPanel_contentContainer' style='padding:0'><img style='width:50px;height:50px' src='"+data[0]['imgurl']+"'></div></td>";
     for(var i in data)
     {
      html += "<td width='30%' align='left'><div class='postPanel_contentContainer' style='padding:0'><p>"+readmore(data[i]['message'])+" <a href='http://www.facebook.com/"+data[i]['id']+ "' target='_blank' class='btn waves-effect waves-light btn-sm  popup_post'>...View More</a></p>";
      if(data[i]['post_type']=='photo')
      {
      html +='<div><a target="_blank" class="postPanel_imageAndNameTextContainer cl cf" href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<img class="postPanel_previewImage" style="width:100%;height:150px" src="'+data[i]['full_picture']+'">'+
                  '</span><span class="postPanel_nameTextContainer"></span></a></div>';
      
      }
      else if(data[i]['post_type']=='video')
      {
      html +='<a target="_blank" class="postPanel_imageAndNameTextContainer cl cf"  href="http://www.facebook.com/'+data[i]['id']+'">'+
                  '<span class="postPanel_imageContainer" id="idb4d"><span class="postPanel_previewImageBackground" style=""></span>'+
                  '<iframe src="https://www.facebook.com/plugins/video.php?href='+data[i]['link']+'&width=500&show_text=false&height=281&appId" frameborder="0" allowfullscreen></iframe>'+
                  '</span><span class="postPanel_nameTextContainer"></span></a>';
      }
       html += '</div>';
       html += '<div  class="postPanel_dataContainer" style="justify-content:flex-start;margin-top:10px;background-color:#fff" id="idb4e">'+
               '<span class="postPanel_actionIconContainer" id="idb52"><span class="postPanel_icon " title="Total reactions"></span><span>Total reactions : '+numberWithCommas(data[i]['total_react'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb5d"><span class="postPanel_icon mdi mdi-comment-outline" title="Comments"></span><span>'+numberWithCommas(data[i]['comment_count'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb5e"><span class="postPanel_icon mdi mdi-share-variant" title="Shares"></span><span>'+numberWithCommas(data[i]['shared'])+'</span></span>'+
                 
                  '</div>';
       html += '<div  class="postPanel_dataContainer" style="justify-content:flex-start" id="idb4e">'+
               '<span class="postPanel_actionIconContainer" id="idb52"><span class="postPanel_icon mdi mdi-thumb-up-outline" title="Likes"></span><span>'+numberWithCommas(data[i]['liked'])+'</span></span>'+
                  '<span id="idb53" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb54"><span class="postPanel_icon  mdi mdi-heart-outline" title="Love"></span><span>'+numberWithCommas(data[i]['loved'])+'</span></span>'+
                  '<span id="idb55" style="display:none"></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb56"><span class="postPanel_icon icon-post-haha" title="Haha" >😆</span><span>'+numberWithCommas(data[i]['haha'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb57"><span class="postPanel_icon icon-post-wow" title="Wow">😮</span><span>'+numberWithCommas(data[i]['wow'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb58"><span class="postPanel_icon icon-post-sad" title="Sad">😪</span><span>'+numberWithCommas(data[i]['sad'])+'</span></span>'+
                  '<span class="postPanel_actionIconContainer" id="idb59"><span class="postPanel_icon icon-post-angry" title="Anger">😡</span><span>'+numberWithCommas(data[i]['anger'])+'</span></span>'+
                                
                  '</div>';
      html +="</td>";
     }
       
      html +="</tr>";
     $("#tbl_top_post tbody").append(html);

     var arrayLength=brand_id_arr.length;
     sr_of_brand = sr_of_brand +1;
     if(sr_of_brand<arrayLength )
     {
      TopPosts(fday,sday,brand_id_arr[sr_of_brand],sr_of_brand);
     }
       $("#top-post-spin").hide();
       

//       

 })
.fail(function() {
     
 });
}
 function FrequentTag(fday,sday,brand_id,sr_of_brand)
    {
      
      $("#frequent-tag-spin").show();
     
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id,limit:10}
    })
    .done(function(data) {
   
     // var html= "<tr><td width='10%'><img style='width:50px;height:50px' src='https://graph.facebook.com/"+data[0]['page_id']+"/picture?type=large'></td><td width='90%' align='left'>";
     if(data.length === 0) var html = '';
    else 
      var html= "<tr><td width='10%'><img style='width:50px;height:50px' src='"+data[0]['imgurl']+"'></td><td width='90%' align='left'>";
     for(var i in data)
     {
           var tagLabel=data[i].tagLabel;
           var tagId=data[i].tagId;
           var tagCount=data[i].tagCount;
     
      html +='<button type="button" style="margin:0.2rem" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_frequent_tag" name="'+brand_id+'" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>';
      
     }
       
      html +="</td></tr>";
     $("#tbl_frequent_tag tbody").append(html);

     var arrayLength=brand_id_arr.length;
     sr_of_brand = sr_of_brand +1;
     if(sr_of_brand<arrayLength )
     {
      FrequentTag(fday,sday,brand_id_arr[sr_of_brand],sr_of_brand);
     }
      
       
 $("#frequent-tag-spin").hide(); 
//       

 })
.fail(function() {
     
 });

}
  // $(document).on('click', '.btn_frequent_tag', function() {
  //    var TagName = $(this).attr('value');
  //    var page_name = $(this).attr('name');
  //    var default_page = $('.hidden-pg').val();
  //    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ page_name+"&default_page="+default_page , '_blank');
  // })

  $(document).on('click', '.btn_frequent_tag', function() {
     var TagName = $(this).attr('value');
     var page_name = $(this).attr('name');
     var default_page = $('.hidden-pg').val();
     window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ page_name+"&default_page="+default_page , '_blank');
  })
    $(document).on('click', '.btn_frequent_tag_post', function() {
     var TagName = $(this).attr('value');
     var page_name = $(this).attr('name');
     var default_page = $('.hidden-pg').val();
     window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+TagName+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&admin_page="+ page_name+"&default_page="+default_page , '_blank');
  })


   function PostFrequentTag(fday,sday,brand_id,sr_of_brand)
    {
      
      $("#post-frequent-tag-spin").show();
     
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),admin_page:brand_id,limit:10,type:'inbound_post'}
    })
    .done(function(data) {//console.log(data);
      // console.log('gettagcount'+JSON.stringify(data));
   // console.log('imgurl is'+data[0][imgurl]);
     // var html= "<tr><td width='10%'><img style='width:50px;height:50px' src='https://graph.facebook.com/"+data[0]['page_id']+"/picture?type=large'></td><td width='90%' align='left'>";
     if(data.length === 0) var html = '';
    else  html= "<tr><td width='10%'><img style='width:50px;height:50px' src='"+data[0]['imgurl']+"'></td><td width='90%' align='left'>";
       // var html= "<tr><td width='10%'><img style='width:50px;height:50px' src='"+data[0]['imgurl']+"'></td><td width='90%' align='left'>";
     for(var i in data)
     {
           var tagLabel=data[i].tagLabel;
           var tagId=data[i].tagId;
           var tagCount=data[i].tagCount;
     
      html +='<button type="button" style="margin:0.2rem" class="btn waves-effect waves-light btn-rounded btn-sm btn-info btn_frequent_tag_post" name="'+brand_id+'" value="'+tagId+'">'+tagLabel+'<span class="label label-light-info" style="padding:1px 5px;margin-left:5px">'+tagCount+'</span></button>';
      
     }
       
      html +="</td></tr>";
     $("#tbl_post_frequent_tag tbody").append(html);

     var arrayLength=brand_id_arr.length;
     sr_of_brand = sr_of_brand +1;
     if(sr_of_brand<arrayLength )
     {
      PostFrequentTag(fday,sday,brand_id_arr[sr_of_brand],sr_of_brand);
     }
       $("#post-frequent-tag-spin").hide();
       

//       

 })
.fail(function() {
     
 });
}




//     function TotalReach(fday,sday,brand_id,sr_of_brand,date_preset)
// { 
      
//       var reachchart = document.getElementById('reach-chart');
//       var reachChart = echarts.init(reachchart);

//   $( "#reach-spin" ).show();


//     $.ajax({
//       type: "GET",
//       dataType:'json',
//       contentType: "application/json",
//       url: "{{route('getFbReach')}}", // This is the URL to the API
//      data: { date_preset: date_preset,period:'day',page_name:brand_id,fday:fday,sday:sday}
//     })
//     .done(function( data ) {
    
//         // alert(Object.keys(data).length);
        
//        var total_reach=[];
//        var reach_total=0;
 
//         for(var i in data) 
//         {
//         //alert(data[i].mentions); 
//           var total=parseInt(data[i].organic)+parseInt(data[i].paid);
//           if(total === 0)
//           {
//              total_reach.push('-');
//           }
//           else
//           {
//             total_reach.push(total);
//           }
          
//           if(jQuery.inArray(data[i].end_time, reachLabel)=='-1')
//           reachLabel.push(data[i].end_time);
          
//         }
// // console.log("reachLabel"); console.log(total_reach);
// $.each(total_reach,function(){reach_total+= parseInt(this) || 0;});

// /*var valueToPush = { };
// valueToPush[Legend_Data[sr_of_brand]]=mention_total;*/
// //alert(Legend_Data);
// //alert(Legend_Data[sr_of_brand]);
// arr_reach_label.push(Legend_Data[sr_of_brand]+' : '+ formatNumber(reach_total));
// //alert(arr_reach_label);
// // var result = [];
// // Legend_Data.forEach(function(key) {
// //     var found = false;
// //     arr_reach_label = arr_reach_label.filter(function(item) {
// //         var res=item.split(" :");
// //         // alert(res[0]);
// //         // alert(key);
// //         // alert(found);
// //         if(!found && String(res[0]) == String(key)) {//alert("hi");
// //             result.push(item);
// //             found = true;
// //             return false;
// //         } else 
// //             return true;
// //     })
// // })
// // arr_reach_label=[];
// // arr_reach_label=result;
// //  alert("arr "+arr_reach_label);

// arr_reach_total.push(reach_total);

 

// reachseriesList.push({
//               xAxes: [{ 
//                   ticks: {
//                   fontColor: "#f5f2f2", // this here
//                 },
//             }],

//             name:Legend_Data[sr_of_brand],
//             type:'line',
//             barMaxWidth:30,
//             smooth: 0.2,
//              barMinHeight:2,
//             data:total_reach,
//             color:colors[sr_of_brand],

//             // markPoint : {
//             //     data : [
//             //     {type : 'max', name: 'maximum'},
//             //     {type : 'min', name: 'minimum'},

//             //     ]
//             // }
//         },

//                            );//end-push

// /*console.log(arr_mention_total);
// console.log(Legend_Data);*/

// option = {
//         color:colors,
//          tooltip: {
//           trigger: 'axis',
//           axisPointer: {
//             type: 'cross'
//           },
//           formatter: function (params) {
//             var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
//             let rez = '<p>' + params[0].name + '</p>';
//        /* console.log(rez);*/ //quite useful for debug
//        params.forEach(item => {
//             //console.log(item);
//             //console.log(item.data);
//             var seriesname =item.seriesName.split(":");
//             var xx = '<p>'   + colorSpan(item.color) + ' ' + seriesname[0] + ': ' + kFormatter(item.data)  + '</p>'
//             rez += xx;
//           });

//        return rez;
//      }
//    },
      
//         legend: {
//         data:Legend_Data,
//         // formatter: '{name}: '+ formatNumber(arr_reach_total[sr_of_brand]),
   
//          padding :0,
       
        
//         },
//         toolbox: {
//             show : true,
//             feature : {
//                 mark : {show: false},
//                 dataView : {show: false, readOnly: false},
//                 magicType : {show: true, type: ['line','bar']},
//                 restore : {show: true},
//                 saveAsImage : {show: true}
//             }
//         },
//         calculable : true,
      
//         xAxis: [{
//     type: 'category', 
//             axisLabel: {
//       formatter: function (value, index) {
//      var date = new Date(value);
//        //console.log(date);
//        return date.getDate();

// },

//     },
// //     axisLabel: {
// //       formatter: function (value, index) {
// //     // Formatted to be month/day; display year only in the first label
// //     const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
// //   "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
// //      var arr = value.split(' - ');
// //     var date = new Date(arr[0]);

// // if(periodType === 'day')
// // {
// //     var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
// // }
// // else 
// // {

// //       var texts = [date.getFullYear(), monthNames[date.getMonth()]];
// // }



// //     return texts.join('-');

// // }
// //     },
//          data : reachLabel,
// }],
        
//         yAxis : [
//         {
//             type : 'value',
//                        axisLabel: {
//         formatter: function (e) {
//             return kFormatter(e);
//         }
//     }
//         }
//         ],
//         series : reachseriesList
        
//     ,

// };
 
//     $( "#reach-spin" ).hide();
//     reachChart.setOption(option, true), $(function() {
//     function resize() {
//         setTimeout(function() {
//             reachChart.resize()
//         }, 100)
//     }
//     $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
// });
        
   
//          })
//             .fail(function() {
//               // If there is no communication between the server, show an error
//              console.log( "error occured in mentions" );
//             });

//         }
function GetStartDate()
  {
     //alert(startDate);
   return startDate.format('YYYY-MM-DD');
  }
  function GetEndDate()
  {
             // alert(endDate);
  return endDate.format('YYYY-MM-DD');
  }
 var permission = GetURLParameter('accountPermission');

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,

        },function(start, end,label) {//alert(label);
        var startDate;
        var endDate;
        var labelDate;
        startDate = start;
        endDate = end;
        labelDate = label;
        reachseriesList=[];reachLabel = [];arr_reach_total=[];arr_reach_label=[];
        fanseriesList=[]; fanLabel = []; arr_fan_total=[]; arr_fan_label=[]; fan_abs_diff=[];
       
        likeseriesList=[];likeLabel = [];arr_like_total=[];arr_like_label=[];likemarkpointlist=[];arr_tag_label=[];
        post=[]; share=[]; comment=[]; summaryLabel=[];
        tag_pos=[];tag_neg=[];tag_neutral=[];tagsentiLabel=[];pos_percent_tag=[];neg_percent_tag=[];
        neutral_percent_tag=[];tag_qty_list=[];tagmarkpointlist=[];tagpercentmarkpointlist=[];
        tag_qty_Label = [];tag_qty_seriesList=[];post_tag_qty_seriesList=[];

        arr_ov_positive=[]; arr_ov_negative=[]; arr_ov_neutral=[]; OverallLabel=[];arr_ov_markpointlist=[];arr_ovQty_markpointlist=[];
        arr_ov_qty_positive=[];arr_ov_qty_negative=[];arr_ov_qty_neutral=[];
        arr_cmt_qty_positive=[];arr_cmt_qty_negative=[];arr_cmt_qty_neutral=[];
        arr_cmt_positive=[]; arr_cmt_negative=[]; arr_cmt_neutral=[]; CommentLabel=[];PostLabel=[];
        arr_cmt_markpointlist=[];arr_cmtQty_markpointlist=[];
        Social_seriesList=[]; socialLabel = [];arr_social_total=[];
        Senti_data1=[];Senti_data2=[];Senti_xAxisData=[];
        RSCseriesList=[];RseriesList=[];
  
       
                    if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) 
          
            {
                 swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }


            else{
               Clear_reaction_table();
        Clear_top_post_table();
        Clear_frequent_tag_table();
        Clear_post_frequent_tag_table();
        //Clear_page_summary_table();
        //Clear_page_ranking_table();
        compare_count=0;

        refreshGraph(startDate,endDate,labelDate);
             }
           }
           else
           {
              Clear_reaction_table();
        Clear_top_post_table();
        Clear_frequent_tag_table();
        Clear_post_frequent_tag_table();
        //Clear_page_summary_table();
        //Clear_page_ranking_table();
        compare_count=0;

        refreshGraph(startDate,endDate,labelDate);
           }
      });


function refreshGraph(startDate,endDate,labelDate)
{//alert(brand_id_arr);
  // alert(brand_id_arr);
  // var arrayLength=brand_id_arr.length;
  // if(arrayLength >1)
  // {
  // for (var i = 0; i < arrayLength; i++) {
  //   compare_count=i;
  //   // alert(Legend_Data[compare_count]);
  //   ChooseDate(startDate,endDate,brand_id_arr[i],compare_count,labelDate);
  // }
    
  // }
  // else
  // {
    // alert(brand_id_arr[0]);
    // alert(brand_id_arr[0]);
    ChooseDate(startDate,endDate,brand_id_arr[0],0,labelDate);
  //}

}

$(document).on('change', '#tag_filter', function () {
      tag_pos=[];tag_neg=[];tag_neutral=[];tagsentiLabel=[];pos_percent_tag=[];neg_percent_tag=[];
        neutral_percent_tag=[];tag_qty_list=[];tagmarkpointlist=[];tagpercentmarkpointlist=[];
      tag_qty_Label = [];tag_qty_seriesList=[];
      tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
      TagQty(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
    
});

$(document).on('change', '#ptag_filter', function () {
     post_tag_qty_seriesList=[];RseriesList=[];postReactColor=[];
      // alert(brand_id_arr);
      PostTagQty(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
      postReaction(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
});




   

$("#btn_add_project").click(function(){

var arrayLength=brand_id_arr.length;

    for (var i = 0; i < arrayLength; i++) {btn_add_project
           var brand_id= brand_id_arr[i];
    //Do something
    // $('#'+brand_id).prop('disabled', true);
     $('#brand_id').children('option[value="'+brand_id+'"]').css('display','none');
}   
   /* $("select#brand_id").val(''); */
   $("select#brand_id").prop('selectedIndex', 0);
   $('#add-project').modal('show'); 

  
});

$("#btn_remove_project").click(function(){
    Clear_reaction_table();
    Clear_top_post_table();
    Clear_frequent_tag_table();
    Clear_post_frequent_tag_table();

     compare_count=0;
     Legend_Data=[]; brand_id_arr=[]; reachseriesList=[]; reachLabel = []; arr_reach_total=[]; arr_reach_label=[];
     fanseriesList=[]; fanLabel = []; arr_fan_total=[]; arr_fan_label=[]; fan_abs_diff=[];// var min_fan_array=0;
     likeseriesList=[]; likeLabel = []; arr_like_total=[]; arr_like_label=[]; likemarkpointlist=[];arr_tag_label=[];
      post=[]; share=[]; comment=[]; summaryLabel=[];
      tag_pos=[]; tag_neg=[]; tag_neutral=[]; tagsentiLabel=[]; pos_percent_tag=[]; neg_percent_tag=[]; tag_qty_list=[]; neutral_percent_tag=[];
     tagmarkpointlist=[] ; tagpercentmarkpointlist=[];
     tag_qty_Label = []; tag_qty_seriesList=[];post_tag_qty_seriesList=[];
      arr_ov_positive=[]; arr_ov_negative=[]; arr_ov_neutral=[]; OverallLabel=[]; arr_ov_markpointlist=[]; arr_ovQty_markpointlist=[];
      arr_ov_qty_positive=[]; arr_ov_qty_negative=[]; arr_ov_qty_neutral=[]
      arr_cmt_positive=[]; arr_cmt_negative=[]; arr_cmt_neutral=[]; CommentLabel=[];PostLabel=[];
     arr_cmt_markpointlist=[]; arr_cmtQty_markpointlist=[];
      arr_cmt_qty_positive=[]; arr_cmt_qty_negative=[]; arr_cmt_qty_neutral=[];
     Social_seriesList=[]; socialLabel = []; arr_social_total=[];
     Senti_xAxisData=[]; Senti_data1=[]; Senti_data2=[];
     RSCseriesList=[];RseriesList=[];postReactColor=[];pieChartColor=[];ptagcolor=[];
     $("#ptag_filter").empty().trigger('change');
     $("#tag_filter").empty().trigger('change');

    compare_count=0;

    var FanGrowthChart = echarts.init(document.getElementById('fan-growth-chart'));
    FanGrowthChart.clear();

    var ReactPieChart = echarts.init(document.getElementById('reaction-pie-chart'));
    ReactPieChart.clear();

    var TagQtyChart = echarts.init(document.getElementById('tag-qty-line-chart'));
    TagQtyChart.clear();

     var PostTagQtyChart = echarts.init(document.getElementById('post-tag-qty-line-chart'));
    PostTagQtyChart.clear();

      var PostReactionChart = echarts.init(document.getElementById('post-reaction-pie-chart'));
    PostReactionChart.clear();

    var PageLikeChart = echarts.init(document.getElementById('page-like-chart'));
    PageLikeChart.clear();

    var SummaryChart = echarts.init(document.getElementById('page-summary-chart'));
    SummaryChart.clear();

    var tagsentimentchart = echarts.getInstanceByDom(document.getElementById("tag-sentiment-chart"));
    tagsentimentchart.clear();

    var tagpercentchart = echarts.getInstanceByDom(document.getElementById("tag-percent-chart"));
    tagpercentchart.clear();

    var PostSentiChart = echarts.init(document.getElementById('post-sentiment-chart'));
    PostSentiChart.clear();

    var PostSentiChartQty = echarts.init(document.getElementById('post-sentiment-qty-chart'));
    PostSentiChartQty.clear();

    var CommentSentiChart = echarts.init(document.getElementById('comment-sentiment-chart'));
    CommentSentiChart.clear();

      var CommentSentiChartQty = echarts.init(document.getElementById('comment-sentiment-qty-chart'));
      CommentSentiChartQty.clear();

    Rebind_Pages();


});


$("#btn_add_brand").click(function(){
  // alert('hey');
   $('#add-project').modal('hide'); 
  var selected_text=$( "#brand_id option:selected" ).text();
  var selected_value=$( "#brand_id option:selected" ).val();


  if(selected_value !== '')
  {

      if(brand_id_arr.length <=0){

        brand_id_arr.push(selected_value);
        Legend_Data.push(selected_value);
        ChooseDate(startDate,endDate,selected_value,0,labelDate);
      }
      else{
        compare_count=compare_count+1;
        Legend_Data.push(selected_value);
        // alert(Legend_Data);
        brand_id_arr.push(selected_value);
        ChooseDate(startDate,endDate,selected_value,compare_count,labelDate);
      }
  }

  //add to legend array

});



function appendtoReactiontable(brand_name,like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,sr_no,total_mention,color)
{
    // total_mention=0;
//   $("#tr_header").append("<th style='color:"+colors[sr_no]+"'><div>"+brand_name+"</div><div>Post: "+total_mention+"</div></th>");
//   $("#tr_like").append("<td>"+kFormatter(like_total)+"</td>");
//   $("#tr_love").append("<td>"+kFormatter(love_total)+"</td>");
//   $("#tr_wow").append("<td>"+kFormatter(wow_total)+"</td>");
//   $("#tr_haha").append("<td>"+kFormatter(haha_total)+"</td>");
//   $("#tr_sad").append("<td>"+kFormatter(sad_total)+"</td>");
//   $("#tr_angry").append("<td>"+kFormatter(angry_total)+"</td>");
if(color == null) color = colors[sr_no];
var html= "<tr>"+
         "<td style='color:"+color+"'>"+brand_name+" ["+total_mention+" posts] </td>"+
         "<td align='right'>"+nFormatter(like_total,1)+"</td>"+
         "<td align='right'>"+nFormatter(love_total,1)+"</td>"+
         "<td align='right'>"+nFormatter(wow_total,1)+"</td>"+
         "<td align='right'>"+nFormatter(haha_total,1)+"</td>"+
         "<td align='right'>"+nFormatter(sad_total,1)+"</td>"+
         "<td align='right'>"+nFormatter(angry_total,1)+"</td>"+
        "</tr>";
 $("#tbl_reaction tbody").append(html);

 
}


function Clear_top_post_table()
{
 $("#div_top_post").empty();
 $("#div_top_post").append(" <table id='tbl_top_post'  class='table table-bordered' style='table-layout: fixed;' >"+
                           "<thead>"+
                           "<tr id='tr_post_header'>"+
                           "<th width='10%'>Ranks</th>"+
                           "<th width='30%' style='text-align: center;'>1st</th>"+
                           "<th width='30%' style='text-align: center;'>2nd</th>"+
                           "<th width='30%' style='text-align: center;'>3rd</th>"+
                           "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                            "</tbody>"+
                              "</table>");

}
function Clear_frequent_tag_table()
{
 $("#div_frequent_tag").empty();
 $("#div_frequent_tag").append(" <table id='tbl_frequent_tag'  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_frequenttag_header'>"+
                           "<th width='10%'></th>"+
                           "<th width='90%' style='text-align: center;'>Topics</th>"+
                           "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                            "</tbody>"+
                              "</table>");
}
function Clear_post_frequent_tag_table()
{
  // alert('hey');
 $("#div_post_frequent_tag").empty();
 $("#div_post_frequent_tag").append(" <table id='tbl_post_frequent_tag'  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_post_frequenttag_header'>"+
                           "<th width='10%'></th>"+
                           "<th width='90%' style='text-align: center;'>Topics</th>"+
                           "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                            "</tbody>"+
                              "</table>");
}

function Clear_reaction_table()
{
 $("#div_reaction").empty();
 $("#div_reaction").append(" <table id='tbl_reaction'  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_header'>"+
                           "<th>Reaction</th>"+
                           "<th style='text-align: right;'>👍 Like</th>"+
                           "<th style='text-align: right;'>😍 Love</th>"+
                           "<th style='text-align: right;'>😯 WOW</th>"+
                           "<th style='text-align: right;'>😆 HA HA</th>"+
                           "<th style='text-align: right;'>😥 Sad</th>"+
                           "<th style='text-align: right;'>😡 Angry</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                            "</tbody>"+
                              "</table>");

}

$(document).on('change', '#tagGroup_filter', function () {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    $('#tag_filter').empty();
     $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gettagBytagGroup')}}", // This is the URL to the API
      data: { tagGroup_id:valueSelected,brand_id:GetURLParameter('pid')}
    })
    .done(function( data ) {
     for(var i in data) {

      $('#tag_filter').append($('<option>', {
            value: data[i]['id'],
            text: data[i]['name']
        }));
     }
      tag_pos=[];tag_neg=[];tag_neutral=[];tagsentiLabel=[];pos_percent_tag=[];neg_percent_tag=[];
        neutral_percent_tag=[];tag_qty_list=[];tagmarkpointlist=[];tagpercentmarkpointlist=[];
     tag_qty_Label = [];tag_qty_seriesList=[];
     tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
     TagQty(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
    
    })
    .fail(function(xhr, textStatus, error) {
     
    });

   
});

$(document).on('change', '#ptagGroup_filter', function () {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    $('#ptag_filter').empty();
     $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gettagBytagGroup')}}", // This is the URL to the API
      data: { tagGroup_id:valueSelected,brand_id:GetURLParameter('pid')}
    })
    .done(function( data ) {
     for(var i in data) {

      $('#ptag_filter').append($('<option>', {
            value: data[i]['id'],
            text: data[i]['name']
        }));
     }
      post_tag_qty_seriesList=[];RseriesList=[];
     PostTagQty(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
     postReaction(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id_arr[0],0);
    })
    .fail(function(xhr, textStatus, error) {
     
    });

   
});



//local customize function
function nFormatter(num, digits ,get_zero=0) {
    if(num<=0 && get_zero==0)
      {
        return '-';
      }
      else
      {
  var si = [
    { value: 1, symbol: "" },
    { value: 1E3, symbol: "k" },
    { value: 1E6, symbol: "M" },
    { value: 1E9, symbol: "G" },
    { value: 1E12, symbol: "T" },
    { value: 1E15, symbol: "P" },
    { value: 1E18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
      }
}

 function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

 function convertKtoThousand(s)
{

    var str=s;
    str=str.toUpperCase();
    if(str === '-')
    {
      return 0;
    }
    if (str.indexOf("K") !== -1) {
    str = str.replace("K",'');
    return parseFloat(str) * 1000;
  } else if (str.indexOf("M") !== -1) {
    str = str.replace("M",'');
     return parseFloat(str) * 1000000;
  } else {
    return parseFloat(str);
  }
  
}
function readmore(message){
        // alert("hi hi ");
          var string = String(message);
          var length = string.length; 
            //alert(length);
                  if (length > 150) {
            // alert("length is greater than 500");

              // truncate

              var stringCut = string.substr(0, 150);
               // alert(stringCut);
              // var endPoint = stringCut.indexOf(" ");

              //if the string doesn't contain any space then it will cut without word basis.
               
              // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
              string =stringCut.substr(0,length);
              // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
              // alert(string);
          }
          return string;


          }
   function numberWithCommas(n) {
      var parts=n.toString().split(".");
      return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
  }
 function LevelCheck(s)
{

    if(parseInt(s) === 1) return "1st";else if(parseInt(s) === 2) return "2nd";else if(parseInt(s) === 3) return "3rd";
    else if(parseInt(s) > 3) return s+"th";
  
}
    function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x1F60D', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }

       
 

       
  });
    </script>
    
<style>
.modal-dialog {
    position: absolute;
    top: 200px;
    right: 100px;
    bottom: 0;
    left: 100px;
    z-index: 10040;
    /* overflow: auto; */
}
.table td, .table th {
    padding: .75rem .5rem .75rem .5rem;
    }
    
.myHeaderContent
{
   margin-right:300px;
}
.table td, .table th {
    vertical-align: top !important;
   
}
.card-body
{
  padding-bottom : 0.1rem;
}
</style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


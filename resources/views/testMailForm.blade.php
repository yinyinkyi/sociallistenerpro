@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
 


              <header class="" id="myHeader">
                <div class="row page-titles" style="padding-bottom:15px;padding-top:15px">
                  <div class="col-md-5 col-8 align-self-center">
                      <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Mail Report</h4>
                  </div>
                </div>
              </header>


                <div class="row" style="margin-top: 10px;">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-body">
                                <form role="form" class="form-horizontal" action="{{route('getMail')}}" method="get" id="myform">
                                    <div class="form-body">
                                     <!--    <h3 class="box-title">Mail Report</h3>
                                        <hr class="m-t-0 m-b-40"> -->
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-12 col-md-offset-3">
                                                <div class="form-group row">
                                                    <!-- <label class="control-label text-right col-md-3">Date Range:</label>
                                                    <div class="col-md-3">
                                                       <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                                         <input type='text' class="form-control dateranges" name="daterange" style="datepicker" />
                                                          <div class="input-group-append">
                                                              <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                              </span>
                                                          </div>
                                                       </div>
                                                    </div> -->
                                                 <!--    <label class="control-label text-right col-md-3">Email:</label>
                                                    <div class="col-md-3">
                                                       <input type="text" name="email" class="form-control">
                                                    </div> -->
                                                    
                                                </div>

                                            </div>
                                        </div>
                                        <br><br>
                                      
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-12 col-md-offset-3">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">who to sent:</label>
                                                    <div class="col-md-3">
                                                        <input type="text" id="email" name="email" class="form-control" placeholder="email address here">
                                                         @if ($errors->any())
                                                            <div class="alert alert-danger">
                                                                <ul>
                                                                    @foreach ($errors->all() as $error)
                                                                        <li>{{ $error }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                          @endif

                                                    </div>
                                                     <div class="col-md-2">
                                                      <button type="submit" class="btn btn-success btn-rounded" style="float:right;">Preview</button>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                     <br><br>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

              
                
              
                
       
@endsection
@push('scripts')

<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>

<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>

<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>

<script src="{{asset('js/waves.js')}}" defer></script>

<script src="{{asset('js/sidebarmenu.js')}}" defer></script>

<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>

<script src="{{asset('js/custom.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>
     <script type="text/javascript">
      var startDate;var endDate;

      $(document).ready(function() {

          var GetURLParameter = function GetURLParameter(sParam) {
          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
              sURLVariables = sPageURL.split('&'),
              sParameterName,
              i;

          for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return sParameterName[1] === undefined ? true : sParameterName[1];
              }
          }
        };

        startDate = moment().subtract(1, 'month');
        endDate = moment();
        label = 'month';

         $('.singledate').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          locale: {
              format: 'DD/MM/YYYY'
          }
        },function(date) {
           
            endDate=date;
            var id =$('input[type=radio][name=period-radio]:checked').attr('id');
            if(id==="period-week")
           {
             startDate = moment(endDate).subtract(1, 'week');
             endDate = endDate;
           }
           else
           {
             startDate = moment(endDate).subtract(1, 'month');
             endDate = endDate;
           }
          
            // $(document).on('click', '#btn_send', function () {
           // ChooseDate(startDate,endDate);
           $('.fday').val()='';
           $('.fday').val()=startDate;
            $('.sday').val()='';
           $('.sday').val()=endDate;
         
        });


         function ChooseDate(start,end){

           $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate=start;
            endDate=end;
             
            sendMail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));

         }

         function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
          }

         // function sendMail(startDate,endDate){
         //     var email =  $('#email').val();
         //     if (validateEmail(email)) {
            
         //     $.ajax({
         //     headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         //     url:'{{ route("getMail") }}',
         //     type: 'GET',
         //     data: {fday:startDate,sday:endDate,email:email,brand_id:GetURLParameter('pid')},
         //    //  success: function(response) {// alert(response)

         //    //  window.open("{{ url('getmail?')}}" +"pid="+ brand_id+"&highlight_text="+ params.name+"&source="+GetURLParameter('source')+"&fday="+GetStartDate()+"&sday="+GetEndDate()+"&admin_page="+admin_page+"&admin_page_id="+admin_page_id+"&default_page="+default_page+"&accountPermission="+accountPermission, '_blank'); 
         //    // }
         //   });
         //     }
         //     else
         //     {
         //        $('.form-control-feedback').text('please enter a valid email address!');
         //     }
            

          

         // }



          $(document).on('click', '#btn_send', function () {
            // ChooseDate(startDate,endDate);

          });



      var permission = GetURLParameter('accountPermission');
   
      $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
                startDate: startDate,
                endDate: endDate,
          },function(start, end,label) {
            if(permission == 'Demo'){
            var minDate = moment().subtract(3, 'months').format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = start.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            if(+from <= +min) 
          
            {
                 swal({
                title: 'Sorry',
                text: "You are allowed to access data for last three months only",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment().subtract(1, 'month');
                endDate = moment();
     
            }


            else{
              var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;

                        $('.fday').empty();
           $('.fday').val =startDate;
            $('.sday').empty();
           $('.sday').val =endDate;
              
              // ChooseDate(startDate,endDate);
           
             }
           }
           else
           {
              var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;
              // ChooseDate(startDate,endDate);

                        $('.fday').empty();
           $('.fday').val =startDate;
            $('.sday').empty();
           $('.sday').val=endDate;
            
           }
        }
      )

      });
  </script>
@endpush


@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
 
      <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Add Fixed Deposit Data</h4>
        </div>
        <div class="col-md-7 col-9 align-self-center">
          <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
            </div>
            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
               <input type='text' class="form-control dateranges" style="datepicker" />
                <div class="input-group-append">
                    <span class="input-group-text">
                      <span class="ti-calendar"></span>
                    </span>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row" >
        <div class="col-lg-12">
            <div class="card card-outline-info">
             
                <div class="card-body">
                    <form action="#" class="form-horizontal" action="" method="post">
                      {{csrf_field()}}
                        <div class="form-body">
                          <!-- data append here -->
                        </div>
                        
                        <div class="form-actions">
  <div class="col-md-6">
      <div class="row" style="padding-left: 20px;">
          <div class="col-md-offset-3 col-md-9">
              <button type="button" id="btn_save" class="btn btn-success">Save</button>
          </div>
      </div>
  </div>
  <div class="col-md-6"> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



              
                
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>
     <script type="text/javascript">
     $(document).ready(function() {
 var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };
  var APP_URL = {!! json_encode(url('/')) !!};

              startDate = moment();
      endDate = moment();
      label = 'day';

             $('.singledate').daterangepicker({
              singleDatePicker: true,
              showDropdowns: true,
              locale: {
                  format: 'DD/MM/YYYY'
              }
          },function(date) {
            // alert(date);
            endDate=date;
           //  var id =$('input[type=radio][name=period-radio]:checked').attr('id');
           //  if(id==="period-week")
           // {
             startDate = moment();
             endDate = date;
           // }
           // else
           // {
             // startDate = moment(endDate).subtract(1, 'month');
             // endDate = endDate;
           // }
          
           // var admin_page=$('.hidden-chip').val();
            // alert(admin_page);     
            // alert('helo');
             // var kw_search = $("#kw_search").val();/
             // alert(kw_search);
             // alert(startDate);
           // ChooseDate(startDate,endDate,admin_page,'','',kw_search);
         
        });




    $('.dateranges').daterangepicker({
      locale: {
              format: 'MMM D, YYYY'
          },
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                 
              },
                startDate: startDate,
                endDate: endDate,
          },function(start, end,label) {
         // alert(start);alert(end);
            var minDate = start.format('MMM DD,YYYY');

            // var selected = $(this).val();
            // var fromDay = selected.split('-');
           
            var fromDay = end.format('MMM DD,YYYY');
        
            // convert them as objects to compare
            var from = new Date(fromDay);
            var min = new Date(minDate); 
        
            // less than equal needs plus sign 
            // less than don't need 
            // if(+from <= min+)
            if(from.getTime()  !== min.getTime()) 
          
            {

                 swal({
                title: 'Sorry',
                text: "Date should not exceed more than one day !",
                type: 'warning',
                showCancelButton: false,
                // confirmButtonColor: '',
                // cancelButtonColor: '',
                // confirmButtonText: '',
                // buttonsStyling: false,
              })
                startDate = moment();
                endDate = moment();
     
            }


            else{
              // alert('eho');
              var startDate;
              var endDate;
              label = label;
              startDate = start;
              endDate = end;
              var kw_search = $("#kw_search").val();
              ChooseDate(startDate,endDate);
              
             }
           
         
        }
      );
    ChooseDate(startDate,endDate);

     function ChooseDate(start,end){
    
        $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            startDate=start;
            endDate=end;
        BindFixedDepoData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
    }

    function GetStartDate(){
   
      return startDate.format('YYYY-MM-DD');
    }
    function GetEndDate(){
              
      return endDate.format('YYYY-MM-DD');
    }
$('.fo-body').append("HELLO");

    function BindFixedDepoData(fday,sday){
     
      $('.form-body').html('');

      $.ajax({
      headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
      url:'{{ route("bind_fixDepoData") }}',
      type: 'GET',
      data: {fday:fday,sday:sday,source:GetURLParameter('source')},
      success: function(data) { 
      console.log(data);
    
      
      for(var i in data){

        var html='';
        var logo = window.location.origin+window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/'))+'/Logo/bank_logo/'+data[i]['logo'];
        // alert(logo);
        html+='<table class="table-responsive table-striped"><thead><tr><th class="d-t"></th><th class="d-t">Minimum</th><th class="d-t">1M</th><th class="d-t">2M</th><th class="d-t">3M</th><th class="d-t">6M</th><th class="d-t">9M</th><th class="d-t">12M</th><th class="d-t">24M</th><th class="d-t">36M</th><th class="d-t">Bank Book/slip</th><th class="d-t">Remarks</th></tr></thead><tbody>';
        html +='<input type="hidden" class="logo" value="'+data[i]['logo']+'">';
        html +='<tr><td><img src="'+logo+'" style="width:50px;height:50px;"><h4 class="box-title bank_name">'+data[i]['bank_name']+'</h4></td>';
        // html +='<div class="row" style="padding-left:20px;">';
        if(data[i]['minium'] != null)
        html +='<td><input type="text" id="minium" class="form-control minium" value="'+data[i]['minium']+'" placeholder="Minimum"></td>';
        else
        html +='<td><input type="text" id="minium" class="form-control minium" value="" placeholder="minium"></td>';
        if(data[i]['month_1'] != null)
        html +='<td><input type="text" id="month_1" class="form-control month_1" value="'+data[i]['month_1']+'" placeholder="Month 1"></td>';
        else
        html +='<td><input type="text" id="month_1" class="form-control month_1" value="" placeholder="Month 1"></td>';
        if(data[i]['month_2'] != null)
        html +='<td><input type="text" id="month_2" class="form-control month_2" value="'+data[i]['month_2']+'" placeholder="Month 2"></td>';
        else
        html +='<td><input type="text" id="month_2" class="form-control month_2" value="" placeholder="Month 2"></td>';
        if(data[i]['month_3'] != null)
        html +='<td><input type="text" id="month_3" class="form-control month_3" value="'+data[i]['month_3']+'" placeholder="Month 3"></td>';
        else
        html +='<td><input type="text" id="month_3" class="form-control month_3" value="" placeholder="Month 3"></td>';
        if(data[i]['month_6'] != null)
        html +='<td><input type="text" id="month_6" class="form-control month_6" value="'+data[i]['month_6']+'" placeholder="Month 6"></td>';
        else
        html +='<td><input type="text" id="month_6" class="form-control month_6" value="" placeholder="Month 6"></td>';
      if(data[i]['month_9'] != null)
        html +='<td><input type="text" id="month_9" class="form-control month_9" value="'+data[i]['month_9']+'" placeholder="Month 9"></td>';
        else
        html +='<td><input type="text" id="month_9" class="form-control month_9" value="" placeholder="Month 9"></td>';
      if(data[i]['month_12'] != null)
        html +='<td><input type="text" id="month_12" class="form-control month_12" value="'+data[i]['month_12']+'" placeholder="Month 12"></td>';
        else
        html +='<td><input type="text" id="month_12" class="form-control month_12" value="" placeholder="Month 12"></td>';
      if(data[i]['month_24'] != null)
        html +='<td><input type="text" id="month_24" class="form-control month_24" value="'+data[i]['month_24']+'" placeholder="Month 24"></td>';
        else
        html +='<td><input type="text" id="month_24" class="form-control month_24" value="" placeholder="Month 24"></td>';
      if(data[i]['month_36'] != null)
        html +='<td><input type="text" id="month_36" class="form-control month_36" value="'+data[i]['month_36']+'" placeholder="Month 36"></td>';
        else
        html +='<td><input type="text" id="month_36" class="form-control month_36" value="" placeholder="Month 36"></td>';
      if(data[i]['bank_book_slip'] != null)
        html +='<td><input type="text" id="bank_book_slip" class="form-control bank_book_slip" value="'+data[i]['bank_book_slip']+'" placeholder="Bank Book/slip"></td>';
        else
        html +='<td><input type="text" id="bank_book_slip" class="form-control bank_book_slip" value="" placeholder="Bank Book/slip"></td>';
        if(data[i]['remarks'] != null)
        html +='<td><input type="text" id="remarks" class="form-control remarks" value="'+data[i]['remarks']+'" placeholder="Remarks"></td>';
        else
        html +='<td><input type="text" id="remarks" class="form-control remarks" value="" placeholder="Remarks"></td></tr></tbody></table>';
       
        // $('.t-body').append(html);
        $('.form-body').append( html );
      }
    }

  });

 }


        $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
         });



        $('#btn_save').on('click',function(e){
           // var arr=[];
           // arr = $('#bank_name').val();
           // alert(arr);
           // alert($('#logo').val());
              var bank_name = [];
             bank_name.push($('.bank_name').map(function () {
                  return $(this).text();
              }).get().join(','));

              var logo = [];
             logo.push($('.logo').map(function () {
                  return $(this).val();
              }).get().join(','));
             // alert(logo);
            

           // if(!($('#usd_buy').val() ))
           // {
           //  $('')
           // }
           // if(!($('#usd_sell').val() ))
           // {
           //  $('.alert-danger').show();
           //  $('.alert-danger').append('<p>Please fill out this field</p>');
           // }
           //  if(!($('#euro_buy').val() ))
           // {
           //  $('.alert-danger').show();
           //  $('.alert-danger').append('<p>Please fill out this field</p>');
           // }
           //  if(!($('#usd_buy').val() ))
           // {
           //  $('.alert-danger').show();
           //  $('.alert-danger').append('<p>Please fill out this field</p>');
           // }
           //            if(!($('#usd_buy').val() ))
           // {
           //  $('.alert-danger').show();
           //  $('.alert-danger').append('<p>Please fill out this field</p>');
           // }
           //            if(!($('#usd_buy').val() ))
           // {
           //  $('.alert-danger').show();
           //  $('.alert-danger').append('<p>Please fill out this field</p>');
           // }
         
           var usd_buy = [];
             usd_buy.push($('.usd_buy').map(function () {
                  return $(this).val();
              }).get().join(','));
           var usd_sell = [];
             usd_sell.push($('.usd_sell').map(function () {
                  return $(this).val();
              }).get().join(','));
           var euro_buy = [];
             euro_buy.push($('.euro_buy').map(function () {
                  return $(this).val();
              }).get().join(','));
           var euro_sell = [];
             euro_sell.push($('.euro_sell').map(function () {
                  return $(this).val();
              }).get().join(','));
           var sgd_buy = [];
             sgd_buy.push($('.sgd_buy').map(function () {
                  return $(this).val();
              }).get().join(','));
           var sgd_sell = [];
             sgd_sell.push($('.sgd_sell').map(function () {
                  return $(this).val();
              }).get().join(','));
            

            $.ajax({
              headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
              url:'{{ route("exchange_store") }}',
              type: 'GET',
              data: {bank_name:bank_name,logo:logo,usd_buy:usd_buy,usd_sell:usd_sell,euro_buy:euro_buy,euro_sell:euro_sell,sgd_buy:sgd_buy,sgd_sell:sgd_sell,fday:GetStartDate(),sday:GetEndDate()},
              success: function(response) { 
                    //console.log(response);

                   //$.each(response.errors, function(key, value){
                   //$('.alert-danger').show();
                   //$('.alert-danger').append('<p>'+value+'</p>');
                   //);
                   // swal.close();
                  window.location = APP_URL+'/exchangeEntry?source=in';
              

              }
            });
          });


        // for icheck

        //      $('.my_page').click(function () {
        //     if ($(this).attr('checked')) {
        //         alert('is checked');
        //     } else {
        //         alert('is not checked');
        //     }
        // })
      });
  </script>
  <style type="text/css">
    ::-webkit-input-placeholder { /* WebKit browsers */
    color:    red;
     opacity: 0.5 !important;
     font-size: 0.7rem;
   }
   .d-t{
    text-align: center;
   }
  </style>
@endpush


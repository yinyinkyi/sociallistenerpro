@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                   <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Word Dictionary</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>



                </div>

      @if(!isset($data))

                @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
                <!-- Row -->
             <div class="row" >
                    <div class="col-12" >
                      
                        <div class="card" >
                            <div class="card-body">
                                <form role="form" class="" action="{{route('wordcloud.store')}}" method="post" id="myform">
                                  {{csrf_field()}}
                                  <div class="box-body">
                                    <div class="row">
                                       <table class="table">
                                        <input type='hidden' name='brand_id' value="{{ $brand_id }}">
                                        <td></td>
                                          <td > Word: </td>
                                          <td><input type="text" class="form-control form-control-line{{ $errors->has('word') ? ' is-invalid' : '' }}" value="{{ old('word') }}" name="word" id="word" autofocus placeholder="Enter word">
                                          @if ($errors->has('word'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('word') }}</strong>
                                                    </span>
                                                @endif
                                              </td> 
                                          <td><button type="submit" class="btn btn-flat btn-green pull-right" style="margin-left: 17px;"  value="">Save</button></td>
                                        </table>
                                     </div>
                                    </div>
                                </form> 
                              </div>
                            </div>            
                          </div>
                          </div>

      @else

        @if(session()->has('message'))
      <div class="">
        {{ session()->get('message') }}
      </div>
       @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        {!! Form::model($data, ['method' => 'PATCH','class'=>'form-material m-t-10','enctype'=>'multipart/form-data','route' => ['wordcloud.update', $data->id]]) !!}
                         {{csrf_field()}}
                                <div class="box-body">
                                    <div class="row">
                                       <table class="table">
                                        
                                        <input type="hidden" id="brand_id" name="brand_id" value="{{ old( 'name', $brand_id) }}" >
                                        <td></td>
                                          <td > Word: </td>
                                          <td><input type="text" class="form-control form-control-line{{ $errors->has('word') ? ' is-invalid' : '' }}" value="{{ old('word', $data->word) }}" name="word" id="word" autofocus>
                                          @if ($errors->has('word'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('word') }}</strong>
                                                    </span>
                                                @endif
                                              </td> 
                                          <td><button type="submit" class="btn btn-flat btn-green pull-right" style="margin-left: 17px;"  value="">Save</button></td>
                                        </table>
                                     </div>
                                    </div>
             {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>
                          @endif
                         <div class="row">
                          <div class="col-12">
                            <div class="card">
                              <div class="card-body">
                                <h4 class="card-title">Words List</h4>
                                 <table id="words_table" class="table">
                                  <thead>
                                    <tr>
                                      <th>Word</th>
                                      <th>Created Time</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>  
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>
     <script type="text/javascript">
     $(document).ready(function() {
  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };

          $('#words_table').DataTable({
       "lengthChange": true,"info": false, "searching": true,
        processing: true,
        serverSide: false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
         "ajax": {
          "url": '{{ route('getwordlist') }}',

 
         "data": {
            "brand_id":GetURLParameter("pid")
          }
 },
         columns: [
            {data: 'word',orderable: true, searchable: true},
            {data: 'created_at',orderable: true, searchable: false},
            {data: 'action', orderable: false, searchable: false}
        ]
        
    }).on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
          var url = $(this).data('remote');
      
                      swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                   $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  data: {method: '_DELETE', submit: true,brand_id:GetURLParameter("pid")}
              }).always(function (data) {
                   $('#words_table').DataTable().rows().invalidate().draw();
                    window.location.reload()
                 
              });
             
              }
            })
      });





      });
  </script>
@endpush

